.class public Lcom/sec/spp/push/notisvc/agent/d;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/sec/spp/push/notisvc/agent/d;

.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:D

.field private final f:I

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/agent/d;->a:Ljava/lang/Object;

    const-class v0, Lcom/sec/spp/push/notisvc/agent/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/agent/d;->c:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "application/json"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/agent/d;->d:Ljava/lang/String;

    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    iput-wide v0, p0, Lcom/sec/spp/push/notisvc/agent/d;->e:D

    const v0, 0xd6d8

    iput v0, p0, Lcom/sec/spp/push/notisvc/agent/d;->f:I

    const v0, 0x1d4c0

    iput v0, p0, Lcom/sec/spp/push/notisvc/agent/d;->g:I

    const-string v0, "bigjoe2013"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/agent/d;->h:Ljava/lang/String;

    const v0, 0x19000

    iput v0, p0, Lcom/sec/spp/push/notisvc/agent/d;->i:I

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/notisvc/agent/d;Ljava/lang/String;)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/agent/d;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Ljava/lang/String;)J
    .locals 5

    new-instance v0, Landroid/os/StatFs;

    invoke-direct {v0, p1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v3, v0

    mul-long v0, v3, v1

    return-wide v0
.end method

.method public static a()Lcom/sec/spp/push/notisvc/agent/d;
    .locals 2

    sget-object v0, Lcom/sec/spp/push/notisvc/agent/d;->b:Lcom/sec/spp/push/notisvc/agent/d;

    if-nez v0, :cond_1

    sget-object v1, Lcom/sec/spp/push/notisvc/agent/d;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/notisvc/agent/d;->b:Lcom/sec/spp/push/notisvc/agent/d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/notisvc/agent/d;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/agent/d;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/agent/d;->b:Lcom/sec/spp/push/notisvc/agent/d;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/sec/spp/push/notisvc/agent/d;->b:Lcom/sec/spp/push/notisvc/agent/d;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;)Ljava/util/Map;
    .locals 5

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "model"

    invoke-static {}, Lcom/sec/spp/push/notisvc/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/a;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "netMcc"

    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-eqz v2, :cond_1

    const-string v1, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "simMcc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz v3, :cond_2

    const-string v1, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "mnc"

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v0
.end method

.method static synthetic a(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/agent/d;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/notisvc/agent/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/content/Context;)Z
    .locals 2

    :try_start_0
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/agent/d;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;)V
    .locals 7

    new-instance v0, Lcom/sec/spp/push/notisvc/agent/e;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/spp/push/notisvc/agent/e;-><init>(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;Lcom/sec/spp/push/notisvc/agent/e;)V

    invoke-virtual {v0, p2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;)V
    .locals 9

    if-nez p6, :cond_0

    const-string v0, "requestHttpsTransmit() : Callback Method is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/agent/d;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string v0, "requestHttpsTransmit() : some parameter is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/agent/d;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "url : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " header : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " body : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " requestMethod : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/agent/d;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x3f0

    invoke-virtual {p6, v0}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/sec/spp/push/notisvc/agent/g;

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/sec/spp/push/notisvc/agent/g;-><init>(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;Lcom/sec/spp/push/notisvc/agent/g;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;)V
    .locals 8

    if-nez p5, :cond_0

    const-string v0, "requestHttpTransmit() : Callback Method is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/agent/d;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string v0, "requestHttpTransmit() : some parameter is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/agent/d;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "url : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " header : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " body : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " requestMethod : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/agent/d;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x3f0

    invoke-virtual {p5, v0}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/sec/spp/push/notisvc/agent/f;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/spp/push/notisvc/agent/f;-><init>(Lcom/sec/spp/push/notisvc/agent/d;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;Lcom/sec/spp/push/notisvc/agent/f;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
