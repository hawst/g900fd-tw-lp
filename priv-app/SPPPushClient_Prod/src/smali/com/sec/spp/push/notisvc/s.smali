.class Lcom/sec/spp/push/notisvc/s;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/s;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    const-string v0, "delete card/feedback data"

    const-string v1, "NotiSvcActivity"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/s;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/s;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v1, "All Cards and Feedbacks are deleted"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/card/k;->a()Lcom/sec/spp/push/notisvc/card/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/k;->b()V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/s;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/s;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/s;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v1, "ERROR : Cannot delete cardfeedbackData. dbhandler is null."

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/a/b;->d()Z

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0
.end method
