.class public Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;
.super Lcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

.field private pkg:Ljava/lang/String;

.field private version:Ljava/lang/String;

.field private final versionNull:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/e;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/e;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;-><init>()V

    const-string v0, "version_null"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->versionNull:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->a(Landroid/os/Parcel;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)V
    .locals 1

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;-><init>()V

    const-string v0, "version_null"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->versionNull:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->pkg:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->version:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)V

    return-object v0
.end method

.method private a(Landroid/os/Parcel;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->pkg:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->version:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->version:Ljava/lang/String;

    const-string v1, "version_null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->version:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 4

    const-string v0, "RegistrationAlarmCallBack. Registration Alarm Received."

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v0, "onReceive. ctx is null. do nothing"

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onReceive. regiFlag is null. regiFlag:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v0, "onReceive. dbHandler is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->pkg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/notisvc/a/b;->s(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v0, "Status is null"

    sget-object v2, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Status is already changed. Alarm Status : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v3}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "Now Status : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.NOTIFICATION_NOTICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "agreement"

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "pkgName"

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->pkg:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "version"

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->version:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PKG : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->pkg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", version : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", RegiStatusFlag Flag : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->pkg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->version:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "version_null"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->version:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
