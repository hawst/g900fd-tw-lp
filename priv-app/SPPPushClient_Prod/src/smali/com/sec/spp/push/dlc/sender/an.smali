.class public final enum Lcom/sec/spp/push/dlc/sender/an;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/dlc/sender/an;

.field public static final enum b:Lcom/sec/spp/push/dlc/sender/an;

.field public static final enum c:Lcom/sec/spp/push/dlc/sender/an;

.field public static final enum d:Lcom/sec/spp/push/dlc/sender/an;

.field public static final enum e:Lcom/sec/spp/push/dlc/sender/an;

.field public static final enum f:Lcom/sec/spp/push/dlc/sender/an;

.field public static final enum g:Lcom/sec/spp/push/dlc/sender/an;

.field private static final synthetic i:[Lcom/sec/spp/push/dlc/sender/an;


# instance fields
.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/sec/spp/push/dlc/sender/an;

    const-string v1, "SUCCESS"

    const-string v2, "1000"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/spp/push/dlc/sender/an;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/an;->a:Lcom/sec/spp/push/dlc/sender/an;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/an;

    const-string v1, "NO_UPDATE"

    const-string v2, "1010"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/spp/push/dlc/sender/an;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/an;->b:Lcom/sec/spp/push/dlc/sender/an;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/an;

    const-string v1, "WRONG_REQUEST_FORMAT"

    const-string v2, "2001"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/spp/push/dlc/sender/an;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/an;->c:Lcom/sec/spp/push/dlc/sender/an;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/an;

    const-string v1, "BLOCKED_DEVICE_TOKEN"

    const-string v2, "2002"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/spp/push/dlc/sender/an;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/an;->d:Lcom/sec/spp/push/dlc/sender/an;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/an;

    const-string v1, "TOO_LONG_REQUEST_MSG"

    const-string v2, "2003"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/spp/push/dlc/sender/an;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/an;->e:Lcom/sec/spp/push/dlc/sender/an;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/an;

    const-string v1, "EXCEEDED_DAILY_QUOTA"

    const/4 v2, 0x5

    const-string v3, "2004"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/dlc/sender/an;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/an;->f:Lcom/sec/spp/push/dlc/sender/an;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/an;

    const-string v1, "INTERNAL_SERVER_ERROR"

    const/4 v2, 0x6

    const-string v3, "3001"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/dlc/sender/an;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/an;->g:Lcom/sec/spp/push/dlc/sender/an;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/spp/push/dlc/sender/an;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/an;->a:Lcom/sec/spp/push/dlc/sender/an;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/spp/push/dlc/sender/an;->b:Lcom/sec/spp/push/dlc/sender/an;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/spp/push/dlc/sender/an;->c:Lcom/sec/spp/push/dlc/sender/an;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/spp/push/dlc/sender/an;->d:Lcom/sec/spp/push/dlc/sender/an;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/spp/push/dlc/sender/an;->e:Lcom/sec/spp/push/dlc/sender/an;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/spp/push/dlc/sender/an;->f:Lcom/sec/spp/push/dlc/sender/an;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/spp/push/dlc/sender/an;->g:Lcom/sec/spp/push/dlc/sender/an;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/spp/push/dlc/sender/an;->i:[Lcom/sec/spp/push/dlc/sender/an;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/sec/spp/push/dlc/sender/an;->h:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/an;
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/dlc/sender/an;->values()[Lcom/sec/spp/push/dlc/sender/an;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    iget-object v5, v1, Lcom/sec/spp/push/dlc/sender/an;->h:Ljava/lang/String;

    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/an;
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/sender/an;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/sender/an;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/dlc/sender/an;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/dlc/sender/an;->i:[Lcom/sec/spp/push/dlc/sender/an;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/dlc/sender/an;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
