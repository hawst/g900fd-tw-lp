.class public Lcom/sec/spp/push/notisvc/registration/a;
.super Lcom/sec/spp/push/notisvc/registration/h;


# static fields
.field private static b:Z

.field private static final c:Ljava/lang/Object;

.field private static final d:Ljava/lang/String;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/sec/spp/push/notisvc/registration/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/a;->c:Ljava/lang/Object;

    const-class v0, Lcom/sec/spp/push/notisvc/registration/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)V
    .locals 1

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/h;-><init>()V

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/app/agree/"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/a;->e:Ljava/lang/String;

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/app/disagree/"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/a;->f:Ljava/lang/String;

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/app/deregistration/"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/a;->g:Ljava/lang/String;

    const-string v0, "service"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/a;->h:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/registration/a;->i:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/spp/push/notisvc/registration/a;->j:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/spp/push/notisvc/registration/a;->k:Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/sec/spp/push/notisvc/registration/a;
    .locals 5

    const/4 v0, 0x0

    if-nez p0, :cond_0

    const-string v1, "buildFactory. intent is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "agreement"

    sget v2, Lcom/sec/spp/push/notisvc/registration/a;->a:I

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "buildFactory. status is null. status num : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "buildFactory. status is wrong. status num : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v1, "pkgName"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    const-string v1, "buildFactory. package name is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, "version"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "buildFactory. status:"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", pkg:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", version:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/a;

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/spp/push/notisvc/registration/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)V

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 14

    const/4 v7, 0x0

    const-string v0, "setAlarmAfterBoot."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_0

    const-string v0, "Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v9

    if-nez v9, :cond_1

    const-string v0, "dbHandler is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;-><init>()V

    const/4 v1, 0x4

    new-array v10, v1, [Lcom/sec/spp/push/notisvc/registration/k;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->g:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v1, v10, v7

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/k;->h:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v2, v10, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v2, v10, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v2, v10, v1

    move v6, v7

    :goto_1
    array-length v1, v10

    if-lt v6, v1, :cond_2

    invoke-virtual {v9}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_2
    aget-object v1, v10, v6

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    invoke-virtual {v9, v1}, Lcom/sec/spp/push/notisvc/a/b;->a(I)Ljava/util/ArrayList;

    move-result-object v11

    if-nez v11, :cond_3

    const-string v0, "regiPkg is null. DB Error"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_3
    move v8, v7

    :goto_2
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v8, v1, :cond_4

    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v9, v1}, Lcom/sec/spp/push/notisvc/a/b;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "regitimer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/32 v12, 0x493e0

    add-long/2addr v3, v12

    aget-object v12, v10, v6

    invoke-static {v1, v5, v12}, Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/alarm/SVCAgreementAlarmHandler;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;)V

    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_2
.end method

.method public static b(Landroid/content/Context;)V
    .locals 9

    const/4 v2, 0x0

    if-nez p0, :cond_0

    const-string v0, "context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v0, "dbHandler is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    new-array v5, v0, [I

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    aput v0, v5, v2

    const/4 v0, 0x1

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->g:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    aput v1, v5, v0

    const/4 v0, 0x2

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->h:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    aput v1, v5, v0

    move v1, v2

    :goto_1
    array-length v0, v5

    if-lt v1, v0, :cond_2

    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_2
    aget v0, v5, v1

    invoke-virtual {v4, v0}, Lcom/sec/spp/push/notisvc/a/b;->a(I)Ljava/util/ArrayList;

    move-result-object v6

    if-nez v6, :cond_3

    const-string v0, "db error. incompleted list is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_3
    move v3, v2

    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    aget v0, v5, v1

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v7

    if-eqz v7, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "request app agree : "

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ", pkg : "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v8, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, Landroid/content/Intent;

    const-string v0, "com.sec.spp.push.NOTIFICATION_NOTICE"

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "agreement"

    invoke-virtual {v7}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v7

    invoke-virtual {v8, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v7, "pkgName"

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "version"

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/sec/spp/push/notisvc/a/b;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "wrong app agree value : "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v7, v5, v1

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", pkg : "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v7, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v7}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "wrong app agree value : "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v7, v5, v1

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", pkg : "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v7, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v7}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method private l(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 4

    if-nez p1, :cond_0

    const-string v0, "Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "dbHandler is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/a;->k:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->b(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v0, "Incomplete Status is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/sec/spp/push/notisvc/registration/a;->i:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    iget-object v3, p0, Lcom/sec/spp/push/notisvc/registration/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/spp/push/notisvc/a/b;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/sec/spp/push/notisvc/a/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    sget-object v0, Lcom/sec/spp/push/notisvc/a/a;->a:Lcom/sec/spp/push/notisvc/a/a;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/notisvc/a/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "DBEvent is error. DBEvent : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "setOnWorking : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean p1, Lcom/sec/spp/push/notisvc/registration/a;->b:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()Z
    .locals 2

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/spp/push/notisvc/registration/a;->b:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method b()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/a;->k:Lcom/sec/spp/push/notisvc/registration/k;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/app/agree/"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/a;->k:Lcom/sec/spp/push/notisvc/registration/k;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/app/disagree/"

    goto :goto_0

    :cond_1
    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/app/deregistration/"

    goto :goto_0
.end method

.method c(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 2

    if-nez p1, :cond_0

    const-string v0, "Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/registration/l;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/a;->k:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "checkBeforeRequest. Not yet Device Reg completed. Do Device Reg"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/registration/a;->h(Landroid/content/Context;)V

    :cond_1
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->b:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0
.end method

.method d(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 2

    const-string v0, "delayRequest."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/registration/a;->l(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;

    move-result-object v0

    return-object v0
.end method

.method e(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/registration/a;->l(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;

    move-result-object v0

    return-object v0
.end method

.method f(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "service"

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/registration/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    const-string v0, "getHttpBody. getSVCBody JSONEXception"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method g(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/agent/h;
    .locals 7

    const/4 v6, 0x0

    if-nez p1, :cond_0

    const-string v0, "Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v6

    :cond_0
    new-instance v0, Lcom/sec/spp/push/notisvc/registration/b;

    iget-object v3, p0, Lcom/sec/spp/push/notisvc/registration/a;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/spp/push/notisvc/registration/a;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/a;->k:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->b(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/spp/push/notisvc/registration/b;-><init>(Lcom/sec/spp/push/notisvc/registration/a;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;Lcom/sec/spp/push/notisvc/registration/b;)V

    move-object v6, v0

    goto :goto_0
.end method
