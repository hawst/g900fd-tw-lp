.class public Lcom/sec/spp/push/dlc/writer/b;
.super Lcom/sec/spp/push/dlc/a/b;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/writer/WriterService;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/dlc/writer/WriterService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/a/b;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 5

    const/16 v4, 0x14

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/4 v0, -0x1

    if-eqz p2, :cond_0

    :try_start_0
    const-string v1, "utf-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    if-eq v1, v2, :cond_1

    :cond_0
    const-string v1, "RC_INVALID_PARAMETER [svcCode]"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    const-string v1, "utf-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    if-eq v1, v2, :cond_4

    :cond_2
    const-string v1, "RC_INVALID_PARAMETER [category]"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    if-eqz p3, :cond_5

    :try_start_1
    const-string v1, "utf-8"

    invoke-virtual {p3, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    if-lt v1, v3, :cond_5

    const-string v1, "utf-8"

    invoke-virtual {p3, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    if-le v1, v4, :cond_6

    :cond_5
    const-string v1, "RC_INVALID_PARAMETER [activity]"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    if-eqz p4, :cond_8

    const-string v1, "utf-8"

    invoke-virtual {p4, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    if-lt v1, v3, :cond_7

    const-string v1, "utf-8"

    invoke-virtual {p4, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    const/16 v2, 0x3c

    if-le v1, v2, :cond_8

    :cond_7
    const-string v1, "RC_INVALID_PARAMETER [deviceId]"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "utf-8"

    invoke-virtual {p5, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_9

    const-string v1, "RC_INVALID_PARAMETER [userId]"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_9
    if-eqz p6, :cond_a

    const-string v1, "utf-8"

    invoke-virtual {p6, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    if-lt v1, v3, :cond_a

    const-string v1, "utf-8"

    invoke-virtual {p6, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    if-le v1, v4, :cond_b

    :cond_a
    const-string v1, "RC_INVALID_PARAMETER [appVersion]"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    if-eqz p7, :cond_c

    const-string v1, "utf-8"

    invoke-virtual {p7, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    if-lt v1, v3, :cond_c

    const-string v1, "utf-8"

    invoke-virtual {p7, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    const/16 v2, 0x382

    if-le v1, v2, :cond_3

    :cond_c
    const-string v1, "RC_INVALID_PARAMETER [body]"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 11

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    iget-object v1, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(I)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    const-string v0, ":"

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, -0x4

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v0, v10}, Lcom/sec/spp/push/dlc/writer/WriterService;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v0, v10}, Lcom/sec/spp/push/dlc/writer/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "Failed to access token check"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x4

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v0, v10}, Lcom/sec/spp/push/dlc/writer/WriterService;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, -0x6

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v0, v10}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "This App is blocked"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x3

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/db/DbService;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "Writer unavailable now"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x2

    goto :goto_0

    :cond_6
    new-instance v0, Lcom/sec/spp/push/dlc/db/Log;

    move-object v1, p2

    move-object v2, p1

    move-wide v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/sec/spp/push/dlc/db/Log;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/Log;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Lcom/sec/spp/push/dlc/writer/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Lcom/sec/spp/push/dlc/writer/WriterService;Lcom/sec/spp/push/dlc/db/Log;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v0, "over send log limit"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x3

    goto/16 :goto_0

    :cond_8
    iget-object v1, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->D:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v1, v2, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Lcom/sec/spp/push/dlc/writer/WriterService;)V

    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJJJ)I
    .locals 23

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(I)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    const-string v1, ":"

    invoke-virtual {v11, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, -0x4

    :goto_0
    return v1

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v1, v11}, Lcom/sec/spp/push/dlc/writer/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "Failed to access token check"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x4

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v1, v11}, Lcom/sec/spp/push/dlc/writer/WriterService;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, -0x6

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/db/DbService;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "Writer unavailable now"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "FAILED : RC_SVC_UNAVAILABLE"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x2

    goto :goto_0

    :cond_4
    new-instance v1, Lcom/sec/spp/push/dlc/db/SummLog;

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v12, p10

    move-wide/from16 v13, p11

    move-wide/from16 v15, p13

    move-wide/from16 v17, p15

    move-wide/from16 v19, p17

    move-wide/from16 v21, p19

    invoke-direct/range {v1 .. v22}, Lcom/sec/spp/push/dlc/db/SummLog;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJJJ)V

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/SummLog;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v2 .. v9}, Lcom/sec/spp/push/dlc/writer/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_5

    const-string v1, "checkParameters error"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v2, v11}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v1, "This App is blocked"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x3

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v2, v1}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Lcom/sec/spp/push/dlc/writer/WriterService;Lcom/sec/spp/push/dlc/db/Log;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v1, "over send log limit"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x3

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    sget-object v3, Lcom/sec/spp/push/dlc/sender/i;->F:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v2, v3, v1}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Lcom/sec/spp/push/dlc/writer/WriterService;)V

    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJJJ)I
    .locals 23

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(I)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    const-string v1, ":"

    invoke-virtual {v11, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, -0x4

    :goto_0
    return v1

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v1, v11}, Lcom/sec/spp/push/dlc/writer/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "Failed to access token check"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x4

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v1, v11}, Lcom/sec/spp/push/dlc/writer/WriterService;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, -0x6

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v1, v11}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "This App is blocked"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x3

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/db/DbService;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "Writer unavailable now"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x2

    goto :goto_0

    :cond_5
    new-instance v1, Lcom/sec/spp/push/dlc/db/SummLog;

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v12, p10

    move-wide/from16 v13, p11

    move-wide/from16 v15, p13

    move-wide/from16 v17, p15

    move-wide/from16 v19, p17

    move-wide/from16 v21, p19

    invoke-direct/range {v1 .. v22}, Lcom/sec/spp/push/dlc/db/SummLog;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJJJ)V

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/SummLog;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v2 .. v9}, Lcom/sec/spp/push/dlc/writer/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_6

    const-string v1, "checkParameters error"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v2, v1}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Lcom/sec/spp/push/dlc/writer/WriterService;Lcom/sec/spp/push/dlc/db/Log;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v1, -0x3

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    sget-object v3, Lcom/sec/spp/push/dlc/sender/i;->F:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v2, v3, v1}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Lcom/sec/spp/push/dlc/writer/WriterService;)V

    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 11

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/writer/WriterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "RequestSend PID ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pkg "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, -0x4

    :goto_1
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v3, v1, :cond_0

    iget-object v10, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    if-eqz v10, :cond_3

    const-string v0, ":"

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, -0x4

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v0, v10}, Lcom/sec/spp/push/dlc/writer/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Failed to access token check"

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x4

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->k(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "urgentPkg ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pkg "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/dlc/writer/WriterService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x5

    goto :goto_1

    :cond_7
    new-instance v0, Lcom/sec/spp/push/dlc/db/Log;

    move-object v1, p2

    move-object v2, p1

    move-wide v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/sec/spp/push/dlc/db/Log;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/Log;->toString()Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Lcom/sec/spp/push/dlc/writer/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    move v0, v1

    goto/16 :goto_1

    :cond_8
    iget-object v1, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->E:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v1, v2, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/b;->a:Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Lcom/sec/spp/push/dlc/writer/WriterService;)V

    const/4 v0, 0x0

    goto/16 :goto_1
.end method
