.class public final Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
.super Lcom/google/protobuf/GeneratedMessage;


# static fields
.field public static final ACK_INFO_FIELD_NUMBER:I = 0x4

.field public static final ACK_TYPE_FIELD_NUMBER:I = 0x3

.field public static final DEVICE_TOKEN_FIELD_NUMBER:I = 0x1

.field public static final NOTI_IDS_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;


# instance fields
.field private ackInfo_:Ljava/lang/String;

.field private ackType_:I

.field private deviceToken_:Ljava/lang/String;

.field private hasAckInfo:Z

.field private hasAckType:Z

.field private hasDeviceToken:Z

.field private memoizedSerializedSize:I

.field private notiIds_:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;-><init>(Z)V

    sput-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->internalForceInit()V

    sget-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    invoke-direct {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessage;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->deviceToken_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->notiIds_:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->ackType_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->ackInfo_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/pns/msg/frontend/MsgFrontend$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessage;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->deviceToken_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->notiIds_:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->ackType_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->ackInfo_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$15200(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->notiIds_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$15202(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->notiIds_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$15302(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasDeviceToken:Z

    return p1
.end method

.method static synthetic access$15402(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->deviceToken_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$15502(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasAckType:Z

    return p1
.end method

.method static synthetic access$15602(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;I)I
    .locals 0

    iput p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->ackType_:I

    return p1
.end method

.method static synthetic access$15702(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasAckInfo:Z

    return p1
.end method

.method static synthetic access$15802(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->ackInfo_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    sget-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    return-object v0
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiAcks_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$14700()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v0

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;
    .locals 1

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->create()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$15000()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeFrom(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 2

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 2

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->access$14900(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAckInfo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->ackInfo_:Ljava/lang/String;

    return-object v0
.end method

.method public getAckType()I
    .locals 1

    iget v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->ackType_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;
    .locals 1

    sget-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    return-object v0
.end method

.method public getDeviceToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->deviceToken_:Ljava/lang/String;

    return-object v0
.end method

.method public getNotiIds(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->notiIds_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNotiIdsCount()I
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->notiIds_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getNotiIdsList()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->notiIds_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v2, 0x0

    iget v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasDeviceToken()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getDeviceToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v0, v2

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getNotiIdsList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    goto :goto_2

    :cond_1
    add-int v0, v1, v2

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getNotiIdsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasAckType()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getAckType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasAckInfo()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getAckInfo()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/UnknownFieldSet;->getSerializedSize()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->memoizedSerializedSize:I

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public hasAckInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasAckInfo:Z

    return v0
.end method

.method public hasAckType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasAckType:Z

    return v0
.end method

.method public hasDeviceToken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasDeviceToken:Z

    return v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    .locals 1

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiAcks_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$14800()Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilderForType()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilderForType()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->toBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->toBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder(Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasDeviceToken()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getDeviceToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getNotiIdsList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasAckType()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getAckType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->hasAckInfo()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getAckInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protobuf/UnknownFieldSet;->writeTo(Lcom/google/protobuf/CodedOutputStream;)V

    return-void
.end method
