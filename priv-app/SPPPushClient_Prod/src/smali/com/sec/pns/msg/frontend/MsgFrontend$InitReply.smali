.class public final Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
.super Lcom/google/protobuf/GeneratedMessage;


# static fields
.field public static final ASYNC_ID_FIELD_NUMBER:I = 0x1

.field public static final RESULT_CODE_FIELD_NUMBER:I = 0x2

.field public static final RESULT_MSG_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;


# instance fields
.field private asyncId_:I

.field private hasAsyncId:Z

.field private hasResultCode:Z

.field private hasResultMsg:Z

.field private memoizedSerializedSize:I

.field private resultCode_:I

.field private resultMsg_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;-><init>(Z)V

    sput-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->internalForceInit()V

    sget-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    invoke-direct {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessage;-><init>()V

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->asyncId_:I

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->resultCode_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->resultMsg_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/pns/msg/frontend/MsgFrontend$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessage;-><init>()V

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->asyncId_:I

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->resultCode_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->resultMsg_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$2802(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasAsyncId:Z

    return p1
.end method

.method static synthetic access$2902(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;I)I
    .locals 0

    iput p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->asyncId_:I

    return p1
.end method

.method static synthetic access$3002(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasResultCode:Z

    return p1
.end method

.method static synthetic access$3102(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;I)I
    .locals 0

    iput p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->resultCode_:I

    return p1
.end method

.method static synthetic access$3202(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasResultMsg:Z

    return p1
.end method

.method static synthetic access$3302(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->resultMsg_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    sget-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    return-object v0
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_InitReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$2300()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v0

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;
    .locals 1

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->create()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2600()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeFrom(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 2

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 2

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;->access$2500(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAsyncId()I
    .locals 1

    iget v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->asyncId_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;
    .locals 1

    sget-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    return-object v0
.end method

.method public getResultCode()I
    .locals 1

    iget v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->resultCode_:I

    return v0
.end method

.method public getResultMsg()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->resultMsg_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    iget v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasAsyncId()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getAsyncId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasResultCode()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getResultCode()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasResultMsg()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getResultMsg()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/UnknownFieldSet;->getSerializedSize()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasAsyncId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasAsyncId:Z

    return v0
.end method

.method public hasResultCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasResultCode:Z

    return v0
.end method

.method public hasResultMsg()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasResultMsg:Z

    return v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    .locals 1

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_InitReply_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$2400()Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilderForType()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilderForType()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->toBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->toBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->newBuilder(Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasAsyncId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getAsyncId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasResultCode()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getResultCode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->hasResultMsg()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getResultMsg()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protobuf/UnknownFieldSet;->writeTo(Lcom/google/protobuf/CodedOutputStream;)V

    return-void
.end method
