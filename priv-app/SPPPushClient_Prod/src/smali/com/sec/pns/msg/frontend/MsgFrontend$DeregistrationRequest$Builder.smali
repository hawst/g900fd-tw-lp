.class public final Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
.super Lcom/google/protobuf/GeneratedMessage$Builder;


# instance fields
.field private result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessage$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$6600(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;
    .locals 1

    invoke-direct {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6700()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->create()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->newUninitializedMessageException(Lcom/google/protobuf/Message;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->buildPartial()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 3

    new-instance v0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    invoke-direct {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;-><init>()V

    new-instance v1, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;-><init>(Lcom/sec/pns/msg/frontend/MsgFrontend$1;)V

    iput-object v1, v0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->build()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->build()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->newUninitializedMessageException(Lcom/google/protobuf/Message;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->buildPartial()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->buildPartial()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->buildPartial()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;
    .locals 2

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->clear()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->clear()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->clear()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call clear() after build()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;-><init>(Lcom/sec/pns/msg/frontend/MsgFrontend$1;)V

    iput-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    return-object p0
.end method

.method public clearAsyncId()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasAsyncId:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$6902(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->asyncId_:I
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7002(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;I)I

    return-object p0
.end method

.method public clearDeviceToken()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasDeviceToken:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7102(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDefaultInstance()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDeviceToken()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->deviceToken_:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7202(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public clearMumId()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasMumId:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7702(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDefaultInstance()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getMumId()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->mumId_:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7802(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public clearRegId()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasRegId:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7302(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDefaultInstance()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getRegId()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->regId_:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7402(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public clearUserData()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasUserData:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7502(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDefaultInstance()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getUserData()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->userData_:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7602(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->clone()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->clone()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessage$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->clone()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->clone()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->clone()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->create()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->mergeFrom(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->clone()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAsyncId()I
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getAsyncId()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDefaultInstance()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptorForType()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDeviceToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMumId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getMumId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRegId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getRegId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getUserData()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasAsyncId()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasAsyncId()Z

    move-result v0

    return v0
.end method

.method public hasDeviceToken()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasDeviceToken()Z

    move-result v0

    return v0
.end method

.method public hasMumId()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasMumId()Z

    move-result v0

    return v0
.end method

.method public hasRegId()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasRegId()Z

    move-result v0

    return v0
.end method

.method public hasUserData()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasUserData()Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic internalGetResult()Lcom/google/protobuf/GeneratedMessage;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->internalGetResult()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method protected internalGetResult()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/AbstractMessage$Builder;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/UnknownFieldSet;->newBuilder(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/UnknownFieldSet$Builder;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/UnknownFieldSet$Builder;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/protobuf/UnknownFieldSet$Builder;->build()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/GeneratedMessage$Builder;

    :goto_1
    return-object p0

    :sswitch_0
    invoke-virtual {v0}, Lcom/google/protobuf/UnknownFieldSet$Builder;->build()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/GeneratedMessage$Builder;

    goto :goto_1

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setAsyncId(I)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setDeviceToken(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setRegId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setUserData(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setMumId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/protobuf/Message;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 1

    instance-of v0, p1, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    invoke-virtual {p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->mergeFrom(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/GeneratedMessage$Builder;->mergeFrom(Lcom/google/protobuf/Message;)Lcom/google/protobuf/AbstractMessage$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDefaultInstance()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasAsyncId()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getAsyncId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setAsyncId(I)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasDeviceToken()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getDeviceToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setDeviceToken(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasRegId()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getRegId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setRegId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasUserData()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getUserData()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setUserData(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasMumId()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getMumId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setMumId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->mergeUnknownFields(Lcom/google/protobuf/UnknownFieldSet;)Lcom/google/protobuf/GeneratedMessage$Builder;

    goto :goto_0
.end method

.method public setAsyncId(I)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasAsyncId:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$6902(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->asyncId_:I
    invoke-static {v0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7002(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;I)I

    return-object p0
.end method

.method public setDeviceToken(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasDeviceToken:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7102(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->deviceToken_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7202(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setMumId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasMumId:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7702(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->mumId_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7802(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setRegId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasRegId:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7302(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->regId_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7402(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setUserData(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->hasUserData:Z
    invoke-static {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7502(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Z)Z

    iget-object v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->result:Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->userData_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->access$7602(Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
