.class public final Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
.super Lcom/google/protobuf/GeneratedMessage;


# static fields
.field public static final ASYNC_ID_FIELD_NUMBER:I = 0x1

.field public static final CREATED_TIME_FIELD_NUMBER:I = 0x2

.field public static final INTERVAL_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;


# instance fields
.field private asyncId_:I

.field private createdTime_:J

.field private hasAsyncId:Z

.field private hasCreatedTime:Z

.field private hasInterval:Z

.field private interval_:I

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;-><init>(Z)V

    sput-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->internalForceInit()V

    sget-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    invoke-direct {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessage;-><init>()V

    iput v2, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->asyncId_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->createdTime_:J

    iput v2, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->interval_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/pns/msg/frontend/MsgFrontend$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessage;-><init>()V

    iput v2, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->asyncId_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->createdTime_:J

    iput v2, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->interval_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$10002(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->createdTime_:J

    return-wide p1
.end method

.method static synthetic access$10102(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasInterval:Z

    return p1
.end method

.method static synthetic access$10202(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;I)I
    .locals 0

    iput p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->interval_:I

    return p1
.end method

.method static synthetic access$9702(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasAsyncId:Z

    return p1
.end method

.method static synthetic access$9802(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;I)I
    .locals 0

    iput p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->asyncId_:I

    return p1
.end method

.method static synthetic access$9902(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasCreatedTime:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    sget-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    return-object v0
.end method

.method public static final getDescriptor()Lcom/google/protobuf/Descriptors$Descriptor;
    .locals 1

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_PingRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$9200()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v0

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;
    .locals 1

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->create()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9500()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeFrom(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 2

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 2

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessage$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    # invokes: Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->buildParsed()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->access$9400(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAsyncId()I
    .locals 1

    iget v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->asyncId_:I

    return v0
.end method

.method public getCreatedTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->createdTime_:J

    return-wide v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;
    .locals 1

    sget-object v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->defaultInstance:Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    return-object v0
.end method

.method public getInterval()I
    .locals 1

    iget v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->interval_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasAsyncId()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getAsyncId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasCreatedTime()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getCreatedTime()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasInterval()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getInterval()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/UnknownFieldSet;->getSerializedSize()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasAsyncId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasAsyncId:Z

    return v0
.end method

.method public hasCreatedTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasCreatedTime:Z

    return v0
.end method

.method public hasInterval()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasInterval:Z

    return v0
.end method

.method protected internalGetFieldAccessorTable()Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    .locals 1

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_PingRequest_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$9300()Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilderForType()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilderForType()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;
    .locals 1

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/Message$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->toBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->toBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder(Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasAsyncId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getAsyncId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasCreatedTime()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getCreatedTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->hasInterval()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getInterval()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->getUnknownFields()Lcom/google/protobuf/UnknownFieldSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protobuf/UnknownFieldSet;->writeTo(Lcom/google/protobuf/CodedOutputStream;)V

    return-void
.end method
