.class public Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice;
.super Lorg/jboss/netty/handler/codec/frame/FrameDecoder;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/jboss/netty/handler/codec/frame/FrameDecoder;-><init>()V

    return-void
.end method


# virtual methods
.method protected decode(Lorg/jboss/netty/channel/ChannelHandlerContext;Lorg/jboss/netty/channel/Channel;Lorg/jboss/netty/buffer/ChannelBuffer;)Ljava/lang/Object;
    .locals 9

    const/4 v1, 0x0

    invoke-interface {p2}, Lorg/jboss/netty/channel/Channel;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->markReaderIndex()V

    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->readableBytes()I

    move-result v0

    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->readByte()B

    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->readByte()B

    move-result v5

    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->readShort()S

    move-result v0

    invoke-static {v5}, Lcom/sec/pns/msg/frontend/MsgFrontendCommon;->checkMessageType(B)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->resetReaderIndex()V

    const/16 v0, 0x7d0

    const-string v2, "Unknown message type. Connection will be closed."

    invoke-static {v0, v2}, Lcom/sec/pns/msg/frontend/MsgFrontendCommon;->generateNotiGroupMsgForSystemError(ILjava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup;

    move-result-object v0

    invoke-interface {p2, v0}, Lorg/jboss/netty/channel/Channel;->write(Ljava/lang/Object;)Lorg/jboss/netty/channel/ChannelFuture;

    move-result-object v0

    new-instance v2, Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice$1;

    invoke-direct {v2, p0}, Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice$1;-><init>(Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice;)V

    invoke-interface {v0, v2}, Lorg/jboss/netty/channel/ChannelFuture;->addListener(Lorg/jboss/netty/channel/ChannelFutureListener;)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->readableBytes()I

    move-result v2

    if-ge v2, v0, :cond_4

    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->resetReaderIndex()V

    move-object v0, v1

    goto :goto_0

    :cond_4
    new-array v0, v0, [B

    invoke-interface {p3, v0}, Lorg/jboss/netty/buffer/ChannelBuffer;->readBytes([B)V

    const/16 v2, 0xd

    if-ne v2, v5, :cond_6

    :try_start_0
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v3, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v3, v4}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/16 v0, 0x400

    :try_start_3
    new-array v0, v0, [B

    :goto_1
    const/4 v6, 0x0

    array-length v7, v0

    invoke-virtual {v3, v0, v6, v7}, Ljava/util/zip/GZIPInputStream;->read([BII)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_5

    const/4 v7, 0x0

    invoke-virtual {v2, v0, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    :goto_2
    :try_start_4
    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->resetReaderIndex()V

    const/16 v4, 0x7d5

    const-string v5, "Message decompression failed. Connection will be closed."

    invoke-static {v4, v5}, Lcom/sec/pns/msg/frontend/MsgFrontendCommon;->generateNotiGroupMsgForSystemError(ILjava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup;

    move-result-object v4

    invoke-interface {p2, v4}, Lorg/jboss/netty/channel/Channel;->write(Ljava/lang/Object;)Lorg/jboss/netty/channel/ChannelFuture;

    move-result-object v4

    new-instance v5, Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice$2;

    invoke-direct {v5, p0}, Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice$2;-><init>(Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice;)V

    invoke-interface {v4, v5}, Lorg/jboss/netty/channel/ChannelFuture;->addListener(Lorg/jboss/netty/channel/ChannelFutureListener;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V

    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V

    move-object v0, v1

    goto/16 :goto_0

    :cond_5
    :try_start_5
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v3}, Ljava/util/zip/GZIPInputStream;->close()V

    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->close()V

    :cond_6
    invoke-static {v5, v0}, Lcom/sec/pns/msg/frontend/MsgFrontendCommon;->newMessageClass(B[B)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p3}, Lorg/jboss/netty/buffer/ChannelBuffer;->resetReaderIndex()V

    const/16 v0, 0x7d4

    const-string v2, "Message parsing failed. Connection will be closed."

    invoke-static {v0, v2}, Lcom/sec/pns/msg/frontend/MsgFrontendCommon;->generateNotiGroupMsgForSystemError(ILjava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup;

    move-result-object v0

    invoke-interface {p2, v0}, Lorg/jboss/netty/channel/Channel;->write(Ljava/lang/Object;)Lorg/jboss/netty/channel/ChannelFuture;

    move-result-object v0

    new-instance v2, Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice$3;

    invoke-direct {v2, p0}, Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice$3;-><init>(Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice;)V

    invoke-interface {v0, v2}, Lorg/jboss/netty/channel/ChannelFuture;->addListener(Lorg/jboss/netty/channel/ChannelFutureListener;)V

    move-object v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    :goto_3
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v3}, Ljava/util/zip/GZIPInputStream;->close()V

    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->close()V

    throw v0

    :catchall_1
    move-exception v0

    move-object v3, v1

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catchall_4
    move-exception v1

    move-object v4, v3

    move-object v3, v2

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v0, v1

    move-object v2, v1

    move-object v3, v1

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v0, v1

    move-object v2, v1

    move-object v3, v4

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v0, v1

    move-object v2, v3

    move-object v3, v4

    goto :goto_2
.end method
