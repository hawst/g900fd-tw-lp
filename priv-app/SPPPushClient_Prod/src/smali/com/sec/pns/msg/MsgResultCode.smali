.class public Lcom/sec/pns/msg/MsgResultCode;
.super Ljava/lang/Object;


# static fields
.field public static final BAD_REQUEST_FOR_PROVISION:I = 0xbb8

.field public static final CANNOT_ACCEPT_LOG_REQUEST:I = 0xfb0

.field public static final CANNOT_PARSE_LOGS:I = 0xfaf

.field public static final CONNECTION_MAX_EXCEEDED:I = 0xfa0

.field public static final DEACTIVATED_SERVICE:I = 0xfb1

.field public static final DEREGISTRATION_FAILED:I = 0xfa8

.field public static final DUPLICATE_DEVICEID_TO_REPROVISION:I = 0xbbc

.field public static final EMPTY_APP_ID:I = 0xfa3

.field public static final EMPTY_DEVICE_LOGS:I = 0xfae

.field public static final EMPTY_DEVICE_TOKEN:I = 0xfa2

.field public static final EMPTY_REG_ID:I = 0xfa4

.field public static final EMPTY_SERVICE:I = 0xfad

.field public static final FAIL_TO_AUTHENTICATE:I = 0xbb9

.field public static final INTERNAL_SERVER_ERROR:I = 0x7d2

.field public static final INTERRUPTED:I = 0x7d3

.field public static final INVALID_DEVICE_TOKEN_TO_REPROVISION:I = 0xbba

.field public static final INVALID_STATE:I = 0xfa1

.field public static final MESSAGE_DECOMPRESSION_FAILED:I = 0x7d5

.field public static final MESSAGE_PARSING_FAILED:I = 0x7d4

.field public static final PROVISION_EXCEPTION:I = 0xbbb

.field public static final REGISTRATION_FAILED:I = 0xfa7

.field public static final REPROVISIONING_REQUIRED:I = 0xfa6

.field public static final REPROVISION_EXCEPTION_SERVER_ID:I = 0xbbd

.field public static final RESET_BY_NEW_INITIALIZATION:I = 0xfa5

.field public static final SUCCESS:I = 0x3e8

.field public static final UNEXPECTED_MESSAGE:I = 0x7d1

.field public static final UNKNOWN_MESSAGE_TYPE:I = 0x7d0

.field public static final UNSUPPORTED_PING_SPECIFICATION:I = 0xfac

.field public static final WRONG_APP_ID:I = 0xfaa

.field public static final WRONG_DEVICE_TOKEN:I = 0xfa9

.field public static final WRONG_REG_ID:I = 0xfab


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
