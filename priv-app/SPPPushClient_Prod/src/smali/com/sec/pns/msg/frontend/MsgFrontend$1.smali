.class final Lcom/sec/pns/msg/frontend/MsgFrontend$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/Descriptors$FileDescriptor$InternalDescriptorAssigner;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public assignDescriptors(Lcom/google/protobuf/Descriptors$FileDescriptor;)Lcom/google/protobuf/ExtensionRegistry;
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->descriptor:Lcom/google/protobuf/Descriptors$FileDescriptor;
    invoke-static {p1}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$26302(Lcom/google/protobuf/Descriptors$FileDescriptor;)Lcom/google/protobuf/Descriptors$FileDescriptor;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_InitRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$002(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_InitRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$000()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "DeviceToken"

    aput-object v3, v2, v6

    const-string v3, "Locale"

    aput-object v3, v2, v7

    const-string v3, "Mcc"

    aput-object v3, v2, v8

    const-string v3, "Latitude"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "Longitude"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "Connectivity"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "UserData"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "Dv"

    aput-object v4, v2, v3

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_InitRequest_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$102(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_InitReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$2302(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_InitReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$2300()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "ResultCode"

    aput-object v3, v2, v6

    const-string v3, "ResultMsg"

    aput-object v3, v2, v7

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_InitReply_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$2402(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_RegistrationRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$3402(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_RegistrationRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$3400()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "DeviceToken"

    aput-object v3, v2, v6

    const-string v3, "AppId"

    aput-object v3, v2, v7

    const-string v3, "UserData"

    aput-object v3, v2, v8

    const-string v3, "MumId"

    aput-object v3, v2, v9

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_RegistrationRequest_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$3502(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_RegistrationReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$4902(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_RegistrationReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$4900()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "ResultCode"

    aput-object v3, v2, v6

    const-string v3, "ResultMsg"

    aput-object v3, v2, v7

    const-string v3, "RegId"

    aput-object v3, v2, v8

    const-string v3, "UserData"

    aput-object v3, v2, v9

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationReply;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationReply$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_RegistrationReply_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$5002(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_DeregistrationRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$6402(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_DeregistrationRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$6400()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "DeviceToken"

    aput-object v3, v2, v6

    const-string v3, "RegId"

    aput-object v3, v2, v7

    const-string v3, "UserData"

    aput-object v3, v2, v8

    const-string v3, "MumId"

    aput-object v3, v2, v9

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_DeregistrationRequest_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$6502(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_DeregistrationReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$7902(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_DeregistrationReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$7900()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "ResultCode"

    aput-object v3, v2, v6

    const-string v3, "ResultMsg"

    aput-object v3, v2, v7

    const-string v3, "UserData"

    aput-object v3, v2, v8

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationReply;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationReply$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_DeregistrationReply_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$8002(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_PingRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$9202(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_PingRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$9200()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "CreatedTime"

    aput-object v3, v2, v6

    const-string v3, "Interval"

    aput-object v3, v2, v7

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_PingRequest_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$9302(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_PingReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$10302(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_PingReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$10300()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "CreatedTime"

    aput-object v3, v2, v6

    const-string v3, "Delta"

    aput-object v3, v2, v7

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$PingReply;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$PingReply$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_PingReply_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$10402(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiElement_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$11402(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiElement_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$11400()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "NotiId"

    aput-object v3, v2, v5

    const-string v3, "AppId"

    aput-object v3, v2, v6

    const-string v3, "ReliableLevel"

    aput-object v3, v2, v7

    const-string v3, "Type"

    aput-object v3, v2, v8

    const-string v3, "Sender"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "Message"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "AppData"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "TimeStamp"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "ConnectionTerm"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "SessionInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "MumId"

    aput-object v4, v2, v3

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiElement_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$11502(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiGroup_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$14102(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiGroup_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$14100()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "NotiElements"

    aput-object v3, v2, v5

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiGroup_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$14202(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiAcks_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$14702(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiAcks_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$14700()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "DeviceToken"

    aput-object v3, v2, v5

    const-string v3, "NotiIds"

    aput-object v3, v2, v6

    const-string v3, "AckType"

    aput-object v3, v2, v7

    const-string v3, "AckInfo"

    aput-object v3, v2, v8

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_NotiAcks_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$14802(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xb

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_ProvisionRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$15902(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_ProvisionRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$15900()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "ClientVersion"

    aput-object v3, v2, v5

    const-string v3, "DeviceId"

    aput-object v3, v2, v6

    const-string v3, "DeviceType"

    aput-object v3, v2, v7

    const-string v3, "DeviceToken"

    aput-object v3, v2, v8

    const-string v3, "DeviceInfo"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "MethodType"

    aput-object v4, v2, v3

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_ProvisionRequest_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$16002(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xc

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_ProvisionReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$17602(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_ProvisionReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$17600()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "ResultCode"

    aput-object v3, v2, v5

    const-string v3, "DeviceToken"

    aput-object v3, v2, v6

    const-string v3, "PrimaryIp"

    aput-object v3, v2, v7

    const-string v3, "PrimaryPort"

    aput-object v3, v2, v8

    const-string v3, "SecondaryIp"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "SecondaryPort"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "PingInterval"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "UserData"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "RegionId"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "RegionDomainName"

    aput-object v4, v2, v3

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_ProvisionReply_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$17702(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$20102(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$20100()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "DeviceToken"

    aput-object v3, v2, v6

    const-string v3, "Service"

    aput-object v3, v2, v7

    const-string v3, "Prefix"

    aput-object v3, v2, v8

    const-string v3, "DeviceLogs"

    aput-object v3, v2, v9

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$LoggingRequest;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$LoggingRequest$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingRequest_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$20202(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xe

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$21502(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$21500()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "ResultCode"

    aput-object v3, v2, v6

    const-string v3, "DeviceToken"

    aput-object v3, v2, v7

    const-string v3, "Service"

    aput-object v3, v2, v8

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$LoggingReply;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$LoggingReply$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingReply_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$21602(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xf

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingConfigRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$22802(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingConfigRequest_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$22800()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "DeviceToken"

    aput-object v3, v2, v6

    const-string v3, "Service"

    aput-object v3, v2, v7

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$LoggingConfigRequest;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$LoggingConfigRequest$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingConfigRequest_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$22902(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x10

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingConfigReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$23802(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingConfigReply_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$23800()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "AsyncId"

    aput-object v3, v2, v5

    const-string v3, "ResultCode"

    aput-object v3, v2, v6

    const-string v3, "DeviceToken"

    aput-object v3, v2, v7

    const-string v3, "ConfigMap"

    aput-object v3, v2, v8

    const-string v3, "CommonConfig"

    aput-object v3, v2, v9

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$LoggingConfigReply;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$LoggingConfigReply$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_LoggingConfigReply_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$23902(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->getDescriptor()Lcom/google/protobuf/Descriptors$FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FileDescriptor;->getMessageTypes()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x11

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$Descriptor;

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_ConfigMap_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$25202(Lcom/google/protobuf/Descriptors$Descriptor;)Lcom/google/protobuf/Descriptors$Descriptor;

    new-instance v0, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    # getter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_ConfigMap_descriptor:Lcom/google/protobuf/Descriptors$Descriptor;
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$25200()Lcom/google/protobuf/Descriptors$Descriptor;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "Service"

    aput-object v3, v2, v5

    const-string v3, "Config"

    aput-object v3, v2, v6

    const-string v3, "ResultCode"

    aput-object v3, v2, v7

    const-class v3, Lcom/sec/pns/msg/frontend/MsgFrontend$ConfigMap;

    const-class v4, Lcom/sec/pns/msg/frontend/MsgFrontend$ConfigMap$Builder;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;-><init>(Lcom/google/protobuf/Descriptors$Descriptor;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    # setter for: Lcom/sec/pns/msg/frontend/MsgFrontend;->internal_static_frontend_ConfigMap_fieldAccessorTable:Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;
    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend;->access$25302(Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;)Lcom/google/protobuf/GeneratedMessage$FieldAccessorTable;

    const/4 v0, 0x0

    return-object v0
.end method
