.class public final Lorg/jboss/netty/handler/codec/spdy/SpdyHttpHeaders$Names;
.super Ljava/lang/Object;


# static fields
.field public static final ASSOCIATED_TO_STREAM_ID:Ljava/lang/String; = "X-SPDY-Associated-To-Stream-ID"

.field public static final PRIORITY:Ljava/lang/String; = "X-SPDY-Priority"

.field public static final SCHEME:Ljava/lang/String; = "X-SPDY-Scheme"

.field public static final STREAM_ID:Ljava/lang/String; = "X-SPDY-Stream-ID"

.field public static final URL:Ljava/lang/String; = "X-SPDY-URL"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
