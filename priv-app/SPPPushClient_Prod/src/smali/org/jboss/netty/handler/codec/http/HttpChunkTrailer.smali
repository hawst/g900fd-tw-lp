.class public interface abstract Lorg/jboss/netty/handler/codec/http/HttpChunkTrailer;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/jboss/netty/handler/codec/http/HttpChunk;


# virtual methods
.method public abstract addHeader(Ljava/lang/String;Ljava/lang/Object;)V
.end method

.method public abstract clearHeaders()V
.end method

.method public abstract containsHeader(Ljava/lang/String;)Z
.end method

.method public abstract getHeader(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getHeaderNames()Ljava/util/Set;
.end method

.method public abstract getHeaders()Ljava/util/List;
.end method

.method public abstract getHeaders(Ljava/lang/String;)Ljava/util/List;
.end method

.method public abstract isLast()Z
.end method

.method public abstract removeHeader(Ljava/lang/String;)V
.end method

.method public abstract setHeader(Ljava/lang/String;Ljava/lang/Iterable;)V
.end method

.method public abstract setHeader(Ljava/lang/String;Ljava/lang/Object;)V
.end method
