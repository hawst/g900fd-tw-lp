.class interface abstract Lorg/jboss/netty/channel/socket/nio/SocketSendBufferPool$SendBuffer;
.super Ljava/lang/Object;


# virtual methods
.method public abstract finished()Z
.end method

.method public abstract release()V
.end method

.method public abstract totalBytes()J
.end method

.method public abstract transferTo(Ljava/nio/channels/DatagramChannel;Ljava/net/SocketAddress;)J
.end method

.method public abstract transferTo(Ljava/nio/channels/WritableByteChannel;)J
.end method

.method public abstract writtenBytes()J
.end method
