.class public interface abstract Lorg/jboss/netty/channel/FileRegion;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/jboss/netty/util/ExternalResourceReleasable;


# virtual methods
.method public abstract getCount()J
.end method

.method public abstract getPosition()J
.end method

.method public abstract transferTo(Ljava/nio/channels/WritableByteChannel;J)J
.end method
