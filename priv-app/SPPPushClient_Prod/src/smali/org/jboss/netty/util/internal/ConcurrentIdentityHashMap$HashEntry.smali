.class final Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;
.super Ljava/lang/Object;


# instance fields
.field final hash:I

.field final key:Ljava/lang/Object;

.field final next:Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;

.field volatile value:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;ILorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;->hash:I

    iput-object p3, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;->next:Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;

    iput-object p1, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;->key:Ljava/lang/Object;

    iput-object p4, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;->value:Ljava/lang/Object;

    return-void
.end method

.method static newArray(I)[Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;
    .locals 1

    new-array v0, p0, [Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;

    return-object v0
.end method


# virtual methods
.method key()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;->key:Ljava/lang/Object;

    return-object v0
.end method

.method setValue(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;->value:Ljava/lang/Object;

    return-void
.end method

.method value()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/jboss/netty/util/internal/ConcurrentIdentityHashMap$HashEntry;->value:Ljava/lang/Object;

    return-object v0
.end method
