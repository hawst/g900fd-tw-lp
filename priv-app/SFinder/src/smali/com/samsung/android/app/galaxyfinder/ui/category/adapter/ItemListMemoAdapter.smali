.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListMemoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 21
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 25
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v3

    .line 26
    .local v3, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v2, 0x0

    .line 28
    .local v2, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;
    invoke-virtual {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 30
    if-nez p2, :cond_1

    .line 31
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f03003b

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 33
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;

    .end local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;
    invoke-direct {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;-><init>()V

    .line 35
    .restart local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;
    const v6, 0x7f0b0070

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 36
    const v6, 0x7f0b006c

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    .line 37
    const v6, 0x7f0b006e

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    .line 38
    const v6, 0x7f0b004d

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    .line 40
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 45
    :goto_0
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v5

    .line 46
    .local v5, "title":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "desc":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v4

    .line 49
    .local v4, "thumb":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v1

    .line 51
    .local v1, "genericIntent":Landroid/content/Intent;
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_2

    .line 52
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 53
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {p0, v6, v5, v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 57
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    .line 58
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    invoke-virtual {p0, v6, v0, v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 64
    :goto_2
    if-eqz v4, :cond_0

    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    if-eqz v6, :cond_0

    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-nez v6, :cond_0

    .line 65
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 66
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    invoke-virtual {p0, v6, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 69
    :cond_0
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    if-eqz v6, :cond_4

    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    :goto_3
    invoke-virtual {p0, v6, v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 72
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    return-object v6

    .line 42
    .end local v0    # "desc":Ljava/lang/String;
    .end local v1    # "genericIntent":Landroid/content/Intent;
    .end local v4    # "thumb":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;
    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;

    .restart local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;
    goto :goto_0

    .line 55
    .restart local v0    # "desc":Ljava/lang/String;
    .restart local v1    # "genericIntent":Landroid/content/Intent;
    .restart local v4    # "thumb":Ljava/lang/String;
    .restart local v5    # "title":Ljava/lang/String;
    :cond_2
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 61
    :cond_3
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMemoAdapter$MemoItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move-object v6, p2

    .line 69
    goto :goto_3
.end method
