.class public final Lcom/samsung/android/app/galaxyfinder/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final anim_duration_search_scroll_view_expanding:I = 0x7f0c0000

.field public static final anim_duration_search_scroll_view_folding:I = 0x7f0c0001

.field public static final anim_duration_search_scroll_view_folding_when_head_up:I = 0x7f0c0002

.field public static final anim_duration_search_scroll_view_head_up:I = 0x7f0c0003

.field public static final anim_search_view_disappearing_height:I = 0x7f0c002b

.field public static final autocomplete_dropdownlist_threshold:I = 0x7f0c0004

.field public static final card_application_frame_column_count:I = 0x7f0c0005

.field public static final card_browser_frame_column_count:I = 0x7f0c0006

.field public static final card_chaton_frame_column_count:I = 0x7f0c0007

.field public static final card_contact_frame_column_count:I = 0x7f0c0008

.field public static final card_default_frame_column_count:I = 0x7f0c0009

.field public static final card_email_frame_column_count:I = 0x7f0c000a

.field public static final card_gallery_expand_count:I = 0x7f0c000b

.field public static final card_gallery_first_row_item_count:I = 0x7f0c002c

.field public static final card_gallery_frame_column_count:I = 0x7f0c000c

.field public static final card_lifetime_frame_column_count:I = 0x7f0c000d

.field public static final card_memo_frame_column_count:I = 0x7f0c000e

.field public static final card_message_frame_column_count:I = 0x7f0c000f

.field public static final card_music_frame_column_count:I = 0x7f0c0010

.field public static final card_myfiles_frame_column_count:I = 0x7f0c0011

.field public static final card_note_frame_column_count:I = 0x7f0c0012

.field public static final card_note_title_text_bg_alpha:I = 0x7f0c0013

.field public static final card_phone_frame_column_count:I = 0x7f0c0014

.field public static final card_planner_frame_column_count:I = 0x7f0c0015

.field public static final card_quickmemo_frame_column_count:I = 0x7f0c0016

.field public static final card_samsunglink_frame_column_count:I = 0x7f0c0017

.field public static final card_scrapbook_frame_column_count:I = 0x7f0c0018

.field public static final card_settings_frame_column_count:I = 0x7f0c0019

.field public static final card_storyalbum_frame_column_count:I = 0x7f0c001a

.field public static final card_video_frame_column_count:I = 0x7f0c001b

.field public static final card_vnote_frame_column_count:I = 0x7f0c001c

.field public static final card_weblink_preview_frame_column_count:I = 0x7f0c001d

.field public static final floating_settings_height:I = 0x7f0c001e

.field public static final floating_settings_width:I = 0x7f0c001f

.field public static final grid_type_max_item_count:I = 0x7f0c0020

.field public static final history_keyword_item_textsize_dp:I = 0x7f0c002d

.field public static final history_list_item_date_text_font_style:I = 0x7f0c0021

.field public static final history_list_item_keyword_text_font_style:I = 0x7f0c0022

.field public static final history_list_item_max_count:I = 0x7f0c0023

.field public static final image_outline_paint_width:I = 0x7f0c0024

.field public static final image_resize_default_height:I = 0x7f0c0025

.field public static final image_resize_default_width:I = 0x7f0c0026

.field public static final list_type_max_item_count:I = 0x7f0c0027

.field public static final max_keyword_query_length:I = 0x7f0c0028

.field public static final recent_tag_keyword_item_textsize_dp:I = 0x7f0c002e

.field public static final search_scroll_view_expand_top_offset:I = 0x7f0c0029

.field public static final search_scroll_view_top_point:I = 0x7f0c002f

.field public static final tag_cloud_item_text_size:I = 0x7f0c0030

.field public static final voice_input_popup_title_shadow_dy:I = 0x7f0c002a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
