.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->onMultiSelectionChanged(IIIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;

.field final synthetic val$b:I

.field final synthetic val$l:I

.field final synthetic val$offset:I

.field final synthetic val$position:I

.field final synthetic val$r:I

.field final synthetic val$t:I


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;IIIIII)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;

    iput p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$position:I

    iput p3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$l:I

    iput p4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$t:I

    iput p5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$r:I

    iput p6, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$b:I

    iput p7, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$offset:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 265
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$position:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 269
    .local v0, "view":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 270
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$l:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$t:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$r:I

    iget v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$b:I

    iget v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;->val$offset:I

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->notifyMultiSelection(IIIII)V

    .line 273
    .end local v0    # "view":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    :cond_0
    return-void
.end method
