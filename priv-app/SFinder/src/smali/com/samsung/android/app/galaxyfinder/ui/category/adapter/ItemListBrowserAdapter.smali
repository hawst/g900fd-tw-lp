.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListBrowserAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 24
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v5

    .line 29
    .local v5, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v2, 0x0

    .line 31
    .local v2, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;
    move-object/from16 v0, p3

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 33
    if-nez p2, :cond_4

    .line 34
    sget-object v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f030032

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 36
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;

    .end local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;
    invoke-direct {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;-><init>()V

    .line 37
    .restart local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;
    const v10, 0x7f0b004f

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 38
    const v10, 0x7f0b0050

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->tvUrl:Landroid/widget/TextView;

    .line 39
    const v10, 0x7f0b004e

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 40
    const v10, 0x7f0b004d

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    iput-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    .line 41
    const v10, 0x7f0b0045

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    iput-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    .line 43
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 48
    :goto_0
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v3

    .line 49
    .local v3, "iconUri":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIconBlob()[B

    move-result-object v1

    .line 51
    .local v1, "favicon":[B
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v4

    .line 53
    .local v4, "intent":Landroid/content/Intent;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v8

    .line 54
    .local v8, "title":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v9

    .line 55
    .local v9, "url":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v6

    .line 57
    .local v6, "sortKeyValue":Ljava/lang/String;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    .line 58
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v11, 0x1

    invoke-virtual {p0, v10, v8, v11}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 62
    :cond_0
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    .line 63
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->tvUrl:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 64
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->tvUrl:Landroid/widget/TextView;

    const/4 v11, 0x1

    invoke-virtual {p0, v10, v9, v11}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 67
    :cond_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 68
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    invoke-virtual {p0, v10, p1, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;->setSubHeaderView(Landroid/view/ViewGroup;ILjava/lang/String;)V

    .line 71
    :cond_2
    if-eqz v1, :cond_5

    array-length v10, v1

    if-lez v10, :cond_5

    .line 72
    const/4 v10, 0x0

    array-length v11, v1

    invoke-static {v1, v10, v11}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 74
    .local v7, "thumbnail":Landroid/graphics/Bitmap;
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 75
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    invoke-virtual {v10, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 76
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 82
    .end local v7    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_3
    :goto_1
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    invoke-virtual {p0, v10, v4, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 84
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    return-object v10

    .line 45
    .end local v1    # "favicon":[B
    .end local v3    # "iconUri":Ljava/lang/String;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v6    # "sortKeyValue":Ljava/lang/String;
    .end local v8    # "title":Ljava/lang/String;
    .end local v9    # "url":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;
    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;

    .restart local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;
    goto :goto_0

    .line 77
    .restart local v1    # "favicon":[B
    .restart local v3    # "iconUri":Ljava/lang/String;
    .restart local v4    # "intent":Landroid/content/Intent;
    .restart local v6    # "sortKeyValue":Ljava/lang/String;
    .restart local v8    # "title":Ljava/lang/String;
    .restart local v9    # "url":Ljava/lang/String;
    :cond_5
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_3

    .line 78
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/4 v11, 0x0

    const/4 v12, -0x1

    invoke-virtual {p0, v10, v3, v11, v12}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 79
    iget-object v10, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
