.class public Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
.super Landroid/app/Service;
.source "LocationTagReadyService.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/tag/TagConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;,
        Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;,
        Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    }
.end annotation


# static fields
.field private static final ADDITIONAL_AUDIO_QUERY_CONDITION:Ljava/lang/String; = " and (latitude != 0 and longitude != 0)"

.field private static final ADDRESS:Ljava/lang/String; = "addr"

.field private static final ALREADY_HAVE_GEOCODE:I = 0x10000000

.field private static final FULL_QUERY_CONDITION:Ljava/lang/String; = "latitude is not null and longitude is not null and addr is null"

.field private static final GEOTAG:Ljava/lang/String; = "GeoTag"

.field private static final ID:Ljava/lang/String; = "_id"

.field private static final LANGCODE:Ljava/lang/String; = "langagecode"

.field private static final LATITUDE:Ljava/lang/String; = "latitude"

.field private static final LOCATION_QUERY_CONDITION:Ljava/lang/String; = "latitude is not null and longitude is not null and (addr is not null and addr is not \'INVALID_GPS\') and (latitude between ? and ?) and (longitude between ? and ?)"

.field private static final LONGITUDE:Ljava/lang/String; = "longitude"

.field private static final MEDIA_PROVIDER_FILE_URI:Landroid/net/Uri;

.field private static final NO_EXIST_CONTENTS:I = -0x1

.field private static final REQUEST_FETCH_DELAY:I = 0x1

.field private static final RESPONSE_FETCH_DELAY:I = 0x2

.field private static final SINGLE_QUERY_CONDITION:Ljava/lang/String; = "_id > ? and latitude is not null and longitude is not null and addr is null"

.field private static final TAG:Ljava/lang/String;

.field private static final contentUris:[Landroid/net/Uri;

.field private static final projection:[Ljava/lang/String;


# instance fields
.field private final LAST_UPDATED_ID:Ljava/lang/String;

.field private final MIN_FETCH_DELAYTIME:J

.field private bWaitingDelayedMessage:Z

.field private mConvertTask:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

.field private volatile mHandler:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

.field private volatile mHandlerThread:Landroid/os/HandlerThread;

.field private mLogPrefs:Landroid/content/SharedPreferences;

.field private mMediaStoreObserver:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Landroid/database/ContentObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    .line 72
    new-array v0, v5, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->contentUris:[Landroid/net/Uri;

    .line 77
    const-string v0, "content://media/external/file"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->MEDIA_PROVIDER_FILE_URI:Landroid/net/Uri;

    .line 112
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "latitude"

    aput-object v1, v0, v3

    const-string v1, "longitude"

    aput-object v1, v0, v4

    const-string v1, "addr"

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->projection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->bWaitingDelayedMessage:Z

    .line 62
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->MIN_FETCH_DELAYTIME:J

    .line 79
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->contentUris:[Landroid/net/Uri;

    array-length v0, v0

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mConvertTask:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mLogPrefs:Landroid/content/SharedPreferences;

    .line 118
    const-string v0, "last_updated_id"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->LAST_UPDATED_ID:Ljava/lang/String;

    .line 530
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->bWaitingDelayedMessage:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "x2"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .param p3, "x3"    # Landroid/net/Uri;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->convertAddress(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->setPrefsUpdateId(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # [Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getData(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/net/Uri;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Landroid/database/Cursor;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->convert(Landroid/net/Uri;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # Ljava/util/ArrayList;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->updateResult(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->MEDIA_PROVIDER_FILE_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # Ljava/lang/Object;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->sendResponse(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/net/Uri;)Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getMatchedUriType(Landroid/net/Uri;)Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/net/Uri;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getNewContentsID(Landroid/net/Uri;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Ljava/lang/String;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getPrefsUpdatedId(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->sendDirectResponse(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;I)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .param p2, "x2"    # Landroid/net/Uri;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->prepareAction(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V

    return-void
.end method

.method private convert(Landroid/net/Uri;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 17
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 959
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 961
    .local v13, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;>;"
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->projection:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v10, v4, v5

    .line 962
    .local v10, "_id":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->projection:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v12, v4, v5

    .line 963
    .local v12, "latitude":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->projection:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v15, v4, v5

    .line 964
    .local v15, "longitude":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->projection:[Ljava/lang/String;

    const/4 v5, 0x3

    aget-object v16, v4, v5

    .line 966
    .local v16, "prevAddr":Ljava/lang/String;
    const/4 v3, 0x0

    .line 967
    .local v3, "id":I
    const/4 v11, 0x0

    .local v11, "lati":Ljava/lang/Double;
    const/4 v14, 0x0

    .line 968
    .local v14, "longi":Ljava/lang/Double;
    const/4 v9, 0x0

    .local v9, "prevAddress":Ljava/lang/String;
    const/4 v8, 0x0

    .line 969
    .local v8, "targeturi":Ljava/lang/String;
    :goto_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 970
    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 971
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    .line 972
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    .line 973
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 974
    int-to-long v4, v3

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 976
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;

    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v14}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct/range {v2 .. v9}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;-><init>(IDDLjava/lang/String;Ljava/lang/String;)V

    .line 977
    .local v2, "data":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;
    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 979
    .end local v2    # "data":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;
    :cond_0
    return-object v13
.end method

.method private convertAddress(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V
    .locals 11
    .param p1, "requestType"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "type"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x0

    .line 460
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "convertAddress() requestType = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", type = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", uri = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->INVALIDTYPE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    if-ne p2, v7, :cond_1

    .line 464
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "convertAddress() type is wrong!!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p3, :cond_0

    .end local p3    # "uri":Landroid/net/Uri;
    :goto_0
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    :goto_1
    return-void

    .line 464
    .restart local p3    # "uri":Landroid/net/Uri;
    :cond_0
    const-string p3, "invalid uri"

    goto :goto_0

    .line 468
    :cond_1
    if-nez p3, :cond_2

    .line 469
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v8, "convertAddress() uri is NULL"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 474
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mConvertTask:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    invoke-virtual {p2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->ordinal()I

    move-result v8

    aget-object v2, v7, v8

    .line 475
    .local v2, "convertTask":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 476
    :cond_3
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    .end local v2    # "convertTask":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandler:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

    invoke-direct {v2, p0, p2, v7}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/os/Handler;)V

    .line 477
    .restart local v2    # "convertTask":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;
    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {v2, v7}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 478
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "convertAddress() type : don\'t be here!! type = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    :cond_4
    const/4 v6, 0x0

    .line 482
    .local v6, "selectionArg":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 483
    .local v3, "selection":Ljava/lang/String;
    move-object v1, p3

    .line 485
    .local v1, "contentUri":Landroid/net/Uri;
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v7

    sget-object v8, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v8

    if-le v7, v8, :cond_6

    .line 486
    const-string v3, "latitude is not null and longitude is not null and addr is null"

    .line 495
    :goto_2
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->AUDIO:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    invoke-virtual {v7, p2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 496
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and (latitude != 0 and longitude != 0)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 499
    :cond_5
    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->requestConvertAddress(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z
    invoke-static {v2, p1, v1, v3, v6}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->access$900(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    .line 501
    .local v0, "bResult":Z
    if-nez v0, :cond_7

    .line 502
    const/4 v7, 0x0

    invoke-direct {p0, p1, v1, v7}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->sendResponse(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/Object;)V

    goto :goto_1

    .line 488
    .end local v0    # "bResult":Z
    :cond_6
    invoke-virtual {p2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->name()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getPrefsUpdatedId(Ljava/lang/String;)J

    move-result-wide v4

    .line 489
    .local v4, "id":J
    const-string v3, "_id > ? and latitude is not null and longitude is not null and addr is null"

    .line 490
    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    .end local v6    # "selectionArg":[Ljava/lang/String;
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    .restart local v6    # "selectionArg":[Ljava/lang/String;
    goto :goto_2

    .line 504
    .end local v4    # "id":J
    .restart local v0    # "bResult":Z
    :cond_7
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v8, "convertAddress() requestConvertAddress() called successfully!!!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private getData(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArg"    # [Ljava/lang/String;

    .prologue
    .line 939
    const/4 v6, 0x0

    .line 941
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->projection:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 948
    :goto_0
    return-object v6

    .line 943
    :catch_0
    move-exception v8

    .line 944
    .local v8, "se":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 945
    .end local v8    # "se":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v7

    .line 946
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getLogPrefs(Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mLogPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 1146
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mLogPrefs:Landroid/content/SharedPreferences;

    .line 1148
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mLogPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private getMatchedUriType(Landroid/net/Uri;)Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .locals 2
    .param p1, "checkUri"    # Landroid/net/Uri;

    .prologue
    .line 380
    if-eqz p1, :cond_1

    .line 381
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->contentUris:[Landroid/net/Uri;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 382
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->contentUris:[Landroid/net/Uri;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 383
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    move-result-object v1

    aget-object v1, v1, v0

    .line 386
    .end local v0    # "i":I
    :goto_1
    return-object v1

    .line 381
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 386
    .end local v0    # "i":I
    :cond_1
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->INVALIDTYPE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    goto :goto_1
.end method

.method private getNewContentsID(Landroid/net/Uri;)J
    .locals 14
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 390
    const-wide/16 v12, -0x1

    .line 392
    .local v12, "rowID":J
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "max(_id)"

    aput-object v4, v2, v1

    const/4 v1, 0x1

    const-string v4, "_data"

    aput-object v4, v2, v1

    const/4 v1, 0x2

    const-string v4, "_display_name"

    aput-object v4, v2, v1

    const/4 v1, 0x3

    const-string v4, "max(date_added)"

    aput-object v4, v2, v1

    const/4 v1, 0x4

    const-string v4, "latitude"

    aput-object v4, v2, v1

    const/4 v1, 0x5

    const-string v4, "addr"

    aput-object v4, v2, v1

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 396
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 397
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v1, "LocationTagReadyService:getNewContentsID() : cursor is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v12

    .line 435
    :goto_0
    return-wide v0

    .line 401
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 402
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v1, "LocationTagReadyService:getNewContentsID() : Not exist data cursor"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-wide v0, v12

    .line 404
    goto :goto_0

    .line 407
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 408
    const-string v0, "max(_id)"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v12, v0

    .line 409
    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 410
    .local v8, "data":Ljava/lang/String;
    const-string v0, "_display_name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 411
    .local v10, "displayName":Ljava/lang/String;
    const-string v0, "max(date_added)"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 412
    .local v9, "date":Ljava/lang/String;
    const-string v0, "latitude"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 413
    .local v11, "latitude":Ljava/lang/String;
    const-string v0, "addr"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 415
    .local v6, "addr":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 416
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LocationTagReadyService:getNewContentsID() : ID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", date_added(latest time) = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", latitude = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", addr = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LocationTagReadyService:getNewContentsID() : data = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", displayName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :cond_2
    if-nez v9, :cond_3

    if-nez v8, :cond_3

    .line 424
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 425
    const-wide/16 v0, -0x1

    goto/16 :goto_0

    .line 428
    :cond_3
    if-eqz v6, :cond_4

    .line 429
    const-wide/32 v0, 0x10000000

    or-long/2addr v12, v0

    .line 433
    .end local v6    # "addr":Ljava/lang/String;
    .end local v8    # "data":Ljava/lang/String;
    .end local v9    # "date":Ljava/lang/String;
    .end local v10    # "displayName":Ljava/lang/String;
    .end local v11    # "latitude":Ljava/lang/String;
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-wide v0, v12

    .line 435
    goto/16 :goto_0
.end method

.method private getPrefsUpdatedId(Ljava/lang/String;)J
    .locals 6
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1152
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getLogPrefs(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "last_updated_id"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1153
    .local v0, "lastUpdatedId":J
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1154
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPrefsUpdatedId() lastUpdatedId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    :cond_0
    return-wide v0
.end method

.method private getServiceStateString(I)Ljava/lang/String;
    .locals 3
    .param p1, "tagState"    # I

    .prologue
    .line 245
    const/4 v0, 0x0

    .line 246
    .local v0, "state":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 260
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WRONG service state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 262
    :goto_0
    return-object v0

    .line 248
    :pswitch_0
    const-string v0, "TAG_SERVICE_BOOTUP"

    .line 249
    goto :goto_0

    .line 251
    :pswitch_1
    const-string v0, "TAG_SERVICE_SDCARD_MOUNTED"

    .line 252
    goto :goto_0

    .line 254
    :pswitch_2
    const-string v0, "TAG_SERVICE_SDCARD_UNMOUNTED"

    .line 255
    goto :goto_0

    .line 257
    :pswitch_3
    const-string v0, "TAG_SERVICE_MEDIA_SCANNER_FINISHED"

    .line 258
    goto :goto_0

    .line 246
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private init()V
    .locals 8

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->registerContentObservers()V

    .line 184
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 185
    .local v3, "type":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->INVALIDTYPE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    if-eq v3, v4, :cond_1

    .line 186
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mConvertTask:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->ordinal()I

    move-result v5

    aget-object v4, v4, v5

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mConvertTask:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->ordinal()I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 188
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mConvertTask:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->ordinal()I

    move-result v5

    new-instance v6, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandler:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

    invoke-direct {v6, p0, v3, v7}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/os/Handler;)V

    aput-object v6, v4, v5

    .line 189
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mConvertTask:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->ordinal()I

    move-result v5

    aget-object v4, v4, v5

    sget-object v5, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 190
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "QueryAsyncTask excuted (type) : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 194
    .end local v3    # "type":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    :cond_2
    return-void
.end method

.method private prepareAction(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V
    .locals 5
    .param p1, "type"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x1

    .line 439
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->bWaitingDelayedMessage:Z

    if-eqz v1, :cond_0

    .line 440
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandler:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->ordinal()I

    move-result v2

    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v3

    invoke-virtual {v1, v4, v2, v3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 442
    .local v0, "msg":Landroid/os/Message;
    if-eqz v0, :cond_0

    .line 443
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandler:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v4, v2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;->removeMessages(ILjava/lang/Object;)V

    .line 447
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 448
    .restart local v0    # "msg":Landroid/os/Message;
    iput v4, v0, Landroid/os/Message;->what:I

    .line 449
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->ordinal()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 450
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 451
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 452
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandler:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 453
    iput-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->bWaitingDelayedMessage:Z

    .line 454
    return-void
.end method

.method private registerContentObservers()V
    .locals 8

    .prologue
    .line 274
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v6, "registerContentObservers()"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->contentUris:[Landroid/net/Uri;

    .local v0, "arr$":[Landroid/net/Uri;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v4, v0, v1

    .line 277
    .local v4, "uri":Landroid/net/Uri;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 278
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "registerContentObservers "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mMediaStoreObserver already exists"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 282
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->useSLinkLocationService()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 283
    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    sget-object v5, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 285
    :cond_1
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SLink location service is enable. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " not register"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 290
    :cond_2
    new-instance v3, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v3, p0, v5}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/os/Handler;)V

    .line 345
    .local v3, "mediaObserver":Landroid/database/ContentObserver;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    invoke-virtual {v5, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "registerContentObservers() uri = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mMediaStoreObserver registered"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_1

    .line 349
    .end local v3    # "mediaObserver":Landroid/database/ContentObserver;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_3
    return-void
.end method

.method private sendDirectResponse(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;I)V
    .locals 6
    .param p1, "type"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "id"    # I

    .prologue
    .line 509
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 510
    .local v0, "updatedIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v4, v1

    invoke-direct {p0, v2, v4, v5}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->setPrefsUpdateId(Ljava/lang/String;J)V

    .line 513
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-direct {p0, v1, p2, v0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->sendResponse(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/Object;)V

    .line 514
    return-void
.end method

.method private sendResponse(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/Object;)V
    .locals 5
    .param p1, "reqType"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "notifyUri"    # Landroid/net/Uri;
    .param p3, "resultList"    # Ljava/lang/Object;

    .prologue
    .line 518
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.app.galaxyfinder.tag.media_location_update_action"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 519
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 520
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "media_convert_type"

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 521
    const-string v2, "media_provider_uri"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    if-eqz p3, :cond_0

    .line 523
    const-string v2, "media_ids"

    check-cast p3, Ljava/util/ArrayList;

    .end local p3    # "resultList":Ljava/lang/Object;
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 525
    :cond_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 526
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->sendBroadcast(Landroid/content/Intent;)V

    .line 527
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendBrodadcast() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reqType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    return-void
.end method

.method private setPrefsUpdateId(Ljava/lang/String;J)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "id"    # J

    .prologue
    .line 1160
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getLogPrefs(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1161
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "last_updated_id"

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1162
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1163
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPrefsUpdateId() key = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", current MAX id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1166
    return-void
.end method

.method private unRegisterContentObservers()V
    .locals 5

    .prologue
    .line 352
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v4, "unregisterContentObservers()"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 355
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/ContentObserver;

    .line 356
    .local v1, "mediaObserver":Landroid/database/ContentObserver;
    if-eqz v1, :cond_0

    .line 357
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 361
    .end local v1    # "mediaObserver":Landroid/database/ContentObserver;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 362
    return-void
.end method

.method private unRegisterObserversBySLink()V
    .locals 8

    .prologue
    .line 365
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->contentUris:[Landroid/net/Uri;

    .local v0, "arr$":[Landroid/net/Uri;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v4, v0, v1

    .line 366
    .local v4, "uri":Landroid/net/Uri;
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unRegisterObserversBySLink() "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", observer = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 369
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/database/ContentObserver;

    .line 370
    .local v3, "mediaObserver":Landroid/database/ContentObserver;
    if-eqz v3, :cond_1

    .line 371
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 372
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SLink location service is enable. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " UNregister"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mMediaStoreObserver:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    .end local v3    # "mediaObserver":Landroid/database/ContentObserver;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 377
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_2
    return-void
.end method

.method private updateAddress(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "addr"    # Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;

    .prologue
    .line 1118
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1120
    .local v0, "langCode":Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1121
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "addr"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    const-string v3, "langagecode"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1124
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id in ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 1126
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1127
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateAddress() uri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", addr = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", selection = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1132
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v2, p3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1136
    :goto_0
    return-void

    .line 1133
    :catch_0
    move-exception v1

    .line 1134
    .local v1, "se":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateResult(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 31
    .param p1, "requestType"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 995
    .local p3, "lists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;>;"
    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    .line 996
    :cond_0
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v29, "updateResult() : contentUriStr or lists is null"

    invoke-static/range {v28 .. v29}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    const/16 v21, 0x0

    .line 1114
    :cond_1
    return-object v21

    .line 1000
    :cond_2
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 1001
    .local v21, "sortedIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz p3, :cond_1

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v28

    if-lez v28, :cond_1

    .line 1002
    new-instance v27, Ljava/util/HashMap;

    invoke-direct/range {v27 .. v27}, Ljava/util/HashMap;-><init>()V

    .line 1003
    .local v27, "updatedAddrMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 1005
    .local v18, "prevAddr2CurrentAddr":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;

    .line 1007
    .local v15, "locData":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;
    if-eqz v15, :cond_3

    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v28

    if-eqz v28, :cond_3

    .line 1008
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v28

    if-eqz v28, :cond_4

    .line 1009
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "updateResult() addr = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", id = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->get_id()I

    move-result v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", targetUri = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getTargeturi()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    :cond_4
    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 1013
    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/ArrayList;

    .line 1014
    .local v13, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->get_id()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1021
    :goto_0
    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getPrevAddress()Ljava/lang/String;

    move-result-object v19

    .line 1022
    .local v19, "prevAddress":Ljava/lang/String;
    if-eqz v19, :cond_3

    .line 1023
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "updateResult() Location Update : from = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " to = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    const-string v28, "\\|"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 1027
    .local v20, "prevValues":[Ljava/lang/String;
    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v28

    const-string v29, "\\|"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1029
    .local v6, "currentValues":[Ljava/lang/String;
    if-eqz v20, :cond_3

    if-eqz v6, :cond_3

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v28, v0

    array-length v0, v6

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_3

    .line 1031
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v9, v0, :cond_3

    .line 1032
    const-string v28, " "

    aget-object v29, v20, v9

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-nez v28, :cond_5

    const-string v28, " "

    aget-object v29, v6, v9

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 1031
    :cond_5
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1016
    .end local v6    # "currentValues":[Ljava/lang/String;
    .end local v9    # "i":I
    .end local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v19    # "prevAddress":Ljava/lang/String;
    .end local v20    # "prevValues":[Ljava/lang/String;
    :cond_6
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1017
    .restart local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->get_id()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1018
    invoke-virtual {v15}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1035
    .restart local v6    # "currentValues":[Ljava/lang/String;
    .restart local v9    # "i":I
    .restart local v19    # "prevAddress":Ljava/lang/String;
    .restart local v20    # "prevValues":[Ljava/lang/String;
    :cond_7
    aget-object v28, v20, v9

    aget-object v29, v6, v9

    move-object/from16 v0, v18

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1036
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "updateResult() Location Update : Prev = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    aget-object v30, v20, v9

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " Current = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    aget-object v30, v6, v9

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1044
    .end local v6    # "currentValues":[Ljava/lang/String;
    .end local v9    # "i":I
    .end local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v15    # "locData":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;
    .end local v19    # "prevAddress":Ljava/lang/String;
    .end local v20    # "prevValues":[Ljava/lang/String;
    :cond_8
    invoke-virtual/range {v27 .. v27}, Ljava/util/HashMap;->isEmpty()Z

    move-result v28

    if-nez v28, :cond_1

    .line 1045
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1046
    .local v4, "builder":Ljava/lang/StringBuilder;
    new-instance v24, Ljava/util/TreeSet;

    invoke-direct/range {v24 .. v24}, Ljava/util/TreeSet;-><init>()V

    .line 1048
    .local v24, "treeIds":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Integer;>;"
    invoke-virtual/range {v27 .. v27}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_9
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_c

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1049
    .local v3, "addr":Ljava/lang/String;
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "updateResult() addr : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/ArrayList;

    .line 1051
    .restart local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v13, :cond_9

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v28

    if-lez v28, :cond_9

    .line 1052
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 1054
    .local v12, "idItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/16 v16, 0x32

    .line 1055
    .local v16, "onceUpdateUnit":I
    const/4 v14, 0x0

    .line 1056
    .local v14, "idsNo":I
    :cond_a
    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_b

    .line 1057
    add-int/lit8 v14, v14, 0x1

    .line 1059
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 1060
    .local v11, "id":I
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 1061
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const/16 v29, 0x2c

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1063
    rem-int v28, v14, v16

    if-nez v28, :cond_a

    .line 1064
    const/16 v28, 0x0

    const-string v29, ","

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->updateAddress(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_4

    .line 1070
    .end local v11    # "id":I
    :cond_b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v28

    if-eqz v28, :cond_9

    .line 1071
    const/16 v28, 0x0

    const-string v29, ","

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->updateAddress(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 1073
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    goto/16 :goto_3

    .line 1078
    .end local v3    # "addr":Ljava/lang/String;
    .end local v12    # "idItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v14    # "idsNo":I
    .end local v16    # "onceUpdateUnit":I
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v28

    sget-object v29, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_FULLSYNC_BOOT_COMPLETED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_e

    .line 1079
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->isEmpty()Z

    move-result v28

    if-nez v28, :cond_e

    .line 1080
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 1081
    .local v23, "tagDboperationlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "type="

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    sget-object v29, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " and "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "data"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "= ?"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 1083
    .local v26, "updateSelection":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_d

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 1084
    .local v8, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 1085
    .local v17, "prevAddr":Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1086
    .local v5, "currentAddr":Ljava/lang/String;
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v28 .. v28}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v22

    .line 1088
    .local v22, "sqlbuilder":Landroid/content/ContentProviderOperation$Builder;
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    aput-object v17, v28, v29

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "rawdata"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1092
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v28

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1095
    .end local v5    # "currentAddr":Ljava/lang/String;
    .end local v8    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v17    # "prevAddr":Ljava/lang/String;
    .end local v22    # "sqlbuilder":Landroid/content/ContentProviderOperation$Builder;
    :cond_d
    :try_start_0
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v28

    if-lez v28, :cond_e

    .line 1096
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v28

    const-string v29, "com.samsung.android.app.galaxyfinder.tag"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1108
    .end local v23    # "tagDboperationlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v26    # "updateSelection":Ljava/lang/String;
    :cond_e
    :goto_6
    invoke-virtual/range {v24 .. v24}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .line 1109
    .local v25, "treeItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_7
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_1

    .line 1110
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/Integer;

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 1099
    .end local v25    # "treeItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v23    # "tagDboperationlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v26    # "updateSelection":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 1101
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_6

    .line 1102
    .end local v7    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v7

    .line 1104
    .local v7, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v7}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_6
.end method


# virtual methods
.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandler:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 1141
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 171
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 172
    new-instance v0, Landroid/os/HandlerThread;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 173
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 174
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandler:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

    .line 175
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->init()V

    .line 176
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setForegroundService(Z)V

    .line 177
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 267
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->unRegisterContentObservers()V

    .line 269
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setForegroundService(Z)V

    .line 270
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 271
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v10, 0x1

    .line 198
    if-nez p1, :cond_1

    .line 199
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v8, "onStartCommand() intent is NULL"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_0
    :goto_0
    return v10

    .line 203
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 205
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v8, "onStartCommand() action is NULL"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 208
    :cond_2
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onStartCommand() action = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const-string v7, "com.samsung.android.app.galaxyfinder.tag.start_full_sync"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 211
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->useSLinkLocationService()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 212
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v8, "SLink Location service is ON. No need to sync full."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 215
    :cond_3
    const-string v7, "com.samsung.android.app.galaxyfinder.TagServiceState"

    const/4 v8, -0x1

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 216
    .local v5, "tagState":I
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    move-result-object v1

    .local v1, "arr$":[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v6, v1, v2

    .line 217
    .local v6, "type":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->INVALIDTYPE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    if-ne v7, v6, :cond_4

    .line 216
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 221
    :cond_4
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onStartCommand : full scanned media = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", tagState = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getServiceStateString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 224
    .local v4, "msg":Landroid/os/Message;
    iput v10, v4, Landroid/os/Message;->what:I

    .line 225
    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->ordinal()I

    move-result v7

    iput v7, v4, Landroid/os/Message;->arg1:I

    .line 226
    iput v5, v4, Landroid/os/Message;->arg2:I

    .line 227
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->contentUris:[Landroid/net/Uri;

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->ordinal()I

    move-result v8

    aget-object v7, v7, v8

    iput-object v7, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 228
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->mHandler:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;

    invoke-virtual {v7, v4}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    .line 230
    .end local v1    # "arr$":[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "msg":Landroid/os/Message;
    .end local v5    # "tagState":I
    .end local v6    # "type":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    :cond_5
    const-string v7, "com.samsung.android.app.galaxyfinder.tag.start_slink_locationservice"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 231
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->unRegisterObserversBySLink()V

    goto/16 :goto_0

    .line 232
    :cond_6
    const-string v7, "com.samsung.android.app.galaxyfinder.tag.stop_slink_locationservice"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 233
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->registerContentObservers()V

    goto/16 :goto_0

    .line 234
    :cond_7
    const-string v7, "com.samsung.android.app.galaxyfinder.tag.start_service"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 238
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;

    const-string v8, "onStartCommand() action is WRONG value"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
