.class public abstract Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;
.super Ljava/lang/Object;
.source "AbstractTagConverter.java"


# instance fields
.field public mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;->mContext:Landroid/content/Context;

    .line 12
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;->mContext:Landroid/content/Context;

    .line 13
    return-void
.end method


# virtual methods
.method public abstract convertToMeaningValue(Landroid/os/Bundle;)Landroid/os/Bundle;
.end method

.method public abstract convertToRawValue(Landroid/os/Bundle;)Landroid/os/Bundle;
.end method
