.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;
.super Ljava/lang/Object;
.source "BaseItemListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageLoader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;
    }
.end annotation


# static fields
.field public static final CONTENT_TYPE_CONTACT:I = 0x1

.field public static final CONTENT_TYPE_CONTACT_ROUNDED_STYLE:I = 0x2

.field public static final CONTENT_TYPE_IMAGE:I = 0x0

.field public static final CONTENT_TYPE_UNKNOWN:I = -0x1

.field public static final CONTENT_TYPE_VIDEO:I = 0x3

.field public static final IMAGE_ORIENTATION_DEFAULT:I = -0x1

.field public static final IMAGE_ROTATION_180:I = 0xb4

.field public static final IMAGE_ROTATION_270:I = 0x10e

.field public static final IMAGE_ROTATION_90:I = 0x5a

.field public static final IMAGE_ROTATION_ADJUST:I = 0x168


# instance fields
.field private final MAX_IMAGE_THREAD_POOL_VALUE:I

.field private final ORIENTATION_ANGLE_270:I

.field private final ORIENTATION_ANGLE_90:I

.field private final SCHEME_TYPE_ANDROID_RESOURCE:I

.field private final SCHEME_TYPE_CONTENT:I

.field private final SCHEME_TYPE_FILE:I

.field private final SCHEME_TYPE_NONE:I

.field executorService:Ljava/util/concurrent/ExecutorService;

.field handler:Landroid/os/Handler;

.field mContext:Landroid/content/Context;

.field mCr:Landroid/content/ContentResolver;

.field private mLimitHeight:I

.field private mLimitWidth:I

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x2

    .line 923
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 899
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->handler:Landroid/os/Handler;

    .line 905
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mLimitWidth:I

    .line 907
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mLimitHeight:I

    .line 909
    const/16 v0, 0x5a

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->ORIENTATION_ANGLE_90:I

    .line 911
    const/16 v0, 0x10e

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->ORIENTATION_ANGLE_270:I

    .line 913
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->SCHEME_TYPE_NONE:I

    .line 915
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->SCHEME_TYPE_FILE:I

    .line 917
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->SCHEME_TYPE_CONTENT:I

    .line 919
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->SCHEME_TYPE_ANDROID_RESOURCE:I

    .line 921
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->MAX_IMAGE_THREAD_POOL_VALUE:I

    .line 924
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mContext:Landroid/content/Context;

    .line 925
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mCr:Landroid/content/ContentResolver;

    .line 926
    invoke-static {v2}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 927
    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/widget/ImageView;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # I
    .param p6, "x6"    # I
    .param p7, "x7"    # I
    .param p8, "x8"    # I

    .prologue
    .line 875
    invoke-direct/range {p0 .. p8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->queuePhoto(Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;IIII)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    .prologue
    .line 875
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->resetThreadPool()V

    return-void
.end method

.method private applyFaceDetectingEffect(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;ILcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "imgInfo"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p4, "angle"    # I
    .param p5, "style"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;

    .prologue
    .line 1640
    iget-object v0, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://media/external/images"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1642
    iget v2, p3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, p3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v4, p3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    move-object v0, p0

    move-object v1, p1

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->getRectForFace(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;IIII)Landroid/graphics/Rect;

    move-result-object v6

    .line 1645
    .local v6, "faceRect":Landroid/graphics/Rect;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_0

    .line 1646
    iget v0, v6, Landroid/graphics/Rect;->left:I

    iget v1, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {p2, v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 1649
    const/4 v0, 0x1

    iput-boolean v0, p5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;->hasFaces:Z

    .line 1653
    .end local v6    # "faceRect":Landroid/graphics/Rect;
    :cond_0
    return-object p2
.end method

.method private applyHighlightEffect(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "imgInfo"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p4, "style"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;

    .prologue
    .line 1530
    if-eqz p1, :cond_0

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mHighLightRects:Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1700(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1531
    const/4 v0, 0x1

    iput-boolean v0, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;->isHighlighted:Z

    .line 1533
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mHighLightRects:Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1700(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/HighLightUtils;->convertStringToRects(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iget v1, p3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-static {p2, v0, v1}, Lcom/samsung/android/app/galaxyfinder/util/ImageUtils;->HighLightBitmap(Landroid/graphics/Bitmap;Ljava/util/List;I)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 1538
    :cond_0
    return-object p2
.end method

.method private applyShadowEffect(ILandroid/graphics/Bitmap;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "type"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "style"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;

    .prologue
    .line 1514
    const/16 v3, 0xca

    if-ne p1, v3, :cond_0

    .line 1515
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a0379

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 1516
    .local v2, "size":I
    const/4 v0, 0x0

    .line 1517
    .local v0, "offsetX":I
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a0378

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 1519
    .local v1, "offsetY":I
    invoke-static {p2, v2, v0, v1}, Lcom/samsung/samm/spenscrap/ClipImageLibrary;->makeImageShadow(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 1521
    const/4 v3, 0x1

    iput-boolean v3, p3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;->hasShadow:Z

    .line 1524
    .end local v0    # "offsetX":I
    .end local v1    # "offsetY":I
    .end local v2    # "size":I
    :cond_0
    return-object p2
.end method

.method private calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 3
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "reqWidth"    # I
    .param p3, "reqHeight"    # I

    .prologue
    .line 1658
    iget v0, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1659
    .local v0, "height":I
    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1660
    .local v2, "width":I
    const/4 v1, 0x1

    .line 1663
    .local v1, "inSampleSize":I
    :goto_0
    if-lt v2, p2, :cond_0

    if-ge v0, p3, :cond_1

    .line 1671
    :cond_0
    return v1

    .line 1666
    :cond_1
    div-int/lit8 v2, v2, 0x2

    .line 1667
    div-int/lit8 v0, v0, 0x2

    .line 1668
    mul-int/lit8 v1, v1, 0x2

    goto :goto_0
.end method

.method private drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v5, 0x0

    .line 1280
    instance-of v2, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    .line 1281
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1290
    :goto_0
    return-object v0

    .line 1284
    .restart local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1286
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1287
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {p1, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1288
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private getCroppedBitmap(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p3, "imageInfo"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 1582
    const/4 v4, 0x0

    .line 1583
    .local v4, "dst":Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    .line 1585
    .local v6, "dstRect":Landroid/graphics/Rect;
    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1586
    .local v2, "bmpWidth":I
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1588
    .local v1, "bmpHeight":I
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mEffectType:I
    invoke-static/range {p3 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v11

    const/16 v12, 0xcb

    if-ne v11, v12, :cond_3

    .line 1589
    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v7

    .line 1590
    .local v7, "dstWidth":I
    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    .line 1592
    .local v5, "dstHeight":I
    if-eqz v7, :cond_0

    if-nez v5, :cond_1

    .line 1593
    :cond_0
    const/4 v11, 0x0

    .line 1635
    .end local v5    # "dstHeight":I
    .end local v7    # "dstWidth":I
    :goto_0
    return-object v11

    .line 1596
    .restart local v5    # "dstHeight":I
    .restart local v7    # "dstWidth":I
    :cond_1
    int-to-float v11, v5

    int-to-float v12, v7

    div-float v10, v11, v12

    .line 1598
    .local v10, "ratio":F
    new-instance v6, Landroid/graphics/Rect;

    .end local v6    # "dstRect":Landroid/graphics/Rect;
    const/4 v11, 0x0

    const/4 v12, 0x0

    int-to-float v13, v2

    mul-float/2addr v13, v10

    float-to-int v13, v13

    invoke-direct {v6, v11, v12, v2, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1623
    .end local v5    # "dstHeight":I
    .end local v7    # "dstWidth":I
    .end local v10    # "ratio":F
    .restart local v6    # "dstRect":Landroid/graphics/Rect;
    :goto_1
    const/4 v3, 0x0

    .line 1626
    .local v3, "decoder":Landroid/graphics/BitmapRegionDecoder;
    const/4 v11, 0x0

    :try_start_0
    invoke-static {p1, v11}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1631
    :goto_2
    if-eqz v3, :cond_2

    .line 1632
    const/4 v11, 0x0

    invoke-virtual {v3, v6, v11}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    :cond_2
    move-object v11, v4

    .line 1635
    goto :goto_0

    .line 1600
    .end local v3    # "decoder":Landroid/graphics/BitmapRegionDecoder;
    :cond_3
    const/4 v9, 0x0

    .line 1602
    .local v9, "pivotOffset":I
    if-le v1, v2, :cond_5

    .line 1603
    iget v11, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mLimitWidth:I

    if-le v2, v11, :cond_4

    .line 1604
    iget v9, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mLimitWidth:I

    .line 1609
    :goto_3
    new-instance v6, Landroid/graphics/Rect;

    .end local v6    # "dstRect":Landroid/graphics/Rect;
    const/4 v11, 0x0

    shr-int/lit8 v12, v1, 0x1

    sub-int/2addr v12, v9

    shr-int/lit8 v13, v1, 0x1

    add-int/2addr v13, v9

    invoke-direct {v6, v11, v12, v9, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v6    # "dstRect":Landroid/graphics/Rect;
    goto :goto_1

    .line 1606
    :cond_4
    move v9, v2

    goto :goto_3

    .line 1612
    :cond_5
    iget v11, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mLimitHeight:I

    if-le v1, v11, :cond_6

    .line 1613
    iget v9, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mLimitHeight:I

    .line 1618
    :goto_4
    new-instance v6, Landroid/graphics/Rect;

    .end local v6    # "dstRect":Landroid/graphics/Rect;
    shr-int/lit8 v11, v2, 0x1

    sub-int/2addr v11, v9

    const/4 v12, 0x0

    shr-int/lit8 v13, v2, 0x1

    add-int/2addr v13, v9

    invoke-direct {v6, v11, v12, v13, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v6    # "dstRect":Landroid/graphics/Rect;
    goto :goto_1

    .line 1615
    :cond_6
    move v9, v1

    goto :goto_4

    .line 1627
    .end local v9    # "pivotOffset":I
    .restart local v3    # "decoder":Landroid/graphics/BitmapRegionDecoder;
    :catch_0
    move-exception v8

    .line 1628
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method private getImageOrientationFromMediaDB(Landroid/net/Uri;)I
    .locals 10
    .param p1, "imageUri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x1

    .line 1320
    const/4 v6, -0x1

    .line 1321
    .local v6, "angle":I
    const/4 v7, 0x0

    .line 1325
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1327
    .local v8, "uri":Ljava/lang/String;
    const-string v0, "content://media/"

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1328
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "orientation"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1332
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v9, :cond_0

    .line 1333
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1334
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 1342
    .end local v8    # "uri":Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v7, :cond_1

    .line 1343
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1346
    :cond_1
    return v6

    .line 1337
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private getRectForFace(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;IIII)Landroid/graphics/Rect;
    .locals 48
    .param p1, "imgInfo"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;
    .param p2, "srcWidth"    # I
    .param p3, "srcHeight"    # I
    .param p4, "sampleSize"    # I
    .param p5, "angle"    # I

    .prologue
    .line 1351
    new-instance v27, Landroid/graphics/RectF;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/RectF;-><init>()V

    .line 1352
    .local v27, "faceRect":Landroid/graphics/RectF;
    const/16 v22, 0x0

    .line 1355
    .local v22, "dstRect":Landroid/graphics/Rect;
    const-string v13, "pos_left"

    .line 1356
    .local v13, "POS_LEFT":Ljava/lang/String;
    const-string v15, "pos_top"

    .line 1357
    .local v15, "POS_TOP":Ljava/lang/String;
    const-string v14, "pos_right"

    .line 1358
    .local v14, "POS_RIGHT":Ljava/lang/String;
    const-string v12, "pos_bottom"

    .line 1359
    .local v12, "POS_BOTTOM":Ljava/lang/String;
    const-string v6, "content://media/external/faces"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 1360
    .local v7, "FACES_URI":Landroid/net/Uri;
    const/4 v6, 0x4

    new-array v8, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v9, "pos_left"

    aput-object v9, v8, v6

    const/4 v6, 0x1

    const-string v9, "pos_top"

    aput-object v9, v8, v6

    const/4 v6, 0x2

    const-string v9, "pos_right"

    aput-object v9, v8, v6

    const/4 v6, 0x3

    const-string v9, "pos_bottom"

    aput-object v9, v8, v6

    .line 1363
    .local v8, "projection":[Ljava/lang/String;
    const-string v39, "image_id =?"

    .line 1365
    .local v39, "selection":Ljava/lang/String;
    const/high16 v6, 0x4f000000

    move-object/from16 v0, v27

    iput v6, v0, Landroid/graphics/RectF;->left:F

    .line 1366
    const/high16 v6, 0x4f000000

    move-object/from16 v0, v27

    iput v6, v0, Landroid/graphics/RectF;->top:F

    .line 1367
    const/4 v6, 0x0

    move-object/from16 v0, v27

    iput v6, v0, Landroid/graphics/RectF;->right:F

    .line 1368
    const/4 v6, 0x0

    move-object/from16 v0, v27

    iput v6, v0, Landroid/graphics/RectF;->bottom:F

    .line 1370
    const/16 v28, 0x0

    .line 1374
    .local v28, "faceRectCursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v30

    .line 1376
    .local v30, "id":J
    const-wide/16 v10, 0x0

    cmp-long v6, v30, v10

    if-lez v6, :cond_4

    .line 1377
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mCr:Landroid/content/ContentResolver;

    const-string v9, "image_id =?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v47

    aput-object v47, v10, v11

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v28

    .line 1381
    if-eqz v28, :cond_4

    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_4

    .line 1382
    const/16 v35, 0x0

    .local v35, "l":I
    const/16 v41, 0x0

    .local v41, "t":I
    const/16 v38, 0x0

    .local v38, "r":I
    const/16 v16, 0x0

    .line 1384
    .local v16, "b":I
    const-string v6, "pos_left"

    move-object/from16 v0, v28

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 1385
    .local v32, "idx_left":I
    const-string v6, "pos_top"

    move-object/from16 v0, v28

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    .line 1386
    .local v34, "idx_top":I
    const-string v6, "pos_right"

    move-object/from16 v0, v28

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 1387
    .local v33, "idx_right":I
    const-string v6, "pos_bottom"

    move-object/from16 v0, v28

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 1388
    .local v29, "idx_bottom":I
    :cond_0
    :goto_0
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1389
    move-object/from16 v0, v28

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v35

    .line 1390
    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v41

    .line 1391
    move-object/from16 v0, v28

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v38

    .line 1392
    invoke-interface/range {v28 .. v29}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 1394
    move/from16 v0, v35

    int-to-float v6, v0

    move-object/from16 v0, v27

    iget v9, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v6, v6, v9

    if-gez v6, :cond_1

    .line 1395
    move/from16 v0, v35

    int-to-float v6, v0

    move-object/from16 v0, v27

    iput v6, v0, Landroid/graphics/RectF;->left:F

    .line 1398
    :cond_1
    move/from16 v0, v41

    int-to-float v6, v0

    move-object/from16 v0, v27

    iget v9, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v6, v6, v9

    if-gez v6, :cond_2

    .line 1399
    move/from16 v0, v41

    int-to-float v6, v0

    move-object/from16 v0, v27

    iput v6, v0, Landroid/graphics/RectF;->top:F

    .line 1402
    :cond_2
    move/from16 v0, v38

    int-to-float v6, v0

    move-object/from16 v0, v27

    iget v9, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v6, v6, v9

    if-lez v6, :cond_3

    .line 1403
    move/from16 v0, v38

    int-to-float v6, v0

    move-object/from16 v0, v27

    iput v6, v0, Landroid/graphics/RectF;->right:F

    .line 1406
    :cond_3
    move/from16 v0, v16

    int-to-float v6, v0

    move-object/from16 v0, v27

    iget v9, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v6, v6, v9

    if-lez v6, :cond_0

    .line 1407
    move/from16 v0, v16

    int-to-float v6, v0

    move-object/from16 v0, v27

    iput v6, v0, Landroid/graphics/RectF;->bottom:F
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1500
    .end local v16    # "b":I
    .end local v29    # "idx_bottom":I
    .end local v30    # "id":J
    .end local v32    # "idx_left":I
    .end local v33    # "idx_right":I
    .end local v34    # "idx_top":I
    .end local v35    # "l":I
    .end local v38    # "r":I
    .end local v41    # "t":I
    :catch_0
    move-exception v26

    .line 1501
    .local v26, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual/range {v26 .. v26}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 1506
    .end local v26    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_4
    :goto_1
    if-eqz v28, :cond_5

    .line 1507
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    .line 1510
    :cond_5
    return-object v22

    .line 1412
    .restart local v16    # "b":I
    .restart local v29    # "idx_bottom":I
    .restart local v30    # "id":J
    .restart local v32    # "idx_left":I
    .restart local v33    # "idx_right":I
    .restart local v34    # "idx_top":I
    .restart local v35    # "l":I
    .restart local v38    # "r":I
    .restart local v41    # "t":I
    :cond_6
    :try_start_1
    invoke-virtual/range {v27 .. v27}, Landroid/graphics/RectF;->width()F

    move-result v6

    const/4 v9, 0x0

    cmpl-float v6, v6, v9

    if-lez v6, :cond_4

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/RectF;->height()F

    move-result v6

    const/4 v9, 0x0

    cmpl-float v6, v6, v9

    if-lez v6, :cond_4

    .line 1414
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v37

    .line 1416
    .local v37, "params":Landroid/view/ViewGroup$LayoutParams;
    const/16 v46, 0x0

    .line 1417
    .local v46, "viewWidth":I
    const/16 v45, 0x0

    .line 1419
    .local v45, "viewHeight":I
    if-eqz v37, :cond_4

    .line 1420
    move-object/from16 v0, v37

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    move/from16 v46, v0

    .line 1421
    move-object/from16 v0, v37

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    move/from16 v45, v0

    .line 1423
    move/from16 v0, p2

    int-to-float v6, v0

    move/from16 v0, p3

    int-to-float v9, v0

    div-float v40, v6, v9

    .line 1424
    .local v40, "srcRatio":F
    move/from16 v0, v46

    int-to-float v6, v0

    move/from16 v0, v45

    int-to-float v9, v0

    div-float v21, v6, v9

    .line 1426
    .local v21, "dstRatio":F
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$1400()Ljava/lang/String;

    move-result-object v6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ratio view info - srcRadio : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v40

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", dstRatio : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1429
    cmpl-float v6, v40, v21

    if-lez v6, :cond_f

    .line 1431
    move/from16 v45, p3

    .line 1432
    move/from16 v0, v45

    int-to-float v6, v0

    mul-float v6, v6, v21

    float-to-int v0, v6

    move/from16 v46, v0

    .line 1439
    :goto_2
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$1400()Ljava/lang/String;

    move-result-object v6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "resized view info - width : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v46

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", height : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v45

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1442
    move-object/from16 v0, v27

    iget v6, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/RectF;->width()F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v6, v9

    move/from16 v0, p4

    int-to-float v9, v0

    div-float/2addr v6, v9

    float-to-int v0, v6

    move/from16 v17, v0

    .line 1443
    .local v17, "centerX":I
    move-object/from16 v0, v27

    iget v6, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/RectF;->height()F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v6, v9

    move/from16 v0, p4

    int-to-float v9, v0

    div-float/2addr v6, v9

    float-to-int v0, v6

    move/from16 v18, v0

    .line 1445
    .local v18, "centerY":I
    shr-int/lit8 v44, v46, 0x1

    .line 1446
    .local v44, "viewHalfWidth":I
    shr-int/lit8 v43, v45, 0x1

    .line 1448
    .local v43, "viewHalfHeight":I
    const/16 v6, 0x5a

    move/from16 v0, p5

    if-eq v0, v6, :cond_7

    const/16 v6, 0x10e

    move/from16 v0, p5

    if-ne v0, v6, :cond_8

    .line 1450
    :cond_7
    move/from16 v42, v44

    .line 1452
    .local v42, "temp":I
    move/from16 v44, v43

    .line 1453
    move/from16 v43, v42

    .line 1456
    .end local v42    # "temp":I
    :cond_8
    sub-int v20, v17, v44

    .line 1457
    .local v20, "dstLeft":I
    add-int v24, v17, v44

    .line 1458
    .local v24, "dstRight":I
    sub-int v25, v18, v43

    .line 1459
    .local v25, "dstTop":I
    add-int v19, v18, v43

    .line 1461
    .local v19, "dstBottom":I
    if-gez v20, :cond_9

    .line 1462
    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(I)I

    move-result v36

    .line 1463
    .local v36, "offset":I
    add-int v24, v24, v36

    .line 1464
    const/16 v20, 0x0

    .line 1467
    .end local v36    # "offset":I
    :cond_9
    move/from16 v0, v24

    move/from16 v1, p2

    if-le v0, v1, :cond_b

    .line 1468
    sub-int v36, v24, p2

    .line 1469
    .restart local v36    # "offset":I
    sub-int v20, v20, v36

    .line 1471
    if-gez v20, :cond_a

    .line 1472
    const/16 v20, 0x0

    .line 1475
    :cond_a
    add-int/lit8 v24, p2, -0x1

    .line 1478
    .end local v36    # "offset":I
    :cond_b
    if-gez v25, :cond_c

    .line 1479
    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(I)I

    move-result v36

    .line 1480
    .restart local v36    # "offset":I
    add-int v19, v19, v36

    .line 1481
    const/16 v25, 0x0

    .line 1484
    .end local v36    # "offset":I
    :cond_c
    move/from16 v0, v19

    move/from16 v1, p3

    if-le v0, v1, :cond_e

    .line 1485
    sub-int v36, v19, p3

    .line 1486
    .restart local v36    # "offset":I
    sub-int v25, v25, v36

    .line 1488
    if-gez v25, :cond_d

    .line 1489
    const/16 v25, 0x0

    .line 1492
    :cond_d
    add-int/lit8 v19, p3, -0x1

    .line 1495
    .end local v36    # "offset":I
    :cond_e
    new-instance v23, Landroid/graphics/Rect;

    move-object/from16 v0, v23

    move/from16 v1, v20

    move/from16 v2, v25

    move/from16 v3, v24

    move/from16 v4, v19

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .end local v22    # "dstRect":Landroid/graphics/Rect;
    .local v23, "dstRect":Landroid/graphics/Rect;
    move-object/from16 v22, v23

    .end local v23    # "dstRect":Landroid/graphics/Rect;
    .restart local v22    # "dstRect":Landroid/graphics/Rect;
    goto/16 :goto_1

    .line 1435
    .end local v17    # "centerX":I
    .end local v18    # "centerY":I
    .end local v19    # "dstBottom":I
    .end local v20    # "dstLeft":I
    .end local v24    # "dstRight":I
    .end local v25    # "dstTop":I
    .end local v43    # "viewHalfHeight":I
    .end local v44    # "viewHalfWidth":I
    :cond_f
    move/from16 v46, p2

    .line 1436
    move/from16 v0, v46

    int-to-float v6, v0

    div-float v6, v6, v21

    float-to-int v0, v6

    move/from16 v45, v0

    goto/16 :goto_2

    .line 1502
    .end local v16    # "b":I
    .end local v21    # "dstRatio":F
    .end local v29    # "idx_bottom":I
    .end local v30    # "id":J
    .end local v32    # "idx_left":I
    .end local v33    # "idx_right":I
    .end local v34    # "idx_top":I
    .end local v35    # "l":I
    .end local v37    # "params":Landroid/view/ViewGroup$LayoutParams;
    .end local v38    # "r":I
    .end local v40    # "srcRatio":F
    .end local v41    # "t":I
    .end local v45    # "viewHeight":I
    .end local v46    # "viewWidth":I
    :catch_1
    move-exception v26

    .line 1503
    .local v26, "e":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1
.end method

.method private getRoundedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "color"    # I

    .prologue
    const/4 v9, 0x0

    .line 1294
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1296
    .local v1, "output":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1298
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 1299
    .local v2, "paint":Landroid/graphics/Paint;
    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v4, v9, v9, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1300
    .local v4, "rect":Landroid/graphics/Rect;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 1302
    .local v5, "rectF":Landroid/graphics/RectF;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1304
    const/4 v6, -0x1

    if-eq p2, v6, :cond_0

    .line 1305
    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1306
    invoke-virtual {v2, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1309
    :cond_0
    invoke-virtual {v0, v5, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1311
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 1312
    .local v3, "paint2":Landroid/graphics/Paint;
    new-instance v6, Landroid/graphics/PorterDuffXfermode;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v7}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1314
    const/4 v6, 0x0

    invoke-virtual {v0, p1, v6, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1316
    return-object v1
.end method

.method private isNeededCroppedBitmap(Landroid/graphics/BitmapFactory$Options;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Z
    .locals 9
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "imageInfo"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    const/4 v8, 0x5

    const/4 v5, 0x1

    .line 1559
    const/4 v0, 0x5

    .line 1560
    .local v0, "BITMAP_NORMAL_RATIO":I
    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1561
    .local v2, "bmpWidth":I
    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1562
    .local v1, "bmpHeight":I
    iget-object v6, p2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    .line 1563
    .local v4, "dstWidth":I
    iget-object v6, p2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    .line 1565
    .local v3, "dstHeight":I
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mEffectType:I
    invoke-static {p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v6

    const/16 v7, 0xcb

    if-ne v6, v7, :cond_1

    .line 1566
    if-lez v4, :cond_1

    if-lez v3, :cond_1

    if-le v2, v4, :cond_1

    if-le v1, v3, :cond_1

    .line 1577
    :cond_0
    :goto_0
    return v5

    .line 1572
    :cond_1
    div-int v6, v1, v2

    if-gt v6, v8, :cond_0

    div-int v6, v2, v1

    if-gt v6, v8, :cond_0

    .line 1577
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private queuePhoto(Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 10
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "altUri"    # Ljava/lang/String;
    .param p4, "highlightRect"    # Ljava/lang/String;
    .param p5, "effect"    # I
    .param p6, "contentType"    # I
    .param p7, "orientation"    # I
    .param p8, "bgColor"    # I

    .prologue
    .line 932
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 935
    .local v0, "p":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 937
    return-void
.end method

.method private resetThreadPool()V
    .locals 1

    .prologue
    .line 940
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 941
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 942
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 944
    :cond_0
    return-void
.end method

.method private setRotation(Landroid/net/Uri;Landroid/graphics/Bitmap;ILcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "imgUri"    # Landroid/net/Uri;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "angle"    # I
    .param p4, "style"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1542
    if-eqz p1, :cond_0

    .line 1543
    if-lez p3, :cond_0

    .line 1544
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1546
    .local v5, "matrix":Landroid/graphics/Matrix;
    int-to-float v0, p3

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1548
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move-object v0, p2

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 1551
    iput-boolean v6, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;->isRotated:Z

    .line 1555
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    :cond_0
    return-object p2
.end method


# virtual methods
.method public getBitmap(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Landroid/graphics/drawable/Drawable;
    .locals 15
    .param p1, "imageInfo"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 1040
    const/4 v7, 0x0

    .line 1041
    .local v7, "drawable":Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    .line 1043
    .local v3, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_3

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_3

    .line 1044
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_e

    .line 1047
    :cond_0
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1203
    :cond_1
    :goto_0
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 1204
    if-eqz v7, :cond_3

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    if-nez v1, :cond_3

    .line 1206
    :cond_2
    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 1207
    .local v12, "src":Landroid/graphics/Bitmap;
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mBgColor:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    invoke-direct {p0, v12, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->getRoundedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1208
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    invoke-direct {v7, v1, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1229
    .end local v12    # "src":Landroid/graphics/Bitmap;
    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    :goto_1
    return-object v7

    .line 1052
    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mCr:Landroid/content/ContentResolver;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v11

    .line 1054
    .local v11, "is":Ljava/io/InputStream;
    if-eqz v11, :cond_1

    .line 1055
    const/4 v1, 0x0

    invoke-static {v11, v1}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1056
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1058
    .end local v11    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v9

    .line 1059
    .local v9, "e":Ljava/lang/Exception;
    const-string v1, "ImageView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unable to open content: "

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1067
    .end local v9    # "e":Ljava/lang/Exception;
    :pswitch_1
    const/4 v10, 0x0

    .line 1070
    .local v10, "inputStream":Ljava/io/InputStream;
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mCr:Landroid/content/ContentResolver;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v10

    .line 1072
    if-eqz v10, :cond_8

    .line 1073
    const/4 v13, 0x0

    .line 1075
    .local v13, "useCache":Z
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1076
    .local v4, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1078
    const/4 v1, 0x0

    invoke-static {v10, v1, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1081
    :try_start_2
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1086
    :goto_2
    :try_start_3
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mCr:Landroid/content/ContentResolver;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v10

    .line 1088
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mLimitWidth:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mLimitHeight:I

    invoke-direct {p0, v4, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v1

    iput v1, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1091
    const/4 v1, 0x0

    iput-boolean v1, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1092
    const/4 v1, 0x1

    iput-boolean v1, v4, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 1093
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1094
    const/4 v1, 0x1

    iput-boolean v1, v4, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 1096
    iget v1, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v2, 0x1

    if-le v1, v2, :cond_4

    .line 1097
    const/4 v13, 0x1

    .line 1102
    :cond_4
    :try_start_4
    move-object/from16 v0, p1

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->isNeededCroppedBitmap(Landroid/graphics/BitmapFactory$Options;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1103
    move-object/from16 v0, p1

    invoke-direct {p0, v10, v4, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->getCroppedBitmap(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v3

    .line 1114
    :goto_3
    if-eqz v3, :cond_8

    .line 1115
    :try_start_5
    new-instance v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;)V

    .line 1116
    .local v6, "style":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;
    const/4 v5, -0x1

    .line 1117
    .local v5, "angle":I
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mImageOrienation:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    const/16 v2, 0x168

    if-ne v1, v2, :cond_b

    .line 1118
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_5

    .line 1119
    const/16 v5, 0x10e

    .line 1129
    :cond_5
    :goto_4
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v3, v4, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->applyHighlightEffect(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v1, p0

    move-object/from16 v2, p1

    .line 1134
    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->applyFaceDetectingEffect(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;ILcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1139
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v1, v3, v5, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->setRotation(Landroid/net/Uri;Landroid/graphics/Bitmap;ILcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1142
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mEffectType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    invoke-direct {p0, v1, v3, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->applyShadowEffect(ILandroid/graphics/Bitmap;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1145
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 1146
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mBgColor:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    invoke-direct {p0, v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->getRoundedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1150
    :cond_6
    new-instance v8, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    invoke-direct {v8, v1, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1153
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    .local v8, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v13, :cond_7

    :try_start_6
    iget-boolean v1, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;->isHighlighted:Z

    if-nez v1, :cond_7

    if-eqz v8, :cond_7

    .line 1154
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getInstance()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCacheKey(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v14}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$1300(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object v0, v8

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    move-object v1, v0

    invoke-virtual {v2, v14, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->putBitmapDrawable(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_e
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_d
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_c
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_7
    move-object v7, v8

    .line 1162
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v5    # "angle":I
    .end local v6    # "style":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;
    .end local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v13    # "useCache":Z
    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_8
    if-eqz v10, :cond_9

    .line 1163
    :try_start_7
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1176
    :cond_9
    :goto_5
    if-eqz v10, :cond_1

    .line 1177
    :try_start_8
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_0

    .line 1179
    :catch_1
    move-exception v9

    .line 1180
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 1082
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v13    # "useCache":Z
    :catch_2
    move-exception v9

    .line 1083
    .restart local v9    # "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_2

    .line 1168
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v9    # "e":Ljava/io/IOException;
    .end local v13    # "useCache":Z
    :catch_3
    move-exception v9

    .line 1169
    .local v9, "e":Ljava/io/FileNotFoundException;
    :goto_6
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1176
    if-eqz v10, :cond_1

    .line 1177
    :try_start_b
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto/16 :goto_0

    .line 1179
    :catch_4
    move-exception v9

    .line 1180
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 1106
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v13    # "useCache":Z
    :cond_a
    const/4 v1, 0x0

    :try_start_c
    invoke-static {v10, v1, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_c .. :try_end_c} :catch_6
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_9
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-object v3

    goto/16 :goto_3

    .line 1109
    :catch_5
    move-exception v9

    .line 1110
    .local v9, "e":Ljava/lang/OutOfMemoryError;
    :try_start_d
    invoke-virtual {v9}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 1111
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getInstance()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->clearLruCache()V
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_9
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_3

    .line 1170
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v9    # "e":Ljava/lang/OutOfMemoryError;
    .end local v13    # "useCache":Z
    :catch_6
    move-exception v9

    .line 1171
    .local v9, "e":Ljava/lang/NullPointerException;
    :goto_7
    :try_start_e
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1176
    if-eqz v10, :cond_1

    .line 1177
    :try_start_f
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7

    goto/16 :goto_0

    .line 1179
    :catch_7
    move-exception v9

    .line 1180
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 1121
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v5    # "angle":I
    .restart local v6    # "style":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;
    .restart local v13    # "useCache":Z
    :cond_b
    :try_start_10
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mImageOrienation:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_c

    .line 1122
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mImageOrienation:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v5

    goto/16 :goto_4

    .line 1125
    :cond_c
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->getImageOrientationFromMediaDB(Landroid/net/Uri;)I

    move-result v5

    goto/16 :goto_4

    .line 1165
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v5    # "angle":I
    .end local v6    # "style":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;
    .end local v13    # "useCache":Z
    :catch_8
    move-exception v9

    .line 1166
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_10 .. :try_end_10} :catch_6
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_9
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_5

    .line 1172
    .end local v9    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v9

    .line 1173
    .local v9, "e":Ljava/lang/Exception;
    :goto_8
    :try_start_11
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 1176
    if-eqz v10, :cond_1

    .line 1177
    :try_start_12
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_a

    goto/16 :goto_0

    .line 1179
    :catch_a
    move-exception v9

    .line 1180
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 1175
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 1176
    :goto_9
    if-eqz v10, :cond_d

    .line 1177
    :try_start_13
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_b

    .line 1181
    :cond_d
    :goto_a
    throw v1

    .line 1179
    :catch_b
    move-exception v9

    .line 1180
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 1187
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "inputStream":Ljava/io/InputStream;
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPackageName:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-static {v1, v2, v14}, Lcom/samsung/android/app/galaxyfinder/util/ResourceLoader;->getResourcesDrawable(Landroid/content/Context;Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1190
    if-eqz v7, :cond_1

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1192
    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 1193
    .restart local v12    # "src":Landroid/graphics/Bitmap;
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mBgColor:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    invoke-direct {p0, v12, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->getRoundedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1194
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    invoke-direct {v7, v1, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1195
    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_0

    .line 1211
    .end local v12    # "src":Landroid/graphics/Bitmap;
    :cond_e
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_f

    .line 1212
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1215
    if-eqz v3, :cond_3

    .line 1216
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    invoke-direct {v7, v1, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1218
    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v7, :cond_3

    .line 1219
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getInstance()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCacheKey(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v14}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$1300(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object v1, v7

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2, v14, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->putBitmapDrawable(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)V

    goto/16 :goto_1

    .line 1225
    :cond_f
    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$1400()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "unknown content type ("

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v14

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v14, ")"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1175
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v5    # "angle":I
    .restart local v6    # "style":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapStyle;
    .restart local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v10    # "inputStream":Ljava/io/InputStream;
    .restart local v13    # "useCache":Z
    :catchall_1
    move-exception v1

    move-object v7, v8

    .end local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_9

    .line 1172
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    :catch_c
    move-exception v9

    move-object v7, v8

    .end local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_8

    .line 1170
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    :catch_d
    move-exception v9

    move-object v7, v8

    .end local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_7

    .line 1168
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    :catch_e
    move-exception v9

    move-object v7, v8

    .end local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_6

    .line 1047
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method imageViewReused(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Z
    .locals 1
    .param p1, "photoToLoad"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 1029
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1032
    const/4 v0, 0x1

    .line 1035
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
