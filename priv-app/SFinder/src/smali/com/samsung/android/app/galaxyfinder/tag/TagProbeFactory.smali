.class public Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;
.super Ljava/lang/Object;
.source "TagProbeFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$1;,
        Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;
    }
.end annotation


# static fields
.field private static final GALLERY_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.gallery3d"

.field private static final MEDIA_URIS:[Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;

.field private static volatile instance:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;

.field private static final mbEnableUserTagInGallery:Z


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->MEDIA_URIS:[Landroid/net/Uri;

    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->instance:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method private createTagProbe(Landroid/app/SearchableInfo;)Ljava/util/List;
    .locals 24
    .param p1, "aSearchable"    # Landroid/app/SearchableInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/SearchableInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getTagFilters()Ljava/lang/String;

    move-result-object v17

    .line 65
    .local v17, "tagFilters":Ljava/lang/String;
    const-string v20, "\\|"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 66
    .local v16, "supportTags":[Ljava/lang/String;
    if-eqz v16, :cond_0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v20, v0

    if-gtz v20, :cond_2

    .line 67
    :cond_0
    const/4 v12, 0x0

    .line 163
    :cond_1
    :goto_0
    return-object v12

    .line 70
    :cond_2
    invoke-static/range {v16 .. v16}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v15

    .line 71
    .local v15, "supportTagList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v15, :cond_3

    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v20

    if-eqz v20, :cond_4

    .line 72
    :cond_3
    const/4 v12, 0x0

    goto :goto_0

    .line 75
    :cond_4
    sget-object v13, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->DEFAULT_Query:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    .line 76
    .local v13, "methodType":Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v14

    .line 78
    .local v14, "pkg":Ljava/lang/String;
    const-string v20, "com.sec.android.gallery3d"

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 79
    sget-object v13, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->DIRECT_Query:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    .line 82
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getTagProviderUri()Ljava/lang/String;

    move-result-object v9

    .line 83
    .local v9, "contentUri":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->MEDIA_URIS:[Landroid/net/Uri;

    .local v7, "arr$":[Landroid/net/Uri;
    array-length v11, v7

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_1
    if-ge v10, v11, :cond_6

    aget-object v6, v7, v10

    .line 84
    .local v6, "aUri":Landroid/net/Uri;
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 85
    sget-object v13, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->DIRECT_Query:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    .line 90
    .end local v6    # "aUri":Landroid/net/Uri;
    :cond_6
    const/4 v4, 0x0

    .line 91
    .local v4, "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    const/4 v8, 0x0

    .line 92
    .local v8, "attr":Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v12, "madeProbes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v18, "tagTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/Constants$TagType;>;"
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v20

    if-eqz v20, :cond_7

    .line 96
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "createTagProbe() Package = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", methodType = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", supportTagList = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_7
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$1;->$SwitchMap$com$samsung$android$app$galaxyfinder$tag$TagProbeFactory$QueryMethodType:[I

    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->ordinal()I

    move-result v21

    aget v20, v20, v21

    packed-switch v20, :pswitch_data_0

    goto/16 :goto_0

    .line 101
    :pswitch_0
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 102
    .local v5, "aTag":Ljava/lang/String;
    const-string v20, "location"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 103
    new-instance v8, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    .end local v8    # "attr":Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    const-string v21, "com.samsung.android.app.galaxyfinder.tag.media_location_update_action"

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    move-object/from16 v3, p1

    invoke-direct {v8, v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;-><init>(Landroid/net/Uri;Ljava/lang/String;ZLandroid/app/SearchableInfo;)V

    .line 105
    .restart local v8    # "attr":Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;

    .end local v4    # "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    sget-object v23, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    aput-object v23, v21, v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v4, v0, v1, v8}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;-><init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V

    .line 129
    .restart local v4    # "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    :goto_3
    if-eqz v4, :cond_8

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_8

    .line 130
    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 83
    .end local v4    # "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    .end local v5    # "aTag":Ljava/lang/String;
    .end local v8    # "attr":Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    .end local v12    # "madeProbes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;>;"
    .end local v18    # "tagTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/Constants$TagType;>;"
    .restart local v6    # "aUri":Landroid/net/Uri;
    .local v10, "i$":I
    :cond_9
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 108
    .end local v6    # "aUri":Landroid/net/Uri;
    .restart local v4    # "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    .restart local v5    # "aTag":Ljava/lang/String;
    .restart local v8    # "attr":Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    .local v10, "i$":Ljava/util/Iterator;
    .restart local v12    # "madeProbes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;>;"
    .restart local v18    # "tagTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/Constants$TagType;>;"
    :cond_a
    const-string v20, "people"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 109
    new-instance v8, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    .end local v8    # "attr":Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    move-object/from16 v3, p1

    invoke-direct {v8, v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;-><init>(Landroid/net/Uri;Ljava/lang/String;ZLandroid/app/SearchableInfo;)V

    .line 110
    .restart local v8    # "attr":Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;

    .end local v4    # "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    sget-object v23, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->PEOPLE:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    aput-object v23, v21, v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v4, v0, v1, v8}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;-><init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V

    .restart local v4    # "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    goto :goto_3

    .line 113
    :cond_b
    const-string v20, "user"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 120
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->TAG:Ljava/lang/String;

    const-string v21, "createTagProbe() Gallery doesn\'t support USER TAG"

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 122
    :cond_c
    const-string v20, "weather"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 123
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->TAG:Ljava/lang/String;

    const-string v21, "createTagProbe() No more support weather tag type."

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 126
    :cond_d
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "createTagProbe() None matched TagProbe > "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 136
    .end local v5    # "aTag":Ljava/lang/String;
    .local v10, "i$":I
    :pswitch_1
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_12

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 137
    .restart local v5    # "aTag":Ljava/lang/String;
    const-string v20, "location"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 138
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 139
    :cond_e
    const-string v20, "people"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 140
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->PEOPLE:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 141
    :cond_f
    const-string v20, "user"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 142
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->USERDEF:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 143
    :cond_10
    const-string v20, "weather"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_11

    .line 144
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->TAG:Ljava/lang/String;

    const-string v21, "createTagProbe() No more support weather tag type."

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 147
    :cond_11
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "createTagProbe() None matched TagProbe > "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 151
    .end local v5    # "aTag":Ljava/lang/String;
    :cond_12
    new-instance v8, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    .end local v8    # "attr":Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    move-object/from16 v3, p1

    invoke-direct {v8, v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;-><init>(Landroid/net/Uri;Ljava/lang/String;ZLandroid/app/SearchableInfo;)V

    .line 152
    .restart local v8    # "attr":Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    sget-object v22, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    aput-object v22, v20, v21

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v19

    check-cast v19, [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    .line 156
    .local v19, "types":[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    new-instance v20, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v8}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;-><init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->instance:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;

    if-nez v0, :cond_0

    .line 52
    const-class v1, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;

    monitor-enter v1

    .line 53
    :try_start_0
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->instance:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;

    .line 54
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :cond_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->instance:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;

    return-object v0

    .line 54
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public get(Landroid/app/SearchableInfo;)Ljava/util/List;
    .locals 1
    .param p1, "aSearchable"    # Landroid/app/SearchableInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/SearchableInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->createTagProbe(Landroid/app/SearchableInfo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
