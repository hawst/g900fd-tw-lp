.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListVNoteAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 25
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 25
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 29
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v12

    .line 30
    .local v12, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v10, 0x0

    .line 32
    .local v10, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 34
    if-nez p2, :cond_6

    .line 35
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v22, 0x7f03004d

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v21 .. v24}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 37
    new-instance v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;

    .end local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;
    invoke-direct {v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;-><init>()V

    .line 39
    .restart local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;
    const v21, 0x7f0b0054

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 40
    const v21, 0x7f0b0056

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvDate:Landroid/widget/TextView;

    .line 41
    const v21, 0x7f0b008c

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvTime:Landroid/widget/TextView;

    .line 42
    const v21, 0x7f0b00a6

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 43
    const v21, 0x7f0b00a7

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvColorBar:Landroid/view/View;

    .line 44
    const v21, 0x7f0b0066

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->ivPersonal:Landroid/widget/ImageView;

    .line 46
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 48
    const/16 v17, 0x0

    .line 49
    .local v17, "resultTime":Ljava/util/Date;
    const-wide/16 v14, 0x0

    .line 50
    .local v14, "milliSec":J
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v13

    .line 51
    .local v13, "name":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v6

    .line 52
    .local v6, "desc":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v20

    .line 53
    .local v20, "time":Ljava/lang/String;
    const/4 v5, 0x0

    .line 56
    .local v5, "date":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 57
    :try_start_0
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 58
    const-wide/16 v22, 0x0

    cmp-long v21, v14, v22

    if-gtz v21, :cond_4

    .line 59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e008b

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 68
    :cond_0
    :goto_0
    if-nez v5, :cond_1

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v21

    new-instance v22, Ljava/util/Date;

    move-object/from16 v0, v22

    invoke-direct {v0, v14, v15}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v21 .. v22}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 74
    :cond_1
    :goto_1
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v13, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 75
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvDate:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v5, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 76
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvTime:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 82
    .end local v5    # "date":Ljava/lang/String;
    .end local v6    # "desc":Ljava/lang/String;
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "milliSec":J
    .end local v17    # "resultTime":Ljava/util/Date;
    .end local v20    # "time":Ljava/lang/String;
    :goto_2
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v4

    .line 83
    .local v4, "color":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v8

    .line 84
    .local v8, "extraValue":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v11

    .line 86
    .local v11, "intent":Landroid/content/Intent;
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_2

    .line 88
    :try_start_1
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 90
    .local v9, "flag":I
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v9, v0, :cond_2

    .line 91
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/widget/RelativeLayout$LayoutParams;

    .line 93
    .local v16, "paramsName":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0a0308

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 95
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 102
    .end local v9    # "flag":I
    .end local v16    # "paramsName":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    :goto_3
    const v19, -0x7fff80

    .line 104
    .local v19, "rgb":I
    if-eqz v4, :cond_3

    .line 106
    :try_start_2
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v19

    .line 112
    :cond_3
    :goto_4
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvColorBar:Landroid/view/View;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 114
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-virtual {v0, v1, v11, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 116
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v21

    return-object v21

    .line 61
    .end local v4    # "color":Ljava/lang/String;
    .end local v8    # "extraValue":Ljava/lang/String;
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v19    # "rgb":I
    .restart local v5    # "date":Ljava/lang/String;
    .restart local v6    # "desc":Ljava/lang/String;
    .restart local v13    # "name":Ljava/lang/String;
    .restart local v14    # "milliSec":J
    .restart local v17    # "resultTime":Ljava/util/Date;
    .restart local v20    # "time":Ljava/lang/String;
    :cond_4
    :try_start_3
    new-instance v18, Ljava/util/Date;

    move-object/from16 v0, v18

    invoke-direct {v0, v14, v15}, Ljava/util/Date;-><init>(J)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    .end local v17    # "resultTime":Ljava/util/Date;
    .local v18, "resultTime":Ljava/util/Date;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v5

    move-object/from16 v17, v18

    .end local v18    # "resultTime":Ljava/util/Date;
    .restart local v17    # "resultTime":Ljava/util/Date;
    goto/16 :goto_0

    .line 65
    :catch_0
    move-exception v7

    .line 66
    .local v7, "e":Ljava/lang/NumberFormatException;
    :goto_5
    :try_start_5
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 68
    if-nez v5, :cond_1

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v21

    new-instance v22, Ljava/util/Date;

    move-object/from16 v0, v22

    invoke-direct {v0, v14, v15}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v21 .. v22}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 68
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v21

    :goto_6
    if-nez v5, :cond_5

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v22

    new-instance v23, Ljava/util/Date;

    move-object/from16 v0, v23

    invoke-direct {v0, v14, v15}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v22 .. v23}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    :cond_5
    throw v21

    .line 79
    .end local v5    # "date":Ljava/lang/String;
    .end local v6    # "desc":Ljava/lang/String;
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "milliSec":J
    .end local v17    # "resultTime":Ljava/util/Date;
    .end local v20    # "time":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;
    check-cast v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;

    .restart local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;
    goto/16 :goto_2

    .line 97
    .restart local v4    # "color":Ljava/lang/String;
    .restart local v8    # "extraValue":Ljava/lang/String;
    .restart local v11    # "intent":Landroid/content/Intent;
    :catch_1
    move-exception v7

    .line 98
    .restart local v7    # "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_3

    .line 107
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .restart local v19    # "rgb":I
    :catch_2
    move-exception v7

    .line 108
    .restart local v7    # "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_4

    .line 68
    .end local v4    # "color":Ljava/lang/String;
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .end local v8    # "extraValue":Ljava/lang/String;
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v19    # "rgb":I
    .restart local v5    # "date":Ljava/lang/String;
    .restart local v6    # "desc":Ljava/lang/String;
    .restart local v13    # "name":Ljava/lang/String;
    .restart local v14    # "milliSec":J
    .restart local v18    # "resultTime":Ljava/util/Date;
    .restart local v20    # "time":Ljava/lang/String;
    :catchall_1
    move-exception v21

    move-object/from16 v17, v18

    .end local v18    # "resultTime":Ljava/util/Date;
    .restart local v17    # "resultTime":Ljava/util/Date;
    goto :goto_6

    .line 65
    .end local v17    # "resultTime":Ljava/util/Date;
    .restart local v18    # "resultTime":Ljava/util/Date;
    :catch_3
    move-exception v7

    move-object/from16 v17, v18

    .end local v18    # "resultTime":Ljava/util/Date;
    .restart local v17    # "resultTime":Ljava/util/Date;
    goto :goto_5
.end method
