.class Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;
.super Ljava/lang/Object;
.source "BaseCategoryView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$600(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v1

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$600(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 428
    :goto_0
    return-void

    .line 426
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->onExpandButtonClick()V

    .line 427
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$700(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->isSelected()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->expendButtonStateChanged(Z)V

    goto :goto_0
.end method
