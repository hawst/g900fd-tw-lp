.class public Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
.super Landroid/widget/LinearLayout;
.source "CustomSearchView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomCloseListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    }
.end annotation


# static fields
.field public static final SEARCH_VIEW_STATE_FLOATING:I = 0x1

.field public static final SEARCH_VIEW_STATE_HIDDEN:I = 0x2

.field public static final SEARCH_VIEW_STATE_ON_TOP:I


# instance fields
.field private final SEARCH_INPUT_MAX_LENGTH:I

.field private final SPEN_USP_LEVEL:I

.field private final TAG:Ljava/lang/String;

.field private bSupportVoiceInput:Z

.field private imm:Landroid/view/inputmethod/InputMethodManager;

.field private mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;

.field private mAutoCompleteAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

.field private mClear:Landroid/widget/ImageView;

.field private mDropDownAnchor:Landroid/view/View;

.field private mDropDownListThreshold:I

.field private mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

.field private mIsRestrictedProfile:Z

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mQueryTextListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;",
            ">;"
        }
    .end annotation
.end field

.field private mRootLayout:Landroid/view/View;

.field private mSearchEditView:Landroid/view/View;

.field private mSearchGroupView:Landroid/view/View;

.field private mSearchProgress:Landroid/widget/ProgressBar;

.field private mTextWatcher:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;

.field private mUserQuery:Ljava/lang/String;

.field private mVoiceRecognize:Landroid/widget/ImageView;

.field private mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

.field private mWritingBuddyShown:Z

.field private final symbolTable:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 142
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 52
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->TAG:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 56
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchGroupView:Landroid/view/View;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchEditView:Landroid/view/View;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    .line 64
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;

    .line 66
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    .line 68
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mClear:Landroid/widget/ImageView;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    .line 74
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mTextWatcher:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownAnchor:Landroid/view/View;

    .line 84
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mAutoCompleteAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .line 93
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddyShown:Z

    .line 95
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/CscFeatureUtil;->getSymbolTableCompareWithCSC()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->symbolTable:Ljava/util/List;

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->bSupportVoiceInput:Z

    .line 102
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mIsRestrictedProfile:Z

    .line 104
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownListThreshold:I

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.spen_usp"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SPEN_USP_LEVEL:I

    .line 143
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->init(Landroid/content/Context;)V

    .line 144
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 147
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->TAG:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 56
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchGroupView:Landroid/view/View;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchEditView:Landroid/view/View;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    .line 64
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;

    .line 66
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    .line 68
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mClear:Landroid/widget/ImageView;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    .line 74
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mTextWatcher:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownAnchor:Landroid/view/View;

    .line 84
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mAutoCompleteAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .line 93
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddyShown:Z

    .line 95
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/CscFeatureUtil;->getSymbolTableCompareWithCSC()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->symbolTable:Ljava/util/List;

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->bSupportVoiceInput:Z

    .line 102
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mIsRestrictedProfile:Z

    .line 104
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownListThreshold:I

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.spen_usp"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SPEN_USP_LEVEL:I

    .line 148
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->init(Landroid/content/Context;)V

    .line 149
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 152
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->TAG:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 56
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchGroupView:Landroid/view/View;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchEditView:Landroid/view/View;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    .line 64
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;

    .line 66
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    .line 68
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mClear:Landroid/widget/ImageView;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    .line 74
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mTextWatcher:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownAnchor:Landroid/view/View;

    .line 84
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mAutoCompleteAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .line 93
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddyShown:Z

    .line 95
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/CscFeatureUtil;->getSymbolTableCompareWithCSC()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->symbolTable:Ljava/util/List;

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->bSupportVoiceInput:Z

    .line 102
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mIsRestrictedProfile:Z

    .line 104
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownListThreshold:I

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.spen_usp"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SPEN_USP_LEVEL:I

    .line 153
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->init(Landroid/content/Context;)V

    .line 154
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddyShown:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->createURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->onQuerySubmit()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->onQueryChanged(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private createURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 157
    if-nez p1, :cond_0

    .line 178
    :goto_0
    return-object v6

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 161
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 162
    .local v4, "nDisplayWidth":I
    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    .line 164
    .local v0, "density":F
    const/4 v5, 0x0

    .line 166
    .local v5, "strEncodedKeyword":Ljava/lang/String;
    :try_start_0
    const-string v7, "UTF-8"

    invoke-static {p1, v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 171
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 173
    .local v1, "deviceName":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "http://www.wolframalpha.com/samsung/?i="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&device="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&mag=2.0"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&width="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&inputsource=equation"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&density="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 178
    .local v6, "url":Ljava/lang/String;
    goto :goto_0

    .line 167
    .end local v1    # "deviceName":Ljava/lang/String;
    .end local v6    # "url":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 168
    .local v2, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method private getDecoratedHint(Landroid/widget/TextView;)Ljava/lang/CharSequence;
    .locals 11
    .param p1, "anchor"    # Landroid/widget/TextView;

    .prologue
    const/4 v10, 0x1

    .line 413
    const/4 v4, 0x0

    .line 415
    .local v4, "ssb":Landroid/text/SpannableStringBuilder;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 417
    .local v2, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 418
    .local v1, "deviceDPI":I
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextSize()F

    move-result v6

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v6, v8

    double-to-int v5, v6

    .line 419
    .local v5, "textSize":I
    const/4 v0, 0x0

    .line 421
    .local v0, "deltaPosition":I
    const/16 v6, 0x280

    if-lt v1, v6, :cond_0

    .line 422
    const/16 v0, -0xf

    .line 427
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f080000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 428
    new-instance v4, Landroid/text/SpannableStringBuilder;

    .end local v4    # "ssb":Landroid/text/SpannableStringBuilder;
    const-string v6, "   "

    invoke-direct {v4, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 429
    .restart local v4    # "ssb":Landroid/text/SpannableStringBuilder;
    invoke-static {v10}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->getFontTypeface(I)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 434
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09003e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 436
    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 438
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0097

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 440
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02008c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 442
    .local v3, "searchIcon":Landroid/graphics/drawable/Drawable;
    const/4 v6, 0x0

    add-int v7, v5, v0

    invoke-virtual {v3, v6, v0, v5, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 443
    new-instance v6, Landroid/text/style/ImageSpan;

    invoke-direct {v6, v3}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 v7, 0x2

    const/16 v8, 0x21

    invoke-virtual {v4, v6, v10, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 445
    return-object v4

    .line 424
    .end local v3    # "searchIcon":Landroid/graphics/drawable/Drawable;
    :cond_0
    const/16 v0, -0xa

    goto :goto_0

    .line 431
    :cond_1
    new-instance v4, Landroid/text/SpannableStringBuilder;

    .end local v4    # "ssb":Landroid/text/SpannableStringBuilder;
    const-string v6, "    "

    invoke-direct {v4, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v4    # "ssb":Landroid/text/SpannableStringBuilder;
    goto :goto_1
.end method

.method private init(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 182
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 183
    .local v0, "res":Landroid/content/res/Resources;
    const-string v1, "input_method"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 184
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 185
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030007

    invoke-virtual {v1, v2, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    .line 187
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 188
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    const v2, 0x7f0b001d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchEditView:Landroid/view/View;

    .line 189
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    const v2, 0x7f0b001e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchGroupView:Landroid/view/View;

    .line 190
    const v1, 0x7f0c0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownListThreshold:I

    .line 192
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchEditView:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchEditView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingStart()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchEditView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchEditView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingStart()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchEditView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 197
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    const v2, 0x7f0b001f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    .line 200
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-direct {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getDecoratedHint(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setHint(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 221
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 250
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$3;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 261
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    const v2, 0x7f090001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setCursorColor(I)V

    .line 262
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I

    invoke-static {p1, v2}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setFilters([Landroid/text/InputFilter;)V

    .line 263
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    const-string v2, "Writingbuddy_UDSModeOn"

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 264
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownListThreshold:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setThreshold(I)V

    .line 265
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    const v2, 0x7f0a037e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setDropDownVerticalOffset(I)V

    .line 268
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getDropDownAnchor()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownAnchor:Landroid/view/View;

    .line 270
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownAnchor:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 271
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mDropDownAnchor:Landroid/view/View;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$4;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 280
    :cond_1
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SPEN_USP_LEVEL:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_2

    .line 281
    new-instance v1, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-direct {v1, v2}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    .line 282
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v1, v7}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setMathWritingEnabled(Z)V

    .line 283
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    const-string v2, "WATCH_ACTION"

    invoke-virtual {v1, v2}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setPrivateCommnad(Ljava/lang/String;)V

    .line 284
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setOnPrivateCommandListner(Lcom/samsung/android/writingbuddy/WritingBuddyImpl$OnPrivateCommandListener;)V

    .line 312
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$6;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setOnTextWritingListener(Lcom/samsung/android/writingbuddy/WritingBuddyImpl$OnTextWritingListener;)V

    .line 339
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$7;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$7;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 350
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mTextWatcher:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;

    .line 351
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mTextWatcher:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 353
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    const v2, 0x7f0b0021

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    .line 354
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$8;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$8;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    const v2, 0x7f0b0022

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mClear:Landroid/widget/ImageView;

    .line 363
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mClear:Landroid/widget/ImageView;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$9;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$9;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 372
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    const v2, 0x7f0b0020

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    .line 374
    invoke-static {p1}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isVoiceInputAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 375
    iput-boolean v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->bSupportVoiceInput:Z

    .line 376
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->reAlignView(Ljava/lang/String;)V

    .line 379
    :cond_3
    invoke-static {p1}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isRestrictedProfile(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 380
    iput-boolean v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mIsRestrictedProfile:Z

    .line 381
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 385
    :cond_4
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->setVisibility(I)V

    .line 386
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->addView(Landroid/view/View;)V

    .line 387
    return-void
.end method

.method private isWritingBuddySymbolQuery(Ljava/lang/String;)Z
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 596
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->symbolTable:Ljava/util/List;

    if-nez v3, :cond_1

    .line 606
    :cond_0
    :goto_0
    return v2

    .line 599
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddyShown:Z

    if-eqz v3, :cond_0

    .line 600
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->symbolTable:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 601
    .local v1, "symbol":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 602
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private onQueryChanged(Ljava/lang/CharSequence;)V
    .locals 5
    .param p1, "newText"    # Ljava/lang/CharSequence;

    .prologue
    .line 467
    if-eqz p1, :cond_2

    .line 468
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 469
    .local v2, "newQuery":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 470
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->TAG:Ljava/lang/String;

    const-string v4, "The new query is same to the previous one."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    .end local v2    # "newQuery":Ljava/lang/String;
    :cond_0
    return-void

    .line 473
    .restart local v2    # "newQuery":Ljava/lang/String;
    :cond_1
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    .line 474
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->reAlignView(Ljava/lang/String;)V

    .line 479
    .end local v2    # "newQuery":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 480
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;

    .line 482
    .local v1, "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->isWritingBuddySymbolQuery(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 483
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;->onWritingBuddyCategoryUpdate(Z)V

    .line 484
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;->onWritingBuddySymbolQuery(Ljava/lang/String;)V

    goto :goto_1

    .line 476
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    goto :goto_0

    .line 486
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;->onQueryTextChange(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method private onQuerySubmit()V
    .locals 3

    .prologue
    .line 449
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 450
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;

    .line 452
    .local v1, "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->isWritingBuddySymbolQuery(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 453
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;->onWritingBuddyCategoryUpdate(Z)V

    .line 454
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;->onWritingBuddySymbolQuery(Ljava/lang/String;)V

    goto :goto_0

    .line 456
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    goto :goto_0

    .line 462
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->dismissDropDown()V

    .line 463
    return-void
.end method

.method private reAlignView(Ljava/lang/String;)V
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 549
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mClear:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 551
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 561
    :goto_0
    return-void

    .line 553
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mClear:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 555
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->bSupportVoiceInput:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mIsRestrictedProfile:Z

    if-nez v0, :cond_1

    .line 556
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 558
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public addOnCustomQueryTextListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;)V
    .locals 1
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;

    .prologue
    .line 493
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    :cond_0
    return-void
.end method

.method public forceDismissDropDown()V
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->forceDismissDropDown()V

    .line 593
    return-void
.end method

.method public getCurrentQueryKeyWord()Ljava/lang/String;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchAutoCompleteAdapter()Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mAutoCompleteAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    return-object v0
.end method

.method public hideKeypad()V
    .locals 3

    .prologue
    .line 397
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 400
    :cond_0
    return-void
.end method

.method public hideProgressBar()V
    .locals 2

    .prologue
    .line 652
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 655
    :cond_0
    return-void
.end method

.method public isQueryKeyWordEmpty()Z
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingKeypad()Z
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWritingBuddyShown()Z
    .locals 1

    .prologue
    .line 642
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddyShown:Z

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 681
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchGroupView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 682
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchGroupView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 685
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v0, :cond_0

    .line 686
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0381

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 688
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchGroupView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 692
    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 693
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 930
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    if-eqz v2, :cond_0

    .line 931
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->forceUpdateDropDownList(Ljava/lang/CharSequence;)V

    .line 934
    :cond_0
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SPEN_USP_LEVEL:I

    const/4 v3, 0x2

    if-lt v2, v3, :cond_1

    .line 935
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddy:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v2}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->isRunning()Z

    move-result v2

    if-nez v2, :cond_1

    .line 936
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 937
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;

    .line 938
    .local v1, "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    const-string v2, "CLOSED"

    invoke-interface {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;->onWritingBuddyMode(Ljava/lang/String;)V

    goto :goto_0

    .line 943
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowVisibilityChanged(I)V

    .line 944
    return-void
.end method

.method public removeTextWatcherAtSearchEditView(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1, "textWatcher"    # Landroid/text/TextWatcher;

    .prologue
    .line 584
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 585
    return-void
.end method

.method public setClearBtn(Z)V
    .locals 3
    .param p1, "isNeedToSet"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 564
    if-eqz p1, :cond_1

    .line 565
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mClear:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 566
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mClear:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 569
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mIsRestrictedProfile:Z

    if-nez v0, :cond_0

    .line 570
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mVoiceRecognize:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setOnClickListenerAtSearchEditView(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 407
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 410
    :cond_0
    return-void
.end method

.method public setOnCustomActionListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;

    .prologue
    .line 499
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;

    .line 500
    return-void
.end method

.method public setQuery(Ljava/lang/String;ZZ)V
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "submit"    # Z
    .param p3, "update"    # Z

    .prologue
    const/4 v2, 0x0

    .line 503
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I

    if-le v0, v1, :cond_0

    .line 504
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 507
    :cond_0
    if-nez p3, :cond_1

    .line 508
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mTextWatcher:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 509
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mUserQuery:Ljava/lang/String;

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 514
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->reAlignView(Ljava/lang/String;)V

    .line 516
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 517
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setSelection(I)V

    .line 519
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getThreshold()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 520
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$10;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->post(Ljava/lang/Runnable;)Z

    .line 533
    :cond_2
    :goto_0
    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 534
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->onQuerySubmit()V

    .line 537
    :cond_3
    if-nez p3, :cond_4

    .line 538
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mTextWatcher:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 540
    :cond_4
    return-void

    .line 530
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setSelection(I)V

    goto :goto_0
.end method

.method public setQueryHint(Ljava/lang/String;)V
    .locals 1
    .param p1, "hint"    # Ljava/lang/String;

    .prologue
    .line 543
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setHint(Ljava/lang/CharSequence;)V

    .line 546
    :cond_0
    return-void
.end method

.method public setSearchAutoCompleteAdapter(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .prologue
    .line 668
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mAutoCompleteAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .line 670
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mAutoCompleteAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 673
    :cond_0
    return-void
.end method

.method public setWidth(I)V
    .locals 2
    .param p1, "pixel"    # I

    .prologue
    .line 658
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 659
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mRootLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 661
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v0, :cond_0

    .line 662
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 665
    .end local v0    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method public showDropDown()V
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->showDropDown()V

    .line 589
    return-void
.end method

.method public showKeypad()V
    .locals 3

    .prologue
    .line 390
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->requestFocus()Z

    .line 392
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 394
    :cond_0
    return-void
.end method

.method public showProgressBar()V
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 649
    :cond_0
    return-void
.end method

.method public stopCursorBlink()V
    .locals 2

    .prologue
    .line 947
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setCursorVisible(Z)V

    .line 948
    return-void
.end method
