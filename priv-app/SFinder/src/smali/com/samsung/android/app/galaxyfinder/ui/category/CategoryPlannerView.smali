.class public Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
.source "CategoryPlannerView.java"


# instance fields
.field private nameOrderSort:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    .line 149
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->nameOrderSort:Ljava/util/Comparator;

    .line 23
    return-void
.end method


# virtual methods
.method protected getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    .locals 7

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    if-nez v0, :cond_0

    .line 34
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v6, "suggestionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->makeCategorizedSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v6

    .line 37
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v0, v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setItems(Ljava/util/ArrayList;)V

    .line 38
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->updateFilteredItems()V

    .line 40
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->getDefaultItemCount()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .line 45
    .end local v6    # "suggestionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    return-object v0
.end method

.method protected getDefaultItemCount()I
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method protected getTemplateLayoutId()I
    .locals 1

    .prologue
    .line 158
    const v0, 0x7f030027

    return v0
.end method

.method protected makeCategorizedSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v2, "categorizedSuggestionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    .line 52
    .local v24, "suggestionSortMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v5, "copyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 56
    .local v9, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v11

    .line 57
    .local v11, "keyValue":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v25

    .line 59
    .local v25, "targetType":Ljava/lang/String;
    if-nez v11, :cond_0

    .line 60
    const-string v11, ""

    .line 63
    :cond_0
    const-string v27, "3"

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 64
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    :cond_1
    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/ArrayList;

    .line 70
    .local v20, "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    if-nez v20, :cond_2

    .line 71
    new-instance v20, Ljava/util/ArrayList;

    .end local v20    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .restart local v20    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    :cond_2
    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v11, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 78
    .end local v9    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v11    # "keyValue":Ljava/lang/String;
    .end local v20    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    .end local v25    # "targetType":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {v24 .. v24}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v21

    .line 80
    .local v21, "sortKeySet":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;->nameOrderSort:Ljava/util/Comparator;

    move-object/from16 v27, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 87
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    .line 89
    .local v22, "sortKeySetSize":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    move/from16 v0, v22

    if-ge v7, v0, :cond_5

    .line 90
    aget-object v23, v21, v7

    check-cast v23, Ljava/lang/String;

    .line 91
    .local v23, "sortKeyValue":Ljava/lang/String;
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/ArrayList;

    .line 93
    .restart local v20    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    const/16 v27, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 95
    .local v4, "categoryHeaderItem":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setIndicationOfFirstItem(Ljava/lang/String;)V

    .line 96
    invoke-virtual {v4, v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setIndicationIndex(I)V

    .line 98
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 99
    .local v10, "itemInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v10, v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setIndicationIndex(I)V

    goto :goto_2

    .line 102
    .end local v10    # "itemInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_4
    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 89
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 105
    .end local v4    # "categoryHeaderItem":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v20    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    .end local v23    # "sortKeyValue":Ljava/lang/String;
    :cond_5
    const/4 v15, 0x0

    .line 107
    .local v15, "resultDate":Ljava/util/Date;
    const/4 v3, 0x0

    .line 108
    .local v3, "category":Ljava/lang/String;
    const/4 v14, 0x0

    .line 110
    .local v14, "result":Ljava/lang/String;
    const/16 v17, 0x0

    .line 111
    .local v17, "sdf":Ljava/text/SimpleDateFormat;
    const-wide/16 v12, 0x0

    .line 112
    .local v12, "milliSec":J
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 114
    .local v19, "size":I
    const/4 v7, 0x0

    move-object/from16 v18, v17

    .end local v17    # "sdf":Ljava/text/SimpleDateFormat;
    .local v18, "sdf":Ljava/text/SimpleDateFormat;
    move-object/from16 v16, v15

    .end local v15    # "resultDate":Ljava/util/Date;
    .local v16, "resultDate":Ljava/util/Date;
    :goto_3
    move/from16 v0, v19

    if-ge v7, v0, :cond_8

    .line 116
    :try_start_0
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v26

    .line 118
    .local v26, "timeInMillis":Ljava/lang/String;
    if-eqz v26, :cond_a

    .line 119
    invoke-static/range {v26 .. v26}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 121
    const-wide/16 v28, 0x0

    cmp-long v27, v12, v28

    if-ltz v27, :cond_a

    .line 122
    new-instance v17, Ljava/text/SimpleDateFormat;

    const-string v27, "EEE, dd MMM yyyy"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    .end local v18    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v17    # "sdf":Ljava/text/SimpleDateFormat;
    :try_start_1
    new-instance v15, Ljava/util/Date;

    invoke-direct {v15, v12, v13}, Ljava/util/Date;-><init>(J)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 125
    .end local v16    # "resultDate":Ljava/util/Date;
    .restart local v15    # "resultDate":Ljava/util/Date;
    :try_start_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v14

    .line 132
    .end local v26    # "timeInMillis":Ljava/lang/String;
    :goto_4
    if-eqz v14, :cond_7

    if-eqz v3, :cond_6

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_7

    .line 134
    :cond_6
    move-object v3, v14

    .line 135
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionGroup(Ljava/lang/String;)V

    .line 114
    :goto_5
    add-int/lit8 v7, v7, 0x1

    move-object/from16 v18, v17

    .end local v17    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v18    # "sdf":Ljava/text/SimpleDateFormat;
    move-object/from16 v16, v15

    .end local v15    # "resultDate":Ljava/util/Date;
    .restart local v16    # "resultDate":Ljava/util/Date;
    goto :goto_3

    .line 128
    :catch_0
    move-exception v6

    move-object/from16 v17, v18

    .end local v18    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v17    # "sdf":Ljava/text/SimpleDateFormat;
    move-object/from16 v15, v16

    .line 129
    .end local v16    # "resultDate":Ljava/util/Date;
    .local v6, "e":Ljava/lang/NumberFormatException;
    .restart local v15    # "resultDate":Ljava/util/Date;
    :goto_6
    invoke-virtual {v6}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_4

    .line 137
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    :cond_7
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionGroup(Ljava/lang/String;)V

    goto :goto_5

    .line 142
    .end local v15    # "resultDate":Ljava/util/Date;
    .end local v17    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v16    # "resultDate":Ljava/util/Date;
    .restart local v18    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_8
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 143
    .restart local v9    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 146
    .end local v9    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_9
    return-object v2

    .line 128
    .end local v18    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v17    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v26    # "timeInMillis":Ljava/lang/String;
    :catch_1
    move-exception v6

    move-object/from16 v15, v16

    .end local v16    # "resultDate":Ljava/util/Date;
    .restart local v15    # "resultDate":Ljava/util/Date;
    goto :goto_6

    :catch_2
    move-exception v6

    goto :goto_6

    .end local v15    # "resultDate":Ljava/util/Date;
    .end local v17    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v16    # "resultDate":Ljava/util/Date;
    .restart local v18    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_a
    move-object/from16 v17, v18

    .end local v18    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v17    # "sdf":Ljava/text/SimpleDateFormat;
    move-object/from16 v15, v16

    .end local v16    # "resultDate":Ljava/util/Date;
    .restart local v15    # "resultDate":Ljava/util/Date;
    goto :goto_4
.end method
