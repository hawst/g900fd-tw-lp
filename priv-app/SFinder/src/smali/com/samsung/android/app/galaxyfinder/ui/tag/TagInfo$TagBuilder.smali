.class public Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;
.super Ljava/lang/Object;
.source "TagInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TagBuilder"
.end annotation


# instance fields
.field private jAttrObject:Lorg/json/JSONObject;

.field private jObject:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->jObject:Lorg/json/JSONObject;

    .line 130
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->jAttrObject:Lorg/json/JSONObject;

    .line 133
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->jObject:Lorg/json/JSONObject;

    .line 134
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->jAttrObject:Lorg/json/JSONObject;

    .line 135
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;)Lorg/json/JSONObject;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->get()Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method private get()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->jObject:Lorg/json/JSONObject;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;
    .locals 4

    .prologue
    .line 168
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->jObject:Lorg/json/JSONObject;

    const-string v2, "tag"

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->jAttrObject:Lorg/json/JSONObject;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_0
    return-object p0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setTagAttr(Ljava/lang/String;Ljava/lang/Object;)Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 138
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 140
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->jAttrObject:Lorg/json/JSONObject;

    invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :cond_0
    :goto_0
    return-object p0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public setTagAttr(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value1"    # Ljava/lang/Object;
    .param p3, "value2"    # Ljava/lang/Object;

    .prologue
    .line 150
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 152
    .local v0, "jValue":Lorg/json/JSONArray;
    invoke-virtual {v0, p2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 153
    invoke-virtual {v0, p3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 155
    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->setTagAttr(Ljava/lang/String;Ljava/lang/Object;)Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;

    move-result-object v1

    return-object v1
.end method
