.class final Lcom/samsung/android/app/galaxyfinder/util/Utils$1;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/util/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$MAX_LENGTH:I

.field final synthetic val$ctx:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/util/Utils$1;->val$ctx:Landroid/content/Context;

    iput p2, p0, Lcom/samsung/android/app/galaxyfinder/util/Utils$1;->val$MAX_LENGTH:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 9
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 460
    # getter for: Lcom/samsung/android/app/galaxyfinder/util/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->access$000()Landroid/widget/Toast;

    move-result-object v3

    if-nez v3, :cond_0

    .line 461
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/util/Utils$1;->val$ctx:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/util/Utils$1;->val$ctx:Landroid/content/Context;

    const v5, 0x7f0e0020

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/samsung/android/app/galaxyfinder/util/Utils$1;->val$MAX_LENGTH:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    # setter for: Lcom/samsung/android/app/galaxyfinder/util/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->access$002(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 466
    :cond_0
    if-nez p2, :cond_1

    if-nez p3, :cond_1

    .line 467
    const/4 v3, 0x0

    .line 499
    :goto_0
    return-object v3

    .line 469
    :cond_1
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v2

    .line 472
    .local v2, "stringSize":I
    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/Utils$1;->val$MAX_LENGTH:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v4

    sub-int v5, p6, p5

    sub-int/2addr v4, v5

    sub-int v1, v3, v4

    .line 474
    .local v1, "keep":I
    if-eqz v2, :cond_2

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/Utils$1;->val$MAX_LENGTH:I

    if-le v3, v2, :cond_2

    .line 479
    if-gtz v1, :cond_2

    .line 480
    # getter for: Lcom/samsung/android/app/galaxyfinder/util/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->access$000()Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 481
    const-string v3, ""

    goto :goto_0

    .line 485
    :cond_2
    if-gtz v1, :cond_3

    .line 486
    # getter for: Lcom/samsung/android/app/galaxyfinder/util/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->access$000()Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 487
    const-string v3, ""

    goto :goto_0

    .line 488
    :cond_3
    sub-int v3, p3, p2

    if-lt v1, v3, :cond_4

    .line 489
    const/4 v3, 0x0

    goto :goto_0

    .line 490
    :cond_4
    sub-int v3, p3, p2

    if-ge v1, v3, :cond_5

    .line 492
    :try_start_0
    # getter for: Lcom/samsung/android/app/galaxyfinder/util/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->access$000()Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 493
    add-int v3, p2, v1

    invoke-interface {p1, p2, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 494
    :catch_0
    move-exception v0

    .line 495
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v3, "Utils"

    const-string v4, "InputFilter IndexOutOfBoundsExce"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    const-string v3, ""

    goto :goto_0

    .line 499
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_5
    const/4 v3, 0x0

    goto :goto_0
.end method
