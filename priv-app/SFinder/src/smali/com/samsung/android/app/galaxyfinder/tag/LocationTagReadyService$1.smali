.class Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;
.super Landroid/database/ContentObserver;
.source "LocationTagReadyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->registerContentObservers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .prologue
    .line 341
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;->onChange(ZLandroid/net/Uri;)V

    .line 342
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 12
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const-wide/32 v10, 0x10000000

    .line 293
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 294
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LocationTagReadyService:onChange() uri = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_0
    if-eqz p2, :cond_1

    .line 297
    invoke-virtual {p2}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v4

    .line 298
    .local v4, "path":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 337
    .end local v4    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 300
    .restart local v4    # "path":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "/"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 304
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getMatchedUriType(Landroid/net/Uri;)Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    invoke-static {v6, p2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$400(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/net/Uri;)Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    move-result-object v5

    .line 305
    .local v5, "type":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->INVALIDTYPE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    if-ne v5, v6, :cond_3

    .line 306
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v6

    const-string v7, "LocationTagReadyService:onChange() getMatchedUriType(uri) is MediaType.INVALIDTYPE"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 310
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getNewContentsID(Landroid/net/Uri;)J
    invoke-static {v6, p2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$500(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/net/Uri;)J

    move-result-wide v0

    .line 311
    .local v0, "id":J
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->name()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getPrefsUpdatedId(Ljava/lang/String;)J
    invoke-static {v6, v7}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$600(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Ljava/lang/String;)J

    move-result-wide v2

    .line 312
    .local v2, "lastUpdatedId":J
    const-wide/16 v6, -0x1

    cmp-long v6, v0, v6

    if-nez v6, :cond_4

    .line 313
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LocationTagReadyService:onChange() There is no data in "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 316
    :cond_4
    and-long v6, v0, v10

    cmp-long v6, v6, v10

    if-nez v6, :cond_6

    .line 318
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LocationTagReadyService:onChange() "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " already has geocode"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    cmp-long v6, v0, v2

    if-gtz v6, :cond_5

    .line 320
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LocationTagReadyService:onChange() id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", lastUpdatedId = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 325
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    sub-long v8, v0, v10

    long-to-int v7, v8

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->sendDirectResponse(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;I)V
    invoke-static {v6, v5, p2, v7}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$700(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;I)V

    goto/16 :goto_0

    .line 327
    :cond_6
    cmp-long v6, v0, v2

    if-gtz v6, :cond_7

    .line 328
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LocationTagReadyService:onChange() "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", lastUpdatedId = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 333
    :cond_7
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LocationTagReadyService:onChange() mediaType : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", uri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->prepareAction(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V
    invoke-static {v6, v5, p2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$800(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V

    goto/16 :goto_0
.end method
