.class Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;
.super Ljava/lang/Object;
.source "LocationTagReadyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConvertTask"
.end annotation


# instance fields
.field private mRequestType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

.field private mSelection:Ljava/lang/String;

.field private mSelectionArgs:[Ljava/lang/String;

.field private mTargetUri:Landroid/net/Uri;

.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p2, "requestType"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 894
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->this$1:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 885
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mRequestType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    .line 887
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mTargetUri:Landroid/net/Uri;

    .line 889
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mSelection:Ljava/lang/String;

    .line 891
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mSelectionArgs:[Ljava/lang/String;

    .line 896
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mRequestType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    .line 897
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mTargetUri:Landroid/net/Uri;

    .line 898
    iput-object p4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mSelection:Ljava/lang/String;

    .line 899
    iput-object p5, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mSelectionArgs:[Ljava/lang/String;

    .line 900
    return-void
.end method


# virtual methods
.method public getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .locals 1

    .prologue
    .line 906
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mRequestType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    return-object v0
.end method

.method public getSelection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mSelection:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectionArgs()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mSelectionArgs:[Ljava/lang/String;

    return-object v0
.end method

.method public getTargetUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 913
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->mTargetUri:Landroid/net/Uri;

    return-object v0
.end method
