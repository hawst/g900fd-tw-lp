.class public Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "SearchEnginePhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SuggestAsyncQueryHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;
    }
.end annotation


# instance fields
.field private mQuery:Ljava/lang/String;

.field private mQueryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Landroid/content/ContentResolver;)V
    .locals 1
    .param p2, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 531
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    .line 532
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 527
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->mQuery:Ljava/lang/String;

    .line 529
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->mQueryList:Ljava/util/ArrayList;

    .line 533
    return-void
.end method


# virtual methods
.method public addWorker(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "token"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "selection"    # Ljava/lang/String;
    .param p6, "selectionArgs"    # [Ljava/lang/String;
    .param p7, "orderBy"    # Ljava/lang/String;

    .prologue
    .line 541
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;-><init>(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;)V

    .line 543
    .local v0, "args":Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;
    iput-object p0, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->handler:Landroid/os/Handler;

    .line 544
    iput-object p3, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->uri:Landroid/net/Uri;

    .line 545
    iput-object p4, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->projection:[Ljava/lang/String;

    .line 546
    iput-object p5, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->selection:Ljava/lang/String;

    .line 547
    iput-object p6, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->selectionArgs:[Ljava/lang/String;

    .line 548
    iput-object p7, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->orderBy:Ljava/lang/String;

    .line 549
    iput-object p2, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->cookie:Ljava/lang/Object;

    .line 551
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->mQueryList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    return-void
.end method

.method public execute()V
    .locals 10

    .prologue
    .line 555
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->mQueryList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->mQueryList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 556
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->mQueryList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;

    .line 557
    .local v8, "args":Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;
    const/4 v1, 0x0

    iget-object v2, v8, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->cookie:Ljava/lang/Object;

    iget-object v3, v8, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v4, v8, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v5, v8, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->selection:Ljava/lang/String;

    iget-object v6, v8, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v7, v8, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;->orderBy:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 561
    .end local v8    # "args":Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler$QueryArgs;
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->mQueryList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 562
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryRequiredTime:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->access$002(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;J)J

    .line 564
    .end local v9    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 10
    .param p1, "token"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 568
    const/4 v1, 0x0

    .local v1, "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    move-object v2, p2

    .line 569
    check-cast v2, Landroid/app/SearchableInfo;

    .line 570
    .local v2, "searchable":Landroid/app/SearchableInfo;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->getAppLog(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->access$100(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 572
    .local v0, "APP_TAG":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SearchEngine"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[PERFORMANCE] onQueryComplete() TIMEINFO(QueryTime) is ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryRequiredTime:J
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->access$000(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    if-eqz p3, :cond_2

    .line 576
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SearchEngine"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onQueryComplete() search result size ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->mQuery:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeCategory(Ljava/lang/String;Landroid/app/SearchableInfo;Landroid/database/Cursor;)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-static {v3, v4, v2, p3}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->access$200(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Ljava/lang/String;Landroid/app/SearchableInfo;Landroid/database/Cursor;)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v1

    .line 583
    if-nez v1, :cond_3

    .line 584
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SearchEngine"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "onQueryComplete() returns null cursor"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    if-eqz v3, :cond_0

    .line 591
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v4, v4, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-interface {v3, v4, v1}, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;->onReceiveSearchResponse(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;Ljava/lang/Object;)V

    .line 594
    :cond_0
    if-eqz p3, :cond_1

    .line 595
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 598
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    # setter for: Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryRequiredTime:J
    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->access$002(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;J)J

    .line 599
    return-void

    .line 578
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SearchEngine"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "onQueryComplete() search result cursor has NULL value"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 586
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v4, v4, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getResultCount()I

    move-result v4

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->setResultCount(I)V

    goto :goto_1
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 536
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->mQuery:Ljava/lang/String;

    .line 537
    return-void
.end method
