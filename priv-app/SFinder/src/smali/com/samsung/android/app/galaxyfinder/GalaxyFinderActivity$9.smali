.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;
.super Ljava/lang/Object;
.source "GalaxyFinderActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I

    .prologue
    const/4 v2, 0x1

    .line 583
    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1300()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->isDataConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 586
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const-string v1, "show_data_info"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->setSettings(Landroid/content/Context;Ljava/lang/String;I)V

    .line 598
    :cond_0
    :goto_0
    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->ENG_MODE:Z
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1400()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 599
    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->VERIFICATION_LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "init - data popup ok"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->initContext(Landroid/content/Context;)V

    .line 602
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->create(Landroid/content/Context;)Lcom/nuance/nmdp/speechkit/SpeechKit;

    .line 604
    return-void

    .line 590
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->isWifiConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const-string v1, "show_wifi_info"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->setSettings(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method
