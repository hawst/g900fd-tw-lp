.class public Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;
.super Ljava/lang/Object;
.source "CustomSearchView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CustomTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V
    .locals 0

    .prologue
    .line 609
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 632
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$600(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)I

    move-result v1

    if-le v0, v1, :cond_0

    .line 638
    :goto_0
    return-void

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->onQueryChanged(Ljava/lang/CharSequence;)V
    invoke-static {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$700(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    .line 613
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$600(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 617
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    .line 621
    if-ne p3, p4, :cond_1

    .line 628
    :cond_0
    :goto_0
    return-void

    .line 625
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$CustomTextWatcher;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->SEARCH_INPUT_MAX_LENGTH:I
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$600(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)I

    move-result v1

    if-lt v0, v1, :cond_0

    goto :goto_0
.end method
