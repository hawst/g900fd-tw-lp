.class public interface abstract Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
.super Ljava/lang/Object;
.source "TagProbe.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;,
        Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;,
        Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;
    }
.end annotation


# static fields
.field public static final HANDLER_DELAY_5SEC:J = 0x1388L

.field public static final MAX_TAGPROBE_THREAD_POOL:I = 0x14

.field public static final TAG_COLLECT_REQUEST:I = 0x0

.field public static final TAG_COLLECT_RESPONSE:I = 0x1


# virtual methods
.method public abstract collect(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;)Ljava/lang/Object;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract monitor(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;)V
.end method

.method public abstract probe(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)V
.end method

.method public abstract requestFullSyncTagProbe(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;)V
.end method

.method public abstract stop()V
.end method

.method public abstract store(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;Ljava/lang/Object;)Z
.end method
