.class public Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SharableAppsListPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SharableAppsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;Landroid/content/Context;I)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    .line 104
    invoke-direct {p0, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 106
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 107
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->mAppList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 120
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f030053

    const/4 v7, 0x0

    invoke-virtual {v5, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 122
    .local v4, "v":Landroid/view/View;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    # getter for: Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->access$000(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Landroid/content/pm/PackageManager;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 123
    const v5, 0x7f0b00b5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 124
    .local v1, "appName":Landroid/widget/TextView;
    const v5, 0x7f0b00b4

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 126
    .local v0, "appIcon":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    # getter for: Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v6}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->access$000(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    # getter for: Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v6}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->access$000(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 129
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 130
    .local v3, "packageName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 132
    .local v2, "className":Ljava/lang/String;
    new-instance v5, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;

    invoke-direct {v5, p0, v3, v2}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;-><init>(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    .end local v0    # "appIcon":Landroid/widget/ImageView;
    .end local v1    # "appName":Landroid/widget/TextView;
    .end local v2    # "className":Ljava/lang/String;
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_0
    return-object v4
.end method

.method public setAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "mitems":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->mAppList:Ljava/util/List;

    .line 111
    return-void
.end method
