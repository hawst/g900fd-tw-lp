.class public Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;
.super Ljava/lang/Object;
.source "SpeechKitWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;
    }
.end annotation


# static fields
.field public static final SPEECHKIT_LANGUAGE_CODE_CHINESE:Ljava/lang/String; = "cmn-CHN"

.field public static final SPEECHKIT_LANGUAGE_CODE_ENGLISH_UK:Ljava/lang/String; = "eng-GBR"

.field public static final SPEECHKIT_LANGUAGE_CODE_ENGLISH_US:Ljava/lang/String; = "eng-USA"

.field public static final SPEECHKIT_LANGUAGE_CODE_FRENCH:Ljava/lang/String; = "fra-FRA"

.field public static final SPEECHKIT_LANGUAGE_CODE_GERMAN:Ljava/lang/String; = "deu-DEU"

.field public static final SPEECHKIT_LANGUAGE_CODE_ITALIAN:Ljava/lang/String; = "ita-ITA"

.field public static final SPEECHKIT_LANGUAGE_CODE_JAPANESE:Ljava/lang/String; = "jpn-JPN"

.field public static final SPEECHKIT_LANGUAGE_CODE_KOREAN:Ljava/lang/String; = "kor-KOR"

.field public static final SPEECHKIT_LANGUAGE_CODE_PORTUGUESE_BR:Ljava/lang/String; = "por-BRA"

.field public static final SPEECHKIT_LANGUAGE_CODE_SPANISH:Ljava/lang/String; = "spa-XLA"

.field private static final SSL_VALUE:Z = true

.field private static SpeechKitAppId:Ljava/lang/String; = null

.field private static final SpeechKitApplicationKey:[B

.field private static final SpeechKitApplicationKeySsl:[B

.field private static final SpeechKitPort:I = 0x1bb

.field private static SpeechKitSsl:Z

.field private static sCurrentLanguage:Ljava/lang/String;

.field private static sIsReleased:Z

.field private static sListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x40

    .line 38
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->SpeechKitSsl:Z

    .line 40
    const-string v0, "Samsung_Android_finderSSL_20140227"

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->SpeechKitAppId:Ljava/lang/String;

    .line 42
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->SpeechKitApplicationKeySsl:[B

    .line 55
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->SpeechKitApplicationKey:[B

    return-void

    .line 42
    :array_0
    .array-data 1
        -0x4ct
        -0x3et
        0x6bt
        0x64t
        0x49t
        -0x19t
        0x40t
        -0x18t
        -0x47t
        -0x12t
        0x47t
        -0x4et
        0x69t
        0x55t
        -0x1ft
        0x7ft
        0x60t
        -0xet
        -0x64t
        -0x4at
        0x1dt
        0x56t
        0x4ct
        0x6bt
        -0x77t
        0x4ft
        -0x33t
        -0x7t
        -0x64t
        -0x11t
        -0x6bt
        -0x77t
        0x2dt
        -0x23t
        0x71t
        -0x9t
        0xdt
        0x14t
        0x4dt
        -0x78t
        -0x41t
        0x68t
        0x7dt
        0x3at
        0x15t
        -0x41t
        -0x63t
        0x52t
        0x3et
        -0x48t
        0x3t
        -0xct
        -0x2ct
        -0x68t
        0x4et
        -0x72t
        -0x5at
        -0x50t
        -0x39t
        0x5ft
        -0x7at
        -0x2t
        -0xft
        0x56t
    .end array-data

    .line 55
    :array_1
    .array-data 1
        -0x55t
        0x60t
        -0x6bt
        0x41t
        0x76t
        0x55t
        -0x43t
        0x48t
        0x4at
        -0x13t
        0x69t
        -0x4at
        -0x1ft
        0x64t
        -0xat
        -0x3ct
        -0x72t
        0x70t
        -0x1et
        -0x15t
        0x5dt
        -0x6bt
        -0x6ft
        0x4et
        0x78t
        -0x49t
        0x4et
        0x77t
        -0x34t
        -0x10t
        -0x71t
        0x46t
        -0x15t
        -0x5ct
        0x7dt
        0x25t
        0x33t
        -0x2ft
        -0x4et
        -0x80t
        0x30t
        -0x55t
        0x43t
        0x74t
        0x3et
        0x67t
        0x1t
        -0x3bt
        0x72t
        0x57t
        -0x4t
        0x15t
        -0x45t
        0x36t
        0x48t
        0x53t
        0x43t
        -0x3bt
        0x3t
        0x71t
        0x1at
        -0x6at
        -0x6bt
        0x59t
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    return-void
.end method

.method public static addOnSpeechKitReleasedListener(Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;

    .prologue
    .line 210
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    :cond_0
    return-void
.end method

.method public static declared-synchronized create(Landroid/content/Context;)Lcom/nuance/nmdp/speechkit/SpeechKit;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    const-class v7, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;

    monitor-enter v7

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    if-nez v0, :cond_3

    .line 105
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sCurrentLanguage:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 112
    const-string v0, "en-US"

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sCurrentLanguage:Ljava/lang/String;

    .line 115
    :cond_0
    const-string v2, "gh.nvc.eng-usa.nuancemobility.net"

    .line 122
    .local v2, "server":Ljava/lang/String;
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sCurrentLanguage:Ljava/lang/String;

    const-string v1, "en-US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 123
    const-string v2, "gh.nvc.eng-usa.nuancemobility.net"

    .line 140
    :cond_1
    :goto_0
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->SpeechKitAppId:Ljava/lang/String;

    const/16 v3, 0x1bb

    sget-boolean v4, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->SpeechKitSsl:Z

    sget-object v5, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->SpeechKitApplicationKeySsl:[B

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/nuance/nmdp/speechkit/SpeechKit;->initialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ[B)Lcom/nuance/nmdp/speechkit/SpeechKit;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    .line 142
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/SpeechKit;->connect()V

    .line 146
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/SpeechKit;->defineAudioPrompt(I)Lcom/nuance/nmdp/speechkit/Prompt;

    move-result-object v6

    .line 149
    .local v6, "beepStop":Lcom/nuance/nmdp/speechkit/Prompt;
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v6, v3, v4}, Lcom/nuance/nmdp/speechkit/SpeechKit;->setDefaultRecognizerPrompts(Lcom/nuance/nmdp/speechkit/Prompt;Lcom/nuance/nmdp/speechkit/Prompt;Lcom/nuance/nmdp/speechkit/Prompt;Lcom/nuance/nmdp/speechkit/Prompt;)V

    .line 152
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    .line 156
    :cond_2
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sIsReleased:Z

    .line 159
    .end local v2    # "server":Ljava/lang/String;
    .end local v6    # "beepStop":Lcom/nuance/nmdp/speechkit/Prompt;
    :cond_3
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v7

    return-object v0

    .line 127
    .restart local v2    # "server":Ljava/lang/String;
    :cond_4
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sCurrentLanguage:Ljava/lang/String;

    const-string v1, "ko-KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 128
    const-string v2, "gh.nvc.kor-kor.nuancemobility.net"

    goto :goto_0

    .line 132
    :cond_5
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sCurrentLanguage:Ljava/lang/String;

    const-string v1, "zh-CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    const-string v2, "gh.nvc.cmn-chn.nuancemobility.net"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 103
    .end local v2    # "server":Ljava/lang/String;
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public static declared-synchronized destroy()V
    .locals 2

    .prologue
    .line 163
    const-class v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sIsReleased:Z

    .line 166
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/SpeechKit;->release()V

    .line 167
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->notifySpeechKitReased()V

    .line 169
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    .line 171
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sCurrentLanguage:Ljava/lang/String;

    .line 173
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 174
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 175
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    :cond_0
    monitor-exit v1

    return-void

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getInstance()Lcom/nuance/nmdp/speechkit/SpeechKit;
    .locals 3

    .prologue
    .line 181
    const-class v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "SpeechKitWrapper.create shoul be called."

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 185
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized isReleased()Z
    .locals 2

    .prologue
    .line 189
    const-class v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sIsReleased:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static notifySpeechKitReased()V
    .locals 3

    .prologue
    .line 222
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 230
    .local v0, "i":I
    .local v1, "sListenersSize":I
    :cond_0
    return-void

    .line 226
    .end local v0    # "i":I
    .end local v1    # "sListenersSize":I
    :cond_1
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 227
    .restart local v1    # "sListenersSize":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 228
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;

    invoke-interface {v2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;->onSpeechKitReleased()V

    .line 227
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static removeOnSpeechKitReleasedListener(Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;

    .prologue
    .line 216
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 219
    :cond_0
    return-void
.end method

.method public static declared-synchronized setCurrentLanguage(Ljava/lang/String;)V
    .locals 2
    .param p0, "language"    # Ljava/lang/String;

    .prologue
    .line 193
    const-class v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sCurrentLanguage:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sCurrentLanguage:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 194
    :cond_0
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sIsReleased:Z

    if-nez v0, :cond_1

    .line 195
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    if-eqz v0, :cond_1

    .line 196
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sIsReleased:Z

    .line 198
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/SpeechKit;->release()V

    .line 199
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->notifySpeechKitReased()V

    .line 201
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sSpeechKit:Lcom/nuance/nmdp/speechkit/SpeechKit;

    .line 205
    :cond_1
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->sCurrentLanguage:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    :cond_2
    monitor-exit v1

    return-void

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
