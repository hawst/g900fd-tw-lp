.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;
.super Landroid/database/DataSetObserver;
.source "SearchListLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

.field final synthetic val$mFooterView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->val$mFooterView:Landroid/view/View;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 357
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 358
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V

    .line 370
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 371
    return-void

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getHistoryDeleteAllThreshHold()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 361
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->val$mFooterView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    goto :goto_0

    .line 365
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;->val$mFooterView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    goto :goto_0
.end method
