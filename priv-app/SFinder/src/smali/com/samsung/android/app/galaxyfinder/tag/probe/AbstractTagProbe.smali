.class public abstract Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;
.super Ljava/lang/Object;
.source "AbstractTagProbe.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;,
        Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$ProcessHandler;
    }
.end annotation


# static fields
.field protected static final DB_COMMIT_CONTENTS_PER_ONCE:I = 0x12c

.field protected static final INVALID_CONTENT_ID:I = -0x1


# instance fields
.field private APP_TAG:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private bWaitingDelayedMessage:Z

.field protected mContentResolver:Landroid/content/ContentResolver;

.field protected mContext:Landroid/content/Context;

.field private mDbObserver:Landroid/database/ContentObserver;

.field private mExecuteService:Ljava/util/concurrent/ExecutorService;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mMainHandler:Landroid/os/Handler;

.field private mStateListener:Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;

.field protected mSupportTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/Constants$TagType;",
            ">;"
        }
    .end annotation
.end field

.field protected mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

.field private mWaitingDelayedObject:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagType"    # [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .param p3, "attr"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-class v6, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->TAG:Ljava/lang/String;

    .line 27
    const-string v6, ""

    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->APP_TAG:Ljava/lang/String;

    .line 37
    iput-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mHandlerThread:Landroid/os/HandlerThread;

    .line 39
    iput-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    .line 43
    iput-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mStateListener:Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;

    .line 46
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->bWaitingDelayedMessage:Z

    .line 48
    iput-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mWaitingDelayedObject:Ljava/lang/Object;

    .line 50
    iput-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mDbObserver:Landroid/database/ContentObserver;

    .line 89
    if-eqz p2, :cond_0

    array-length v6, p2

    if-gtz v6, :cond_1

    .line 90
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "tagType is incorrect"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 92
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    if-nez v6, :cond_3

    .line 93
    :cond_2
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "DbAttribute is incorrect"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 96
    :cond_3
    :try_start_0
    invoke-virtual {p3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getSearchableInfo()Landroid/app/SearchableInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\\."

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 97
    .local v5, "splitPackage":[Ljava/lang/String;
    if-eqz v5, :cond_4

    array-length v6, v5

    if-lez v6, :cond_4

    .line 98
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v5

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v5, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->TAG:Ljava/lang/String;

    .line 99
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v5

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v5, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->APP_TAG:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    .end local v5    # "splitPackage":[Ljava/lang/String;
    :cond_4
    :goto_0
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mContext:Landroid/content/Context;

    .line 105
    new-instance v6, Ljava/util/ArrayList;

    array-length v7, p2

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mSupportTagList:Ljava/util/ArrayList;

    .line 106
    move-object v1, p2

    .local v1, "arr$":[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_5

    aget-object v0, v1, v2

    .line 107
    .local v0, "aTagType":Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mSupportTagList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 101
    .end local v0    # "aTagType":Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .end local v1    # "arr$":[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :catch_0
    move-exception v4

    .line 102
    .local v4, "npe":Ljava/lang/NullPointerException;
    invoke-virtual {v4}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 109
    .end local v4    # "npe":Ljava/lang/NullPointerException;
    .restart local v1    # "arr$":[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_5
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    .line 110
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->init()V

    .line 111
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->bWaitingDelayedMessage:Z

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mWaitingDelayedObject:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private convertContactIdToName(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 252
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 253
    .local v1, "myPhoneUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 254
    .local v6, "displayName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 256
    .local v9, "phoneCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 258
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 259
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 260
    const-string v0, "display_name"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 262
    .local v8, "nameIdx":I
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 269
    .end local v8    # "nameIdx":I
    :cond_0
    if-eqz v9, :cond_1

    .line 270
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 271
    const/4 v9, 0x0

    .line 274
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "convertContactIdToName() "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v6, :cond_3

    move-object v0, v6

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    return-object v6

    .line 264
    :catch_0
    move-exception v10

    .line 265
    .local v10, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269
    if-eqz v9, :cond_1

    .line 270
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 271
    const/4 v9, 0x0

    goto :goto_0

    .line 266
    .end local v10    # "se":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v7

    .line 267
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 269
    if-eqz v9, :cond_1

    .line 270
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 271
    const/4 v9, 0x0

    goto :goto_0

    .line 269
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_2

    .line 270
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 271
    const/4 v9, 0x0

    :cond_2
    throw v0

    .line 274
    :cond_3
    const-string v0, "No display name"

    goto :goto_1
.end method

.method protected static getID(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 280
    const/16 v0, 0x2f

    .line 281
    .local v0, "delimeter":C
    const/4 v1, 0x0

    .line 282
    .local v1, "id":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 283
    const/16 v2, 0x2f

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 285
    :cond_0
    return-object v1
.end method

.method private init()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    .line 115
    new-instance v0, Landroid/os/HandlerThread;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mHandlerThread:Landroid/os/HandlerThread;

    .line 116
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 117
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$ProcessHandler;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$ProcessHandler;-><init>(Landroid/os/Looper;Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    .line 118
    const/16 v0, 0x14

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mExecuteService:Ljava/util/concurrent/ExecutorService;

    .line 119
    return-void
.end method

.method private registerContentObserver()V
    .locals 4

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mDbObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 132
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$1;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mDbObserver:Landroid/database/ContentObserver;

    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->isNotifyForDescendents()Z

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mDbObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Register observer] registerContentObserver() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    return-void
.end method


# virtual methods
.method protected convert(Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "tagType"    # Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 235
    move-object v0, p2

    .line 236
    .local v0, "convertedResult":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 237
    const/4 p2, 0x0

    .line 248
    .end local p2    # "tag":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p2

    .line 239
    .restart local p2    # "tag":Ljava/lang/String;
    :cond_1
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 241
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->USERDEF:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 243
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->PEOPLE:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 244
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->convertContactIdToName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object p2, v0

    .line 248
    goto :goto_0

    .line 246
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "convert() There is no matched tag type, tagType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getLogTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->APP_TAG:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 294
    .local v0, "b":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mSupportTagList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    .line 295
    .local v2, "type":Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    const/16 v3, 0x5f

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 297
    .end local v2    # "type":Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method protected abstract getRequest(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;
.end method

.method public monitor(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;)V
    .locals 1
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->registerContentObserver()V

    .line 125
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->setProbeProcessListener(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;)V

    .line 127
    :cond_0
    return-void
.end method

.method protected abstract onChangeInternal(Landroid/net/Uri;)V
.end method

.method protected prepare(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 166
    invoke-virtual {p0, p1, p2, p1, p2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->prepare(JJ)V

    .line 167
    return-void
.end method

.method protected prepare(JJ)V
    .locals 19
    .param p1, "sid"    # J
    .param p3, "eid"    # J

    .prologue
    .line 170
    move-wide/from16 v4, p1

    .line 171
    .local v4, "start":J
    move-wide/from16 v6, p3

    .line 172
    .local v6, "end":J
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->bWaitingDelayedMessage:Z

    if-eqz v2, :cond_2

    .line 173
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mWaitingDelayedObject:Ljava/lang/Object;

    invoke-virtual {v2, v3, v15}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v14

    .line 174
    .local v14, "msg":Landroid/os/Message;
    if-eqz v14, :cond_2

    .line 175
    iget-object v2, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 176
    iget-object v12, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v12, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    .line 177
    .local v12, "cInfo":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->getStartId()J

    move-result-wide v10

    .line 178
    .local v10, "beforeStartId":J
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->getEndId()J

    move-result-wide v8

    .line 179
    .local v8, "beforeEndId":J
    cmp-long v2, p1, v10

    if-lez v2, :cond_0

    .line 180
    move-wide v4, v10

    .line 182
    :cond_0
    cmp-long v2, p3, v8

    if-gez v2, :cond_1

    .line 183
    move-wide v6, v8

    .line 186
    .end local v8    # "beforeEndId":J
    .end local v10    # "beforeStartId":J
    .end local v12    # "cInfo":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 189
    .end local v14    # "msg":Landroid/os/Message;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v13

    .line 190
    .local v13, "message":Landroid/os/Message;
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;JJ)V

    iput-object v2, v13, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    const-wide/16 v16, 0x1388

    move-wide/from16 v0, v16

    invoke-virtual {v2, v13, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 192
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->bWaitingDelayedMessage:Z

    .line 193
    iget-object v2, v13, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mWaitingDelayedObject:Ljava/lang/Object;

    .line 194
    return-void
.end method

.method public probe(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    .prologue
    .line 198
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->getRequest(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

    move-result-object v0

    .line 199
    .local v0, "req":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;
    if-eqz v0, :cond_0

    .line 200
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mExecuteService:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 202
    :cond_0
    return-void
.end method

.method public requestFullSyncTagProbe(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;)V
    .locals 8
    .param p1, "reqType"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 150
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->bWaitingDelayedMessage:Z

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    .line 152
    .local v7, "msg":Landroid/os/Message;
    if-eqz v7, :cond_0

    .line 153
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 156
    .end local v7    # "msg":Landroid/os/Message;
    :cond_0
    new-instance v6, Landroid/os/Message;

    invoke-direct {v6}, Landroid/os/Message;-><init>()V

    .line 157
    .local v6, "message":Landroid/os/Message;
    iput v1, v6, Landroid/os/Message;->what:I

    .line 158
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    move-object v1, p1

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;JJ)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 159
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->bWaitingDelayedMessage:Z

    .line 161
    return-void
.end method

.method protected setProbeProcessListener(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mStateListener:Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;

    .line 206
    return-void
.end method

.method public stop()V
    .locals 0

    .prologue
    .line 229
    return-void
.end method
