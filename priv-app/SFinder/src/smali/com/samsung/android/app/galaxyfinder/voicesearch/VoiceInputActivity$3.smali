.class Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;
.super Ljava/lang/Object;
.source "VoiceInputActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->onError(Lcom/nuance/nmdp/speechkit/Recognizer;Lcom/nuance/nmdp/speechkit/SpeechError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

.field final synthetic val$message:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;->val$message:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 378
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    # invokes: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setState(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$200(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V

    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$100(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;->val$message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$300(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonDivider:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$400(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 383
    return-void
.end method
