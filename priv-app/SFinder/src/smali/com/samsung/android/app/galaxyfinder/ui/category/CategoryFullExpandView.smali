.class public Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;
.super Landroid/widget/LinearLayout;
.source "CategoryFullExpandView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;
    }
.end annotation


# static fields
.field private static final CATEGORY_VIEW_TYPE_GRID:Ljava/lang/String; = "grid"

.field private static final CATEGORY_VIEW_TYPE_MIX:Ljava/lang/String; = "mix"

.field private static final SUB_TYPE_GRID:I = 0x3


# instance fields
.field private final INVALID_RES_ID:I

.field private final MAX_COLUMN_COUNT:I

.field private mAttrIdColumnHeight:I

.field private mAttrIdColumnWidth:I

.field private mAttrIdHorizontalSpacing:I

.field private mAttrIdMarginLeft:I

.field private mAttrIdMarginRight:I

.field private mAttrIdVerticalSpacing:I

.field private mColumnCnt:I

.field private mColumnHeight:I

.field private mColumnWidth:I

.field private mContext:Landroid/content/Context;

.field private mCurOrientation:I

.field private mHorizontalSpacing:I

.field mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

.field private mListType:Ljava/lang/String;

.field private mMarginBottom:I

.field private mMarginLeft:I

.field private mMarginRight:I

.field private mMarginTop:I

.field private mMaxItemCntInList:I

.field private mMaxItemCntIntGrid:I

.field private mResources:Landroid/content/res/Resources;

.field private mVerticalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    const/16 v0, 0x19

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->MAX_COLUMN_COUNT:I

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->INVALID_RES_ID:I

    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/16 v0, 0x19

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->MAX_COLUMN_COUNT:I

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->INVALID_RES_ID:I

    .line 84
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const/16 v0, 0x19

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->MAX_COLUMN_COUNT:I

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->INVALID_RES_ID:I

    .line 89
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    return-void
.end method

.method private addItemGridViewType(II)V
    .locals 8
    .param p1, "oldCnt"    # I
    .param p2, "curCnt"    # I

    .prologue
    .line 652
    sub-int v4, p2, p1

    .line 653
    .local v4, "remainAddItem":I
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v2

    .line 655
    .local v2, "last":I
    const/4 v1, 0x0

    .line 657
    .local v1, "child":Landroid/view/View;
    sub-int v5, p2, v2

    iget v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntIntGrid:I

    if-ge v5, v6, :cond_0

    .line 658
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    move v3, p1

    .line 663
    .end local p1    # "oldCnt":I
    .local v3, "oldCnt":I
    :goto_0
    if-lez v4, :cond_2

    .line 664
    const/4 v0, 0x1

    .line 665
    .local v0, "bAddNewRow":Z
    add-int/lit8 p1, v3, 0x1

    .end local v3    # "oldCnt":I
    .restart local p1    # "oldCnt":I
    if-nez v0, :cond_1

    const/4 v5, 0x1

    :goto_1
    invoke-direct {p0, v3, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getNewItemGridType(IZ)Landroid/view/View;

    move-result-object v1

    .line 666
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addView(Landroid/view/View;)V

    .line 667
    add-int/lit8 v4, v4, -0x1

    move v3, p1

    .line 668
    .end local p1    # "oldCnt":I
    .restart local v3    # "oldCnt":I
    goto :goto_0

    .line 660
    .end local v0    # "bAddNewRow":Z
    .end local v3    # "oldCnt":I
    .restart local p1    # "oldCnt":I
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntIntGrid:I

    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    mul-int/2addr v6, v7

    add-int/2addr v6, v2

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    move v3, p1

    .end local p1    # "oldCnt":I
    .restart local v3    # "oldCnt":I
    goto :goto_0

    .line 665
    .end local v3    # "oldCnt":I
    .restart local v0    # "bAddNewRow":Z
    .restart local p1    # "oldCnt":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 669
    .end local v0    # "bAddNewRow":Z
    .end local p1    # "oldCnt":I
    .restart local v3    # "oldCnt":I
    :cond_2
    return-void
.end method

.method private addItemListViewType(II)V
    .locals 6
    .param p1, "oldCnt"    # I
    .param p2, "curCnt"    # I

    .prologue
    const/4 v5, 0x0

    .line 633
    if-le p2, p1, :cond_2

    .line 634
    const/4 v0, 0x0

    .line 635
    .local v0, "child":Landroid/view/View;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v2

    .line 636
    .local v2, "last":I
    add-int/lit8 v3, v2, 0xa

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 637
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntInList:I

    add-int/2addr v4, v2

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 641
    :goto_0
    move v1, p1

    .local v1, "i":I
    :goto_1
    if-ge v1, p2, :cond_2

    .line 642
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v3, v1, v5, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 644
    if-eqz v0, :cond_0

    .line 645
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addView(Landroid/view/View;)V

    .line 641
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 639
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    goto :goto_0

    .line 649
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "last":I
    :cond_2
    return-void
.end method

.method private addItemMixViewType(II)V
    .locals 8
    .param p1, "oldCnt"    # I
    .param p2, "curCnt"    # I

    .prologue
    const/4 v7, 0x0

    .line 672
    if-le p2, p1, :cond_3

    .line 673
    const/4 v0, 0x0

    .line 674
    .local v0, "child":Landroid/view/View;
    const/4 v4, 0x0

    .line 675
    .local v4, "subType":I
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v1

    .line 676
    .local v1, "first":I
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v3

    .line 678
    .local v3, "last":I
    sub-int v5, v3, v1

    const/16 v6, 0xa

    if-ge v5, v6, :cond_0

    .line 679
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntInList:I

    add-int/2addr v6, v1

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 682
    :cond_0
    move v2, p1

    .local v2, "i":I
    :goto_0
    if-ge v2, p2, :cond_3

    .line 683
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getViewSubType(I)I

    move-result v4

    .line 685
    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 686
    const/4 v5, 0x0

    invoke-direct {p0, v2, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getNewItemGridType(IZ)Landroid/view/View;

    move-result-object v0

    .line 691
    :goto_1
    if-eqz v0, :cond_1

    .line 692
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addView(Landroid/view/View;)V

    .line 682
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 688
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5, v2, v7, v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 696
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "first":I
    .end local v2    # "i":I
    .end local v3    # "last":I
    .end local v4    # "subType":I
    :cond_3
    return-void
.end method

.method private findImageViewAndRelease(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f090037

    .line 826
    instance-of v5, p1, Landroid/view/ViewGroup;

    if-eqz v5, :cond_0

    move-object v4, p1

    .line 827
    check-cast v4, Landroid/view/ViewGroup;

    .line 828
    .local v4, "vg":Landroid/view/ViewGroup;
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 829
    .local v0, "childCnt":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 830
    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->findImageViewAndRelease(Landroid/view/View;)V

    .line 829
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 832
    .end local v0    # "childCnt":I
    .end local v2    # "i":I
    .end local v4    # "vg":Landroid/view/ViewGroup;
    :cond_0
    instance-of v5, p1, Landroid/widget/ImageView;

    if-eqz v5, :cond_1

    move-object v3, p1

    .line 833
    check-cast v3, Landroid/widget/ImageView;

    .line 834
    .local v3, "iv":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f0a00bf

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 836
    .local v1, "dividerHeight":I
    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v5

    if-le v5, v1, :cond_1

    invoke-virtual {v3}, Landroid/widget/ImageView;->getId()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->isReleaseImageView(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 837
    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 838
    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 839
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getBgResId()I

    move-result v5

    const v6, 0x7f020128

    if-ne v5, v6, :cond_2

    .line 840
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 848
    .end local v1    # "dividerHeight":I
    .end local v3    # "iv":Landroid/widget/ImageView;
    :cond_1
    :goto_1
    return-void

    .line 841
    .restart local v1    # "dividerHeight":I
    .restart local v3    # "iv":Landroid/widget/ImageView;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getBgResId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_3

    .line 842
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getBgResId()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 844
    :cond_3
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method private getChildCountInOneLine(II)I
    .locals 9
    .param p1, "startIndex"    # I
    .param p2, "parentWidth"    # I

    .prologue
    .line 231
    const/4 v3, 0x0

    .line 232
    .local v3, "count":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v1

    .line 234
    .local v1, "childCount":I
    if-ge p1, v1, :cond_2

    .line 235
    const/4 v6, 0x0

    .line 237
    .local v6, "totalWidth":I
    move v5, p1

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_2

    .line 238
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 240
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_0

    .line 237
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 244
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 246
    .local v2, "childWidth":I
    add-int v7, v6, v2

    if-le v7, p2, :cond_1

    move v4, v3

    .line 255
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childWidth":I
    .end local v3    # "count":I
    .end local v5    # "i":I
    .end local v6    # "totalWidth":I
    .local v4, "count":I
    :goto_2
    return v4

    .line 249
    .end local v4    # "count":I
    .restart local v0    # "child":Landroid/view/View;
    .restart local v2    # "childWidth":I
    .restart local v3    # "count":I
    .restart local v5    # "i":I
    .restart local v6    # "totalWidth":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 250
    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    add-int/2addr v7, v2

    add-int/2addr v6, v7

    goto :goto_1

    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childWidth":I
    .end local v5    # "i":I
    .end local v6    # "totalWidth":I
    :cond_2
    move v4, v3

    .line 255
    .end local v3    # "count":I
    .restart local v4    # "count":I
    goto :goto_2
.end method

.method private getMeasuredChildCount(III)I
    .locals 5
    .param p1, "parentWidth"    # I
    .param p2, "childWidth"    # I
    .param p3, "spacing"    # I

    .prologue
    .line 203
    const/4 v1, 0x0

    .line 205
    .local v1, "columnCnt":I
    const/4 v0, 0x1

    .local v0, "column":I
    :goto_0
    const/16 v3, 0x19

    if-ge v0, v3, :cond_0

    .line 206
    mul-int v3, v0, p2

    add-int/lit8 v4, v0, -0x1

    mul-int/2addr v4, p3

    add-int v2, v3, v4

    .line 208
    .local v2, "width":I
    if-le v2, p1, :cond_1

    .line 209
    add-int/lit8 v1, v0, -0x1

    .line 214
    .end local v2    # "width":I
    :cond_0
    return v1

    .line 205
    .restart local v2    # "width":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getNewItemGridType(IZ)Landroid/view/View;
    .locals 5
    .param p1, "index"    # I
    .param p2, "bAddSpacing"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 598
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2, p1, v3, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 600
    .local v0, "itemView":Landroid/view/View;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnWidth:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnHeight:I

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 602
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-eqz p2, :cond_0

    .line 603
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 606
    :cond_0
    if-eqz v0, :cond_1

    .line 607
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 610
    :cond_1
    return-object v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 439
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mContext:Landroid/content/Context;

    .line 441
    if-eqz p2, :cond_0

    .line 442
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/R$styleable;->CategoryView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 444
    .local v0, "customAr":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 445
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    .line 446
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnHeight:I

    .line 448
    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnWidth:I

    .line 450
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    .line 452
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginRight:I

    .line 454
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginTop:I

    .line 455
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginBottom:I

    .line 458
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mVerticalSpacing:I

    .line 460
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    .line 463
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    .line 465
    invoke-virtual {v0, v5, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdColumnWidth:I

    .line 467
    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdColumnHeight:I

    .line 469
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdHorizontalSpacing:I

    .line 471
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdVerticalSpacing:I

    .line 473
    invoke-virtual {v0, v6, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdMarginLeft:I

    .line 475
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdMarginRight:I

    .line 478
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 482
    .end local v0    # "customAr":Landroid/content/res/TypedArray;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 483
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mResources:Landroid/content/res/Resources;

    .line 486
    :cond_1
    const-string v1, "grid"

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 487
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setOrientation(I)V

    .line 492
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mCurOrientation:I

    .line 494
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0c0027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntInList:I

    .line 495
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0c0020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntIntGrid:I

    .line 496
    return-void

    .line 489
    :cond_2
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setOrientation(I)V

    goto :goto_0
.end method

.method private isReleaseImageView(I)Z
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 852
    const/16 v2, 0x1a

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 862
    .local v0, "except":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 863
    aget v2, v0, v1

    if-ne p1, v2, :cond_0

    .line 864
    const/4 v2, 0x0

    .line 867
    :goto_1
    return v2

    .line 862
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 867
    :cond_1
    const/4 v2, 0x1

    goto :goto_1

    .line 852
    :array_0
    .array-data 4
        0x7f0b0083
        0x7f0b0086
        0x7f0b0085
        0x7f0b0087
        0x7f0b0089
        0x7f0b0084
        0x7f0b0073
        0x7f0b005c
        0x7f0b0096
        0x7f0b0097
        0x7f0b005b
        0x7f0b008a
        0x7f0b008b
        0x7f0b0072
        0x7f0b007b
        0x7f0b00a2
        0x7f0b005e
        0x7f0b00a9
        0x7f0b0016
        0x7f0b009f
        0x7f0b009c
        0x7f0b004e
        0x7f0b00a6
        0x7f0b0066
        0x7f0b007e
        0x7f0b007f
    .end array-data
.end method

.method private setChildFrame(Landroid/view/View;IIII)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 435
    add-int v0, p2, p4

    add-int v1, p3, p5

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 436
    return-void
.end method

.method private setVisibleIndex(IIILcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;II)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "startY"    # I
    .param p3, "endY"    # I
    .param p4, "itemIndex"    # Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;
    .param p5, "count"    # I
    .param p6, "sizeY"    # I

    .prologue
    const/4 v1, 0x1

    .line 402
    if-gez p2, :cond_2

    if-lez p3, :cond_2

    .line 403
    iget-boolean v0, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->bShow:Z

    if-nez v0, :cond_0

    .line 404
    iput p1, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->first:I

    .line 406
    :cond_0
    iput-boolean v1, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->bShow:Z

    .line 407
    add-int/lit8 v0, p5, -0x1

    if-ne p1, v0, :cond_1

    .line 408
    iput p1, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->last:I

    .line 432
    :cond_1
    :goto_0
    return-void

    .line 410
    :cond_2
    if-ltz p2, :cond_5

    if-gt p3, p6, :cond_5

    .line 411
    iget-boolean v0, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->bShow:Z

    if-nez v0, :cond_3

    .line 412
    iput-boolean v1, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->bShow:Z

    .line 413
    iput p1, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->first:I

    .line 415
    :cond_3
    add-int/lit8 v0, p5, -0x1

    if-eq p1, v0, :cond_4

    if-ne p3, p6, :cond_1

    .line 416
    :cond_4
    iput p1, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->last:I

    goto :goto_0

    .line 419
    :cond_5
    if-ge p2, p6, :cond_7

    if-le p3, p6, :cond_7

    .line 420
    iget-boolean v0, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->bShow:Z

    if-nez v0, :cond_6

    .line 421
    iput-boolean v1, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->bShow:Z

    .line 422
    iput p1, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->first:I

    .line 424
    :cond_6
    iput p1, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->last:I

    goto :goto_0

    .line 426
    :cond_7
    iget-boolean v0, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->bShow:Z

    if-eqz v0, :cond_1

    .line 427
    iget v0, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->last:I

    if-nez v0, :cond_1

    .line 428
    add-int/lit8 v0, p1, -0x1

    iput v0, p4, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->last:I

    goto :goto_0
.end method

.method private updateListItemByRange(II)V
    .locals 9
    .param p1, "startRow"    # I
    .param p2, "endRow"    # I

    .prologue
    .line 706
    const/4 v0, 0x0

    .line 708
    .local v0, "child":Landroid/view/View;
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-gt v1, p2, :cond_4

    .line 709
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 711
    if-eqz v0, :cond_1

    .line 712
    const-string v7, "mix"

    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 713
    instance-of v7, v0, Landroid/widget/LinearLayout;

    if-eqz v7, :cond_0

    move-object v6, v0

    .line 715
    check-cast v6, Landroid/widget/LinearLayout;

    .line 716
    .local v6, "rowLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    .line 718
    .local v5, "rowCnt":I
    if-lez v5, :cond_1

    .line 719
    const/4 v4, 0x0

    .line 720
    .local v4, "rowChild":Landroid/view/View;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-ge v3, v5, :cond_1

    .line 721
    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 722
    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v2

    .line 723
    .local v2, "itemIndex":I
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7, v2, v4, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 720
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 728
    .end local v2    # "itemIndex":I
    .end local v3    # "j":I
    .end local v4    # "rowChild":Landroid/view/View;
    .end local v5    # "rowCnt":I
    .end local v6    # "rowLayout":Landroid/widget/LinearLayout;
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7, v1, v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 708
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 731
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getEntireCount()I

    move-result v7

    if-lt v1, v7, :cond_3

    .line 732
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getEntireCount()I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    .line 735
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7, v1, v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 739
    :cond_4
    return-void
.end method


# virtual methods
.method public appendExtraItem(I)V
    .locals 3
    .param p1, "oldCnt"    # I

    .prologue
    .line 614
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v0

    .line 616
    .local v0, "curCnt":I
    const-string v1, "mix"

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 618
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addItemMixViewType(II)V

    .line 627
    :goto_0
    if-lez p1, :cond_0

    .line 628
    add-int/lit8 v1, p1, -0x1

    invoke-direct {p0, v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListItemByRange(II)V

    .line 630
    :cond_0
    return-void

    .line 619
    :cond_1
    const-string v1, "grid"

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 621
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addItemGridViewType(II)V

    goto :goto_0

    .line 624
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addItemListViewType(II)V

    goto :goto_0
.end method

.method public compositCategoryList()V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v11, 0x0

    .line 534
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v6

    .line 535
    .local v6, "totalCnt":I
    const/4 v0, 0x0

    .line 537
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v7

    if-lez v7, :cond_0

    .line 538
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->removeAllViews()V

    .line 542
    :cond_0
    const-string v7, "mix"

    iget-object v10, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 543
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v6, :cond_a

    .line 544
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getViewSubType(I)I

    move-result v5

    .line 546
    .local v5, "subType":I
    const/4 v7, 0x3

    if-ne v5, v7, :cond_4

    .line 547
    move v3, v2

    .local v3, "j":I
    :goto_1
    if-ge v3, v6, :cond_a

    .line 548
    const/4 v1, 0x0

    .local v1, "h":I
    :goto_2
    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    if-ge v1, v7, :cond_1

    .line 549
    add-int v7, v3, v1

    if-lt v7, v6, :cond_2

    .line 547
    :cond_1
    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    add-int/2addr v3, v7

    goto :goto_1

    .line 552
    :cond_2
    add-int v10, v3, v1

    if-eqz v1, :cond_3

    move v7, v8

    :goto_3
    invoke-direct {p0, v10, v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getNewItemGridType(IZ)Landroid/view/View;

    move-result-object v0

    .line 553
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addView(Landroid/view/View;)V

    .line 548
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move v7, v9

    .line 552
    goto :goto_3

    .line 558
    .end local v1    # "h":I
    .end local v3    # "j":I
    :cond_4
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7, v2, v11, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 559
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addView(Landroid/view/View;)V

    .line 543
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 562
    .end local v2    # "i":I
    .end local v5    # "subType":I
    :cond_5
    const-string v7, "grid"

    iget-object v10, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 564
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v7

    sub-int v7, v6, v7

    iget v10, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntIntGrid:I

    if-ge v7, v10, :cond_6

    .line 565
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 568
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    if-ge v2, v6, :cond_a

    .line 569
    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    mul-int/2addr v7, v2

    iget v10, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    add-int v4, v7, v10

    .line 571
    .local v4, "rowItemCnt":I
    if-lt v4, v6, :cond_7

    .line 572
    move v4, v6

    .line 575
    :cond_7
    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    mul-int v3, v2, v7

    .restart local v3    # "j":I
    :goto_5
    if-ge v3, v4, :cond_9

    .line 576
    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    mul-int/2addr v7, v2

    if-eq v3, v7, :cond_8

    move v7, v8

    :goto_6
    invoke-direct {p0, v3, v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getNewItemGridType(IZ)Landroid/view/View;

    move-result-object v0

    .line 577
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addView(Landroid/view/View;)V

    .line 575
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_8
    move v7, v9

    .line 576
    goto :goto_6

    .line 581
    :cond_9
    if-ne v4, v6, :cond_b

    .line 595
    .end local v3    # "j":I
    .end local v4    # "rowItemCnt":I
    :cond_a
    return-void

    .line 568
    .restart local v3    # "j":I
    .restart local v4    # "rowItemCnt":I
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 586
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v4    # "rowItemCnt":I
    :cond_c
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v7

    sub-int v7, v6, v7

    iget v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntInList:I

    if-ge v7, v8, :cond_d

    .line 587
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 589
    :cond_d
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_7
    if-ge v2, v6, :cond_a

    .line 590
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v7, v2, v11, v11}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 591
    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->addView(Landroid/view/View;I)V

    .line 589
    add-int/lit8 v2, v2, 0x1

    goto :goto_7
.end method

.method public getMaxLineCount()I
    .locals 2

    .prologue
    .line 889
    const-string v0, "grid"

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 890
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntIntGrid:I

    .line 892
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMaxItemCntInList:I

    goto :goto_0
.end method

.method public isCategoryGridType()Z
    .locals 2

    .prologue
    .line 882
    const-string v0, "grid"

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 883
    const/4 v0, 0x1

    .line 885
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCategoryListType()Z
    .locals 2

    .prologue
    .line 875
    const-string v0, "mix"

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "grid"

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 876
    :cond_0
    const/4 v0, 0x0

    .line 878
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, -0x1

    .line 500
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdColumnWidth:I

    if-eq v0, v2, :cond_0

    .line 501
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdColumnWidth:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnWidth:I

    .line 504
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdColumnHeight:I

    if-eq v0, v2, :cond_1

    .line 505
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdColumnHeight:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnHeight:I

    .line 508
    :cond_1
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdHorizontalSpacing:I

    if-eq v0, v2, :cond_2

    .line 509
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdHorizontalSpacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    .line 512
    :cond_2
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdVerticalSpacing:I

    if-eq v0, v2, :cond_3

    .line 513
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdVerticalSpacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mVerticalSpacing:I

    .line 516
    :cond_3
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdMarginLeft:I

    if-eq v0, v2, :cond_4

    .line 517
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdMarginLeft:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    .line 520
    :cond_4
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdMarginRight:I

    if-eq v0, v2, :cond_5

    .line 521
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mAttrIdMarginRight:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginRight:I

    .line 524
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 525
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 37
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 260
    const/16 v26, 0x0

    .line 261
    .local v26, "bNeedUpdate":Z
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v14

    .line 262
    .local v14, "count":I
    sub-int v3, p4, p2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getPaddingStart()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getPaddingEnd()I

    move-result v11

    add-int/2addr v9, v11

    sub-int/2addr v3, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginRight:I

    add-int/2addr v9, v11

    sub-int v33, v3, v9

    .line 264
    .local v33, "parentWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getLayoutDirection()I

    move-result v3

    const/4 v9, 0x1

    if-ne v3, v9, :cond_2

    const/16 v31, 0x1

    .line 266
    .local v31, "isLayoutRtl":Z
    :goto_0
    new-instance v13, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;

    invoke-direct {v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;-><init>()V

    .line 267
    .local v13, "itemIndex":Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mContext:Landroid/content/Context;

    const-string v9, "window"

    invoke-virtual {v3, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Landroid/view/WindowManager;

    .line 268
    .local v36, "wm":Landroid/view/WindowManager;
    invoke-interface/range {v36 .. v36}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v27

    .line 269
    .local v27, "display":Landroid/view/Display;
    new-instance v34, Landroid/graphics/Point;

    invoke-direct/range {v34 .. v34}, Landroid/graphics/Point;-><init>()V

    .line 270
    .local v34, "size":Landroid/graphics/Point;
    const/4 v3, 0x2

    new-array v0, v3, [I

    move-object/from16 v32, v0

    .line 271
    .local v32, "loc":[I
    move-object/from16 v0, v27

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 273
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mCurOrientation:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    if-eq v3, v9, :cond_0

    .line 274
    const/16 v26, 0x1

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mCurOrientation:I

    .line 278
    :cond_0
    const-string v3, "grid"

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 279
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginTop:I

    .line 280
    .local v6, "dstTop":I
    const/4 v5, 0x0

    .line 282
    .local v5, "dstStart":I
    const/16 v28, 0x0

    .local v28, "i":I
    :goto_1
    move/from16 v0, v28

    if-ge v0, v14, :cond_b

    .line 283
    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v33

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCountInOneLine(II)I

    move-result v35

    .line 285
    .local v35, "subCount":I
    if-lez v35, :cond_a

    .line 286
    const/4 v7, 0x0

    .local v7, "childWidth":I
    const/4 v8, 0x0

    .line 288
    .local v8, "childHeight":I
    if-eqz v31, :cond_3

    sub-int v3, p4, p2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getPaddingStart()I

    move-result v9

    sub-int/2addr v3, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    sub-int v5, v3, v9

    .line 291
    :goto_2
    move/from16 v10, v28

    .local v10, "j":I
    :goto_3
    add-int v3, v28, v35

    if-ge v10, v3, :cond_9

    .line 292
    move/from16 v29, v10

    .line 294
    .local v29, "index":I
    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 296
    .local v4, "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v9, 0x8

    if-ne v3, v9, :cond_4

    .line 291
    :cond_1
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 264
    .end local v4    # "child":Landroid/view/View;
    .end local v5    # "dstStart":I
    .end local v6    # "dstTop":I
    .end local v7    # "childWidth":I
    .end local v8    # "childHeight":I
    .end local v10    # "j":I
    .end local v13    # "itemIndex":Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;
    .end local v27    # "display":Landroid/view/Display;
    .end local v28    # "i":I
    .end local v29    # "index":I
    .end local v31    # "isLayoutRtl":Z
    .end local v32    # "loc":[I
    .end local v34    # "size":Landroid/graphics/Point;
    .end local v35    # "subCount":I
    .end local v36    # "wm":Landroid/view/WindowManager;
    :cond_2
    const/16 v31, 0x0

    goto/16 :goto_0

    .line 288
    .restart local v5    # "dstStart":I
    .restart local v6    # "dstTop":I
    .restart local v7    # "childWidth":I
    .restart local v8    # "childHeight":I
    .restart local v13    # "itemIndex":Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;
    .restart local v27    # "display":Landroid/view/Display;
    .restart local v28    # "i":I
    .restart local v31    # "isLayoutRtl":Z
    .restart local v32    # "loc":[I
    .restart local v34    # "size":Landroid/graphics/Point;
    .restart local v35    # "subCount":I
    .restart local v36    # "wm":Landroid/view/WindowManager;
    :cond_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    goto :goto_2

    .line 300
    .restart local v4    # "child":Landroid/view/View;
    .restart local v10    # "j":I
    .restart local v29    # "index":I
    :cond_4
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 301
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 303
    if-eqz v31, :cond_7

    .line 304
    sub-int/2addr v5, v7

    move-object/from16 v3, p0

    .line 305
    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setChildFrame(Landroid/view/View;IIII)V

    .line 306
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    sub-int/2addr v5, v3

    .line 312
    :goto_5
    if-nez p1, :cond_5

    if-eqz v26, :cond_1

    .line 313
    :cond_5
    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 314
    const/4 v3, 0x1

    aget v3, v32, v3

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int v12, v3, v9

    .line 316
    .local v12, "viewEndY":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mCurOrientation:I

    const/4 v9, 0x2

    if-ne v3, v9, :cond_8

    const/4 v3, 0x1

    aget v3, v32, v3

    move-object/from16 v0, v34

    iget v9, v0, Landroid/graphics/Point;->y:I

    if-le v3, v9, :cond_8

    .line 318
    const/4 v3, 0x1

    aget v9, v32, v3

    move-object/from16 v0, v34

    iget v11, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v34

    iget v15, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v11, v15

    sub-int/2addr v9, v11

    aput v9, v32, v3

    .line 319
    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v34

    iget v9, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v9

    sub-int/2addr v12, v3

    .line 326
    :cond_6
    :goto_6
    const/4 v3, 0x1

    aget v11, v32, v3

    move-object/from16 v0, v34

    iget v15, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v15}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setVisibleIndex(IIILcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;II)V

    goto :goto_4

    .end local v12    # "viewEndY":I
    :cond_7
    move-object/from16 v3, p0

    .line 308
    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setChildFrame(Landroid/view/View;IIII)V

    .line 309
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    add-int/2addr v3, v7

    add-int/2addr v5, v3

    goto :goto_5

    .line 320
    .restart local v12    # "viewEndY":I
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mCurOrientation:I

    const/4 v9, 0x1

    if-ne v3, v9, :cond_6

    const/4 v3, 0x1

    aget v3, v32, v3

    if-gez v3, :cond_6

    .line 322
    const/4 v3, 0x1

    aget v9, v32, v3

    move-object/from16 v0, v34

    iget v11, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v34

    iget v15, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v11, v15

    add-int/2addr v9, v11

    aput v9, v32, v3

    .line 323
    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v34

    iget v9, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v9

    add-int/2addr v12, v3

    goto :goto_6

    .line 330
    .end local v4    # "child":Landroid/view/View;
    .end local v12    # "viewEndY":I
    .end local v29    # "index":I
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mVerticalSpacing:I

    add-int/2addr v3, v8

    add-int/2addr v6, v3

    .line 331
    add-int/lit8 v3, v35, -0x1

    add-int v28, v28, v3

    .line 282
    .end local v7    # "childWidth":I
    .end local v8    # "childHeight":I
    .end local v10    # "j":I
    :cond_a
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_1

    .line 335
    .end local v35    # "subCount":I
    :cond_b
    if-nez p1, :cond_c

    if-eqz v26, :cond_d

    .line 336
    :cond_c
    iget v3, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->first:I

    const/4 v9, -0x1

    if-eq v3, v9, :cond_d

    iget v3, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->last:I

    const/4 v9, -0x1

    if-eq v3, v9, :cond_d

    .line 337
    iget v3, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->first:I

    iget v9, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->last:I

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1, v3, v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListByPage(ZII)V

    .line 398
    .end local v5    # "dstStart":I
    .end local v6    # "dstTop":I
    :cond_d
    :goto_7
    return-void

    .line 341
    .end local v28    # "i":I
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    move/from16 v17, v0

    .local v17, "childLeft":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginTop:I

    move/from16 v18, v0

    .line 343
    .local v18, "childTop":I
    const/16 v28, 0x0

    .restart local v28    # "i":I
    :goto_8
    move/from16 v0, v28

    if-ge v0, v14, :cond_18

    .line 344
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 345
    .restart local v4    # "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move/from16 v0, v28

    invoke-virtual {v3, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getViewSubType(I)I

    move-result v3

    const/4 v9, 0x3

    if-ne v3, v9, :cond_f

    const/16 v30, 0x1

    .line 347
    .local v30, "isGridType":Z
    :goto_9
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v9, 0x8

    if-ne v3, v9, :cond_10

    .line 343
    :goto_a
    add-int/lit8 v28, v28, 0x1

    goto :goto_8

    .line 345
    .end local v30    # "isGridType":Z
    :cond_f
    const/16 v30, 0x0

    goto :goto_9

    .line 351
    .restart local v30    # "isGridType":Z
    :cond_10
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 352
    .restart local v7    # "childWidth":I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 354
    .restart local v8    # "childHeight":I
    if-eqz v30, :cond_15

    .line 355
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    const/4 v9, 0x1

    if-ne v3, v9, :cond_12

    .line 356
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    move/from16 v17, v0

    .line 357
    mul-int v3, v8, v28

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mVerticalSpacing:I

    mul-int v9, v9, v28

    add-int v18, v3, v9

    :goto_b
    move-object/from16 v15, p0

    move-object/from16 v16, v4

    move/from16 v19, v7

    move/from16 v20, v8

    .line 372
    invoke-direct/range {v15 .. v20}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setChildFrame(Landroid/view/View;IIII)V

    .line 374
    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 375
    const/4 v3, 0x1

    aget v3, v32, v3

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int v12, v3, v9

    .line 377
    .restart local v12    # "viewEndY":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mCurOrientation:I

    const/4 v9, 0x2

    if-ne v3, v9, :cond_16

    const/4 v3, 0x1

    aget v3, v32, v3

    move-object/from16 v0, v34

    iget v9, v0, Landroid/graphics/Point;->y:I

    if-le v3, v9, :cond_16

    .line 378
    const/4 v3, 0x1

    aget v9, v32, v3

    move-object/from16 v0, v34

    iget v11, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v34

    iget v15, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v11, v15

    sub-int/2addr v9, v11

    aput v9, v32, v3

    .line 379
    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v34

    iget v9, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v9

    sub-int/2addr v12, v3

    .line 385
    :cond_11
    :goto_c
    const/4 v3, 0x1

    aget v21, v32, v3

    move-object/from16 v0, v34

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v19, p0

    move/from16 v20, v28

    move/from16 v22, v12

    move-object/from16 v23, v13

    move/from16 v24, v14

    invoke-direct/range {v19 .. v25}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setVisibleIndex(IIILcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;II)V

    .line 387
    if-eqz v30, :cond_17

    .line 388
    add-int v17, v17, v7

    goto/16 :goto_a

    .line 358
    .end local v12    # "viewEndY":I
    :cond_12
    add-int v3, v17, v7

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    add-int/2addr v3, v9

    sub-int v9, p4, p2

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginRight:I

    sub-int/2addr v9, v11

    if-le v3, v9, :cond_13

    .line 359
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    move/from16 v17, v0

    .line 360
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mVerticalSpacing:I

    add-int/2addr v3, v8

    add-int v18, v18, v3

    goto :goto_b

    .line 362
    :cond_13
    if-nez v28, :cond_14

    .line 363
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    move/from16 v17, v0

    goto/16 :goto_b

    .line 365
    :cond_14
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    add-int v17, v17, v3

    goto/16 :goto_b

    .line 369
    :cond_15
    const/16 v17, 0x0

    goto/16 :goto_b

    .line 380
    .restart local v12    # "viewEndY":I
    :cond_16
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mCurOrientation:I

    const/4 v9, 0x1

    if-ne v3, v9, :cond_11

    const/4 v3, 0x1

    aget v3, v32, v3

    if-gez v3, :cond_11

    .line 381
    const/4 v3, 0x1

    aget v9, v32, v3

    move-object/from16 v0, v34

    iget v11, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v34

    iget v15, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v11, v15

    add-int/2addr v9, v11

    aput v9, v32, v3

    .line 382
    move-object/from16 v0, v34

    iget v3, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v34

    iget v9, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v9

    add-int/2addr v12, v3

    goto :goto_c

    .line 390
    :cond_17
    add-int v18, v18, v8

    goto/16 :goto_a

    .line 394
    .end local v4    # "child":Landroid/view/View;
    .end local v7    # "childWidth":I
    .end local v8    # "childHeight":I
    .end local v12    # "viewEndY":I
    .end local v30    # "isGridType":Z
    :cond_18
    iget v3, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->first:I

    const/4 v9, -0x1

    if-eq v3, v9, :cond_d

    iget v3, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->last:I

    const/4 v9, -0x1

    if-eq v3, v9, :cond_d

    .line 395
    iget v3, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->first:I

    iget v9, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView$Index;->last:I

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1, v3, v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListByPage(ZII)V

    goto/16 :goto_7
.end method

.method protected onMeasure(II)V
    .locals 20
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 97
    const/16 v16, 0x140

    .local v16, "width":I
    const/16 v8, 0x140

    .line 99
    .local v8, "height":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v14

    .line 100
    .local v14, "wMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    .line 102
    .local v15, "wSpec":I
    sparse-switch v14, :sswitch_data_0

    .line 113
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v4

    .line 115
    .local v4, "childCnt":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getExpandedMode()I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_c

    if-lez v4, :cond_c

    .line 117
    const-string v17, "grid"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 118
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnWidth:I

    .line 119
    .local v6, "childWidth":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnHeight:I

    .line 121
    .local v5, "childHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginRight:I

    move/from16 v18, v0

    add-int v17, v17, v18

    sub-int v10, v16, v17

    .line 122
    .local v10, "parentWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnWidth:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v10, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getMeasuredChildCount(III)I

    move-result v7

    .line 124
    .local v7, "colCnt":I
    if-lez v4, :cond_0

    if-nez v7, :cond_0

    .line 125
    const/4 v7, 0x1

    .line 128
    :cond_0
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v4, :cond_2

    .line 129
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 131
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 128
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 104
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "childCnt":I
    .end local v5    # "childHeight":I
    .end local v6    # "childWidth":I
    .end local v7    # "colCnt":I
    .end local v9    # "i":I
    .end local v10    # "parentWidth":I
    :sswitch_0
    move/from16 v16, v15

    .line 105
    goto :goto_0

    .line 107
    :sswitch_1
    move/from16 v16, v15

    .line 108
    goto :goto_0

    .line 135
    .restart local v3    # "child":Landroid/view/View;
    .restart local v4    # "childCnt":I
    .restart local v5    # "childHeight":I
    .restart local v6    # "childWidth":I
    .restart local v7    # "colCnt":I
    .restart local v9    # "i":I
    .restart local v10    # "parentWidth":I
    :cond_1
    const/high16 v17, 0x40000000    # 2.0f

    move/from16 v0, v17

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v17

    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v18

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 139
    .end local v3    # "child":Landroid/view/View;
    :cond_2
    if-lez v7, :cond_4

    .line 140
    div-int v11, v4, v7

    .line 142
    .local v11, "rowCnt":I
    rem-int v17, v4, v7

    if-eqz v17, :cond_3

    .line 143
    add-int/lit8 v11, v11, 0x1

    .line 146
    :cond_3
    mul-int v17, v5, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mVerticalSpacing:I

    move/from16 v18, v0

    add-int/lit8 v19, v11, -0x1

    mul-int v18, v18, v19

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginTop:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginBottom:I

    move/from16 v18, v0

    add-int v8, v17, v18

    .line 149
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setColumnCount(I)V

    .line 153
    .end local v11    # "rowCnt":I
    :cond_4
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setMeasuredDimension(II)V

    .line 200
    .end local v5    # "childHeight":I
    .end local v6    # "childWidth":I
    .end local v7    # "colCnt":I
    .end local v9    # "i":I
    .end local v10    # "parentWidth":I
    :goto_3
    return-void

    .line 154
    :cond_5
    const-string v17, "mix"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mListType:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 155
    const/4 v8, 0x0

    .line 157
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnWidth:I

    .line 158
    .restart local v6    # "childWidth":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnHeight:I

    .line 160
    .restart local v5    # "childHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginLeft:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mMarginRight:I

    move/from16 v18, v0

    add-int v17, v17, v18

    sub-int v10, v16, v17

    .line 161
    .restart local v10    # "parentWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnWidth:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mHorizontalSpacing:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v10, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getMeasuredChildCount(III)I

    move-result v7

    .line 162
    .restart local v7    # "colCnt":I
    const/4 v12, 0x0

    .line 164
    .local v12, "rowCount":I
    if-lez v4, :cond_6

    if-nez v7, :cond_6

    .line 165
    const/4 v7, 0x1

    .line 168
    :cond_6
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_4
    if-ge v9, v4, :cond_a

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getViewSubType(I)I

    move-result v13

    .line 170
    .local v13, "subType":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 172
    .restart local v3    # "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    .line 168
    :goto_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 176
    :cond_7
    const/16 v17, 0x3

    move/from16 v0, v17

    if-ne v13, v0, :cond_9

    .line 177
    const/high16 v17, 0x40000000    # 2.0f

    move/from16 v0, v17

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v17

    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v18

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/view/View;->measure(II)V

    .line 180
    rem-int v17, v12, v7

    if-nez v17, :cond_8

    .line 181
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    add-int v8, v8, v17

    .line 184
    :cond_8
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    .line 186
    :cond_9
    const/high16 v17, 0x40000000    # 2.0f

    invoke-static/range {v16 .. v17}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/view/View;->measure(II)V

    .line 189
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    add-int v8, v8, v17

    goto :goto_5

    .line 193
    .end local v3    # "child":Landroid/view/View;
    .end local v13    # "subType":I
    :cond_a
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setMeasuredDimension(II)V

    goto/16 :goto_3

    .line 195
    .end local v5    # "childHeight":I
    .end local v6    # "childWidth":I
    .end local v7    # "colCnt":I
    .end local v9    # "i":I
    .end local v10    # "parentWidth":I
    .end local v12    # "rowCount":I
    :cond_b
    invoke-super/range {p0 .. p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto/16 :goto_3

    .line 198
    :cond_c
    invoke-super/range {p0 .. p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto/16 :goto_3

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public refreshCheckBoxAllItem()V
    .locals 2

    .prologue
    .line 742
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v0

    .line 743
    .local v0, "childCnt":I
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListItemByRange(II)V

    .line 744
    return-void
.end method

.method public refreshItemCurrentPage()V
    .locals 3

    .prologue
    .line 699
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v0

    .line 700
    .local v0, "first":I
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v1

    .line 702
    .local v1, "last":I
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListByPage(ZII)V

    .line 703
    return-void
.end method

.method public releaseItemWithoutShowing()V
    .locals 6

    .prologue
    .line 812
    const/4 v0, 0x0

    .line 813
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v1

    .line 814
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 815
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v3

    if-lt v2, v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 817
    :cond_0
    const-string v3, "release"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "release row:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 820
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->findImageViewAndRelease(Landroid/view/View;)V

    .line 814
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 823
    :cond_2
    return-void
.end method

.method public releaseViewWhenScreenOut(IIII)V
    .locals 6
    .param p1, "oldFirst"    # I
    .param p2, "oldLast"    # I
    .param p3, "newFirst"    # I
    .param p4, "newLast"    # I

    .prologue
    const/4 v3, -0x1

    .line 770
    const/4 v2, 0x0

    .line 771
    .local v2, "startRelase":I
    const/4 v1, 0x0

    .line 773
    .local v1, "endRelease":I
    if-ge p1, p3, :cond_3

    .line 775
    if-le p3, p2, :cond_2

    .line 776
    move v2, p1

    .line 777
    add-int/lit8 v1, p2, 0x1

    .line 797
    :cond_0
    :goto_0
    if-ne p4, v3, :cond_1

    if-ne p3, v3, :cond_1

    .line 798
    move v2, p1

    .line 799
    add-int/lit8 v1, p2, 0x1

    .line 802
    :cond_1
    const/4 v0, 0x0

    .line 803
    .local v0, "child":Landroid/view/View;
    const-string v3, "Rlease"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ImageView Rlease from"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/to :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    :goto_1
    if-ge v2, v1, :cond_6

    .line 806
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 807
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->findImageViewAndRelease(Landroid/view/View;)V

    .line 805
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 779
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    move v2, p1

    .line 780
    move v1, p3

    goto :goto_0

    .line 782
    :cond_3
    if-le p1, p3, :cond_5

    .line 784
    if-ge p4, p1, :cond_4

    .line 785
    move v2, p1

    .line 786
    add-int/lit8 v1, p2, 0x1

    goto :goto_0

    .line 788
    :cond_4
    add-int/lit8 v2, p4, 0x1

    .line 789
    add-int/lit8 v1, p2, 0x1

    goto :goto_0

    .line 792
    :cond_5
    if-le p2, p4, :cond_0

    .line 793
    add-int/lit8 v2, p4, 0x1

    .line 794
    add-int/lit8 v1, p2, 0x1

    goto :goto_0

    .line 809
    .restart local v0    # "child":Landroid/view/View;
    :cond_6
    return-void
.end method

.method public setAdapter(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .prologue
    .line 528
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .line 529
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setColumnCount(I)V

    .line 530
    return-void
.end method

.method public updateColumnCount(I)V
    .locals 0
    .param p1, "newCnt"    # I

    .prologue
    .line 871
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mColumnCnt:I

    .line 872
    return-void
.end method

.method public updateListByPage(ZII)V
    .locals 5
    .param p1, "bForce"    # Z
    .param p2, "newFirst"    # I
    .param p3, "newLast"    # I

    .prologue
    .line 747
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v0

    .line 748
    .local v0, "oldFirst":I
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v1

    .line 751
    .local v1, "oldLast":I
    if-nez p1, :cond_1

    if-ne p2, v0, :cond_1

    if-eq p3, v1, :cond_0

    if-ltz v1, :cond_1

    if-ge p3, v1, :cond_1

    .line 753
    :cond_0
    const-string v2, "Category"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Dont update dont change index:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    :goto_0
    return-void

    .line 758
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    .line 759
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 760
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->resetImageLoader()V

    .line 762
    const-string v2, "Category"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update  index: First:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " newLast:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "name:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->mItemAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->releaseItemWithoutShowing()V

    .line 766
    invoke-direct {p0, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListItemByRange(II)V

    goto :goto_0
.end method
