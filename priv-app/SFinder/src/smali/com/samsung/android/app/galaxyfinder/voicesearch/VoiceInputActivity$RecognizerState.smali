.class final enum Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;
.super Ljava/lang/Enum;
.source "VoiceInputActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RecognizerState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

.field public static final enum IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

.field public static final enum LISTENING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

.field public static final enum PREPARING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

.field public static final enum RECOGNIZING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 83
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    const-string v1, "PREPARING"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->PREPARING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    const-string v1, "LISTENING"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->LISTENING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    const-string v1, "RECOGNIZING"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->RECOGNIZING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    .line 82
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->PREPARING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->LISTENING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->RECOGNIZING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 82
    const-class v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    invoke-virtual {v0}, [Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    return-object v0
.end method
