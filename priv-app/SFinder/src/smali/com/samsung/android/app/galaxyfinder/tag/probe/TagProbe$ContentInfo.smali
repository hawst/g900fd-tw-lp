.class public Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;
.super Ljava/lang/Object;
.source "TagProbe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContentInfo"
.end annotation


# instance fields
.field private eid:J

.field private reqType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

.field private sid:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;JJ)V
    .locals 0
    .param p1, "reqType"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "sid"    # J
    .param p4, "eid"    # J

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->reqType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    .line 43
    iput-wide p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->sid:J

    .line 44
    iput-wide p4, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->eid:J

    .line 45
    return-void
.end method


# virtual methods
.method public getEndId()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->eid:J

    return-wide v0
.end method

.method public getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->reqType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    return-object v0
.end method

.method public getStartId()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->sid:J

    return-wide v0
.end method
