.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0

    .prologue
    .line 513
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionGroupChanged(I)V
    .locals 10
    .param p1, "index"    # I

    .prologue
    const-wide/16 v8, 0x190

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 580
    const/4 v3, 0x4

    if-eq p1, v3, :cond_5

    .line 581
    move v1, p1

    .line 582
    .local v1, "item":I
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSearchResult()Z

    move-result v3

    if-nez v3, :cond_3

    .line 583
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V

    .line 584
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 586
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedPanel()Z
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getDetailPanelHeight()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 589
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getTagPanelHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-direct {v0, v5, v5, v3, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 591
    .local v0, "ani":Landroid/view/animation/TranslateAnimation;
    invoke-virtual {v0, v8, v9}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 592
    invoke-virtual {v0, v6}, Landroid/view/animation/TranslateAnimation;->setFillBefore(Z)V

    .line 593
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 648
    .end local v0    # "ani":Landroid/view/animation/TranslateAnimation;
    .end local v1    # "item":I
    :cond_0
    :goto_0
    return-void

    .line 594
    .restart local v1    # "item":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedPanel()Z
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 595
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getTagPanelHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-direct {v0, v5, v5, v5, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 597
    .restart local v0    # "ani":Landroid/view/animation/TranslateAnimation;
    invoke-virtual {v0, v8, v9}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 599
    new-instance v3, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10$1;

    invoke-direct {v3, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;I)V

    invoke-virtual {v0, v3}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 613
    invoke-virtual {v0, v6}, Landroid/view/animation/TranslateAnimation;->setFillEnabled(Z)V

    .line 614
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 617
    .end local v0    # "ani":Landroid/view/animation/TranslateAnimation;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->hideGroupPager(IZ)V

    goto :goto_0

    .line 620
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V

    .line 621
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingOpenedPanel()Z
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2600(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 622
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getTagPanelHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-direct {v2, v5, v5, v5, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 624
    .local v2, "trans":Landroid/view/animation/TranslateAnimation;
    new-instance v3, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10$2;

    invoke-direct {v3, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;I)V

    invoke-virtual {v2, v3}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 638
    invoke-virtual {v2, v6}, Landroid/view/animation/TranslateAnimation;->setFillEnabled(Z)V

    .line 639
    invoke-virtual {v2, v8, v9}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 640
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 642
    .end local v2    # "trans":Landroid/view/animation/TranslateAnimation;
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    move-result-object v3

    invoke-virtual {v3, v1, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->hideGroupPager(IZ)V

    goto/16 :goto_0

    .line 646
    .end local v1    # "item":I
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    move-result-object v3

    invoke-virtual {v3, p1, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->hideGroupPager(IZ)V

    goto/16 :goto_0
.end method

.method public onActionHandwriting(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 558
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryText(Ljava/lang/String;Z)V
    invoke-static {v0, p1, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Ljava/lang/String;Z)V

    .line 559
    return-void
.end method

.method public onActionItemAdded(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 546
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isUserTag()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->addSelectedUserTag(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    invoke-static {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 548
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->makeInternalTagQuery(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    invoke-static {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 550
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActionItemAdded : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActionItemAdded : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedUserTagBuffer()Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V

    .line 554
    return-void
.end method

.method public onActionItemChanged(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 1
    .param p1, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 518
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->clearFocus()V

    .line 524
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSearchResult()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedFilter()Z
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1700(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isUserTag()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 530
    :cond_1
    :goto_0
    return-void

    .line 529
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->makeTagQuery()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    goto :goto_0
.end method

.method public onActionItemRemoved(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 534
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isUserTag()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->removeSelectedUserTag(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    invoke-static {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 536
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->makeInternalTagQuery(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    invoke-static {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 538
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActionItemRemoved : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActionItemRemoved : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedUserTagBuffer()Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V

    .line 542
    return-void
.end method

.method public onActionItemsAdded(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 563
    .local p1, "datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 564
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 565
    .local v0, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isUserTag()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 566
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->addSelectedUserTag(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    invoke-static {v2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 568
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActionItemsAdded : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActionItemsAdded : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedUserTagBuffer()Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 574
    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->makeTagQuery()V
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    .line 576
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    return-void
.end method
