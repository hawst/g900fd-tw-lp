.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;
.super Ljava/lang/Object;
.source "ItemListMyfilesAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ThumbnailRunnable"
.end annotation


# instance fields
.field photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;)V
    .locals 0
    .param p2, "photoToLoad"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    .prologue
    .line 322
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 323
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;->photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    .line 324
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 330
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;->photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    iget-object v4, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->imageViewReused(Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 339
    :goto_0
    return-void

    .line 332
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;->photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->getThumbNail(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 334
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;->photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    invoke-direct {v0, v3, v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;Landroid/graphics/drawable/Drawable;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;)V

    .line 335
    .local v0, "bd":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 336
    .end local v0    # "bd":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v2

    .line 337
    .local v2, "th":Ljava/lang/Throwable;
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
