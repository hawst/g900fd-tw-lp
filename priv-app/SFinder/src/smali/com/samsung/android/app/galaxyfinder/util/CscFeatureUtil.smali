.class public Lcom/samsung/android/app/galaxyfinder/util/CscFeatureUtil;
.super Ljava/lang/Object;
.source "CscFeatureUtil.java"


# static fields
.field private static final ADD_SYMBOL:Ljava/lang/String; = "+"

.field private static final DELETE_SYMBOL:Ljava/lang/String; = "-"

.field private static final DELIMETER_SYMBOL:Ljava/lang/String; = ","

.field private static final FONT_CIRCLE:Ljava/lang/String; = "\uf893"

.field private static final FONT_HEART:Ljava/lang/String; = "\uf894"

.field private static final FONT_QUESTION:Ljava/lang/String; = "?"

.field private static final FONT_SHARP:Ljava/lang/String; = "#"

.field private static final FONT_SMILE:Ljava/lang/String; = "\uf895"

.field private static final FONT_STAR:Ljava/lang/String; = "\uf892"

.field private static final SYMBOL_CIRCLE:Ljava/lang/String; = "circle"

.field private static final SYMBOL_HEART:Ljava/lang/String; = "heart"

.field private static final SYMBOL_QUESTION:Ljava/lang/String; = "question"

.field private static final SYMBOL_SHARP:Ljava/lang/String; = "sharp"

.field private static final SYMBOL_SMILE:Ljava/lang/String; = "smile"

.field private static final SYMBOL_STAR:Ljava/lang/String; = "star"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addDefaultSymbolTable()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v0, "symbolTable":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "?"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    const-string v1, "\uf892"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    const-string v1, "\uf893"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    const-string v1, "\uf894"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    const-string v1, "\uf895"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    return-object v0
.end method

.method private static convertCSCToFont(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "cscStr"    # Ljava/lang/String;

    .prologue
    .line 43
    const-string v0, "sharp"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const-string v0, "#"

    .line 56
    :goto_0
    return-object v0

    .line 45
    :cond_0
    const-string v0, "question"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    const-string v0, "?"

    goto :goto_0

    .line 47
    :cond_1
    const-string v0, "star"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 48
    const-string v0, "\uf892"

    goto :goto_0

    .line 49
    :cond_2
    const-string v0, "circle"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 50
    const-string v0, "\uf893"

    goto :goto_0

    .line 51
    :cond_3
    const-string v0, "heart"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 52
    const-string v0, "\uf894"

    goto :goto_0

    .line 53
    :cond_4
    const-string v0, "smile"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 54
    const-string v0, "\uf895"

    goto :goto_0

    .line 56
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getSymbolTableCompareWithCSC()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    .line 78
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/CscFeatureUtil;->addDefaultSymbolTable()Ljava/util/List;

    move-result-object v6

    .line 80
    .local v6, "symbolFontList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    .line 81
    .local v1, "cscFeature":Lcom/sec/android/app/CscFeature;
    const-string v10, "CscFeature_Finder_ConfigSymbolSearch"

    invoke-virtual {v1, v10}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 84
    .local v8, "symbolString":Ljava/lang/String;
    if-nez v8, :cond_1

    .line 131
    .end local v6    # "symbolFontList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object v6

    .line 88
    .restart local v6    # "symbolFontList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const-string v10, ","

    invoke-virtual {v8, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 90
    .local v5, "symbolArray":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v10, v5

    if-ge v2, v10, :cond_8

    .line 91
    aget-object v9, v5, v2

    .line 93
    .local v9, "temp":Ljava/lang/String;
    if-eqz v9, :cond_2

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 90
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 97
    :cond_3
    const-string v10, "-"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 98
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v9, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/app/galaxyfinder/util/CscFeatureUtil;->convertCSCToFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 100
    .local v4, "str":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 101
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v3, v10, -0x1

    .local v3, "index":I
    :goto_3
    if-ltz v3, :cond_2

    .line 102
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 103
    invoke-interface {v6, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 101
    :cond_4
    add-int/lit8 v3, v3, -0x1

    goto :goto_3

    .line 108
    .end local v3    # "index":I
    .end local v4    # "str":Ljava/lang/String;
    :cond_5
    const-string v10, "+"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 109
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v9, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/app/galaxyfinder/util/CscFeatureUtil;->convertCSCToFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 111
    .restart local v4    # "str":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 112
    const/4 v0, 0x0

    .line 113
    .local v0, "bExist":Z
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 114
    .local v7, "symbolSize":I
    const/4 v3, 0x0

    .restart local v3    # "index":I
    :goto_4
    if-ge v3, v7, :cond_7

    .line 115
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 116
    const/4 v0, 0x1

    .line 114
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 121
    :cond_7
    if-nez v0, :cond_2

    .line 122
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 128
    .end local v0    # "bExist":Z
    .end local v3    # "index":I
    .end local v4    # "str":Ljava/lang/String;
    .end local v7    # "symbolSize":I
    .end local v9    # "temp":Ljava/lang/String;
    :cond_8
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    if-gtz v10, :cond_0

    .line 129
    const/4 v6, 0x0

    goto/16 :goto_0
.end method
