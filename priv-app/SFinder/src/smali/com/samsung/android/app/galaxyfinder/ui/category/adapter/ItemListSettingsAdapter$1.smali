.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$1;
.super Ljava/lang/Object;
.source "ItemListSettingsAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setCtrlModeAction(Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$1;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "checked"    # Z

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 75
    .local v0, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    if-eqz p2, :cond_0

    const-string v1, "1"

    :goto_0
    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionExtraFlags(Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$1;->val$intent:Landroid/content/Intent;

    const-string v2, "value"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 78
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$1;->val$intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 79
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->mPackageName:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->setLastestActionCategory(Ljava/lang/String;)V

    .line 80
    return-void

    .line 75
    :cond_0
    const-string v1, "0"

    goto :goto_0
.end method
