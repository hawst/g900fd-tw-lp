.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListGalleryGridAdapter.java"


# instance fields
.field private mCurrentExpandedRowCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 17
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedRowCount:I

    .line 22
    return-void
.end method

.method private adjustRowCount()V
    .locals 3

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedRowCount:I

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedRowCount:I

    .line 63
    return-void
.end method

.method private getItemView(Landroid/view/LayoutInflater;II)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "position"    # I
    .param p3, "res"    # I

    .prologue
    .line 49
    const/4 v1, 0x0

    .line 50
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 52
    .local v0, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    if-eqz v0, :cond_0

    .line 53
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, p3, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 55
    invoke-direct {p0, v1, v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->setDataInItem(Landroid/view/View;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;I)V

    .line 58
    :cond_0
    return-object v1
.end method

.method private setDataInItem(Landroid/view/View;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;I)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "item"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .param p3, "position"    # I

    .prologue
    .line 99
    const v4, 0x7f0b0062

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 100
    .local v1, "ivThumb":Landroid/widget/ImageView;
    invoke-virtual {p2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v3

    .line 101
    .local v3, "thumb":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText5()Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "desc":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0030

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 108
    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-nez v4, :cond_0

    .line 109
    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {p0, v1, v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 112
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, p1, v4, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 113
    return-void
.end method


# virtual methods
.method public expandMoreItem()I
    .locals 3

    .prologue
    .line 82
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 83
    .local v0, "expandCount":I
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mExpandedMode:I

    .line 84
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedCount:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedCount:I

    .line 85
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedCount:I

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 86
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedCount:I

    .line 88
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->adjustRowCount()V

    .line 89
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedCount:I

    return v1
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mExpandedMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedRowCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 29
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mDefaultDisplayedItemCount:I

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 36
    invoke-virtual {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 38
    if-nez p2, :cond_0

    .line 39
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030039

    invoke-direct {p0, v0, p1, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->getItemView(Landroid/view/LayoutInflater;II)Landroid/view/View;

    move-result-object p2

    .line 45
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 42
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->refreshDataInItemPhase2(ILandroid/view/View;)V

    goto :goto_0
.end method

.method public refreshDataInItemPhase2(ILandroid/view/View;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 118
    const-string v0, "Gallery"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "refreshDataInItemPhase2 out of  array :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    invoke-direct {p0, p2, v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->setDataInItem(Landroid/view/View;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;I)V

    goto :goto_0
.end method

.method public refreshExpandedCount(II)V
    .locals 0
    .param p1, "entireItemCount"    # I
    .param p2, "defaultItemCount"    # I

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->refreshExpandedCount(II)V

    .line 95
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->adjustRowCount()V

    .line 96
    return-void
.end method

.method public setExpandState(I)V
    .locals 3
    .param p1, "expandstate"    # I

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setExpandState(I)V

    .line 68
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 69
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 70
    .local v0, "defaultCnt":I
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedRowCount:I

    if-ge v1, v0, :cond_0

    .line 71
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedRowCount:I

    .line 76
    .end local v0    # "defaultCnt":I
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mDefaultDisplayedItemCount:I

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListGalleryGridAdapter;->mCurrentExpandedRowCount:I

    goto :goto_0
.end method
