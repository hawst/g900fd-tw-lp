.class public Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "GalaxyFinderSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 80
    return-void
.end method

.method private setFloatingSettings()V
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 75
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 76
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 77
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 78
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f0e0071

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 23
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f080000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 24
    .local v2, "isTablet":Z
    if-eqz v2, :cond_0

    .line 25
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->setFloatingSettings()V

    .line 28
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 29
    const v3, 0x103012b

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->setTheme(I)V

    .line 32
    :cond_1
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const v3, 0x7f03005e

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->setContentView(I)V

    .line 36
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;

    invoke-direct {v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;-><init>()V

    .line 38
    .local v1, "frag":Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    const v4, 0x7f0b00cc

    invoke-virtual {v3, v4, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    .line 40
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 44
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 46
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_2

    .line 47
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 48
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 49
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 50
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 52
    :cond_2
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 62
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 64
    .local v0, "itemID":I
    packed-switch v0, :pswitch_data_0

    .line 69
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 66
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;->finish()V

    .line 67
    const/4 v1, 0x1

    goto :goto_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 58
    return-void
.end method
