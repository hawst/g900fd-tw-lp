.class public Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterPeople;
.super Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;
.source "TagConverterPeople.java"


# instance fields
.field private final contactProjection:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;-><init>(Landroid/content/Context;)V

    .line 13
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterPeople;->contactProjection:[Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public convertToMeaningValue(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 11
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 28
    const/4 v8, 0x0

    .line 30
    .local v8, "result":Landroid/os/Bundle;
    if-eqz p1, :cond_1

    .line 31
    const-string v1, "value"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 33
    .local v10, "value":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterPeople;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 34
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterPeople;->contactProjection:[Ljava/lang/String;

    const-string v3, "lookup=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 39
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 42
    .local v7, "name":Ljava/lang/String;
    if-eqz v10, :cond_0

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 43
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    .end local v8    # "result":Landroid/os/Bundle;
    .local v9, "result":Landroid/os/Bundle;
    :try_start_1
    const-string v1, "value"

    invoke-virtual {v9, v1, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v8, v9

    .line 49
    .end local v7    # "name":Ljava/lang/String;
    .end local v9    # "result":Landroid/os/Bundle;
    .restart local v8    # "result":Landroid/os/Bundle;
    :cond_0
    if-eqz v6, :cond_1

    .line 50
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 55
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v10    # "value":Ljava/lang/String;
    :cond_1
    return-object v8

    .line 49
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "value":Ljava/lang/String;
    :catchall_0
    move-exception v1

    :goto_0
    if-eqz v6, :cond_2

    .line 50
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1

    .line 49
    .end local v8    # "result":Landroid/os/Bundle;
    .restart local v7    # "name":Ljava/lang/String;
    .restart local v9    # "result":Landroid/os/Bundle;
    :catchall_1
    move-exception v1

    move-object v8, v9

    .end local v9    # "result":Landroid/os/Bundle;
    .restart local v8    # "result":Landroid/os/Bundle;
    goto :goto_0
.end method

.method public convertToRawValue(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 23
    const/4 v0, 0x0

    return-object v0
.end method
