.class public Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
.super Ljava/lang/Object;
.source "QueryAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/search/QueryAction$QueryType;
    }
.end annotation


# instance fields
.field private mFrom:Ljava/lang/String;

.field private mId:I

.field private mKey:Ljava/lang/String;

.field private mQuery:Landroid/os/Bundle;

.field private mResultCount:I

.field private mSearchFilters:Landroid/os/Bundle;

.field private mTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTargets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTimeSpan:Ljava/lang/String;

.field private mType:I

.field private final onlyOneTagSelected:I


# direct methods
.method public constructor <init>(Landroid/os/Bundle;I)V
    .locals 2
    .param p1, "query"    # Landroid/os/Bundle;
    .param p2, "type"    # I

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mId:I

    .line 15
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    .line 17
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mKey:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTags:Ljava/util/ArrayList;

    .line 21
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTargets:Ljava/util/ArrayList;

    .line 23
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mSearchFilters:Landroid/os/Bundle;

    .line 25
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTimeSpan:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mFrom:Ljava/lang/String;

    .line 29
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mType:I

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mResultCount:I

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->onlyOneTagSelected:I

    .line 36
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->getActionId()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mId:I

    .line 37
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    .line 38
    iput p2, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mType:I

    .line 40
    if-eqz p1, :cond_5

    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mKey:Ljava/lang/String;

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_tags"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_tags"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTags:Ljava/util/ArrayList;

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_targets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_targets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTargets:Ljava/util/ArrayList;

    .line 53
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_from"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_from"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mFrom:Ljava/lang/String;

    .line 57
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_time_span"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 58
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_time_span"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTimeSpan:Ljava/lang/String;

    .line 61
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_search_filters"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 62
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    const-string v1, "extra_search_filters"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mSearchFilters:Landroid/os/Bundle;

    .line 66
    :cond_5
    return-void
.end method


# virtual methods
.method public getFrom()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mFrom:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mId:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method public getQuery()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mQuery:Landroid/os/Bundle;

    return-object v0
.end method

.method public getResultCount()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mResultCount:I

    return v0
.end method

.method public getSearchFilters()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mSearchFilters:Landroid/os/Bundle;

    return-object v0
.end method

.method public getTags()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTags:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTargets()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTargets:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTimeSpan()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTimeSpan:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mType:I

    return v0
.end method

.method public isValidResult()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 114
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mResultCount:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTimeSpan:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mKey:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mSearchFilters:Landroid/os/Bundle;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTags:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mTags:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 118
    :cond_0
    return v0
.end method

.method public setResultCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->mResultCount:I

    .line 110
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 138
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getQuery()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 139
    const-string v1, "Query : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getQuery()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 142
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 143
    const-string v1, "Key : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 146
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTags()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 147
    const-string v1, "Tags : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTags()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 150
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTargets()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 151
    const-string v1, "Targets : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTargets()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 154
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getFrom()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 155
    const-string v1, "From : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getFrom()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 158
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTimeSpan()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 159
    const-string v1, "Timespan : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTimeSpan()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getSearchFilters()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 163
    const-string v1, "Search filter : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getSearchFilters()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 166
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getType()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    .line 167
    const-string v1, "Type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 170
    :cond_7
    const-string v1, "Result count : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getResultCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 172
    const-string v1, "Valid result : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->isValidResult()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 174
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
