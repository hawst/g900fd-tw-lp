.class public final enum Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;
.super Ljava/lang/Enum;
.source "ContentShareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResultType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

.field public static final enum AvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

.field public static final enum ExceedToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

.field public static final enum NotAvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    const-string v1, "AvailableToShare"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->AvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    const-string v1, "ExceedToShare"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->ExceedToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    const-string v1, "NotAvailableToShare"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->NotAvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    .line 25
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->AvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->ExceedToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->NotAvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    invoke-virtual {v0}, [Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    return-object v0
.end method
