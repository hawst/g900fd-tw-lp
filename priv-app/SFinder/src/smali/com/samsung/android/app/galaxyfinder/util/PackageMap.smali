.class public Lcom/samsung/android/app/galaxyfinder/util/PackageMap;
.super Ljava/lang/Object;
.source "PackageMap.java"


# instance fields
.field private mPackageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/PackageMap;->mPackageMap:Ljava/util/HashMap;

    .line 16
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/util/PackageMap;->makePackageMap()V

    .line 17
    return-void
.end method

.method private makePackageMap()V
    .locals 6

    .prologue
    .line 20
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getSearchableList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 23
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 24
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 25
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/util/PackageMap;->mPackageMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/SearchableInfo;

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/SearchableInfo;

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_0
    return-void
.end method


# virtual methods
.method public getProviderPackageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "appPackageName"    # Ljava/lang/String;

    .prologue
    .line 31
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/util/PackageMap;->mPackageMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 32
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 35
    .end local p1    # "appPackageName":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "appPackageName":Ljava/lang/String;
    :cond_0
    move-object p1, v0

    goto :goto_0
.end method
