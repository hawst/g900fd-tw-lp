.class public abstract Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
.super Landroid/widget/LinearLayout;
.source "BaseCategoryView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$InfoFlag;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnMultiSelectionListener;
    }
.end annotation


# static fields
.field private static final INVALID_VALUE:I = -0x1

.field private static mLayoutInflater:Landroid/view/LayoutInflater;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mCallback:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

.field private mCategoryCheck:Landroid/widget/CheckBox;

.field protected mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

.field private mChangedExpandableState:Z

.field private mEntireItemCount:I

.field private mExpandControlView:Landroid/widget/ImageView;

.field private mExpandFullyTextView:Landroid/widget/TextView;

.field protected mExpandedMode:I

.field private mFooterView:Landroid/view/ViewGroup;

.field private mHeaderView:Landroid/view/ViewGroup;

.field private mIsTablet:Z

.field protected mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

.field private mMultiSelectionArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNumberTextView:Landroid/widget/TextView;

.field protected mPreviousShowMode:I

.field protected mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

.field private mSelectableItemCount:I

.field private mSelectedItemCount:I

.field private mTemplateLayout:Landroid/view/View;

.field mTitleClickListener:Landroid/view/View$OnClickListener;

.field private mTitleTextView:Landroid/widget/TextView;

.field private mViewMode:I

.field private mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 146
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 42
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->TAG:Ljava/lang/String;

    .line 50
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mChangedExpandableState:Z

    .line 52
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTemplateLayout:Landroid/view/View;

    .line 56
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .line 62
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .line 64
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mMultiSelectionArray:Ljava/util/ArrayList;

    .line 66
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    .line 68
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandFullyTextView:Landroid/widget/TextView;

    .line 74
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mNumberTextView:Landroid/widget/TextView;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTitleTextView:Landroid/widget/TextView;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    .line 80
    const/16 v0, 0x97

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewMode:I

    .line 82
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    .line 84
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectableItemCount:I

    .line 86
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I

    .line 88
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    .line 90
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 92
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mPreviousShowMode:I

    .line 98
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

    .line 416
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$5;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTitleClickListener:Landroid/view/View$OnClickListener;

    .line 148
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .line 149
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    .line 151
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mLayoutInflater:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0f0006

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 156
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    .line 158
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->init(Landroid/content/Context;)V

    .line 159
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I

    return p1
.end method

.method static synthetic access$008(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I

    return v0
.end method

.method static synthetic access$010(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectableItemCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    return-object v0
.end method

.method private addBodyView()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 339
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getTemplateLayoutId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTemplateLayout:Landroid/view/View;

    .line 340
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTemplateLayout:Landroid/view/View;

    const v4, 0x7f0b0048

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    iput-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    .line 341
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090056

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setBackgroundColor(I)V

    .line 343
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTemplateLayout:Landroid/view/View;

    .line 345
    .local v0, "body":Landroid/view/View;
    iget-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-eqz v3, :cond_0

    .line 346
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030003

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 349
    .local v1, "container":Landroid/view/ViewGroup;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 352
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a003f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 354
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a032f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    .line 357
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 358
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTemplateLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 360
    move-object v0, v1

    .line 363
    .end local v1    # "container":Landroid/view/ViewGroup;
    .end local v2    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addView(Landroid/view/View;)V

    .line 364
    return-void
.end method

.method private addDividerView()V
    .locals 5

    .prologue
    .line 193
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 195
    .local v0, "divider":Landroid/widget/ImageView;
    const v2, 0x7f0b005c

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setId(I)V

    .line 196
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020008

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 197
    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 198
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0041

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 203
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0042

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 206
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 208
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addView(Landroid/view/View;)V

    .line 209
    return-void
.end method

.method private addFooterView()V
    .locals 5

    .prologue
    .line 302
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030004

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    .line 304
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a032a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 324
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a032e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 326
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a032f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    .line 329
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 331
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    const v2, 0x7f0b0014

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandFullyTextView:Landroid/widget/TextView;

    .line 332
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandFullyTextView:Landroid/widget/TextView;

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->getFontTypeface(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 335
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addView(Landroid/view/View;)V

    .line 336
    return-void
.end method

.method private addHeaderView()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 213
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f030005

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    .line 217
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0060

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-direct {v2, v3, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 220
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 222
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v7

    if-le v3, v7, :cond_2

    move v3, v4

    :goto_0
    invoke-virtual {v6, v3}, Landroid/view/ViewGroup;->setSoundEffectsEnabled(Z)V

    .line 226
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const v6, 0x7f0b0017

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTitleTextView:Landroid/widget/TextView;

    .line 227
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTitleTextView:Landroid/widget/TextView;

    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->getFontTypeface(I)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 229
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getCategoryName()Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "label":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e004c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 233
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 234
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const v6, 0x7f0b0018

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mNumberTextView:Landroid/widget/TextView;

    .line 240
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mNumberTextView:Landroid/widget/TextView;

    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->getFontTypeface(I)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 241
    const-string v3, "%d"

    new-array v6, v4, [Ljava/lang/Object;

    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "mEntireCount":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mNumberTextView:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const v5, 0x7f0b0016

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    .line 246
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 247
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    new-instance v5, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)V

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 255
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    new-instance v5, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)V

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 286
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const v5, 0x7f0b0019

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    .line 287
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 291
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v4

    if-gt v3, v4, :cond_1

    .line 294
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 296
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addView(Landroid/view/View;)V

    .line 297
    return-void

    .end local v0    # "label":Ljava/lang/String;
    .end local v1    # "mEntireCount":Ljava/lang/String;
    :cond_2
    move v3, v5

    .line 223
    goto/16 :goto_0
.end method

.method private closeCategoryList()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 468
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 471
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->showControlAnimation(Z)V

    .line 473
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-nez v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 475
    const v0, 0x7f020190

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setBackgroundResource(I)V

    .line 477
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setVisibility(I)V

    .line 479
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    if-eqz v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getHeight()I

    move-result v1

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;->onChangedCategoryState(Landroid/view/View;I)V

    .line 482
    :cond_1
    return-void
.end method

.method private disable()V
    .locals 8

    .prologue
    .line 710
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v3

    .line 712
    .local v3, "overlay":Landroid/view/ViewOverlay;
    if-eqz v3, :cond_0

    .line 713
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getPaddingStart()I

    move-result v2

    .line 714
    .local v2, "l":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getPaddingTop()I

    move-result v5

    .line 715
    .local v5, "t":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getPaddingEnd()I

    move-result v7

    sub-int v4, v6, v7

    .line 716
    .local v4, "r":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getPaddingBottom()I

    move-result v7

    sub-int v0, v6, v7

    .line 718
    .local v0, "b":I
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090052

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v1, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 719
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v2, v5, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 721
    invoke-virtual {v3}, Landroid/view/ViewOverlay;->clear()V

    .line 722
    invoke-virtual {v3, v1}, Landroid/view/ViewOverlay;->add(Landroid/graphics/drawable/Drawable;)V

    .line 725
    .end local v0    # "b":I
    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    .end local v2    # "l":I
    .end local v4    # "r":I
    .end local v5    # "t":I
    :cond_0
    const/high16 v6, 0x60000

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setDescendantFocusability(I)V

    .line 726
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setEnabled(Z)V

    .line 727
    return-void
.end method

.method private enable()V
    .locals 2

    .prologue
    .line 730
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v0

    .line 732
    .local v0, "overlay":Landroid/view/ViewOverlay;
    if-eqz v0, :cond_0

    .line 733
    invoke-virtual {v0}, Landroid/view/ViewOverlay;->clear()V

    .line 736
    :cond_0
    const/high16 v1, 0x20000

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setDescendantFocusability(I)V

    .line 737
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setEnabled(Z)V

    .line 738
    return-void
.end method

.method private expandPartialItemList()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 511
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 512
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v0

    .line 514
    .local v0, "preVisibleItemCnt":I
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 515
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v0

    .line 517
    :cond_0
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    sub-int/2addr v1, v0

    if-lez v1, :cond_1

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    add-int/lit8 v1, v1, -0x14

    if-lez v1, :cond_1

    .line 519
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 522
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setExpandState(I)V

    .line 524
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mChangedExpandableState:Z

    .line 526
    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->showControlAnimation(Z)V

    .line 528
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-nez v1, :cond_2

    .line 529
    const v1, 0x7f0200d7

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setBackgroundResource(I)V

    .line 530
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const v2, 0x7f0200d8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 533
    :cond_2
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-ne v1, v3, :cond_3

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v2

    if-gt v1, v2, :cond_3

    .line 534
    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 535
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 537
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setVisibility(I)V

    .line 538
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->appendExtraItem(I)V

    .line 540
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    if-eqz v1, :cond_4

    .line 541
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getHeight()I

    move-result v2

    invoke-interface {v1, p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;->onChangedCategoryState(Landroid/view/View;I)V

    .line 543
    :cond_4
    return-void
.end method

.method private foldedDefaultItemList()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 485
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 486
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setExpandState(I)V

    .line 488
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 490
    invoke-direct {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->showControlAnimation(Z)V

    .line 492
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-nez v0, :cond_0

    .line 493
    const v0, 0x7f0200d7

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setBackgroundResource(I)V

    .line 494
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const v1, 0x7f0200d8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setVisibility(I)V

    .line 498
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 500
    instance-of v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryWebLinkView;

    if-nez v0, :cond_1

    .line 501
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->compositCategoryList()V

    .line 504
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    if-eqz v0, :cond_2

    .line 505
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getHeight()I

    move-result v1

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;->onChangedCategoryState(Landroid/view/View;I)V

    .line 507
    :cond_2
    return-void
.end method

.method private setSelectionMode(Z)V
    .locals 8
    .param p1, "enabled"    # Z

    .prologue
    const/4 v5, 0x0

    .line 668
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    .line 670
    .local v3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    const/4 v4, 0x0

    .line 671
    .local v4, "validItemCount":I
    const/4 v0, 0x0

    .line 673
    .local v0, "checkedItemCount":I
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 674
    .local v2, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isValid()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 675
    add-int/lit8 v4, v4, 0x1

    .line 678
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 679
    add-int/lit8 v0, v0, 0x1

    .line 682
    :cond_2
    if-nez p1, :cond_0

    .line 683
    invoke-virtual {v2, v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setChecked(Z)V

    goto :goto_0

    .line 687
    .end local v2    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_3
    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectableItemCount:I

    .line 688
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I

    .line 690
    if-eqz p1, :cond_6

    iget v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectableItemCount:I

    if-lez v6, :cond_6

    .line 691
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    invoke-virtual {v6, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 692
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectableItemCount:I

    if-ne v7, v0, :cond_4

    const/4 v5, 0x1

    :cond_4
    invoke-virtual {v6, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 697
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 698
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewMode:I

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setViewMode(I)V

    .line 699
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->notifyDataSetChanged()V

    .line 701
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->refreshCheckBoxAllItem()V

    .line 703
    :cond_5
    return-void

    .line 694
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_1
.end method

.method private showControlAnimation(Z)V
    .locals 3
    .param p1, "bSelectable"    # Z

    .prologue
    .line 546
    const/4 v1, 0x0

    .line 549
    .local v1, "aniId":I
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-eqz v2, :cond_1

    .line 550
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 554
    :cond_1
    if-eqz p1, :cond_2

    .line 555
    const v1, 0x7f040002

    .line 560
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 562
    .local v0, "ani":Landroid/view/animation/Animation;
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$6;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$6;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;Z)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 578
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillEnabled(Z)V

    .line 579
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 580
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 557
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :cond_2
    const v1, 0x7f040003

    goto :goto_1
.end method


# virtual methods
.method public collapseCategoryListView()V
    .locals 2

    .prologue
    .line 928
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setVisibility(I)V

    .line 930
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSoundEffectsEnabled(Z)V

    .line 931
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-nez v0, :cond_0

    .line 932
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 933
    const v0, 0x7f020190

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setBackgroundResource(I)V

    .line 935
    :cond_0
    return-void
.end method

.method public collapseCategoryListView2()V
    .locals 2

    .prologue
    .line 938
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setVisibility(I)V

    .line 939
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-nez v0, :cond_0

    .line 940
    const v0, 0x7f0200d7

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setBackgroundResource(I)V

    .line 941
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const v1, 0x7f0200d8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 943
    :cond_0
    return-void
.end method

.method public convertVisibleIndex()V
    .locals 4

    .prologue
    .line 1084
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 1085
    .local v1, "lastRow":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getVisbleDiff()I

    move-result v0

    .line 1086
    .local v0, "diff":I
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getMaxLineCount()I

    move-result v2

    if-le v0, v2, :cond_0

    .line 1087
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    sub-int v3, v1, v0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    .line 1091
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 1092
    return-void

    .line 1089
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getMaxLineCount()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    const/4 v0, 0x0

    .line 399
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public expandCategoryListView(Z)V
    .locals 5
    .param p1, "bRemake"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 946
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-nez v1, :cond_0

    .line 947
    const v1, 0x7f0200d7

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setBackgroundResource(I)V

    .line 948
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const v2, 0x7f0200d8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 950
    :cond_0
    if-eqz p1, :cond_2

    .line 951
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setVisibility(I)V

    .line 952
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->compositCategoryList()V

    .line 972
    :cond_1
    :goto_0
    return-void

    .line 954
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setVisibility(I)V

    .line 956
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-ne v1, v4, :cond_4

    .line 957
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isFullyExpanded()Z

    move-result v1

    if-nez v1, :cond_3

    .line 958
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 960
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v1

    if-eqz v1, :cond_4

    .line 961
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v2

    sub-int v0, v1, v2

    .line 963
    .local v0, "diff":I
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1, v4, v3, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListByPage(ZII)V

    .line 966
    .end local v0    # "diff":I
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v1

    if-gez v1, :cond_1

    .line 967
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 968
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    .line 969
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->refreshItemCurrentPage()V

    goto :goto_0
.end method

.method protected expendButtonStateChanged(Z)V
    .locals 3
    .param p1, "selected"    # Z

    .prologue
    .line 367
    if-eqz p1, :cond_0

    .line 368
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 374
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected abstract getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.end method

.method public getCategoryExpandedMode()I
    .locals 1

    .prologue
    .line 984
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    return v0
.end method

.method public getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    return-object v0
.end method

.method public getCategoryViewItem()I
    .locals 1

    .prologue
    .line 992
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getCategoryViewMode()I
    .locals 1

    .prologue
    .line 988
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewMode:I

    return v0
.end method

.method public getCenterItemIndex(I)I
    .locals 7
    .param p1, "pivot"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x0

    .line 1036
    const/4 v0, 0x0

    .line 1037
    .local v0, "centerIndex":I
    const/4 v2, 0x0

    .line 1038
    .local v2, "itemIndex":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v3, v4, Landroid/content/res/Configuration;->orientation:I

    .line 1040
    .local v3, "orienatition":I
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getMaxLineCount()I

    move-result v1

    .line 1042
    .local v1, "defaultLine":I
    if-ne v3, v5, :cond_0

    .line 1044
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    const/4 v5, 0x1

    invoke-virtual {v4, p1, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstItemIndexInRow(II)I

    move-result v2

    .line 1052
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getRowByItemIndex(I)I

    move-result v0

    .line 1054
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->isCategoryListType()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1055
    shl-int/lit8 v4, v1, 0x1

    if-lt v0, v4, :cond_1

    .line 1056
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    shl-int/lit8 v5, v1, 0x1

    sub-int v5, v0, v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    .line 1068
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v4

    if-lt v0, v4, :cond_4

    .line 1069
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 1070
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 1076
    :goto_2
    return v0

    .line 1048
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4, p1, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstItemIndexInRow(II)I

    move-result v2

    goto :goto_0

    .line 1058
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    goto :goto_1

    .line 1061
    :cond_2
    if-lt v0, v1, :cond_3

    .line 1062
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    sub-int v5, v0, v1

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    goto :goto_1

    .line 1064
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    goto :goto_1

    .line 1072
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    add-int v5, v0, v1

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    goto :goto_2
.end method

.method protected abstract getDefaultItemCount()I
.end method

.method public getFirstItemIndeInScreen()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 1095
    const/4 v0, 0x0

    .line 1096
    .local v0, "child":Landroid/view/View;
    new-array v2, v6, [I

    .line 1097
    .local v2, "loc":[I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryExpandedMode()I

    move-result v5

    if-eq v5, v6, :cond_1

    .line 1098
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v3

    .line 1099
    .local v3, "resultCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1100
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v5, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1101
    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1102
    const/4 v5, 0x1

    aget v5, v2, v5

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int v4, v5, v6

    .line 1104
    .local v4, "viewEndY":I
    if-ltz v4, :cond_0

    .line 1109
    .end local v1    # "i":I
    .end local v3    # "resultCount":I
    .end local v4    # "viewEndY":I
    :goto_1
    return v1

    .line 1099
    .restart local v1    # "i":I
    .restart local v3    # "resultCount":I
    .restart local v4    # "viewEndY":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1109
    .end local v1    # "i":I
    .end local v3    # "resultCount":I
    .end local v4    # "viewEndY":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getItemViewInCategory(I)Landroid/view/View;
    .locals 1
    .param p1, "centerIndex"    # I

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 1029
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    .line 1032
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedItemCount()I
    .locals 1

    .prologue
    .line 593
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I

    return v0
.end method

.method public getSelectedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 589
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getCheckedItems()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getTemplateLayoutId()I
.end method

.method public getVisbleDiff()I
    .locals 2

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method protected init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 162
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setOrientation(I)V

    .line 164
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-eqz v0, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addDividerView()V

    .line 168
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addHeaderView()V

    .line 169
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addBodyView()V

    .line 170
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addFooterView()V

    .line 172
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .line 174
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setItemStateListener(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;)V

    .line 176
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewMode:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setViewMode(I)V

    .line 178
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setAdapter(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)V

    .line 179
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->compositCategoryList()V

    .line 182
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    .line 183
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 186
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mIsTablet:Z

    if-nez v0, :cond_2

    .line 187
    const v0, 0x7f0200d7

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setBackgroundResource(I)V

    .line 188
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    const v1, 0x7f0200d8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 190
    :cond_2
    return-void
.end method

.method public notifyMultiSelection(IIIII)V
    .locals 11
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I
    .param p5, "offset"    # I

    .prologue
    .line 1122
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->isEnabled()Z

    move-result v8

    if-nez v8, :cond_1

    .line 1164
    :cond_0
    return-void

    .line 1126
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v5

    .line 1128
    .local v5, "count":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v5, :cond_0

    .line 1129
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v8, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1131
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_3

    .line 1128
    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1135
    :cond_3
    const/4 v8, 0x2

    new-array v7, v8, [I

    .line 1137
    .local v7, "loc":[I
    invoke-virtual {v0, v7}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1139
    const/4 v8, 0x0

    aget v2, v7, v8

    .line 1140
    .local v2, "childLeft":I
    const/4 v8, 0x1

    aget v8, v7, v8

    add-int v4, p5, v8

    .line 1141
    .local v4, "childTop":I
    const/4 v8, 0x0

    aget v8, v7, v8

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int v3, v8, v9

    .line 1142
    .local v3, "childRight":I
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int v1, v4, v8

    .line 1144
    .local v1, "childBottom":I
    if-le p1, v2, :cond_4

    if-le p2, v4, :cond_4

    if-ge p3, v3, :cond_4

    if-lt p4, v1, :cond_a

    :cond_4
    if-le p1, v2, :cond_5

    if-lt p3, v3, :cond_7

    :cond_5
    if-ge p1, v2, :cond_6

    if-gt p3, v2, :cond_7

    :cond_6
    if-ge p1, v3, :cond_b

    if-le p3, v3, :cond_b

    :cond_7
    if-le p2, v4, :cond_8

    if-lt p4, v1, :cond_a

    :cond_8
    if-ge p2, v4, :cond_9

    if-gt p4, v4, :cond_a

    :cond_9
    if-ge p2, v1, :cond_b

    if-le p4, v1, :cond_b

    .line 1148
    :cond_a
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mMultiSelectionArray:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1149
    const-string v8, "PENSELECT"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "added : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mMultiSelectionArray:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1152
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v8, v0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->onChangeMultiSelection(Landroid/view/View;I)V

    goto :goto_1

    .line 1156
    :cond_b
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mMultiSelectionArray:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1157
    const-string v8, "PENSELECT"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "removed : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mMultiSelectionArray:Ljava/util/ArrayList;

    new-instance v9, Ljava/lang/Integer;

    invoke-direct {v9, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1160
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v8, v0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->onChangeMultiSelection(Landroid/view/View;I)V

    goto/16 :goto_1
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 378
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 379
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setDefaultCount(I)V

    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v1

    if-gt v0, v1, :cond_1

    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 382
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    if-ne v0, v1, :cond_0

    .line 388
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    goto :goto_0
.end method

.method protected onExpandButtonClick()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 443
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v3

    if-le v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setSoundEffectsEnabled(Z)V

    .line 445
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-ne v0, v4, :cond_2

    .line 447
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mPreviousShowMode:I

    .line 448
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->expandPartialItemList()V

    .line 464
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 443
    goto :goto_0

    .line 449
    :cond_2
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-nez v0, :cond_3

    .line 450
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mPreviousShowMode:I

    .line 451
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->expandPartialItemList()V

    goto :goto_1

    .line 453
    :cond_3
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mPreviousShowMode:I

    if-ne v0, v2, :cond_4

    .line 454
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 458
    :goto_2
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-nez v0, :cond_5

    .line 459
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->foldedDefaultItemList()V

    goto :goto_1

    .line 456
    :cond_4
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mPreviousShowMode:I

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    goto :goto_2

    .line 460
    :cond_5
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-ne v0, v4, :cond_0

    .line 461
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->closeCategoryList()V

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/4 v1, 0x0

    .line 404
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mChangedExpandableState:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    if-eqz v0, :cond_0

    .line 405
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mChangedExpandableState:Z

    .line 406
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;->onChangeExpandMode(Landroid/view/View;Z)V

    .line 409
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 410
    return-void
.end method

.method public pause()V
    .locals 0

    .prologue
    .line 414
    return-void
.end method

.method public prepareMultiSelection()V
    .locals 1

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mMultiSelectionArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1115
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mMultiSelectionArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1119
    :goto_0
    return-void

    .line 1117
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mMultiSelectionArray:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public refreshCategoryAfterCollapse(Z)V
    .locals 6
    .param p1, "bBelowView"    # Z

    .prologue
    .line 894
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v0

    .line 895
    .local v0, "first":I
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v1

    .line 896
    .local v1, "last":I
    if-eqz p1, :cond_3

    .line 897
    if-eqz v0, :cond_0

    .line 898
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    .line 899
    const/4 v0, 0x0

    .line 901
    :cond_0
    if-gez v1, :cond_2

    .line 902
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getMaxLineCount()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getColumnCount()I

    move-result v5

    mul-int v3, v4, v5

    .line 903
    .local v3, "visible_thread":I
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 904
    move v1, v3

    .line 924
    .end local v3    # "visible_thread":I
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListByPage(ZII)V

    .line 925
    return-void

    .line 906
    :cond_2
    iget v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    .line 907
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getMaxLineCount()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getColumnCount()I

    move-result v5

    mul-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 908
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    goto :goto_0

    .line 912
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 913
    .local v2, "lastRow":I
    const/4 v4, -0x1

    if-ne v1, v4, :cond_4

    .line 914
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 915
    move v1, v2

    .line 917
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getMaxLineCount()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getColumnCount()I

    move-result v5

    mul-int/2addr v4, v5

    sub-int v0, v2, v4

    .line 918
    if-gez v0, :cond_5

    .line 919
    const/4 v0, 0x0

    .line 921
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    goto :goto_0
.end method

.method public refreshCurrentVisible()V
    .locals 4

    .prologue
    .line 888
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v0

    .line 889
    .local v0, "first":I
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v1

    .line 890
    .local v1, "last":I
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListByPage(ZII)V

    .line 891
    return-void
.end method

.method public refreshData()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 751
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v0

    .line 753
    .local v0, "adapter":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    if-eqz v0, :cond_2

    .line 754
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getEntireCount()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    .line 756
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v4

    if-le v1, v4, :cond_3

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->setSoundEffectsEnabled(Z)V

    .line 758
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-eq v1, v5, :cond_0

    .line 760
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v3

    if-gt v1, v3, :cond_4

    .line 761
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 762
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 763
    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setExpandState(I)V

    .line 769
    :cond_0
    :goto_1
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-eq v1, v5, :cond_1

    .line 771
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    iget v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    sub-int/2addr v1, v3

    if-lez v1, :cond_5

    .line 772
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 779
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mNumberTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 781
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->setAdapter(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)V

    .line 783
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->compositCategoryList()V

    .line 787
    :cond_2
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    return v1

    :cond_3
    move v1, v2

    .line 756
    goto :goto_0

    .line 765
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 774
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 775
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->refreshExpandedCount(II)V

    goto :goto_2
.end method

.method public refreshViewInScreen()V
    .locals 15

    .prologue
    .line 813
    iget-object v13, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v3

    .line 815
    .local v3, "cnt":I
    const/4 v10, 0x0

    .line 816
    .local v10, "view":Landroid/view/View;
    const/4 v2, 0x0

    .line 817
    .local v2, "bShowItem":Z
    const/4 v13, 0x2

    new-array v8, v13, [I

    .line 818
    .local v8, "loc":[I
    const/4 v5, -0x1

    .local v5, "first":I
    const/4 v7, -0x1

    .line 819
    .local v7, "last":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "window"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/WindowManager;

    .line 820
    .local v12, "wm":Landroid/view/WindowManager;
    invoke-interface {v12}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 821
    .local v4, "display":Landroid/view/Display;
    new-instance v9, Landroid/graphics/Point;

    invoke-direct {v9}, Landroid/graphics/Point;-><init>()V

    .line 822
    .local v9, "size":Landroid/graphics/Point;
    invoke-virtual {v4, v9}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 824
    const/4 v0, 0x0

    .line 825
    .local v0, "FIRST_START_LINE":I
    iget v13, v9, Landroid/graphics/Point;->y:I

    add-int/lit8 v1, v13, 0x64

    .line 827
    .local v1, "LAST_END_LINE":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v3, :cond_0

    .line 828
    iget-object v13, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v13, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 829
    invoke-virtual {v10, v8}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 830
    const/4 v13, 0x1

    aget v13, v8, v13

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v14

    add-int v11, v13, v14

    .line 833
    .local v11, "viewEndY":I
    if-nez v6, :cond_2

    const/4 v13, 0x1

    aget v13, v8, v13

    iget v14, v9, Landroid/graphics/Point;->y:I

    if-le v13, v14, :cond_2

    .line 834
    iget-object v13, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->releaseItemWithoutShowing()V

    .line 870
    .end local v11    # "viewEndY":I
    :cond_0
    :goto_1
    const/4 v13, -0x1

    if-eq v5, v13, :cond_1

    const/4 v13, -0x1

    if-eq v7, v13, :cond_1

    .line 871
    iget-object v13, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14, v5, v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->updateListByPage(ZII)V

    .line 873
    :cond_1
    return-void

    .line 837
    .restart local v11    # "viewEndY":I
    :cond_2
    const/4 v13, 0x1

    aget v13, v8, v13

    if-gez v13, :cond_5

    if-lez v11, :cond_5

    .line 838
    if-nez v2, :cond_3

    .line 839
    move v5, v6

    .line 841
    :cond_3
    const/4 v2, 0x1

    .line 842
    add-int/lit8 v13, v3, -0x1

    if-ne v6, v13, :cond_4

    .line 843
    move v7, v6

    .line 827
    :cond_4
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 845
    :cond_5
    const/4 v13, 0x1

    aget v13, v8, v13

    if-ltz v13, :cond_8

    if-gt v11, v1, :cond_8

    .line 846
    if-nez v2, :cond_6

    .line 847
    const/4 v2, 0x1

    .line 848
    move v5, v6

    .line 850
    :cond_6
    add-int/lit8 v13, v3, -0x1

    if-eq v6, v13, :cond_7

    iget v13, v9, Landroid/graphics/Point;->y:I

    if-ne v11, v13, :cond_4

    .line 851
    :cond_7
    move v7, v6

    goto :goto_2

    .line 854
    :cond_8
    const/4 v13, 0x1

    aget v13, v8, v13

    if-ge v13, v1, :cond_a

    if-le v11, v1, :cond_a

    .line 855
    if-nez v2, :cond_9

    .line 856
    const/4 v2, 0x1

    .line 857
    move v5, v6

    .line 859
    :cond_9
    move v7, v6

    goto :goto_2

    .line 861
    :cond_a
    if-eqz v2, :cond_4

    .line 862
    if-gtz v7, :cond_b

    .line 863
    add-int/lit8 v7, v6, -0x1

    .line 865
    :cond_b
    const/4 v2, 0x0

    .line 866
    goto :goto_1
.end method

.method public releaseAllItem()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 876
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v2

    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->releaseViewWhenScreenOut(IIII)V

    .line 878
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    .line 879
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    .line 880
    return-void
.end method

.method public releaseVisibleItem()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 883
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getFirstVisibleIndex()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getLastVisibleIndex()I

    move-result v2

    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->releaseViewWhenScreenOut(IIII)V

    .line 885
    return-void
.end method

.method public replaceData(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 741
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 742
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->replaceItems(Ljava/util/ArrayList;)V

    .line 744
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->refreshData()I

    .line 748
    :goto_0
    return-void

    .line 746
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setInfoFlag(I)V
    .locals 4
    .param p1, "flag"    # I

    .prologue
    .line 432
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mNumberTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mHeaderView:Landroid/view/ViewGroup;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getCategoryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e004c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 436
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mNumberTextView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 439
    :cond_0
    return-void
.end method

.method public setViewMode(I)V
    .locals 5
    .param p1, "categoryViewMode"    # I

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 597
    packed-switch p1, :pswitch_data_0

    .line 662
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown view mode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    :cond_0
    :goto_0
    return-void

    .line 599
    :pswitch_0
    const/16 v1, 0x97

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewMode:I

    .line 601
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->enable()V

    .line 603
    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setSelectionMode(Z)V

    goto :goto_0

    .line 607
    :pswitch_1
    const/16 v1, 0x98

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewMode:I

    .line 609
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->enable()V

    .line 611
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setSelectionMode(Z)V

    goto :goto_0

    .line 615
    :pswitch_2
    const/16 v1, 0x99

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewMode:I

    .line 617
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->disable()V

    .line 619
    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setSelectionMode(Z)V

    goto :goto_0

    .line 623
    :pswitch_3
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewMode:I

    .line 625
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-ne v1, v2, :cond_3

    .line 626
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mPreviousShowMode:I

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 631
    :goto_1
    const/4 v0, 0x0

    .line 632
    .local v0, "bRemake":Z
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_4

    .line 634
    :cond_1
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 635
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setExpandState(I)V

    .line 636
    const/4 v0, 0x1

    .line 643
    :cond_2
    :goto_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->expandCategoryListView(Z)V

    .line 645
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-ne v1, v4, :cond_0

    .line 646
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 628
    .end local v0    # "bRemake":Z
    :cond_3
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mPreviousShowMode:I

    goto :goto_1

    .line 637
    .restart local v0    # "bRemake":Z
    :cond_4
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-ne v1, v4, :cond_2

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v2

    if-gt v1, v2, :cond_2

    .line 639
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    goto :goto_2

    .line 650
    .end local v0    # "bRemake":Z
    :pswitch_4
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewMode:I

    .line 651
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 653
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mPreviousShowMode:I

    .line 655
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 658
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 659
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->collapseCategoryListView()V

    goto :goto_0

    .line 597
    :pswitch_data_0
    .packed-switch 0x97
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public setViewStateListener(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    .prologue
    .line 706
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    .line 707
    return-void
.end method

.method public updateCategoryWhenRotate(Z)V
    .locals 5
    .param p1, "bAfter"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 996
    if-eqz p1, :cond_4

    .line 998
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 999
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->expandCategoryListView(Z)V

    .line 1002
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    if-ne v0, v3, :cond_2

    .line 1003
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1005
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isFullyExpanded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1006
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1014
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mEntireItemCount:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v1

    if-gt v0, v1, :cond_3

    .line 1015
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1025
    :goto_1
    return-void

    .line 1008
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 1011
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFirstVisibleIndex(I)V

    .line 1012
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getDefaultItemCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLastVisibleIndex(I)V

    goto :goto_0

    .line 1017
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1020
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandControlView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1022
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1023
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->collapseCategoryListView()V

    goto :goto_1
.end method
