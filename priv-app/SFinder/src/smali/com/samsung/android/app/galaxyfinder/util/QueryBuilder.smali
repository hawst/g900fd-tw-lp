.class public Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;
.super Ljava/lang/Object;
.source "QueryBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;
    }
.end annotation


# static fields
.field private static final AND:Ljava/lang/String; = "&"

.field private static final END_BLOCK:Ljava/lang/String; = "]"

.field private static final OR:Ljava/lang/String; = "|"

.field private static final REGEX_AND:Ljava/lang/String; = "\\&+"

.field private static final REGEX_OPERATORS:Ljava/lang/String; = "\\&+|\\|+|\\.+"

.field private static final REGEX_OR:Ljava/lang/String; = "\\|+"

.field private static final REGEX_SPACE:Ljava/lang/String; = "\\p{Space}+"

.field private static final SPACE_OP:Ljava/lang/String; = "."

.field private static final SP_AND_SP:Ljava/lang/String; = " & "

.field private static final SP_OR_SP:Ljava/lang/String; = " | "

.field private static final SP_SPACE_OP_SP:Ljava/lang/String; = " . "

.field private static final START_BLOCK:Ljava/lang/String; = "["


# instance fields
.field private mPatternOp:Ljava/util/regex/Pattern;

.field private mPriority:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

.field private mSb:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;)V
    .locals 1
    .param p1, "p"    # Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    .line 14
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mPatternOp:Ljava/util/regex/Pattern;

    .line 16
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->AND:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mPriority:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    .line 48
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mPriority:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    .line 49
    const-string v0, "\\&+|\\|+|\\.+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mPatternOp:Ljava/util/regex/Pattern;

    .line 50
    return-void
.end method

.method private changeSpaceToOp(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 53
    const-string v0, "\\p{Space}+"

    const-string v1, " . "

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 54
    return-object p1
.end method

.method private getPriorityOp(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "tmpOp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mPriority:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    sget-object v3, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->OR:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    if-ne v2, v3, :cond_5

    .line 70
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 71
    .local v1, "op":Ljava/lang/String;
    const-string v2, "|"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    const-string v2, "|"

    .line 96
    .end local v1    # "op":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 74
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 75
    .restart local v1    # "op":Ljava/lang/String;
    const-string v2, "&"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 76
    const-string v2, "&"

    goto :goto_0

    .line 78
    .end local v1    # "op":Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 79
    .restart local v1    # "op":Ljava/lang/String;
    const-string v2, "."

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 80
    const-string v2, "|"

    goto :goto_0

    .line 83
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "op":Ljava/lang/String;
    :cond_5
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 84
    .restart local v1    # "op":Ljava/lang/String;
    const-string v2, "&"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 85
    const-string v2, "&"

    goto :goto_0

    .line 87
    .end local v1    # "op":Ljava/lang/String;
    :cond_7
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 88
    .restart local v1    # "op":Ljava/lang/String;
    const-string v2, "|"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 89
    const-string v2, "|"

    goto :goto_0

    .line 91
    .end local v1    # "op":Ljava/lang/String;
    :cond_9
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 92
    .restart local v1    # "op":Ljava/lang/String;
    const-string v2, "."

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 93
    const-string v2, "&"

    goto/16 :goto_0

    .line 96
    .end local v1    # "op":Ljava/lang/String;
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private insertSpaceBetweenOp(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 58
    const-string v0, "\\&+"

    const-string v1, " & "

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 59
    const-string v0, "\\|+"

    const-string v1, " | "

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 60
    return-object p1
.end method

.method private isOp(Ljava/lang/String;)Z
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mPatternOp:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 65
    .local v0, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    return v1
.end method

.method private makeBlock(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "bReplaceOp"    # Z

    .prologue
    .line 106
    if-eqz p1, :cond_0

    .line 107
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    if-eqz p2, :cond_1

    .line 110
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->replaceOp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    :cond_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private replaceOp(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 100
    const-string v0, "\\&+"

    const-string v1, "&"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 101
    const-string v0, "\\|+"

    const-string v1, "|"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 102
    return-object p1
.end method


# virtual methods
.method public replace(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 119
    const-string v4, "&"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "|"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "["

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "]"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "."

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 121
    :cond_0
    const/4 v4, 0x0

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->makeBlock(Ljava/lang/String;Z)V

    .line 122
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 149
    :goto_0
    return-object v4

    .line 125
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->changeSpaceToOp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 126
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->insertSpaceBetweenOp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 127
    const-string v4, "SearchQuery"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Insert Space between operations : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v3, "tmpOp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, "\\p{Space}+"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "strArray":[Ljava/lang/String;
    array-length v1, v2

    .line 133
    .local v1, "nArrayConut":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_4

    .line 135
    aget-object v4, v2, v0

    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->isOp(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 136
    aget-object v4, v2, v0

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    add-int/lit8 v4, v1, -0x1

    if-ge v0, v4, :cond_2

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v2, v4

    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->isOp(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 139
    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->getPriorityOp(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v7}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->makeBlock(Ljava/lang/String;Z)V

    .line 140
    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 133
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 145
    :cond_3
    aget-object v4, v2, v0

    invoke-direct {p0, v4, v7}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->makeBlock(Ljava/lang/String;Z)V

    goto :goto_2

    .line 148
    :cond_4
    const-string v4, "SearchQuery"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Built Query : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->mSb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
