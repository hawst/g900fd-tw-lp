.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;
.super Landroid/preference/Preference;
.source "GalaxyFinderSettingsActivity.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$Accessibility;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SettingsPreferenceDefault"
.end annotation


# instance fields
.field private mContentDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->mContentDescription:Ljava/lang/String;

    .line 140
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->init()V

    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->mContentDescription:Ljava/lang/String;

    .line 145
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->init()V

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->mContentDescription:Ljava/lang/String;

    .line 151
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->setTitle(Ljava/lang/CharSequence;)V

    .line 152
    invoke-virtual {p0, p3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->setKey(Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 172
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->setEnabled(Z)V

    .line 173
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->setSelectable(Z)V

    .line 174
    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 157
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 159
    const v1, 0x1020016

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 161
    .local v0, "title":Landroid/view/View;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->mContentDescription:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->mContentDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 164
    :cond_0
    return-void
.end method

.method public setContentDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->mContentDescription:Ljava/lang/String;

    .line 169
    return-void
.end method
