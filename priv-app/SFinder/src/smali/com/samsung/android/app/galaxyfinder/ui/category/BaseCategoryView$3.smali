.class Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;
.super Ljava/lang/Object;
.source "BaseCategoryView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addHeaderView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 259
    move-object v1, p1

    check-cast v1, Landroid/widget/CheckBox;

    .line 262
    .local v1, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 263
    .local v0, "check":Z
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setCheckedAllItems(ZZ)I

    move-result v2

    .line 265
    .local v2, "count":I
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 266
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->notifyDataSetChanged()V

    .line 267
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->refreshCheckBoxAllItem()V

    .line 270
    :cond_0
    if-eqz v0, :cond_2

    .line 271
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I
    invoke-static {v3, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$002(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;I)I

    .line 276
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 277
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    iget-object v5, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;->onChangeSelectedCount(ILjava/lang/String;)V

    .line 280
    :cond_1
    return-void

    .line 273
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$002(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;I)I

    goto :goto_0
.end method
