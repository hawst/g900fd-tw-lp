.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListDefaultAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 23
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x1

    .line 27
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v3

    .line 28
    .local v3, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v2, 0x0

    .line 30
    .local v2, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;
    invoke-virtual {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 32
    if-nez p2, :cond_0

    .line 33
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f030037

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 35
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;

    .end local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;
    invoke-direct {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;-><init>()V

    .line 37
    .restart local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;
    const v6, 0x7f0b004f

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 38
    const v6, 0x7f0b005d

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    .line 40
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 45
    :goto_0
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v5

    .line 46
    .local v5, "title":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "desc":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v1

    .line 49
    .local v1, "genericIntent":Landroid/content/Intent;
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {p0, v6, v5, v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 51
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 52
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    invoke-virtual {p0, v6, v0, v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 61
    :goto_1
    invoke-virtual {p0, p2, v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 63
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    return-object v6

    .line 42
    .end local v0    # "desc":Ljava/lang/String;
    .end local v1    # "genericIntent":Landroid/content/Intent;
    .end local v5    # "title":Ljava/lang/String;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;
    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;

    .restart local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;
    goto :goto_0

    .line 54
    .restart local v0    # "desc":Ljava/lang/String;
    .restart local v1    # "genericIntent":Landroid/content/Intent;
    .restart local v5    # "title":Ljava/lang/String;
    :cond_1
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v4, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 56
    .local v4, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v6, 0xf

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 58
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListDefaultAdapter$DefaultItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method
