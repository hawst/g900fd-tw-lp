.class public Lcom/samsung/android/app/galaxyfinder/voicesearch/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ACTION_SEC_DIALOG_ACTIVITY_FOR_NON_ACTIVITI_CREATED:Ljava/lang/String; = "com.sec.android.app.translator.DIALOG_ACTIVITY_FOR_NON_ACTIVITI_CREATED"

.field public static final ACTION_SEC_HELP:Ljava/lang/String; = "com.samsung.helphub.HELP"

.field public static final ACTION_SEC_MAIN_FRAGMENT_FINISHED:Ljava/lang/String; = "com.sec.android.app.translator.MAIN_FRAGMENT_FINISHED"

.field public static final ACTION_SEC_TRANSLATE:Ljava/lang/String; = "com.sec.android.app.translator.TRANSLATE"

.field public static final ACTION_SEC_TRANSLATE_FOR_NON_ACTIVITY:Ljava/lang/String; = "com.sec.android.app.translator.TRANSLATE_FOR_NON_ACTIVITY"

.field public static final ACTION_SEC_TRANSLATE_GET_SUPPORTED_LANGUAGES:Ljava/lang/String; = "com.sec.android.app.translator.GET_SUPPORTED_LANGUAGES"

.field public static final ACTION_SEC_TRANSLATE_GET_SUPPORTED_LANGUAGES_RESULT:Ljava/lang/String; = "com.sec.android.app.translator.GET_SUPPORTED_LANGUAGES_RESULT"

.field public static final ACTION_SEC_TRANSLATE_RESULT:Ljava/lang/String; = "com.sec.android.app.translator.TRANSLATE_RESULT"

.field public static final ACTION_SEC_TRANSLATE_START_HELP_MODE:Ljava/lang/String; = "com.sec.android.app.translator.START_HELP_MODE"

.field public static final DRAWER_MENU_INDEX_CONVERSATION:I = 0x4

.field public static final DRAWER_MENU_INDEX_FAVORITES:I = 0x2

.field public static final DRAWER_MENU_INDEX_HISTORY:I = 0x1

.field public static final DRAWER_MENU_INDEX_PHRASES:I = 0x3

.field public static final DRAWER_MENU_INDEX_TRANSLATOR:I = 0x0

.field public static final ERROR_ALREADY_EXISTED:I = -0x3ea

.field public static final ERROR_FAIL:I = -0x3e9

.field public static final ERROR_IN_CALL:I = -0x3eb

.field public static final ERROR_SERVER_ACCOUNT_FAIL:I = 0x3e9

.field public static final ERROR_SERVER_AUTH_ERROR:I = 0x3ef

.field public static final ERROR_SERVER_EXCEPTION_ERROR:I = 0x3f1

.field public static final ERROR_SERVER_NETWORK_ERROR:I = 0x3eb

.field public static final ERROR_SERVER_NOT_SUPPORTED_LANGUAGE:I = 0x3ea

.field public static final ERROR_SERVER_SERVER_ERROR:I = 0x3ec

.field public static final ERROR_UNKNOWN:I = -0x3e8

.field public static final ERROR_UNSUPPORTED_LANGUAGE:I = -0x3ec

.field public static final EXTRA_NAME_AUTO_START_TRANSLATION:Ljava/lang/String; = "auto_start_translation"

.field public static final EXTRA_NAME_CALLER:Ljava/lang/String; = "caller"

.field public static final EXTRA_NAME_CATEGORY_ID:Ljava/lang/String; = "category_id"

.field public static final EXTRA_NAME_CURRENT_MENU_INDEX:Ljava/lang/String; = "current_menu_index"

.field public static final EXTRA_NAME_DO_NOT_USE_SELECT_RESULT_DIALOG:Ljava/lang/String; = "do_not_use_select_result_dialog"

.field public static final EXTRA_NAME_EDITABLE_SOURCETEXT:Ljava/lang/String; = "editable_source_text"

.field public static final EXTRA_NAME_ENABLE_SHARE_VIA_MENU:Ljava/lang/String; = "enable_share_via_menu"

.field public static final EXTRA_NAME_GET_ALWAYS_SOURCE_AND_TARGET_TEXT:Ljava/lang/String; = "get_always_source_and_target_text"

.field public static final EXTRA_NAME_HELP_MODE:Ljava/lang/String; = "helpMode"

.field public static final EXTRA_NAME_HTML_SOURCE_TEXT:Ljava/lang/String; = "html_source_text"

.field public static final EXTRA_NAME_ITEM_ID:Ljava/lang/String; = "item_id"

.field public static final EXTRA_NAME_KEYWORD_ID:Ljava/lang/String; = "keyword_id"

.field public static final EXTRA_NAME_MODE:Ljava/lang/String; = "mode"

.field public static final EXTRA_NAME_RESULT_CODE:Ljava/lang/String; = "result_code"

.field public static final EXTRA_NAME_SELECT_SOURCE_TEXT:Ljava/lang/String; = "select_source_text"

.field public static final EXTRA_NAME_SELECT_TARGET_TEXT:Ljava/lang/String; = "select_target_text"

.field public static final EXTRA_NAME_SOURCE_LANGUAGE:Ljava/lang/String; = "source_language"

.field public static final EXTRA_NAME_SOURCE_TEXT:Ljava/lang/String; = "source_text"

.field public static final EXTRA_NAME_SUPPORTED_LANGUAGES:Ljava/lang/String; = "supported_languages"

.field public static final EXTRA_NAME_TABLE:Ljava/lang/String; = "table"

.field public static final EXTRA_NAME_TARGET_LANGUAGE:Ljava/lang/String; = "target_language"

.field public static final EXTRA_NAME_TARGET_TEXT:Ljava/lang/String; = "target_text"

.field public static final EXTRA_NAME_TRANSLATION_ENGINE_TYPE:Ljava/lang/String; = "translation_engine_type"

.field public static final EXTRA_NAME_USE_BROADCAST_RECEIVER:Ljava/lang/String; = "use_broadcast_receiver"

.field public static final EXTRA_NAME_USE_CONTENT_PROVIDER:Ljava/lang/String; = "use_content_provider"

.field public static final EXTRA_VALUE_ENGINE_GENERAL_TYPE:I = 0x1

.field public static final EXTRA_VALUE_ENGINE_TECHNICAL_TYPE:I = 0x3

.field public static final EXTRA_VALUE_HELP_MODE_PHRASES:Ljava/lang/String; = "searchPhrases"

.field public static final EXTRA_VALUE_HELP_MODE_TRANSLATE:Ljava/lang/String; = "translate"

.field public static final EXTRA_VALUE_INPUT_MODE:Ljava/lang/String; = "input"

.field public static final EXTRA_VALUE_MODE_DELETE:I = 0x0

.field public static final EXTRA_VALUE_MODE_SHARE:I = 0x1

.field public static final EXTRA_VALUE_RESULT_CANCEL:I = 0x0

.field public static final EXTRA_VALUE_RESULT_OK:I = 0x1

.field public static final EXTRA_VALUE_VIEWER_MODE:Ljava/lang/String; = "viewer"

.field public static final HELP_APPLICATION_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.helphub"

.field public static final IS_MOVABLE_KEYPAD:Ljava/lang/String; = "AxT9IME.isMovableKeypad"

.field public static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field public static final LANGUAGE_ARRAY_FULL:[[Ljava/lang/String;

.field public static final LANGUAGE_CODE_AUTO:Ljava/lang/String; = "unknown"

.field public static final LANGUAGE_CODE_CHINESE:Ljava/lang/String; = "zh-CN"

.field public static final LANGUAGE_CODE_ENGLISH_UK:Ljava/lang/String; = "en-GB"

.field public static final LANGUAGE_CODE_ENGLISH_US:Ljava/lang/String; = "en-US"

.field public static final LANGUAGE_CODE_FRENCH:Ljava/lang/String; = "fr-FR"

.field public static final LANGUAGE_CODE_GERMAN:Ljava/lang/String; = "de-DE"

.field public static final LANGUAGE_CODE_ITALIAN:Ljava/lang/String; = "it-IT"

.field public static final LANGUAGE_CODE_JAPANESE:Ljava/lang/String; = "ja-JP"

.field public static final LANGUAGE_CODE_KOREAN:Ljava/lang/String; = "ko-KR"

.field public static final LANGUAGE_CODE_PORTUGUESE_BR:Ljava/lang/String; = "pt-BR"

.field public static final LANGUAGE_CODE_SPANISH:Ljava/lang/String; = "es-ES"

.field public static final PREF_KEY_IS_FIRST_LAUNCHED:Ljava/lang/String; = "is_first_launched"

.field public static final PREF_KEY_MODULE_LAST_SOURCE_LANGUAGE:Ljava/lang/String; = "key_module_last_source_language"

.field public static final PREF_KEY_MODULE_LAST_TARGET_LANGUAGE:Ljava/lang/String; = "key_module_last_target_language"

.field public static final PREF_KEY_READOUT_AFTER_TRANSLATION:Ljava/lang/String; = "readout_after_translation"

.field public static final PREF_KEY_SHOW_ADDITIONAL_CHARGES_MSG:Ljava/lang/String; = "key_show_additional_charges_msg"

.field public static final PREF_KEY_SHOW_ADDITIONAL_ROAMING_CHARGES_MSG:Ljava/lang/String; = "key_show_additional_roaming_charges_msg"

.field public static final PREF_KEY_SHOW_FULLSCREEN_INFO_MSG:Ljava/lang/String; = "key_show_fullscreen_info_msg"

.field public static final PREF_KEY_SORT_ORDER_HISTORY:Ljava/lang/String; = "key_sort_order_history"

.field public static final PREF_KEY_SORT_ORDER_STARRED:Ljava/lang/String; = "key_sort_order_starred"

.field public static final PREF_KEY_SOURCE_LANGUAGE:Ljava/lang/String; = "key_source_language"

.field public static final PREF_KEY_TARGET_LANGUAGE:Ljava/lang/String; = "key_target_language"

.field public static final PREF_KEY_TTS_SPEECH_RATE:Ljava/lang/String; = "key_tts_speech_rate"

.field public static final PREF_VALUE_SORT_BY_ALPHABETS:I = 0x1

.field public static final PREF_VALUE_SORT_BY_DEFAULT:I = 0x3

.field public static final PREF_VALUE_SORT_BY_FREQUENCY:I = 0x2

.field public static final PREF_VALUE_SORT_BY_TIME:I = 0x3

.field public static final PREF_VALUE_SORT_BY_TIME_REVERSE:I = 0x4

.field public static final REQUEST_AXT9INFO:Ljava/lang/String; = "RequestAxT9Info"

.field public static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field public static final SUCCESS:I = 0x3e8

.field public static final TAG:Ljava/lang/String; = "Translator"

.field public static final TEXT_MAX_LENGTH:I = 0x4e20

.field public static final TTS_SPEAKING_TYPE_NOTHING:I = 0x7d0

.field public static final TTS_SPEAKING_TYPE_SOURCE:I = 0x7d1

.field public static final TTS_SPEAKING_TYPE_TARGET:I = 0x7d2

.field public static final URI:Ljava/lang/String; = "content://com.sec.android.app.translator.provider/data"


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 135
    const/16 v0, 0xc

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "zh-CN"

    aput-object v2, v1, v4

    const-string v2, "en-US"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "zh-CN"

    aput-object v2, v1, v4

    const-string v2, "ja-JP"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "zh-CN"

    aput-object v2, v1, v4

    const-string v2, "ko-KR"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/4 v1, 0x3

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "en-US"

    aput-object v3, v2, v4

    const-string v3, "zh-CN"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "en-US"

    aput-object v3, v2, v4

    const-string v3, "ja-JP"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "en-US"

    aput-object v3, v2, v4

    const-string v3, "ko-KR"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ja-JP"

    aput-object v3, v2, v4

    const-string v3, "zh-CN"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ja-JP"

    aput-object v3, v2, v4

    const-string v3, "en-US"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ja-JP"

    aput-object v3, v2, v4

    const-string v3, "ko-KR"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ko-KR"

    aput-object v3, v2, v4

    const-string v3, "zh-CN"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ko-KR"

    aput-object v3, v2, v4

    const-string v3, "en-US"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ko-KR"

    aput-object v3, v2, v4

    const-string v3, "ja-JP"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/Constants;->LANGUAGE_ARRAY_FULL:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
