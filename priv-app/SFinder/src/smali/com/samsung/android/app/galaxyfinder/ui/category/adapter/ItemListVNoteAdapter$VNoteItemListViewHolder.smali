.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;
.super Ljava/lang/Object;
.source "ItemListVNoteAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VNoteItemListViewHolder"
.end annotation


# instance fields
.field public ivIcon:Landroid/widget/ImageView;

.field public ivPersonal:Landroid/widget/ImageView;

.field public tvColorBar:Landroid/view/View;

.field public tvDate:Landroid/widget/TextView;

.field public tvName:Landroid/widget/TextView;

.field public tvTime:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 123
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvDate:Landroid/widget/TextView;

    .line 125
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvTime:Landroid/widget/TextView;

    .line 127
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 129
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->tvColorBar:Landroid/view/View;

    .line 131
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVNoteAdapter$VNoteItemListViewHolder;->ivPersonal:Landroid/widget/ImageView;

    return-void
.end method
