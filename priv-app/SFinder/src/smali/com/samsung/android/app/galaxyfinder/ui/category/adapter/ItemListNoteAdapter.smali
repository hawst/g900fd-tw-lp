.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListNoteAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;
    }
.end annotation


# static fields
.field private static final SNOTE_TYPE_ICON_CLOUD:I = 0x20

.field private static final SNOTE_TYPE_ICON_FAVORITE:I = 0x10

.field private static final SNOTE_TYPE_ICON_LOCK:I = 0x40

.field private static final SNOTE_TYPE_ICON_RECORD:I = 0x4

.field private static final SNOTE_TYPE_ICON_TAG:I = 0x2

.field private static final TEMPLATE_SNOTE_SUBTYPE_ACTIONMEMO:I = 0x3

.field private static final TEMPLATE_SNOTE_SUBTYPE_BOOK:I = 0x1

.field private static final TEMPLATE_SNOTE_SUBTYPE_CUSTOM:I = 0x5

.field private static final TEMPLATE_SNOTE_SUBTYPE_FOLDER:I = 0x2

.field private static final TEMPLATE_SNOTE_SUBTYPE_TRANSPARENT_COVER:I = 0x4


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 43
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 25
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 47
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v20

    .line 48
    .local v20, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/16 v17, 0x0

    .line 50
    .local v17, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v24

    .line 51
    .local v24, "title":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v13

    .line 52
    .local v13, "date":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v14

    .line 53
    .local v14, "desc":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v6

    .line 55
    .local v6, "icon1":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v23

    .line 56
    .local v23, "targetType":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v16

    .line 58
    .local v16, "extraFlags":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v19

    .line 60
    .local v19, "intent":Landroid/content/Intent;
    const/16 v22, 0x0

    .line 61
    .local v22, "subType":I
    const/16 v18, 0x0

    .line 63
    .local v18, "iconSet":I
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 65
    if-eqz v23, :cond_0

    .line 67
    :try_start_0
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v22

    .line 73
    :cond_0
    :goto_0
    if-eqz v16, :cond_1

    .line 75
    :try_start_1
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v18

    .line 81
    :cond_1
    :goto_1
    if-nez p2, :cond_7

    .line 82
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    .line 83
    .local v21, "res":Landroid/content/res/Resources;
    new-instance v17, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;

    .end local v17    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;
    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;-><init>()V

    .line 84
    .restart local v17    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030042

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 86
    packed-switch v22, :pswitch_data_0

    .line 109
    :goto_2
    :pswitch_0
    const v4, 0x7f0b004b

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 110
    const v4, 0x7f0b0083

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivTag:Landroid/widget/ImageView;

    .line 111
    const v4, 0x7f0b0084

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivRecord:Landroid/widget/ImageView;

    .line 112
    const v4, 0x7f0b0085

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivFavorite:Landroid/widget/ImageView;

    .line 113
    const v4, 0x7f0b0086

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivCloud:Landroid/widget/ImageView;

    .line 114
    const v4, 0x7f0b0066

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivLock:Landroid/widget/ImageView;

    .line 116
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 121
    .end local v21    # "res":Landroid/content/res/Resources;
    :goto_3
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v1, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 122
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v14, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 124
    and-int/lit8 v4, v18, 0x2

    if-lez v4, :cond_2

    .line 125
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivTag:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    :cond_2
    and-int/lit8 v4, v18, 0x4

    if-lez v4, :cond_3

    .line 128
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivRecord:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 130
    :cond_3
    and-int/lit8 v4, v18, 0x10

    if-lez v4, :cond_4

    .line 131
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivFavorite:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 133
    :cond_4
    and-int/lit8 v4, v18, 0x20

    if-lez v4, :cond_5

    .line 134
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivCloud:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 136
    :cond_5
    and-int/lit8 v4, v18, 0x40

    if-lez v4, :cond_6

    .line 137
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivLock:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 140
    :cond_6
    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/16 v10, 0xcb

    const/4 v11, 0x0

    const/16 v12, 0x168

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v12}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 143
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    move/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 144
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2, v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;->setGridViewDesc(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    return-object v4

    .line 68
    :catch_0
    move-exception v15

    .line 69
    .local v15, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v15}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_0

    .line 76
    .end local v15    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v15

    .line 77
    .restart local v15    # "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v15}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_1

    .line 90
    .end local v15    # "e":Ljava/lang/NumberFormatException;
    .restart local v21    # "res":Landroid/content/res/Resources;
    :pswitch_1
    const v4, 0x7f0b0070

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    goto/16 :goto_2

    .line 94
    :pswitch_2
    const v4, 0x7f0b0081

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 95
    const v4, 0x7f0b0082

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    goto/16 :goto_2

    .line 99
    :pswitch_3
    const v4, 0x7f0b0080

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 101
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const v5, 0x7f020143

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 118
    .end local v21    # "res":Landroid/content/res/Resources;
    :cond_7
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;
    check-cast v17, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;

    .restart local v17    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;
    goto/16 :goto_3

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method
