.class public Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;
.super Lcom/samsung/android/app/galaxyfinder/tag/probe/DirectTagProbe;
.source "PeopleTagProbe.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$1;,
        Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;,
        Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    }
.end annotation


# static fields
.field private static final FACE_TAG_ME:Ljava/lang/String; = "profile/Me"

.field private static final GALLERY_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.gallery3d"

.field private static final MEDIA_PROVIDER_FACES_URI:Landroid/net/Uri;

.field private static final MEDIA_PROVIDER_PERSONS_URI:Landroid/net/Uri;

.field private static final THIS_IS_NOT_FACE_TAGGED:I = 0x1


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "content://media/external/faces"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->MEDIA_PROVIDER_FACES_URI:Landroid/net/Uri;

    .line 32
    const-string v0, "content://media/external/persons"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->MEDIA_PROVIDER_PERSONS_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagType"    # [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .param p3, "attr"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DirectTagProbe;-><init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V

    .line 28
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->MEDIA_PROVIDER_FACES_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->setContentUri(Landroid/net/Uri;)V

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->getLogTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    .line 45
    return-void
.end method

.method private getContentInfoFromImage(I)Ljava/lang/String;
    .locals 10
    .param p1, "imageID"    # I

    .prologue
    const/4 v4, 0x0

    .line 398
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 399
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v9, "datetaken"

    aput-object v9, v2, v5

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 403
    .local v8, "imageCursor":Landroid/database/Cursor;
    if-nez v8, :cond_0

    .line 404
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    const-string v1, "getContentInfoFromImage() imageCursor is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :goto_0
    return-object v4

    .line 407
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 408
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    const-string v1, "getContentInfoFromImage() Not exist data imageCursor"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 413
    :cond_1
    const/4 v6, 0x0

    .line 414
    .local v6, "datetaken":Ljava/lang/String;
    const-string v0, "datetaken"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 415
    .local v7, "idx_datetaken":I
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 416
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 418
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v4, v6

    .line 420
    goto :goto_0
.end method

.method private getNameFromImage(I)Ljava/util/ArrayList;
    .locals 10
    .param p1, "imageID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "image_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 374
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Faces;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v9, "name"

    aput-object v9, v2, v5

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 377
    .local v6, "facesCursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 378
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    const-string v1, "getNameFromImage() facesCursor is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :goto_0
    return-object v4

    .line 381
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 382
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    const-string v1, "getNameFromImage() Not exist data facesCursor"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 387
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 388
    .local v8, "nameArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 389
    .local v7, "idx_name":I
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 390
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 392
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v4, v8

    .line 394
    goto :goto_0
.end method

.method private getPersonName(I)Ljava/lang/String;
    .locals 11
    .param p1, "personID"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 342
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->MEDIA_PROVIDER_PERSONS_URI:Landroid/net/Uri;

    new-array v2, v10, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "name"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 345
    .local v6, "mpPersonsCursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 346
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    const-string v1, "getPersonName() mpPersonsCursor is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :goto_0
    return-object v4

    .line 349
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    const-string v1, "getPersonName() Not exist data mpPersonsCursor"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 355
    :cond_1
    const/4 v8, 0x0

    .line 356
    .local v8, "nameArray":[Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 357
    const-string v0, "name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 358
    .local v7, "name":Ljava/lang/String;
    const-string v0, "profile/Me"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 359
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v4, v7

    .line 360
    goto :goto_0

    .line 362
    :cond_2
    const-string v0, "/"

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 364
    .end local v7    # "name":Ljava/lang/String;
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 366
    const/4 v9, 0x0

    .line 367
    .local v9, "personName":Ljava/lang/String;
    if-eqz v8, :cond_4

    array-length v0, v8

    if-le v0, v10, :cond_4

    .line 368
    aget-object v9, v8, v10

    :cond_4
    move-object v4, v9

    .line 369
    goto :goto_0
.end method


# virtual methods
.method public collect(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;)Ljava/lang/Object;
    .locals 56
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    .line 65
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    const-string v4, "People:collect() start FACE TAG sync"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const/16 v36, 0x0

    .line 67
    .local v36, "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    const-string v6, "(person_id!=1)"

    .line 68
    .local v6, "selection":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v14, v3, [Landroid/database/Cursor;

    .line 70
    .local v14, "cursor":[Landroid/database/Cursor;
    const/16 v54, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->getQueryUri()Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v55, "_id"

    aput-object v55, v5, v7

    const/4 v7, 0x1

    const-string v55, "image_id"

    aput-object v55, v5, v7

    const/4 v7, 0x2

    const-string v55, "person_id"

    aput-object v55, v5, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    aput-object v3, v14, v54

    .line 73
    const/4 v3, 0x0

    aget-object v3, v14, v3

    if-nez v3, :cond_2

    .line 74
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->getQueryUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cursor is NULL"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v37

    .line 274
    const/4 v3, 0x0

    aget-object v3, v14, v3

    if-eqz v3, :cond_0

    .line 275
    const/4 v3, 0x0

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_0
    const/4 v3, 0x1

    aget-object v3, v14, v3

    if-eqz v3, :cond_1

    .line 277
    const/4 v3, 0x1

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 279
    :cond_1
    :goto_0
    return-object v37

    .line 77
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 78
    :goto_1
    const/4 v3, 0x0

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 79
    const/4 v3, 0x0

    aget-object v3, v14, v3

    const/4 v4, 0x0

    aget-object v4, v14, v4

    const-string v5, "_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v46

    .line 80
    .local v46, "tempID":I
    const/4 v3, 0x0

    aget-object v3, v14, v3

    const/4 v4, 0x0

    aget-object v4, v14, v4

    const-string v5, "image_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v47

    .line 81
    .local v47, "tempImageID":I
    const/4 v3, 0x0

    aget-object v3, v14, v3

    const/4 v4, 0x0

    aget-object v4, v14, v4

    const-string v5, "person_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v48

    .line 82
    .local v48, "tempPersonID":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() [in media/external/faces] id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v46

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", image = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", tempPersonID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 267
    .end local v46    # "tempID":I
    .end local v47    # "tempImageID":I
    .end local v48    # "tempPersonID":I
    :catch_0
    move-exception v43

    .line 268
    .local v43, "se":Landroid/database/sqlite/SQLiteException;
    :goto_2
    :try_start_2
    invoke-virtual/range {v43 .. v43}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 269
    const/16 v37, 0x0

    .line 274
    const/4 v3, 0x0

    aget-object v3, v14, v3

    if-eqz v3, :cond_3

    .line 275
    const/4 v3, 0x0

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_3
    const/4 v3, 0x1

    aget-object v3, v14, v3

    if-eqz v3, :cond_1

    .line 277
    const/4 v3, 0x1

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 87
    .end local v43    # "se":Landroid/database/sqlite/SQLiteException;
    :cond_4
    const/4 v3, 0x1

    :try_start_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Faces;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x5

    new-array v9, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v9, v4

    const/4 v4, 0x1

    const-string v5, "face_id"

    aput-object v5, v9, v4

    const/4 v4, 0x2

    const-string v5, "image_id"

    aput-object v5, v9, v4

    const/4 v4, 0x3

    const-string v5, "person_id"

    aput-object v5, v9, v4

    const/4 v4, 0x4

    const-string v5, "name"

    aput-object v5, v9, v4

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v12, "face_id asc"

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    aput-object v4, v14, v3

    .line 90
    const/4 v3, 0x1

    aget-object v3, v14, v3

    if-nez v3, :cond_6

    .line 91
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Faces;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cursor is NULL"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v37

    .line 274
    const/4 v3, 0x0

    aget-object v3, v14, v3

    if-eqz v3, :cond_5

    .line 275
    const/4 v3, 0x0

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_5
    const/4 v3, 0x1

    aget-object v3, v14, v3

    if-eqz v3, :cond_1

    .line 277
    const/4 v3, 0x1

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 94
    :cond_6
    :try_start_4
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 95
    :goto_3
    const/4 v3, 0x1

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 96
    const/4 v3, 0x1

    aget-object v3, v14, v3

    const/4 v4, 0x1

    aget-object v4, v14, v4

    const-string v5, "_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v46

    .line 97
    .restart local v46    # "tempID":I
    const/4 v3, 0x1

    aget-object v3, v14, v3

    const/4 v4, 0x1

    aget-object v4, v14, v4

    const-string v5, "face_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v45

    .line 98
    .local v45, "tempFaceID":I
    const/4 v3, 0x1

    aget-object v3, v14, v3

    const/4 v4, 0x1

    aget-object v4, v14, v4

    const-string v5, "image_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v47

    .line 99
    .restart local v47    # "tempImageID":I
    const/4 v3, 0x1

    aget-object v3, v14, v3

    const/4 v4, 0x1

    aget-object v4, v14, v4

    const-string v5, "person_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v48

    .line 100
    .restart local v48    # "tempPersonID":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() [in S Finder/faces] id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v46

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", faceID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v45

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", image = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", tempPersonID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_3

    .line 270
    .end local v45    # "tempFaceID":I
    .end local v46    # "tempID":I
    .end local v47    # "tempImageID":I
    .end local v48    # "tempPersonID":I
    :catch_1
    move-exception v19

    .line 271
    .local v19, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_5
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 272
    const/16 v37, 0x0

    .line 274
    const/4 v3, 0x0

    aget-object v3, v14, v3

    if-eqz v3, :cond_7

    .line 275
    const/4 v3, 0x0

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_7
    const/4 v3, 0x1

    aget-object v3, v14, v3

    if-eqz v3, :cond_1

    .line 277
    const/4 v3, 0x1

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 106
    .end local v19    # "e":Ljava/lang/Exception;
    :cond_8
    :try_start_6
    new-instance v24, Landroid/database/CursorJoiner;

    const/4 v3, 0x0

    aget-object v3, v14, v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "_id"

    aput-object v7, v4, v5

    const/4 v5, 0x1

    aget-object v5, v14, v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/16 v54, 0x0

    const-string v55, "face_id"

    aput-object v55, v7, v54

    move-object/from16 v0, v24

    invoke-direct {v0, v3, v4, v5, v7}, Landroid/database/CursorJoiner;-><init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V

    .line 111
    .local v24, "joiner":Landroid/database/CursorJoiner;
    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .local v33, "queryResultAdd":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    new-instance v35, Ljava/util/ArrayList;

    invoke-direct/range {v35 .. v35}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v35, "queryResultUpdate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    new-instance v34, Ljava/util/ArrayList;

    invoke-direct/range {v34 .. v34}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v34, "queryResultDelete":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v37, Ljava/util/HashMap;

    invoke-direct/range {v37 .. v37}, Ljava/util/HashMap;-><init>()V
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 115
    .end local v36    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .local v37, "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    :try_start_7
    new-instance v26, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;

    const/4 v3, 0x0

    aget-object v3, v14, v3

    move-object/from16 v0, v26

    invoke-direct {v0, v3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 116
    .local v26, "left":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    new-instance v38, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;

    const/4 v3, 0x1

    aget-object v3, v14, v3

    move-object/from16 v0, v38

    invoke-direct {v0, v3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 117
    .local v38, "right":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    const/4 v15, 0x0

    .line 118
    .local v15, "dataCount":I
    invoke-virtual/range {v24 .. v24}, Landroid/database/CursorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :cond_9
    :goto_5
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/database/CursorJoiner$Result;

    .line 119
    .local v25, "joinerResult":Landroid/database/CursorJoiner$Result;
    add-int/lit8 v15, v15, 0x1

    .line 120
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 121
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() [START CursorJoiner] No. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_a
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$1;->$SwitchMap$android$database$CursorJoiner$Result:[I

    invoke-virtual/range {v25 .. v25}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_5

    .line 126
    :pswitch_0
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getPersonID()I

    move-result v32

    .line 128
    .local v32, "personID":I
    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->getPersonName(I)Ljava/lang/String;

    move-result-object v28

    .line 129
    .local v28, "name":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() [LEFT] _id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ImageID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getImageID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", PersonID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v32

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_b
    new-instance v3, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getID()I

    move-result v4

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getImageID()I

    move-result v5

    move/from16 v0, v32

    move-object/from16 v1, v28

    invoke-direct {v3, v4, v5, v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;-><init>(IIILjava/lang/String;)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getImageID()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 267
    .end local v15    # "dataCount":I
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v25    # "joinerResult":Landroid/database/CursorJoiner$Result;
    .end local v26    # "left":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    .end local v28    # "name":Ljava/lang/String;
    .end local v32    # "personID":I
    .end local v38    # "right":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    :catch_2
    move-exception v43

    move-object/from16 v36, v37

    .end local v37    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v36    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    goto/16 :goto_2

    .line 141
    .end local v36    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v15    # "dataCount":I
    .restart local v21    # "i$":Ljava/util/Iterator;
    .restart local v25    # "joinerResult":Landroid/database/CursorJoiner$Result;
    .restart local v26    # "left":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    .restart local v37    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v38    # "right":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    :pswitch_1
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 142
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() [RIGHT] _id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", faceID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getFaceID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ImageID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getImageID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", PersonID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getPersonID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_c
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getID()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getImageID()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 270
    .end local v15    # "dataCount":I
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v25    # "joinerResult":Landroid/database/CursorJoiner$Result;
    .end local v26    # "left":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    .end local v38    # "right":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    :catch_3
    move-exception v19

    move-object/from16 v36, v37

    .end local v37    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v36    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    goto/16 :goto_4

    .line 154
    .end local v36    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v15    # "dataCount":I
    .restart local v21    # "i$":Ljava/util/Iterator;
    .restart local v25    # "joinerResult":Landroid/database/CursorJoiner$Result;
    .restart local v26    # "left":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    .restart local v37    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v38    # "right":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    :pswitch_2
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getID()I

    move-result v9

    .line 155
    .local v9, "left_id":I
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getImageID()I

    move-result v10

    .line 156
    .local v10, "left_imageID":I
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getPersonID()I

    move-result v11

    .line 157
    .local v11, "left_personID":I
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->getPersonName(I)Ljava/lang/String;

    move-result-object v27

    .line 158
    .local v27, "left_name":Ljava/lang/String;
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getID()I

    move-result v8

    .line 159
    .local v8, "right_id":I
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getFaceID()I

    move-result v39

    .line 160
    .local v39, "right_faceID":I
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getImageID()I

    move-result v40

    .line 161
    .local v40, "right_imageID":I
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getPersonID()I

    move-result v42

    .line 162
    .local v42, "right_personID":I
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->getName()Ljava/lang/String;

    move-result-object v41

    .line 163
    .local v41, "right_name":Ljava/lang/String;
    move/from16 v0, v39

    if-ne v9, v0, :cond_d

    move/from16 v0, v40

    if-ne v10, v0, :cond_d

    move/from16 v0, v42

    if-eq v11, v0, :cond_9

    .line 168
    :cond_d
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 169
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() [BOTH-LEFT] _id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ImageID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", PersonID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() [BOTH-RIGHT] _id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", faceID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v39

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ImageID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v40

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", PersonID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v42

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_e
    move-object/from16 v12, v41

    .line 177
    .local v12, "changeName":Ljava/lang/String;
    move/from16 v0, v42

    if-eq v11, v0, :cond_f

    .line 178
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->getPersonName(I)Ljava/lang/String;

    move-result-object v12

    .line 179
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 180
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() [BOTH-RIGHT] >>>>>> Change name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_f
    new-instance v7, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;

    invoke-direct/range {v7 .. v12}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;-><init>(IIIILjava/lang/String;)V

    move-object/from16 v0, v35

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_5

    .line 274
    .end local v8    # "right_id":I
    .end local v9    # "left_id":I
    .end local v10    # "left_imageID":I
    .end local v11    # "left_personID":I
    .end local v12    # "changeName":Ljava/lang/String;
    .end local v15    # "dataCount":I
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v25    # "joinerResult":Landroid/database/CursorJoiner$Result;
    .end local v26    # "left":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    .end local v27    # "left_name":Ljava/lang/String;
    .end local v38    # "right":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    .end local v39    # "right_faceID":I
    .end local v40    # "right_imageID":I
    .end local v41    # "right_name":Ljava/lang/String;
    .end local v42    # "right_personID":I
    :catchall_0
    move-exception v3

    move-object/from16 v36, v37

    .end local v24    # "joiner":Landroid/database/CursorJoiner;
    .end local v33    # "queryResultAdd":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    .end local v34    # "queryResultDelete":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v35    # "queryResultUpdate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    .end local v37    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v36    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    :goto_6
    const/4 v4, 0x0

    aget-object v4, v14, v4

    if-eqz v4, :cond_10

    .line 275
    const/4 v4, 0x0

    aget-object v4, v14, v4

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_10
    const/4 v4, 0x1

    aget-object v4, v14, v4

    if-eqz v4, :cond_11

    .line 277
    const/4 v4, 0x1

    aget-object v4, v14, v4

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_11
    throw v3

    .line 191
    .end local v36    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v15    # "dataCount":I
    .restart local v21    # "i$":Ljava/util/Iterator;
    .restart local v24    # "joiner":Landroid/database/CursorJoiner;
    .restart local v26    # "left":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    .restart local v33    # "queryResultAdd":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    .restart local v34    # "queryResultDelete":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v35    # "queryResultUpdate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    .restart local v37    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v38    # "right":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    :cond_12
    :try_start_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    const-string v4, "People:collect() cursor join completed"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const/4 v3, 0x3

    new-array v0, v3, [I

    move-object/from16 v20, v0

    fill-array-data v20, :array_0

    .line 196
    .local v20, "faceCount":[I
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 197
    .local v13, "addItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    :goto_7
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 198
    const/4 v3, 0x0

    aget v4, v20, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v20, v3

    .line 199
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;

    .line 200
    .local v31, "newFaceInfo":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;
    new-instance v53, Landroid/content/ContentValues;

    invoke-direct/range {v53 .. v53}, Landroid/content/ContentValues;-><init>()V

    .line 201
    .local v53, "values":Landroid/content/ContentValues;
    const-string v3, "face_id"

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->getFaceID()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 202
    const-string v3, "image_id"

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->getImageID()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 203
    const-string v3, "person_id"

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->getPersonID()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 204
    const-string v3, "name"

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Faces;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v53

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_7

    .line 207
    .end local v31    # "newFaceInfo":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;
    .end local v53    # "values":Landroid/content/ContentValues;
    :cond_13
    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v51

    .line 208
    .local v51, "updateItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    :goto_8
    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 209
    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v50

    check-cast v50, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;

    .line 210
    .local v50, "updateFaceInfo":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;
    new-instance v53, Landroid/content/ContentValues;

    invoke-direct/range {v53 .. v53}, Landroid/content/ContentValues;-><init>()V

    .line 211
    .restart local v53    # "values":Landroid/content/ContentValues;
    const-string v3, "face_id"

    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->getFaceID()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 212
    const-string v3, "image_id"

    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->getImageID()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 213
    const-string v3, "person_id"

    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->getPersonID()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 214
    const-string v3, "name"

    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->getID()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v52

    .line 216
    .local v52, "updateSelection":Ljava/lang/String;
    const/4 v3, 0x1

    aget v4, v20, v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Faces;->CONTENT_URI:Landroid/net/Uri;

    const/16 v54, 0x0

    move-object/from16 v0, v53

    move-object/from16 v1, v52

    move-object/from16 v2, v54

    invoke-virtual {v5, v7, v0, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    aput v4, v20, v3

    goto :goto_8

    .line 219
    .end local v50    # "updateFaceInfo":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;
    .end local v52    # "updateSelection":Ljava/lang/String;
    .end local v53    # "values":Landroid/content/ContentValues;
    :cond_14
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 220
    .local v17, "deleteItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_9
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 221
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 222
    .local v16, "delRow":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 223
    .local v18, "deleteSelection":Ljava/lang/String;
    const/4 v3, 0x2

    aget v4, v20, v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Faces;->CONTENT_URI:Landroid/net/Uri;

    const/16 v54, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v54

    invoke-virtual {v5, v7, v0, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    aput v4, v20, v3

    goto :goto_9

    .line 225
    .end local v16    # "delRow":I
    .end local v18    # "deleteSelection":Ljava/lang/String;
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() Face db change finished. insert = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget v5, v20, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", update = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget v5, v20, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", delete = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x2

    aget v5, v20, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-virtual/range {v37 .. v37}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1a

    .line 229
    invoke-virtual/range {v37 .. v37}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_16
    :goto_a
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 230
    .local v22, "imageID":I
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->getNameFromImage(I)Ljava/util/ArrayList;

    move-result-object v29

    .line 231
    .local v29, "nameArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v44, Ljava/util/ArrayList;

    invoke-direct/range {v44 .. v44}, Ljava/util/ArrayList;-><init>()V

    .line 232
    .local v44, "tagsIDArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v29, :cond_18

    .line 233
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v30

    .line 234
    .local v30, "nameItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_b
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 235
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 236
    .restart local v28    # "name":Ljava/lang/String;
    new-instance v53, Landroid/content/ContentValues;

    invoke-direct/range {v53 .. v53}, Landroid/content/ContentValues;-><init>()V

    .line 237
    .restart local v53    # "values":Landroid/content/ContentValues;
    const-string v3, "type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 238
    const-string v3, "rawdata"

    move-object/from16 v0, v53

    move-object/from16 v1, v28

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const-string v3, "data"

    move-object/from16 v0, v53

    move-object/from16 v1, v28

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v53

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v23

    .line 241
    .local v23, "insertUri":Landroid/net/Uri;
    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 243
    .end local v23    # "insertUri":Landroid/net/Uri;
    .end local v28    # "name":Ljava/lang/String;
    .end local v53    # "values":Landroid/content/ContentValues;
    :cond_17
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 244
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() imageID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", names = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    .end local v30    # "nameItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_18
    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_16

    .line 250
    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_19

    .line 251
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->getContentInfoFromImage(I)Ljava/lang/String;

    move-result-object v49

    .line 252
    .local v49, "timestamp":Ljava/lang/String;
    new-instance v53, Landroid/content/ContentValues;

    invoke-direct/range {v53 .. v53}, Landroid/content/ContentValues;-><init>()V

    .line 253
    .restart local v53    # "values":Landroid/content/ContentValues;
    if-eqz v49, :cond_19

    .line 254
    const-string v3, "timestamp"

    move-object/from16 v0, v53

    move-object/from16 v1, v49

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v3, "contenturi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v3, "appname"

    const-string v4, "com.sec.android.gallery3d"

    move-object/from16 v0, v53

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Contents;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v53

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v23

    .line 258
    .restart local v23    # "insertUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Contents;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " data inserted. row no = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    .end local v23    # "insertUri":Landroid/net/Uri;
    .end local v49    # "timestamp":Ljava/lang/String;
    .end local v53    # "values":Landroid/content/ContentValues;
    :cond_19
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v37

    move-object/from16 v1, v44

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_a

    .line 266
    .end local v22    # "imageID":I
    .end local v29    # "nameArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v44    # "tagsIDArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "People:collect() return map data : resultMap = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v37 .. v37}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 274
    const/4 v3, 0x0

    aget-object v3, v14, v3

    if-eqz v3, :cond_1b

    .line 275
    const/4 v3, 0x0

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_1b
    const/4 v3, 0x1

    aget-object v3, v14, v3

    if-eqz v3, :cond_1c

    .line 277
    const/4 v3, 0x1

    aget-object v3, v14, v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_1c
    move-object/from16 v36, v37

    .line 279
    .end local v37    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .restart local v36    # "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    goto/16 :goto_0

    .line 274
    .end local v13    # "addItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    .end local v15    # "dataCount":I
    .end local v17    # "deleteItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v20    # "faceCount":[I
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v24    # "joiner":Landroid/database/CursorJoiner;
    .end local v26    # "left":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    .end local v33    # "queryResultAdd":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    .end local v34    # "queryResultDelete":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v35    # "queryResultUpdate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    .end local v38    # "right":Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
    .end local v51    # "updateItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;>;"
    :catchall_1
    move-exception v3

    goto/16 :goto_6

    .line 123
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 193
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method protected getRequest(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;
    .locals 5
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    .prologue
    .line 57
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getSearchableInfo()Landroid/app/SearchableInfo;

    move-result-object v0

    .line 58
    .local v0, "searchable":Landroid/app/SearchableInfo;
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1
.end method

.method protected onChangeInternal(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 49
    if-eqz p1, :cond_0

    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->MEDIA_PROVIDER_FACES_URI:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChange() mFacesDbObserver Uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->prepare(J)V

    .line 53
    :cond_0
    return-void
.end method

.method public store(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;Ljava/lang/Object;)Z
    .locals 22
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;
    .param p2, "result"    # Ljava/lang/Object;

    .prologue
    .line 284
    if-nez p2, :cond_0

    .line 285
    const/16 v17, 0x0

    .line 338
    :goto_0
    return v17

    .line 287
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v9

    .local v9, "helper":Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;
    move-object/from16 v6, p2

    .line 290
    check-cast v6, Ljava/util/HashMap;

    .line 291
    .local v6, "convertResult":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 292
    .local v12, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;>;"
    :cond_1
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_a

    .line 293
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 294
    .local v8, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    sget-object v18, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v15

    .line 295
    .local v15, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "People:store() [Step.1] Check about image uri = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_2
    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->checkContentValidRecord(Ljava/lang/String;)J

    move-result-wide v4

    .line 300
    .local v4, "contentsID":J
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "People:store() [Step.1] contents_id = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    :cond_3
    const-wide/16 v18, -0x1

    cmp-long v17, v4, v18

    if-eqz v17, :cond_1

    .line 306
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 307
    .local v13, "taggingIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getFaceTagOfContent(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 308
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_6

    .line 309
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v17

    if-lez v17, :cond_4

    .line 310
    const-string v17, "_id"

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 311
    .local v11, "idx_taggingID":I
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 312
    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 314
    .end local v11    # "idx_taggingID":I
    :cond_4
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "People:store() [Step.2] Check already exists taggingIds = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v13}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 321
    :cond_6
    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_9

    .line 322
    const/16 v16, 0x0

    .line 323
    .local v16, "where":Ljava/lang/StringBuilder;
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 324
    .local v14, "taggingid":Ljava/lang/String;
    if-nez v16, :cond_7

    .line 325
    new-instance v16, Ljava/lang/StringBuilder;

    .end local v16    # "where":Ljava/lang/StringBuilder;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "_id="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .restart local v16    # "where":Ljava/lang/StringBuilder;
    goto :goto_3

    .line 327
    :cond_7
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, " OR _id="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 329
    .end local v14    # "taggingid":Ljava/lang/String;
    :cond_8
    if-eqz v16, :cond_9

    .line 330
    const-string v17, "tagging"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v9, v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->rawBulkDelete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "People:store() [Step.3] [REMOVE] TagDb.Tagging._ID - "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v16    # "where":Ljava/lang/StringBuilder;
    :cond_9
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v9, v4, v5, v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->bulkInsertOnPeople(JLjava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 338
    .end local v4    # "contentsID":J
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .end local v13    # "taggingIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v15    # "uri":Landroid/net/Uri;
    :cond_a
    const/16 v17, 0x1

    goto/16 :goto_0
.end method
