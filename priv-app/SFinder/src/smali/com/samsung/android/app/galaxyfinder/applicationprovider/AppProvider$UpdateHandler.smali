.class Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider$UpdateHandler;
.super Landroid/os/Handler;
.source "AppProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider$UpdateHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;

    .line 193
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 194
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 198
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 206
    # getter for: Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :goto_0
    return-void

    .line 200
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider$UpdateHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;->updateApplicationsList(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;->access$300(Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider$UpdateHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;->updateApplicationsList(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;->access$300(Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;Ljava/lang/String;)V

    goto :goto_0

    .line 198
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
