.class public Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
.super Landroid/widget/FrameLayout;
.source "FilterGroupDetailView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;
    }
.end annotation


# instance fields
.field private bDisplaying:Z

.field protected mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

.field protected mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

.field private mDataSetObserver:Landroid/database/DataSetObserver;

.field private mHorizontalSpacing:I

.field protected mPageIndex:I

.field private mParentScrollView:Landroid/widget/ScrollView;

.field private mToggle:Z

.field private mVerticalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "index"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mPageIndex:I

    .line 29
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    .line 31
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    .line 33
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mHorizontalSpacing:I

    .line 35
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mVerticalSpacing:I

    .line 37
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mToggle:Z

    .line 39
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->bDisplaying:Z

    .line 43
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 69
    iput p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mPageIndex:I

    .line 71
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->init()V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mPageIndex:I

    .line 29
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    .line 31
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    .line 33
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mHorizontalSpacing:I

    .line 35
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mVerticalSpacing:I

    .line 37
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mToggle:Z

    .line 39
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->bDisplaying:Z

    .line 43
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 77
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->init()V

    .line 78
    return-void
.end method

.method private getChildCountInOneLine(II)I
    .locals 9
    .param p1, "startIndex"    # I
    .param p2, "parentWidth"    # I

    .prologue
    .line 125
    const/4 v3, 0x0

    .line 126
    .local v3, "count":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 128
    .local v1, "childCount":I
    if-ge p1, v1, :cond_2

    .line 129
    const/4 v6, 0x0

    .line 131
    .local v6, "totalWidth":I
    move v5, p1

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_2

    .line 132
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 134
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_0

    .line 131
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 138
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 140
    .local v2, "childWidth":I
    add-int v7, v6, v2

    if-le v7, p2, :cond_1

    move v4, v3

    .line 149
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childWidth":I
    .end local v3    # "count":I
    .end local v5    # "i":I
    .end local v6    # "totalWidth":I
    .local v4, "count":I
    :goto_2
    return v4

    .line 143
    .end local v4    # "count":I
    .restart local v0    # "child":Landroid/view/View;
    .restart local v2    # "childWidth":I
    .restart local v3    # "count":I
    .restart local v5    # "i":I
    .restart local v6    # "totalWidth":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 144
    iget v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mHorizontalSpacing:I

    add-int/2addr v7, v2

    add-int/2addr v6, v7

    goto :goto_1

    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childWidth":I
    .end local v5    # "i":I
    .end local v6    # "totalWidth":I
    :cond_2
    move v4, v3

    .line 149
    .end local v3    # "count":I
    .restart local v4    # "count":I
    goto :goto_2
.end method

.method private init()V
    .locals 7

    .prologue
    .line 81
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03d4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 84
    .local v2, "s":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03d6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 85
    .local v3, "t":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 86
    .local v1, "e":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03d3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 88
    .local v0, "b":I
    invoke-virtual {p0, v2, v3, v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setPaddingRelative(IIII)V

    .line 90
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03cf

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mHorizontalSpacing:I

    .line 92
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03db

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mVerticalSpacing:I

    .line 95
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03d2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setMinimumHeight(I)V

    .line 97
    return-void
.end method

.method private setChildFrame(Landroid/view/View;IIII)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "start"    # I
    .param p3, "top"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 331
    add-int v0, p2, p4

    add-int v1, p3, p5

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 332
    return-void
.end method


# virtual methods
.method public findViewByTagData(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)Landroid/view/View;
    .locals 6
    .param p1, "dstData"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 110
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 111
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 112
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 114
    .local v3, "srcData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    if-eqz v3, :cond_0

    .line 115
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 121
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "srcData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :goto_1
    return-object v0

    .line 110
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "srcData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 121
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "srcData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public hasSelectedItems()Z
    .locals 4

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 408
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 409
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 411
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 412
    const/4 v3, 0x1

    .line 416
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v3

    .line 408
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 416
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public isDisplaying()Z
    .locals 1

    .prologue
    .line 434
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->bDisplaying:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 5

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 391
    .local v1, "count":I
    const/4 v2, 0x1

    .line 393
    .local v2, "empty":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 394
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 396
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 397
    const/4 v2, 0x0

    .line 402
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return v2

    .line 393
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public isToggleMode()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mToggle:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 336
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    if-eqz v1, :cond_1

    .line 337
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 338
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 340
    .local v0, "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 341
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 345
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const-string v4, ", "

    aput-object v4, v1, v2

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->getTagType(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v5

    const-string v4, ", "

    aput-object v4, v1, v6

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e00ad

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v4, v5, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {p1, v1}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setViewDesc(Landroid/view/View;[Ljava/lang/String;)V

    .line 368
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    invoke-interface {v1, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;->onActionItemClick(Landroid/view/View;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 370
    .end local v0    # "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_1
    return-void

    :cond_2
    move v1, v3

    .line 337
    goto :goto_0

    .line 357
    .restart local v0    # "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_3
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const-string v4, ", "

    aput-object v4, v1, v2

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->getTagType(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v5

    const-string v4, ", "

    aput-object v4, v1, v6

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e002f

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v4, v5, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {p1, v1}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setViewDesc(Landroid/view/View;[Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setMinimumHeight(I)V

    .line 104
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 105
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 18
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 154
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v7

    .line 155
    .local v7, "count":I
    sub-int v1, p4, p2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingStart()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingEnd()I

    move-result v17

    add-int v16, v16, v17

    sub-int v14, v1, v16

    .line 156
    .local v14, "parentWidth":I
    sub-int v1, p5, p3

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingTop()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingBottom()I

    move-result v17

    add-int v16, v16, v17

    sub-int v13, v1, v16

    .line 157
    .local v13, "parentHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingStart()I

    move-result v3

    .local v3, "dstStart":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingTop()I

    move-result v4

    .line 158
    .local v4, "dstTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getLayoutDirection()I

    move-result v1

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v1, v0, :cond_1

    const/4 v10, 0x1

    .line 160
    .local v10, "isLayoutRtl":Z
    :goto_0
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v7, :cond_0

    .line 161
    const/4 v1, 0x1

    if-ne v7, v1, :cond_2

    .line 162
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 163
    .local v2, "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/FrameLayout$LayoutParams;

    .line 165
    .local v12, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, v12, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v1, v1, 0x70

    const/16 v16, 0x10

    move/from16 v0, v16

    if-ne v1, v0, :cond_2

    iget v1, v12, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v1, v1, 0x7

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v1, v0, :cond_2

    .line 167
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 168
    .local v5, "childWidth":I
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 170
    .local v6, "childHeight":I
    shr-int/lit8 v1, v14, 0x1

    shr-int/lit8 v16, v5, 0x1

    sub-int v1, v1, v16

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingLeft()I

    move-result v16

    add-int v3, v1, v16

    .line 171
    shr-int/lit8 v1, v13, 0x1

    shr-int/lit8 v16, v6, 0x1

    sub-int v1, v1, v16

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingTop()I

    move-result v16

    add-int v4, v1, v16

    move-object/from16 v1, p0

    .line 173
    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setChildFrame(Landroid/view/View;IIII)V

    .line 213
    .end local v2    # "child":Landroid/view/View;
    .end local v5    # "childWidth":I
    .end local v6    # "childHeight":I
    .end local v12    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    return-void

    .line 158
    .end local v8    # "i":I
    .end local v10    # "isLayoutRtl":Z
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    .line 178
    .restart local v8    # "i":I
    .restart local v10    # "isLayoutRtl":Z
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v14}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCountInOneLine(II)I

    move-result v15

    .line 180
    .local v15, "subCount":I
    if-lez v15, :cond_0

    .line 181
    const/4 v5, 0x0

    .restart local v5    # "childWidth":I
    const/4 v6, 0x0

    .line 183
    .restart local v6    # "childHeight":I
    if-eqz v10, :cond_3

    sub-int v1, p4, p2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingStart()I

    move-result v16

    sub-int v3, v1, v16

    .line 185
    :goto_2
    move v11, v8

    .local v11, "j":I
    :goto_3
    add-int v1, v8, v15

    if-ge v11, v1, :cond_6

    .line 186
    move v9, v11

    .line 188
    .local v9, "index":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 190
    .restart local v2    # "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v16, 0x8

    move/from16 v0, v16

    if-ne v1, v0, :cond_4

    .line 185
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 183
    .end local v2    # "child":Landroid/view/View;
    .end local v9    # "index":I
    .end local v11    # "j":I
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingStart()I

    move-result v1

    add-int v3, p2, v1

    goto :goto_2

    .line 194
    .restart local v2    # "child":Landroid/view/View;
    .restart local v9    # "index":I
    .restart local v11    # "j":I
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 195
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 197
    if-eqz v10, :cond_5

    .line 198
    sub-int/2addr v3, v5

    move-object/from16 v1, p0

    .line 199
    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setChildFrame(Landroid/view/View;IIII)V

    .line 200
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mHorizontalSpacing:I

    sub-int/2addr v3, v1

    goto :goto_4

    :cond_5
    move-object/from16 v1, p0

    .line 202
    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setChildFrame(Landroid/view/View;IIII)V

    .line 203
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mHorizontalSpacing:I

    add-int/2addr v1, v5

    add-int/2addr v3, v1

    goto :goto_4

    .line 207
    .end local v2    # "child":Landroid/view/View;
    .end local v9    # "index":I
    :cond_6
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mVerticalSpacing:I

    add-int/2addr v1, v6

    add-int/2addr v4, v1

    .line 208
    add-int/lit8 v1, v15, -0x1

    add-int/2addr v8, v1

    .line 160
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 20
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 217
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v18

    .line 218
    .local v18, "width":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v13

    .line 219
    .local v13, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingStart()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingEnd()I

    move-result v4

    add-int/2addr v2, v4

    sub-int v9, v18, v2

    .line 220
    .local v9, "availableWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingTop()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v2, v4

    sub-int v8, v13, v2

    .line 221
    .local v8, "availableHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v12

    .line 222
    .local v12, "count":I
    const/16 v19, 0x0

    .local v19, "widthSum":I
    const/4 v14, 0x0

    .line 223
    .local v14, "heightSum":I
    const/16 v17, 0x0

    .line 225
    .local v17, "row":I
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    if-ge v15, v12, :cond_4

    .line 226
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 227
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/widget/FrameLayout$LayoutParams;

    .line 229
    .local v16, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-ne v2, v4, :cond_0

    .line 225
    :goto_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 233
    :cond_0
    move-object/from16 v0, v16

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    move-object/from16 v0, v16

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 234
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getMinimumHeight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    sub-int v8, v2, v4

    .line 236
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v9, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v8, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/view/View;->measure(II)V

    .line 242
    :goto_2
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    .line 243
    .local v11, "childWidth":I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 245
    .local v10, "childHeight":I
    add-int v2, v19, v11

    if-le v2, v9, :cond_2

    .line 246
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mVerticalSpacing:I

    add-int/2addr v2, v10

    add-int/2addr v14, v2

    .line 247
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mHorizontalSpacing:I

    add-int v19, v11, v2

    .line 249
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 239
    .end local v10    # "childHeight":I
    .end local v11    # "childWidth":I
    :cond_1
    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p1

    move/from16 v6, p2

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->measureChildWithMargins(Landroid/view/View;IIII)V

    goto :goto_2

    .line 251
    .restart local v10    # "childHeight":I
    .restart local v11    # "childWidth":I
    :cond_2
    if-nez v17, :cond_3

    .line 252
    move v14, v10

    .line 255
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mHorizontalSpacing:I

    add-int/2addr v2, v11

    add-int v19, v19, v2

    goto :goto_1

    .line 259
    .end local v3    # "child":Landroid/view/View;
    .end local v10    # "childHeight":I
    .end local v11    # "childWidth":I
    .end local v16    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingTop()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v14, v2

    .line 261
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getMinimumHeight()I

    move-result v2

    if-ge v14, v2, :cond_5

    .line 262
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getMinimumHeight()I

    move-result v14

    .line 265
    :cond_5
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v14, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setMeasuredDimension(II)V

    .line 267
    return-void
.end method

.method public onNotifyShowing()V
    .locals 2

    .prologue
    .line 381
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mToggle:Z

    if-eqz v1, :cond_0

    .line 382
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 383
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 384
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->onClick(Landroid/view/View;)V

    .line 387
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    .line 271
    return-void
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    if-nez v0, :cond_0

    .line 275
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    .line 276
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->setData(Ljava/util/ArrayList;)V

    .line 280
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->notifyDataSetChanged()V

    .line 281
    return-void
.end method

.method public setDisplay(Z)V
    .locals 0
    .param p1, "bShow"    # Z

    .prologue
    .line 430
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->bDisplaying:Z

    .line 431
    return-void
.end method

.method public setItemState(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 303
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 304
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 306
    .local v0, "child":Landroid/view/View;
    if-eqz p1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 307
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 303
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 309
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    .line 312
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public setItemStateUnrelatedTimeFilter(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 317
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 318
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 320
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->supportTimeTag()Z

    move-result v3

    if-nez v3, :cond_0

    .line 321
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 322
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 317
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 324
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    .line 328
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    return-void
.end method

.method public setParentScrollView(Landroid/widget/ScrollView;)V
    .locals 0
    .param p1, "sv"    # Landroid/widget/ScrollView;

    .prologue
    .line 420
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mParentScrollView:Landroid/widget/ScrollView;

    .line 421
    return-void
.end method

.method public setScrollEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 424
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mParentScrollView:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mParentScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, p1}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 427
    :cond_0
    return-void
.end method

.method public setToggleMode(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 373
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mToggle:Z

    .line 374
    return-void
.end method

.method public setUnselectedState()V
    .locals 4

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 286
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 287
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 289
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 290
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 286
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 298
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method
