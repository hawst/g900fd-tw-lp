.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;
.super Ljava/lang/Object;
.source "ItemListMyfilesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbnailLoader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;
    }
.end annotation


# instance fields
.field private final MAX_IMAGE_THREAD_POOL_VALUE:I

.field executorService:Ljava/util/concurrent/ExecutorService;

.field handler:Landroid/os/Handler;

.field mCr:Landroid/content/ContentResolver;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x2

    .line 286
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->handler:Landroid/os/Handler;

    .line 284
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->MAX_IMAGE_THREAD_POOL_VALUE:I

    .line 287
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->mCr:Landroid/content/ContentResolver;

    .line 288
    invoke-static {v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 289
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 276
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->queuePhoto(Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;Ljava/lang/String;)V

    return-void
.end method

.method private queuePhoto(Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;Ljava/lang/String;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 292
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;Ljava/lang/String;)V

    .line 294
    .local v0, "p":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailRunnable;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 295
    return-void
.end method

.method private resetThreadPool()V
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 304
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 306
    :cond_0
    return-void
.end method


# virtual methods
.method public getThumbNail(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "holder"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    .prologue
    .line 355
    const/4 v0, 0x0

    .line 356
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    .line 358
    .local v4, "thumbImfo":Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;
    iget-object v5, v4, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mPath:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 360
    .local v3, "mUri":Landroid/net/Uri;
    iget v5, v4, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mFileType:I

    const/16 v6, 0x3ed

    if-ne v5, v6, :cond_1

    .line 364
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->mCr:Landroid/content/ContentResolver;

    invoke-virtual {v5, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 366
    .local v2, "is":Ljava/io/InputStream;
    if-eqz v2, :cond_0

    .line 367
    const/4 v5, 0x0

    invoke-static {v2, v5}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 368
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 382
    .end local v2    # "is":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-object v0

    .line 370
    :catch_0
    move-exception v1

    .line 371
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "ImageView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to open content: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 374
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;

    iget-object v5, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mContext:Landroid/content/Context;

    iget-object v6, v4, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mPath:Ljava/lang/String;

    iget-object v7, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;->mMimeType:Ljava/lang/String;

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getThumbnailDrawableWithMakeCache(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 377
    if-eqz v0, :cond_0

    .line 378
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getInstance()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    move-result-object v6

    iget-object v7, v4, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mKey:Ljava/lang/String;

    move-object v5, v0

    check-cast v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, v7, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->putBitmapDrawable(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)V

    goto :goto_0
.end method

.method imageViewReused(Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;)Z
    .locals 1
    .param p1, "photoToLoad"    # Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    .prologue
    .line 344
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 347
    const/4 v0, 0x1

    .line 350
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetImageLoader()V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mThumbnailLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;)Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    move-result-object v0

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->resetThreadPool()V

    .line 299
    return-void
.end method
