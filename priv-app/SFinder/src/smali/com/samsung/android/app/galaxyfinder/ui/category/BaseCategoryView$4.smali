.class Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;
.super Ljava/lang/Object;
.source "BaseCategoryView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->addFooterView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 308
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v0

    .line 309
    .local v0, "preVisibleItemCnt":I
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    const/4 v2, 0x1

    iput v2, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mExpandedMode:I

    .line 311
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->expandMoreItem()I

    .line 313
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mResultView:Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryFullExpandView;->appendExtraItem(I)V

    .line 314
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isFullyExpanded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 315
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$4;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mFooterView:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$500(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Landroid/view/ViewGroup;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 317
    :cond_0
    return-void
.end method
