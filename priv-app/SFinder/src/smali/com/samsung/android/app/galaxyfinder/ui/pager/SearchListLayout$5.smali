.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

.field final synthetic val$mFooterView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->val$mFooterView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0e0055

    .line 378
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 380
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 381
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0056

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 382
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 394
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x1040000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 403
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 404
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 405
    return-void
.end method
