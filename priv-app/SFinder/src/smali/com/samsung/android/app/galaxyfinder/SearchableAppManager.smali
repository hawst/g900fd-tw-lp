.class public Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;
.super Ljava/lang/Object;
.source "SearchableAppManager.java"


# static fields
.field private static final CONTEXT_PKG_NAME:Ljava/lang/String; = "com.samsung.android.providers.context"

.field private static final NO_APPLICATION_LABEL:Ljava/lang/String; = ""

.field private static final TAG:Ljava/lang/String; = "SearchableAppManager"

.field private static mCategoryStable:Z

.field private static mCurrentOrderStable:Z

.field private static mCurrentSearchableList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile mInstance:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

.field private static mPackageMap:Lcom/samsung/android/app/galaxyfinder/util/PackageMap;

.field private static mSearchableAppStable:Z

.field private static mSearchableList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static mSearchablePackageStable:Z

.field private static mTagSupportedPackageMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private comperator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    sput-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchableList:Ljava/util/List;

    .line 47
    sput-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentSearchableList:Ljava/util/List;

    .line 49
    sput-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mTagSupportedPackageMap:Ljava/util/Map;

    .line 51
    sput-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mPackageMap:Lcom/samsung/android/app/galaxyfinder/util/PackageMap;

    .line 53
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchableAppStable:Z

    .line 55
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentOrderStable:Z

    .line 57
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCategoryStable:Z

    .line 59
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchablePackageStable:Z

    .line 61
    sput-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mInstance:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager$1;-><init>(Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->comperator:Ljava/util/Comparator;

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getSearchableList(Z)Ljava/util/ArrayList;

    .line 65
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getTagSupportedPackageMap()Ljava/util/Map;

    .line 66
    return-void
.end method

.method private getFrequentAppList()Ljava/util/ArrayList;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v9

    .line 219
    .local v9, "context":Landroid/content/Context;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getPackageMap()Lcom/samsung/android/app/galaxyfinder/util/PackageMap;

    move-result-object v17

    .line 220
    .local v17, "packageNamedMap":Lcom/samsung/android/app/galaxyfinder/util/PackageMap;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v13, "frequentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v23, -0x1

    .line 225
    .local v23, "version":I
    :try_start_0
    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v3, "com.samsung.android.providers.context"

    const/16 v4, 0x80

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v16

    .line 227
    .local v16, "pInfo":Landroid/content/pm/PackageInfo;
    if-eqz v16, :cond_0

    .line 228
    move-object/from16 v0, v16

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v23, v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    :cond_0
    const/4 v1, -0x1

    move/from16 v0, v23

    if-ne v0, v1, :cond_3

    .line 236
    const/16 v22, 0x0

    .line 301
    .end local v16    # "pInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    :goto_0
    return-object v22

    .line 230
    :catch_0
    move-exception v12

    .line 231
    .local v12, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    invoke-virtual {v12}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 235
    const/4 v1, -0x1

    move/from16 v0, v23

    if-ne v0, v1, :cond_3

    .line 236
    const/16 v22, 0x0

    goto :goto_0

    .line 232
    .end local v12    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v11

    .line 233
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    const/4 v1, -0x1

    move/from16 v0, v23

    if-ne v0, v1, :cond_3

    .line 236
    const/16 v22, 0x0

    goto :goto_0

    .line 235
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    const/4 v3, -0x1

    move/from16 v0, v23

    if-ne v0, v3, :cond_2

    .line 236
    const/16 v22, 0x0

    goto :goto_0

    :cond_2
    throw v1

    .line 239
    :cond_3
    const/4 v2, 0x0

    .line 240
    .local v2, "uri":Landroid/net/Uri;
    const/4 v1, 0x2

    move/from16 v0, v23

    if-lt v0, v1, :cond_4

    .line 241
    const-string v1, "content://com.samsung.android.providers.context.profile/app_used?frequency=frequent&time_span=0001-00-00"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 246
    :goto_1
    const/4 v10, 0x0

    .line 248
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_2
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 249
    if-nez v10, :cond_5

    .line 250
    const-string v1, "SearchableAppManager"

    const-string v3, "[SW] freqcursor is null"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 251
    const/16 v22, 0x0

    .line 284
    if-eqz v10, :cond_1

    .line 285
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 244
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_4
    const-string v1, "content://com.samsung.android.providers.context/app_usage/freq"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    .line 253
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :cond_5
    :try_start_3
    const-string v1, "SearchableAppManager"

    const-string v3, "[SW] Success to get freqcursor"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v19

    .line 255
    .local v19, "rowCount":I
    const-string v1, "SearchableAppManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SW] cursor rowcount() :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    if-nez v19, :cond_6

    .line 257
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 258
    const/16 v22, 0x0

    .line 284
    if-eqz v10, :cond_1

    .line 285
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 260
    :cond_6
    const/4 v1, 0x2

    move/from16 v0, v23

    if-lt v0, v1, :cond_d

    :try_start_4
    const-string v1, "app_id"

    :goto_2
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 261
    .local v14, "idx_name":I
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 263
    :cond_7
    invoke-interface {v10, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 264
    .local v8, "appPackageName":Ljava/lang/String;
    const-string v1, "com.android.contacts"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 265
    const-string v1, "com.android.providers.contacts"

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    :cond_8
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/android/app/galaxyfinder/util/PackageMap;->getProviderPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 269
    .local v18, "providerPackageName":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 272
    const-string v3, "SearchableAppManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current1 package_name : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v1, 0x2

    move/from16 v0, v23

    if-lt v0, v1, :cond_e

    const-string v1, "app_id"

    :goto_3
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const-string v3, "SearchableAppManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current1 count : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v1, 0x2

    move/from16 v0, v23

    if-lt v0, v1, :cond_f

    const-string v1, "frequency"

    :goto_4
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_9
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v1

    if-nez v1, :cond_7

    .line 284
    .end local v8    # "appPackageName":Ljava/lang/String;
    .end local v18    # "providerPackageName":Ljava/lang/String;
    :cond_a
    if-eqz v10, :cond_b

    .line 285
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 289
    .end local v14    # "idx_name":I
    .end local v19    # "rowCount":I
    :cond_b
    :goto_5
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 291
    .local v22, "uniqueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "com.samsung.android.app.galaxyfinder"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    new-instance v21, Ljava/util/LinkedHashSet;

    move-object/from16 v0, v21

    invoke-direct {v0, v13}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 293
    .local v21, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 294
    .local v15, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_c
    :goto_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 296
    .local v7, "appName":Ljava/lang/String;
    const-string v1, "com.samsung.android.app.galaxyfinder"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 297
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 260
    .end local v7    # "appName":Ljava/lang/String;
    .end local v15    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v21    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v22    # "uniqueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v19    # "rowCount":I
    :cond_d
    :try_start_5
    const-string v1, "package_name"

    goto/16 :goto_2

    .line 272
    .restart local v8    # "appPackageName":Ljava/lang/String;
    .restart local v14    # "idx_name":I
    .restart local v18    # "providerPackageName":Ljava/lang/String;
    :cond_e
    const-string v1, "package_name"

    goto :goto_3

    .line 274
    :cond_f
    const-string v1, "app_freq"
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_4

    .line 279
    .end local v8    # "appPackageName":Ljava/lang/String;
    .end local v14    # "idx_name":I
    .end local v18    # "providerPackageName":Ljava/lang/String;
    .end local v19    # "rowCount":I
    :catch_2
    move-exception v20

    .line 280
    .local v20, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_6
    invoke-virtual/range {v20 .. v20}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 284
    if-eqz v10, :cond_b

    .line 285
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 281
    .end local v20    # "se":Landroid/database/sqlite/SQLiteException;
    :catch_3
    move-exception v11

    .line 282
    .restart local v11    # "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 284
    if-eqz v10, :cond_b

    .line 285
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 284
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v1

    if-eqz v10, :cond_10

    .line 285
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_10
    throw v1
.end method

.method public static getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mInstance:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    if-nez v0, :cond_1

    .line 70
    const-class v1, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    monitor-enter v1

    .line 71
    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mInstance:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mInstance:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    .line 74
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :cond_1
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mInstance:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    return-object v0

    .line 74
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private declared-synchronized getPackageMap()Lcom/samsung/android/app/galaxyfinder/util/PackageMap;
    .locals 1

    .prologue
    .line 305
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchablePackageStable:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mPackageMap:Lcom/samsung/android/app/galaxyfinder/util/PackageMap;

    if-eqz v0, :cond_0

    .line 306
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mPackageMap:Lcom/samsung/android/app/galaxyfinder/util/PackageMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    :goto_0
    monitor-exit p0

    return-object v0

    .line 308
    :cond_0
    :try_start_1
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/util/PackageMap;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/util/PackageMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mPackageMap:Lcom/samsung/android/app/galaxyfinder/util/PackageMap;

    .line 309
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchablePackageStable:Z

    .line 310
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mPackageMap:Lcom/samsung/android/app/galaxyfinder/util/PackageMap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 305
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getTagSupportedPackageMap()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCategoryStable:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mTagSupportedPackageMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "SearchableAppManager"

    const-string v1, "[Searchable] getTagSupportedPackageMap() reUsed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mTagSupportedPackageMap:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    :goto_0
    monitor-exit p0

    return-object v0

    .line 162
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->updateTagSupportPackage()V

    .line 163
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCategoryStable:Z

    .line 164
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mTagSupportedPackageMap:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateSearchableList(Z)V
    .locals 11
    .param p1, "includeGlobal"    # Z

    .prologue
    .line 314
    const-string v8, "SearchableAppManager"

    const-string v9, "[Searchable] updateSearchableList() reBuild"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v8

    const-string v9, "search"

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/SearchManager;

    .line 317
    .local v6, "sm":Landroid/app/SearchManager;
    invoke-virtual {v6, p1}, Landroid/app/SearchManager;->getSearchablesInInsightSearch(Z)Ljava/util/List;

    move-result-object v5

    .line 318
    .local v5, "searchables":Ljava/util/List;, "Ljava/util/List<Landroid/app/SearchableInfo;>;"
    if-eqz v5, :cond_c

    .line 319
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 320
    .local v4, "searchableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 322
    .local v7, "uniqueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/SearchableInfo;

    .line 323
    .local v3, "searchable":Landroid/app/SearchableInfo;
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v2

    .line 324
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "className":Ljava/lang/String;
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 327
    const-string v8, "SearchableAppManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is duplicate"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 331
    :cond_1
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.google.android.apps.books"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.google.android.music"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.google.android.videos"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.google.android.apps.docs"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 343
    const-string v8, "com.twitter.android"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 344
    const-string v8, "SearchableAppManager"

    const-string v9, "Twitter package (com.twitter.android) is excluded"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 349
    :cond_2
    const-string v8, ".amazon."

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "com.imdb.mobile"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 351
    :cond_3
    const-string v8, "SearchableAppManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Amazon package ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") is excluded"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 356
    :cond_4
    const-string v8, ".ebay."

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 357
    const-string v8, "SearchableAppManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Amazon package ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") is excluded"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 362
    :cond_5
    sget-boolean v8, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v8, :cond_6

    const-string v8, "com.baidu.searchbox_samsung"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.baidu.netdisk_ss"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.baidu.searchbox"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.sec.android.app.dialertab.calllog.LogsSearchResultsActivity"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.samsung.dialer.calllog.LogsSearchResultsActivity"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 372
    :cond_6
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v8

    const-string v9, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v8, v9}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.android.jcontacts"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    const-string v8, "com.android.contacts.activities.PeopleActivity"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 379
    const-string v8, "SearchableAppManager"

    const-string v9, "docomo contacts package ( com.android.contacts ) is excluded"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 385
    :cond_7
    const-string v8, "com.tripadvisor.tripadvisor"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 386
    const-string v8, "SearchableAppManager"

    const-string v9, "TripAdvisor package (com.tripadvisor.tripadvisor) is excluded"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 392
    :cond_8
    const-string v8, "com.paypal.android.p2pmobile"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 393
    const-string v8, "SearchableAppManager"

    const-string v9, "Paypal package (com.paypal.android.p2pmobile) is excluded"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 397
    :cond_9
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 400
    .end local v0    # "className":Ljava/lang/String;
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "searchable":Landroid/app/SearchableInfo;
    :cond_a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_b

    .line 401
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->comperator:Ljava/util/Comparator;

    invoke-static {v4, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 403
    :cond_b
    monitor-enter p0

    .line 404
    :try_start_0
    sput-object v4, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchableList:Ljava/util/List;

    .line 405
    monitor-exit p0

    .line 407
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "searchableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    .end local v7    # "uniqueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_c
    return-void

    .line 405
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v4    # "searchableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    .restart local v7    # "uniqueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v8

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8
.end method

.method private updateTagSupportPackage()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 171
    const-string v5, "SearchableAppManager"

    const-string v6, "[Searchable] updateTagSupportPackage() reBuild"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getSearchableList(Z)Ljava/util/ArrayList;

    move-result-object v2

    .line 173
    .local v2, "searchableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 174
    .local v4, "tagSupportedPkg":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchableInfo;

    .line 175
    .local v1, "searchable":Landroid/app/SearchableInfo;
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getAdvancedSearchType()Ljava/lang/String;

    move-result-object v3

    .line 176
    .local v3, "tagSearch":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string v5, "tagSearch"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getTagProviderUri()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 178
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->isCheckedApplication(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 179
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 181
    :cond_1
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 185
    .end local v1    # "searchable":Landroid/app/SearchableInfo;
    .end local v3    # "tagSearch":Ljava/lang/String;
    :cond_2
    monitor-enter p0

    .line 186
    :try_start_0
    sput-object v4, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mTagSupportedPackageMap:Ljava/util/Map;

    .line 187
    monitor-exit p0

    .line 188
    return-void

    .line 187
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method


# virtual methods
.method public getApplicationLabel(Landroid/content/ComponentName;I)Ljava/lang/String;
    .locals 11
    .param p1, "cn"    # Landroid/content/ComponentName;
    .param p2, "id"    # I

    .prologue
    .line 422
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v2

    .line 423
    .local v2, "context":Landroid/content/Context;
    const/4 v4, 0x0

    .line 424
    .local v4, "label":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 426
    .local v5, "packageName":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 429
    .local v6, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v8, "com.twitter.android"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.tripadvisor.tripadvisor"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.onepassword.passwordmanager"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.android.calendar"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 433
    :cond_0
    const/4 v8, 0x0

    invoke-virtual {v6, v5, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 434
    .local v1, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v6, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .end local v1    # "ai":Landroid/content/pm/ApplicationInfo;
    :goto_0
    move-object v8, v4

    .line 460
    :goto_1
    return-object v8

    .line 436
    :cond_1
    invoke-virtual {v6, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v7

    .line 437
    .local v7, "res":Landroid/content/res/Resources;
    invoke-virtual {v7, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v4

    goto :goto_0

    .line 440
    .end local v7    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v3

    .line 441
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 442
    if-eqz v5, :cond_2

    .line 443
    const-string v8, "SearchableAppManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[For debug] NameNotFoundException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    :cond_2
    const-string v8, ""

    goto :goto_1

    .line 446
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v3

    .line 447
    .local v3, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v3}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    .line 448
    if-eqz v5, :cond_3

    .line 449
    const-string v8, "SearchableAppManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[For debug] NotFoundException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    :cond_3
    const-string v8, ""

    goto :goto_1

    .line 452
    .end local v3    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_2
    move-exception v3

    .line 453
    .local v3, "e":Ljava/lang/SecurityException;
    invoke-virtual {v3}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 454
    if-eqz v5, :cond_4

    .line 455
    const-string v8, "SearchableAppManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[For debug] SecurityException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_4
    const-string v8, ""

    goto :goto_1
.end method

.method public getOrderedSearchableList()Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 98
    .local v12, "s":J
    monitor-enter p0

    .line 99
    :try_start_0
    sget-boolean v16, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentOrderStable:Z

    if-eqz v16, :cond_0

    sget-object v16, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentSearchableList:Ljava/util/List;

    if-eqz v16, :cond_0

    .line 100
    const-string v16, "SearchableAppManager"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "[Searchable] getOrderedSearchableList() reUsed : time = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v12

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v16, Ljava/util/ArrayList;

    sget-object v17, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentSearchableList:Ljava/util/List;

    invoke-direct/range {v16 .. v17}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit p0

    .line 150
    :goto_0
    return-object v16

    .line 103
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    const/4 v11, 0x0

    .line 105
    .local v11, "restList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/LinkedHashSet;

    invoke-direct {v8}, Ljava/util/LinkedHashSet;-><init>()V

    .line 106
    .local v8, "mergedPackageList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v15, "searchableInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getFrequentAppList()Ljava/util/ArrayList;

    move-result-object v4

    .line 110
    .local v4, "freqList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v4, :cond_1

    .line 111
    invoke-virtual {v8, v4}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 114
    :cond_1
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getSearchableList(Z)Ljava/util/ArrayList;

    move-result-object v3

    .line 115
    .local v3, "currentSearchableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 118
    .local v9, "nSearchableList":I
    const/4 v2, 0x0

    .line 119
    .local v2, "checkedPkgName":Ljava/lang/String;
    new-instance v11, Ljava/util/ArrayList;

    .end local v11    # "restList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .restart local v11    # "restList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v9, :cond_3

    .line 121
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/app/SearchableInfo;

    invoke-virtual/range {v16 .. v16}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v2

    .line 122
    if-eqz v4, :cond_2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 120
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 103
    .end local v2    # "checkedPkgName":Ljava/lang/String;
    .end local v3    # "currentSearchableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    .end local v4    # "freqList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "i":I
    .end local v8    # "mergedPackageList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .end local v9    # "nSearchableList":I
    .end local v11    # "restList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v15    # "searchableInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    :catchall_0
    move-exception v16

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v16

    .line 125
    .restart local v2    # "checkedPkgName":Ljava/lang/String;
    .restart local v3    # "currentSearchableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    .restart local v4    # "freqList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "i":I
    .restart local v8    # "mergedPackageList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .restart local v9    # "nSearchableList":I
    .restart local v11    # "restList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v15    # "searchableInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    :cond_2
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 128
    :cond_3
    invoke-virtual {v8, v11}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 130
    invoke-virtual {v8}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 131
    .local v10, "pkg":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_4

    .line 134
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->isCheckedApplication(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 137
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/app/SearchableInfo;

    .line 138
    .local v14, "searchable":Landroid/app/SearchableInfo;
    invoke-virtual {v14}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 139
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 144
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "pkg":Ljava/lang/String;
    .end local v14    # "searchable":Landroid/app/SearchableInfo;
    :cond_6
    const-string v16, "SearchableAppManager"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "[Searchable] getOrderedSearchableList() reBuild : time = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v12

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    monitor-enter p0

    .line 147
    :try_start_2
    sput-object v15, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentSearchableList:Ljava/util/List;

    .line 148
    const/16 v16, 0x1

    sput-boolean v16, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentOrderStable:Z

    .line 149
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 150
    new-instance v16, Ljava/util/ArrayList;

    sget-object v17, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentSearchableList:Ljava/util/List;

    invoke-direct/range {v16 .. v17}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/16 :goto_0

    .line 149
    :catchall_1
    move-exception v16

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v16
.end method

.method public declared-synchronized getSearchableList(Z)Ljava/util/ArrayList;
    .locals 2
    .param p1, "includeGlobal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchableAppStable:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchableList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 84
    const-string v0, "SearchableAppManager"

    const-string v1, "[Searchable] getSearchableList() reUsed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchableList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :goto_0
    monitor-exit p0

    return-object v0

    .line 87
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->updateSearchableList(Z)V

    .line 88
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchableAppStable:Z

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchableList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTagSupportedPackageList(Z)Ljava/util/ArrayList;
    .locals 5
    .param p1, "isChecked"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getTagSupportedPackageMap()Ljava/util/Map;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 197
    .local v3, "tagSupportedPkg":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v2, "packageNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 199
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 200
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 201
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 202
    .local v0, "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-ne v4, p1, :cond_0

    .line 203
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 207
    .end local v0    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;>;"
    :cond_1
    return-object v2
.end method

.method public isCheckedApplication(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 464
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getInstance()Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getChildStatus(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized setCurrentOrderChanged()V
    .locals 1

    .prologue
    .line 481
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentOrderStable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    monitor-exit p0

    return-void

    .line 481
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSearchableAppChanged()V
    .locals 2

    .prologue
    .line 468
    monitor-enter p0

    :try_start_0
    const-string v0, "SearchableAppManager"

    const-string v1, "[Searchable] setSearchableAppChanged() : updateSearchableList will be Invoked"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchableAppStable:Z

    .line 471
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mSearchablePackageStable:Z

    .line 472
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCurrentOrderStable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 473
    monitor-exit p0

    return-void

    .line 468
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSearchableCategoryChanged()V
    .locals 2

    .prologue
    .line 476
    monitor-enter p0

    :try_start_0
    const-string v0, "SearchableAppManager"

    const-string v1, "[Searchable] setSearchableCategoryChanged() : updateTagSupportPackage will be Invoked"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->mCategoryStable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478
    monitor-exit p0

    return-void

    .line 476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
