.class public Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;
.super Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;
.source "SearchEnginePhone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;,
        Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;
    }
.end annotation


# instance fields
.field final LOCACTION:Ljava/lang/String;

.field final PEOPLE:Ljava/lang/String;

.field final TAGDILIMETER:Ljava/lang/String;

.field final TAG_TYPES:[Ljava/lang/String;

.field final USERDEF:Ljava/lang/String;

.field final WEATHER:Ljava/lang/String;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mQueryAsyncExecutorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private mQueryHandler:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

.field private mQueryRequiredTime:J

.field private mUseAsyncSearchEngine:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mContentResolver:Landroid/content/ContentResolver;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryHandler:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

    .line 37
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAsyncExecutorList:Ljava/util/ArrayList;

    .line 39
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mUseAsyncSearchEngine:Z

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryRequiredTime:J

    .line 351
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "location"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "people"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "weather"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "userdef"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->TAG_TYPES:[Ljava/lang/String;

    .line 355
    const-string v0, "location="

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->LOCACTION:Ljava/lang/String;

    .line 357
    const-string v0, "people="

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->PEOPLE:Ljava/lang/String;

    .line 359
    const-string v0, "weather="

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->WEATHER:Ljava/lang/String;

    .line 361
    const-string v0, "userdef="

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->USERDEF:Ljava/lang/String;

    .line 363
    const-string v0, "="

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->TAGDILIMETER:Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mContentResolver:Landroid/content/ContentResolver;

    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mUseAsyncSearchEngine:Z

    .line 47
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mUseAsyncSearchEngine:Z

    if-eqz v0, :cond_0

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAsyncExecutorList:Ljava/util/ArrayList;

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;-><init>(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryHandler:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryRequiredTime:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;
    .param p1, "x1"    # J

    .prologue
    .line 31
    iput-wide p1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryRequiredTime:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->getAppLog(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Ljava/lang/String;Landroid/app/SearchableInfo;Landroid/database/Cursor;)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/app/SearchableInfo;
    .param p3, "x3"    # Landroid/database/Cursor;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeCategory(Ljava/lang/String;Landroid/app/SearchableInfo;Landroid/database/Cursor;)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v0

    return-object v0
.end method

.method private doEncodeQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 482
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 483
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->AND:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;-><init>(Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;)V

    .line 484
    .local v0, "qBuilder":Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;
    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;->replace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 487
    .end local v0    # "qBuilder":Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;
    :cond_0
    const-string v1, "SearchEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doEncodeQuery() query : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    return-object p1
.end method

.method private getAppLog(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 706
    const-string v2, "\\."

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 707
    .local v1, "splitPackage":[Ljava/lang/String;
    const-string v0, ""

    .line 708
    .local v0, "APP_TAG":Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 709
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 712
    :cond_0
    return-object v0
.end method

.method private makeCategory(Ljava/lang/String;Landroid/app/SearchableInfo;Landroid/database/Cursor;)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .locals 7
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "searchable"    # Landroid/app/SearchableInfo;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 453
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;-><init>(Landroid/content/Context;)V

    .line 454
    .local v1, "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v4

    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getLabelId()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getApplicationLabel(Landroid/content/ComponentName;I)Ljava/lang/String;

    move-result-object v2

    .line 456
    .local v2, "label":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->shouldIncludeInInsightSearch()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 457
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSearchLayoutStyle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setTemplateStyle(Ljava/lang/String;)V

    .line 460
    :cond_0
    const/4 v3, 0x0

    .line 461
    .local v3, "supportTagSearch":Z
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getAdvancedSearchType()Ljava/lang/String;

    move-result-object v0

    .line 463
    .local v0, "advancedSearchType":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 464
    const-string v4, "tagSearch"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 465
    const/4 v3, 0x1

    .line 469
    :cond_1
    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setCategoryName(Ljava/lang/String;)V

    .line 470
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setQuery(Ljava/lang/String;)V

    .line 471
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setPackageName(Ljava/lang/String;)V

    .line 472
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setSearchActivity(Landroid/content/ComponentName;)V

    .line 473
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSuggestIntentAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSuggestIntentData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setBaseIntent(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    invoke-virtual {v1, v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setTagSearch(Z)V

    .line 476
    invoke-virtual {v1, p3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setData(Landroid/database/Cursor;)V

    .line 478
    return-object v1
.end method

.method private makeQueryForSearch(Landroid/app/SearchableInfo;Ljava/lang/String;Landroid/net/Uri;Landroid/os/Bundle;Z)Landroid/net/Uri;
    .locals 28
    .param p1, "searchable"    # Landroid/app/SearchableInfo;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "queryParams"    # Landroid/net/Uri;
    .param p4, "searchFilters"    # Landroid/os/Bundle;
    .param p5, "tagProviderOnly"    # Z

    .prologue
    .line 152
    if-nez p1, :cond_0

    .line 153
    const/16 v23, 0x0

    .line 303
    :goto_0
    return-object v23

    .line 155
    :cond_0
    const/16 v23, 0x0

    .line 156
    .local v23, "uri":Landroid/net/Uri;
    new-instance v24, Landroid/net/Uri$Builder;

    invoke-direct/range {v24 .. v24}, Landroid/net/Uri$Builder;-><init>()V

    .line 158
    .local v24, "uriBuilder":Landroid/net/Uri$Builder;
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 159
    const-string v25, "content"

    invoke-virtual/range {v24 .. v25}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 161
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestPath()Ljava/lang/String;

    move-result-object v25

    if-eqz v25, :cond_1

    .line 162
    new-instance v12, Ljava/util/StringTokenizer;

    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestPath()Ljava/lang/String;

    move-result-object v25

    const-string v26, "/"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v12, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .local v12, "st":Ljava/util/StringTokenizer;
    if-eqz v12, :cond_1

    .line 165
    :goto_1
    invoke-virtual {v12}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v25

    if-eqz v25, :cond_1

    .line 166
    invoke-virtual {v12}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v10

    .line 168
    .local v10, "path":Ljava/lang/String;
    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    .line 173
    .end local v10    # "path":Ljava/lang/String;
    .end local v12    # "st":Ljava/util/StringTokenizer;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->shouldIncludeInInsightSearch()Z

    move-result v25

    if-eqz v25, :cond_4

    .line 174
    const-string v25, "search_suggest_regex_query"

    invoke-virtual/range {v24 .. v25}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 179
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestSelection()Ljava/lang/String;

    move-result-object v25

    if-nez v25, :cond_2

    .line 182
    if-eqz p2, :cond_2

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_2

    .line 183
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->shouldIncludeInInsightSearch()Z

    move-result v25

    if-eqz v25, :cond_5

    .line 184
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->doEncodeQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 185
    .local v11, "refinedQuery":Ljava/lang/String;
    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 193
    .end local v11    # "refinedQuery":Ljava/lang/String;
    :cond_2
    :goto_3
    if-eqz p5, :cond_6

    .line 194
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getTagFilters()Ljava/lang/String;

    move-result-object v25

    if-eqz v25, :cond_3

    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getTagFilters()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-eqz v25, :cond_6

    .line 195
    :cond_3
    const/16 v23, 0x0

    goto :goto_0

    .line 176
    :cond_4
    const-string v25, "search_suggest_query"

    invoke-virtual/range {v24 .. v25}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_2

    .line 187
    :cond_5
    move-object/from16 v0, v24

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_3

    .line 201
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTimeSpan()Ljava/lang/String;

    move-result-object v25

    if-eqz v25, :cond_7

    .line 202
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getAdvancedSearchType()Ljava/lang/String;

    move-result-object v25

    if-eqz v25, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getAdvancedSearchType()Ljava/lang/String;

    move-result-object v25

    const-string v26, "timeSpan"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTimeSpan()Ljava/lang/String;

    move-result-object v21

    .line 205
    .local v21, "timeSpan":Ljava/lang/String;
    new-instance v22, Ljava/util/StringTokenizer;

    const-string v25, ","

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    .local v22, "tokenizer":Ljava/util/StringTokenizer;
    const-wide/16 v14, -0x1

    .line 208
    .local v14, "stime":J
    const-wide/16 v6, -0x1

    .line 210
    .local v6, "etime":J
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v25

    if-eqz v25, :cond_7

    .line 212
    :try_start_0
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 213
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v6

    .line 221
    :goto_4
    const-wide/16 v26, 0x0

    cmp-long v25, v14, v26

    if-lez v25, :cond_7

    .line 222
    const-string v25, "stime"

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v24 .. v26}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 223
    const-string v25, "etime"

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v24 .. v26}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 233
    .end local v6    # "etime":J
    .end local v14    # "stime":J
    .end local v21    # "timeSpan":Ljava/lang/String;
    .end local v22    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_7
    if-eqz p3, :cond_10

    .line 234
    if-eqz p1, :cond_e

    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getTagFilters()Ljava/lang/String;

    move-result-object v25

    if-eqz v25, :cond_e

    .line 235
    const/4 v5, 0x0

    .line 236
    .local v5, "hasTagQueryParams":Z
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getTagFilters()Ljava/lang/String;

    move-result-object v16

    .line 239
    .local v16, "tagFilters":Ljava/lang/String;
    const-string v25, "location"

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v17

    .line 240
    .local v17, "tagLocation":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v25, "people"

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v18

    .line 241
    .local v18, "tagPeople":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v25, "weather"

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v20

    .line 242
    .local v20, "tagWeather":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v25, "userdef"

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v19

    .line 244
    .local v19, "tagUserdef":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v17, :cond_9

    const-string v25, "location"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 245
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 246
    .local v13, "tag":Ljava/lang/String;
    const-string v25, "location"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 247
    const/4 v5, 0x1

    .line 248
    goto :goto_5

    .line 214
    .end local v5    # "hasTagQueryParams":Z
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v13    # "tag":Ljava/lang/String;
    .end local v16    # "tagFilters":Ljava/lang/String;
    .end local v17    # "tagLocation":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v18    # "tagPeople":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v19    # "tagUserdef":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v20    # "tagWeather":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v6    # "etime":J
    .restart local v14    # "stime":J
    .restart local v21    # "timeSpan":Ljava/lang/String;
    .restart local v22    # "tokenizer":Ljava/util/StringTokenizer;
    :catch_0
    move-exception v9

    .line 215
    .local v9, "ne":Ljava/lang/NumberFormatException;
    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_4

    .line 216
    .end local v9    # "ne":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v4

    .line 217
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 227
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v6    # "etime":J
    .end local v14    # "stime":J
    .end local v21    # "timeSpan":Ljava/lang/String;
    .end local v22    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_8
    const-string v25, "SearchEngine"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " is not support the query with time span. ignored."

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 251
    .restart local v5    # "hasTagQueryParams":Z
    .restart local v16    # "tagFilters":Ljava/lang/String;
    .restart local v17    # "tagLocation":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v18    # "tagPeople":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v19    # "tagUserdef":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v20    # "tagWeather":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_9
    if-eqz v18, :cond_a

    const-string v25, "people"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 252
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 253
    .restart local v13    # "tag":Ljava/lang/String;
    const-string v25, "people"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 254
    const/4 v5, 0x1

    .line 255
    goto :goto_6

    .line 258
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v13    # "tag":Ljava/lang/String;
    :cond_a
    if-eqz v20, :cond_b

    const-string v25, "weather"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 259
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 260
    .restart local v13    # "tag":Ljava/lang/String;
    const-string v25, "weather"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 261
    const/4 v5, 0x1

    .line 262
    goto :goto_7

    .line 270
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v13    # "tag":Ljava/lang/String;
    :cond_b
    if-eqz v19, :cond_c

    const-string v25, "user"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 271
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 272
    .restart local v13    # "tag":Ljava/lang/String;
    const-string v25, "userdef"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 273
    const/4 v5, 0x1

    .line 274
    goto :goto_8

    .line 277
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v13    # "tag":Ljava/lang/String;
    :cond_c
    if-nez v5, :cond_10

    if-eqz p2, :cond_d

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-eqz v25, :cond_10

    .line 278
    :cond_d
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 282
    .end local v5    # "hasTagQueryParams":Z
    .end local v16    # "tagFilters":Ljava/lang/String;
    .end local v17    # "tagLocation":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v18    # "tagPeople":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v19    # "tagUserdef":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v20    # "tagWeather":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_e
    if-eqz p2, :cond_f

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-eqz v25, :cond_10

    .line 283
    :cond_f
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 289
    :cond_10
    if-eqz p4, :cond_11

    invoke-virtual/range {p4 .. p4}, Landroid/os/Bundle;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_11

    .line 290
    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p4

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_12

    .line 291
    const-string v25, "searchfilter"

    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p4

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v24 .. v26}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 298
    :cond_11
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v23

    .line 300
    const-string v25, "SearchEngine"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "makeQueryForSearch() package : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {p1 .. p1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ", uri : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 294
    :cond_12
    const/16 v23, 0x0

    goto/16 :goto_0
.end method

.method private makeQueryParams(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/net/Uri;
    .locals 13
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .local p2, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .line 307
    new-instance v8, Landroid/net/Uri$Builder;

    invoke-direct {v8}, Landroid/net/Uri$Builder;-><init>()V

    .line 309
    .local v8, "uriBuilder":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_0

    .line 310
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 311
    .local v5, "tag":Ljava/lang/String;
    invoke-direct {p0, v5, v8}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeTagQueryForSearch(Ljava/lang/String;Landroid/net/Uri$Builder;)Z

    goto :goto_0

    .line 315
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v5    # "tag":Ljava/lang/String;
    :cond_0
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_3

    .line 316
    invoke-direct {p0, p1, v8}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeTagQueryForSearch(Ljava/lang/String;Landroid/net/Uri$Builder;)Z

    move-result v6

    .line 317
    .local v6, "tagResult":Z
    if-nez v6, :cond_3

    .line 318
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v10, " "

    invoke-direct {v4, p1, v10}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    .local v4, "strToken":Ljava/util/StringTokenizer;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 320
    .local v3, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 322
    .local v1, "idx":I
    :goto_1
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 323
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 325
    .restart local v5    # "tag":Ljava/lang/String;
    invoke-direct {p0, v5, v8}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeTagQueryForSearch(Ljava/lang/String;Landroid/net/Uri$Builder;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 326
    const/4 v1, 0x0

    .line 327
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_1

    .line 331
    :cond_1
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "idx":I
    .local v2, "idx":I
    if-lez v1, :cond_2

    .line 332
    const/16 v10, 0x20

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10, v8}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeTagQueryForSearch(Ljava/lang/String;Landroid/net/Uri$Builder;)Z

    .line 334
    const/4 v1, 0x0

    .line 335
    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_1

    .line 337
    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    :cond_2
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_1

    .line 343
    .end local v1    # "idx":I
    .end local v3    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v4    # "strToken":Ljava/util/StringTokenizer;
    .end local v5    # "tag":Ljava/lang/String;
    .end local v6    # "tagResult":Z
    :cond_3
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    .line 344
    .local v7, "uri":Landroid/net/Uri;
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 346
    .local v9, "uriString":Ljava/lang/String;
    const-string v10, "SearchEngine"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "makeQueryParams() uri : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_5

    :cond_4
    const/4 v7, 0x0

    .end local v7    # "uri":Landroid/net/Uri;
    :cond_5
    return-object v7
.end method

.method private makeTagQueryForSearch(Ljava/lang/String;Landroid/net/Uri$Builder;)Z
    .locals 24
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "builder"    # Landroid/net/Uri$Builder;

    .prologue
    .line 366
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 367
    :cond_0
    const/4 v8, 0x0

    .line 448
    :goto_0
    return v8

    .line 370
    :cond_1
    const/4 v8, 0x0

    .line 371
    .local v8, "bResult":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 372
    .local v2, "cr":Landroid/content/ContentResolver;
    const-string v16, "data match ?"

    .line 374
    .local v16, "selection":Ljava/lang/String;
    const/16 v21, 0x0

    .line 375
    .local v21, "tagtype":Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    const/4 v9, 0x0

    .line 377
    .local v9, "cursor":Landroid/database/Cursor;
    const-string v3, "location="

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "people="

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "weather="

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "userdef="

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 379
    :cond_2
    const-string v3, "="

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 381
    .local v20, "tags":[Ljava/lang/String;
    if-eqz v20, :cond_3

    move-object/from16 v0, v20

    array-length v3, v0

    const/4 v4, 0x1

    if-le v3, v4, :cond_3

    .line 382
    const/4 v3, 0x0

    aget-object v3, v20, v3

    const/4 v4, 0x1

    aget-object v4, v20, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 446
    .end local v20    # "tags":[Ljava/lang/String;
    :cond_3
    :goto_1
    const-string v3, "SearchEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "makeTagQueryForSearch() uri builder : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 385
    :cond_4
    const-string v3, "\\p{Punct}"

    const-string v4, " "

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 387
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$TagsFts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "type"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "rawdata"

    aput-object v6, v4, v5

    const-string v5, "data match ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 393
    if-eqz v9, :cond_3

    .line 394
    const/16 v22, 0x0

    .line 395
    .local v22, "type":I
    const/4 v15, 0x0

    .line 396
    .local v15, "rawData":Ljava/lang/String;
    const/16 v18, 0x0

    .line 398
    .local v18, "tagName":Ljava/lang/String;
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_c

    .line 399
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 401
    .local v17, "tagBuilder":Ljava/lang/StringBuilder;
    const-string v3, "type"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 402
    .local v12, "idx_type":I
    const-string v3, "rawdata"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 403
    .local v11, "idx_rawData":I
    :cond_5
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 404
    invoke-interface {v9, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 405
    invoke-interface {v9, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 406
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->values()[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-result-object v3

    aget-object v21, v3, v22

    .line 407
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->TAG_TYPES:[Ljava/lang/String;

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v4

    aget-object v18, v3, v4

    .line 409
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    sget-object v3, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->USERDEF:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 410
    :cond_6
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    :goto_3
    if-eqz v18, :cond_5

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_5

    .line 416
    const/4 v13, 0x0

    .line 417
    .local v13, "isAppendedValue":Z
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v14

    .line 419
    .local v14, "queryParams":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v14, :cond_8

    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 421
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 422
    .local v23, "value":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 424
    .local v19, "tagValue":Ljava/lang/String;
    if-eqz v23, :cond_7

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 425
    const-string v3, "SearchEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ignored. tag name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", tag value : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    const/4 v13, 0x1

    .line 433
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v19    # "tagValue":Ljava/lang/String;
    .end local v23    # "value":Ljava/lang/String;
    :cond_8
    if-nez v13, :cond_9

    .line 434
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 437
    :cond_9
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    goto/16 :goto_2

    .line 412
    .end local v13    # "isAppendedValue":Z
    .end local v14    # "queryParams":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_a
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 440
    :cond_b
    const/4 v8, 0x1

    .line 442
    .end local v11    # "idx_rawData":I
    .end local v12    # "idx_type":I
    .end local v17    # "tagBuilder":Ljava/lang/StringBuilder;
    :cond_c
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .prologue
    .line 493
    const-string v2, "SearchEngine"

    const-string v3, "cancel"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mUseAsyncSearchEngine:Z

    if-eqz v2, :cond_0

    .line 496
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAsyncExecutorList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAsyncExecutorList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 497
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAsyncExecutorList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;

    .line 498
    .local v0, "executor":Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->cancel(Z)Z

    goto :goto_0

    .line 502
    .end local v0    # "executor":Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryHandler:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

    if-eqz v2, :cond_1

    .line 503
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryHandler:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->cancelOperation(I)V

    .line 506
    :cond_1
    return-void
.end method

.method public prepare(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)I
    .locals 31
    .param p1, "qaction"    # Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    .prologue
    .line 56
    const-string v2, "SearchEngine"

    const-string v9, "prepare() search start"

    invoke-static {v2, v9}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const/16 v17, 0x0

    .line 59
    .local v17, "handleCount":I
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    .line 60
    const/4 v7, 0x0

    .line 62
    .local v7, "tagProviderOnly":Z
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTargets()Ljava/util/ArrayList;

    move-result-object v30

    .line 63
    .local v30, "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTags()Ljava/util/ArrayList;

    move-result-object v29

    .line 64
    .local v29, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getSearchFilters()Landroid/os/Bundle;

    move-result-object v6

    .line 66
    .local v6, "filters":Landroid/os/Bundle;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v4

    .line 67
    .local v4, "query":Ljava/lang/String;
    const/4 v5, 0x0

    .line 70
    .local v5, "queryParamUri":Landroid/net/Uri;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v29, :cond_1

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 71
    const/4 v7, 0x1

    .line 72
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeQueryParams(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/net/Uri;

    move-result-object v5

    .line 77
    :goto_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getOrderedSearchableList()Ljava/util/ArrayList;

    move-result-object v28

    .line 78
    .local v28, "searchables":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/app/SearchableInfo;>;"
    if-eqz v28, :cond_9

    .line 79
    invoke-interface/range {v28 .. v28}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .local v26, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/SearchableInfo;

    .line 80
    .local v3, "searchable":Landroid/app/SearchableInfo;
    const-string v2, "SearchEngine"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "prepare() searchable package : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    if-eqz v30, :cond_2

    .line 83
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 85
    const-string v2, "SearchEngine"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "prepare() searchable package : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", this package is not included in target lists"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 74
    .end local v3    # "searchable":Landroid/app/SearchableInfo;
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v28    # "searchables":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/app/SearchableInfo;>;"
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v4, v1}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeQueryParams(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/net/Uri;

    move-result-object v5

    goto :goto_0

    .line 93
    .restart local v3    # "searchable":Landroid/app/SearchableInfo;
    .restart local v26    # "i$":Ljava/util/Iterator;
    .restart local v28    # "searchables":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/app/SearchableInfo;>;"
    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestThreshold()I

    move-result v9

    if-ge v2, v9, :cond_3

    .line 94
    const-string v2, "SearchEngine"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "prepare() searchable package : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", threshold : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestThreshold()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_3
    move-object/from16 v2, p0

    .line 100
    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeQueryForSearch(Landroid/app/SearchableInfo;Ljava/lang/String;Landroid/net/Uri;Landroid/os/Bundle;Z)Landroid/net/Uri;

    move-result-object v12

    .line 101
    .local v12, "uri":Landroid/net/Uri;
    if-eqz v12, :cond_0

    .line 102
    const/16 v23, 0x0

    .line 103
    .local v23, "selection":Ljava/lang/String;
    const/4 v14, 0x0

    .line 105
    .local v14, "selectionArgs":[Ljava/lang/String;
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 106
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestSelection()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 107
    move-object/from16 v27, v4

    .line 111
    .local v27, "refinedQuery":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->shouldIncludeInInsightSearch()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 112
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->doEncodeQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 115
    :cond_4
    const/4 v2, 0x1

    new-array v14, v2, [Ljava/lang/String;

    .end local v14    # "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v27, v14, v2

    .line 119
    .restart local v14    # "selectionArgs":[Ljava/lang/String;
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestSelection()Ljava/lang/String;

    move-result-object v23

    .line 123
    .end local v27    # "refinedQuery":Ljava/lang/String;
    :cond_5
    if-nez v4, :cond_6

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->shouldIncludeInInsightSearch()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    :cond_6
    const-string v9, "SearchEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "prepare() requested search uri : "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, ", selection : "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, ", args : "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-eqz v14, :cond_7

    const/4 v2, 0x0

    aget-object v2, v14, v2

    :goto_2
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mUseAsyncSearchEngine:Z

    if-eqz v2, :cond_8

    .line 129
    new-instance v8, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestSelection()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    move-object/from16 v16, v0

    move-object/from16 v9, p0

    move-object v11, v4

    move-object v15, v3

    invoke-direct/range {v8 .. v17}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;-><init>(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Landroid/content/ContentResolver;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/Object;Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;I)V

    .line 132
    .local v8, "executor":Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAsyncExecutorList:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    .end local v8    # "executor":Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;
    :goto_3
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 124
    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    .line 135
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryHandler:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

    invoke-virtual {v2, v4}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->setQuery(Ljava/lang/String;)V

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryHandler:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v22, 0x0

    const/16 v25, 0x0

    move-object/from16 v20, v3

    move-object/from16 v21, v12

    move-object/from16 v24, v14

    invoke-virtual/range {v18 .. v25}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->addWorker(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 146
    .end local v3    # "searchable":Landroid/app/SearchableInfo;
    .end local v12    # "uri":Landroid/net/Uri;
    .end local v14    # "selectionArgs":[Ljava/lang/String;
    .end local v23    # "selection":Ljava/lang/String;
    .end local v26    # "i$":Ljava/util/Iterator;
    :cond_9
    return v17
.end method

.method public start()V
    .locals 4

    .prologue
    .line 510
    const-string v2, "SearchEngine"

    const-string v3, "start"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mUseAsyncSearchEngine:Z

    if-eqz v2, :cond_0

    .line 513
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAsyncExecutorList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAsyncExecutorList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 514
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAsyncExecutorList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;

    .line 515
    .local v0, "executor":Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 519
    .end local v0    # "executor":Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryHandler:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

    if-eqz v2, :cond_1

    .line 520
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryHandler:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$SuggestAsyncQueryHandler;->execute()V

    .line 523
    :cond_1
    return-void
.end method
