.class public Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/search/SearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TagLoaderRunnable"
.end annotation


# instance fields
.field private mObject:Ljava/lang/Object;

.field private mParentHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Landroid/os/Handler;Ljava/lang/Object;)V
    .locals 1
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 805
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->mParentHandler:Landroid/os/Handler;

    .line 803
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->mObject:Ljava/lang/Object;

    .line 806
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->mParentHandler:Landroid/os/Handler;

    .line 807
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->mObject:Ljava/lang/Object;

    .line 808
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 812
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->mObject:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->mObject:Ljava/lang/Object;

    instance-of v1, v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    if-eqz v1, :cond_0

    .line 813
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->mObject:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->retrieveTagsFromTagService(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Z
    invoke-static {v2, v1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$1300(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Z

    move-result v0

    .line 814
    .local v0, "hasTags":Z
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->mParentHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 816
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tag retrieved : hasTags = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    .end local v0    # "hasTags":Z
    :cond_0
    return-void
.end method
