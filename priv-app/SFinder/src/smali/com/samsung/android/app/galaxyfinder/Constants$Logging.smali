.class public Lcom/samsung/android/app/galaxyfinder/Constants$Logging;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Logging"
.end annotation


# static fields
.field public static final LOG_ADD_SEARCH_CATEGORY_FEATURE:Ljava/lang/String; = "ADCA"

.field public static final LOG_FORMULA:I = 0x2

.field public static final LOG_FORMULA_FEATURE:Ljava/lang/String; = "WLFR"

.field public static final LOG_KEYWORD:I = 0x1

.field public static final LOG_REMOVE_SEARCH_CATEGORY_FEATURE:Ljava/lang/String; = "RMCA"

.field public static final LOG_SEARCH:I = 0x3

.field public static final LOG_SEARCH_FEATURE:Ljava/lang/String; = "SRCH"

.field public static final LOG_SELECT_SEARCH_RESULT:Ljava/lang/String; = "SRRS"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
