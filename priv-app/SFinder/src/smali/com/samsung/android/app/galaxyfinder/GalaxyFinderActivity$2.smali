.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "GalaxyFinderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$2;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 209
    if-eqz p2, :cond_1

    .line 210
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 211
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 214
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$2;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const/4 v2, 0x1

    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->prepareTagRefresh(IZZ)V
    invoke-static {v1, v2, v3, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$400(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;IZZ)V

    .line 217
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    return-void
.end method
