.class public Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryItem;
.super Ljava/lang/Object;
.source "HistoryItem.java"


# instance fields
.field mDate:Ljava/lang/Long;

.field mKeyword:Ljava/lang/String;

.field mSelected:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "date"    # Ljava/lang/Long;
    .param p3, "select"    # Ljava/lang/Boolean;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryItem;->mKeyword:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryItem;->mDate:Ljava/lang/Long;

    .line 14
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryItem;->mSelected:Ljava/lang/Boolean;

    .line 15
    return-void
.end method


# virtual methods
.method public getHistoryItemDate()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryItem;->mDate:Ljava/lang/Long;

    return-object v0
.end method

.method public getHistoryItemKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryItem;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedState()Z
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryItem;->mSelected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public setSelectedState(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "selected"    # Ljava/lang/Boolean;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryItem;->mSelected:Ljava/lang/Boolean;

    .line 27
    return-void
.end method
