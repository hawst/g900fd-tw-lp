.class public Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
.super Landroid/app/Activity;
.source "GalaxyFinderActivity.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;
.implements Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;
    }
.end annotation


# static fields
.field private static final ENG_MODE:Z

.field private static final REQUEST_CODE_DISABLED_APP:I = 0x0

.field private static final REQUEST_CODE_GOOGLE_VOICE:I = 0x64

.field private static final REQUEST_CODE_NUANCE_VOICE:I = 0x65

.field private static final REQUEST_CODE_SHARE_VIA:I = 0xc8

.field private static final TAG:Ljava/lang/String;

.field private static VERIFICATION_LOG_TAG:Ljava/lang/String;

.field private static mCheckBox:Landroid/widget/CheckBox;

.field private static mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private static final supportCopyContent:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final CHANGE_SELECTION_MODE:I

.field private final IMM_TAG_CLOUD_REFRESH:I

.field private final MIN_REFRESH_DELAYTIME:J

.field private final SEARCH_CATEGORY_REFRESH:I

.field private final WAIT_TAG_CLOUD_REFRESH:I

.field private bWaitingDelayedMessage:Z

.field private mActionBar:Landroid/app/ActionBar;

.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeCallback:Landroid/view/ActionMode$Callback;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCategoryListPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mCategoryStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

.field private mCustomActionBarSearchbox:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

.field private mDateChangedBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mDefaultPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mEnabledSavingHistory:Z

.field private final mHandler:Landroid/os/Handler;

.field private mIsShowingVoiceInputDialog:Z

.field private mIsTablet:Z

.field private mLastKeypadState:Z

.field private mProgressDlg:Landroid/app/ProgressDialog;

.field private mResource:Landroid/content/res/Resources;

.field private mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

.field private mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

.field private mSelectedItemCount:I

.field private mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

.field private mTagObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 87
    const-class v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->TAG:Ljava/lang/String;

    .line 119
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 123
    const-string v0, "VerificationLog"

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->VERIFICATION_LOG_TAG:Ljava/lang/String;

    .line 125
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->ENG_MODE:Z

    .line 1274
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "com.sec.android.app.myfiles"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "com.sec.android.gallery3d"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "com.samsung.android.app.pinboard"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->supportCopyContent:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 97
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mIsTablet:Z

    .line 99
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSelectedItemCount:I

    .line 101
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .line 103
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .line 105
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionBar:Landroid/app/ActionBar;

    .line 107
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;

    .line 109
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    .line 111
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mResource:Landroid/content/res/Resources;

    .line 113
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;

    .line 115
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCustomActionBarSearchbox:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .line 117
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCategoryStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    .line 127
    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mLastKeypadState:Z

    .line 134
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->bWaitingDelayedMessage:Z

    .line 136
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->WAIT_TAG_CLOUD_REFRESH:I

    .line 138
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->IMM_TAG_CLOUD_REFRESH:I

    .line 140
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->SEARCH_CATEGORY_REFRESH:I

    .line 142
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->CHANGE_SELECTION_MODE:I

    .line 144
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->MIN_REFRESH_DELAYTIME:J

    .line 146
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mEnabledSavingHistory:Z

    .line 148
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 150
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    .line 158
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;

    .line 205
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$2;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mDateChangedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 220
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$3;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mTagObserver:Landroid/database/ContentObserver;

    .line 227
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$4;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCategoryListPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 236
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$5;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mDefaultPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSelectedItemCount:I

    return v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
    .param p1, "x1"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSelectedItemCount:I

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->bWaitingDelayedMessage:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/view/ActionMode;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
    .param p1, "x1"    # Landroid/view/ActionMode;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->onMenuSelect()V

    return-void
.end method

.method static synthetic access$1300()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1400()Z
    .locals 1

    .prologue
    .line 84
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->ENG_MODE:Z

    return v0
.end method

.method static synthetic access$1500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->VERIFICATION_LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mIsTablet:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCustomActionBarSearchbox:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->doShareVia()V

    return-void
.end method

.method static synthetic access$1900()Ljava/util/Set;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->supportCopyContent:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->stopActionMode()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->prepareTagRefresh(IZZ)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mEnabledSavingHistory:Z

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mEnabledSavingHistory:Z

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->showDisabledAppDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startVoiceRecognition()V

    return-void
.end method

.method private adjustInputMode(Z)V
    .locals 3
    .param p1, "adjustResize"    # Z

    .prologue
    .line 1215
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1216
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    const/4 v0, 0x0

    .line 1218
    .local v0, "hasFlag":Z
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    and-int/lit8 v2, v2, 0x1

    if-lez v2, :cond_0

    .line 1219
    const/4 v0, 0x1

    .line 1222
    :cond_0
    if-eqz p1, :cond_2

    .line 1224
    if-nez v0, :cond_1

    .line 1225
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 1226
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1235
    :cond_1
    :goto_0
    return-void

    .line 1230
    :cond_2
    if-eqz v0, :cond_1

    .line 1231
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    and-int/lit8 v2, v2, -0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 1232
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0
.end method

.method private copyToClipboard()V
    .locals 2

    .prologue
    .line 1281
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    if-eqz v0, :cond_0

    .line 1282
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$14;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedItems(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;)V

    .line 1331
    :cond_0
    return-void
.end method

.method private createMultiWindowActivity()V
    .locals 2

    .prologue
    .line 270
    :try_start_0
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    sput-object v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :goto_0
    return-void

    .line 271
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    goto :goto_0
.end method

.method private doShareVia()V
    .locals 2

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$13;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$13;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedItems(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;)V

    .line 1144
    return-void
.end method

.method public static getMultiWindowInstance()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1

    .prologue
    .line 277
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_0

    .line 278
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 280
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTagServiceFullSyncState(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 680
    const-string v4, "pref_tagservice_state"

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 682
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v4, "pref_tagservice_fullsync_boot"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 684
    .local v3, "tagServiceStateFlag":I
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getTagServiceFullSyncState : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 686
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 687
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "pref_tagservice_fullsync_boot"

    const/4 v5, 0x2

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 690
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 691
    .local v0, "bResult":Z
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getTagServiceFullSyncState : change tagservicefullsync state = 2, commit ? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    .end local v0    # "bResult":Z
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return v3
.end method

.method private init()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 388
    const-string v6, "audio"

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 393
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 395
    .local v4, "prefs":Landroid/content/SharedPreferences;
    const-string v6, "setting_use_search_history"

    invoke-interface {v4, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mEnabledSavingHistory:Z

    .line 396
    iget-boolean v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mEnabledSavingHistory:Z

    invoke-static {v6}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->setHistoryState(Z)V

    .line 401
    const v6, 0x7f0b00b6

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .line 402
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setSystemUiVisibility(I)V

    .line 403
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    new-instance v7, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setSearchActionListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;)V

    .line 458
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCustomActionBarSearchbox:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setCustomSearchView(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V

    .line 459
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->isMultiWindowMode()Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setMultiWindowModeChanged(Z)V

    .line 460
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v6, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setGenericActionListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;)V

    .line 472
    new-instance v6, Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    const-string v7, "SearchControllerThread"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .line 474
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->start()V

    .line 475
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    new-instance v7, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->setListener(Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;)V

    .line 503
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->isEnabledMenuSelect()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 504
    new-instance v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCategoryStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    .line 531
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCategoryStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setItemStateListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;)V

    .line 534
    :cond_0
    invoke-direct {p0, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getTagServiceFullSyncState(Landroid/content/Context;)I

    move-result v6

    if-ne v6, v9, :cond_7

    .line 535
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->TAG:Ljava/lang/String;

    const-string v7, "start TagReady full sync"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    const-class v6, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;

    const-string v7, "com.samsung.android.app.galaxyfinder.tag.start_full_sync"

    invoke-direct {p0, p0, v6, v7, v9}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startTagService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V

    .line 538
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->TAG:Ljava/lang/String;

    const-string v7, "start Location TagReady full sync"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    const-class v6, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    const-string v7, "com.samsung.android.app.galaxyfinder.tag.start_full_sync"

    invoke-direct {p0, p0, v6, v7, v9}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startTagService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V

    .line 548
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->registerObservers()V

    .line 549
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->registerReceivers()V

    .line 551
    sget-boolean v6, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v6, :cond_5

    .line 552
    const-string v6, "show_data_info"

    invoke-static {p0, v6}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->getSettings(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->isDataConnected(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    const-string v6, "show_wifi_info"

    invoke-static {p0, v6}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->getSettings(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_9

    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->isWifiConnected(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 558
    :cond_2
    const/4 v3, 0x0

    .line 559
    .local v3, "nuancevoice_connection_msg":Ljava/lang/String;
    const/4 v5, 0x0

    .line 560
    .local v5, "title":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 561
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030051

    invoke-virtual {v6, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 563
    .local v2, "layout":Landroid/view/View;
    const v6, 0x7f0b00ae

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    sput-object v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 564
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCheckBox:Landroid/widget/CheckBox;

    const v7, 0x7f0e00b2

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setText(I)V

    .line 565
    const v6, 0x7f0b00ac

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 566
    .local v1, "connectionMessage":Landroid/widget/TextView;
    sget-boolean v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->ENG_MODE:Z

    if-eqz v6, :cond_3

    .line 567
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->VERIFICATION_LOG_TAG:Ljava/lang/String;

    const-string v7, "init - wlan or data"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    :cond_3
    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->isDataConnected(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 570
    const v6, 0x7f0e007f

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 571
    const v6, 0x7f0e007d

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 576
    :cond_4
    :goto_1
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 577
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 578
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 579
    const v6, 0x7f0e007e

    new-instance v7, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$9;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 606
    const v6, 0x7f0e0069

    new-instance v7, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$10;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$10;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 613
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 633
    .end local v0    # "alertDialog":Landroid/app/AlertDialog$Builder;
    .end local v1    # "connectionMessage":Landroid/widget/TextView;
    .end local v2    # "layout":Landroid/view/View;
    .end local v3    # "nuancevoice_connection_msg":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setAppMainWindow(Landroid/view/Window;)V

    .line 635
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->invalidateOptionsMenu()V

    .line 636
    const/4 v6, 0x7

    const/4 v7, 0x0

    invoke-direct {p0, v6, v9, v7}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->prepareTagRefresh(IZZ)V

    .line 639
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->requestSyncDataByWidget()V

    .line 641
    sget-boolean v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->ENG_MODE:Z

    if-eqz v6, :cond_6

    .line 642
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->VERIFICATION_LOG_TAG:Ljava/lang/String;

    const-string v7, "Executed"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    :cond_6
    return-void

    .line 543
    :cond_7
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.samsung.android.app.galaxyfinder.tag.start_service"

    const-class v8, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;

    invoke-direct {v6, v7, v10, p0, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 545
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.samsung.android.app.galaxyfinder.tag.start_service"

    const-class v8, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-direct {v6, v7, v10, p0, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 572
    .restart local v0    # "alertDialog":Landroid/app/AlertDialog$Builder;
    .restart local v1    # "connectionMessage":Landroid/widget/TextView;
    .restart local v2    # "layout":Landroid/view/View;
    .restart local v3    # "nuancevoice_connection_msg":Ljava/lang/String;
    .restart local v5    # "title":Ljava/lang/String;
    :cond_8
    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->isWifiConnected(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 573
    const v6, 0x7f0e0086

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 574
    const v6, 0x7f0e0085

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 615
    .end local v0    # "alertDialog":Landroid/app/AlertDialog$Builder;
    .end local v1    # "connectionMessage":Landroid/widget/TextView;
    .end local v2    # "layout":Landroid/view/View;
    .end local v3    # "nuancevoice_connection_msg":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    :cond_9
    const-string v6, "show_data_info"

    invoke-static {p0, v6}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->getSettings(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    if-ne v6, v9, :cond_a

    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->isDataConnected(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_b

    :cond_a
    const-string v6, "show_wifi_info"

    invoke-static {p0, v6}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->getSettings(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    if-ne v6, v9, :cond_d

    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->isWifiConnected(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 621
    :cond_b
    sget-boolean v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->ENG_MODE:Z

    if-eqz v6, :cond_c

    .line 622
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->VERIFICATION_LOG_TAG:Ljava/lang/String;

    const-string v7, "dialog off"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    :cond_c
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->initContext(Landroid/content/Context;)V

    .line 625
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->create(Landroid/content/Context;)Lcom/nuance/nmdp/speechkit/SpeechKit;

    .line 628
    :cond_d
    sget-boolean v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->ENG_MODE:Z

    if-eqz v6, :cond_5

    .line 629
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->VERIFICATION_LOG_TAG:Ljava/lang/String;

    const-string v7, "init - no wlan, no data"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private multiWindowTrayBarEnable(Z)V
    .locals 3
    .param p1, "TrayBarEnable"    # Z

    .prologue
    .line 740
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 742
    .local v1, "window":Landroid/view/Window;
    if-eqz v1, :cond_0

    .line 743
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 745
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    if-eqz p1, :cond_1

    .line 746
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    and-int/lit8 v2, v2, -0x3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 751
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 753
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    return-void

    .line 748
    .restart local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_1
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    goto :goto_0
.end method

.method private onMenuHelp()V
    .locals 7

    .prologue
    .line 994
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.samsung.helphub"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 995
    .local v2, "info":Landroid/content/pm/PackageInfo;
    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v1, v4, 0xa

    .line 996
    .local v1, "helpVersionCode":I
    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    .line 1027
    .end local v1    # "helpVersionCode":I
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-void

    .line 1001
    .restart local v1    # "helpVersionCode":I
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    .line 1006
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1007
    .local v3, "intent":Landroid/content/Intent;
    const-string v4, "helphub:section"

    const-string v5, "galaxyfinder"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1008
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1022
    .end local v1    # "helpVersionCode":I
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 1024
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1010
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "helpVersionCode":I
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_2
    const/4 v4, 0x3

    if-ne v1, v4, :cond_0

    .line 1018
    :try_start_1
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1019
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v4, "helphub:appid"

    const-string v5, "s_finder"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1020
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private onMenuRefresh()V
    .locals 1

    .prologue
    .line 1030
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->stopActionMode()V

    .line 1032
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getInstance()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->clearLruCache()V

    .line 1034
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    if-eqz v0, :cond_0

    .line 1035
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->requestRequery()V

    .line 1037
    :cond_0
    return-void
.end method

.method private onMenuSelect()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 977
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 978
    const v0, 0x7f0e00aa

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 990
    :goto_0
    return-void

    .line 983
    :cond_0
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSelectedItemCount:I

    .line 985
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_1

    .line 986
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;

    .line 989
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->enableSelectionMode()V

    goto :goto_0
.end method

.method private onMenuSettings()V
    .locals 3

    .prologue
    .line 1040
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1042
    .local v0, "settingsIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1044
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startActivity(Landroid/content/Intent;)V

    .line 1045
    return-void
.end method

.method private prepareTagRefresh(IZZ)V
    .locals 6
    .param p1, "tagCloudType"    # I
    .param p2, "neededDelay"    # Z
    .param p3, "neededRequery"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 249
    if-eqz p2, :cond_2

    .line 250
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->bWaitingDelayedMessage:Z

    if-eqz v1, :cond_0

    .line 251
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 254
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;

    if-eqz p3, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v3, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 256
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 258
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->bWaitingDelayedMessage:Z

    .line 263
    .end local v0    # "msg":Landroid/os/Message;
    :goto_1
    return-void

    :cond_1
    move v1, v3

    .line 254
    goto :goto_0

    .line 260
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;

    if-eqz p3, :cond_3

    move v3, v2

    :cond_3
    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1
.end method

.method private registerObservers()V
    .locals 5

    .prologue
    .line 756
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mTagObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 759
    const-string v1, "pref_package"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 760
    .local v0, "prefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCategoryListPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 762
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 763
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mDefaultPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 764
    return-void
.end method

.method private registerReceivers()V
    .locals 3

    .prologue
    .line 777
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 778
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 779
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 780
    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 782
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 783
    .local v0, "dateChangedFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 784
    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 785
    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 786
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mDateChangedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 787
    return-void
.end method

.method private requestKeypad(ZJ)V
    .locals 4
    .param p1, "flag"    # Z
    .param p2, "delay"    # J

    .prologue
    .line 715
    move v0, p1

    .line 716
    .local v0, "bTurnOn":Z
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCustomActionBarSearchbox:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    if-eqz v1, :cond_0

    .line 717
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Z)V

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 736
    :cond_0
    return-void
.end method

.method private requestSyncDataByWidget()V
    .locals 3

    .prologue
    .line 647
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;->categoryType:[Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;->timeIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 649
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearFocus()V

    .line 650
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mLastKeypadState:Z

    .line 651
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;->categoryType:[Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    iget v2, v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;->timeIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->startSyncTagByLauncher([Ljava/lang/String;I)V

    .line 653
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    .line 655
    :cond_1
    return-void
.end method

.method private setActionBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1048
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionBar:Landroid/app/ActionBar;

    .line 1050
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 1051
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 1053
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCustomActionBarSearchbox:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .line 1055
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCustomActionBarSearchbox:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1056
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1057
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1058
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 1059
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1062
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->isEnabledMenuSelect()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1063
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    .line 1120
    :cond_1
    return-void
.end method

.method private setSyndData(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 658
    if-eqz p1, :cond_1

    .line 660
    const-string v1, "category"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 661
    .local v0, "category":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 662
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    invoke-direct {v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    .line 663
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;->categoryType:[Ljava/lang/String;

    .line 664
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    const-string v2, "time"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;->timeIndex:I

    .line 669
    .end local v0    # "category":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 667
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSyncTagData:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$SyncTagData;

    goto :goto_0
.end method

.method private showDisabledAppDialog(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1334
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1335
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.popupuireceiver"

    const-string v2, "com.sec.android.app.popupuireceiver.DisableApp"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1337
    const-string v1, "app_package_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1339
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1340
    return-void
.end method

.method private startSLinkLocationService()V
    .locals 5

    .prologue
    .line 698
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->useSLinkLocationService()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 700
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v1

    .line 702
    .local v1, "isSignedIn":Z
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Slink platform] isSignedIn = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    if-nez v1, :cond_0

    .line 704
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->startReverseGeocodeService()V

    .line 706
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->TAG:Ljava/lang/String;

    const-string v3, "[Slink platform] SlinkSignIn startReverseGeocodeService"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 712
    .end local v1    # "isSignedIn":Z
    :cond_0
    :goto_0
    return-void

    .line 708
    :catch_0
    move-exception v0

    .line 709
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private startTagService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "state"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 672
    .local p2, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 673
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 674
    const-string v1, "com.samsung.android.app.galaxyfinder.TagServiceState"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 675
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 676
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startTagService() intent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    return-void
.end method

.method private startVoiceRecognition()V
    .locals 7

    .prologue
    .line 1147
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1148
    .local v2, "pm":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 1151
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 1152
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1154
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.speech.extra.LANGUAGE_MODEL"

    const-string v4, "free_form"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1157
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v3, :cond_0

    .line 1158
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v4, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x2

    invoke-virtual {v3, v4, v5, v6}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 1162
    :cond_0
    const/16 v3, 0x64

    invoke-virtual {p0, v1, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1164
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method private stopActionMode()V
    .locals 1

    .prologue
    .line 1123
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1126
    :cond_0
    return-void
.end method

.method private unregisterObservers()V
    .locals 3

    .prologue
    .line 767
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mTagObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 769
    const-string v1, "pref_package"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 770
    .local v0, "prefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCategoryListPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 772
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 773
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mDefaultPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 774
    return-void
.end method

.method private unregisterReceivers()V
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mDateChangedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mDateChangedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 793
    :cond_0
    return-void
.end method


# virtual methods
.method public isMultiWindowMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 285
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 288
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x0

    .line 913
    const/4 v1, 0x0

    .line 915
    .local v1, "result":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 973
    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 974
    return-void

    .line 917
    :sswitch_1
    if-ne p2, v2, :cond_1

    .line 921
    const-string v2, "android.speech.extra.RESULTS"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 924
    .local v0, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    .line 925
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "result":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 927
    .restart local v1    # "result":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 928
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->requestSearchByVoiceRecognition(Ljava/lang/String;)V

    .line 933
    .end local v0    # "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v2, :cond_0

    .line 934
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    goto :goto_0

    .line 939
    :sswitch_2
    iput-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mIsShowingVoiceInputDialog:Z

    .line 942
    if-ne p2, v2, :cond_0

    .line 943
    const-string v2, "android.speech.extra.RESULTS"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 945
    .restart local v0    # "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 946
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "result":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 948
    .restart local v1    # "result":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 949
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->requestSearchByVoiceRecognition(Ljava/lang/String;)V

    goto :goto_0

    .line 959
    .end local v0    # "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :sswitch_3
    const/16 v2, -0x64

    if-eq p2, v2, :cond_0

    .line 963
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 964
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v4, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 915
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0xc8 -> :sswitch_3
    .end sparse-switch
.end method

.method public onChangeWindowMode(Z)V
    .locals 0
    .param p1, "minimize"    # Z

    .prologue
    .line 1202
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 304
    sget-boolean v4, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->ENG_MODE:Z

    if-eqz v4, :cond_0

    .line 305
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->VERIFICATION_LOG_TAG:Ljava/lang/String;

    const-string v5, "onCreate"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 310
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mResource:Landroid/content/res/Resources;

    .line 312
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mResource:Landroid/content/res/Resources;

    const/high16 v5, 0x7f080000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mIsTablet:Z

    .line 314
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 315
    .local v3, "params":Landroid/view/WindowManager$LayoutParams;
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 316
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 317
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 319
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->setActionBar()V

    .line 320
    const v4, 0x7f030055

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->setContentView(I)V

    .line 321
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->createMultiWindowActivity()V

    .line 323
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getMultiWindowInstance()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v2

    .line 325
    .local v2, "multiWinInstance":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    if-eqz v2, :cond_1

    .line 326
    invoke-virtual {v2, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 329
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startSLinkLocationService()V

    .line 330
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 332
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_2

    .line 333
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 334
    .local v0, "bd":Landroid/os/Bundle;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->setSyndData(Landroid/os/Bundle;)V

    .line 337
    .end local v0    # "bd":Landroid/os/Bundle;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->init()V

    .line 338
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 857
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 858
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f100000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 860
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 831
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->unregisterObservers()V

    .line 832
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->unregisterReceivers()V

    .line 834
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mLastKeypadState:Z

    .line 836
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    if-eqz v1, :cond_0

    .line 837
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->quit()Z

    .line 838
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .line 841
    :cond_0
    sget-boolean v1, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v1, :cond_1

    .line 842
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->destroy()V

    .line 845
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getHistoryAdapter()Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    move-result-object v0

    .line 846
    .local v0, "historyAdapter":Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    if-eqz v0, :cond_2

    .line 847
    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 850
    :cond_2
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getInstance()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->clearLruCache()V

    .line 852
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 853
    return-void
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1256
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    and-int/lit16 v0, v1, -0x7001

    .line 1257
    .local v0, "filteredMetaState":I
    invoke-static {v0}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1258
    packed-switch p1, :pswitch_data_0

    .line 1271
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyShortcut(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1

    .line 1260
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    if-eqz v1, :cond_0

    .line 1261
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->focusCustomSearchView()V

    goto :goto_0

    .line 1265
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->copyToClipboard()V

    goto :goto_0

    .line 1258
    :pswitch_data_0
    .packed-switch 0x1f
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 797
    sparse-switch p1, :sswitch_data_0

    .line 826
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 799
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isWritingBuddyShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 803
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 807
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    if-eqz v0, :cond_0

    .line 808
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->processKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 809
    const/4 v0, 0x1

    goto :goto_1

    .line 815
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->requestUpdateByKeypad()V

    goto :goto_0

    .line 818
    :sswitch_2
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->onMenuHelp()V

    goto :goto_0

    .line 821
    :sswitch_3
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->onMenuRefresh()V

    goto :goto_0

    .line 797
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
        0x83 -> :sswitch_2
        0x87 -> :sswitch_3
    .end sparse-switch
.end method

.method public onModeChanged(Z)V
    .locals 1
    .param p1, "isMultiWindowMode"    # Z

    .prologue
    .line 1239
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    if-eqz v0, :cond_0

    .line 1240
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setMultiWindowModeChanged(Z)V

    .line 1242
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 294
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->setIntent(Landroid/content/Intent;)V

    .line 296
    if-eqz p1, :cond_0

    .line 297
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 298
    .local v0, "bd":Landroid/os/Bundle;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->setSyndData(Landroid/os/Bundle;)V

    .line 300
    .end local v0    # "bd":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 889
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 891
    .local v0, "itemID":I
    packed-switch v0, :pswitch_data_0

    .line 908
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 893
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->onMenuSelect()V

    goto :goto_0

    .line 896
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->onMenuRefresh()V

    goto :goto_0

    .line 899
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->onMenuHelp()V

    goto :goto_0

    .line 902
    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->onMenuSettings()V

    goto :goto_0

    .line 891
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b00dd
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->pause()V

    .line 344
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isShowingKeypad()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mLastKeypadState:Z

    .line 345
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideKeypad()V

    .line 348
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 349
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f0b00e0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 865
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearchListHeadUp()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearchListExpand()Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_0
    move v2, v4

    .line 868
    .local v2, "hasResult":Z
    :goto_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->isEnabledMenuSelect()Z

    move-result v5

    and-int v1, v5, v2

    .line 869
    .local v1, "enabledSelectMenu":Z
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->isEnabledMenuRefresh()Z

    move-result v5

    and-int v0, v5, v2

    .line 871
    .local v0, "enabledRefreshMenu":Z
    const v5, 0x7f0b00dd

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    if-eqz v1, :cond_4

    move v5, v4

    :goto_1
    invoke-interface {v6, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 872
    const v5, 0x7f0b00de

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    if-eqz v0, :cond_5

    move v5, v4

    :goto_2
    invoke-interface {v6, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 873
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 874
    const v5, 0x7f0b00df

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 876
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isRestrictedProfile(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 877
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 880
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isInstalledHelp(Landroid/content/pm/PackageManager;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 881
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 884
    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    return v3

    .end local v0    # "enabledRefreshMenu":Z
    .end local v1    # "enabledSelectMenu":Z
    .end local v2    # "hasResult":Z
    :cond_3
    move v2, v3

    .line 865
    goto :goto_0

    .restart local v0    # "enabledRefreshMenu":Z
    .restart local v1    # "enabledSelectMenu":Z
    .restart local v2    # "hasResult":Z
    :cond_4
    move v5, v3

    .line 871
    goto :goto_1

    :cond_5
    move v5, v3

    .line 872
    goto :goto_2
.end method

.method public onRequestWindowInputMode(Z)V
    .locals 0
    .param p1, "adjustResize"    # Z

    .prologue
    .line 1211
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->adjustInputMode(Z)V

    .line 1212
    return-void
.end method

.method public onRequestWindowMoveToBack()V
    .locals 0

    .prologue
    .line 1207
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 353
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 355
    sget-boolean v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->ENG_MODE:Z

    if-eqz v2, :cond_0

    .line 356
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->VERIFICATION_LOG_TAG:Ljava/lang/String;

    const-string v3, "onResume"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->setCurrentOrderChanged()V

    .line 361
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->requestSyncDataByWidget()V

    .line 363
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mLastKeypadState:Z

    const-wide/16 v4, 0x12c

    invoke-direct {p0, v2, v4, v5}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->requestKeypad(ZJ)V

    .line 365
    const/4 v1, 0x0

    .line 366
    .local v1, "needToRefresh":Z
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getLastestActionCategory()Ljava/lang/String;

    move-result-object v0

    .line 368
    .local v0, "actionCategory":Ljava/lang/String;
    const-string v2, "com.android.settings"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 369
    const/4 v1, 0x1

    .line 372
    :cond_1
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->clearLastestActionCategory()V

    .line 374
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    if-eqz v2, :cond_3

    .line 375
    if-eqz v1, :cond_2

    .line 376
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->requestReloadData()V

    .line 379
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mEnabledSavingHistory:Z

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setAllowSavingHistory(Z)V

    .line 381
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getVisibleCustomSearchView()Z

    move-result v2

    if-nez v2, :cond_3

    .line 382
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideKeypad()V

    .line 385
    :cond_3
    return-void
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1247
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 0
    .param p1, "zone"    # I

    .prologue
    .line 1252
    return-void
.end method

.method public startNuanceVoiceRecognition()V
    .locals 9

    .prologue
    .line 1167
    const-string v7, "audio"

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 1168
    .local v1, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v7

    const/4 v8, 0x2

    if-eq v7, v8, :cond_0

    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_2

    .line 1170
    :cond_0
    const v7, 0x7f0e0034

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 1197
    :cond_1
    :goto_0
    return-void

    .line 1176
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    .line 1177
    .local v6, "locale":Ljava/util/Locale;
    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 1178
    .local v3, "defaultLanguage":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    .line 1179
    .local v2, "defaultCountry":Ljava/lang/String;
    const-string v0, "-"

    .line 1181
    .local v0, "LOCALE_DELIMITER":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1183
    .local v5, "languageCode":Ljava/lang/String;
    iget-boolean v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mIsShowingVoiceInputDialog:Z

    if-nez v7, :cond_1

    .line 1184
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->setCurrentLanguage(Ljava/lang/String;)V

    .line 1186
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->isReleased()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1187
    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->create(Landroid/content/Context;)Lcom/nuance/nmdp/speechkit/SpeechKit;

    .line 1190
    :cond_3
    new-instance v4, Landroid/content/Intent;

    const-class v7, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    invoke-direct {v4, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1191
    .local v4, "intent":Landroid/content/Intent;
    const-string v7, "android.speech.extra.LANGUAGE"

    invoke-virtual {v4, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1192
    const/16 v7, 0x65

    invoke-virtual {p0, v4, v7}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1194
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mIsShowingVoiceInputDialog:Z

    goto :goto_0
.end method
