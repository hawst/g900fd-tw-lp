.class public Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb;
.super Ljava/lang/Object;
.source "TagDb.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$TagsFts;,
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$FacesColumns;,
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Faces;,
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$TagsColumns;,
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;,
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$TaggingColumns;,
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tagging;,
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$ContentsColumns;,
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Contents;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag"

.field public static final CONTENTS_TABLE_NAME:Ljava/lang/String; = "contents"

.field public static final DATABASE_NAME:Ljava/lang/String; = "Tag.db"

.field public static final DATABASE_VERSION:I = 0x2

.field public static final FACES_TABLE_NAME:Ljava/lang/String; = "faces"

.field public static final TAGGING_TABLE_NAME:Ljava/lang/String; = "tagging"

.field public static final TAGS_FTS_TABLE_NAME:Ljava/lang/String; = "tags_fts"

.field public static final TAGS_TABLE_NAME:Ljava/lang/String; = "tags"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    return-void
.end method
