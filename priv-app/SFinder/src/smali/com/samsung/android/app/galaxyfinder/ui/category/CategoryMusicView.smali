.class public Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
.source "CategoryMusicView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    .locals 8

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->getDefaultItemCount()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 50
    .local v7, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setValidity(Z)V

    goto :goto_0

    .line 55
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    return-object v0
.end method

.method protected getDefaultItemCount()I
    .locals 2

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method protected getTemplateLayoutId()I
    .locals 1

    .prologue
    .line 60
    const v0, 0x7f030022

    return v0
.end method

.method protected init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    const-string v1, ""

    .line 27
    .local v1, "preStr":Ljava/lang/String;
    const/4 v2, 0x0

    .line 28
    .local v2, "si":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 30
    .local v3, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 31
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "si":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    check-cast v2, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 32
    .restart local v2    # "si":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 34
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-virtual {v2, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setIndicationOfFirstItem(Ljava/lang/String;)V

    .line 30
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->init(Landroid/content/Context;)V

    .line 39
    return-void
.end method
