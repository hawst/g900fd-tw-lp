.class public Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;
.super Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;
.source "LocationTagProbe.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$1;,
        Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LocationTagProbe"


# instance fields
.field private mMediaLocationUpdateReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagType"    # [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .param p3, "attr"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;-><init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mMediaLocationUpdateReceiver:Landroid/content/BroadcastReceiver;

    .line 24
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->init()V

    .line 25
    return-void
.end method

.method private init()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->registerReceiver()V

    .line 29
    return-void
.end method

.method private registerReceiver()V
    .locals 4

    .prologue
    .line 37
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mMediaLocationUpdateReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 38
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$1;)V

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mMediaLocationUpdateReceiver:Landroid/content/BroadcastReceiver;

    .line 39
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag.media_location_update_action"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 40
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mMediaLocationUpdateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 41
    const-string v1, "LocationTagProbe"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Register receiver] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " registered"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method


# virtual methods
.method public monitor(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;)V
    .locals 3
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;

    .prologue
    .line 47
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getReceivedAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 48
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mMediaLocationUpdateReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 49
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$1;)V

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mMediaLocationUpdateReceiver:Landroid/content/BroadcastReceiver;

    .line 50
    new-instance v0, Landroid/content/IntentFilter;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getReceivedAction()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 51
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mMediaLocationUpdateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 53
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->setProbeProcessListener(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;)V

    .line 55
    :cond_1
    return-void
.end method
