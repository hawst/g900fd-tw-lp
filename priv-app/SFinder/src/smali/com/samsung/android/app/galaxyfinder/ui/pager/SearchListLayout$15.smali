.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$15;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

.field final synthetic val$controller:Landroid/view/animation/LayoutAnimationController;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Landroid/view/animation/LayoutAnimationController;)V
    .locals 0

    .prologue
    .line 2399
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$15;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$15;->val$controller:Landroid/view/animation/LayoutAnimationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 2403
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$15;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearching()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2404
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$15;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 2405
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$15;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$15;->val$controller:Landroid/view/animation/LayoutAnimationController;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 2407
    :cond_0
    return-void
.end method
