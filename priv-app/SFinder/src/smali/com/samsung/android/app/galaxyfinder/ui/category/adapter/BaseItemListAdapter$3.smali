.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;
.super Ljava/lang/Object;
.source "BaseItemListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 728
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->val$intent:Landroid/content/Intent;

    iput p3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 732
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->val$intent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 733
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isSelectableMode()Z
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$500(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 734
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->val$position:I

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setCheckedChange(Landroid/view/View;I)V
    invoke-static {v1, p1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/view/View;I)V

    .line 747
    :cond_0
    :goto_0
    return-void

    .line 737
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->val$intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 738
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mListener:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

    invoke-interface {v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;->onActionClicked(Landroid/view/View;)V

    .line 739
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPackageName:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->setLastestActionCategory(Ljava/lang/String;)V

    .line 740
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->sendLogInfo(Landroid/view/View;)V
    invoke-static {v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$600(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/view/View;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 741
    :catch_0
    move-exception v0

    .line 742
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0005

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
