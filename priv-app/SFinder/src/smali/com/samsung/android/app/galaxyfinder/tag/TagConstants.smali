.class public interface abstract Lcom/samsung/android/app/galaxyfinder/tag/TagConstants;
.super Ljava/lang/Object;
.source "TagConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagReadyActionType;,
        Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    }
.end annotation


# static fields
.field public static final BLANK_TAG:Ljava/lang/String; = " "

.field public static final CONTENTS_ID:Ljava/lang/String; = "contents_id"

.field public static final FACE_TAG_QUERY:I = 0x10

.field public static final INVALID_GPS:Ljava/lang/String; = "INVALID_GPS"

.field public static final LOCATION_TAG_QUERY:I = 0x8

.field public static final MEANINGFUL_TAG_DELIMITER:C = '|'

.field public static final MEANINGFUL_TAG_DELIMITER_RGEX:Ljava/lang/String; = "\\|"

.field public static final MEDIA_CONVERT_TYPE:Ljava/lang/String; = "media_convert_type"

.field public static final MEDIA_IDS:Ljava/lang/String; = "media_ids"

.field public static final MEDIA_LOCATION_UPDATE_ACTION:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag.media_location_update_action"

.field public static final MEDIA_PROVIDER_URI:Ljava/lang/String; = "media_provider_uri"

.field public static final NOT_YET_INSERTED_TO_TAG_DB:Ljava/lang/String; = "-10"

.field public static final NULL_TAG:Ljava/lang/String; = "null"

.field public static final PREF_TAGSERVICE_STATE:Ljava/lang/String; = "pref_tagservice_state"

.field public static final RELOAD_TAGSERVICE_ACTION:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag.reload_service"

.field public static final SAMSUNG_LINK_PROVIDE_GEO_ADDRESS_LEVEL:I = 0x9

.field public static final START_ACTION:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag.start_fetch"

.field public static final START_FULL_SYNC_ACTION:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag.start_full_sync"

.field public static final START_SLINK_LOCATIONSERVICE_ACTION:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag.start_slink_locationservice"

.field public static final START_TAGSERVICE_ACTION:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag.start_service"

.field public static final STOP_ACTION:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag.stop_action"

.field public static final STOP_SLINK_LOCATIONSERVICE_ACTION:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag.stop_slink_locationservice"

.field public static final TAGGING_ID:Ljava/lang/String; = "taging_id"

.field public static final TAG_QUERY_FULLY:I = 0x1

.field public static final TAG_QUERY_FULLY_MEDIA_UNMOUNTED:I = 0x2

.field public static final TAG_QUERY_INIT:I = 0x0

.field public static final TAG_QUERY_PARTIALLY:I = 0x4

.field public static final TAG_SERVICE_BOOTUP:I = 0x1

.field public static final TAG_SERVICE_FULLSYNC_MASK:I = 0x7

.field public static final TAG_SERVICE_INIT:I = 0x0

.field public static final TAG_SERVICE_MEDIA_SCANNER_FINISHED:I = 0x4

.field public static final TAG_SERVICE_SDCARD_MOUNTED:I = 0x2

.field public static final TAG_SERVICE_SDCARD_UNMOUNTED:I = 0x3

.field public static final TAG_SERVICE_STATE_INTENT:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.TagServiceState"

.field public static final USER_TAG_DELETE:I = 0x40

.field public static final USER_TAG_QUERY:I = 0x20

.field public static final WEATHER_UPDATE_ACTION:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag.weather_update_action"
