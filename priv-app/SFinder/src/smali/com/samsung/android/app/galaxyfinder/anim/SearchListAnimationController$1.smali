.class Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SearchListAnimationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->playScrollViewFolding(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    iget v1, v1, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_FOLDED:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    .line 166
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->access$000(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 167
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 168
    return-void
.end method
