.class public final enum Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
.super Ljava/lang/Enum;
.source "WeatherType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

.field public static final enum CLEAR_NIGHT:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

.field public static final enum CLOUDY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

.field public static final enum NONE:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

.field public static final enum RAINNY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

.field public static final enum SNOWY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

.field public static final enum SUNNY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;


# instance fields
.field private final stringId:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 7
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    const-string v1, "NONE"

    const v2, 0x7f0e00d5

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->NONE:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    const-string v1, "SUNNY_DAY"

    const v2, 0x7f0e00d5

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->SUNNY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    const-string v1, "CLOUDY_DAY"

    const v2, 0x7f0e00d2

    invoke-direct {v0, v1, v7, v7, v2}, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->CLOUDY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    .line 8
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    const-string v1, "RAINNY_DAY"

    const v2, 0x7f0e00d3

    invoke-direct {v0, v1, v8, v8, v2}, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->RAINNY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    const-string v1, "SNOWY_DAY"

    const v2, 0x7f0e00d4

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->SNOWY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    .line 9
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    const-string v1, "CLEAR_NIGHT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const v4, 0x7f0e00d1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->CLEAR_NIGHT:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    .line 6
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->NONE:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->SUNNY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->CLOUDY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->RAINNY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->SNOWY_DAY:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->CLEAR_NIGHT:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "value"    # I
    .param p4, "stringId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->value:I

    .line 23
    iput p4, p0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->stringId:I

    .line 24
    return-void
.end method

.method public static fromValue(I)Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 35
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    move-result-object v4

    .line 36
    .local v4, "types":[Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
    move-object v0, v4

    .local v0, "arr$":[Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 37
    .local v3, "type":Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->getValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 40
    .end local v3    # "type":Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
    :goto_1
    return-object v3

    .line 36
    .restart local v3    # "type":Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    .end local v3    # "type":Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
    :cond_1
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->NONE:Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    invoke-virtual {v0}, [Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;

    return-object v0
.end method


# virtual methods
.method public getStringId()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->stringId:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/WeatherType;->value:I

    return v0
.end method
