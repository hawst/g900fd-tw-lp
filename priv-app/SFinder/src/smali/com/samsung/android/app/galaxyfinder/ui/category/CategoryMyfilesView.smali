.class public Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
.source "CategoryMyfilesView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    .locals 6

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->getDefaultItemCount()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    return-object v0
.end method

.method protected getDefaultItemCount()I
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method protected getTemplateLayoutId()I
    .locals 1

    .prologue
    .line 46
    const v0, 0x7f030023

    return v0
.end method

.method protected init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;->makeCategorizedSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->replaceItems(Ljava/util/ArrayList;)V

    .line 30
    invoke-super {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->init(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method protected makeCategorizedSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    const/4 v12, 0x3

    new-array v7, v12, [Ljava/util/ArrayList;

    .line 53
    .local v7, "results":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v10, "sorted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v12, v7

    if-ge v2, v12, :cond_0

    .line 56
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    aput-object v12, v7, v2

    .line 55
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 60
    .local v9, "si":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v11

    .line 61
    .local v11, "type":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 62
    const/4 v5, -0x1

    .line 64
    .local v5, "index":I
    :try_start_0
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 69
    :goto_2
    if-ltz v5, :cond_1

    const/4 v12, 0x2

    if-gt v5, v12, :cond_1

    .line 70
    aget-object v12, v7, v5

    invoke-virtual {v12, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 65
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v12, "NumberFormat Error : "

    invoke-static {v12, v11}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 75
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .end local v5    # "index":I
    .end local v9    # "si":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v11    # "type":Ljava/lang/String;
    :cond_2
    move-object v0, v7

    .local v0, "arr$":[Ljava/util/ArrayList;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v4, v3

    .end local v3    # "i$":I
    .local v4, "i$":I
    :goto_3
    if-ge v4, v6, :cond_5

    aget-object v8, v0, v4

    .line 76
    .local v8, "rs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .end local v4    # "i$":I
    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 77
    .restart local v9    # "si":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    if-eqz v9, :cond_3

    .line 78
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 75
    .end local v9    # "si":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_4
    add-int/lit8 v3, v4, 0x1

    .local v3, "i$":I
    move v4, v3

    .end local v3    # "i$":I
    .restart local v4    # "i$":I
    goto :goto_3

    .line 83
    .end local v8    # "rs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    :cond_5
    return-object v10
.end method
