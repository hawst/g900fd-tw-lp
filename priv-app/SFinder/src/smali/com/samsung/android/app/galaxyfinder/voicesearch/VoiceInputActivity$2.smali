.class Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$2;
.super Ljava/lang/Object;
.source "VoiceInputActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$2;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 174
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 179
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v3, 0x0

    .line 183
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$2;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$100(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$2;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$100(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$2;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a050a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 190
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$2;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$100(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$2;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0508

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method
