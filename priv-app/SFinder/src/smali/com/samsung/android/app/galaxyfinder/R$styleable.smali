.class public final Lcom/samsung/android/app/galaxyfinder/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CategoryView:[I

.field public static final CategoryView_columnCount:I = 0x0

.field public static final CategoryView_columnHeight:I = 0x1

.field public static final CategoryView_columnWidth:I = 0x2

.field public static final CategoryView_horizontalSpacing:I = 0x8

.field public static final CategoryView_listType:I = 0x9

.field public static final CategoryView_marginBottom:I = 0x6

.field public static final CategoryView_marginLeft:I = 0x3

.field public static final CategoryView_marginRight:I = 0x4

.field public static final CategoryView_marginTop:I = 0x5

.field public static final CategoryView_verticalSpacing:I = 0x7

.field public static final TwTouchPunchView:[I

.field public static final TwTouchPunchView_punchshape:I = 0x0

.field public static final TwTouchPunchView_punchshow:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2760
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/R$styleable;->CategoryView:[I

    .line 2933
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/R$styleable;->TwTouchPunchView:[I

    return-void

    .line 2760
    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
    .end array-data

    .line 2933
    :array_1
    .array-data 4
        0x7f01000a
        0x7f01000b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
