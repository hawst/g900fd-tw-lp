.class public Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterWeather;
.super Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;
.source "TagConverterWeather.java"


# static fields
.field private static final WEATHER_TABLE:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 9
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Sunny"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Cloudy"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Rainy"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Foggy"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Balmy"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterWeather;->WEATHER_TABLE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method


# virtual methods
.method public convertToMeaningValue(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 44
    const/4 v1, 0x0

    .line 46
    .local v1, "result":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 47
    const-string v3, "value"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 48
    .local v2, "value":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 50
    .local v0, "index":I
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterWeather;->WEATHER_TABLE:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 51
    new-instance v1, Landroid/os/Bundle;

    .end local v1    # "result":Landroid/os/Bundle;
    invoke-direct {v1, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 53
    .restart local v1    # "result":Landroid/os/Bundle;
    const-string v3, "encoding"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 54
    const-string v3, "value"

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterWeather;->WEATHER_TABLE:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .end local v0    # "index":I
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method public convertToRawValue(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 9
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 19
    const/4 v4, 0x0

    .line 21
    .local v4, "result":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 22
    const-string v7, "value"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 23
    .local v5, "value":Ljava/lang/String;
    const/4 v2, 0x0

    .line 25
    .local v2, "index":I
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterWeather;->WEATHER_TABLE:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v6, v0, v1

    .line 26
    .local v6, "weather":Ljava/lang/String;
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 27
    new-instance v4, Landroid/os/Bundle;

    .end local v4    # "result":Landroid/os/Bundle;
    invoke-direct {v4, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 29
    .restart local v4    # "result":Landroid/os/Bundle;
    const-string v7, "encoding"

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 30
    const-string v7, "value"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "index":I
    .end local v3    # "len$":I
    .end local v5    # "value":Ljava/lang/String;
    .end local v6    # "weather":Ljava/lang/String;
    :cond_0
    return-object v4

    .line 35
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "i$":I
    .restart local v2    # "index":I
    .restart local v3    # "len$":I
    .restart local v5    # "value":Ljava/lang/String;
    .restart local v6    # "weather":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 25
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
