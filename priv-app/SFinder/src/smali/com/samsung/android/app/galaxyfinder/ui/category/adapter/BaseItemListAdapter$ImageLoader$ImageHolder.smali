.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;
.super Ljava/lang/Object;
.source "BaseItemListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageHolder"
.end annotation


# instance fields
.field public imageView:Landroid/widget/ImageView;

.field private mAltUri:Landroid/net/Uri;

.field private mBgColor:I

.field private mContentType:I

.field private mEffectType:I

.field private mHighLightRects:Ljava/lang/String;

.field private mImageOrienation:I

.field private mSchemeType:I

.field public mUri:Landroid/net/Uri;

.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 3
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "i"    # Landroid/widget/ImageView;
    .param p4, "altUri"    # Ljava/lang/String;
    .param p5, "rects"    # Ljava/lang/String;
    .param p6, "effect"    # I
    .param p7, "contentType"    # I
    .param p8, "orientation"    # I
    .param p9, "bgColor"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 967
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 952
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mAltUri:Landroid/net/Uri;

    .line 954
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mHighLightRects:Ljava/lang/String;

    .line 956
    const/16 v0, 0xc9

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mEffectType:I

    .line 958
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I

    .line 960
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I

    .line 962
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mImageOrienation:I

    .line 964
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mBgColor:I

    .line 968
    if-eqz p2, :cond_0

    .line 969
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    .line 972
    :cond_0
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    .line 974
    if-eqz p4, :cond_1

    .line 975
    invoke-static {p4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mAltUri:Landroid/net/Uri;

    .line 978
    :cond_1
    if-eqz p5, :cond_2

    .line 979
    iput-object p5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mHighLightRects:Ljava/lang/String;

    .line 982
    :cond_2
    iput p6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mEffectType:I

    .line 984
    if-eqz p2, :cond_3

    .line 985
    const-string v0, "file"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 986
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I

    .line 996
    :cond_3
    :goto_0
    iput p7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I

    .line 997
    iput p8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mImageOrienation:I

    .line 998
    iput p9, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mBgColor:I

    .line 999
    return-void

    .line 987
    :cond_4
    const-string v0, "content"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 988
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I

    goto :goto_0

    .line 989
    :cond_5
    const-string v0, "android.resource"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 990
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I

    goto :goto_0

    .line 992
    :cond_6
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 946
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mImageOrienation:I

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 946
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mEffectType:I

    return v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 946
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mBgColor:I

    return v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mAltUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mHighLightRects:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 946
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 946
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I

    return v0
.end method
