.class public Lcom/samsung/android/app/galaxyfinder/util/FontLoader;
.super Ljava/lang/Object;
.source "FontLoader.java"


# static fields
.field public static final TAG:Ljava/lang/String;

.field public static mRobotoBold:Landroid/graphics/Typeface;

.field public static mRobotoLight:Landroid/graphics/Typeface;

.field public static mRobotoRegular:Landroid/graphics/Typeface;

.field public static mRobotoThin:Landroid/graphics/Typeface;

.field public static mSamsungSansLight:Landroid/graphics/Typeface;

.field public static mSamsungSansRegular:Landroid/graphics/Typeface;

.field public static mSamsungSansThin:Landroid/graphics/Typeface;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15
    const-class v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->TAG:Ljava/lang/String;

    .line 17
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoBold:Landroid/graphics/Typeface;

    .line 19
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoRegular:Landroid/graphics/Typeface;

    .line 21
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoThin:Landroid/graphics/Typeface;

    .line 23
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoLight:Landroid/graphics/Typeface;

    .line 25
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansThin:Landroid/graphics/Typeface;

    .line 27
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansLight:Landroid/graphics/Typeface;

    .line 29
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansRegular:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createTypefaceFromContext(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 7
    .param p0, "pkgName"    # Ljava/lang/String;
    .param p1, "fontpath"    # Ljava/lang/String;

    .prologue
    .line 125
    const/4 v3, 0x0

    .line 127
    .local v3, "typeface":Landroid/graphics/Typeface;
    const/4 v2, 0x0

    .line 130
    .local v2, "flipContext":Landroid/content/Context;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, p0, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 136
    if-eqz v2, :cond_0

    .line 137
    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 138
    .local v0, "am":Landroid/content/res/AssetManager;
    invoke-static {v0, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 144
    .end local v0    # "am":Landroid/content/res/AssetManager;
    :goto_0
    return-object v3

    .line 140
    :cond_0
    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 142
    goto :goto_0

    .line 132
    :catch_0
    move-exception v1

    .line 133
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 134
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "font ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") is not enabled."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    if-eqz v2, :cond_1

    .line 137
    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 138
    .restart local v0    # "am":Landroid/content/res/AssetManager;
    invoke-static {v0, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 139
    goto :goto_0

    .line 140
    .end local v0    # "am":Landroid/content/res/AssetManager;
    :cond_1
    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 142
    goto :goto_0

    .line 136
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v4

    if-eqz v2, :cond_2

    .line 137
    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 138
    .restart local v0    # "am":Landroid/content/res/AssetManager;
    invoke-static {v0, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 140
    .end local v0    # "am":Landroid/content/res/AssetManager;
    :goto_1
    throw v4

    :cond_2
    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_1
.end method

.method private static createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 5
    .param p0, "fontpath"    # Ljava/lang/String;

    .prologue
    .line 109
    const/4 v1, 0x0

    .line 112
    .local v1, "typeface":Landroid/graphics/Typeface;
    :try_start_0
    invoke-static {p0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 116
    if-nez v1, :cond_0

    .line 117
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 121
    :cond_0
    :goto_0
    return-object v1

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "font ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") is not enabled."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    if-nez v1, :cond_0

    .line 117
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0

    .line 116
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-nez v1, :cond_1

    .line 117
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :cond_1
    throw v2
.end method

.method public static getFontTypeface(I)Landroid/graphics/Typeface;
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 32
    const/4 v0, 0x4

    if-ne p0, v0, :cond_1

    .line 33
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansThin:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    .line 34
    const-string v0, "/system/fonts/SamsungSans-Thin.ttf"

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansThin:Landroid/graphics/Typeface;

    .line 37
    :cond_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansThin:Landroid/graphics/Typeface;

    .line 105
    :goto_0
    return-object v0

    .line 40
    :cond_1
    const/4 v0, 0x5

    if-ne p0, v0, :cond_3

    .line 41
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansLight:Landroid/graphics/Typeface;

    if-nez v0, :cond_2

    .line 42
    const-string v0, "/system/fonts/SamsungSans-Light.ttf"

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansLight:Landroid/graphics/Typeface;

    .line 45
    :cond_2
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansLight:Landroid/graphics/Typeface;

    goto :goto_0

    .line 48
    :cond_3
    const/4 v0, 0x6

    if-ne p0, v0, :cond_5

    .line 49
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansRegular:Landroid/graphics/Typeface;

    if-nez v0, :cond_4

    .line 50
    const-string v0, "com.monotype.android.font.samsungsans"

    const-string v1, "fonts/Samsungsans.ttf"

    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->createTypefaceFromContext(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansRegular:Landroid/graphics/Typeface;

    .line 54
    :cond_4
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mSamsungSansRegular:Landroid/graphics/Typeface;

    goto :goto_0

    .line 57
    :cond_5
    sget-boolean v0, Landroid/graphics/Typeface;->isFlipFontUsed:Z

    if-nez v0, :cond_a

    .line 59
    packed-switch p0, :pswitch_data_0

    .line 92
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported typeface : %d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0

    .line 61
    :pswitch_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoBold:Landroid/graphics/Typeface;

    if-nez v0, :cond_6

    .line 62
    const-string v0, "/system/fonts/Roboto-Bold.ttf"

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoBold:Landroid/graphics/Typeface;

    .line 65
    :cond_6
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoBold:Landroid/graphics/Typeface;

    goto :goto_0

    .line 68
    :pswitch_1
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoRegular:Landroid/graphics/Typeface;

    if-nez v0, :cond_7

    .line 69
    const-string v0, "/system/fonts/Roboto-Regular.ttf"

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoRegular:Landroid/graphics/Typeface;

    .line 72
    :cond_7
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoRegular:Landroid/graphics/Typeface;

    goto :goto_0

    .line 75
    :pswitch_2
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoThin:Landroid/graphics/Typeface;

    if-nez v0, :cond_8

    .line 76
    const-string v0, "/system/fonts/Roboto-Thin.ttf"

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoThin:Landroid/graphics/Typeface;

    .line 79
    :cond_8
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoThin:Landroid/graphics/Typeface;

    goto :goto_0

    .line 82
    :pswitch_3
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoLight:Landroid/graphics/Typeface;

    if-nez v0, :cond_9

    .line 83
    const-string v0, "/system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoLight:Landroid/graphics/Typeface;

    .line 84
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoLight:Landroid/graphics/Typeface;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    if-ne v0, v1, :cond_9

    .line 85
    const-string v0, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->createTypefaceFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoLight:Landroid/graphics/Typeface;

    .line 89
    :cond_9
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->mRobotoLight:Landroid/graphics/Typeface;

    goto/16 :goto_0

    .line 97
    :cond_a
    packed-switch p0, :pswitch_data_1

    .line 101
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto/16 :goto_0

    .line 99
    :pswitch_4
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    goto/16 :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 97
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method
