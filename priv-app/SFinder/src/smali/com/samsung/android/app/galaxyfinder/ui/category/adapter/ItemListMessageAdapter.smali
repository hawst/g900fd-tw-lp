.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListMessageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$1;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;
    }
.end annotation


# static fields
.field private static final CARD_TYPE_ATTACHMENT_RECEIVE:I = 0x2

.field private static final CARD_TYPE_ATTACHMENT_SEND:I = 0x3

.field private static final CARD_TYPE_CONVERSATION:I = 0x4

.field private static final CARD_TYPE_RECEIVE:I = 0x0

.field private static final CARD_TYPE_SENT_FAILED:I = 0x5

.field private static final CARD_TYPE_SENT_FAILED_ATTACH:I = 0x6


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 34
    return-void
.end method

.method private getLayoutResourceId(I)I
    .locals 1
    .param p1, "msgType"    # I

    .prologue
    .line 122
    const v0, 0x7f03003c

    .line 124
    .local v0, "layoutId":I
    packed-switch p1, :pswitch_data_0

    .line 140
    :pswitch_0
    const v0, 0x7f03003d

    .line 144
    :goto_0
    return v0

    .line 127
    :pswitch_1
    const v0, 0x7f03003c

    .line 128
    goto :goto_0

    .line 131
    :pswitch_2
    const v0, 0x7f03003f

    .line 132
    goto :goto_0

    .line 136
    :pswitch_3
    const v0, 0x7f03003e

    .line 137
    goto :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private getMessageType(Ljava/lang/String;)I
    .locals 2
    .param p1, "targetType"    # Ljava/lang/String;

    .prologue
    .line 148
    const/4 v1, 0x0

    .line 151
    .local v1, "type":I
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 156
    :goto_0
    return v1

    .line 152
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 22
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 38
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v13

    .line 39
    .local v13, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v10, 0x0

    .line 41
    .local v10, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 43
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v18

    .line 45
    .local v18, "targetType":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->getMessageType(Ljava/lang/String;)I

    move-result v15

    .line 46
    .local v15, "msgType":I
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->getLayoutResourceId(I)I

    move-result v14

    .line 48
    .local v14, "layoutId":I
    if-nez p2, :cond_5

    .line 49
    sget-object v19, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v14, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 51
    new-instance v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;

    .end local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-direct {v10, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$1;)V

    .line 53
    .restart local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;
    const v19, 0x7f0b0060

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->tvSender:Landroid/widget/TextView;

    .line 54
    const v19, 0x7f0b0052

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->tvBody:Landroid/widget/TextView;

    .line 55
    const v19, 0x7f0b0056

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->tvDate:Landroid/widget/TextView;

    .line 56
    const v19, 0x7f0b0075

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->tvFailed:Landroid/widget/TextView;

    .line 57
    const v19, 0x7f0b004e

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/ImageView;

    move-object/from16 v0, v19

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->ivProfile:Landroid/widget/ImageView;

    .line 58
    const v19, 0x7f0b0073

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/ImageView;

    move-object/from16 v0, v19

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->ivAttachment:Landroid/widget/ImageView;

    .line 59
    const v19, 0x7f0b004d

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup;

    move-object/from16 v0, v19

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    .line 60
    const v19, 0x7f0b0045

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup;

    move-object/from16 v0, v19

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    .line 62
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 67
    :goto_0
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v16

    .line 68
    .local v16, "sender":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v5

    .line 69
    .local v5, "body":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v7

    .line 70
    .local v7, "date":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v17

    .line 71
    .local v17, "sortKeyValue":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v11

    .line 72
    .local v11, "icon":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v4

    .line 74
    .local v4, "bgColor":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v12

    .line 76
    .local v12, "intent":Landroid/content/Intent;
    const/4 v6, 0x0

    .line 78
    .local v6, "color":I
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->tvSender:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v16

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 79
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->tvBody:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v5, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 81
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->ivAttachment:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1

    .line 82
    const/16 v19, 0x2

    move/from16 v0, v19

    if-eq v15, v0, :cond_0

    const/16 v19, 0x3

    move/from16 v0, v19

    if-eq v15, v0, :cond_0

    const/16 v19, 0x6

    move/from16 v0, v19

    if-ne v15, v0, :cond_6

    .line 84
    :cond_0
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->ivAttachment:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 90
    :cond_1
    :goto_1
    const/16 v19, 0x5

    move/from16 v0, v19

    if-eq v15, v0, :cond_2

    const/16 v19, 0x6

    move/from16 v0, v19

    if-ne v15, v0, :cond_7

    .line 91
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e0033

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 92
    .local v9, "error":Ljava/lang/String;
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->tvFailed:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v9, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 97
    .end local v9    # "error":Ljava/lang/String;
    :goto_2
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_3

    .line 98
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p1

    move-object/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->setSubHeaderView(Landroid/view/ViewGroup;ILjava/lang/String;)V

    .line 101
    :cond_3
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 102
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->ivProfile:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f0200d9

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 115
    :cond_4
    :goto_3
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    if-eqz v19, :cond_a

    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p1

    invoke-virtual {v0, v1, v12, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 118
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v19

    return-object v19

    .line 64
    .end local v4    # "bgColor":Ljava/lang/String;
    .end local v5    # "body":Ljava/lang/String;
    .end local v6    # "color":I
    .end local v7    # "date":Ljava/lang/String;
    .end local v11    # "icon":Ljava/lang/String;
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v16    # "sender":Ljava/lang/String;
    .end local v17    # "sortKeyValue":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;
    check-cast v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;

    .restart local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;
    goto/16 :goto_0

    .line 86
    .restart local v4    # "bgColor":Ljava/lang/String;
    .restart local v5    # "body":Ljava/lang/String;
    .restart local v6    # "color":I
    .restart local v7    # "date":Ljava/lang/String;
    .restart local v11    # "icon":Ljava/lang/String;
    .restart local v12    # "intent":Landroid/content/Intent;
    .restart local v16    # "sender":Ljava/lang/String;
    .restart local v17    # "sortKeyValue":Ljava/lang/String;
    :cond_6
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->ivAttachment:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 94
    :cond_7
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->tvDate:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    goto :goto_2

    .line 103
    :cond_8
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->ivProfile:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v19

    if-nez v19, :cond_4

    .line 104
    if-eqz v4, :cond_9

    .line 106
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 112
    :cond_9
    :goto_5
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter$MessageItemViewHolder;->ivProfile:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v11, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMessageAdapter;->setImageViewForContactRoundedStyle(Landroid/widget/ImageView;Ljava/lang/String;I)V

    goto :goto_3

    .line 107
    :catch_0
    move-exception v8

    .line 108
    .local v8, "e":Ljava/lang/NumberFormatException;
    const/4 v6, 0x0

    goto :goto_5

    .end local v8    # "e":Ljava/lang/NumberFormatException;
    :cond_a
    move-object/from16 v19, p2

    .line 115
    goto :goto_4
.end method
