.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelUpdateRequest()V
    .locals 2

    .prologue
    const/16 v1, 0x12f

    .line 244
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 247
    :cond_0
    return-void
.end method

.method public onMoveScroll(II)V
    .locals 4
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I

    .prologue
    const/16 v2, 0x12f

    .line 210
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 214
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 216
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 217
    return-void
.end method

.method public onMultiSelectionChanged(IIIIII)V
    .locals 9
    .param p1, "position"    # I
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I
    .param p6, "offset"    # I

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$600(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    move-result-object v8

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;IIIIII)V

    invoke-virtual {v8, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMultiSelectionEnd()V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method public onMultiSelectionStart()V
    .locals 5

    .prologue
    .line 280
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSelectionMode()Z

    move-result v3

    if-nez v3, :cond_0

    .line 281
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$700(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 282
    const-string v3, "PENSElECT"

    const-string v4, "trying to enter selection mode"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$700(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;->onChangeSelectionModeState(Z)V

    .line 289
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 290
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 292
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 293
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 295
    .local v2, "view":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->prepareMultiSelection()V

    .line 292
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 298
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "view":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    :cond_1
    return-void
.end method

.method public onPinchEvent(ZI)V
    .locals 7
    .param p1, "bZoomin"    # Z
    .param p2, "y"    # I

    .prologue
    const/4 v6, 0x0

    .line 221
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 222
    .local v3, "viewCnt":I
    const/4 v1, 0x0

    .line 223
    .local v1, "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    const/4 v0, 0x0

    .line 224
    .local v0, "bFindView":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 225
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 227
    .restart local v1    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-static {v1, p2}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->findIndexInScreenByY(Landroid/view/View;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 228
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryExpandedMode()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    .line 229
    const/4 v0, 0x1

    .line 234
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setCategoryViewMode(Z)V
    invoke-static {v4, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Z)V

    .line 235
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 236
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 240
    :goto_1
    return-void

    .line 224
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 238
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    move-result-object v4

    invoke-virtual {v4, v6, v6}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->scrollTo(II)V

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideKeypad()V

    .line 252
    return-void
.end method
