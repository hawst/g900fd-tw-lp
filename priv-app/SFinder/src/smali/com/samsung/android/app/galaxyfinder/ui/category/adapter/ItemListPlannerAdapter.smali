.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListPlannerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;
    }
.end annotation


# static fields
.field static final EXTRAFLAGS_ALARM:I = 0x4

.field static final EXTRAFLAGS_PRIORITY_HIGH:I = 0x40

.field static final EXTRAFLAGS_PRIORITY_LOW:I = 0x10

.field static final EXTRAFLAGS_PRIORITY_MEDIUM:I = 0x20

.field static final EXTRAFLAGS_REPEAT:I = 0x8

.field static final EXTRAFLAGS_TASK_CHECKED:I = 0x1

.field static final EXTRAFLAGS_TASK_UNCHECKED:I


# instance fields
.field private final TEMPLATE_PLANNER_SUBTYPE_EVENT:I

.field private final TEMPLATE_PLANNER_SUBTYPE_EVENT_OTHER:I

.field private final TEMPLATE_PLANNER_SUBTYPE_INK:I

.field private final TEMPLATE_PLANNER_SUBTYPE_TASK:I

.field private final TEMPLATE_PLANNER_SUBTYPE_TASK_DESC:I

.field private mStartGridIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->TEMPLATE_PLANNER_SUBTYPE_TASK:I

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->TEMPLATE_PLANNER_SUBTYPE_EVENT:I

    .line 34
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->TEMPLATE_PLANNER_SUBTYPE_EVENT_OTHER:I

    .line 36
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->TEMPLATE_PLANNER_SUBTYPE_INK:I

    .line 38
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->TEMPLATE_PLANNER_SUBTYPE_TASK_DESC:I

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->convertStringToInt(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;Landroid/widget/TextView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # Z

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setTextStrike(Landroid/widget/TextView;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;Landroid/content/ComponentName;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;
    .param p1, "x1"    # Landroid/content/ComponentName;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setTaskStatus(Landroid/content/ComponentName;Ljava/lang/String;Z)V

    return-void
.end method

.method private convertStringToInt(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;)I
    .locals 3
    .param p1, "item"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .prologue
    .line 292
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v1

    .line 293
    .local v1, "extra":Ljava/lang/String;
    const/4 v2, -0x1

    .line 295
    .local v2, "extraInt":I
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 299
    :goto_0
    return v2

    .line 296
    :catch_0
    move-exception v0

    .line 297
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method private getPriority(I)I
    .locals 1
    .param p1, "extra"    # I

    .prologue
    .line 304
    and-int/lit8 v0, p1, 0x10

    if-lez v0, :cond_0

    .line 305
    const v0, 0x7f0200a4

    .line 311
    :goto_0
    return v0

    .line 306
    :cond_0
    and-int/lit8 v0, p1, 0x20

    if-lez v0, :cond_1

    .line 307
    const/4 v0, 0x0

    goto :goto_0

    .line 308
    :cond_1
    and-int/lit8 v0, p1, 0x40

    if-lez v0, :cond_2

    .line 309
    const v0, 0x7f0200a3

    goto :goto_0

    .line 311
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private isVisible(II)Z
    .locals 1
    .param p1, "extra"    # I
    .param p2, "flag"    # I

    .prologue
    .line 315
    and-int v0, p1, p2

    if-lez v0, :cond_0

    .line 316
    const/4 v0, 0x1

    .line 318
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setTaskStatus(Landroid/content/ComponentName;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "component"    # Landroid/content/ComponentName;
    .param p2, "taskID"    # Ljava/lang/String;
    .param p3, "isDone"    # Z

    .prologue
    .line 358
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.FINDO_ITEM_STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 360
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "component"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 361
    const-string v1, "id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    const-string v1, "value"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 364
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 365
    return-void
.end method

.method private setTextStrike(Landroid/widget/TextView;Z)V
    .locals 9
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "strike"    # Z

    .prologue
    const/4 v8, 0x0

    .line 368
    new-instance v3, Landroid/text/SpannableString;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 369
    .local v3, "spannable":Landroid/text/SpannableString;
    new-instance v6, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v6}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    invoke-virtual {v3, v6, v8, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 371
    if-eqz p2, :cond_0

    .line 372
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    :goto_0
    return-void

    .line 374
    :cond_0
    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v6

    const-class v7, Landroid/text/style/StrikethroughSpan;

    invoke-virtual {v3, v8, v6, v7}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/text/style/StrikethroughSpan;

    .line 377
    .local v5, "uspans":[Landroid/text/style/StrikethroughSpan;
    move-object v0, v5

    .local v0, "arr$":[Landroid/text/style/StrikethroughSpan;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 378
    .local v4, "us":Landroid/text/style/StrikethroughSpan;
    invoke-virtual {v3, v4}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    .line 377
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 381
    .end local v4    # "us":Landroid/text/style/StrikethroughSpan;
    :cond_1
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public checkItemDispaly(ILandroid/view/ViewGroup;)V
    .locals 5
    .param p1, "i"    # I
    .param p2, "parents"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x1

    .line 338
    const/4 v0, 0x0

    .line 340
    .local v0, "rowIndex":I
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mColumnCnt:I

    if-le v1, v4, :cond_2

    .line 341
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 342
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    sub-int v2, p1, v2

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mColumnCnt:I

    div-int/2addr v2, v3

    add-int v0, v1, v2

    .line 347
    :goto_0
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mFirstVisibleIndex:I

    if-lt v0, v1, :cond_1

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mLastVisibleIndex:I

    if-gt v0, v1, :cond_1

    .line 348
    iput-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mIsDisplay:Z

    .line 355
    :goto_1
    return-void

    .line 344
    :cond_0
    move v0, p1

    goto :goto_0

    .line 350
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mIsDisplay:Z

    goto :goto_1

    .line 353
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    goto :goto_1
.end method

.method public getFirstItemIndexInRow(II)I
    .locals 3
    .param p1, "row"    # I
    .param p2, "orientation"    # I

    .prologue
    .line 63
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    if-gt p1, v1, :cond_0

    .line 67
    .end local p1    # "row":I
    :goto_0
    return p1

    .line 66
    .restart local p1    # "row":I
    :cond_0
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    sub-int v0, p1, v1

    .line 67
    .local v0, "grid":I
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mColumnCnt:I

    mul-int/2addr v2, v0

    add-int p1, v1, v2

    goto :goto_0
.end method

.method public getRowByItemIndex(I)I
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 73
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    if-gt p1, v1, :cond_0

    .line 77
    .end local p1    # "index":I
    :goto_0
    return p1

    .line 76
    .restart local p1    # "index":I
    :cond_0
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    sub-int v0, p1, v1

    .line 77
    .local v0, "grid":I
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mColumnCnt:I

    div-int v2, v0, v2

    add-int p1, v1, v2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 36
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 83
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v16

    .line 84
    .local v16, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    new-instance v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;

    invoke-direct {v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;-><init>()V

    .line 86
    .local v13, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;
    const/16 v25, 0x0

    .line 87
    .local v25, "str1":Ljava/lang/String;
    const/16 v26, 0x0

    .line 88
    .local v26, "str2":Ljava/lang/String;
    const/16 v27, 0x0

    .line 89
    .local v27, "subTitle":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v12

    .line 91
    .local v12, "groupName":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v8

    .line 92
    .local v8, "colorCode":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v31

    .line 94
    .local v31, "timeInfo":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v11

    .line 95
    .local v11, "extraFlags":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v29

    .line 97
    .local v29, "targetType":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v15

    .line 99
    .local v15, "intent":Landroid/content/Intent;
    const/16 v23, 0x0

    .line 101
    .local v23, "resultTime":Ljava/util/Date;
    const/4 v6, 0x0

    .line 102
    .local v6, "checkbox":Z
    const/4 v5, 0x0

    .line 103
    .local v5, "alarm":Z
    const/16 v22, 0x0

    .line 104
    .local v22, "repeat":Z
    const/16 v21, -0x1

    .line 106
    .local v21, "priority":I
    const-wide/16 v18, 0x0

    .line 107
    .local v18, "millisec":J
    const/4 v10, -0x1

    .line 108
    .local v10, "extra":I
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->convertStringToInt(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;)I

    move-result v10

    .line 109
    const/16 v32, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v10, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->isVisible(II)Z

    move-result v6

    .line 110
    const/16 v32, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v10, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->isVisible(II)Z

    move-result v5

    .line 111
    const/16 v32, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v10, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->isVisible(II)Z

    move-result v22

    .line 112
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->getPriority(I)I

    move-result v21

    .line 114
    const/16 v28, 0x0

    .line 115
    .local v28, "subType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mResources:Landroid/content/res/Resources;

    move-object/from16 v32, v0

    const v33, 0x7f09002c

    invoke-virtual/range {v32 .. v33}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 118
    .local v4, "accountColor":I
    if-eqz v29, :cond_0

    .line 119
    :try_start_0
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    .line 122
    :cond_0
    const/16 v32, 0x3

    move/from16 v0, v28

    move/from16 v1, v32

    if-eq v0, v1, :cond_1

    .line 123
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 129
    :cond_1
    :goto_0
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mPosition:I

    .line 131
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 134
    if-eqz v31, :cond_2

    .line 135
    :try_start_1
    invoke-static/range {v31 .. v31}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 137
    const-wide/16 v32, 0x0

    cmp-long v32, v18, v32

    if-gtz v32, :cond_5

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    const v33, 0x7f0e008b

    invoke-virtual/range {v32 .. v33}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v27

    .line 148
    :cond_2
    :goto_1
    if-nez v23, :cond_3

    .line 149
    new-instance v23, Ljava/util/Date;

    .end local v23    # "resultTime":Ljava/util/Date;
    invoke-direct/range {v23 .. v23}, Ljava/util/Date;-><init>()V

    .line 152
    .restart local v23    # "resultTime":Ljava/util/Date;
    :cond_3
    :goto_2
    packed-switch v28, :pswitch_data_0

    .line 277
    :cond_4
    :goto_3
    const v32, 0x7f0b004d

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    .line 279
    .local v17, "mainGroup":Landroid/view/View;
    if-eqz v17, :cond_14

    .line 280
    const v32, 0x7f0200f2

    move-object/from16 v0, v17

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 281
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, p1

    invoke-virtual {v0, v1, v15, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 286
    :goto_4
    move-object/from16 v0, p2

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 288
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v32

    return-object v32

    .line 125
    .end local v17    # "mainGroup":Landroid/view/View;
    :catch_0
    move-exception v20

    .line 126
    .local v20, "nfe":Ljava/lang/NumberFormatException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0

    .line 140
    .end local v20    # "nfe":Ljava/lang/NumberFormatException;
    :cond_5
    :try_start_2
    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 141
    .end local v23    # "resultTime":Ljava/util/Date;
    .local v24, "resultTime":Ljava/util/Date;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-static/range {v32 .. v32}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v27

    move-object/from16 v23, v24

    .end local v24    # "resultTime":Ljava/util/Date;
    .restart local v23    # "resultTime":Ljava/util/Date;
    goto :goto_1

    .line 144
    :catch_1
    move-exception v9

    .line 145
    .local v9, "e":Ljava/lang/NumberFormatException;
    :goto_5
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 146
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-wide v18

    .line 148
    if-nez v23, :cond_3

    .line 149
    new-instance v23, Ljava/util/Date;

    .end local v23    # "resultTime":Ljava/util/Date;
    invoke-direct/range {v23 .. v23}, Ljava/util/Date;-><init>()V

    .restart local v23    # "resultTime":Ljava/util/Date;
    goto :goto_2

    .line 148
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v32

    :goto_6
    if-nez v23, :cond_6

    .line 149
    new-instance v23, Ljava/util/Date;

    .end local v23    # "resultTime":Ljava/util/Date;
    invoke-direct/range {v23 .. v23}, Ljava/util/Date;-><init>()V

    .restart local v23    # "resultTime":Ljava/util/Date;
    :cond_6
    throw v32

    .line 157
    :pswitch_0
    if-nez p2, :cond_11

    .line 158
    sget-object v32, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v33, 0x7f030045

    const/16 v34, 0x0

    const/16 v35, 0x0

    invoke-virtual/range {v32 .. v35}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 161
    const v32, 0x7f0b0070

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/TextView;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv1:Landroid/widget/TextView;

    .line 162
    const v32, 0x7f0b006c

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/TextView;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv2:Landroid/widget/TextView;

    .line 163
    const v32, 0x7f0b0094

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/TextView;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv3:Landroid/widget/TextView;

    .line 164
    const v32, 0x7f0b0093

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/CheckBox;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->cb1:Landroid/widget/CheckBox;

    .line 165
    const v32, 0x7f0b0095

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/LinearLayout;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->ll1:Landroid/widget/LinearLayout;

    .line 166
    const v32, 0x7f0b0096

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/ImageView;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv1:Landroid/widget/ImageView;

    .line 167
    const v32, 0x7f0b0097

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/ImageView;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv2:Landroid/widget/ImageView;

    .line 168
    const v32, 0x7f0b0045

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/view/ViewGroup;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    .line 171
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v25

    .line 172
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v26

    .line 174
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv1:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v25

    move/from16 v3, v33

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 176
    if-eqz v26, :cond_7

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v32

    if-nez v32, :cond_7

    .line 177
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv2:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v26

    move/from16 v3, v33

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 178
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv2:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    :cond_7
    if-eqz v28, :cond_8

    const/16 v32, 0x4

    move/from16 v0, v28

    move/from16 v1, v32

    if-ne v0, v1, :cond_f

    .line 183
    :cond_8
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->cb1:Landroid/widget/CheckBox;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 184
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv3:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x8

    invoke-virtual/range {v32 .. v33}, Landroid/widget/TextView;->setVisibility(I)V

    .line 194
    :cond_9
    :goto_7
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_a

    .line 195
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move/from16 v2, p1

    move-object/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setSubHeaderView(Landroid/view/ViewGroup;ILjava/lang/String;)V

    .line 200
    :cond_a
    :goto_8
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->cb1:Landroid/widget/CheckBox;

    move-object/from16 v32, v0

    if-eqz v32, :cond_b

    .line 201
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->cb1:Landroid/widget/CheckBox;

    move-object/from16 v32, v0

    new-instance v33, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;)V

    invoke-virtual/range {v32 .. v33}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    if-eqz v6, :cond_b

    .line 221
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv1:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move/from16 v2, v33

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setTextStrike(Landroid/widget/TextView;Z)V

    .line 222
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->cb1:Landroid/widget/CheckBox;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 226
    :cond_b
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv1:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    if-eqz v32, :cond_e

    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv2:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    if-eqz v32, :cond_e

    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->ll1:Landroid/widget/LinearLayout;

    move-object/from16 v32, v0

    if-eqz v32, :cond_e

    .line 227
    if-eqz v5, :cond_c

    .line 228
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv1:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 229
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->ll1:Landroid/widget/LinearLayout;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 231
    :cond_c
    if-eqz v22, :cond_d

    .line 232
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv2:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 233
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv2:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const v33, 0x7f020093

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 234
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->ll1:Landroid/widget/LinearLayout;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 236
    :cond_d
    const/16 v32, -0x1

    move/from16 v0, v21

    move/from16 v1, v32

    if-eq v0, v1, :cond_e

    if-eqz v21, :cond_e

    .line 237
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->ll1:Landroid/widget/LinearLayout;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 238
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv2:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 239
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv2:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 242
    :cond_e
    const v32, 0x7f0b0098

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 243
    .local v7, "colorBar":Landroid/view/View;
    invoke-virtual {v7, v4}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_3

    .line 185
    .end local v7    # "colorBar":Landroid/view/View;
    :cond_f
    const/16 v32, 0x1

    move/from16 v0, v28

    move/from16 v1, v32

    if-eq v0, v1, :cond_10

    const/16 v32, 0x2

    move/from16 v0, v28

    move/from16 v1, v32

    if-ne v0, v1, :cond_9

    .line 187
    :cond_10
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->cb1:Landroid/widget/CheckBox;

    move-object/from16 v32, v0

    const/16 v33, 0x8

    invoke-virtual/range {v32 .. v33}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 188
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv3:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv3:Landroid/widget/TextView;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    const/16 v34, 0x101

    move-object/from16 v0, v33

    move-wide/from16 v1, v18

    move/from16 v3, v34

    invoke-static {v0, v1, v2, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    move/from16 v3, v34

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    goto/16 :goto_7

    .line 198
    :cond_11
    const v32, 0x7f0b0070

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/TextView;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv1:Landroid/widget/TextView;

    goto/16 :goto_8

    .line 246
    :pswitch_1
    if-nez p2, :cond_12

    .line 247
    sget-object v32, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v33, 0x7f030047

    const/16 v34, 0x0

    const/16 v35, 0x0

    invoke-virtual/range {v32 .. v35}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 249
    new-instance v32, Landroid/view/ViewGroup$LayoutParams;

    const/16 v33, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v34

    const v35, 0x7f0a025c

    invoke-virtual/range {v34 .. v35}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    invoke-direct/range {v32 .. v34}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, p2

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 253
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v25

    .line 254
    const v32, 0x7f0b009b

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/TextView;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv1:Landroid/widget/TextView;

    .line 255
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv1:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v25

    move/from16 v3, v33

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 258
    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    move/from16 v32, v0

    const/16 v33, -0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_13

    .line 259
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mStartGridIndex:I

    .line 262
    :cond_13
    const v32, 0x7f0b004b

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/ImageView;

    move-object/from16 v0, v32

    iput-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv1:Landroid/widget/ImageView;

    .line 264
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v30

    .line 265
    .local v30, "thumb":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionInkData()Ljava/lang/String;

    move-result-object v14

    .line 267
    .local v14, "inkData":Ljava/lang/String;
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv1:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v32

    if-nez v32, :cond_4

    .line 268
    iget-object v0, v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->iv1:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const/16 v33, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v30

    move/from16 v3, v33

    invoke-virtual {v0, v1, v2, v14, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_3

    .line 283
    .end local v14    # "inkData":Ljava/lang/String;
    .end local v30    # "thumb":Ljava/lang/String;
    .restart local v17    # "mainGroup":Landroid/view/View;
    :cond_14
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-virtual {v0, v1, v15, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    goto/16 :goto_4

    .line 148
    .end local v17    # "mainGroup":Landroid/view/View;
    .end local v23    # "resultTime":Ljava/util/Date;
    .restart local v24    # "resultTime":Ljava/util/Date;
    :catchall_1
    move-exception v32

    move-object/from16 v23, v24

    .end local v24    # "resultTime":Ljava/util/Date;
    .restart local v23    # "resultTime":Ljava/util/Date;
    goto/16 :goto_6

    .line 144
    .end local v23    # "resultTime":Ljava/util/Date;
    .restart local v24    # "resultTime":Ljava/util/Date;
    :catch_2
    move-exception v9

    move-object/from16 v23, v24

    .end local v24    # "resultTime":Ljava/util/Date;
    .restart local v23    # "resultTime":Ljava/util/Date;
    goto/16 :goto_5

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getViewSubType(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 322
    const/4 v2, 0x0

    .line 324
    .local v2, "subType":I
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v0

    .line 325
    .local v0, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v3

    .line 328
    .local v3, "targetType":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 333
    :goto_0
    return v2

    .line 329
    :catch_0
    move-exception v1

    .line 330
    .local v1, "nfe":Ljava/lang/NumberFormatException;
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public hasSubTitle(I)Z
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 386
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge p1, v3, :cond_0

    .line 387
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v1

    .line 388
    .local v1, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v0

    .line 389
    .local v0, "groupName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 390
    const/4 v2, 0x1

    .line 396
    .end local v0    # "groupName":Ljava/lang/String;
    .end local v1    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_0
    return v2
.end method
