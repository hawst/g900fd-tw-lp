.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;
.super Ljava/lang/Object;
.source "GalaxyFinderActivity.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0

    .prologue
    .line 504
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChangeSelection(I)V
    .locals 9
    .param p1, "count"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 508
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSelectedItemCount:I
    invoke-static {v5, p1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1002(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;I)I

    .line 510
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1100(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/view/ActionMode;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 511
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1100(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/view/ActionMode;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ActionMode;->getCustomView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0b001c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 513
    .local v2, "title":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const v6, 0x7f0e006a

    new-array v7, v4, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSelectedItemCount:I
    invoke-static {v8}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 515
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1100(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/view/ActionMode;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v1

    .line 516
    .local v1, "menu":Landroid/view/Menu;
    const v5, 0x7f0b00e5

    invoke-interface {v1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 518
    .local v0, "item":Landroid/view/MenuItem;
    if-nez p1, :cond_1

    :goto_0
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 520
    .end local v0    # "item":Landroid/view/MenuItem;
    .end local v1    # "menu":Landroid/view/Menu;
    .end local v2    # "title":Landroid/widget/TextView;
    :cond_0
    return-void

    .restart local v0    # "item":Landroid/view/MenuItem;
    .restart local v1    # "menu":Landroid/view/Menu;
    .restart local v2    # "title":Landroid/widget/TextView;
    :cond_1
    move v3, v4

    .line 518
    goto :goto_0
.end method

.method public onChangeSelectionModeState(Z)V
    .locals 1
    .param p1, "activated"    # Z

    .prologue
    .line 524
    if-eqz p1, :cond_0

    .line 525
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->onMenuSelect()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1200(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    .line 527
    :cond_0
    return-void
.end method
