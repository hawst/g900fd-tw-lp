.class public final Lcom/samsung/android/app/galaxyfinder/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final actionbar_custom_dropdown_layout:I = 0x7f030000

.field public static final activity_nuance_voice_policy:I = 0x7f030001

.field public static final activity_voice_input:I = 0x7f030002

.field public static final category_body_layout:I = 0x7f030003

.field public static final category_footer_layout:I = 0x7f030004

.field public static final category_header_layout:I = 0x7f030005

.field public static final custom_actionbar_layout:I = 0x7f030006

.field public static final custom_search_layout:I = 0x7f030007

.field public static final filter_item_layout:I = 0x7f030008

.field public static final filter_panel_detail_hint_layout:I = 0x7f030009

.field public static final filter_panel_layout:I = 0x7f03000a

.field public static final filter_selected_item_layout:I = 0x7f03000b

.field public static final group_item_tagcloud_expandable:I = 0x7f03000c

.field public static final help_popup_start_up_layout:I = 0x7f03000d

.field public static final help_popup_try_it_guide_1:I = 0x7f03000e

.field public static final help_popup_try_it_guide_2:I = 0x7f03000f

.field public static final help_popup_try_it_guide_4:I = 0x7f030010

.field public static final help_popup_try_it_guide_5:I = 0x7f030011

.field public static final help_popup_try_it_guide_none:I = 0x7f030012

.field public static final history_delete_all:I = 0x7f030013

.field public static final history_list_item_fullscreen_layout:I = 0x7f030014

.field public static final history_list_item_layout:I = 0x7f030015

.field public static final item_subtitle_layout:I = 0x7f030016

.field public static final layout_dropdown_simple_list_item:I = 0x7f030017

.field public static final layout_template_application:I = 0x7f030018

.field public static final layout_template_browser:I = 0x7f030019

.field public static final layout_template_chaton:I = 0x7f03001a

.field public static final layout_template_contact:I = 0x7f03001b

.field public static final layout_template_default:I = 0x7f03001c

.field public static final layout_template_email:I = 0x7f03001d

.field public static final layout_template_gallery_grid:I = 0x7f03001e

.field public static final layout_template_lifetimes:I = 0x7f03001f

.field public static final layout_template_memo:I = 0x7f030020

.field public static final layout_template_message:I = 0x7f030021

.field public static final layout_template_music:I = 0x7f030022

.field public static final layout_template_myfiles:I = 0x7f030023

.field public static final layout_template_note:I = 0x7f030024

.field public static final layout_template_phone:I = 0x7f030025

.field public static final layout_template_pinall:I = 0x7f030026

.field public static final layout_template_planner:I = 0x7f030027

.field public static final layout_template_quickmemo:I = 0x7f030028

.field public static final layout_template_samsung_link:I = 0x7f030029

.field public static final layout_template_settings:I = 0x7f03002a

.field public static final layout_template_story_album:I = 0x7f03002b

.field public static final layout_template_video:I = 0x7f03002c

.field public static final layout_template_vnote:I = 0x7f03002d

.field public static final layout_template_weblink:I = 0x7f03002e

.field public static final layout_template_webpreview:I = 0x7f03002f

.field public static final list_item_section:I = 0x7f030030

.field public static final list_item_template_application:I = 0x7f030031

.field public static final list_item_template_browser:I = 0x7f030032

.field public static final list_item_template_browser_preview:I = 0x7f030033

.field public static final list_item_template_chaton:I = 0x7f030034

.field public static final list_item_template_chaton_send:I = 0x7f030035

.field public static final list_item_template_contact:I = 0x7f030036

.field public static final list_item_template_default:I = 0x7f030037

.field public static final list_item_template_email:I = 0x7f030038

.field public static final list_item_template_gallery_grid:I = 0x7f030039

.field public static final list_item_template_lifetimes:I = 0x7f03003a

.field public static final list_item_template_memo:I = 0x7f03003b

.field public static final list_item_template_message_receive:I = 0x7f03003c

.field public static final list_item_template_message_send:I = 0x7f03003d

.field public static final list_item_template_message_send_failed:I = 0x7f03003e

.field public static final list_item_template_message_thread:I = 0x7f03003f

.field public static final list_item_template_music:I = 0x7f030040

.field public static final list_item_template_myfiles:I = 0x7f030041

.field public static final list_item_template_note:I = 0x7f030042

.field public static final list_item_template_phone:I = 0x7f030043

.field public static final list_item_template_pinall:I = 0x7f030044

.field public static final list_item_template_planner:I = 0x7f030045

.field public static final list_item_template_planner_event:I = 0x7f030046

.field public static final list_item_template_planner_ink:I = 0x7f030047

.field public static final list_item_template_quick_memo:I = 0x7f030048

.field public static final list_item_template_samsung_link:I = 0x7f030049

.field public static final list_item_template_settings:I = 0x7f03004a

.field public static final list_item_template_story_album:I = 0x7f03004b

.field public static final list_item_template_video:I = 0x7f03004c

.field public static final list_item_template_vnote:I = 0x7f03004d

.field public static final list_item_template_weblink:I = 0x7f03004e

.field public static final list_item_template_webpreview:I = 0x7f03004f

.field public static final multi_select_mode_preference:I = 0x7f030050

.field public static final nuance_voice_data_connection_dialog_layout:I = 0x7f030051

.field public static final nuance_voice_policy_dialog_layout:I = 0x7f030052

.field public static final popup_list_share_via_item:I = 0x7f030053

.field public static final preference_widget_twcheckbox:I = 0x7f030054

.field public static final root_layout:I = 0x7f030055

.field public static final round_more_icon:I = 0x7f030056

.field public static final search_list_layout:I = 0x7f030057

.field public static final search_result_item_divider:I = 0x7f030058

.field public static final settings_category_list:I = 0x7f030059

.field public static final settings_category_list_item:I = 0x7f03005a

.field public static final settings_category_list_selectall_item:I = 0x7f03005b

.field public static final settings_delete_history_list_item:I = 0x7f03005c

.field public static final settings_list_common_selectall:I = 0x7f03005d

.field public static final settings_main_layout:I = 0x7f03005e

.field public static final settings_select_filter_listitem_layout:I = 0x7f03005f

.field public static final symbol_help_layout:I = 0x7f030060

.field public static final tag_scrollview_horizontal_style:I = 0x7f030061

.field public static final tag_scrollview_vertical_style:I = 0x7f030062

.field public static final tagcloud_layout:I = 0x7f030063


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
