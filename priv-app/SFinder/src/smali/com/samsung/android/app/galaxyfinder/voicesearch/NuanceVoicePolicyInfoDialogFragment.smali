.class public Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;
.super Landroid/app/DialogFragment;
.source "NuanceVoicePolicyInfoDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;
    }
.end annotation


# instance fields
.field private mCallbacks:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->mCallbacks:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;)Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->mCallbacks:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 35
    instance-of v0, p1, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 36
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->mCallbacks:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;

    .line 38
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 39
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x28

    .line 44
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 45
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f030052

    invoke-virtual {v7, v8, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 47
    .local v2, "layout":Landroid/view/View;
    const v7, 0x7f0b00b2

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 48
    .local v1, "checkbox_layout":Landroid/view/View;
    const v7, 0x7f0b00b1

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 50
    .local v6, "warningText":Landroid/widget/TextView;
    const v7, 0x7f0e0058

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 51
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 52
    const/16 v7, 0x8

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 54
    const v7, 0x7f0e0059

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 55
    .local v5, "nuancevoice_msg":Ljava/lang/String;
    const v7, 0x7f0e0080

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 57
    .local v3, "nuancevoice_link1":Ljava/lang/String;
    const v7, 0x7f0e0084

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 59
    .local v4, "nuancevoice_link3":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<a href=\"sherif-activity://nuancevoiceinfo\">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "</a>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v3, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 61
    const-string v7, "ro.csc.country_code"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "KOREA"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ko_KR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 69
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<a href=http://www.vlingo.com/wap/samsung-asr-privacy-addendum/en>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "</a>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v4, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 72
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 74
    const/4 v7, 0x0

    invoke-virtual {v6, v9, v7, v9, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 76
    const v7, 0x7f0e0006

    new-instance v8, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$1;

    invoke-direct {v8, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$1;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 93
    const v7, 0x7f0e002c

    invoke-virtual {v0, v7, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 94
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    return-object v7
.end method
