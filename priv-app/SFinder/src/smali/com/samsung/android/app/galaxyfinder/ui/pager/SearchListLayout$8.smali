.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComputeScroll(II)V
    .locals 2
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I

    .prologue
    .line 481
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->updatePossibilityTopFlicking()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    .line 483
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 484
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I
    invoke-static {v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1602(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;I)I

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    const/16 v0, 0x6e

    if-ge p2, v0, :cond_2

    .line 489
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I
    invoke-static {v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1602(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;I)I

    goto :goto_0

    .line 491
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1600(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)I

    move-result v0

    sub-int/2addr v0, p2

    const/16 v1, 0x28

    if-le v0, v1, :cond_3

    .line 492
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I
    invoke-static {v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1602(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;I)I

    goto :goto_0

    .line 493
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1600(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)I

    move-result v0

    sub-int/2addr v0, p2

    const/16 v1, -0x28

    if-ge v0, v1, :cond_0

    .line 494
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I
    invoke-static {v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1602(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;I)I

    goto :goto_0
.end method
