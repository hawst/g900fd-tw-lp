.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;
.super Ljava/lang/Object;
.source "GalaxyFinderCategoryActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "selectedItem"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "itemId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "listView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-nez p3, :cond_2

    .line 139
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectedCount:I
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectableCategorySize:I
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$100(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)I

    move-result v2

    if-eq v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->setCheckedAll(Z)V
    invoke-static {v1, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$200(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Z)V

    .line 144
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getSelectedCount()I
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$400(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)I

    move-result v1

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectedCount:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$002(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;I)I

    .line 145
    return-void

    .line 139
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 140
    :cond_2
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-ltz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->performListItemClick(J)V
    invoke-static {v0, p4, p5}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$300(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;J)V

    goto :goto_1
.end method
