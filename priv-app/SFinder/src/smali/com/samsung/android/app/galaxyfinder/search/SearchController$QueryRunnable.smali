.class public Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/search/SearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "QueryRunnable"
.end annotation


# instance fields
.field private engines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;",
            ">;"
        }
    .end annotation
.end field

.field private qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Ljava/util/List;Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)V
    .locals 1
    .param p3, "qa"    # Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;",
            ">;",
            "Lcom/samsung/android/app/galaxyfinder/search/QueryAction;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "e":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;>;"
    const/4 v0, 0x0

    .line 322
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    .line 320
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->engines:Ljava/util/List;

    .line 323
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    .line 324
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->engines:Ljava/util/List;

    .line 325
    return-void
.end method


# virtual methods
.method public cancel()Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    .locals 3

    .prologue
    .line 376
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->engines:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 377
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->engines:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;

    .line 378
    .local v0, "engine":Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->cancel()V

    goto :goto_0

    .line 382
    .end local v0    # "engine":Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    return-object v2
.end method

.method public run()V
    .locals 6

    .prologue
    .line 329
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->setCurrentRunnable(Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;)V
    invoke-static {v3, p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$900(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;)V

    .line 331
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    if-eqz v3, :cond_4

    .line 332
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "run. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->engines:Ljava/util/List;

    if-eqz v3, :cond_5

    .line 335
    const/4 v2, 0x0

    .line 337
    .local v2, "pendingAction":I
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$1000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertKeywordLog(Landroid/content/Context;Ljava/lang/String;I)V

    .line 338
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$1000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "SRCH"

    const/4 v5, 0x3

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertKeywordLog(Landroid/content/Context;Ljava/lang/String;I)V

    .line 341
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->engines:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;

    .line 342
    .local v0, "engine":Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;
    if-eqz v0, :cond_0

    .line 343
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->setListener(Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;)V

    .line 345
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->prepare(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)I

    move-result v3

    add-int/2addr v2, v3

    .line 347
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$400(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 348
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$400(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v2, v3

    .line 351
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$400(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pending action count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->start()V

    goto :goto_0

    .line 359
    .end local v0    # "engine":Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;
    :cond_2
    if-gtz v2, :cond_4

    .line 360
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$400(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$800(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 364
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->qaction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->startWebSearchQuery(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)V
    invoke-static {v3, v4}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$1100(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)V

    .line 367
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ignored action"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "pendingAction":I
    :cond_4
    :goto_1
    return-void

    .line 370
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "no available search engines"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
