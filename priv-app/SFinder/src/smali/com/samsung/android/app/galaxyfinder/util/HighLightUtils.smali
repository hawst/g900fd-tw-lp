.class public Lcom/samsung/android/app/galaxyfinder/util/HighLightUtils;
.super Ljava/lang/Object;
.source "HighLightUtils.java"


# static fields
.field private static final DELIMETER_ATTRIBUTES:Ljava/lang/String; = "[,]"

.field private static final DELIMETER_RECTS:Ljava/lang/String; = "\\|"

.field private static final RECT_ATTRIBUTE_COUNT:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertStringToRects(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p0, "rects"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 18
    const-string v10, "\\|"

    invoke-virtual {p0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 20
    .local v5, "rectArray":[Ljava/lang/String;
    array-length v10, v5

    if-gtz v10, :cond_1

    move-object v7, v8

    .line 52
    :cond_0
    :goto_0
    return-object v7

    .line 24
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 26
    .local v7, "tempRectFList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v10, v5

    if-ge v0, v10, :cond_8

    .line 27
    aget-object v10, v5, v0

    const-string v11, "[,]"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 29
    .local v6, "rectAttribute":[Ljava/lang/String;
    array-length v10, v6

    const/4 v11, 0x4

    if-ne v10, v11, :cond_6

    .line 31
    aget-object v10, v6, v9

    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v10

    float-to-int v2, v10

    .line 32
    .local v2, "nLeft":I
    const/4 v10, 0x1

    aget-object v10, v6, v10

    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v10

    float-to-int v4, v10

    .line 33
    .local v4, "nTop":I
    const/4 v10, 0x2

    aget-object v10, v6, v10

    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v10

    float-to-int v3, v10

    .line 34
    .local v3, "nRight":I
    const/4 v10, 0x3

    aget-object v10, v6, v10

    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v10

    float-to-int v1, v10

    .line 36
    .local v1, "nBottom":I
    if-gez v2, :cond_2

    move v2, v9

    .line 37
    :cond_2
    if-gez v4, :cond_3

    move v4, v9

    .line 38
    :cond_3
    if-gez v3, :cond_4

    move v3, v9

    .line 39
    :cond_4
    if-gez v1, :cond_5

    move v1, v9

    .line 41
    :cond_5
    if-nez v2, :cond_7

    if-nez v4, :cond_7

    if-nez v3, :cond_7

    if-nez v1, :cond_7

    .line 26
    .end local v1    # "nBottom":I
    .end local v2    # "nLeft":I
    .end local v3    # "nRight":I
    .end local v4    # "nTop":I
    :cond_6
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 45
    .restart local v1    # "nBottom":I
    .restart local v2    # "nLeft":I
    .restart local v3    # "nRight":I
    .restart local v4    # "nTop":I
    :cond_7
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10, v2, v4, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 49
    .end local v1    # "nBottom":I
    .end local v2    # "nLeft":I
    .end local v3    # "nRight":I
    .end local v4    # "nTop":I
    .end local v6    # "rectAttribute":[Ljava/lang/String;
    :cond_8
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-gtz v9, :cond_0

    move-object v7, v8

    .line 52
    goto :goto_0
.end method
