.class Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$1;
.super Ljava/lang/Object;
.source "NuanceVoicePolicyInfoDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$1;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$1;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "show_policy_info"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->setSettings(Landroid/content/Context;Ljava/lang/String;I)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$1;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$1;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->dismissAllowingStateLoss()V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$1;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->mCallbacks:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->access$000(Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;)Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$1;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->mCallbacks:Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->access$000(Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;)Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment$Callbacks;->startNuanceVoiceRecognition()V

    .line 91
    :cond_1
    return-void
.end method
