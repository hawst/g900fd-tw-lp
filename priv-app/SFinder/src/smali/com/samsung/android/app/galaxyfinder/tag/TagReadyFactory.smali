.class public Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;
.super Ljava/lang/Object;
.source "TagReadyFactory.java"


# static fields
.field private static volatile instance:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;


# instance fields
.field private mContext:Landroid/content/Context;

.field private tagReadyMgrTable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/tag/TagReady;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->instance:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->tagReadyMgrTable:Ljava/util/Map;

    .line 12
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->mContext:Landroid/content/Context;

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->tagReadyMgrTable:Ljava/util/Map;

    .line 18
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->mContext:Landroid/content/Context;

    .line 19
    return-void
.end method

.method private createTagReady(Landroid/app/SearchableInfo;)Lcom/samsung/android/app/galaxyfinder/tag/TagReady;
    .locals 2
    .param p1, "searchableInfo"    # Landroid/app/SearchableInfo;

    .prologue
    .line 44
    const/4 v0, 0x0

    .line 45
    .local v0, "tagReady":Lcom/samsung/android/app/galaxyfinder/tag/TagReady;
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    .end local v0    # "tagReady":Lcom/samsung/android/app/galaxyfinder/tag/TagReady;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;-><init>(Landroid/content/Context;Landroid/app/SearchableInfo;)V

    .line 46
    .restart local v0    # "tagReady":Lcom/samsung/android/app/galaxyfinder/tag/TagReady;
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->instance:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;

    if-nez v0, :cond_0

    .line 23
    const-class v1, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;

    monitor-enter v1

    .line 24
    :try_start_0
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->instance:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;

    .line 25
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->instance:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getTagReady(Landroid/app/SearchableInfo;)Lcom/samsung/android/app/galaxyfinder/tag/TagReady;
    .locals 5
    .param p1, "searchableInfo"    # Landroid/app/SearchableInfo;

    .prologue
    .line 31
    const/4 v2, 0x0

    .line 32
    .local v2, "tagReady":Lcom/samsung/android/app/galaxyfinder/tag/TagReady;
    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->tagReadyMgrTable:Ljava/util/Map;

    monitor-enter v4

    .line 34
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->tagReadyMgrTable:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    move-object v2, v0

    .line 35
    if-nez v2, :cond_0

    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->createTagReady(Landroid/app/SearchableInfo;)Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    move-result-object v2

    .line 37
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->tagReadyMgrTable:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    :cond_0
    monitor-exit v4

    .line 40
    return-object v2

    .line 39
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method
