.class public Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
.super Ljava/lang/Object;
.source "TagData.java"


# static fields
.field private static final PROFILE_DELIMITER:Ljava/lang/String; = "/"

.field private static final PROFILE_PREFIX:Ljava/lang/String; = "profile"

.field public static final SEARCH_FILTER_HANDWRITING:Ljava/lang/String; = "handwriting"

.field public static final TAG_TYPE_CATEGORY:I = 0x5

.field public static final TAG_TYPE_FACE:I = 0x1

.field public static final TAG_TYPE_FILTER:I = 0x6

.field public static final TAG_TYPE_FLAG_CATEGORY:I = 0x20

.field public static final TAG_TYPE_FLAG_FACE:I = 0x2

.field public static final TAG_TYPE_FLAG_FILTER:I = 0x40

.field public static final TAG_TYPE_FLAG_LOCATION:I = 0x4

.field public static final TAG_TYPE_FLAG_TIME:I = 0x10

.field public static final TAG_TYPE_FLAG_USERDEF:I = 0x8

.field public static final TAG_TYPE_FLAG_WEATHER:I = 0x1

.field public static final TAG_TYPE_LOCATION:I = 0x0

.field public static final TAG_TYPE_TIME:I = 0x4

.field public static final TAG_TYPE_USERDEF:I = 0x3

.field public static final TAG_TYPE_WEATHER:I = 0x2

.field public static tagComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsEnabled:Z

.field private mIsSelected:Z

.field private mRawTagData:Ljava/lang/String;

.field private mSearchFilter:Ljava/lang/String;

.field private mSupportTimeTag:Z

.field private mTag:Ljava/lang/Object;

.field private mTagCount:I

.field private mTagTypeFlags:I

.field private mTextTagData:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 271
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData$1;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData$1;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->tagComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "tagData"    # Ljava/lang/String;
    .param p2, "tagType"    # Ljava/lang/String;
    .param p3, "rawTagData"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagCount:I

    .line 50
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    .line 52
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTextTagData:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mRawTagData:Ljava/lang/String;

    .line 56
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mSearchFilter:Ljava/lang/String;

    .line 58
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mIsSelected:Z

    .line 60
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mIsEnabled:Z

    .line 62
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mSupportTimeTag:Z

    .line 64
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTag:Ljava/lang/Object;

    .line 66
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mContext:Landroid/content/Context;

    .line 69
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mContext:Landroid/content/Context;

    .line 71
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->accumulateTagType(Ljava/lang/String;)V

    .line 73
    invoke-direct {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->initTagData(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mRawTagData:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 76
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mIsEnabled:Z

    .line 79
    :cond_0
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagCount:I

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "tagData"    # Ljava/lang/String;
    .param p2, "tagType"    # Ljava/lang/String;
    .param p3, "rawTagData"    # Ljava/lang/String;
    .param p4, "count"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :try_start_0
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 87
    .local v1, "tagCount":I
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->addTagCount(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .end local v1    # "tagCount":I
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method private incTagCount()V
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagCount:I

    .line 206
    return-void
.end method

.method private initTagData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "textData"    # Ljava/lang/String;
    .param p2, "rawData"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isFaceTag()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 95
    const-string v0, "profile/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTextTagData:Ljava/lang/String;

    .line 97
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mRawTagData:Ljava/lang/String;

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTextTagData:Ljava/lang/String;

    .line 103
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mRawTagData:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public accumulateTagData(Ljava/lang/String;)V
    .locals 0
    .param p1, "tagType"    # Ljava/lang/String;

    .prologue
    .line 229
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->accumulateTagType(Ljava/lang/String;)V

    .line 230
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->incTagCount()V

    .line 231
    return-void
.end method

.method public accumulateTagType(Ljava/lang/String;)V
    .locals 3
    .param p1, "tagType"    # Ljava/lang/String;

    .prologue
    .line 140
    if-eqz p1, :cond_0

    .line 142
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 143
    .local v1, "tagTypeIndex":I
    packed-switch v1, :pswitch_data_0

    .line 186
    .end local v1    # "tagTypeIndex":I
    :cond_0
    :goto_0
    return-void

    .line 146
    .restart local v1    # "tagTypeIndex":I
    :pswitch_0
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 182
    .end local v1    # "tagTypeIndex":I
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0

    .line 151
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v1    # "tagTypeIndex":I
    :pswitch_1
    :try_start_1
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    goto :goto_0

    .line 156
    :pswitch_2
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    goto :goto_0

    .line 161
    :pswitch_3
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    goto :goto_0

    .line 166
    :pswitch_4
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    goto :goto_0

    .line 171
    :pswitch_5
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    goto :goto_0

    .line 176
    :pswitch_6
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 143
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public addTagCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 225
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagCount:I

    .line 226
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 261
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 263
    .local v0, "target":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagTypeFlags()I

    move-result v1

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagTypeFlags()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 265
    const/4 v1, 0x1

    .line 268
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRawTagData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mRawTagData:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchFilter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mSearchFilter:Ljava/lang/String;

    return-object v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTag:Ljava/lang/Object;

    return-object v0
.end method

.method public getTagCount()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagCount:I

    return v0
.end method

.method public getTagTypeFlags()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    return v0
.end method

.method public getTextTagData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTextTagData:Ljava/lang/String;

    return-object v0
.end method

.method public isCategoryTag()Z
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x20

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mIsEnabled:Z

    return v0
.end method

.method public isFaceTag()Z
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFilterTag()Z
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x40

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLocationTag()Z
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mIsSelected:Z

    return v0
.end method

.method public isTimeTag()Z
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x10

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUserTag()Z
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x8

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x2

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "isEnabled"    # Z

    .prologue
    .line 221
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mIsEnabled:Z

    .line 222
    return-void
.end method

.method public setSearchFilter(Ljava/lang/String;)V
    .locals 0
    .param p1, "queryFilter"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mSearchFilter:Ljava/lang/String;

    .line 133
    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "isSelected"    # Z

    .prologue
    .line 213
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mIsSelected:Z

    .line 214
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTag:Ljava/lang/Object;

    .line 108
    return-void
.end method

.method public setTagDatas(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tagData"    # Ljava/lang/String;
    .param p2, "rawTagData"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mTextTagData:Ljava/lang/String;

    .line 116
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mRawTagData:Ljava/lang/String;

    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mRawTagData:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mIsEnabled:Z

    .line 121
    :cond_0
    return-void
.end method

.method public setTimeTagSupport(Z)V
    .locals 0
    .param p1, "support"    # Z

    .prologue
    .line 124
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mSupportTimeTag:Z

    .line 125
    return-void
.end method

.method public supportTimeTag()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->mSupportTimeTag:Z

    return v0
.end method
