.class public interface abstract Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$FilterGroupInfo;
.super Ljava/lang/Object;
.source "FilterPanelMainView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FilterGroupInfo"
.end annotation


# static fields
.field public static final FILTER_GROUP_CATEGORY:Ljava/lang/String; = "filter_group_category"

.field public static final FILTER_GROUP_CATEGORY_FILTERS:Ljava/lang/String; = "filter_group_category_filters"

.field public static final FILTER_GROUP_TIME:Ljava/lang/String; = "filter_group_time"

.field public static final FILTER_GROUP_USER:Ljava/lang/String; = "filter_group_user"
