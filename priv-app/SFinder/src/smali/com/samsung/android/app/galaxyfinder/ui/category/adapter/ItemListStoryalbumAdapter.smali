.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListStoryalbumAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 23
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 27
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v4

    .line 28
    .local v4, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v2, 0x0

    .line 30
    .local v2, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;
    invoke-virtual {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 32
    if-nez p2, :cond_0

    .line 33
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f03004b

    invoke-virtual {v6, v7, v9, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 36
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;

    .end local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;
    invoke-direct {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;-><init>()V

    .line 38
    .restart local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;
    const v6, 0x7f0b004b

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    .line 40
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 42
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v5

    .line 43
    .local v5, "title":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v1

    .line 45
    .local v1, "date":Ljava/lang/String;
    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    aput-object v5, v6, v8

    const/4 v7, 0x1

    const-string v8, ", "

    aput-object v8, v6, v7

    const/4 v7, 0x2

    aput-object v1, v6, v7

    invoke-static {p2, v6}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setViewDesc(Landroid/view/View;[Ljava/lang/String;)V

    .line 50
    .end local v1    # "date":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    :goto_0
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "album":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v3

    .line 54
    .local v3, "intent":Landroid/content/Intent;
    iget-object v6, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    const/4 v7, -0x1

    invoke-virtual {p0, v6, v0, v9, v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 56
    invoke-virtual {p0, p2, v3, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 58
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    return-object v6

    .line 47
    .end local v0    # "album":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;
    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;

    .restart local v2    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListStoryalbumAdapter$StoryalbumItemListViewHolder;
    goto :goto_0
.end method
