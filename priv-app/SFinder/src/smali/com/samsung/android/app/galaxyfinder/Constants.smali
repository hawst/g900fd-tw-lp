.class public interface abstract Lcom/samsung/android/app/galaxyfinder/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/Constants$HistoryColumns;,
        Lcom/samsung/android/app/galaxyfinder/Constants$Logging;,
        Lcom/samsung/android/app/galaxyfinder/Constants$Wolfram;,
        Lcom/samsung/android/app/galaxyfinder/Constants$TagType;,
        Lcom/samsung/android/app/galaxyfinder/Constants$TagSupportType;,
        Lcom/samsung/android/app/galaxyfinder/Constants$CategoryType;,
        Lcom/samsung/android/app/galaxyfinder/Constants$Providers;,
        Lcom/samsung/android/app/galaxyfinder/Constants$Package;
    }
.end annotation


# static fields
.field public static final ACTION_ITEM_STATUS_CHANGED:Ljava/lang/String; = "android.intent.action.FINDO_ITEM_STATUS_CHANGED"

.field public static final CATEGORY_SETTINGS_APP:Ljava/lang/String; = "com.android.settings"

.field public static final CATEGORY_SETTINGS_PROVIDER:Ljava/lang/String; = "com.sec.providers.settingsearch"

.field public static final CATEGORY_UPDATE_BY_KEYPAD:I = 0x12e

.field public static final CATEGORY_UPDATE_BY_ROTATE:I = 0x12d

.field public static final CATEGORY_UPDATE_BY_SCROLL:I = 0x12f

.field public static final CATEGORY_VIEW_MODE_COLLAPSE:I = 0x9a

.field public static final CATEGORY_VIEW_MODE_EXPAND:I = 0x9b

.field public static final CATEGORY_VIEW_MODE_LOCK:I = 0x99

.field public static final CATEGORY_VIEW_MODE_NORMAL:I = 0x97

.field public static final CATEGORY_VIEW_MODE_SELECTION:I = 0x98

.field public static final CLOSE_WRITING_BUDDY:Ljava/lang/String; = "CLOSED"

.field public static final C_CHINA:Z

.field public static final DATA_TYPE_ADDRESS:I = 0x4

.field public static final DATA_TYPE_DATE:I = 0x3

.field public static final DATA_TYPE_EMAIL:I = 0x1

.field public static final DATA_TYPE_PHONE:I = 0x0

.field public static final DATA_TYPE_TIME:I = 0x5

.field public static final DATA_TYPE_UNKNOWN:I = -0x1

.field public static final DATA_TYPE_URL:I = 0x2

.field public static final EXTERNAL_STORAGE_DIRECTORY:Ljava/lang/String; = "/storage/extSdCard"

.field public static final EXTRA_ADVANCED_SEARCH_TYPE_TAG:Ljava/lang/String; = "tagSearch"

.field public static final EXTRA_QUERY_FROM:Ljava/lang/String; = "extra_query_from"

.field public static final EXTRA_QUERY_KEY:Ljava/lang/String; = "extra_query_key"

.field public static final EXTRA_SEARCH_FILTERS:Ljava/lang/String; = "extra_search_filters"

.field public static final EXTRA_TAGS:Ljava/lang/String; = "extra_tags"

.field public static final EXTRA_TARGETS:Ljava/lang/String; = "extra_targets"

.field public static final EXTRA_TIME_SPAN:Ljava/lang/String; = "extra_time_span"

.field public static final HISTORY_PROJECTION:[Ljava/lang/String;

.field public static final IMG_EFFECT_CROP_TOP:I = 0xcb

.field public static final IMG_EFFECT_NONE:I = 0xc9

.field public static final IMG_EFFECT_SHADOW:I = 0xca

.field public static final INTERNAL_SYSTEM_STORAGE_DIRECTORY:Ljava/lang/String;

.field public static final ITEM_COMPONENT:Ljava/lang/String; = "component"

.field public static final ITEM_ID:Ljava/lang/String; = "id"

.field public static final ITEM_VALUE:Ljava/lang/String; = "value"

.field public static final KEYWORD_TYPE:I = 0x80

.field public static final MAX_SELECT_LIMIT_COUNT:I = 0x64

.field public static final MODE_SELECTION_SHARE_VIA:I = 0x1

.field public static final MODE_SELECTION_UNKNOWN:I = -0x1

.field public static final MSG_INPUT_TIMER_EXPIRED:I = 0x67

.field public static final MSG_MODE_CHANGE:I = 0x65

.field public static final MSG_SCROLL_TO_SELECTED_GROUP:I = 0x66

.field public static final MSG_SEARCH_CLEAR:I = 0x3

.field public static final MSG_SEARCH_CLEAR_BY_BACKKEY:I = 0x4

.field public static final MSG_SEARCH_COMPLETED:I = 0x1

.field public static final MSG_SEARCH_INPUT_EMPTY:I = 0x9

.field public static final MSG_SEARCH_REQUEST:I = 0x2

.field public static final MSG_SEARCH_REQUEST_BY_VR:I = 0x5

.field public static final MSG_SEARCH_REQUEST_RELOAD:I = 0x6

.field public static final MSG_SEARCH_RESPONSE:I = 0x0

.field public static final MSG_SEARCH_RESPONSE_RELOAD:I = 0x7

.field public static final MSG_SEARCH_RESPONSE_TAG_RETRIEVED:I = 0x8

.field public static final OPEN_WRITING_BUDDY:Ljava/lang/String; = "OPENED"

.field public static final PACKAGE_ADDED:Ljava/lang/String; = "android.intent.action.PACKAGE_ADDED"

.field public static final PACKAGE_CHANGED:Ljava/lang/String; = "android.intent.action.PACKAGE_CHANGED"

.field public static final PACKAGE_REMOVED:Ljava/lang/String; = "android.intent.action.PACKAGE_REMOVED"

.field public static final PACKAGE_REPLACED:Ljava/lang/String; = "android.intent.action.PACKAGE_REPLACED"

.field public static final PACKAGE_SCHEME:Ljava/lang/String; = "package"

.field public static final PREF_MEDIASCANNER_READY_ONCE:Ljava/lang/String; = "pref_media_scanner_ready_once"

.field public static final PREF_TAGSERVICE_FULL_SYNC_AFTER_BOOT:Ljava/lang/String; = "pref_tagservice_fullsync_boot"

.field public static final PREF_TAGSERVICE_STATE:Ljava/lang/String; = "pref_tagservice_state"

.field public static final PREF_TAG_SERVICE_FULL_SYNC_READY:I = 0x1

.field public static final PREF_TAG_SERVICE_FULL_SYNC_WORKED:I = 0x2

.field public static final PREF_TAG_SERVICE_MEDIA_SCANNER_INIT:I = 0x0

.field public static final PREF_TAG_SERVICE_MEDIA_SCANNER_READY_ONCE:I = 0x1

.field public static final QUERY_FROM_HISTORY:Ljava/lang/String; = "query_from_history"

.field public static final QUERY_FROM_RELOAD:Ljava/lang/String; = "query_from_reload"

.field public static final QUERY_FROM_REQUERY:Ljava/lang/String; = "query_from_requery"

.field public static final QUERY_FROM_TAG:Ljava/lang/String; = "query_from_tag"

.field public static final QUERY_FROM_TAG_CLOUD:Ljava/lang/String; = "query_from_tag_cloud"

.field public static final QUERY_FROM_USER:Ljava/lang/String; = "query_from_user"

.field public static final QUERY_FROM_VR:Ljava/lang/String; = "query_from_vr"

.field public static final QUICKSEARCH_CLSNAME:Ljava/lang/String; = "com.google.android.search.core.google.GoogleSearch"

.field public static final QUICKSEARCH_PKGNAME:Ljava/lang/String; = "com.google.android.googlequicksearchbox"

.field public static final RELATED_MASK:I = 0x10

.field public static final SAMSUNG_LINK_LOCATION_SERVICE_ENABLE_VERSION:I = 0x7153

.field public static final SETTINGS_SEARCH_CATEGORY:Ljava/lang/String; = "setting_search_category"

.field public static final SETTINGS_SEARCH_CATEGORY_WEBSEARCH_KEY:Ljava/lang/String; = "websearch"

.field public static final SETTINGS_USE_SEARCH_HISTORY:Ljava/lang/String; = "setting_use_search_history"

.field public static final SHARE_MODE_SEND_TO_SNOTE:I = 0xca

.field public static final SHARE_MODE_SEND_TO_STORYALBUM:I = 0xc9

.field public static final SHARE_MODE_SHARE_VIA:I = 0xcb

.field public static final SPR_CODE:Z

.field public static final SUPPORT_INK_HIGHLIGHT:Z = true

.field public static final TAG_CLOUD_FLAG_CATEGORY:I = 0x2

.field public static final TAG_CLOUD_FLAG_LOCATION:I = 0x8

.field public static final TAG_CLOUD_FLAG_TIME:I = 0x1

.field public static final TAG_CLOUD_FLAG_USER_DEF:I = 0x4

.field public static final TAG_CLOUD_TYPE_CATEGORY:I = 0x2

.field public static final TAG_CLOUD_TYPE_NONE:I = 0x0

.field public static final TAG_CLOUD_TYPE_TIME:I = 0x1

.field public static final TAG_CLOUD_TYPE_USER:I = 0x4

.field public static final TAG_DELIMETER_SUB:Ljava/lang/String; = ","

.field public static final TAG_QPARAM_ENDID:Ljava/lang/String; = "endid"

.field public static final TAG_QPARAM_STARTID:Ljava/lang/String; = "startid"

.field public static final TAG_SERVICE_BOOTUP:I = 0x1

.field public static final TAG_SERVICE_INIT:I = 0x0

.field public static final TASK_TYPE_ADDRESS:I = 0x8

.field public static final TASK_TYPE_ADD_BOOKMARK:I = 0x5

.field public static final TASK_TYPE_ADD_CONTACT:I = 0x1

.field public static final TASK_TYPE_ALARM:I = 0x6

.field public static final TASK_TYPE_CALL:I = 0x0

.field public static final TASK_TYPE_EMAIL:I = 0x3

.field public static final TASK_TYPE_OPEN_URL:I = 0x4

.field public static final TASK_TYPE_SCHEDULE:I = 0x7

.field public static final TASK_TYPE_SCHEDULE_VIEW:I = 0x9

.field public static final TASK_TYPE_SEND_MESSAGE:I = 0x2

.field public static final TASK_TYPE_UNKNOWN:I = -0x1

.field public static final TEMPLATE_TYPE_APPLICATION:Ljava/lang/String; = "suggest_template_application"

.field public static final TEMPLATE_TYPE_BROWSER:Ljava/lang/String; = "suggest_template_history_web"

.field public static final TEMPLATE_TYPE_CHATON:Ljava/lang/String; = "suggest_template_chat"

.field public static final TEMPLATE_TYPE_CONTACT:Ljava/lang/String; = "suggest_template_contact"

.field public static final TEMPLATE_TYPE_DEFAULT:Ljava/lang/String; = "suggest_template_default"

.field public static final TEMPLATE_TYPE_EMAIL:Ljava/lang/String; = "suggest_template_email"

.field public static final TEMPLATE_TYPE_GALLERY:Ljava/lang/String; = "suggest_template_content_image"

.field public static final TEMPLATE_TYPE_LIFETIME:Ljava/lang/String; = "suggest_template_lifetime"

.field public static final TEMPLATE_TYPE_MEMO:Ljava/lang/String; = "suggest_template_memo"

.field public static final TEMPLATE_TYPE_MESSAGE:Ljava/lang/String; = "suggest_template_message"

.field public static final TEMPLATE_TYPE_MUSIC:Ljava/lang/String; = "suggest_template_content_music"

.field public static final TEMPLATE_TYPE_MYFILES:Ljava/lang/String; = "suggest_template_content_file"

.field public static final TEMPLATE_TYPE_NOTE:Ljava/lang/String; = "suggest_template_note"

.field public static final TEMPLATE_TYPE_PHONE:Ljava/lang/String; = "suggest_template_history_call"

.field public static final TEMPLATE_TYPE_PINALL:Ljava/lang/String; = "suggest_template_pinall"

.field public static final TEMPLATE_TYPE_PLANNER:Ljava/lang/String; = "suggest_template_planner"

.field public static final TEMPLATE_TYPE_QUICKMEMO:Ljava/lang/String; = "suggest_template_ink_memo"

.field public static final TEMPLATE_TYPE_SAMSUNGLINK:Ljava/lang/String; = "suggest_template_content_file_cloud"

.field public static final TEMPLATE_TYPE_SETTINGS:Ljava/lang/String; = "suggest_template_settings"

.field public static final TEMPLATE_TYPE_STORYALBUM:Ljava/lang/String; = "suggest_template_coverpage"

.field public static final TEMPLATE_TYPE_VIDEO:Ljava/lang/String; = "suggest_template_content_video"

.field public static final TEMPLATE_TYPE_VNOTE:Ljava/lang/String; = "suggest_template_content_voice"

.field public static final TEMPLATE_TYPE_WEB_LINK:Ljava/lang/String; = "suggest_template_web_link"

.field public static final TYPEFACE_ROBOTO_BOLD:I = 0x0

.field public static final TYPEFACE_ROBOTO_LIGHT:I = 0x3

.field public static final TYPEFACE_ROBOTO_REGULAR:I = 0x1

.field public static final TYPEFACE_ROBOTO_THIN:I = 0x2

.field public static final TYPEFACE_SAMSUNGSANS_LIGHT:I = 0x5

.field public static final TYPEFACE_SAMSUNGSANS_REGULAR:I = 0x6

.field public static final TYPEFACE_SAMSUNGSANS_THIN:I = 0x4

.field public static final VZW_CODE:Z

.field public static final WRITING_BUDDY_UDS_MODE:Ljava/lang/String; = "Writingbuddy_UDSModeOn"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 337
    const-string v0, "China"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    .line 341
    const-string v0, "VZW"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/Constants;->VZW_CODE:Z

    .line 344
    const-string v0, "SPR"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "VMU"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BST"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "XAS"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/Constants;->SPR_CODE:Z

    .line 461
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/media"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/Constants;->INTERNAL_SYSTEM_STORAGE_DIRECTORY:Ljava/lang/String;

    .line 483
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v1

    const-string v1, "type"

    aput-object v1, v0, v2

    const/4 v1, 0x2

    const-string v2, "history_keyword"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "added_date"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/Constants;->HISTORY_PROJECTION:[Ljava/lang/String;

    return-void

    :cond_1
    move v0, v1

    .line 344
    goto :goto_0
.end method
