.class public Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;
.super Ljava/lang/Object;
.source "SuggestionCategoryBaseInfo.java"


# instance fields
.field private mBaseIntent:Landroid/content/Intent;

.field private mCategoryName:Ljava/lang/String;

.field public mContext:Landroid/content/Context;

.field private mPackageName:Ljava/lang/String;

.field private mQuery:Ljava/lang/String;

.field private mSearchActivity:Landroid/content/ComponentName;

.field private mSupportTagSearch:Z

.field private mTemplateStyle:Ljava/lang/String;

.field private mThresold:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mContext:Landroid/content/Context;

    .line 15
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mQuery:Ljava/lang/String;

    .line 17
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mCategoryName:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mPackageName:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mSearchActivity:Landroid/content/ComponentName;

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mThresold:I

    .line 25
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mBaseIntent:Landroid/content/Intent;

    .line 27
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mTemplateStyle:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mSupportTagSearch:Z

    .line 32
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method


# virtual methods
.method public getBaseIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mBaseIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchActivity()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mSearchActivity:Landroid/content/ComponentName;

    return-object v0
.end method

.method public getTemplateStyle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mTemplateStyle:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 62
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.provider.smartclip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "suggest_template_pinall"

    .line 71
    :goto_0
    return-object v0

    .line 66
    :cond_0
    const-string v0, "suggest_template_default"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mTemplateStyle:Ljava/lang/String;

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mTemplateStyle:Ljava/lang/String;

    goto :goto_0
.end method

.method public getThresold()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mThresold:I

    return v0
.end method

.method protected isSortedAlready()Z
    .locals 2

    .prologue
    .line 115
    const-string v0, "com.sec.android.app.music"

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const/4 v0, 0x1

    .line 118
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBaseIntent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;

    .prologue
    .line 95
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mBaseIntent:Landroid/content/Intent;

    .line 97
    if-eqz p2, :cond_0

    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mBaseIntent:Landroid/content/Intent;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 100
    :cond_0
    return-void
.end method

.method public setCategoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "categoryName"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mCategoryName:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mPackageName:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mQuery:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setSearchActivity(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mSearchActivity:Landroid/content/ComponentName;

    .line 84
    return-void
.end method

.method public setTagSearch(Z)V
    .locals 0
    .param p1, "support"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mSupportTagSearch:Z

    .line 108
    return-void
.end method

.method public setTemplateStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "templateStyle"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mTemplateStyle:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setThresold(I)V
    .locals 0
    .param p1, "thresold"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mThresold:I

    .line 92
    return-void
.end method

.method public supportTagSearch()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;->mSupportTagSearch:Z

    return v0
.end method
