.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListLifeTimesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 22
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    .line 26
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v4

    .line 27
    .local v4, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v3, 0x0

    .line 29
    .local v3, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;
    invoke-virtual {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 31
    if-nez p2, :cond_1

    .line 32
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f03003a

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 35
    new-instance v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;

    .end local v3    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;
    invoke-direct {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;-><init>()V

    .line 37
    .restart local v3    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;
    const v6, 0x7f0b006c

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    .line 38
    const v6, 0x7f0b0043

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;->tvDate:Landroid/widget/TextView;

    .line 39
    const v6, 0x7f0b006d

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    .line 41
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 46
    :goto_0
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v1

    .line 47
    .local v1, "desc":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "date":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v5

    .line 50
    .local v5, "thumb":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v2

    .line 52
    .local v2, "genericIntent":Landroid/content/Intent;
    iget-object v6, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    const/4 v7, 0x1

    invoke-virtual {p0, v6, v1, v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 53
    iget-object v6, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;->tvDate:Landroid/widget/TextView;

    invoke-virtual {p0, v6, v0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 55
    if-eqz v5, :cond_0

    iget-object v6, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    if-eqz v6, :cond_0

    iget-object v6, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-nez v6, :cond_0

    .line 56
    iget-object v6, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 57
    iget-object v6, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    invoke-virtual {p0, v6, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 60
    :cond_0
    invoke-virtual {p0, p2, v2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 62
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    return-object v6

    .line 43
    .end local v0    # "date":Ljava/lang/String;
    .end local v1    # "desc":Ljava/lang/String;
    .end local v2    # "genericIntent":Landroid/content/Intent;
    .end local v5    # "thumb":Ljava/lang/String;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;
    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;

    .restart local v3    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter$LifeTimesItemListViewHolder;
    goto :goto_0
.end method
