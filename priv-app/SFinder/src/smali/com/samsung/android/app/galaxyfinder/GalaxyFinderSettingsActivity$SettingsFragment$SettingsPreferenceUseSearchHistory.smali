.class public Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;
.super Landroid/preference/CheckBoxPreference;
.source "GalaxyFinderSettingsActivity.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$Accessibility;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SettingsPreferenceUseSearchHistory"
.end annotation


# instance fields
.field private mContentDescription:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;

    .line 184
    invoke-direct {p0, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->mContentDescription:Ljava/lang/String;

    .line 185
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->init()V

    .line 186
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;

    .line 189
    invoke-direct {p0, p2, p3}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->mContentDescription:Ljava/lang/String;

    .line 190
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->init()V

    .line 191
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 210
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->setDefaultValue(Ljava/lang/Object;)V

    .line 211
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->setEnabled(Z)V

    .line 212
    const-string v0, "setting_use_search_history"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->setKey(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->setSelectable(Z)V

    .line 214
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e00cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->setTitle(Ljava/lang/CharSequence;)V

    .line 216
    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 195
    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->onBindView(Landroid/view/View;)V

    .line 197
    const v1, 0x1020016

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 199
    .local v0, "title":Landroid/view/View;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->mContentDescription:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->mContentDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 202
    :cond_0
    return-void
.end method

.method public setContentDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->mContentDescription:Ljava/lang/String;

    .line 207
    return-void
.end method
