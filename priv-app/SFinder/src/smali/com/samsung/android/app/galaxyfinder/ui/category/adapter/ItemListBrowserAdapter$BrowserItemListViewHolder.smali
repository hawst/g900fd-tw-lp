.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;
.super Ljava/lang/Object;
.source "ItemListBrowserAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BrowserItemListViewHolder"
.end annotation


# instance fields
.field public ivIcon:Landroid/widget/ImageView;

.field public tvTitle:Landroid/widget/TextView;

.field public tvUrl:Landroid/widget/TextView;

.field public vgItemGroup:Landroid/view/ViewGroup;

.field public vgSubHeader:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 91
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->tvUrl:Landroid/widget/TextView;

    .line 93
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 95
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    .line 97
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListBrowserAdapter$BrowserItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    return-void
.end method
