.class public Lcom/samsung/android/app/galaxyfinder/ui/category/widget/SelectableImageView;
.super Landroid/widget/ImageView;
.source "SelectableImageView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 4

    .prologue
    .line 28
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 30
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/widget/SelectableImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090077

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 32
    .local v1, "list":Landroid/content/res/ColorStateList;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/widget/SelectableImageView;->getDrawableState()[I

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 34
    .local v0, "color":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/widget/SelectableImageView;->setColorFilter(I)V

    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/widget/SelectableImageView;->invalidate()V

    .line 36
    return-void
.end method
