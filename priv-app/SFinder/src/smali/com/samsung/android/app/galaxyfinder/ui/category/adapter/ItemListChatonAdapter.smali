.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListChatonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;
    }
.end annotation


# static fields
.field private static final CARD_TYPE_RECEIVE:I = 0x0

.field private static final CARD_TYPE_SEND:I = 0x1


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 27
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 18
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 31
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v8

    .line 32
    .local v8, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v5, 0x0

    .line 34
    .local v5, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 36
    const/4 v9, 0x0

    .line 37
    .local v9, "messageType":I
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v4

    .line 38
    .local v4, "extraFlags":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 40
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 46
    :cond_0
    :goto_0
    if-nez p2, :cond_4

    .line 47
    new-instance v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;

    .end local v5    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;
    invoke-direct {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;-><init>()V

    .line 49
    .restart local v5    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;
    const/4 v14, 0x1

    if-ne v9, v14, :cond_3

    .line 50
    sget-object v14, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v15, 0x7f030035

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v14 .. v17}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 57
    :goto_1
    const v14, 0x7f0b0053

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    iput-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->iv1:Landroid/widget/ImageView;

    .line 58
    const v14, 0x7f0b0054

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->tv1:Landroid/widget/TextView;

    .line 59
    const v14, 0x7f0b0057

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->tv2:Landroid/widget/TextView;

    .line 60
    const v14, 0x7f0b0056

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->tv3:Landroid/widget/TextView;

    .line 61
    const v14, 0x7f0b0055

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/RelativeLayout;

    iput-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->rl1:Landroid/widget/RelativeLayout;

    .line 62
    const v14, 0x7f0b0045

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup;

    iput-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    .line 64
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 66
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v11

    .line 67
    .local v11, "str1":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v12

    .line 68
    .local v12, "str2":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v13

    .line 69
    .local v13, "str3":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v10

    .line 71
    .local v10, "sortKeyValue":Ljava/lang/String;
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->tv1:Landroid/widget/TextView;

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v11, v15}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 72
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->tv2:Landroid/widget/TextView;

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v12, v15}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 73
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->tv3:Landroid/widget/TextView;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v13, v15}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 75
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 76
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v14, v1, v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->setSubHeaderView(Landroid/view/ViewGroup;ILjava/lang/String;)V

    .line 79
    :cond_1
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_2

    .line 80
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->rl1:Landroid/widget/RelativeLayout;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 87
    .end local v10    # "sortKeyValue":Ljava/lang/String;
    .end local v11    # "str1":Ljava/lang/String;
    .end local v12    # "str2":Ljava/lang/String;
    .end local v13    # "str3":Ljava/lang/String;
    :cond_2
    :goto_2
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v6

    .line 89
    .local v6, "icon1":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v7

    .line 91
    .local v7, "intent":Landroid/content/Intent;
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;->iv1:Landroid/widget/ImageView;

    const/4 v15, 0x0

    const/16 v16, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v14, v6, v15, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 93
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 95
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    return-object v14

    .line 41
    .end local v6    # "icon1":Ljava/lang/String;
    .end local v7    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v3

    .line 42
    .local v3, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v3}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_0

    .line 53
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    sget-object v14, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v15, 0x7f030034

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v14 .. v17}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_1

    .line 84
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;
    check-cast v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;

    .restart local v5    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter$ChatonItemListViewHolder;
    goto :goto_2
.end method
