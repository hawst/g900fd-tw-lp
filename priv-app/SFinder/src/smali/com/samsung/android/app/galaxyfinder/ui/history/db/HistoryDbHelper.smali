.class public Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "HistoryDbHelper.java"


# static fields
.field private static mInstance:Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;


# instance fields
.field private final GF_DB_INT_TYPE:Ljava/lang/String;

.field private final GF_DB_PRIMARY_KEY_TYPE:Ljava/lang/String;

.field private final GF_DB_TEXT_TYPE:Ljava/lang/String;

.field private final GF_DB_TIME_TYPE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->mInstance:Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const-string v0, "SFinderHistory.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 11
    const-string v0, "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->GF_DB_PRIMARY_KEY_TYPE:Ljava/lang/String;

    .line 13
    const-string v0, "TEXT"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->GF_DB_TEXT_TYPE:Ljava/lang/String;

    .line 15
    const-string v0, "TIMESTAMP"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->GF_DB_TIME_TYPE:Ljava/lang/String;

    .line 17
    const-string v0, "INTEGER"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->GF_DB_INT_TYPE:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    const-class v1, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->mInstance:Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->mInstance:Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;

    .line 25
    :cond_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->mInstance:Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 52
    :try_start_0
    const-string v1, "CREATE TABLE IF NOT EXISTS history (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,history_keyword TEXT,added_date TIMESTAMP);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 60
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 61
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 35
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 40
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS history"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDbHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 46
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    goto :goto_0
.end method
