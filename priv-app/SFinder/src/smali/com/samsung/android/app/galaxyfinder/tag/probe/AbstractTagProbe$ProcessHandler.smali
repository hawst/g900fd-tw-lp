.class final Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$ProcessHandler;
.super Landroid/os/Handler;
.source "AbstractTagProbe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProcessHandler"
.end annotation


# instance fields
.field private final mTagProbe:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;)V
    .locals 1
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "owner"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 61
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$ProcessHandler;->mTagProbe:Ljava/lang/ref/WeakReference;

    .line 62
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 66
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$ProcessHandler;->mTagProbe:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    .line 67
    .local v1, "owner":Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 68
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 80
    const-class v2, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UNKNOWN request message!!!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 70
    :pswitch_1
    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->bWaitingDelayedMessage:Z
    invoke-static {v1, v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->access$002(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;Z)Z

    .line 71
    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mWaitingDelayedObject:Ljava/lang/Object;
    invoke-static {v1, v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->access$102(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v2, v2, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    if-eqz v2, :cond_0

    .line 73
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    .line 74
    .local v0, "info":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;
    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->probe(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)V

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
