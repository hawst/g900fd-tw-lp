.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5$1;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 388
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->deleteAllHistoryItem()V

    .line 389
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getRecentHistoryItems(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 391
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;->val$mFooterView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 392
    return-void
.end method
