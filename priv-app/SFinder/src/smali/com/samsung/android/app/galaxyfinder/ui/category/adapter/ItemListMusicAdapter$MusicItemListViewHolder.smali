.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;
.super Ljava/lang/Object;
.source "ItemListMusicAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MusicItemListViewHolder"
.end annotation


# instance fields
.field public ivIcon:Landroid/widget/ImageView;

.field public ivThumb:Landroid/widget/ImageView;

.field public tvDesc:Landroid/widget/TextView;

.field public tvTitle:Landroid/widget/TextView;

.field public vgSubHeader:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 78
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    .line 80
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    .line 82
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 84
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    return-void
.end method
