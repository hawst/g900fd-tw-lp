.class public Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;
.super Ljava/lang/Object;
.source "ThumbnailUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;
    }
.end annotation


# static fields
.field public static final FILE_TYPE_APK:I = 0x3ec

.field public static final FILE_TYPE_AUDIO:I = 0x3eb

.field public static final FILE_TYPE_ETC:I = 0x3ed

.field public static final FILE_TYPE_FOLDER:I = 0x7d0

.field public static final FILE_TYPE_IMAGE:I = 0x3e9

.field public static final FILE_TYPE_VIDEO:I = 0x3ea

.field public static final GET_THUMBNAIL_TIME:I = 0xe4e1c0

.field public static final MICRO_HEIGHT:I = 0x8c

.field public static final MICRO_WIDTH:I = 0xbe


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method public static getAlbumartBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumID"    # I

    .prologue
    const/4 v11, 0x0

    .line 409
    const/4 v1, 0x0

    .line 411
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 412
    .local v6, "sBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    const-string v12, "content://media/external/audio/albumart"

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 414
    .local v0, "artworkUri":Landroid/net/Uri;
    const/16 v10, 0x80

    .line 415
    .local v10, "width":I
    const/16 v5, 0x80

    .line 417
    .local v5, "height":I
    const/4 v4, 0x0

    .line 420
    .local v4, "fileDescriptor":Landroid/os/ParcelFileDescriptor;
    int-to-long v12, p1

    :try_start_0
    invoke-static {v0, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    .line 421
    .local v9, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "r"

    invoke-virtual {v12, v9, v13}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 422
    if-nez v4, :cond_1

    .line 463
    if-eqz v4, :cond_0

    .line 465
    :try_start_1
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 470
    :goto_0
    if-nez v1, :cond_0

    .line 476
    .end local v9    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_1
    return-object v11

    .line 426
    .restart local v9    # "uri":Landroid/net/Uri;
    :cond_1
    const/4 v7, 0x1

    .line 427
    .local v7, "sampleSize":I
    const/4 v11, 0x1

    :try_start_2
    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 428
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v12, v6}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 431
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gt v11, v5, :cond_2

    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-le v11, v10, :cond_3

    .line 433
    :cond_2
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v12, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-le v11, v12, :cond_7

    .line 434
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v11, v11

    int-to-float v12, v5

    div-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 440
    :cond_3
    :goto_2
    iput v7, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 441
    const/4 v11, 0x0

    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 443
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v12, v6}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 446
    .local v2, "bm":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_5

    .line 448
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v11, v10, :cond_4

    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eq v11, v5, :cond_5

    .line 449
    :cond_4
    const/4 v11, 0x1

    invoke-static {v2, v10, v5, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 450
    .local v8, "tmpBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 451
    move-object v2, v8

    .line 455
    .end local v8    # "tmpBitmap":Landroid/graphics/Bitmap;
    :cond_5
    move-object v1, v2

    .line 463
    if-eqz v4, :cond_6

    .line 465
    :try_start_3
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 470
    :goto_3
    if-nez v1, :cond_6

    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v7    # "sampleSize":I
    .end local v9    # "uri":Landroid/net/Uri;
    :cond_6
    :goto_4
    move-object v11, v1

    .line 476
    goto :goto_1

    .line 436
    .restart local v7    # "sampleSize":I
    .restart local v9    # "uri":Landroid/net/Uri;
    :cond_7
    :try_start_4
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v11, v11

    int-to-float v12, v10

    div-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v7

    goto :goto_2

    .line 457
    .end local v7    # "sampleSize":I
    .end local v9    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v11

    .line 463
    if-eqz v4, :cond_6

    .line 465
    :try_start_5
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 470
    :goto_5
    if-nez v1, :cond_6

    goto :goto_4

    .line 459
    :catch_1
    move-exception v3

    .line 460
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    :try_start_6
    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 463
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v11

    if-eqz v4, :cond_8

    .line 465
    :try_start_7
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 470
    :goto_6
    if-nez v1, :cond_8

    :cond_8
    throw v11

    .line 466
    .restart local v9    # "uri":Landroid/net/Uri;
    :catch_2
    move-exception v12

    goto :goto_0

    .restart local v2    # "bm":Landroid/graphics/Bitmap;
    .restart local v7    # "sampleSize":I
    :catch_3
    move-exception v11

    goto :goto_3

    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v7    # "sampleSize":I
    .end local v9    # "uri":Landroid/net/Uri;
    :catch_4
    move-exception v11

    goto :goto_5

    :catch_5
    move-exception v12

    goto :goto_6
.end method

.method public static getAlbumartBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 309
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 310
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 311
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 313
    const/4 v8, 0x0

    .line 314
    .local v8, "c":Landroid/database/Cursor;
    const/4 v6, -0x1

    .line 317
    .local v6, "albumId":I
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 319
    if-eqz v8, :cond_1

    .line 321
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    const-string v1, "album_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 326
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 329
    :cond_1
    const/4 v1, -0x1

    if-eq v6, v1, :cond_2

    .line 330
    invoke-static {p0, v6}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getAlbumartBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 340
    .end local v6    # "albumId":I
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_2
    :goto_0
    return-object v7

    .line 332
    .restart local v6    # "albumId":I
    .restart local v8    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 333
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 334
    if-eqz v8, :cond_2

    .line 335
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getApkBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 344
    const/4 v3, 0x0

    .line 346
    .local v3, "retBmp":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, p1, v5}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 349
    .local v2, "mPkgInfo":Landroid/content/pm/PackageInfo;
    if-eqz v2, :cond_0

    .line 353
    :try_start_0
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-static {p0, v4, p1}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getApkDrawable(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 355
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 357
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getResBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 368
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_0
    return-object v3

    .line 360
    :catch_0
    move-exception v1

    .line 361
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 362
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v1

    .line 364
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getApkDrawable(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 374
    const/4 v0, 0x0

    .line 376
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    iput-object p2, p1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 378
    iput-object p2, p1, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 380
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->icon:I

    if-eqz v1, :cond_0

    .line 382
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 385
    :cond_0
    return-object v0
.end method

.method public static getDefaultImageResId(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0, "mimetype"    # Ljava/lang/String;
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 108
    invoke-static {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getFileType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 110
    .local v1, "fileType":I
    sparse-switch v1, :sswitch_data_0

    .line 127
    const v0, 0x7f0200c4

    .line 130
    .local v0, "defaultIconResId":I
    :goto_0
    return v0

    .line 112
    .end local v0    # "defaultIconResId":I
    :sswitch_0
    const v0, 0x7f0200c6

    .line 113
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 115
    .end local v0    # "defaultIconResId":I
    :sswitch_1
    const v0, 0x7f0200c8

    .line 116
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 118
    .end local v0    # "defaultIconResId":I
    :sswitch_2
    const v0, 0x7f0200c3

    .line 119
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 121
    .end local v0    # "defaultIconResId":I
    :sswitch_3
    const v0, 0x7f0200c4

    .line 122
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 124
    .end local v0    # "defaultIconResId":I
    :sswitch_4
    const v0, 0x7f0200c5

    .line 125
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 110
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_1
        0x3eb -> :sswitch_2
        0x3ed -> :sswitch_3
        0x7d0 -> :sswitch_4
    .end sparse-switch
.end method

.method public static getExifOrientation(Ljava/lang/String;)I
    .locals 6
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    .line 481
    const/4 v0, 0x0

    .line 482
    .local v0, "degree":I
    const/4 v1, 0x0

    .line 485
    .local v1, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "exif":Landroid/media/ExifInterface;
    .local v2, "exif":Landroid/media/ExifInterface;
    move-object v1, v2

    .line 490
    .end local v2    # "exif":Landroid/media/ExifInterface;
    .restart local v1    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v1, :cond_0

    .line 492
    const-string v4, "Orientation"

    invoke-virtual {v1, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    .line 494
    .local v3, "orientation":I
    if-eq v3, v5, :cond_0

    .line 497
    packed-switch v3, :pswitch_data_0

    .line 516
    .end local v3    # "orientation":I
    :cond_0
    :goto_1
    :pswitch_0
    return v0

    .line 500
    .restart local v3    # "orientation":I
    :pswitch_1
    const/16 v0, 0x5a

    .line 501
    goto :goto_1

    .line 504
    :pswitch_2
    const/16 v0, 0xb4

    .line 505
    goto :goto_1

    .line 508
    :pswitch_3
    const/16 v0, 0x10e

    .line 509
    goto :goto_1

    .line 486
    .end local v3    # "orientation":I
    :catch_0
    move-exception v4

    goto :goto_0

    .line 497
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getFileType(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "mimeType"    # Ljava/lang/String;
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 87
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 88
    const-string v0, "image/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const/16 v0, 0x3e9

    .line 103
    :goto_0
    return v0

    .line 90
    :cond_0
    const-string v0, "video/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    const/16 v0, 0x3ea

    goto :goto_0

    .line 92
    :cond_1
    const-string v0, "audio/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    const/16 v0, 0x3eb

    goto :goto_0

    .line 94
    :cond_2
    const-string v0, "application/apk"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 95
    const/16 v0, 0x3ec

    goto :goto_0

    .line 98
    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    .line 99
    :cond_4
    const/16 v0, 0x7d0

    goto :goto_0

    .line 103
    :cond_5
    const/16 v0, 0x3ed

    goto :goto_0
.end method

.method public static getImageBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 168
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 169
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v13, 0x0

    .line 171
    .local v13, "retBmp":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 172
    const/4 v6, 0x0

    .line 175
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "_data= ? COLLATE LOCALIZED"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 180
    if-eqz v6, :cond_2

    .line 182
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 184
    const-string v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 185
    .local v9, "index":I
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 187
    .local v10, "id":J
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-static {v0, v10, v11, v1, v2}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 190
    if-eqz v13, :cond_1

    .line 192
    const-string v1, "orientation"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 193
    .local v12, "index2":I
    invoke-interface {v6, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 194
    .local v7, "degree":I
    if-nez v7, :cond_0

    .line 195
    invoke-static {p1}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getExifOrientation(Ljava/lang/String;)I

    move-result v7

    .line 198
    :cond_0
    if-eqz v7, :cond_1

    .line 199
    invoke-static {v13, v7}, Lcom/samsung/android/app/galaxyfinder/util/BitmapUtils;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 203
    .end local v7    # "degree":I
    .end local v9    # "index":I
    .end local v10    # "id":J
    .end local v12    # "index2":I
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_2
    :goto_0
    return-object v13

    .line 206
    .restart local v6    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 207
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 208
    if-eqz v6, :cond_2

    .line 209
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private static getResBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "d"    # Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    const/16 v6, 0x7f

    const/4 v5, 0x0

    .line 390
    const/4 v0, 0x0

    .line 392
    .local v0, "bmp":Landroid/graphics/Bitmap;
    const/16 v3, 0x80

    .line 394
    .local v3, "w":I
    const/16 v2, 0x80

    .line 396
    .local v2, "h":I
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 398
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 400
    .local v1, "c":Landroid/graphics/Canvas;
    invoke-virtual {p0, v5, v5, v6, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 402
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 404
    return-object v0
.end method

.method public static getThumbnailDrawableWithMakeCache(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 53
    const/4 v2, 0x0

    .line 54
    .local v2, "resultDrawable":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v1, 0x0

    .line 55
    .local v1, "resultBitmap":Landroid/graphics/Bitmap;
    const/16 v0, 0x3ed

    .line 57
    .local v0, "fileType":I
    if-nez v2, :cond_0

    .line 58
    invoke-static {p2, p1}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getFileType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 60
    packed-switch v0, :pswitch_data_0

    .line 79
    :goto_0
    :pswitch_0
    if-eqz v1, :cond_0

    .line 80
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "resultDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 83
    .restart local v2    # "resultDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_0
    return-object v2

    .line 62
    :pswitch_1
    invoke-static {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getImageBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 63
    goto :goto_0

    .line 65
    :pswitch_2
    invoke-static {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getVideoBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 66
    goto :goto_0

    .line 68
    :pswitch_3
    invoke-static {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getAlbumartBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 69
    goto :goto_0

    .line 71
    :pswitch_4
    invoke-static {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getApkBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 72
    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public static getVideoBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 235
    const/4 v6, 0x0

    .line 236
    .local v6, "b":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 238
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 240
    const/4 v7, 0x0

    .line 243
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 246
    if-eqz v7, :cond_1

    .line 248
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    const-string v1, "_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 252
    .local v9, "index":I
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 254
    .local v10, "id":J
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-static {v0, v10, v11, v1, v2}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 257
    .end local v9    # "index":I
    .end local v10    # "id":J
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    .end local v7    # "c":Landroid/database/Cursor;
    :cond_1
    :goto_0
    return-object v6

    .line 260
    .restart local v7    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 261
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static setDefaultImage(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "view"    # Landroid/widget/ImageView;
    .param p1, "mimetype"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-static {p1, p2}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getFileType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 137
    .local v1, "fileType":I
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_0

    .line 140
    sparse-switch v1, :sswitch_data_0

    .line 157
    const v0, 0x7f0200c4

    .line 161
    .local v0, "defaultIconResId":I
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 162
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 164
    .end local v0    # "defaultIconResId":I
    :cond_0
    return-void

    .line 142
    :sswitch_0
    const v0, 0x7f0200c6

    .line 143
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 145
    .end local v0    # "defaultIconResId":I
    :sswitch_1
    const v0, 0x7f0200c8

    .line 146
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 148
    .end local v0    # "defaultIconResId":I
    :sswitch_2
    const v0, 0x7f0200c3

    .line 149
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 151
    .end local v0    # "defaultIconResId":I
    :sswitch_3
    const v0, 0x7f0200c5

    .line 152
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 154
    .end local v0    # "defaultIconResId":I
    :sswitch_4
    const v0, 0x7f0200c4

    .line 155
    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 140
    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_1
        0x3eb -> :sswitch_2
        0x3ed -> :sswitch_4
        0x7d0 -> :sswitch_3
    .end sparse-switch
.end method
