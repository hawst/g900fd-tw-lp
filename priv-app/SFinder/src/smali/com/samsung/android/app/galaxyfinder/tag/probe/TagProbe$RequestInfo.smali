.class public Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;
.super Ljava/lang/Object;
.source "TagProbe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RequestInfo"
.end annotation


# instance fields
.field private pkgName:Ljava/lang/String;

.field private queryUri:Landroid/net/Uri;

.field private reqType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p1, "reqType"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "pkg"    # Ljava/lang/String;
    .param p3, "queryUri"    # Landroid/net/Uri;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->reqType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    .line 70
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->queryUri:Landroid/net/Uri;

    .line 71
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->pkgName:Ljava/lang/String;

    .line 72
    return-void
.end method


# virtual methods
.method public getPkgName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->pkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->queryUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->reqType:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    return-object v0
.end method
