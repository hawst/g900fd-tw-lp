.class public Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "TagDBHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper$TagInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;


# instance fields
.field final FOREIGNKEY:Ljava/lang/String;

.field private final PERFORMANCE:Ljava/lang/String;

.field protected final TAGDB_ADDRESS_TYPE:Ljava/lang/String;

.field protected final TAGDB_APPNAME_TYPE:Ljava/lang/String;

.field protected final TAGDB_BOOLEAN_TYPE:Ljava/lang/String;

.field protected final TAGDB_CONTACTNAME_TYPE:Ljava/lang/String;

.field protected final TAGDB_COUNTRY_TYPE:Ljava/lang/String;

.field protected final TAGDB_DATE_TYPE:Ljava/lang/String;

.field protected final TAGDB_DOUBLE_TYPE:Ljava/lang/String;

.field protected final TAGDB_INTEGER_TYPE:Ljava/lang/String;

.field protected final TAGDB_LONG_TYPE:Ljava/lang/String;

.field protected final TAGDB_PRIMARYKEY_TYPE:Ljava/lang/String;

.field protected final TAGDB_TEXT_TYPE:Ljava/lang/String;

.field protected final TAGDB_TIMESTAMP_TYPE:Ljava/lang/String;

.field protected final TAGDB_URI_TYPE:Ljava/lang/String;

.field protected final TAGDB_WEATHER_TYPE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    const-string v0, "Tag.db"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 32
    const-string v0, "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_PRIMARYKEY_TYPE:Ljava/lang/String;

    .line 34
    const-string v0, "BOOLEAN"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_BOOLEAN_TYPE:Ljava/lang/String;

    .line 36
    const-string v0, "INTEGER"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_INTEGER_TYPE:Ljava/lang/String;

    .line 38
    const-string v0, "DOUBLE"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_DOUBLE_TYPE:Ljava/lang/String;

    .line 40
    const-string v0, "LONG"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_LONG_TYPE:Ljava/lang/String;

    .line 42
    const-string v0, "DATE"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_DATE_TYPE:Ljava/lang/String;

    .line 44
    const-string v0, "TIMESTAMP"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_TIMESTAMP_TYPE:Ljava/lang/String;

    .line 46
    const-string v0, "VARCHAR(50)"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_WEATHER_TYPE:Ljava/lang/String;

    .line 48
    const-string v0, "VARCHAR(50)"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_CONTACTNAME_TYPE:Ljava/lang/String;

    .line 50
    const-string v0, "VARCHAR(50)"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_ADDRESS_TYPE:Ljava/lang/String;

    .line 52
    const-string v0, "VARCHAR(20)"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_COUNTRY_TYPE:Ljava/lang/String;

    .line 54
    const-string v0, "TEXT"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_TEXT_TYPE:Ljava/lang/String;

    .line 56
    const-string v0, "VARCHAR(50)"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_APPNAME_TYPE:Ljava/lang/String;

    .line 58
    const-string v0, "VARCHAR(100)"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAGDB_URI_TYPE:Ljava/lang/String;

    .line 60
    const-string v0, "foreign key"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->FOREIGNKEY:Ljava/lang/String;

    .line 62
    const-string v0, "[PERFORMANCE] "

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->PERFORMANCE:Ljava/lang/String;

    .line 68
    return-void
.end method

.method private bulkDeleteInContentDb(Ljava/util/ArrayList;Ljava/lang/String;)I
    .locals 18
    .param p2, "APP_TAG"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 971
    .local p1, "shouldDelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    const-string v12, "DELETE FROM contents WHERE _id = ?"

    .line 973
    .local v12, "sql":Ljava/lang/String;
    const/4 v7, 0x0

    .line 974
    .local v7, "deleteCount":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 976
    .local v6, "deleteContentIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v13, 0x0

    .line 977
    .local v13, "statement":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 981
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;

    .line 983
    .local v4, "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    :try_start_0
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getId()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 989
    .local v2, "contentID":J
    const-string v14, "content_id"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2, v3}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->checkValidRecord(Ljava/lang/String;J)Z

    move-result v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_0

    .line 990
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 984
    .end local v2    # "contentID":J
    :catch_0
    move-exception v11

    .line 985
    .local v11, "nfe":Ljava/lang/NumberFormatException;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "bulkDeleteInContentDb contentID : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v11}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 994
    .end local v4    # "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v11    # "nfe":Ljava/lang/NumberFormatException;
    :cond_1
    monitor-enter p0

    .line 996
    :try_start_1
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 997
    invoke-virtual {v5, v12}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v13

    .line 999
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    .line 1000
    .local v10, "id":Ljava/lang/Long;
    const/4 v14, 0x1

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v13, v14, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1003
    :try_start_2
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v14

    add-int/2addr v7, v14

    goto :goto_1

    .line 1005
    :catch_1
    move-exception v8

    .line 1006
    .local v8, "e":Landroid/database/SQLException;
    :try_start_3
    invoke-virtual {v8}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 1011
    .end local v8    # "e":Landroid/database/SQLException;
    .end local v10    # "id":Ljava/lang/Long;
    :catch_2
    move-exception v8

    .line 1012
    .restart local v8    # "e":Landroid/database/SQLException;
    :try_start_4
    invoke-virtual {v8}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1014
    if-eqz v13, :cond_2

    .line 1015
    :try_start_5
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 1016
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1018
    :cond_2
    if-eqz v5, :cond_3

    .line 1019
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1022
    .end local v8    # "e":Landroid/database/SQLException;
    :cond_3
    :goto_2
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1024
    return v7

    .line 1010
    :cond_4
    :try_start_6
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1014
    if-eqz v13, :cond_5

    .line 1015
    :try_start_7
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 1016
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1018
    :cond_5
    if-eqz v5, :cond_3

    .line 1019
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_2

    .line 1022
    :catchall_0
    move-exception v14

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v14

    .line 1014
    :catchall_1
    move-exception v14

    if-eqz v13, :cond_6

    .line 1015
    :try_start_8
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 1016
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1018
    :cond_6
    if-eqz v5, :cond_7

    .line 1019
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_7
    throw v14
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method

.method private bulkDeleteInTagDb(Ljava/util/ArrayList;Ljava/lang/String;)I
    .locals 24
    .param p2, "APP_TAG"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 872
    .local p1, "shouldDelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    const-string v13, "DELETE FROM tags WHERE _id = ?"

    .line 873
    .local v13, "sql":Ljava/lang/String;
    const-string v9, "DELETE FROM tags_fts WHERE type = ? AND data = ?"

    .line 876
    .local v9, "ftsSql":Ljava/lang/String;
    const/4 v6, 0x0

    .line 877
    .local v6, "deleteCount":I
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 878
    .local v19, "tagInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper$TagInfo;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 880
    .local v7, "deleteTagIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v14, 0x0

    .line 881
    .local v14, "statement":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 887
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;

    .line 889
    .local v4, "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    :try_start_0
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getTid()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 890
    .local v16, "tagID":J
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getType()I

    move-result v20

    .line 891
    .local v20, "tagType":I
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getTagData()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v15

    .line 897
    .local v15, "tagData":Ljava/lang/String;
    const-string v21, "tag_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->checkValidRecord(Ljava/lang/String;J)Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_0

    .line 898
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 899
    new-instance v21, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper$TagInfo;

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-direct {v0, v1, v15}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper$TagInfo;-><init>(ILjava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 892
    .end local v15    # "tagData":Ljava/lang/String;
    .end local v16    # "tagID":J
    .end local v20    # "tagType":I
    :catch_0
    move-exception v12

    .line 893
    .local v12, "nfe":Ljava/lang/NumberFormatException;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "bulkDeleteInTagDb tagID : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v12}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 903
    .end local v4    # "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v12    # "nfe":Ljava/lang/NumberFormatException;
    :cond_1
    monitor-enter p0

    .line 905
    :try_start_1
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 906
    invoke-virtual {v5, v13}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v14

    .line 908
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    .line 909
    .local v11, "id":Ljava/lang/Long;
    const/16 v21, 0x1

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-virtual {v14, v0, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 912
    :try_start_2
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v21

    add-int v6, v6, v21

    goto :goto_1

    .line 914
    :catch_1
    move-exception v8

    .line 915
    .local v8, "e":Landroid/database/SQLException;
    :try_start_3
    invoke-virtual {v8}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 920
    .end local v8    # "e":Landroid/database/SQLException;
    .end local v11    # "id":Ljava/lang/Long;
    :catch_2
    move-exception v8

    .line 921
    .restart local v8    # "e":Landroid/database/SQLException;
    :try_start_4
    invoke-virtual {v8}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 923
    if-eqz v14, :cond_2

    .line 924
    :try_start_5
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 925
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 927
    :cond_2
    if-eqz v5, :cond_3

    .line 928
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 931
    .end local v8    # "e":Landroid/database/SQLException;
    :cond_3
    :goto_2
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 933
    monitor-enter p0

    .line 935
    :try_start_6
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 936
    invoke-virtual {v5, v9}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v14

    .line 938
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper$TagInfo;

    .line 939
    .local v18, "tagInfo":Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper$TagInfo;
    const/16 v21, 0x1

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper$TagInfo;->mTagType:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    move/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-virtual {v14, v0, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 940
    const/16 v21, 0x2

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper$TagInfo;->mTagData:Ljava/lang/String;

    move-object/from16 v22, v0

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v14, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 943
    :try_start_7
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I
    :try_end_7
    .catch Landroid/database/SQLException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_3

    .line 945
    :catch_3
    move-exception v8

    .line 946
    .restart local v8    # "e":Landroid/database/SQLException;
    :try_start_8
    invoke-virtual {v8}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_8
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto :goto_3

    .line 951
    .end local v8    # "e":Landroid/database/SQLException;
    .end local v18    # "tagInfo":Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper$TagInfo;
    :catch_4
    move-exception v8

    .line 952
    .restart local v8    # "e":Landroid/database/SQLException;
    :try_start_9
    invoke-virtual {v8}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 954
    if-eqz v14, :cond_4

    .line 955
    :try_start_a
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 956
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 958
    :cond_4
    if-eqz v5, :cond_5

    .line 959
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 962
    .end local v8    # "e":Landroid/database/SQLException;
    :cond_5
    :goto_4
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 964
    if-lez v6, :cond_6

    .line 965
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    sget-object v22, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 967
    :cond_6
    return v6

    .line 919
    :cond_7
    :try_start_b
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_b
    .catch Landroid/database/SQLException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 923
    if-eqz v14, :cond_8

    .line 924
    :try_start_c
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 925
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 927
    :cond_8
    if-eqz v5, :cond_3

    .line 928
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_2

    .line 931
    :catchall_0
    move-exception v21

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    throw v21

    .line 923
    :catchall_1
    move-exception v21

    if-eqz v14, :cond_9

    .line 924
    :try_start_d
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 925
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 927
    :cond_9
    if-eqz v5, :cond_a

    .line 928
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_a
    throw v21
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 950
    :cond_b
    :try_start_e
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e
    .catch Landroid/database/SQLException; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 954
    if-eqz v14, :cond_c

    .line 955
    :try_start_f
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 956
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 958
    :cond_c
    if-eqz v5, :cond_5

    .line 959
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_4

    .line 962
    :catchall_2
    move-exception v21

    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    throw v21

    .line 954
    :catchall_3
    move-exception v21

    if-eqz v14, :cond_d

    .line 955
    :try_start_10
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 956
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 958
    :cond_d
    if-eqz v5, :cond_e

    .line 959
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_e
    throw v21
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2
.end method

.method private bulkDeleteInTaggingDb(Ljava/util/ArrayList;Ljava/lang/String;)I
    .locals 17
    .param p2, "APP_TAG"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 820
    .local p1, "shouldDelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    const-string v10, "DELETE FROM tagging WHERE content_id = ? AND tag_id = ?"

    .line 823
    .local v10, "sql":Ljava/lang/String;
    const/4 v6, 0x0

    .line 824
    .local v6, "deleteCount":I
    const/4 v11, 0x0

    .line 825
    .local v11, "statement":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 827
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    monitor-enter p0

    .line 832
    :try_start_0
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 833
    invoke-virtual {v5, v10}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v11

    .line 835
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 837
    .local v4, "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    :try_start_1
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getId()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 838
    .local v2, "contentID":J
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getTid()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v12

    .line 845
    .local v12, "tagID":J
    const/4 v14, 0x1

    :try_start_2
    invoke-virtual {v11, v14, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 846
    const/4 v14, 0x2

    invoke-virtual {v11, v14, v12, v13}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 848
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v14

    add-int/2addr v6, v14

    goto :goto_0

    .line 839
    .end local v2    # "contentID":J
    .end local v12    # "tagID":J
    :catch_0
    move-exception v9

    .line 840
    .local v9, "nfe":Ljava/lang/NumberFormatException;
    :try_start_3
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "bulkDeleteInTaggingDb contentID : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 855
    .end local v4    # "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "nfe":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v7

    .line 856
    .local v7, "e":Landroid/database/SQLException;
    :try_start_4
    invoke-virtual {v7}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 858
    if-eqz v11, :cond_0

    .line 859
    :try_start_5
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 860
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 862
    :cond_0
    if-eqz v5, :cond_1

    .line 863
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 866
    .end local v7    # "e":Landroid/database/SQLException;
    :cond_1
    :goto_1
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 868
    return v6

    .line 850
    .restart local v2    # "contentID":J
    .restart local v4    # "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v12    # "tagID":J
    :catch_2
    move-exception v7

    .line 851
    .restart local v7    # "e":Landroid/database/SQLException;
    :try_start_6
    invoke-virtual {v7}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 858
    .end local v2    # "contentID":J
    .end local v4    # "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v7    # "e":Landroid/database/SQLException;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v12    # "tagID":J
    :catchall_0
    move-exception v14

    if-eqz v11, :cond_2

    .line 859
    :try_start_7
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 860
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 862
    :cond_2
    if-eqz v5, :cond_3

    .line 863
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_3
    throw v14

    .line 866
    :catchall_1
    move-exception v14

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v14

    .line 854
    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_8
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_8
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 858
    if-eqz v11, :cond_5

    .line 859
    :try_start_9
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 860
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 862
    :cond_5
    if-eqz v5, :cond_1

    .line 863
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1
.end method

.method private bulkInsertInContentDb(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 21
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "APP_TAG"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 522
    .local p1, "fromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    const-string v8, "timestamp, contenturi, appname"

    .line 523
    .local v8, "contentColumns":Ljava/lang/String;
    const-string v11, "INSERT INTO contents(timestamp, contenturi, appname) VALUES (?, ?, ?)"

    .line 525
    .local v11, "contentSql":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getAllContentUri(Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v10

    .line 527
    .local v10, "contentMapFromTagDB":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/16 v16, 0x0

    .line 528
    .local v16, "statement":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v13

    .line 530
    .local v13, "db":Landroid/database/sqlite/SQLiteDatabase;
    monitor-enter p0

    .line 532
    const/16 v17, 0x0

    .line 533
    .local v17, "timeStamp":Ljava/lang/String;
    const/4 v12, 0x0

    .line 535
    .local v12, "contentUri":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 536
    invoke-virtual {v13, v11}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v16

    .line 538
    invoke-virtual/range {p1 .. p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 539
    .local v15, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 540
    .local v9, "contentMapFromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/4 v4, 0x0

    .line 541
    .local v4, "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_a

    .line 542
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 543
    .local v5, "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    check-cast v4, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;

    .line 545
    .restart local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getContenturi()Ljava/lang/String;

    move-result-object v12

    .line 546
    if-eqz v12, :cond_0

    .line 549
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getCreateTime()Ljava/lang/String;

    move-result-object v17

    .line 550
    if-eqz v17, :cond_0

    .line 554
    const/16 v18, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v18

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 555
    const/16 v18, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1, v12}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 556
    const/16 v18, 0x3

    move-object/from16 v0, v16

    move/from16 v1, v18

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 558
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v6

    .line 559
    .local v6, "cid":J
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 560
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "bulkInsertInContentDb() CONTENTS cid = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", uri = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    :cond_1
    const-wide/16 v18, -0x1

    cmp-long v18, v6, v18

    if-eqz v18, :cond_0

    .line 562
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setId(Ljava/lang/String;)V

    .line 563
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 565
    .end local v6    # "cid":J
    :catch_0
    move-exception v14

    .line 566
    .local v14, "e":Landroid/database/sqlite/SQLiteConstraintException;
    :try_start_2
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 567
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "bulkInsertInContentDb() CONTENTS uri = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    :cond_2
    invoke-virtual {v9, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    .line 569
    .local v6, "cid":Ljava/lang/Long;
    if-eqz v6, :cond_6

    .line 570
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 571
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "bulkInsertInContentDb() CONTENTS [FromApp] find already exist key(cid) = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    :cond_3
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setId(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 587
    .end local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v5    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .end local v6    # "cid":Ljava/lang/Long;
    .end local v9    # "contentMapFromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v14    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    .end local v15    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    :catch_1
    move-exception v14

    .line 588
    .local v14, "e":Landroid/database/SQLException;
    :try_start_3
    invoke-virtual {v14}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 590
    if-eqz v16, :cond_4

    .line 591
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 592
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 594
    :cond_4
    if-eqz v13, :cond_5

    .line 595
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 598
    .end local v14    # "e":Landroid/database/SQLException;
    :cond_5
    :goto_1
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 599
    return-void

    .line 576
    .restart local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .restart local v5    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .restart local v6    # "cid":Ljava/lang/Long;
    .restart local v9    # "contentMapFromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .local v14, "e":Landroid/database/sqlite/SQLiteConstraintException;
    .restart local v15    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    :cond_6
    :try_start_5
    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "cid":Ljava/lang/Long;
    check-cast v6, Ljava/lang/Long;

    .line 577
    .restart local v6    # "cid":Ljava/lang/Long;
    if-eqz v6, :cond_0

    .line 578
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 579
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "bulkInsertInContentDb() CONTENTS [FromTagDb] find already exist key(cid) = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    :cond_7
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setId(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 590
    .end local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v5    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .end local v6    # "cid":Ljava/lang/Long;
    .end local v9    # "contentMapFromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v14    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    .end local v15    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    :catchall_0
    move-exception v18

    if-eqz v16, :cond_8

    .line 591
    :try_start_6
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 592
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 594
    :cond_8
    if-eqz v13, :cond_9

    .line 595
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_9
    throw v18

    .line 598
    :catchall_1
    move-exception v18

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v18

    .line 586
    .restart local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .restart local v9    # "contentMapFromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v15    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    :cond_a
    :try_start_7
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7
    .catch Landroid/database/SQLException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 590
    if-eqz v16, :cond_b

    .line 591
    :try_start_8
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 592
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 594
    :cond_b
    if-eqz v13, :cond_5

    .line 595
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1
.end method

.method private bulkInsertInTagDb(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 24
    .param p2, "APP_TAG"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 602
    .local p1, "fromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    const-string v14, "type, rawdata, data"

    .line 603
    .local v14, "tagsColumns":Ljava/lang/String;
    const-string v17, "INSERT INTO tags(type, rawdata, data) VALUES (?, ?, ?)"

    .line 605
    .local v17, "tagsSql":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getAllTagsData(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v16

    .line 607
    .local v16, "tagsMapFromTagDB":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/4 v8, 0x0

    .line 608
    .local v8, "isInsertTag":Z
    const/4 v10, 0x0

    .line 609
    .local v10, "statement":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 611
    .local v6, "db":Landroid/database/sqlite/SQLiteDatabase;
    monitor-enter p0

    .line 613
    const/4 v12, 0x0

    .line 614
    .local v12, "tagType":I
    const/4 v13, 0x0

    .line 615
    .local v13, "tagValue":Ljava/lang/String;
    const/4 v11, 0x0

    .line 617
    .local v11, "tagKey":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 618
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v10

    .line 620
    invoke-virtual/range {p1 .. p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 621
    .local v9, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 622
    .local v15, "tagsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/4 v4, 0x0

    .line 623
    .local v4, "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_b

    .line 624
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 625
    .local v5, "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    check-cast v4, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;

    .line 627
    .restart local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getType()I

    move-result v12

    .line 628
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getTagData()Ljava/lang/String;

    move-result-object v13

    .line 629
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v21, v12, 0x7c

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 631
    const/16 v20, 0x1

    int-to-long v0, v12

    move-wide/from16 v22, v0

    move/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v10, v0, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 632
    const/16 v20, 0x2

    move/from16 v0, v20

    invoke-virtual {v10, v0, v13}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 633
    const/16 v20, 0x3

    move/from16 v0, v20

    invoke-virtual {v10, v0, v13}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 635
    :try_start_1
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v18

    .line 636
    .local v18, "tid":J
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 637
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "bulkInsertInTagDb() TAGS tid = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", tagType = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", tagValue = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    :cond_1
    const-wide/16 v20, -0x1

    cmp-long v20, v18, v20

    if-eqz v20, :cond_0

    .line 639
    const/4 v8, 0x1

    .line 640
    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setTid(Ljava/lang/String;)V

    .line 641
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v15, v11, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 643
    .end local v18    # "tid":J
    :catch_0
    move-exception v7

    .line 644
    .local v7, "e":Landroid/database/sqlite/SQLiteConstraintException;
    :try_start_2
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v20

    if-eqz v20, :cond_2

    .line 645
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "bulkInsertInTagDb() TAGS tagType = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", tagValue = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    :cond_2
    invoke-virtual {v15, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    .line 647
    .local v18, "tid":Ljava/lang/Long;
    if-eqz v18, :cond_7

    .line 648
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 649
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "bulkInsertInTagDb() TAGS [FromApp] find already exist key(tid) = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    :cond_3
    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setTid(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 665
    .end local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v5    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .end local v7    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    .end local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    .end local v15    # "tagsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v18    # "tid":Ljava/lang/Long;
    :catch_1
    move-exception v7

    .line 666
    .local v7, "e":Landroid/database/SQLException;
    :try_start_3
    invoke-virtual {v7}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 668
    if-eqz v10, :cond_4

    .line 669
    :try_start_4
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 670
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 672
    :cond_4
    if-eqz v6, :cond_5

    .line 673
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 676
    .end local v7    # "e":Landroid/database/SQLException;
    :cond_5
    :goto_1
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 678
    if-eqz v8, :cond_6

    .line 680
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->syncTagDataToTagsTable()V

    .line 681
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const-string v21, "bulkInsertInTagDb() syncTagDataToTagsTable completed"

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    sget-object v21, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 685
    :cond_6
    return-void

    .line 654
    .restart local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .restart local v5    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .local v7, "e":Landroid/database/sqlite/SQLiteConstraintException;
    .restart local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    .restart local v15    # "tagsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v18    # "tid":Ljava/lang/Long;
    :cond_7
    :try_start_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "tid":Ljava/lang/Long;
    check-cast v18, Ljava/lang/Long;

    .line 655
    .restart local v18    # "tid":Ljava/lang/Long;
    if-eqz v18, :cond_0

    .line 656
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v20

    if-eqz v20, :cond_8

    .line 657
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "bulkInsertInTagDb() TAGS [FromTagDb] find already exist key(tid) = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :cond_8
    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setTid(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 668
    .end local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v5    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .end local v7    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    .end local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    .end local v15    # "tagsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v18    # "tid":Ljava/lang/Long;
    :catchall_0
    move-exception v20

    if-eqz v10, :cond_9

    .line 669
    :try_start_6
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 670
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 672
    :cond_9
    if-eqz v6, :cond_a

    .line 673
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_a
    throw v20

    .line 676
    :catchall_1
    move-exception v20

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v20

    .line 664
    .restart local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .restart local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    .restart local v15    # "tagsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_b
    :try_start_7
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7
    .catch Landroid/database/SQLException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 668
    if-eqz v10, :cond_c

    .line 669
    :try_start_8
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 670
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 672
    :cond_c
    if-eqz v6, :cond_5

    .line 673
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_1
.end method

.method private bulkInsertInTaggingDb(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 22
    .param p2, "APP_TAG"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 718
    .local p1, "fromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    const-string v13, "content_id, tag_id"

    .line 719
    .local v13, "taggingColumns":Ljava/lang/String;
    const-string v16, "INSERT INTO tagging(content_id, tag_id) VALUES (?, ?)"

    .line 721
    .local v16, "taggingSql":Ljava/lang/String;
    const/4 v12, 0x0

    .line 722
    .local v12, "statement":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 724
    .local v8, "db":Landroid/database/sqlite/SQLiteDatabase;
    monitor-enter p0

    .line 729
    :try_start_0
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 730
    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v12

    .line 732
    invoke-virtual/range {p1 .. p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 734
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    const/4 v4, 0x0

    .line 735
    .local v4, "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_7

    .line 736
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 737
    .local v5, "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    check-cast v4, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 740
    .restart local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    :try_start_1
    const-string v17, "-10"

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getId()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_1

    const-string v17, "-10"

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getTid()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 742
    :cond_1
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "bulkInsertInTaggingDb() TAGGING uri = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getContenturi()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", cid = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getId()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", tid = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getTid()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "bulkInsertInTaggingDb() TAGGING data = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getTagData()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 749
    :catch_0
    move-exception v11

    .line 750
    .local v11, "nfe":Ljava/lang/NumberFormatException;
    :try_start_2
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const-string v20, "bulkInsertInTaggingDb() NumberFormatException is occurred"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 769
    .end local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v5    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .end local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    .end local v11    # "nfe":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v9

    .line 770
    .local v9, "e":Landroid/database/SQLException;
    :try_start_3
    invoke-virtual {v9}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 772
    if-eqz v12, :cond_2

    .line 773
    :try_start_4
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 774
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 776
    :cond_2
    if-eqz v8, :cond_3

    .line 777
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 780
    .end local v9    # "e":Landroid/database/SQLException;
    :cond_3
    :goto_1
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 781
    return-void

    .line 747
    .restart local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .restart local v5    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .restart local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    :cond_4
    :try_start_5
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getId()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 748
    .local v6, "cid":J
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->getTid()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-wide v18

    .line 754
    .local v18, "tid":J
    const/16 v17, 0x1

    :try_start_6
    move/from16 v0, v17

    invoke-virtual {v12, v0, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 755
    const/16 v17, 0x2

    move/from16 v0, v17

    move-wide/from16 v1, v18

    invoke-virtual {v12, v0, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 757
    :try_start_7
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v14

    .line 758
    .local v14, "taggingId":J
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 759
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "bulkInsertInTaggingDb() TAGGING taggingId = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", cid = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", tid = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Landroid/database/SQLException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 761
    .end local v14    # "taggingId":J
    :catch_2
    move-exception v9

    .line 762
    .local v9, "e":Landroid/database/sqlite/SQLiteConstraintException;
    :try_start_8
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 763
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "bulkInsertInTaggingDb() TAGGING find already exist - tagType = cid = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", tid = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 772
    .end local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v5    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .end local v6    # "cid":J
    .end local v9    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    .end local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    .end local v18    # "tid":J
    :catchall_0
    move-exception v17

    if-eqz v12, :cond_5

    .line 773
    :try_start_9
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 774
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 776
    :cond_5
    if-eqz v8, :cond_6

    .line 777
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_6
    throw v17

    .line 780
    :catchall_1
    move-exception v17

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v17

    .line 768
    .restart local v4    # "aData":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .restart local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;>;"
    :cond_7
    :try_start_a
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_a
    .catch Landroid/database/SQLException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 772
    if-eqz v12, :cond_8

    .line 773
    :try_start_b
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 774
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 776
    :cond_8
    if-eqz v8, :cond_3

    .line 777
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_1
.end method

.method private createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 108
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 109
    const-string v0, "CREATE TABLE IF NOT EXISTS contents (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,timestamp LONG,contenturi VARCHAR(100) not null UNIQUE,appname VARCHAR(50),filepath TEXT not null DEFAULT \'\');"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 116
    const-string v0, "CREATE TABLE IF NOT EXISTS tagging (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,content_id INTEGER,tag_id INTEGER,foreign key(content_id) REFERENCES contents (_id), foreign key(tag_id) REFERENCES tags (_id) UNIQUE([content_id], [tag_id]) ON CONFLICT IGNORE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 124
    const-string v0, "CREATE TABLE IF NOT EXISTS tags (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,type INTEGER,rawdata TEXT,data TEXT,CONSTRAINT uniqueTag UNIQUE (type,rawdata));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 131
    const-string v0, "CREATE VIRTUAL TABLE IF NOT EXISTS tags_fts USING FTS3 (type,rawdata,data);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 135
    const-string v0, "CREATE TABLE IF NOT EXISTS faces (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,face_id INTEGER,image_id INTEGER,person_id INTEGER,name TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 143
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 144
    return-void
.end method

.method private doRawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 1046
    const-wide/16 v2, 0x0

    .line 1047
    .local v2, "queryTime":J
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1048
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1049
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[PERFORMANCE] doRawQuery() SQL > "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    :cond_0
    const/4 v0, 0x0

    .line 1054
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1055
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1057
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1058
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[PERFORMANCE] doRawQuery() Query\'s data COUNT = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], QueryTime = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v2

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1065
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    :goto_0
    return-object v0

    .line 1061
    :catch_0
    move-exception v4

    .line 1062
    .local v4, "se":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method private declared-synchronized getAllContentData(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "appname"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 193
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT _id, contenturi FROM contents WHERE appname = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' GROUP BY contenturi"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 196
    .local v2, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 197
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 201
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    monitor-exit p0

    return-object v3

    .line 198
    :catch_0
    move-exception v1

    .line 199
    .local v1, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 193
    .end local v1    # "se":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private getAllContentUri(Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 10
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "APP_TAG"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 165
    .local v1, "contentUriMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getAllContentData(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 166
    .local v0, "contentCursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 167
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "getAllContentUri() contentCursor is NULL"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :goto_0
    return-object v1

    .line 171
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getAllContentUri() getAllContentData count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-nez v7, :cond_1

    .line 173
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 174
    const/4 v0, 0x0

    .line 175
    goto :goto_0

    .line 178
    :cond_1
    const-string v7, "_id"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 179
    .local v4, "idx_ID":I
    const-string v7, "contenturi"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 180
    .local v5, "idx_contentID":I
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 181
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 182
    .local v2, "id":J
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 184
    .local v6, "uri":Ljava/lang/String;
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 186
    .end local v2    # "id":J
    .end local v6    # "uri":Ljava/lang/String;
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 187
    const/4 v0, 0x0

    .line 189
    goto :goto_0
.end method

.method private declared-synchronized getAllTagData()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 233
    monitor-enter p0

    :try_start_0
    const-string v2, "SELECT _id, type, data FROM tags"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    .local v2, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 236
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v4, "SELECT _id, type, data FROM tags"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 240
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    monitor-exit p0

    return-object v3

    .line 237
    :catch_0
    move-exception v1

    .line 238
    .local v1, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 233
    .end local v1    # "se":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private getAllTagsData(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 9
    .param p1, "APP_TAG"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 206
    .local v5, "tagsDataMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getAllTagData()Landroid/database/Cursor;

    move-result-object v4

    .line 207
    .local v4, "tagsCursor":Landroid/database/Cursor;
    if-nez v4, :cond_0

    .line 208
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "getAllTagsData() tagsCursor is NULL"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :goto_0
    return-object v5

    .line 212
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAllTagsData() getAllTagData count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-nez v6, :cond_1

    .line 214
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 215
    const/4 v4, 0x0

    .line 216
    goto :goto_0

    .line 219
    :cond_1
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 220
    const-string v6, "_id"

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 221
    .local v0, "id":J
    const-string v6, "type"

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 222
    .local v2, "tagType":I
    const-string v6, "data"

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 224
    .local v3, "tagValue":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v7, v2, 0x7c

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 226
    .end local v0    # "id":J
    .end local v2    # "tagType":I
    .end local v3    # "tagValue":Ljava/lang/String;
    :cond_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 227
    const/4 v4, 0x0

    .line 229
    goto :goto_0
.end method

.method private getAppTag(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 1069
    const-string v2, "\\."

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1070
    .local v1, "splitPackage":[Ljava/lang/String;
    const-string v0, ""

    .line 1071
    .local v0, "APP_TAG":Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 1072
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1075
    :cond_0
    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1079
    const-class v1, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->mInstance:Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    if-nez v0, :cond_0

    .line 1080
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->mInstance:Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    .line 1082
    :cond_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->mInstance:Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1079
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private makeWherePackageList(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 488
    .local p1, "packageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 489
    .local v0, "WHERE":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 490
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 491
    .local v2, "packageName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 492
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NOT (appname=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " OR appname=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 497
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 500
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-object v0
.end method

.method private syncTagDataToTagsTable()V
    .locals 6

    .prologue
    .line 694
    const-string v0, "type, rawdata, data"

    .line 695
    .local v0, "columns":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "INSERT INTO tags_fts("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SELECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "tags"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE NOT EXISTS (SELECT DISTINCT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "tags_fts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE tags_fts.[type] = tags.[type] AND tags_fts.[rawdata] = tags.[rawdata] AND tags_fts.[data] = tags.[data])"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 700
    .local v3, "sql":Ljava/lang/String;
    monitor-enter p0

    .line 701
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 703
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 704
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 705
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 709
    if-eqz v1, :cond_0

    .line 710
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 713
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 714
    return-void

    .line 706
    :catch_0
    move-exception v2

    .line 707
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 709
    if-eqz v1, :cond_0

    .line 710
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .line 713
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 709
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v4

    if-eqz v1, :cond_1

    .line 710
    :try_start_5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_1
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private upgradeDatabaseToVersion2(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 157
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    const-string v1, "upgradeDatabaseToVersion2 start"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    const-string v0, "ALTER TABLE contents ADD COLUMN filepath TEXT not null DEFAULT \'\';"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 160
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    const-string v1, "upgradeDatabaseToVersion2 end"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    return-void
.end method

.method private upgradeTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 147
    const-string v0, "DROP TABLE IF EXISTS contents"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 148
    const-string v0, "DROP TABLE IF EXISTS tagging"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 149
    const-string v0, "DROP TABLE IF EXISTS tags"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 150
    const-string v0, "DROP TABLE IF EXISTS faces"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 151
    const-string v0, "DROP TABLE IF EXISTS tags_fts"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 153
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 154
    return-void
.end method


# virtual methods
.method public bulkDelete(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 12
    .param p2, "appName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 784
    .local p1, "shouldDelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 787
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getAppTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 789
    .local v0, "APP_TAG":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 790
    .local v4, "start":J
    move-wide v6, v4

    .line 791
    .local v6, "total":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "[PERFORMANCE] bulkDelete() START ---------------------------------------------------"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->bulkDeleteInTaggingDb(Ljava/util/ArrayList;Ljava/lang/String;)I

    move-result v3

    .line 795
    .local v3, "delTagging":I
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 796
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[PERFORMANCE] bulkDelete() step 1 end - ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] delete(s) tagging ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v4

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] ms"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 802
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->bulkDeleteInContentDb(Ljava/util/ArrayList;Ljava/lang/String;)I

    move-result v1

    .line 803
    .local v1, "delContent":I
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 804
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[PERFORMANCE] bulkDelete() step 2 end - ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] delete(s) contents ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v4

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] ms"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 810
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->bulkDeleteInTagDb(Ljava/util/ArrayList;Ljava/lang/String;)I

    move-result v2

    .line 811
    .local v2, "delTag":I
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 812
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[PERFORMANCE] bulkDelete() step 3 end - ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] delete(s) tags ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v4

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] ms"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[PERFORMANCE] bulkDelete() TOTAL time for bulkDelete ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v6

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] ms"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public bulkInsert(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 8
    .param p2, "appName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 504
    .local p1, "fromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getAppTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 508
    .local v0, "APP_TAG":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 509
    .local v2, "start":J
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "[PERFORMANCE] bulkInsert() START ---------------------------------------------------"

    invoke-static {v1, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->bulkInsertInContentDb(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->bulkInsertInTagDb(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 516
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->bulkInsertInTaggingDb(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 518
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[PERFORMANCE] bulkInsert() time for bulkInsert ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized bulkInsertOnPeople(JLjava/util/ArrayList;Ljava/lang/String;)V
    .locals 15
    .param p1, "contentID"    # J
    .param p4, "APP_TAG"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 349
    .local p3, "tagIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    monitor-enter p0

    if-nez p3, :cond_1

    .line 389
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 352
    :cond_1
    :try_start_0
    const-string v6, "content_id, tag_id"

    .line 353
    .local v6, "taggingColumns":Ljava/lang/String;
    const-string v7, "INSERT INTO tagging(content_id, tag_id) VALUES (?, ?)"

    .line 355
    .local v7, "taggingSql":Ljava/lang/String;
    const/4 v5, 0x0

    .line 356
    .local v5, "statement":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 359
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 360
    invoke-virtual {v2, v7}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v5

    .line 362
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 363
    .local v10, "tagid":Ljava/lang/Integer;
    const/4 v11, 0x1

    move-wide/from16 v0, p1

    invoke-virtual {v5, v11, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 364
    const/4 v11, 0x2

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v12

    int-to-long v12, v12

    invoke-virtual {v5, v11, v12, v13}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 366
    :try_start_2
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v8

    .line 367
    .local v8, "taggingId":J
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 368
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "People:store() bulkInsertOnPeople [Step.4] TAGGING taggingId = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", cid = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p1

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", tid = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p4

    invoke-static {v0, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 370
    .end local v8    # "taggingId":J
    :catch_0
    move-exception v3

    .line 371
    .local v3, "e":Landroid/database/sqlite/SQLiteConstraintException;
    :try_start_3
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 372
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "People:store() bulkInsertOnPeople [Step.4] TAGGING find already exist - tagType = cid = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p1

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", tid = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p4

    invoke-static {v0, v11}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 378
    .end local v3    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v10    # "tagid":Ljava/lang/Integer;
    :catch_1
    move-exception v3

    .line 379
    .local v3, "e":Landroid/database/SQLException;
    :try_start_4
    invoke-virtual {v3}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 381
    if-eqz v5, :cond_3

    .line 382
    :try_start_5
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 383
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 385
    :cond_3
    if-eqz v2, :cond_0

    .line 386
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 349
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "e":Landroid/database/SQLException;
    .end local v5    # "statement":Landroid/database/sqlite/SQLiteStatement;
    .end local v6    # "taggingColumns":Ljava/lang/String;
    .end local v7    # "taggingSql":Ljava/lang/String;
    :catchall_0
    move-exception v11

    monitor-exit p0

    throw v11

    .line 377
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v5    # "statement":Landroid/database/sqlite/SQLiteStatement;
    .restart local v6    # "taggingColumns":Ljava/lang/String;
    .restart local v7    # "taggingSql":Ljava/lang/String;
    :cond_4
    :try_start_6
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 381
    if-eqz v5, :cond_5

    .line 382
    :try_start_7
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 383
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 385
    :cond_5
    if-eqz v2, :cond_0

    .line 386
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 381
    .end local v4    # "i$":Ljava/util/Iterator;
    :catchall_1
    move-exception v11

    if-eqz v5, :cond_6

    .line 382
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 383
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 385
    :cond_6
    if-eqz v2, :cond_7

    .line 386
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_7
    throw v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method public declared-synchronized checkContentValidRecord(Ljava/lang/String;)J
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 392
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT _id FROM contents WHERE contenturi=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 395
    .local v3, "sql":Ljava/lang/String;
    const-wide/16 v0, -0x1

    .line 397
    .local v0, "contentsID":J
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v4

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 402
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 398
    :catch_0
    move-exception v2

    .line 399
    .local v2, "se":Landroid/database/sqlite/SQLiteDoneException;
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDoneException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 392
    .end local v0    # "contentsID":J
    .end local v2    # "se":Landroid/database/sqlite/SQLiteDoneException;
    .end local v3    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized checkValidRecord(Ljava/lang/String;J)Z
    .locals 8
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "id"    # J

    .prologue
    .line 1028
    monitor-enter p0

    if-nez p1, :cond_1

    .line 1029
    const/4 v0, 0x0

    .line 1042
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 1032
    :cond_1
    const/4 v0, 0x0

    .line 1033
    .local v0, "isValid":Z
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SELECT count(_id) AS recordcnt FROM tagging WHERE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1035
    .local v2, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 1036
    const/4 v0, 0x1

    goto :goto_0

    .line 1038
    :catch_0
    move-exception v1

    .line 1039
    .local v1, "se":Landroid/database/sqlite/SQLiteDoneException;
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDoneException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1028
    .end local v1    # "se":Landroid/database/sqlite/SQLiteDoneException;
    .end local v2    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized getAllContentTagData(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "appname"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 309
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT contents.[_id] AS cid, tags.[_id] AS tid, tags.[type], tags.[data], contents.[contenturi] FROM tags INNER JOIN tagging ON tags.[_id] = tagging.[tag_id] INNER JOIN contents ON tagging.[content_id] = contents.[_id] WHERE contents.[appname] = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' order by contents.[contenturi]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 314
    .local v2, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 315
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 320
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    monitor-exit p0

    return-object v3

    .line 316
    :catch_0
    move-exception v1

    .line 317
    .local v1, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 309
    .end local v1    # "se":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized getDeletedUserTag(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/database/Cursor;
    .locals 9
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 245
    .local p2, "userTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->USERDEF:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 246
    .local v5, "userTagType":Ljava/lang/String;
    const-string v4, "SELECT tags.[_id] AS tags_id, tags.[data], tagging.[_id] AS tagging_id FROM tags INNER JOIN tagging ON tagging.[content_id] = contents.[_id] INNER JOIN contents ON contents.[contenturi] = ? WHERE tags.[_id] = tagging.[tag_id] and tags.[type] = ?"

    .line 251
    .local v4, "sql":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 252
    const-string v1, ""

    .line 253
    .local v1, "existUserTag":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 254
    .local v2, "ite":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 255
    const-string v6, ""

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 256
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " or "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 258
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "tags.[data]=\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 261
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and not ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 262
    const-string v6, "TagDBHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDeletedUserTag() sql = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    .end local v1    # "existUserTag":Ljava/lang/String;
    .end local v2    # "ite":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 267
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object v5, v6, v7

    invoke-virtual {v0, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 274
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_1
    monitor-exit p0

    return-object v6

    .line 270
    :catch_0
    move-exception v3

    .line 271
    .local v3, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 274
    const/4 v6, 0x0

    goto :goto_1

    .line 245
    .end local v3    # "se":Landroid/database/sqlite/SQLiteException;
    .end local v4    # "sql":Ljava/lang/String;
    .end local v5    # "userTagType":Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public declared-synchronized getFaceTagOfContent(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 294
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->PEOPLE:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 295
    .local v1, "faceTagType":Ljava/lang/String;
    const-string v3, "SELECT tagging.[_id] FROM tagging INNER JOIN tags ON tagging.[tag_id]= tags.[_id] WHERE tags.[type]= ? and tagging.[content_id]= ?"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    .local v3, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 299
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 305
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    monitor-exit p0

    return-object v4

    .line 302
    :catch_0
    move-exception v2

    .line 303
    .local v2, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 305
    const/4 v4, 0x0

    goto :goto_0

    .line 294
    .end local v1    # "faceTagType":Ljava/lang/String;
    .end local v2    # "se":Landroid/database/sqlite/SQLiteException;
    .end local v3    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized getMatchedTag(Ljava/lang/String;Ljava/util/ArrayList;Z)Landroid/database/Cursor;
    .locals 6
    .param p1, "keyword"    # Ljava/lang/String;
    .param p3, "bExactlyMatch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 434
    .local p2, "packageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->makeWherePackageList(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v3

    .line 436
    .local v3, "where":Ljava/lang/String;
    move-object v1, p1

    .line 437
    .local v1, "likeKeyword":Ljava/lang/String;
    if-nez p3, :cond_0

    .line 438
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 441
    :cond_0
    if-nez v3, :cond_1

    .line 442
    const-string v2, "SELECT type, rawdata FROM tags WHERE rawdata LIKE ?"

    .line 443
    .local v2, "sql":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v0, v4

    .line 457
    .local v0, "args":[Ljava/lang/String;
    :goto_0
    invoke-direct {p0, v2, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->doRawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    monitor-exit p0

    return-object v4

    .line 447
    .end local v0    # "args":[Ljava/lang/String;
    .end local v2    # "sql":Ljava/lang/String;
    :cond_1
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT tags.[type], tags.[rawdata] FROM tags WHERE tags.[rawdata] LIKE ? AND tags.[_id] IN (SELECT DISTINCT tagging.[tag_id] FROM tagging WHERE tagging.[content_id] IN (SELECT contents.[_id] FROM contents WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND contents.[_id] IN ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SELECT tagging.[content_id] FROM tagging INNER JOIN tags "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ON tagging.[tag_id] = tags.[_id] AND tags.[data] LIKE ?)))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 452
    .restart local v2    # "sql":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v0, v4

    const/4 v4, 0x1

    aput-object v1, v0, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v0    # "args":[Ljava/lang/String;
    goto :goto_0

    .line 434
    .end local v0    # "args":[Ljava/lang/String;
    .end local v1    # "likeKeyword":Ljava/lang/String;
    .end local v2    # "sql":Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized getRelatedTag(Ljava/lang/String;ILjava/util/ArrayList;)Landroid/database/Cursor;
    .locals 8
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 461
    .local p3, "packageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v5

    if-lt p2, v5, :cond_0

    sget-object v5, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->USERDEF:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v5

    if-le p2, v5, :cond_1

    .line 462
    :cond_0
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getRelatedTag() : Wrong tag type = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    const/4 v5, 0x0

    .line 482
    :goto_0
    monitor-exit p0

    return-object v5

    .line 465
    :cond_1
    :try_start_1
    invoke-direct {p0, p3}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->makeWherePackageList(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    .line 467
    .local v4, "where":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SELECT type, rawdata FROM tags WHERE _id IN (SELECT DISTINCT tag_id FROM tagging WHERE content_id IN (SELECT contents.[_id] FROM contents WHERE "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v4, :cond_2

    const-string v5, ""

    :goto_1
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " contents.[_id] IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SELECT tagging.[content_id] FROM tagging INNER JOIN tags "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ON tagging.[tag_id] = tags.[_id] AND tags.[data] LIKE ?))) AND tags.[type] >= ?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 474
    .local v2, "sql":Ljava/lang/String;
    const/4 v1, 0x0

    .line 475
    .local v1, "selectionArgs":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 476
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->values()[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-result-object v5

    aget-object v3, v5, p2

    .line 477
    .local v3, "tagType":Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    if-ne v3, v5, :cond_3

    .line 478
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->PEOPLE:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 482
    :goto_2
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-direct {p0, v2, v5}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->doRawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    .line 467
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "selectionArgs":Ljava/lang/String;
    .end local v2    # "sql":Ljava/lang/String;
    .end local v3    # "tagType":Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " and "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 480
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local v1    # "selectionArgs":Ljava/lang/String;
    .restart local v2    # "sql":Ljava/lang/String;
    .restart local v3    # "tagType":Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    :cond_3
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_2

    .line 461
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "selectionArgs":Ljava/lang/String;
    .end local v2    # "sql":Ljava/lang/String;
    .end local v3    # "tagType":Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .end local v4    # "where":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized getTagInfo(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "tid"    # Ljava/lang/String;

    .prologue
    .line 336
    monitor-enter p0

    :try_start_0
    const-string v2, "SELECT tags.[type], tags.[rawdata] FROM tags WHERE tags.[_id] = ?"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338
    .local v2, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 339
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "SELECT tags.[type], tags.[rawdata] FROM tags WHERE tags.[_id] = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 345
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    monitor-exit p0

    return-object v3

    .line 342
    :catch_0
    move-exception v1

    .line 343
    .local v1, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 345
    const/4 v3, 0x0

    goto :goto_0

    .line 336
    .end local v1    # "se":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized getTaggingCountofContent(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 278
    monitor-enter p0

    :try_start_0
    const-string v2, "SELECT count(*) AS tagging_count FROM tagging INNER JOIN contents ON contents.[contenturi] = ? WHERE tagging.[content_id] = contents.[_id]"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    .local v2, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 284
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 290
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    monitor-exit p0

    return-object v3

    .line 287
    :catch_0
    move-exception v1

    .line 288
    .local v1, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 290
    const/4 v3, 0x0

    goto :goto_0

    .line 278
    .end local v1    # "se":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized getWeatherTagInfo()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 324
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT tags.[_id], tags.[rawdata] FROM tags WHERE tags.[type] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->WEATHER:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 327
    .local v2, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 328
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 332
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    monitor-exit p0

    return-object v3

    .line 329
    :catch_0
    move-exception v1

    .line 330
    .local v1, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 324
    .end local v1    # "se":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 73
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 77
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Upgrading s finder db FROM version "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    packed-switch p2, :pswitch_data_0

    .line 104
    :goto_0
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->upgradeTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 105
    :cond_0
    :goto_1
    return-void

    .line 81
    :pswitch_0
    const/4 v1, 0x1

    if-le p3, v1, :cond_0

    .line 85
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 87
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->upgradeDatabaseToVersion2(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 88
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "ex":Ljava/lang/Throwable;
    :try_start_1
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v0    # "ex":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 79
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 407
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 408
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 410
    const/4 v5, 0x0

    .line 411
    .local v5, "groupBy":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 413
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 415
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    :try_start_1
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 420
    :goto_0
    monitor-exit p0

    return-object v8

    .line 416
    :catch_0
    move-exception v9

    .line 417
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 407
    .end local v0    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "groupBy":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized rawBulkDelete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 425
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 426
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 430
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    monitor-exit p0

    return v2

    .line 427
    :catch_0
    move-exception v1

    .line 428
    .local v1, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 430
    const/4 v2, -0x1

    goto :goto_0

    .line 425
    .end local v1    # "se":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
