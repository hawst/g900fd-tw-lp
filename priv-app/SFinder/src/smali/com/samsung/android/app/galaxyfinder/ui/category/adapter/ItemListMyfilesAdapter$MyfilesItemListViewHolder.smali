.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;
.super Ljava/lang/Object;
.source "ItemListMyfilesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyfilesItemListViewHolder"
.end annotation


# instance fields
.field public ivIcon:Landroid/widget/ImageView;

.field public ivOverlayIcon:Landroid/widget/ImageView;

.field public ivTask:Landroid/widget/ImageView;

.field public tvContent:Landroid/widget/TextView;

.field public tvName:Landroid/widget/TextView;

.field public vgCtrlGroup:Landroid/view/ViewGroup;

.field public vgMainGroup:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 263
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->tvContent:Landroid/widget/TextView;

    .line 265
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 267
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivOverlayIcon:Landroid/widget/ImageView;

    .line 269
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    .line 271
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    .line 273
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    return-void
.end method
