.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListContactAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;
    }
.end annotation


# static fields
.field private static final TARGET_TYPE_ADDRESS:I = 0x4

.field private static final TARGET_TYPE_BIRTHDAY:I = 0x5

.field private static final TARGET_TYPE_EMAIL:I = 0x2

.field private static final TARGET_TYPE_IM:I = 0x7

.field private static final TARGET_TYPE_NAME:I = 0x0

.field private static final TARGET_TYPE_NICKNAME:I = 0x9

.field private static final TARGET_TYPE_NOTE:I = 0x8

.field private static final TARGET_TYPE_ORGANIZATION:I = 0x6

.field private static final TARGET_TYPE_PHONE:I = 0x1

.field private static final TARGET_TYPE_URL:I = 0x3


# instance fields
.field private bNotSupportCall:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->bNotSupportCall:Z

    .line 47
    return-void
.end method

.method private getReplyActionIcon(I)I
    .locals 1
    .param p1, "typeIndex"    # I

    .prologue
    const v0, 0x7f0200f0

    .line 195
    packed-switch p1, :pswitch_data_0

    .line 212
    :goto_0
    :pswitch_0
    return v0

    .line 200
    :pswitch_1
    const v0, 0x7f0200f7

    goto :goto_0

    .line 203
    :pswitch_2
    const v0, 0x7f0200f3

    goto :goto_0

    .line 206
    :pswitch_3
    const v0, 0x7f0200f4

    goto :goto_0

    .line 209
    :pswitch_4
    const v0, 0x7f0200f1

    goto :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 21
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 51
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v10

    .line 52
    .local v10, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v7, 0x0

    .line 54
    .local v7, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 56
    if-nez p2, :cond_5

    .line 57
    sget-object v17, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v18, 0x7f030036

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 59
    new-instance v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;

    .end local v7    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;
    invoke-direct {v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;-><init>()V

    .line 61
    .restart local v7    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;
    const v17, 0x7f0b0054

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 62
    const v17, 0x7f0b0059

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    .line 63
    const v17, 0x7f0b004e

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 64
    const v17, 0x7f0b005b

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    .line 65
    const v17, 0x7f0b0058

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup;

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    .line 66
    const v17, 0x7f0b005a

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup;

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    .line 68
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 73
    :goto_0
    invoke-virtual {v10}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v11

    .line 74
    .local v11, "name":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v5

    .line 76
    .local v5, "desc":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v8

    .line 77
    .local v8, "icon":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, "bgColor":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v14

    .line 80
    .local v14, "target":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v9

    .line 82
    .local v9, "intent":Landroid/content/Intent;
    const/4 v15, 0x0

    .line 83
    .local v15, "targetType":I
    const/4 v4, -0x1

    .line 84
    .local v4, "color":I
    const/16 v16, -0x1

    .line 86
    .local v16, "taskType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isWifiDevice(Landroid/content/Context;)Z

    move-result v17

    if-nez v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isVoiceCapable(Landroid/content/Context;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 87
    :cond_0
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->bNotSupportCall:Z

    .line 91
    :cond_1
    :try_start_0
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v15

    .line 96
    :goto_1
    if-eqz v3, :cond_2

    .line 98
    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    .line 104
    :cond_2
    :goto_2
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v11, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 105
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v5, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 107
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v17

    if-nez v17, :cond_3

    .line 108
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->setImageViewForContactRoundedStyle(Landroid/widget/ImageView;Ljava/lang/String;I)V

    .line 111
    :cond_3
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 112
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 114
    packed-switch v15, :pswitch_data_0

    .line 174
    :goto_3
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 176
    .local v13, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 178
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    .end local v13    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :goto_4
    const/16 v17, -0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_4

    .line 183
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 184
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->getReplyActionIcon(I)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 186
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v5, v2, v15}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->setTaskSuggestionAction(Landroid/view/View;Ljava/lang/String;II)Z

    .line 189
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-virtual {v0, v1, v9, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 191
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    return-object v17

    .line 70
    .end local v3    # "bgColor":Ljava/lang/String;
    .end local v4    # "color":I
    .end local v5    # "desc":Ljava/lang/String;
    .end local v8    # "icon":Ljava/lang/String;
    .end local v9    # "intent":Landroid/content/Intent;
    .end local v11    # "name":Ljava/lang/String;
    .end local v14    # "target":Ljava/lang/String;
    .end local v15    # "targetType":I
    .end local v16    # "taskType":I
    :cond_5
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;
    check-cast v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;

    .restart local v7    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;
    goto/16 :goto_0

    .line 92
    .restart local v3    # "bgColor":Ljava/lang/String;
    .restart local v4    # "color":I
    .restart local v5    # "desc":Ljava/lang/String;
    .restart local v8    # "icon":Ljava/lang/String;
    .restart local v9    # "intent":Landroid/content/Intent;
    .restart local v11    # "name":Ljava/lang/String;
    .restart local v14    # "target":Ljava/lang/String;
    .restart local v15    # "targetType":I
    .restart local v16    # "taskType":I
    :catch_0
    move-exception v6

    .line 93
    .local v6, "e":Ljava/lang/NumberFormatException;
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 99
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v6

    .line 100
    .restart local v6    # "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v6}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_2

    .line 116
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->bNotSupportCall:Z

    move/from16 v17, v0

    if-nez v17, :cond_6

    .line 117
    const/16 v16, 0x0

    .line 118
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e0024

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x20

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e000c

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 122
    :cond_6
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 124
    .restart local v13    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 126
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    .line 131
    .end local v13    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :pswitch_1
    const/16 v16, 0x3

    .line 132
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e0026

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x20

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e000c

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 138
    :pswitch_2
    const/16 v16, 0x4

    .line 139
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e0023

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x20

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e000c

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 145
    :pswitch_3
    const/16 v16, 0x9

    .line 146
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e0025

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x20

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e000c

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 152
    :pswitch_4
    const/16 v16, 0x8

    .line 153
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e0027

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x20

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0e000c

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 162
    :pswitch_5
    const/16 v16, -0x1

    .line 163
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 165
    .local v12, "param":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 167
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    .line 171
    .end local v12    # "param":Landroid/view/ViewGroup$MarginLayoutParams;
    :pswitch_6
    iget-object v0, v7, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListContactAdapter$ContactItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method
