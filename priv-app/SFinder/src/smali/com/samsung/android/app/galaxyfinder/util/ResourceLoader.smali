.class public Lcom/samsung/android/app/galaxyfinder/util/ResourceLoader;
.super Ljava/lang/Object;
.source "ResourceLoader.java"


# static fields
.field private static final RES_TYPE_DRAWABLE:I = 0x1

.field private static final RES_TYPE_STRING:I

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/samsung/android/app/galaxyfinder/util/ResourceLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/ResourceLoader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getResourcesDrawable(Landroid/content/Context;Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packagename"    # Ljava/lang/String;
    .param p2, "id"    # I

    .prologue
    .line 20
    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v1}, Lcom/samsung/android/app/galaxyfinder/util/ResourceLoader;->getResourcesObject(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/Object;

    move-result-object v0

    .line 22
    .local v0, "obj":Ljava/lang/Object;
    if-eqz v0, :cond_0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .end local v0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "obj":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getResourcesObject(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/Object;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packagename"    # Ljava/lang/String;
    .param p2, "id"    # I
    .param p3, "type"    # I

    .prologue
    .line 32
    const/4 v1, 0x0

    .line 36
    .local v1, "obj":Ljava/lang/Object;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 38
    .local v2, "res":Landroid/content/res/Resources;
    if-eqz v2, :cond_0

    .line 39
    packed-switch p3, :pswitch_data_0

    .line 56
    .end local v1    # "obj":Ljava/lang/Object;
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_0
    :goto_0
    return-object v1

    .line 41
    .restart local v1    # "obj":Ljava/lang/Object;
    .restart local v2    # "res":Landroid/content/res/Resources;
    :pswitch_0
    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 42
    .local v1, "obj":Landroid/graphics/drawable/Drawable;
    goto :goto_0

    .line 45
    .local v1, "obj":Ljava/lang/Object;
    :pswitch_1
    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .local v1, "obj":Ljava/lang/String;
    goto :goto_0

    .line 49
    .end local v2    # "res":Landroid/content/res/Resources;
    .local v1, "obj":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/util/ResourceLoader;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getResourcesObject : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", type : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 53
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getResourcesString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packagename"    # Ljava/lang/String;
    .param p2, "id"    # I

    .prologue
    .line 26
    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1}, Lcom/samsung/android/app/galaxyfinder/util/ResourceLoader;->getResourcesObject(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/Object;

    move-result-object v0

    .line 28
    .local v0, "obj":Ljava/lang/Object;
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/String;

    .end local v0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "obj":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
