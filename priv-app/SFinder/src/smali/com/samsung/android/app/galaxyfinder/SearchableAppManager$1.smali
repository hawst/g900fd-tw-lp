.class Lcom/samsung/android/app/galaxyfinder/SearchableAppManager$1;
.super Ljava/lang/Object;
.source "SearchableAppManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/app/SearchableInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager$1;->this$0:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/app/SearchableInfo;Landroid/app/SearchableInfo;)I
    .locals 6
    .param p1, "object1"    # Landroid/app/SearchableInfo;
    .param p2, "object2"    # Landroid/app/SearchableInfo;

    .prologue
    .line 412
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager$1;->this$0:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getLabelId()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getApplicationLabel(Landroid/content/ComponentName;I)Ljava/lang/String;

    move-result-object v1

    .line 413
    .local v1, "label1":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager$1;->this$0:Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getLabelId()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getApplicationLabel(Landroid/content/ComponentName;I)Ljava/lang/String;

    move-result-object v2

    .line 414
    .local v2, "label2":Ljava/lang/String;
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    .line 415
    .local v0, "collator":Ljava/text/Collator;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/text/Collator;->setStrength(I)V

    .line 416
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 409
    check-cast p1, Landroid/app/SearchableInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/app/SearchableInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager$1;->compare(Landroid/app/SearchableInfo;Landroid/app/SearchableInfo;)I

    move-result v0

    return v0
.end method
