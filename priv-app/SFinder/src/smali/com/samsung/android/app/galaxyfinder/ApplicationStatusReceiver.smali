.class public Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ApplicationStatusReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$ReceiveType;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 22
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->TAG:Ljava/lang/String;

    .line 24
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->mHandler:Landroid/os/Handler;

    .line 116
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->checkSLinkPackagee(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private checkSLinkPackagee(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 85
    const-string v2, "com.samsung.android.sdk.samsunglink"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->TAG:Ljava/lang/String;

    const-string v3, "[Slink platform] package changed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    .line 91
    .local v0, "app":Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;
    invoke-virtual {v0, p2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->initStateSLinkLocationService(Ljava/lang/String;)V

    .line 93
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 94
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->useSLinkLocationService()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "com.samsung.android.app.galaxyfinder.tag.start_slink_locationservice"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 100
    .end local v1    # "i":Landroid/content/Intent;
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    .restart local v1    # "i":Landroid/content/Intent;
    const-string v2, "com.samsung.android.app.galaxyfinder.tag.stop_slink_locationservice"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 108
    if-nez p2, :cond_0

    .line 114
    :goto_0
    return-void

    .line 111
    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 112
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$ReceiveType;

    invoke-direct {v1, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$ReceiveType;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 113
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
