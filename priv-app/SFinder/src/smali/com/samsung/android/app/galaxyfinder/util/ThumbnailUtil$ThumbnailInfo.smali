.class public Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;
.super Ljava/lang/Object;
.source "ThumbnailUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThumbnailInfo"
.end annotation


# instance fields
.field public mFileType:I

.field public mIconImageView:Landroid/widget/ImageView;

.field public mIconUri:Landroid/net/Uri;

.field public mKey:Ljava/lang/String;

.field public mMimeType:Ljava/lang/String;

.field public mOverlayIconImageView:Landroid/widget/ImageView;

.field public mPath:Ljava/lang/String;

.field public mThumbnailDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field public mThumbnailInfoContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
