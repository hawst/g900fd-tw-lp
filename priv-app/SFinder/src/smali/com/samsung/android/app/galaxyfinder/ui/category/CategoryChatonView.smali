.class public Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
.source "CategoryChatonView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    .locals 6

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->getDefaultItemCount()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListChatonAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    return-object v0
.end method

.method protected getDefaultItemCount()I
    .locals 2

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method protected getTemplateLayoutId()I
    .locals 1

    .prologue
    .line 72
    const v0, 0x7f03001a

    return v0
.end method

.method protected init(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const/4 v0, 0x0

    .line 25
    .local v0, "count":I
    const/4 v2, -0x1

    .line 26
    .local v2, "marker":I
    const/4 v3, -0x1

    .line 27
    .local v3, "prevMarker":I
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 29
    .local v4, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 30
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 32
    const/4 v5, -0x1

    if-eq v2, v5, :cond_0

    .line 33
    move v3, v2

    .line 34
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setIndicationOfFirstItem(Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x0

    .line 42
    :cond_0
    move v2, v1

    .line 43
    add-int/lit8 v0, v0, 0x1

    .line 29
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 45
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 48
    :cond_2
    if-eq v2, v3, :cond_3

    .line 49
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setIndicationOfFirstItem(Ljava/lang/String;)V

    .line 56
    :cond_3
    invoke-super {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->init(Landroid/content/Context;)V

    .line 57
    return-void
.end method
