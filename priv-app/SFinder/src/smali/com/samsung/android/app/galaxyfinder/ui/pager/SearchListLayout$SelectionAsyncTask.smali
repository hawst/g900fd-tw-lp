.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;
.super Landroid/os/AsyncTask;
.source "SearchListLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public mCallback:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;)V
    .locals 1
    .param p2, "callback"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;

    .prologue
    .line 1972
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1970
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;

    .line 1973
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;

    .line 1974
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1968
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 7
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 1978
    const/4 v3, 0x0

    .line 1979
    .local v3, "itemsMap":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectableCategoriesView()Ljava/util/List;
    invoke-static {v6}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/util/List;

    move-result-object v5

    .line 1981
    .local v5, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-eqz v5, :cond_1

    .line 1982
    new-instance v3, Ljava/util/LinkedHashMap;

    .end local v3    # "itemsMap":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1984
    .restart local v3    # "itemsMap":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .local v4, "view":Landroid/view/View;
    move-object v0, v4

    .line 1985
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 1986
    .local v0, "baseView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getSelectedItems()Ljava/util/List;

    move-result-object v2

    .line 1988
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 1989
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v6

    invoke-virtual {v3, v6, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1994
    .end local v0    # "baseView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    .end local v4    # "view":Landroid/view/View;
    :cond_1
    return-object v3
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1999
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;

    if-eqz v0, :cond_0

    .line 2000
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;->onRetrieveSelectedItems(Ljava/lang/Object;)V

    .line 2003
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2004
    return-void
.end method
