.class public Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
.super Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;
.source "SuggestionCategoryInfo.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mFilteredItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mOrder:I

.field private mTagSelectionFilter:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private nameOrderSort:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryBaseInfo;-><init>(Landroid/content/Context;)V

    .line 25
    const-class v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mFilteredItemList:Ljava/util/ArrayList;

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    .line 35
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mOrder:I

    .line 613
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo$1;-><init>(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->nameOrderSort:Ljava/util/Comparator;

    .line 39
    return-void
.end method

.method private addItem(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_0
    return-void
.end method

.method private getActionIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "extra"    # Ljava/lang/String;

    .prologue
    .line 468
    const/4 v0, 0x0

    .line 470
    .local v0, "intent":Landroid/content/Intent;
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getBaseIntent()Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_0

    .line 471
    const/4 v2, 0x0

    .line 505
    :goto_0
    return-object v2

    .line 474
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getBaseIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_5

    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getBaseIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 476
    .restart local v0    # "intent":Landroid/content/Intent;
    :goto_1
    if-eqz p1, :cond_1

    .line 477
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 480
    :cond_1
    if-eqz p2, :cond_7

    .line 481
    const/4 v1, 0x0

    .line 482
    .local v1, "uri":Landroid/net/Uri;
    if-eqz p3, :cond_6

    .line 483
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, p3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 487
    :goto_2
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 495
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_2
    :goto_3
    if-eqz p4, :cond_3

    .line 496
    const-string v2, "intent_extra_data_key"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 499
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getQuery()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 500
    const-string v2, "query"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 503
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 504
    const v2, 0x10008000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object v2, v0

    .line 505
    goto :goto_0

    .line 474
    :cond_5
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    goto :goto_1

    .line 485
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_6
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_2

    .line 489
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_7
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz p3, :cond_2

    .line 490
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, p3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 491
    .restart local v1    # "uri":Landroid/net/Uri;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_3
.end method

.method private setValidItems()V
    .locals 5

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 151
    .local v2, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionDataToShare()Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "data":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionMimeType()Ljava/lang/String;

    move-result-object v3

    .line 154
    .local v3, "mimetype":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mContext:Landroid/content/Context;

    invoke-static {v4, v0, v3}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isDRMContent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 156
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setValidity(Z)V

    goto :goto_0

    .line 158
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setValidity(Z)V

    goto :goto_0

    .line 162
    .end local v0    # "data":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v3    # "mimetype":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public clearTagFilter()V
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 541
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 543
    :cond_0
    return-void
.end method

.method public getAllTags()Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 546
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 548
    .local v0, "allTags":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->supportTagSearch()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 549
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 550
    .local v3, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getAllTags()Ljava/util/HashMap;

    move-result-object v6

    .line 551
    .local v6, "tags":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 552
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 553
    .local v4, "key":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 555
    .local v5, "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 556
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagCount()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->addTagCount(I)V

    .line 559
    :cond_1
    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 564
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .end local v6    # "tags":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    :cond_2
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v0, 0x0

    .end local v0    # "allTags":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    :cond_3
    return-object v0
.end method

.method public getCheckedItems()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    .line 98
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    const/4 v0, 0x0

    .line 100
    .local v0, "checkedItems":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 101
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 102
    .local v2, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isValid()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 103
    if-nez v0, :cond_1

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "checkedItems":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .restart local v0    # "checkedItems":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    :cond_1
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_2
    return-object v0
.end method

.method public getItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mFilteredItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mOrder:I

    return v0
.end method

.method protected makeCategorizedSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 575
    .local v0, "categorizedSuggestionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 577
    .local v11, "suggestionSortMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 579
    .local v4, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v6

    .line 580
    .local v6, "keyValue":Ljava/lang/String;
    if-nez v6, :cond_0

    .line 581
    const-string v6, ""

    .line 584
    :cond_0
    invoke-virtual {v11, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 585
    .local v7, "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    if-nez v7, :cond_1

    .line 586
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 589
    .restart local v7    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    :cond_1
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 590
    invoke-virtual {v11, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 593
    .end local v4    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v6    # "keyValue":Ljava/lang/String;
    .end local v7    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    :cond_2
    invoke-virtual {v11}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v8

    .line 594
    .local v8, "sortKeySet":[Ljava/lang/Object;
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->nameOrderSort:Ljava/util/Comparator;

    invoke-static {v8, v12}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 596
    array-length v9, v8

    .line 598
    .local v9, "sortKeySetSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v9, :cond_4

    .line 599
    aget-object v10, v8, v2

    check-cast v10, Ljava/lang/String;

    .line 600
    .local v10, "sortKeyValue":Ljava/lang/String;
    invoke-virtual {v11, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 601
    .restart local v7    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    const/4 v12, 0x0

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 602
    .local v1, "categoryHeaderItem":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v1, v10}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setIndicationOfFirstItem(Ljava/lang/String;)V

    .line 603
    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setIndicationIndex(I)V

    .line 604
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 605
    .local v5, "itemInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v5, v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setIndicationIndex(I)V

    goto :goto_2

    .line 608
    .end local v5    # "itemInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_3
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 598
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 610
    .end local v1    # "categoryHeaderItem":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v7    # "sortColumn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    .end local v10    # "sortKeyValue":Ljava/lang/String;
    :cond_4
    return-object v0
.end method

.method public replaceItems(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->updateFilteredItems()V

    .line 68
    return-void
.end method

.method public setCheckedAllItems(ZZ)I
    .locals 5
    .param p1, "checked"    # Z
    .param p2, "onlyValid"    # Z

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 77
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    .line 79
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 80
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 81
    .local v2, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    if-eqz p2, :cond_1

    .line 82
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isValid()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 83
    invoke-virtual {v2, p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setChecked(Z)V

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_1
    invoke-virtual {v2, p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setChecked(Z)V

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_2
    return v0
.end method

.method public setData(Landroid/database/Cursor;)V
    .locals 62
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 172
    if-eqz p1, :cond_1d

    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v57

    if-eqz v57, :cond_1d

    .line 173
    const/4 v10, 0x0

    .line 174
    .local v10, "hasSortKeyword":Z
    const-wide/16 v48, 0x0

    .line 175
    .local v48, "processTime":J
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_1f

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    const-string v58, "setData : preparing ----------------------------------------------------"

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v48

    .line 182
    :goto_0
    const-string v57, "suggest_text_1"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 183
    .local v33, "idx_text1":I
    const-string v57, "suggest_text_2"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    .line 184
    .local v34, "idx_text2":I
    const-string v57, "suggest_text_3"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    .line 185
    .local v35, "idx_text3":I
    const-string v57, "suggest_text_4"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    .line 186
    .local v36, "idx_text4":I
    const-string v57, "suggest_text_5"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v37

    .line 188
    .local v37, "idx_text5":I
    const-string v57, "suggest_icon_1"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 189
    .local v19, "idx_icon1":I
    const-string v57, "suggest_icon_2"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 190
    .local v20, "idx_icon2":I
    const-string v57, "suggest_icon_3"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 191
    .local v21, "idx_icon3":I
    const-string v57, "suggest_icon_4"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 193
    .local v22, "idx_icon4":I
    const-string v57, "orientation"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 194
    .local v30, "idx_orientation":I
    const-string v57, "suggest_icon_1_blob"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 195
    .local v23, "idx_icon_blob":I
    const-string v57, "suggest_intent_action"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 196
    .local v25, "idx_intent_action":I
    const-string v57, "suggest_intent_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 197
    .local v26, "idx_intent_data":I
    const-string v57, "suggest_intent_data_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 198
    .local v27, "idx_intent_data_id":I
    const-string v57, "suggest_intent_extra_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 199
    .local v28, "idx_intent_extra":I
    const-string v57, "suggest_uri"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v38

    .line 200
    .local v38, "idx_uri":I
    const-string v57, "suggest_data_to_share"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 201
    .local v31, "idx_share":I
    const-string v57, "suggest_mime_type"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 202
    .local v29, "idx_mime_type":I
    const-string v57, "suggest_target_type"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 203
    .local v32, "idx_target_type":I
    const-string v57, "suggest_group"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 204
    .local v17, "idx_group":I
    const-string v57, "suggest_extra_flags"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 205
    .local v16, "idx_extra_flag":I
    const-string v57, "suggest_ink_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 206
    .local v24, "idx_ink":I
    const-string v57, "highlight_content"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 209
    .local v18, "idx_highlight":I
    :cond_0
    const/16 v50, 0x0

    .local v50, "text1":Ljava/lang/String;
    const/16 v51, 0x0

    .local v51, "text2":Ljava/lang/String;
    const/16 v52, 0x0

    .local v52, "text3":Ljava/lang/String;
    const/16 v53, 0x0

    .local v53, "text4":Ljava/lang/String;
    const/16 v54, 0x0

    .local v54, "text5":Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "icon1":Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "icon2":Ljava/lang/String;
    const/4 v14, 0x0

    .local v14, "icon3":Ljava/lang/String;
    const/4 v15, 0x0

    .line 210
    .local v15, "icon4":Ljava/lang/String;
    const/16 v56, 0x0

    .local v56, "uri":Ljava/lang/String;
    const/16 v46, 0x0

    .local v46, "mimetype":Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "dataToShare":Ljava/lang/String;
    const/16 v55, 0x0

    .local v55, "type":Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "group":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "extraflags":Ljava/lang/String;
    const/16 v39, 0x0

    .local v39, "inkData":Ljava/lang/String;
    const/4 v11, 0x0

    .line 211
    .local v11, "highlight":Ljava/lang/String;
    const/4 v6, 0x0

    .line 212
    .local v6, "blob":[B
    const/16 v47, 0x0

    .line 214
    .local v47, "orientation":I
    if-ltz v33, :cond_1

    .line 215
    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v50

    .line 216
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_1

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "text1 : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_1
    if-ltz v34, :cond_2

    .line 222
    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v51

    .line 223
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_2

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "text2 : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_2
    if-ltz v35, :cond_3

    .line 229
    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v52

    .line 230
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_3

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "text3 : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_3
    if-ltz v36, :cond_4

    .line 236
    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v53

    .line 237
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_4

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "text4 : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_4
    if-ltz v37, :cond_5

    .line 244
    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v54

    .line 245
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_5

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "text5 : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_5
    if-ltz v19, :cond_6

    .line 251
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 252
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_6

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "icon1 : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_6
    if-ltz v20, :cond_7

    .line 258
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 259
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_7

    .line 260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "icon2 : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_7
    if-ltz v21, :cond_8

    .line 264
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 265
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_8

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "icon3 : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_8
    if-ltz v22, :cond_9

    .line 271
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 272
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_9

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "icon4 : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_9
    if-ltz v30, :cond_a

    .line 284
    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v47

    .line 285
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_a

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "orientation : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :cond_a
    if-ltz v23, :cond_b

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getType(I)I

    move-result v57

    const/16 v58, 0x4

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_b

    .line 292
    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 293
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_b

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v58, v0

    new-instance v57, Ljava/lang/StringBuilder;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "blob : "

    move-object/from16 v0, v57

    move-object/from16 v1, v59

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v59

    if-eqz v6, :cond_21

    const-string v57, "has value"

    :goto_1
    move-object/from16 v0, v59

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, v58

    move-object/from16 v1, v57

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :cond_b
    const/16 v41, 0x0

    .local v41, "intentActionStr":Ljava/lang/String;
    const/16 v42, 0x0

    .local v42, "intentData":Ljava/lang/String;
    const/16 v43, 0x0

    .local v43, "intentDataId":Ljava/lang/String;
    const/16 v44, 0x0

    .line 300
    .local v44, "intentExtraData":Ljava/lang/String;
    if-ltz v25, :cond_c

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getType(I)I

    move-result v57

    const/16 v58, 0x3

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_c

    .line 302
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v41

    .line 303
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_c

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "intentActionStr : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_c
    if-ltz v26, :cond_d

    .line 309
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    .line 310
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_d

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "intentData : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_d
    if-ltz v27, :cond_e

    .line 316
    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v43

    .line 317
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_e

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "intentDataId : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_e
    if-ltz v28, :cond_f

    .line 323
    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v44

    .line 324
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_f

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "intentExtraData : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :cond_f
    if-ltz v38, :cond_10

    .line 330
    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v56

    .line 331
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_10

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "uri : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_10
    if-ltz v31, :cond_11

    .line 337
    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 338
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_11

    .line 339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "dataToShare : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_11
    if-nez v7, :cond_12

    .line 347
    move-object/from16 v7, v56

    .line 350
    :cond_12
    if-ltz v29, :cond_14

    .line 351
    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v46

    .line 353
    if-eqz v46, :cond_13

    if-eqz v7, :cond_13

    const-string v57, "file"

    move-object/from16 v0, v57

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v57

    if-eqz v57, :cond_13

    .line 355
    move-object/from16 v0, v46

    invoke-static {v7, v0}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->getShareMimeType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    .line 358
    :cond_13
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_14

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "mimetype : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :cond_14
    if-ltz v32, :cond_15

    .line 364
    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v55

    .line 365
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_15

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "type : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    :cond_15
    if-ltz v17, :cond_16

    .line 371
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 372
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_16

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "group : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    :cond_16
    if-ltz v16, :cond_17

    .line 378
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 379
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_17

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "extraflags : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_17
    if-ltz v24, :cond_18

    .line 385
    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v39

    .line 386
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_18

    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "inkData : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_18
    if-ltz v18, :cond_19

    .line 392
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 393
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_19

    .line 394
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "highlight_content : "

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_19
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    move-object/from16 v2, v42

    move-object/from16 v3, v43

    move-object/from16 v4, v44

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getActionIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v40

    .line 401
    .local v40, "intent":Landroid/content/Intent;
    if-eqz v40, :cond_1a

    .line 402
    const-string v57, "intent_extra_from"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mContext:Landroid/content/Context;

    move-object/from16 v58, v0

    invoke-virtual/range {v58 .. v58}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v58

    move-object/from16 v0, v40

    move-object/from16 v1, v57

    move-object/from16 v2, v58

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 403
    const-string v57, "intent_extra_target_type"

    move-object/from16 v0, v40

    move-object/from16 v1, v57

    move-object/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 406
    :cond_1a
    new-instance v45, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    invoke-direct/range {v45 .. v45}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;-><init>()V

    .line 408
    .local v45, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getQuery()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, v45

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setBaseQuery(Ljava/lang/String;)V

    .line 409
    move-object/from16 v0, v45

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionText1(Ljava/lang/String;)V

    .line 410
    move-object/from16 v0, v45

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionText2(Ljava/lang/String;)V

    .line 411
    move-object/from16 v0, v45

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionText3(Ljava/lang/String;)V

    .line 412
    move-object/from16 v0, v45

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionText4(Ljava/lang/String;)V

    .line 413
    move-object/from16 v0, v45

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionText5(Ljava/lang/String;)V

    .line 414
    move-object/from16 v0, v45

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIcon1(Ljava/lang/String;)V

    .line 415
    move-object/from16 v0, v45

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIcon2(Ljava/lang/String;)V

    .line 416
    move-object/from16 v0, v45

    invoke-virtual {v0, v14}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIcon3(Ljava/lang/String;)V

    .line 417
    move-object/from16 v0, v45

    invoke-virtual {v0, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIcon4(Ljava/lang/String;)V

    .line 418
    move-object/from16 v0, v45

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIconOrientation(I)V

    .line 419
    move-object/from16 v0, v45

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIntentAction(Ljava/lang/String;)V

    .line 420
    move-object/from16 v0, v45

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIntentData(Ljava/lang/String;)V

    .line 421
    move-object/from16 v0, v45

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIntentDataId(Ljava/lang/String;)V

    .line 422
    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIntentExtraData(Ljava/lang/String;)V

    .line 423
    move-object/from16 v0, v45

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionActionIntent(Landroid/content/Intent;)V

    .line 424
    move-object/from16 v0, v45

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionUri(Ljava/lang/String;)V

    .line 425
    move-object/from16 v0, v45

    invoke-virtual {v0, v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionDataToShare(Ljava/lang/String;)V

    .line 426
    invoke-virtual/range {v45 .. v46}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionMimeType(Ljava/lang/String;)V

    .line 427
    move-object/from16 v0, v45

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionTargetType(Ljava/lang/String;)V

    .line 428
    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionIconBlob([B)V

    .line 429
    move-object/from16 v0, v45

    invoke-virtual {v0, v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionGroup(Ljava/lang/String;)V

    .line 430
    move-object/from16 v0, v45

    invoke-virtual {v0, v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionExtraFlags(Ljava/lang/String;)V

    .line 431
    move-object/from16 v0, v45

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionInkData(Ljava/lang/String;)V

    .line 432
    move-object/from16 v0, v45

    invoke-virtual {v0, v11}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionContentHighLight(Ljava/lang/String;)V

    .line 433
    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->addItem(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;)V

    .line 440
    if-nez v10, :cond_1b

    if-eqz v9, :cond_1b

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v57

    if-nez v57, :cond_1b

    .line 441
    const/4 v10, 0x1

    .line 443
    :cond_1b
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v57

    if-nez v57, :cond_0

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    move-object/from16 v57, v0

    if-eqz v57, :cond_1c

    if-eqz v10, :cond_1c

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->isSortedAlready()Z

    move-result v57

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_1c

    .line 446
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    move-object/from16 v57, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->makeCategorizedSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v57

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setItems(Ljava/util/ArrayList;)V

    .line 449
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->updateFilteredItems()V

    .line 450
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setValidItems()V

    .line 452
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v57

    if-eqz v57, :cond_22

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "setData : completed - PERFORMANCE Query\'s data COUNT = ["

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v59

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v58

    const-string v59, "], QueryTime = ["

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v60

    sub-long v60, v60, v48

    move-object/from16 v0, v58

    move-wide/from16 v1, v60

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v58

    const-string v59, " ms]"

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    .end local v6    # "blob":[B
    .end local v7    # "dataToShare":Ljava/lang/String;
    .end local v8    # "extraflags":Ljava/lang/String;
    .end local v9    # "group":Ljava/lang/String;
    .end local v10    # "hasSortKeyword":Z
    .end local v11    # "highlight":Ljava/lang/String;
    .end local v12    # "icon1":Ljava/lang/String;
    .end local v13    # "icon2":Ljava/lang/String;
    .end local v14    # "icon3":Ljava/lang/String;
    .end local v15    # "icon4":Ljava/lang/String;
    .end local v16    # "idx_extra_flag":I
    .end local v17    # "idx_group":I
    .end local v18    # "idx_highlight":I
    .end local v19    # "idx_icon1":I
    .end local v20    # "idx_icon2":I
    .end local v21    # "idx_icon3":I
    .end local v22    # "idx_icon4":I
    .end local v23    # "idx_icon_blob":I
    .end local v24    # "idx_ink":I
    .end local v25    # "idx_intent_action":I
    .end local v26    # "idx_intent_data":I
    .end local v27    # "idx_intent_data_id":I
    .end local v28    # "idx_intent_extra":I
    .end local v29    # "idx_mime_type":I
    .end local v30    # "idx_orientation":I
    .end local v31    # "idx_share":I
    .end local v32    # "idx_target_type":I
    .end local v33    # "idx_text1":I
    .end local v34    # "idx_text2":I
    .end local v35    # "idx_text3":I
    .end local v36    # "idx_text4":I
    .end local v37    # "idx_text5":I
    .end local v38    # "idx_uri":I
    .end local v39    # "inkData":Ljava/lang/String;
    .end local v40    # "intent":Landroid/content/Intent;
    .end local v41    # "intentActionStr":Ljava/lang/String;
    .end local v42    # "intentData":Ljava/lang/String;
    .end local v43    # "intentDataId":Ljava/lang/String;
    .end local v44    # "intentExtraData":Ljava/lang/String;
    .end local v45    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v46    # "mimetype":Ljava/lang/String;
    .end local v47    # "orientation":I
    .end local v48    # "processTime":J
    .end local v50    # "text1":Ljava/lang/String;
    .end local v51    # "text2":Ljava/lang/String;
    .end local v52    # "text3":Ljava/lang/String;
    .end local v53    # "text4":Ljava/lang/String;
    .end local v54    # "text5":Ljava/lang/String;
    .end local v55    # "type":Ljava/lang/String;
    .end local v56    # "uri":Ljava/lang/String;
    :cond_1d
    :goto_2
    if-eqz p1, :cond_1e

    .line 461
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 462
    const/16 p1, 0x0

    .line 465
    :cond_1e
    return-void

    .line 179
    .restart local v10    # "hasSortKeyword":Z
    .restart local v48    # "processTime":J
    :cond_1f
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    const-string v58, "setData : preparing"

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 460
    .end local v10    # "hasSortKeyword":Z
    .end local v48    # "processTime":J
    :catchall_0
    move-exception v57

    if-eqz p1, :cond_20

    .line 461
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 462
    const/16 p1, 0x0

    :cond_20
    throw v57

    .line 294
    .restart local v6    # "blob":[B
    .restart local v7    # "dataToShare":Ljava/lang/String;
    .restart local v8    # "extraflags":Ljava/lang/String;
    .restart local v9    # "group":Ljava/lang/String;
    .restart local v10    # "hasSortKeyword":Z
    .restart local v11    # "highlight":Ljava/lang/String;
    .restart local v12    # "icon1":Ljava/lang/String;
    .restart local v13    # "icon2":Ljava/lang/String;
    .restart local v14    # "icon3":Ljava/lang/String;
    .restart local v15    # "icon4":Ljava/lang/String;
    .restart local v16    # "idx_extra_flag":I
    .restart local v17    # "idx_group":I
    .restart local v18    # "idx_highlight":I
    .restart local v19    # "idx_icon1":I
    .restart local v20    # "idx_icon2":I
    .restart local v21    # "idx_icon3":I
    .restart local v22    # "idx_icon4":I
    .restart local v23    # "idx_icon_blob":I
    .restart local v24    # "idx_ink":I
    .restart local v25    # "idx_intent_action":I
    .restart local v26    # "idx_intent_data":I
    .restart local v27    # "idx_intent_data_id":I
    .restart local v28    # "idx_intent_extra":I
    .restart local v29    # "idx_mime_type":I
    .restart local v30    # "idx_orientation":I
    .restart local v31    # "idx_share":I
    .restart local v32    # "idx_target_type":I
    .restart local v33    # "idx_text1":I
    .restart local v34    # "idx_text2":I
    .restart local v35    # "idx_text3":I
    .restart local v36    # "idx_text4":I
    .restart local v37    # "idx_text5":I
    .restart local v38    # "idx_uri":I
    .restart local v39    # "inkData":Ljava/lang/String;
    .restart local v46    # "mimetype":Ljava/lang/String;
    .restart local v47    # "orientation":I
    .restart local v48    # "processTime":J
    .restart local v50    # "text1":Ljava/lang/String;
    .restart local v51    # "text2":Ljava/lang/String;
    .restart local v52    # "text3":Ljava/lang/String;
    .restart local v53    # "text4":Ljava/lang/String;
    .restart local v54    # "text5":Ljava/lang/String;
    .restart local v55    # "type":Ljava/lang/String;
    .restart local v56    # "uri":Ljava/lang/String;
    :cond_21
    :try_start_2
    const-string v57, "null"

    goto/16 :goto_1

    .line 456
    .restart local v40    # "intent":Landroid/content/Intent;
    .restart local v41    # "intentActionStr":Ljava/lang/String;
    .restart local v42    # "intentData":Ljava/lang/String;
    .restart local v43    # "intentDataId":Ljava/lang/String;
    .restart local v44    # "intentExtraData":Ljava/lang/String;
    .restart local v45    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->TAG:Ljava/lang/String;

    move-object/from16 v57, v0

    const-string v58, "setData : completed"

    invoke-static/range {v57 .. v58}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public setItems(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 60
    :cond_1
    return-void
.end method

.method public setOrder(I)V
    .locals 0
    .param p1, "order"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mOrder:I

    .line 43
    return-void
.end method

.method public setTagFilter(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "isSelected"    # Z

    .prologue
    .line 512
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 513
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 517
    if-eqz p2, :cond_1

    .line 518
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    :cond_1
    :goto_0
    return-void

    .line 521
    :cond_2
    if-nez p2, :cond_1

    .line 522
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setTagFilters(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 528
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 529
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 534
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 535
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 537
    :cond_1
    return-void
.end method

.method public updateFilteredItems()V
    .locals 7

    .prologue
    .line 115
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mFilteredItemList:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 116
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mFilteredItemList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 117
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mFilteredItemList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 120
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mFilteredItemList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 122
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->supportTagSearch()Z

    move-result v5

    if-nez v5, :cond_2

    .line 124
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mFilteredItemList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 146
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v0, "beRemovedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mFilteredItemList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 131
    .local v3, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mTagSelectionFilter:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 132
    .local v4, "selectTag":Ljava/lang/String;
    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->doesHaveTag(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_4

    .line 133
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 138
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v4    # "selectTag":Ljava/lang/String;
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 139
    .restart local v3    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->mFilteredItemList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 142
    .end local v3    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    :cond_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method
