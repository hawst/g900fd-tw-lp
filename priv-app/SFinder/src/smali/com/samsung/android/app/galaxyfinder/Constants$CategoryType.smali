.class public Lcom/samsung/android/app/galaxyfinder/Constants$CategoryType;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CategoryType"
.end annotation


# static fields
.field public static final COMMUNICATION:Ljava/lang/String; = "communication"

.field public static final HANDWRITING:Ljava/lang/String; = "handwriting"

.field public static final HELP:Ljava/lang/String; = "help"

.field public static final IMAGES:Ljava/lang/String; = "images"

.field public static final INVALID:Ljava/lang/String; = "invalid"

.field public static final MUSIC:Ljava/lang/String; = "music"

.field public static final NOTES:Ljava/lang/String; = "notes"

.field public static final PERSONAL:Ljava/lang/String; = "personal"

.field public static final VIDEOS:Ljava/lang/String; = "videos"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
