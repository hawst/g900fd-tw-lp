.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$11;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Landroid/widget/FilterQueryProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setCustomSearchView(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0

    .prologue
    .line 737
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$11;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public runQuery(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 5
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x0

    .line 740
    if-nez p1, :cond_0

    move-object v0, v1

    .line 741
    .local v0, "query":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$11;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setCustomSearchView() query : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 745
    :goto_1
    return-object v1

    .line 740
    .end local v0    # "query":Ljava/lang/String;
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 745
    .restart local v0    # "query":Ljava/lang/String;
    :cond_1
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getMergedHistoryTagsItems(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_1
.end method
