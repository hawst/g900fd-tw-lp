.class public Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;
.super Ljava/lang/Object;
.source "CategoryPreferences.java"


# static fields
.field public static final PREF_PACKAGE:Ljava/lang/String; = "pref_package"

.field private static volatile mInstance:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mInstance:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    .line 35
    return-void
.end method

.method private getConfigPrefs(Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "configPrefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 125
    return-object v0
.end method

.method public static getInstance()Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;
    .locals 2

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mInstance:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    if-nez v0, :cond_1

    .line 39
    const-class v1, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    monitor-enter v1

    .line 40
    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mInstance:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mInstance:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    .line 43
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    :cond_1
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mInstance:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    return-object v0

    .line 43
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getSearchCPList(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    invoke-static {p1}, Lcom/samsung/android/app/galaxyfinder/cp/CPISO;->getCurrentISO(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "ISO":Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->getCPData(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method private updateSearchableList(Landroid/content/SharedPreferences;)V
    .locals 7
    .param p1, "prefPkg"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v6, 0x1

    .line 56
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 57
    .local v0, "editorPkg":Landroid/content/SharedPreferences$Editor;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getSearchableList(Z)Ljava/util/ArrayList;

    move-result-object v2

    .line 59
    .local v2, "pkgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 60
    .local v4, "pkgSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 61
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/SearchableInfo;

    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v3

    .line 62
    .local v3, "pkgName":Ljava/lang/String;
    invoke-interface {p1, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 63
    const-string v5, "com.tripadvisor.tripadvisor"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 64
    const/4 v5, 0x0

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 60
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 66
    :cond_1
    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 70
    .end local v3    # "pkgName":Ljava/lang/String;
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 71
    return-void
.end method

.method private updateWebCpList(Landroid/content/SharedPreferences;)V
    .locals 10
    .param p1, "prefPkg"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v9, 0x1

    .line 74
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 76
    .local v2, "editorPkg":Landroid/content/SharedPreferences$Editor;
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/high16 v8, 0x7f080000

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 77
    .local v4, "isTablet":Z
    if-eqz v4, :cond_1

    .line 78
    const-string v7, "websearch"

    invoke-interface {p1, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 79
    const-string v7, "websearch"

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 105
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 106
    return-void

    .line 82
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getSearchCPList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 83
    .local v1, "cpList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;>;"
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 84
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    .line 85
    .local v0, "aCp":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getName()Ljava/lang/String;

    move-result-object v6

    .line 86
    .local v6, "name":Ljava/lang/String;
    if-eqz v6, :cond_2

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 87
    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 92
    .end local v0    # "aCp":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "name":Ljava/lang/String;
    :cond_3
    sget-boolean v7, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v7, :cond_4

    .line 93
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e000b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "key":Ljava/lang/String;
    if-eqz v5, :cond_0

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v5, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 96
    invoke-interface {v2, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 99
    .end local v5    # "key":Ljava/lang/String;
    :cond_4
    const-string v7, "google"

    invoke-interface {p1, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 100
    const-string v7, "google"

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method


# virtual methods
.method public getChildStatus(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 162
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    const-string v3, "pref_package"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 163
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 165
    .local v1, "status":Z
    return v1
.end method

.method public getMyDeviceMap()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    const-string v2, "pref_package"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 171
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method public init()V
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setContextServiceSurveyMode()V

    .line 50
    const-string v1, "pref_package"

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getConfigPrefs(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 51
    .local v0, "prefPkg":Landroid/content/SharedPreferences;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->updateSearchableList(Landroid/content/SharedPreferences;)V

    .line 52
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->updateWebCpList(Landroid/content/SharedPreferences;)V

    .line 53
    return-void
.end method

.method public refreshSearchCategory()V
    .locals 1

    .prologue
    .line 113
    const-string v0, "pref_package"

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getConfigPrefs(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->updateSearchableList(Landroid/content/SharedPreferences;)V

    .line 114
    return-void
.end method

.method public saveCheckedCategories(Ljava/util/HashMap;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    const/4 v9, 0x0

    .line 137
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    const-string v8, "pref_package"

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 139
    .local v6, "prefdevicepkg":Landroid/content/SharedPreferences;
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 141
    .local v1, "editordevicepkg":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 142
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 144
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 145
    .local v2, "entry":Ljava/util/Map$Entry;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 146
    .local v4, "packageName":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 147
    .local v0, "currentCheck":Z
    invoke-interface {v6, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 148
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 149
    .local v5, "prefChecked":Z
    if-eq v5, v0, :cond_0

    .line 150
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    invoke-static {v7, v4, v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertCategoryLog(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 156
    .end local v5    # "prefChecked":Z
    :cond_0
    :goto_1
    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 153
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    invoke-static {v7, v4, v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertCategoryLog(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_1

    .line 158
    .end local v0    # "currentCheck":Z
    .end local v2    # "entry":Ljava/util/Map$Entry;
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_2
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 159
    return-void
.end method

.method public saveCheckedCategory(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "isChecked"    # Z

    .prologue
    .line 129
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->mContext:Landroid/content/Context;

    const-string v3, "pref_package"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 131
    .local v1, "prefdevicepkg":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 132
    .local v0, "editordevicepkg":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 133
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 134
    return-void
.end method
