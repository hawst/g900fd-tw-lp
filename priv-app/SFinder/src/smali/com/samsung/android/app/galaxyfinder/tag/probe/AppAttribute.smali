.class public Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;
.super Ljava/lang/Object;
.source "AppAttribute.java"


# instance fields
.field private contentUri:Landroid/net/Uri;

.field private notifyForDescendents:Z

.field private receivedAction:Ljava/lang/String;

.field private searchableInfo:Landroid/app/SearchableInfo;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;ZLandroid/app/SearchableInfo;)V
    .locals 0
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "notifyForDescendents"    # Z
    .param p4, "searchable"    # Landroid/app/SearchableInfo;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->contentUri:Landroid/net/Uri;

    .line 19
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->receivedAction:Ljava/lang/String;

    .line 20
    iput-boolean p3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->notifyForDescendents:Z

    .line 21
    iput-object p4, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->searchableInfo:Landroid/app/SearchableInfo;

    .line 22
    return-void
.end method


# virtual methods
.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->contentUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getReceivedAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->receivedAction:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchableInfo()Landroid/app/SearchableInfo;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->searchableInfo:Landroid/app/SearchableInfo;

    return-object v0
.end method

.method public isNotifyForDescendents()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->notifyForDescendents:Z

    return v0
.end method

.method public setContentUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->contentUri:Landroid/net/Uri;

    .line 36
    return-void
.end method

.method public setNotifyForDescendents(Z)V
    .locals 0
    .param p1, "notifyForDescendents"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->notifyForDescendents:Z

    .line 50
    return-void
.end method

.method public setReceivedAction(Ljava/lang/String;)V
    .locals 0
    .param p1, "receivedAction"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->receivedAction:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setSearchableInfo(Landroid/app/SearchableInfo;)V
    .locals 0
    .param p1, "searchableInfo"    # Landroid/app/SearchableInfo;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->searchableInfo:Landroid/app/SearchableInfo;

    .line 64
    return-void
.end method
