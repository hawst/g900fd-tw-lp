.class public Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;
.super Landroid/app/Application;
.source "GalaxyFinderApplication.java"


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static mFeatureChecked:Z

.field private static mFeatureEnabled:Z

.field private static mInstance:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

.field private static mLastestActionCategory:Ljava/lang/String;

.field private static mUseSamsungLinkLocationService:Z


# instance fields
.field private mButtonWidth:I

.field mCategory2PackagesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mExtraScrollWidth:I

.field private mScrollWidth:I

.field private mTotalWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 29
    const-class v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->TAG:Ljava/lang/String;

    .line 31
    sput-object v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mInstance:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    .line 33
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mFeatureChecked:Z

    .line 35
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mFeatureEnabled:Z

    .line 37
    sput-object v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mLastestActionCategory:Ljava/lang/String;

    .line 51
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mUseSamsungLinkLocationService:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mContext:Landroid/content/Context;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    .line 43
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mButtonWidth:I

    .line 45
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mTotalWidth:I

    .line 47
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mScrollWidth:I

    .line 49
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mExtraScrollWidth:I

    return-void
.end method

.method public static clearLastestActionCategory()V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mLastestActionCategory:Ljava/lang/String;

    .line 176
    return-void
.end method

.method private existSLinkPackage()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v1

    .line 99
    .local v1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 100
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/pm/ApplicationInfo;>;"
    const/4 v0, 0x0

    .line 101
    .local v0, "app":Landroid/content/pm/ApplicationInfo;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 102
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "app":Landroid/content/pm/ApplicationInfo;
    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 103
    .restart local v0    # "app":Landroid/content/pm/ApplicationInfo;
    const-string v4, "com.samsung.android.sdk.samsunglink"

    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 104
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->TAG:Ljava/lang/String;

    const-string v4, "[Slink platform] SLink package exists"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v3, 0x1

    .line 110
    :goto_0
    return v3

    .line 109
    :cond_1
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->TAG:Ljava/lang/String;

    const-string v5, "[Slink platform] SLink package doesn\'t exists"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mInstance:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;-><init>()V

    .line 64
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mInstance:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    goto :goto_0
.end method

.method public static getLastestActionCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mLastestActionCategory:Ljava/lang/String;

    return-object v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 69
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mInstance:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    .line 70
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mContext:Landroid/content/Context;

    .line 72
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->initConfig()V

    .line 73
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getInstance()Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->init()V

    .line 75
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->initPreferences()V

    .line 76
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->makeHelperPackagesMap()V

    .line 78
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->existSLinkPackage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->initStateSLinkLocationService(Ljava/lang/String;)V

    .line 81
    :cond_0
    return-void
.end method

.method private initPreferences()V
    .locals 4

    .prologue
    .line 84
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 86
    .local v1, "prefs":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_1

    .line 87
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 89
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "setting_use_search_history"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 90
    const-string v2, "setting_use_search_history"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 93
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 95
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    return-void
.end method

.method public static setLastestActionCategory(Ljava/lang/String;)V
    .locals 0
    .param p0, "category"    # Ljava/lang/String;

    .prologue
    .line 167
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mLastestActionCategory:Ljava/lang/String;

    .line 168
    return-void
.end method

.method private setSLinkLocationServiceState(Z)V
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 143
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Slink platform] use state of slink location service is ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mUseSamsungLinkLocationService:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] to ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    sput-boolean p1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mUseSamsungLinkLocationService:Z

    .line 146
    return-void
.end method


# virtual methods
.method public getButtonWidth()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mButtonWidth:I

    return v0
.end method

.method public getExtraScrollWidth()I
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mExtraScrollWidth:I

    return v0
.end method

.method public getHelperPackages(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "helperCategory"    # Ljava/lang/String;

    .prologue
    .line 265
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getScrollWidth()I
    .locals 1

    .prologue
    .line 291
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mScrollWidth:I

    return v0
.end method

.method public getTotalWidth()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mTotalWidth:I

    return v0
.end method

.method public declared-synchronized hasFingerprintFeature()Z
    .locals 2

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mFeatureChecked:Z

    if-eqz v0, :cond_0

    .line 156
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mFeatureEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    :goto_0
    monitor-exit p0

    return v0

    .line 159
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.fingerprint_manager_service"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mFeatureEnabled:Z

    .line 162
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mFeatureChecked:Z

    .line 163
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mFeatureEnabled:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public initStateSLinkLocationService(Ljava/lang/String;)V
    .locals 6
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 114
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 115
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->setSLinkLocationServiceState(Z)V

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v2

    .line 121
    .local v2, "slinkInstance":Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    if-nez v2, :cond_2

    .line 122
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->TAG:Ljava/lang/String;

    const-string v4, "SlinkSignInUtils.getInstance is NULL"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 137
    .end local v2    # "slinkInstance":Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 126
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "slinkInstance":Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getPlatformVersionCode()Ljava/lang/Integer;

    move-result-object v1

    .line 127
    .local v1, "platformVersion":Ljava/lang/Integer;
    if-nez v1, :cond_3

    .line 128
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->TAG:Ljava/lang/String;

    const-string v4, "Slink getPlatformVersionCode is NULL"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 131
    :cond_3
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Slink platform] Version = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x7153

    if-lt v3, v4, :cond_0

    .line 134
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->setSLinkLocationServiceState(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public makeHelperPackagesMap()V
    .locals 10

    .prologue
    .line 179
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getSearchableList(Z)Ljava/util/ArrayList;

    move-result-object v3

    .line 182
    .local v3, "searchableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 183
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 186
    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/SearchableInfo;

    .line 187
    .local v2, "searchable":Landroid/app/SearchableInfo;
    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getCategoryFilters()Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "categories":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v4

    .line 189
    .local v4, "suggestPackage":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v6, "handwriting"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 190
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "handwriting"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 191
    .local v5, "value":Ljava/lang/String;
    if-eqz v5, :cond_9

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_9

    .line 192
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "handwriting"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    .end local v5    # "value":Ljava/lang/String;
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    const-string v6, "notes"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 199
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "notes"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 200
    .restart local v5    # "value":Ljava/lang/String;
    if-eqz v5, :cond_a

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_a

    .line 201
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "notes"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    .end local v5    # "value":Ljava/lang/String;
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    const-string v6, "communication"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 208
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "communication"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 209
    .restart local v5    # "value":Ljava/lang/String;
    if-eqz v5, :cond_b

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_b

    .line 210
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "communication"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    .end local v5    # "value":Ljava/lang/String;
    :cond_4
    :goto_3
    if-eqz v0, :cond_5

    const-string v6, "help"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 217
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "help"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 218
    .restart local v5    # "value":Ljava/lang/String;
    if-eqz v5, :cond_c

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_c

    .line 219
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "help"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    .end local v5    # "value":Ljava/lang/String;
    :cond_5
    :goto_4
    if-eqz v0, :cond_6

    const-string v6, "images"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 226
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "images"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 227
    .restart local v5    # "value":Ljava/lang/String;
    if-eqz v5, :cond_d

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_d

    .line 228
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "images"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    .end local v5    # "value":Ljava/lang/String;
    :cond_6
    :goto_5
    if-eqz v0, :cond_7

    const-string v6, "music"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 235
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "music"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 236
    .restart local v5    # "value":Ljava/lang/String;
    if-eqz v5, :cond_e

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_e

    .line 237
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "music"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    .end local v5    # "value":Ljava/lang/String;
    :cond_7
    :goto_6
    if-eqz v0, :cond_8

    const-string v6, "videos"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 244
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "videos"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 245
    .restart local v5    # "value":Ljava/lang/String;
    if-eqz v5, :cond_f

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_f

    .line 246
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "videos"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    .end local v5    # "value":Ljava/lang/String;
    :cond_8
    :goto_7
    if-eqz v0, :cond_1

    const-string v6, "personal"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 253
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "personal"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 254
    .restart local v5    # "value":Ljava/lang/String;
    if-eqz v5, :cond_10

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_10

    .line 255
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "personal"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 195
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "handwriting"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 204
    :cond_a
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "notes"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 213
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "communication"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 222
    :cond_c
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "help"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 231
    :cond_d
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "images"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 240
    :cond_e
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "music"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 249
    :cond_f
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "videos"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    .line 258
    :cond_10
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mCategory2PackagesMap:Ljava/util/HashMap;

    const-string v7, "personal"

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 262
    .end local v0    # "categories":Ljava/lang/String;
    .end local v2    # "searchable":Landroid/app/SearchableInfo;
    .end local v4    # "suggestPackage":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :cond_11
    return-void
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 57
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->init()V

    .line 58
    return-void
.end method

.method public setExtraScrollWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 279
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mExtraScrollWidth:I

    .line 280
    return-void
.end method

.method public setScrollWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 275
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mScrollWidth:I

    .line 276
    return-void
.end method

.method public setTutorialLocation(II)V
    .locals 0
    .param p1, "expectedWidth"    # I
    .param p2, "totalWidth"    # I

    .prologue
    .line 270
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mButtonWidth:I

    .line 271
    iput p2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mTotalWidth:I

    .line 272
    return-void
.end method

.method public useSLinkLocationService()Z
    .locals 3

    .prologue
    .line 149
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Slink platform] The state of Slink geocode service is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mUseSamsungLinkLocationService:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->mUseSamsungLinkLocationService:Z

    return v0
.end method
