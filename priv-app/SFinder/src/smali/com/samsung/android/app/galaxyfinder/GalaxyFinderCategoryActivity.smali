.class public Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;
.super Landroid/app/ListActivity;
.source "GalaxyFinderCategoryActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$WebCpCategory;,
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SeparatorCategory;,
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SearchableCategory;,
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SelectAllCategory;,
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;,
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;
    }
.end annotation


# instance fields
.field private final CATEGORY_TYPE_CP:I

.field private final CATEGORY_TYPE_SEARCHABLE:I

.field private final CATEGORY_TYPE_SELECT_ALL:I

.field private final CATEGORY_TYPE_SEPARATOR:I

.field private mBaseLinearLayout:Landroid/widget/LinearLayout;

.field private mCategoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryListAdapter:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;

.field private mCategoryPrefrences:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

.field private mCheckBox:Landroid/view/View;

.field private mDropDownButton:Landroid/widget/TextView;

.field private mHeaderView:Landroid/view/View;

.field private mIsTablet:Z

.field private mListAfter:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListBefore:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListPopup:Landroid/widget/ListPopupWindow;

.field private mListPopupAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mMyDeviceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectableCategorySize:I

.field private mSelectedCount:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 53
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->CATEGORY_TYPE_SEARCHABLE:I

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->CATEGORY_TYPE_CP:I

    .line 57
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->CATEGORY_TYPE_SEPARATOR:I

    .line 59
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->CATEGORY_TYPE_SELECT_ALL:I

    .line 61
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getInstance()Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryPrefrences:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    .line 67
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListAfter:Ljava/util/ArrayList;

    .line 69
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCheckBox:Landroid/view/View;

    .line 71
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryListAdapter:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;

    .line 73
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mDropDownButton:Landroid/widget/TextView;

    .line 75
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListPopup:Landroid/widget/ListPopupWindow;

    .line 77
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListPopupAdapter:Landroid/widget/ArrayAdapter;

    .line 79
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectedCount:I

    .line 81
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectableCategorySize:I

    .line 83
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListView:Landroid/widget/ListView;

    .line 87
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mBaseLinearLayout:Landroid/widget/LinearLayout;

    .line 89
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mIsTablet:Z

    .line 601
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectedCount:I

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectedCount:I

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectableCategorySize:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->setCheckedAll(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;
    .param p1, "x1"    # J

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->performListItemClick(J)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getSelectedCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCheckBox:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCheckBox:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->isCheckedPackage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private addSearchableCategory()V
    .locals 10

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 190
    .local v5, "resource":Landroid/content/res/Resources;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getSearchableList(Z)Ljava/util/ArrayList;

    move-result-object v6

    .line 192
    .local v6, "searchableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 193
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchableInfo;

    .line 194
    .local v0, "aInfo":Landroid/app/SearchableInfo;
    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getLabelId()I

    move-result v8

    invoke-direct {p0, v7, v8}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getApplicationLabel(Landroid/content/ComponentName;I)Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "appLabel":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v4

    .line 196
    .local v4, "providerName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 198
    .local v3, "icon":Landroid/graphics/drawable/Drawable;
    const-string v7, "com.android.contacts"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "com.android.jcontacts"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "com.samsung.contacts"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 202
    :cond_0
    const v7, 0x7f020081

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 206
    :goto_1
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    new-instance v8, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SearchableCategory;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9, v1, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SearchableCategory;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    :cond_1
    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, p0, v7}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getApplicationIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_1

    .line 210
    .end local v0    # "aInfo":Landroid/app/SearchableInfo;
    .end local v1    # "appLabel":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v4    # "providerName":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private addSelectAllCategory()V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SelectAllCategory;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SelectAllCategory;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 186
    return-void
.end method

.method private addWebCategory()V
    .locals 13

    .prologue
    const v12, 0x7f0e00a4

    const/16 v10, 0x2f

    const v11, 0x7f0e000b

    .line 214
    invoke-direct {p0, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getSearchCPList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 215
    .local v2, "cpList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;>;"
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 217
    .local v5, "resource":Landroid/content/res/Resources;
    iget-boolean v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mIsTablet:Z

    if-eqz v7, :cond_3

    .line 218
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    sget-boolean v7, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v7, :cond_2

    .line 219
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "android.resource://"

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 221
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f02018e

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    new-instance v8, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$WebCpCategory;

    const-string v9, "websearch"

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, p0, v9, v10, v11}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$WebCpCategory;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    :cond_2
    :goto_0
    return-void

    .line 228
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    new-instance v8, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SeparatorCategory;

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SeparatorCategory;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_8

    .line 232
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    .line 233
    .local v0, "aCpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getName()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_4

    .line 236
    const/4 v4, 0x0

    .line 238
    .local v4, "iconUri":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getIcon()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 239
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getIcon()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.resource"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 240
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getIcon()Ljava/lang/String;

    move-result-object v4

    .line 245
    :cond_5
    :goto_2
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getName()Ljava/lang/String;

    move-result-object v6

    .line 246
    .local v6, "webName":Ljava/lang/String;
    const-string v7, "Baidu"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 247
    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 249
    :cond_6
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    new-instance v8, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$WebCpCategory;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getName()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9, v6, v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$WebCpCategory;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 242
    .end local v6    # "webName":Ljava/lang/String;
    :cond_7
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getIcon()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/galaxyfinder/cp/CPFileLoad;->getIconURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 256
    .end local v0    # "aCpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "iconUri":Ljava/lang/String;
    :cond_8
    sget-boolean v7, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v7, :cond_9

    .line 257
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "android.resource://"

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 260
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f02018c

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    new-instance v8, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$WebCpCategory;

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, p0, v9, v10, v11}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$WebCpCategory;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 268
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    :cond_9
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private getApplicationIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 322
    const-string v4, "com.samsung.android.app.galaxyfinder"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 323
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 339
    :goto_0
    return-object v1

    .line 326
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 327
    .local v2, "mPM":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 330
    .local v1, "icon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    invoke-virtual {v2, p2}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    goto :goto_0

    .line 331
    :catch_0
    move-exception v0

    .line 332
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v1, v3

    .line 333
    goto :goto_0

    .line 334
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 335
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    move-object v1, v3

    .line 336
    goto :goto_0
.end method

.method private getApplicationLabel(Landroid/content/ComponentName;I)Ljava/lang/String;
    .locals 1
    .param p1, "cn"    # Landroid/content/ComponentName;
    .param p2, "id"    # I

    .prologue
    .line 318
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getApplicationLabel(Landroid/content/ComponentName;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCheckedDeviceList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 362
    .local v2, "packageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 363
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 364
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 366
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 367
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 371
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_1
    return-object v2
.end method

.method private getSearchCPList(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    invoke-static {p1}, Lcom/samsung/android/app/galaxyfinder/cp/CPISO;->getCurrentISO(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 285
    .local v0, "ISO":Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->getCPData(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method private getSelectedCount()I
    .locals 5

    .prologue
    .line 439
    const/4 v0, 0x0

    .line 441
    .local v0, "count":I
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 442
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 443
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 444
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 445
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 448
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_1
    return v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->prepareCategoryDataSet()V

    .line 123
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->setMyDeviceMap()V

    .line 125
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getSelectedCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectedCount:I

    .line 126
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectableCategorySize:I

    .line 128
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getCheckedDeviceList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListBefore:Ljava/util/ArrayList;

    .line 129
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;

    const v1, 0x7f03005a

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryListAdapter:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;

    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryListAdapter:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 133
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$1;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 148
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->setActionBar()V

    .line 150
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->setFinishOnTouchOutside(Z)V

    .line 151
    return-void
.end method

.method private isCheckedPackage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 375
    const/4 v0, 0x0

    .line 376
    .local v0, "checked":Z
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 377
    .local v1, "value":Ljava/lang/Boolean;
    if-eqz v1, :cond_0

    .line 378
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 380
    :cond_0
    return v0
.end method

.method private performListItemClick(J)V
    .locals 5
    .param p1, "itemId"    # J

    .prologue
    .line 163
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    long-to-int v3, p1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->getCatetoryId()Ljava/lang/String;

    move-result-object v1

    .line 165
    .local v1, "packageName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 166
    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->isCheckedPackage(Ljava/lang/String;)Z

    move-result v0

    .line 167
    .local v0, "isChecked":Z
    if-nez v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-direct {p0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->saveCheckedCategory(Ljava/lang/String;Z)V

    .line 168
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryListAdapter:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->notifyDataSetChanged()V

    .line 170
    .end local v0    # "isChecked":Z
    :cond_0
    return-void

    .line 167
    .restart local v0    # "isChecked":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private prepareCategoryDataSet()V
    .locals 1

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->addSearchableCategory()V

    .line 277
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->addWebCategory()V

    .line 278
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 279
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->addSelectAllCategory()V

    .line 281
    :cond_0
    return-void
.end method

.method private saveCheckedCategory(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "checked"    # Z

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    return-void
.end method

.method private saveCurrentSetting()V
    .locals 2

    .prologue
    .line 309
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getCheckedDeviceList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListAfter:Ljava/util/ArrayList;

    .line 311
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListBefore:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListAfter:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListAfter:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListBefore:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 312
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->setSearchableCategoryChanged()V

    .line 313
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryPrefrences:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->saveCheckedCategories(Ljava/util/HashMap;)V

    .line 315
    :cond_1
    return-void
.end method

.method private selectAllChecked(Z)V
    .locals 5
    .param p1, "checked"    # Z

    .prologue
    .line 388
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 389
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 390
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 391
    .local v0, "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 393
    .end local v0    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_0
    return-void
.end method

.method private setActionBar()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 174
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 175
    .local v0, "actionBar":Landroid/app/ActionBar;
    const v1, 0x7f0e0098

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 176
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 177
    const/4 v1, 0x2

    invoke-virtual {v0, v3, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 178
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 179
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 180
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 181
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 182
    return-void
.end method

.method private setCheckedAll(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 432
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryListAdapter:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;

    if-eqz v0, :cond_0

    .line 433
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->selectAllChecked(Z)V

    .line 434
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryListAdapter:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->notifyDataSetChanged()V

    .line 436
    :cond_0
    return-void
.end method

.method private setFloatingSettings()V
    .locals 5

    .prologue
    .line 396
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 397
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 398
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 399
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 400
    const v1, 0x3ecccccd    # 0.4f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 401
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 402
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c001f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setLayout(II)V

    .line 404
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x35

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 405
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x7f020168

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 407
    const v1, 0x7f04000d

    const v2, 0x7f04000e

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->overridePendingTransition(II)V

    .line 408
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListView:Landroid/widget/ListView;

    const/high16 v2, 0x3000000

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 409
    return-void
.end method

.method private setMyDeviceMap()V
    .locals 5

    .prologue
    .line 343
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    .line 344
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryPrefrences:Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getMyDeviceMap()Ljava/util/Map;

    move-result-object v2

    .line 346
    .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 347
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 348
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->getCatetoryId()Ljava/lang/String;

    move-result-object v3

    .line 350
    .local v3, "packageName":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 351
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 352
    .local v0, "isEnabled":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    .line 353
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mMyDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v4, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 357
    .end local v0    # "isEnabled":Ljava/lang/Boolean;
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 453
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 454
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->finish()V

    .line 457
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0}, Landroid/app/ListActivity;->finish()V

    .line 157
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mIsTablet:Z

    if-eqz v0, :cond_0

    .line 158
    const v0, 0x7f04000d

    const v1, 0x7f04000e

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->overridePendingTransition(II)V

    .line 160
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "conf"    # Landroid/content/res/Configuration;

    .prologue
    .line 423
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 425
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mIsTablet:Z

    if-eqz v0, :cond_0

    .line 426
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 429
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mIsTablet:Z

    .line 95
    const v0, 0x7f030059

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->setContentView(I)V

    .line 97
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListView:Landroid/widget/ListView;

    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 99
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 100
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 102
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mIsTablet:Z

    if-eqz v0, :cond_1

    .line 103
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->setFloatingSettings()V

    .line 108
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const v0, 0x103012b

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->setTheme(I)V

    .line 112
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->init()V

    .line 113
    return-void

    .line 105
    :cond_1
    const v0, 0x7f0b00c6

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mBaseLinearLayout:Landroid/widget/LinearLayout;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 413
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 290
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 292
    .local v0, "itemID":I
    sparse-switch v0, :sswitch_data_0

    .line 304
    :goto_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 294
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->finish()V

    goto :goto_0

    .line 297
    :sswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->finish()V

    .line 298
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->saveCurrentSetting()V

    goto :goto_0

    .line 292
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0b00e4 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->saveCurrentSetting()V

    .line 118
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 119
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 418
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
