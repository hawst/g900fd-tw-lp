.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListPhoneAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;
    }
.end annotation


# static fields
.field static final CALL_STATE_ICON_TYPE_AUTO_REJECTED_CALL:I = 0x10

.field static final CALL_STATE_ICON_TYPE_MISSED_CALL:I = 0x4

.field static final CALL_STATE_ICON_TYPE_RECEIVED:I = 0x2

.field static final CALL_STATE_ICON_TYPE_REJECTED_CALL:I = 0x8

.field static final CALL_STATE_ICON_TYPE_SENT:I = 0x1

.field static final CALL_TYPE_ICON_INTERNET_CALL:I = 0x20

.field static final CALL_TYPE_ICON_MMS:I = 0x2

.field static final CALL_TYPE_ICON_SMS:I = 0x1

.field static final CALL_TYPE_ICON_VIDEO_CALL:I = 0x8

.field static final CALL_TYPE_ICON_VOICEMAIL:I = 0x4

.field static final CALL_TYPE_ICON_VOICE_CALL:I = 0x10


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 22
    return-void
.end method

.method private getCallStateIcon(I)I
    .locals 1
    .param p1, "iconType"    # I

    .prologue
    .line 112
    sparse-switch p1, :sswitch_data_0

    .line 124
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 114
    :sswitch_0
    const v0, 0x7f0200ad

    goto :goto_0

    .line 116
    :sswitch_1
    const v0, 0x7f0200ab

    goto :goto_0

    .line 118
    :sswitch_2
    const v0, 0x7f0200aa

    goto :goto_0

    .line 120
    :sswitch_3
    const v0, 0x7f0200ac

    goto :goto_0

    .line 122
    :sswitch_4
    const v0, 0x7f0200a9

    goto :goto_0

    .line 112
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 20
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 26
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v12

    .line 27
    .local v12, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v8, 0x0

    .line 29
    .local v8, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 31
    if-nez p2, :cond_6

    .line 32
    sget-object v16, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v17, 0x7f030043

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-virtual/range {v16 .. v19}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 34
    new-instance v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;

    .end local v8    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;
    invoke-direct {v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;-><init>()V

    .line 36
    .restart local v8    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;
    const v16, 0x7f0b0054

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 37
    const v16, 0x7f0b004e

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    move-object/from16 v0, v16

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 38
    const v16, 0x7f0b0056

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->tvDate:Landroid/widget/TextView;

    .line 39
    const v16, 0x7f0b008c

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->tvTime:Landroid/widget/TextView;

    .line 40
    const v16, 0x7f0b008a

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    move-object/from16 v0, v16

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->ivState:Landroid/widget/ImageView;

    .line 41
    const v16, 0x7f0b005b

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    move-object/from16 v0, v16

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    .line 42
    const v16, 0x7f0b0058

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/view/ViewGroup;

    move-object/from16 v0, v16

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    .line 44
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    :goto_0
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v13

    .line 50
    .local v13, "name":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v6

    .line 51
    .local v6, "date":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText4()Ljava/lang/String;

    move-result-object v14

    .line 52
    .local v14, "time":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v10

    .line 53
    .local v10, "iconUri":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v3

    .line 54
    .local v3, "bgColor":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v15

    .line 55
    .local v15, "typeInfoSet":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v11

    .line 57
    .local v11, "intent":Landroid/content/Intent;
    const/4 v5, -0x1

    .line 59
    .local v5, "color":I
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_0

    const-string v16, "0"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_0

    .line 61
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 67
    :cond_0
    :goto_1
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 69
    :try_start_1
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 70
    .local v9, "iconFlagSet":I
    shr-int/lit8 v16, v9, 0x6

    and-int/lit8 v16, v16, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;->getCallStateIcon(I)I

    move-result v4

    .line 72
    .local v4, "callStateIconResId":I
    if-lez v4, :cond_1

    .line 73
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->ivState:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 80
    .end local v4    # "callStateIconResId":I
    .end local v9    # "iconFlagSet":I
    :cond_1
    :goto_2
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_2

    .line 81
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v13, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 84
    :cond_2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_3

    .line 85
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->tvDate:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 88
    :cond_3
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_4

    .line 89
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->tvTime:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v14, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 92
    :cond_4
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v16

    if-nez v16, :cond_5

    .line 93
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v10, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;->setImageViewForContactRoundedStyle(Landroid/widget/ImageView;Ljava/lang/String;I)V

    .line 96
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-virtual {v0, v1, v11, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 98
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v16

    return-object v16

    .line 46
    .end local v3    # "bgColor":Ljava/lang/String;
    .end local v5    # "color":I
    .end local v6    # "date":Ljava/lang/String;
    .end local v10    # "iconUri":Ljava/lang/String;
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "time":Ljava/lang/String;
    .end local v15    # "typeInfoSet":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;
    check-cast v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;

    .restart local v8    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPhoneAdapter$PhoneItemListViewHolder;
    goto/16 :goto_0

    .line 62
    .restart local v3    # "bgColor":Ljava/lang/String;
    .restart local v5    # "color":I
    .restart local v6    # "date":Ljava/lang/String;
    .restart local v10    # "iconUri":Ljava/lang/String;
    .restart local v11    # "intent":Landroid/content/Intent;
    .restart local v13    # "name":Ljava/lang/String;
    .restart local v14    # "time":Ljava/lang/String;
    .restart local v15    # "typeInfoSet":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 63
    .local v7, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_1

    .line 75
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v7

    .line 76
    .restart local v7    # "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_2
.end method
