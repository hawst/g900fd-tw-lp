.class public final Lcom/samsung/android/app/galaxyfinder/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_common_button_background_selector:I = 0x7f020000

.field public static final actionbar_dropdown_button_selector:I = 0x7f020001

.field public static final actionbar_dropdown_text_selector:I = 0x7f020002

.field public static final actionbar_icon_list_close:I = 0x7f020003

.field public static final actionbar_icon_list_close_dim:I = 0x7f020004

.field public static final actionbar_icon_list_open:I = 0x7f020005

.field public static final actionbar_icon_list_open_dim:I = 0x7f020006

.field public static final actionbar_select_all_selector:I = 0x7f020007

.field public static final app_shadow:I = 0x7f020008

.field public static final applications:I = 0x7f020009

.field public static final base_bg:I = 0x7f02000a

.field public static final block_selection:I = 0x7f02000b

.field public static final btn_app_close_default:I = 0x7f02000c

.field public static final btn_app_close_disabled:I = 0x7f02000d

.field public static final btn_app_close_disabled_focused:I = 0x7f02000e

.field public static final btn_app_close_focus:I = 0x7f02000f

.field public static final btn_app_close_press:I = 0x7f020010

.field public static final btn_app_open_default:I = 0x7f020011

.field public static final btn_app_open_disabled:I = 0x7f020012

.field public static final btn_app_open_disabled_focused:I = 0x7f020013

.field public static final btn_app_open_focus:I = 0x7f020014

.field public static final btn_app_open_press:I = 0x7f020015

.field public static final btn_calendar:I = 0x7f020016

.field public static final btn_calendar_press:I = 0x7f020017

.field public static final btn_call:I = 0x7f020018

.field public static final btn_call_press:I = 0x7f020019

.field public static final btn_email:I = 0x7f02001a

.field public static final btn_email_press:I = 0x7f02001b

.field public static final btn_enter:I = 0x7f02001c

.field public static final btn_enter_press:I = 0x7f02001d

.field public static final btn_map:I = 0x7f02001e

.field public static final btn_map_press:I = 0x7f02001f

.field public static final btn_message:I = 0x7f020020

.field public static final btn_message_press:I = 0x7f020021

.field public static final btn_more:I = 0x7f020022

.field public static final btn_more_press:I = 0x7f020023

.field public static final btn_next_depth_selector:I = 0x7f020024

.field public static final btn_web:I = 0x7f020025

.field public static final btn_web_press:I = 0x7f020026

.field public static final button_mic_idle:I = 0x7f020027

.field public static final button_mic_listening:I = 0x7f020028

.field public static final button_mic_prepare:I = 0x7f020029

.field public static final button_mic_recognizing:I = 0x7f02002a

.field public static final button_voice_input_text_color:I = 0x7f02002b

.field public static final card_gallery_desc_overlay_selector:I = 0x7f02002c

.field public static final category_delete:I = 0x7f02002d

.field public static final category_delete_focus:I = 0x7f02002e

.field public static final category_delete_press:I = 0x7f02002f

.field public static final category_select_bg:I = 0x7f020030

.field public static final category_select_focus_bg:I = 0x7f020031

.field public static final category_select_press_bg:I = 0x7f020032

.field public static final category_show_more_btn_selector:I = 0x7f020033

.field public static final checkbox_selector:I = 0x7f020034

.field public static final controller_down:I = 0x7f020035

.field public static final controller_up:I = 0x7f020036

.field public static final custom_progress_bar:I = 0x7f020037

.field public static final divider_do:I = 0x7f020038

.field public static final divider_main_page:I = 0x7f020039

.field public static final divider_page:I = 0x7f02003a

.field public static final dnd_list_item_selector:I = 0x7f02003b

.field public static final email_list_low_priority_01:I = 0x7f02003c

.field public static final email_list_priority:I = 0x7f02003d

.field public static final ep_thumbview_book_02:I = 0x7f02003e

.field public static final expand_button_close_selector:I = 0x7f02003f

.field public static final expand_button_open_selector:I = 0x7f020040

.field public static final filter_bg:I = 0x7f020041

.field public static final filter_group_1_icon_selector:I = 0x7f020042

.field public static final filter_group_1_text_selector:I = 0x7f020043

.field public static final filter_group_2_icon_selector:I = 0x7f020044

.field public static final filter_group_2_text_selector:I = 0x7f020045

.field public static final filter_group_3_icon_selector:I = 0x7f020046

.field public static final filter_group_3_text_selector:I = 0x7f020047

.field public static final filter_group_4_icon_selector:I = 0x7f020048

.field public static final filter_group_4_text_selector:I = 0x7f020049

.field public static final filter_group_5_icon_selector:I = 0x7f02004a

.field public static final filter_group_5_text_selector:I = 0x7f02004b

.field public static final filter_ic_category:I = 0x7f02004c

.field public static final filter_ic_category_disabled:I = 0x7f02004d

.field public static final filter_ic_category_focused:I = 0x7f02004e

.field public static final filter_ic_category_pressed:I = 0x7f02004f

.field public static final filter_ic_handwriting:I = 0x7f020050

.field public static final filter_ic_handwriting_dim:I = 0x7f020051

.field public static final filter_ic_handwriting_focused:I = 0x7f020052

.field public static final filter_ic_handwriting_pressed:I = 0x7f020053

.field public static final filter_ic_location:I = 0x7f020054

.field public static final filter_ic_location_disabled:I = 0x7f020055

.field public static final filter_ic_location_focused:I = 0x7f020056

.field public static final filter_ic_location_pressed:I = 0x7f020057

.field public static final filter_ic_tag:I = 0x7f020058

.field public static final filter_ic_tag_disabled:I = 0x7f020059

.field public static final filter_ic_tag_focused:I = 0x7f02005a

.field public static final filter_ic_tag_pressed:I = 0x7f02005b

.field public static final filter_ic_time:I = 0x7f02005c

.field public static final filter_ic_time_disabled:I = 0x7f02005d

.field public static final filter_ic_time_focused:I = 0x7f02005e

.field public static final filter_ic_time_pressed:I = 0x7f02005f

.field public static final filter_item_text_selector:I = 0x7f020060

.field public static final filter_selected_item_bg_selector:I = 0x7f020061

.field public static final filter_selected_item_delete_btn_selector:I = 0x7f020062

.field public static final galaxy_finder:I = 0x7f020063

.field public static final group_item_subtitle_btn_selector:I = 0x7f020064

.field public static final group_seperator_bg:I = 0x7f020065

.field public static final guide_bubble_button_selector:I = 0x7f020066

.field public static final handwriting_bg:I = 0x7f020067

.field public static final help_popup_picker_b_c:I = 0x7f020068

.field public static final help_popup_picker_bg_w_01:I = 0x7f020069

.field public static final help_popup_picker_t_c:I = 0x7f02006a

.field public static final help_start_now_01:I = 0x7f02006b

.field public static final help_start_now_02:I = 0x7f02006c

.field public static final help_start_now_03:I = 0x7f02006d

.field public static final help_start_now_04:I = 0x7f02006e

.field public static final help_tap_1_default:I = 0x7f02006f

.field public static final history_list_delete_all_selector:I = 0x7f020070

.field public static final ic_actionbar_option_selector:I = 0x7f020071

.field public static final ic_albums:I = 0x7f020072

.field public static final ic_artist:I = 0x7f020073

.field public static final ic_attach:I = 0x7f020074

.field public static final ic_attach_parsing:I = 0x7f020075

.field public static final ic_bg_selector:I = 0x7f020076

.field public static final ic_btn_round_more_selector:I = 0x7f020077

.field public static final ic_category:I = 0x7f020078

.field public static final ic_category_press:I = 0x7f020079

.field public static final ic_category_selector:I = 0x7f02007a

.field public static final ic_close:I = 0x7f02007b

.field public static final ic_finger_01:I = 0x7f02007c

.field public static final ic_finger_02:I = 0x7f02007d

.field public static final ic_finger_03:I = 0x7f02007e

.field public static final ic_focus:I = 0x7f02007f

.field public static final ic_gallery_note:I = 0x7f020080

.field public static final ic_launcher_phone:I = 0x7f020081

.field public static final ic_location:I = 0x7f020082

.field public static final ic_location_press:I = 0x7f020083

.field public static final ic_location_selector:I = 0x7f020084

.field public static final ic_memo:I = 0x7f020085

.field public static final ic_playlist:I = 0x7f020086

.field public static final ic_popup_category:I = 0x7f020087

.field public static final ic_popup_location:I = 0x7f020088

.field public static final ic_popup_tag:I = 0x7f020089

.field public static final ic_popup_time:I = 0x7f02008a

.field public static final ic_press:I = 0x7f02008b

.field public static final ic_search:I = 0x7f02008c

.field public static final ic_snote_favorite:I = 0x7f02008d

.field public static final ic_snote_hold:I = 0x7f02008e

.field public static final ic_snote_rec:I = 0x7f02008f

.field public static final ic_snote_tag:I = 0x7f020090

.field public static final ic_song:I = 0x7f020091

.field public static final ic_splanner_alarm:I = 0x7f020092

.field public static final ic_splanner_repeat:I = 0x7f020093

.field public static final ic_tag:I = 0x7f020094

.field public static final ic_tag_press:I = 0x7f020095

.field public static final ic_tag_selector:I = 0x7f020096

.field public static final ic_time:I = 0x7f020097

.field public static final ic_time_press:I = 0x7f020098

.field public static final ic_time_selector:I = 0x7f020099

.field public static final ic_voice:I = 0x7f02009a

.field public static final ic_voicenote_stt:I = 0x7f02009b

.field public static final icon_failed_normal:I = 0x7f02009c

.field public static final icon_nothing:I = 0x7f02009d

.field public static final improvements_input_ic_tag:I = 0x7f02009e

.field public static final inner_stroke:I = 0x7f02009f

.field public static final item_grid_selector:I = 0x7f0200a0

.field public static final item_list_selector:I = 0x7f0200a1

.field public static final item_scrapbook_selector:I = 0x7f0200a2

.field public static final list_ic_priority:I = 0x7f0200a3

.field public static final list_ic_priority_low:I = 0x7f0200a4

.field public static final list_icon_fwd:I = 0x7f0200a5

.field public static final list_icon_re:I = 0x7f0200a6

.field public static final list_icon_vip:I = 0x7f0200a7

.field public static final live_memo_shadow:I = 0x7f0200a8

.field public static final log_auto_rejected_call_phone:I = 0x7f0200a9

.field public static final log_missed_call_phone:I = 0x7f0200aa

.field public static final log_received_phone:I = 0x7f0200ab

.field public static final log_rejected_call_phone:I = 0x7f0200ac

.field public static final log_sent_phone:I = 0x7f0200ad

.field public static final logs_internet_call:I = 0x7f0200ae

.field public static final logs_mms:I = 0x7f0200af

.field public static final logs_sms:I = 0x7f0200b0

.field public static final logs_video_call:I = 0x7f0200b1

.field public static final logs_voice_call:I = 0x7f0200b2

.field public static final logs_voicemail:I = 0x7f0200b3

.field public static final main_bg:I = 0x7f0200b4

.field public static final main_subtitle_bg:I = 0x7f0200b5

.field public static final mainpage_btn_focus:I = 0x7f0200b6

.field public static final mainpage_btn_normal:I = 0x7f0200b7

.field public static final mainpage_btn_press:I = 0x7f0200b8

.field public static final mainpage_btn_select:I = 0x7f0200b9

.field public static final message_bubble_failedmessage_01:I = 0x7f0200ba

.field public static final message_bubble_failedmessage_01_press:I = 0x7f0200bb

.field public static final message_bubble_receivedmessage_01_press:I = 0x7f0200bc

.field public static final message_bubble_sentmessage_01_press:I = 0x7f0200bd

.field public static final message_receive:I = 0x7f0200be

.field public static final message_send:I = 0x7f0200bf

.field public static final more_background_focused:I = 0x7f0200c0

.field public static final more_background_pressed:I = 0x7f0200c1

.field public static final more_background_selected:I = 0x7f0200c2

.field public static final myfiles_icon_audio:I = 0x7f0200c3

.field public static final myfiles_icon_etc:I = 0x7f0200c4

.field public static final myfiles_icon_folder_normal_thumb:I = 0x7f0200c5

.field public static final myfiles_icon_image:I = 0x7f0200c6

.field public static final myfiles_icon_play:I = 0x7f0200c7

.field public static final myfiles_icon_video:I = 0x7f0200c8

.field public static final option_icon_help:I = 0x7f0200c9

.field public static final option_icon_help_disabled:I = 0x7f0200ca

.field public static final option_icon_help_selector:I = 0x7f0200cb

.field public static final option_icon_refresh:I = 0x7f0200cc

.field public static final option_icon_refresh_disabled:I = 0x7f0200cd

.field public static final option_icon_refresh_selector:I = 0x7f0200ce

.field public static final option_icon_setting:I = 0x7f0200cf

.field public static final option_icon_setting_disabled:I = 0x7f0200d0

.field public static final option_icon_setting_selector:I = 0x7f0200d1

.field public static final option_icon_share:I = 0x7f0200d2

.field public static final option_icon_share_disabled:I = 0x7f0200d3

.field public static final option_icon_share_selector:I = 0x7f0200d4

.field public static final option_more_bg_selector:I = 0x7f0200d5

.field public static final page_bg:I = 0x7f0200d6

.field public static final page_bottom_bg:I = 0x7f0200d7

.field public static final page_title_bg:I = 0x7f0200d8

.field public static final photo_box:I = 0x7f0200d9

.field public static final popup_button:I = 0x7f0200da

.field public static final popup_list_share_via_background:I = 0x7f0200db

.field public static final quickmemo_check_off:I = 0x7f0200dc

.field public static final quickmemo_check_off_focused:I = 0x7f0200dd

.field public static final quickmemo_check_off_pressed:I = 0x7f0200de

.field public static final quickmemo_check_on:I = 0x7f0200df

.field public static final quickmemo_check_on_focused:I = 0x7f0200e0

.field public static final quickmemo_check_on_pressed:I = 0x7f0200e1

.field public static final ripple_backgroud_selected_email:I = 0x7f0200e2

.field public static final ripple_background:I = 0x7f0200e3

.field public static final ripple_background_footer:I = 0x7f0200e4

.field public static final s_note_text_bg:I = 0x7f0200e5

.field public static final scrap_book_bg:I = 0x7f0200e6

.field public static final scrollbar_sfinder:I = 0x7f0200e7

.field public static final search_background_selector:I = 0x7f0200e8

.field public static final search_bar_focus:I = 0x7f0200e9

.field public static final search_bar_press:I = 0x7f0200ea

.field public static final search_bar_select:I = 0x7f0200eb

.field public static final search_history_bg:I = 0x7f0200ec

.field public static final search_history_line:I = 0x7f0200ed

.field public static final search_icon_more:I = 0x7f0200ee

.field public static final search_list_email_bg_selector:I = 0x7f0200ef

.field public static final search_list_item_btn_call_selector:I = 0x7f0200f0

.field public static final search_list_item_btn_date_selector:I = 0x7f0200f1

.field public static final search_list_item_btn_do_bg_selector:I = 0x7f0200f2

.field public static final search_list_item_btn_internet_selector:I = 0x7f0200f3

.field public static final search_list_item_btn_map_selector:I = 0x7f0200f4

.field public static final search_list_item_btn_message_selector:I = 0x7f0200f5

.field public static final search_list_item_btn_myfiles_selector:I = 0x7f0200f6

.field public static final search_list_item_btn_send_selector:I = 0x7f0200f7

.field public static final search_list_item_btn_video_selector:I = 0x7f0200f8

.field public static final search_list_item_footer_do_bg_selector:I = 0x7f0200f9

.field public static final search_list_item_message_receive_bg_selector:I = 0x7f0200fa

.field public static final search_list_item_message_send_bg_selector:I = 0x7f0200fb

.field public static final search_list_item_message_sent_failed_bg_selector:I = 0x7f0200fc

.field public static final search_normal:I = 0x7f0200fd

.field public static final search_thumbnail_outline:I = 0x7f0200fe

.field public static final searching_no_bg_bt_center_press:I = 0x7f0200ff

.field public static final searching_no_bg_bt_left_press:I = 0x7f020100

.field public static final searching_no_bg_bt_one_press:I = 0x7f020101

.field public static final searching_no_bg_bt_right_press:I = 0x7f020102

.field public static final settings_bg:I = 0x7f020103

.field public static final shape_voiceinputdialog:I = 0x7f020104

.field public static final show_more_button_selector:I = 0x7f020105

.field public static final snote_library_ic_cloudsync:I = 0x7f020106

.field public static final snote_library_ic_cloudsync_press:I = 0x7f020107

.field public static final snote_library_ic_cloudsync_selector:I = 0x7f020108

.field public static final snote_library_ic_favorite:I = 0x7f020109

.field public static final snote_library_ic_favorite_press:I = 0x7f02010a

.field public static final snote_library_ic_favorite_selector:I = 0x7f02010b

.field public static final snote_library_ic_folder:I = 0x7f02010c

.field public static final snote_library_ic_folder_press:I = 0x7f02010d

.field public static final snote_library_ic_folder_selector:I = 0x7f02010e

.field public static final snote_library_ic_personal:I = 0x7f02010f

.field public static final snote_library_ic_personal_press:I = 0x7f020110

.field public static final snote_library_ic_record:I = 0x7f020111

.field public static final snote_library_ic_record_press:I = 0x7f020112

.field public static final snote_library_ic_record_selector:I = 0x7f020113

.field public static final snote_library_ic_tag:I = 0x7f020114

.field public static final snote_library_ic_tag_press:I = 0x7f020115

.field public static final snote_library_ic_tag_selector:I = 0x7f020116

.field public static final snote_library_lock:I = 0x7f020117

.field public static final subtitle_btn_close:I = 0x7f020118

.field public static final subtitle_btn_close_press:I = 0x7f020119

.field public static final subtitle_btn_open:I = 0x7f02011a

.field public static final subtitle_btn_open_press:I = 0x7f02011b

.field public static final subtitle_divider:I = 0x7f02011c

.field public static final tag_focus:I = 0x7f02011d

.field public static final tag_press:I = 0x7f02011e

.field public static final textfield_search_bg:I = 0x7f02011f

.field public static final textfield_search_default:I = 0x7f020120

.field public static final textfield_search_focus:I = 0x7f020121

.field public static final textfield_search_focused:I = 0x7f020122

.field public static final textfield_search_press:I = 0x7f020123

.field public static final textfield_search_select:I = 0x7f020124

.field public static final textonly_bg:I = 0x7f020125

.field public static final thumbnail_checkbox_selector:I = 0x7f020126

.field public static final thumbnail_default_note_shadow:I = 0x7f020127

.field public static final thumbnail_shadow:I = 0x7f020128

.field public static final tipbubble_btn_focus:I = 0x7f020129

.field public static final tipbubble_btn_normal:I = 0x7f02012a

.field public static final tipbubble_btn_press:I = 0x7f02012b

.field public static final translator_eq_bg01:I = 0x7f02012c

.field public static final translator_mic:I = 0x7f02012d

.field public static final translator_mic_0:I = 0x7f02012e

.field public static final translator_mic_0_1:I = 0x7f02012f

.field public static final translator_mic_1:I = 0x7f020130

.field public static final translator_mic_10:I = 0x7f020131

.field public static final translator_mic_11:I = 0x7f020132

.field public static final translator_mic_2:I = 0x7f020133

.field public static final translator_mic_3:I = 0x7f020134

.field public static final translator_mic_4:I = 0x7f020135

.field public static final translator_mic_5:I = 0x7f020136

.field public static final translator_mic_6:I = 0x7f020137

.field public static final translator_mic_7:I = 0x7f020138

.field public static final translator_mic_8:I = 0x7f020139

.field public static final translator_mic_9:I = 0x7f02013a

.field public static final translator_mic_bg:I = 0x7f02013b

.field public static final translator_mic_loading:I = 0x7f02013c

.field public static final translator_process_bg:I = 0x7f02013d

.field public static final translator_que_bg:I = 0x7f02013e

.field public static final translator_standby_mic:I = 0x7f02013f

.field public static final translator_standby_mic_bg:I = 0x7f020140

.field public static final translator_standby_mic_bg_focus:I = 0x7f020141

.field public static final translator_standby_mic_bg_press:I = 0x7f020142

.field public static final transparency_thumbnail_cover_text_bg:I = 0x7f020143

.field public static final tw_action_bar_icon_cancel_02_disabled_holo_dark:I = 0x7f020144

.field public static final tw_action_bar_icon_check_disabled_holo_dark:I = 0x7f020145

.field public static final tw_action_bar_icon_check_holo_dark:I = 0x7f020146

.field public static final tw_action_bar_icon_check_selector:I = 0x7f020147

.field public static final tw_action_item_background_focused_holo_light:I = 0x7f020148

.field public static final tw_action_item_background_pressed_holo_light:I = 0x7f020149

.field public static final tw_btn_next_depth_disabled_holo_dark:I = 0x7f02014a

.field public static final tw_btn_next_depth_focused_holo_light:I = 0x7f02014b

.field public static final tw_btn_next_depth_holo_dark:I = 0x7f02014c

.field public static final tw_btn_next_depth_holo_light:I = 0x7f02014d

.field public static final tw_btn_next_depth_pressed_holo_dark:I = 0x7f02014e

.field public static final tw_btn_next_depth_pressed_holo_light:I = 0x7f02014f

.field public static final tw_btn_reorder_disabled_focused_holo_light:I = 0x7f020150

.field public static final tw_btn_reorder_disabled_holo_light:I = 0x7f020151

.field public static final tw_btn_reorder_focused_holo_light:I = 0x7f020152

.field public static final tw_btn_reorder_holo_light:I = 0x7f020153

.field public static final tw_btn_reorder_pressed_holo_light:I = 0x7f020154

.field public static final tw_btn_round_more_disabled_focused_holo_light:I = 0x7f020155

.field public static final tw_btn_round_more_disabled_holo_light:I = 0x7f020156

.field public static final tw_btn_round_more_focused_holo_light:I = 0x7f020157

.field public static final tw_btn_round_more_holo_light:I = 0x7f020158

.field public static final tw_btn_round_more_pressed_holo_light:I = 0x7f020159

.field public static final tw_btn_thumb_check_off_focused_holo_light:I = 0x7f02015a

.field public static final tw_btn_thumb_check_off_holo_light:I = 0x7f02015b

.field public static final tw_btn_thumb_check_off_pressed_holo_light:I = 0x7f02015c

.field public static final tw_btn_thumb_check_on_focused_holo_light:I = 0x7f02015d

.field public static final tw_btn_thumb_check_on_holo_light:I = 0x7f02015e

.field public static final tw_btn_thumb_check_on_pressed_holo_light:I = 0x7f02015f

.field public static final tw_buttonbarbutton_selector_default_holo_light:I = 0x7f020160

.field public static final tw_buttonbarbutton_selector_default_line:I = 0x7f020161

.field public static final tw_buttonbarbutton_selector_disabled_focused_holo_light:I = 0x7f020162

.field public static final tw_buttonbarbutton_selector_disabled_holo_light:I = 0x7f020163

.field public static final tw_buttonbarbutton_selector_focused_holo_light:I = 0x7f020164

.field public static final tw_buttonbarbutton_selector_pressed_holo_light:I = 0x7f020165

.field public static final tw_buttonbarbutton_selector_selected_holo_light:I = 0x7f020166

.field public static final tw_cab_background_top_holo_dark:I = 0x7f020167

.field public static final tw_create_bg_holo_light:I = 0x7f020168

.field public static final tw_dialog_bottom_medium_holo_light:I = 0x7f020169

.field public static final tw_dialog_middle_holo_light:I = 0x7f02016a

.field public static final tw_ic_menu_done_dim_holo_light:I = 0x7f02016b

.field public static final tw_ic_menu_done_holo_light:I = 0x7f02016c

.field public static final tw_list_divider_holo_light:I = 0x7f02016d

.field public static final tw_list_focuse_selected_holo_light:I = 0x7f02016e

.field public static final tw_list_focused_holo_dark:I = 0x7f02016f

.field public static final tw_list_focused_holo_light:I = 0x7f020170

.field public static final tw_list_icon_minus_mtrl:I = 0x7f020171

.field public static final tw_list_icon_reorder_disabled_focused_holo_dark:I = 0x7f020172

.field public static final tw_list_icon_reorder_disabled_holo_dark:I = 0x7f020173

.field public static final tw_list_icon_reorder_focused_holo_dark:I = 0x7f020174

.field public static final tw_list_icon_reorder_holo_dark:I = 0x7f020175

.field public static final tw_list_icon_reorder_pressed_holo_dark:I = 0x7f020176

.field public static final tw_list_pressed_holo_dark:I = 0x7f020177

.field public static final tw_list_pressed_holo_light:I = 0x7f020178

.field public static final tw_list_section_divider_holo_light:I = 0x7f020179

.field public static final tw_list_selected_holo_dark:I = 0x7f02017a

.field public static final tw_list_selected_holo_light:I = 0x7f02017b

.field public static final tw_list_selector:I = 0x7f02017c

.field public static final tw_select_all_bg_holo_dark:I = 0x7f02017d

.field public static final tw_select_all_bg_holo_light:I = 0x7f02017e

.field public static final tw_spinner_ab_default_holo_light:I = 0x7f02017f

.field public static final tw_spinner_ab_disabled_holo_light:I = 0x7f020180

.field public static final tw_spinner_ab_focused_holo_light:I = 0x7f020181

.field public static final tw_spinner_ab_pressed_holo_light:I = 0x7f020182

.field public static final tw_spinner_ab_selected_holo_light:I = 0x7f020183

.field public static final tw_spinner_mtrl_am_alpha:I = 0x7f020184

.field public static final tw_sub_action_bar_bg_holo_dark:I = 0x7f020185

.field public static final video_progress_bar:I = 0x7f020186

.field public static final video_progress_bg:I = 0x7f020187

.field public static final view_mic_idle:I = 0x7f020188

.field public static final view_mic_listening:I = 0x7f020189

.field public static final view_mic_prepare:I = 0x7f02018a

.field public static final view_mic_recognizing:I = 0x7f02018b

.field public static final web_cp_logo_baidu:I = 0x7f02018c

.field public static final web_cp_logo_google:I = 0x7f02018d

.field public static final websearch_category:I = 0x7f02018e

.field public static final writing_symbol:I = 0x7f02018f

.field public static final zoom_title_bg_01:I = 0x7f020190


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
