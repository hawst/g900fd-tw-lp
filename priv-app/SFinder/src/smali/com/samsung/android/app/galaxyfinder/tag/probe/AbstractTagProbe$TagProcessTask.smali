.class public Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;
.super Ljava/lang/Object;
.source "AbstractTagProbe.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "TagProcessTask"
.end annotation


# instance fields
.field private mInfo:Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;)V
    .locals 0
    .param p2, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;->mInfo:Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

    .line 215
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 219
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;->mInfo:Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;->mInfo:Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->collect(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->store(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;Ljava/lang/Object;)Z

    move-result v0

    .line 220
    .local v0, "bResult":Z
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->access$300(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 221
    .local v1, "msg":Landroid/os/Message;
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 222
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$TagProcessTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->mMainHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->access$300(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 223
    return-void
.end method
