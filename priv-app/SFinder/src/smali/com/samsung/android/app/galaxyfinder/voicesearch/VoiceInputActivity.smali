.class public Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;
.super Landroid/app/Activity;
.source "VoiceInputActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/nuance/nmdp/speechkit/Recognizer$Listener;
.implements Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$9;,
        Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;,
        Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$SineEaseOut;,
        Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;
    }
.end annotation


# static fields
.field private static final ANIM_FRAME_RATE:D = 33.333333333333336

.field private static final ANIM_GREEN_DOT_DURATION_TIME:D = 2333.3333333333335

.field private static final MSG_GET_AUDIO_LEVEL:I = 0x4

.field private static final MSG_RECOGNIZER_START:I = 0x0

.field private static final MSG_RECOGNIZER_START_RECORDING:I = 0x2

.field private static final MSG_UPDATE_PROCESSING:I = 0x3

.field private static final MSG_UPDATE_RMS:I = 0x1

.field private static final SINE_OUT:Landroid/view/animation/Interpolator;

.field private static isRestore:Z

.field private static mAnimationDrawable:Landroid/graphics/drawable/AnimationDrawable;

.field private static mAnimatorListening:Landroid/view/ViewPropertyAnimator;

.field private static mAnimatorRecognizing:Landroid/view/ViewPropertyAnimator;

.field private static mRatio:F


# instance fields
.field private mAudioLevelArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioManager:Landroid/media/AudioManager;

.field private mButtonCancel:Landroid/widget/Button;

.field private mButtonDivider:Landroid/widget/ImageView;

.field private mButtonRetry:Landroid/widget/Button;

.field mCheck:Z

.field private mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

.field private mImageButtonMicrophone:Landroid/widget/ImageButton;

.field private mImageViewAnimationListening:Landroid/widget/ImageView;

.field private mImageViewAnimationRecognizing:Landroid/widget/ImageView;

.field private mImageViewMicrophone:Landroid/widget/ImageView;

.field private mImageViewMicrophoneCircle:Landroid/widget/ImageView;

.field private mIsAlreadySpeechKitReleased:Z

.field private mLanguageCode:Ljava/lang/String;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPrompt:Ljava/lang/String;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

.field private mSoundCorrect:I

.field private mSoundPool:Landroid/media/SoundPool;

.field private mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

.field private mTextViewMessage:Landroid/widget/TextView;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 495
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$SineEaseOut;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$SineEaseOut;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->SINE_OUT:Landroid/view/animation/Interpolator;

    .line 503
    const/high16 v0, -0x40800000    # -1.0f

    sput v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRatio:F

    .line 505
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isRestore:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 86
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    .line 130
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$1;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 493
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mCheck:Z

    .line 818
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->removeAllHandlerMessage()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->startRecognizer()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->updateRms()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Lcom/nuance/nmdp/speechkit/Recognizer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->updateProcessing()V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getAudioLevel()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setState(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonDivider:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500()Z
    .locals 1

    .prologue
    .line 65
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isRestore:Z

    return v0
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 65
    sput-boolean p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isRestore:Z

    return p0
.end method

.method static synthetic access$600(FJ)V
    .locals 1
    .param p0, "x0"    # F
    .param p1, "x1"    # J

    .prologue
    .line 65
    invoke-static {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->startSacleAnimationReverse(FJ)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800()Landroid/view/ViewPropertyAnimator;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorRecognizing:Landroid/view/ViewPropertyAnimator;

    return-object v0
.end method

.method static synthetic access$802(Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;
    .locals 0
    .param p0, "x0"    # Landroid/view/ViewPropertyAnimator;

    .prologue
    .line 65
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorRecognizing:Landroid/view/ViewPropertyAnimator;

    return-object p0
.end method

.method static synthetic access$902(Landroid/graphics/drawable/AnimationDrawable;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 0
    .param p0, "x0"    # Landroid/graphics/drawable/AnimationDrawable;

    .prologue
    .line 65
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    return-object p0
.end method

.method private getAudioLevel()V
    .locals 0

    .prologue
    .line 491
    return-void
.end method

.method private getNmspLanguageCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 785
    const-string v0, "ko-KR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 786
    const-string v0, "kor-KOR"

    .line 807
    :goto_0
    return-object v0

    .line 787
    :cond_0
    const-string v0, "en-US"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 788
    const-string v0, "eng-USA"

    goto :goto_0

    .line 789
    :cond_1
    const-string v0, "en-GB"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 790
    const-string v0, "eng-GBR"

    goto :goto_0

    .line 791
    :cond_2
    const-string v0, "zh-CN"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 792
    const-string v0, "cmn-CHN"

    goto :goto_0

    .line 793
    :cond_3
    const-string v0, "ja-JP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 794
    const-string v0, "jpn-JPN"

    goto :goto_0

    .line 795
    :cond_4
    const-string v0, "fr-FR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 796
    const-string v0, "fra-FRA"

    goto :goto_0

    .line 797
    :cond_5
    const-string v0, "es-ES"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 798
    const-string v0, "spa-XLA"

    goto :goto_0

    .line 799
    :cond_6
    const-string v0, "de-DE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 800
    const-string v0, "deu-DEU"

    goto :goto_0

    .line 801
    :cond_7
    const-string v0, "it-IT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 802
    const-string v0, "ita-ITA"

    goto :goto_0

    .line 803
    :cond_8
    const-string v0, "pt-BR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 804
    const-string v0, "por-BRA"

    goto :goto_0

    .line 807
    :cond_9
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeAllHandlerMessage()V
    .locals 2

    .prologue
    .line 811
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->removeMessages(I)V

    .line 812
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->removeMessages(I)V

    .line 813
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->removeMessages(I)V

    .line 814
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->removeMessages(I)V

    .line 815
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->removeMessages(I)V

    .line 816
    return-void
.end method

.method private sendResult(Lcom/nuance/nmdp/speechkit/Recognition;)V
    .locals 5
    .param p1, "results"    # Lcom/nuance/nmdp/speechkit/Recognition;

    .prologue
    .line 466
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 468
    .local v1, "intent":Landroid/content/Intent;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 469
    .local v3, "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p1}, Lcom/nuance/nmdp/speechkit/Recognition;->getResultCount()I

    move-result v2

    .line 470
    .local v2, "resultCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 471
    invoke-interface {p1, v0}, Lcom/nuance/nmdp/speechkit/Recognition;->getResult(I)Lcom/nuance/nmdp/speechkit/Recognition$Result;

    move-result-object v4

    invoke-interface {v4}, Lcom/nuance/nmdp/speechkit/Recognition$Result;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 470
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 473
    :cond_0
    const-string v4, "android.speech.extra.RESULTS"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 474
    const/4 v4, -0x1

    invoke-virtual {p0, v4, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setResult(ILandroid/content/Intent;)V

    .line 475
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->finish()V

    .line 476
    return-void
.end method

.method private setState(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V
    .locals 7
    .param p1, "state"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 652
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    .line 654
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$9;->$SwitchMap$com$samsung$android$app$galaxyfinder$voicesearch$VoiceInputActivity$RecognizerState:[I

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 782
    :cond_0
    :goto_0
    return-void

    .line 656
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioLevelArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 658
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    const v1, 0x7f0e008d

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 660
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophone:Landroid/widget/ImageView;

    const v1, 0x7f02018a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 661
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    const v1, 0x7f020029

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 663
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophoneCircle:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 665
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 666
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 668
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 669
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setSoundEffectsEnabled(Z)V

    .line 671
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-virtual {v0, v4, v1, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 674
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 676
    iput-object v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_0

    .line 681
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioLevelArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 683
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophone:Landroid/widget/ImageView;

    const v1, 0x7f020188

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 684
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    const v1, 0x7f020027

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 686
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 687
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 689
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 690
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setSoundEffectsEnabled(Z)V

    .line 692
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 694
    iput-object v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto/16 :goto_0

    .line 699
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mPrompt:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mPrompt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 703
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophone:Landroid/widget/ImageView;

    const v1, 0x7f020189

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 704
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    const v1, 0x7f020028

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 706
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 707
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophone:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->bringToFront()V

    .line 708
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->bringToFront()V

    .line 710
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationListening:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 711
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophoneCircle:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 713
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 714
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSoundEffectsEnabled(Z)V

    .line 717
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mCheck:Z

    .line 718
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRatio:F

    .line 719
    sput-boolean v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isRestore:Z

    .line 720
    sput-object v4, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorListening:Landroid/view/ViewPropertyAnimator;

    .line 721
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->updateRms()V

    .line 731
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 732
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mPowerManager:Landroid/os/PowerManager;

    .line 733
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mPowerManager:Landroid/os/PowerManager;

    const v1, 0x30000006

    const-string v2, "S Translator WakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 736
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto/16 :goto_0

    .line 741
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioLevelArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 743
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    const v1, 0x7f0e008e

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 745
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophone:Landroid/widget/ImageView;

    const v1, 0x7f02018b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 748
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 749
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->bringToFront()V

    .line 751
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 752
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationListening:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 753
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 755
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setSoundEffectsEnabled(Z)V

    .line 772
    sput-object v4, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorRecognizing:Landroid/view/ViewPropertyAnimator;

    .line 773
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->updateProcessing()V

    .line 775
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 776
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 777
    iput-object v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto/16 :goto_0

    .line 654
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private declared-synchronized startRecognizer()V
    .locals 8

    .prologue
    .line 433
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    if-eqz v0, :cond_0

    .line 434
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->getInstance()Lcom/nuance/nmdp/speechkit/SpeechKit;

    move-result-object v0

    const-string v1, "websearch"

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mLanguageCode:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getNmspLanguageCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lcom/nuance/nmdp/speechkit/SpeechKit;->createRecognizer(Ljava/lang/String;ILjava/lang/String;Lcom/nuance/nmdp/speechkit/Recognizer$Listener;Landroid/os/Handler;)Lcom/nuance/nmdp/speechkit/Recognizer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    .line 442
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    if-eqz v0, :cond_1

    .line 443
    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->addOnSpeechKitReleasedListener(Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;)V

    .line 445
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->PREPARING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setState(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V

    .line 447
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mSoundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mSoundCorrect:I

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 448
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463
    :goto_0
    monitor-exit p0

    return-void

    .line 450
    :cond_1
    :try_start_2
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setState(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V

    .line 452
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    const v1, 0x7f0e0033

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 453
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 454
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonDivider:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 456
    :catch_0
    move-exception v7

    .line 457
    .local v7, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setState(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V

    .line 459
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    const v1, 0x7f0e0033

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 461
    const-string v0, "Translator"

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 433
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private startSacleAnimation(FJLandroid/view/animation/Interpolator;)V
    .locals 2
    .param p1, "ratio"    # F
    .param p2, "duration"    # J
    .param p4, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 536
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorListening:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationListening:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 538
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationListening:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 540
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationListening:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorListening:Landroid/view/ViewPropertyAnimator;

    .line 542
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorListening:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$7;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 567
    :cond_0
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isRestore:Z

    if-nez v0, :cond_1

    sget v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRatio:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 574
    :goto_0
    return-void

    .line 570
    :cond_1
    sput p1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRatio:F

    .line 571
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorListening:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 572
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorListening:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private static startSacleAnimationReverse(FJ)V
    .locals 1
    .param p0, "ratio"    # F
    .param p1, "duration"    # J

    .prologue
    .line 577
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorListening:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_1

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isRestore:Z

    if-nez v0, :cond_2

    sget v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRatio:F

    cmpl-float v0, p0, v0

    if-eqz v0, :cond_0

    .line 583
    :cond_2
    sput p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRatio:F

    .line 584
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorListening:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p0}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private updateProcessing()V
    .locals 6

    .prologue
    .line 602
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    sget-object v3, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->RECOGNIZING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    if-nez v2, :cond_3

    .line 603
    :cond_0
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v2, :cond_1

    .line 604
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 606
    :cond_1
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorRecognizing:Landroid/view/ViewPropertyAnimator;

    if-eqz v2, :cond_2

    .line 607
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorRecognizing:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 649
    :cond_2
    :goto_0
    return-void

    .line 612
    :cond_3
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-nez v2, :cond_4

    .line 613
    new-instance v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    .line 614
    .local v0, "animationDrawable":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f040000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .end local v0    # "animationDrawable":Landroid/graphics/drawable/AnimationDrawable;
    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 616
    .restart local v0    # "animationDrawable":Landroid/graphics/drawable/AnimationDrawable;
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$8;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$8;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;Landroid/graphics/drawable/AnimationDrawable;)V

    sput-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 642
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setRotation(F)V

    .line 643
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;

    sget-object v3, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 644
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 646
    .end local v0    # "animationDrawable":Landroid/graphics/drawable/AnimationDrawable;
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 647
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->what:I

    .line 648
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    const-wide/16 v4, 0x21

    invoke-virtual {v2, v1, v4, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private updateRms()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x64

    const/4 v8, 0x1

    .line 508
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    sget-object v5, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->LISTENING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    if-nez v4, :cond_1

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 511
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    invoke-interface {v4}, Lcom/nuance/nmdp/speechkit/Recognizer;->getAudioLevel()F

    move-result v2

    .line 512
    .local v2, "rmsValue":F
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rmsValue:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    const/high16 v4, 0x41f00000    # 30.0f

    cmpg-float v4, v2, v4

    if-gez v4, :cond_2

    .line 515
    const/high16 v2, 0x41f00000    # 30.0f

    .line 517
    :cond_2
    iget-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mCheck:Z

    if-eqz v4, :cond_4

    .line 518
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mCheck:Z

    .line 520
    const/high16 v4, 0x42480000    # 50.0f

    cmpg-float v4, v2, v4

    if-gtz v4, :cond_3

    const v4, 0x3f19999a    # 0.6f

    :goto_1
    mul-float/2addr v2, v4

    .line 521
    float-to-double v4, v2

    const-wide v6, 0x3f947ae147ae147bL    # 0.02

    mul-double/2addr v4, v6

    const-wide v6, 0x3fdccccccccccccdL    # 0.45

    add-double/2addr v4, v6

    double-to-float v3, v4

    .line 522
    .local v3, "scaleRatio":F
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 523
    .local v0, "deger":D
    float-to-double v4, v3

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    div-double/2addr v4, v0

    double-to-float v3, v4

    .line 525
    const-wide/16 v4, 0xc8

    sget-object v6, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->startSacleAnimation(FJLandroid/view/animation/Interpolator;)V

    .line 527
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    invoke-virtual {v4, v8, v10, v11}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 520
    .end local v0    # "deger":D
    .end local v3    # "scaleRatio":F
    :cond_3
    const/high16 v4, 0x3fc00000    # 1.5f

    goto :goto_1

    .line 529
    :cond_4
    iput-boolean v8, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mCheck:Z

    .line 531
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    invoke-virtual {v4, v8, v10, v11}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 306
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 308
    .local v1, "id":I
    packed-switch v1, :pswitch_data_0

    .line 343
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 310
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    if-eqz v2, :cond_1

    .line 311
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    invoke-interface {v2}, Lcom/nuance/nmdp/speechkit/Recognizer;->cancel()V

    .line 313
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->finish()V

    goto :goto_0

    .line 318
    :pswitch_2
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 319
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 321
    :cond_2
    const v2, 0x7f0e0034

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 325
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->startRecognizer()V

    goto :goto_0

    .line 329
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    sget-object v3, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    if-ne v2, v3, :cond_4

    .line 330
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->startRecognizer()V

    goto :goto_0

    .line 331
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    sget-object v3, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->LISTENING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    if-ne v2, v3, :cond_5

    .line 332
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    if-eqz v2, :cond_0

    .line 333
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    invoke-interface {v2}, Lcom/nuance/nmdp/speechkit/Recognizer;->stopRecording()V

    goto :goto_0

    .line 335
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    sget-object v3, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->RECOGNIZING:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    if-ne v2, v3, :cond_0

    .line 336
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    if-eqz v2, :cond_0

    .line 337
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    invoke-interface {v2}, Lcom/nuance/nmdp/speechkit/Recognizer;->cancel()V

    goto :goto_0

    .line 308
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b000e
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x1

    const v7, 0x3dcccccd    # 0.1f

    const/4 v10, 0x0

    const/16 v9, 0x8

    .line 142
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 146
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 149
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 152
    invoke-virtual {p0, v11}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->requestWindowFeature(I)Z

    .line 154
    invoke-virtual {p0, v12}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setVolumeControlStream(I)V

    .line 156
    const/4 v5, 0x2

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 157
    const/high16 v5, 0x3f000000    # 0.5f

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 158
    const v5, 0x7f030002

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setContentView(I)V

    .line 160
    const v5, 0x7f0b0009

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    .line 163
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c002a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-float v5, v5

    mul-float v4, v5, v7

    .line 166
    .local v4, "shadowDy":F
    mul-float v3, v4, v7

    .line 167
    .local v3, "radius":F
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090068

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v5, v3, v6, v4, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 170
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    new-instance v6, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$2;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$2;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 194
    const v5, 0x7f0b000a

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationListening:Landroid/widget/ImageView;

    .line 195
    const v5, 0x7f0b000b

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;

    .line 196
    const v5, 0x7f0b000d

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophone:Landroid/widget/ImageView;

    .line 197
    const v5, 0x7f0b000e

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    .line 198
    const v5, 0x7f0b000c

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophoneCircle:Landroid/widget/ImageView;

    .line 199
    const v5, 0x7f0b000f

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonCancel:Landroid/widget/Button;

    .line 200
    const v5, 0x7f0b0011

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;

    .line 201
    const v5, 0x7f0b0010

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonDivider:Landroid/widget/ImageView;

    .line 204
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;

    invoke-virtual {v5, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 205
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonDivider:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 206
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationListening:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 207
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 208
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewMicrophoneCircle:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 209
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 211
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonCancel:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v7}, Landroid/widget/ImageButton;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f0e000c

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 218
    new-instance v5, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    .line 220
    const-string v5, "audio"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/media/AudioManager;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 222
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioLevelArray:Ljava/util/ArrayList;

    .line 224
    new-instance v5, Landroid/media/SoundPool;

    const/4 v6, 0x5

    invoke-direct {v5, v6, v12, v10}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mSoundPool:Landroid/media/SoundPool;

    .line 225
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f060001

    invoke-virtual {v5, v6, v7, v11}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mSoundCorrect:I

    .line 227
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 228
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "android.speech.extra.LANGUAGE"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mLanguageCode:Ljava/lang/String;

    .line 231
    const v5, 0x7f0e00b5

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mPrompt:Ljava/lang/String;

    .line 237
    new-instance v0, Landroid/content/IntentFilter;

    const-string v5, "com.sec.android.app.translator.MAIN_FRAGMENT_FINISHED"

    invoke-direct {v0, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 238
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 242
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mHandler:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;

    const-wide/16 v6, 0xfa

    invoke-virtual {v5, v10, v6, v7}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 244
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setState(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V

    .line 245
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->removeAllHandlerMessage()V

    .line 285
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 287
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setState(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V

    .line 289
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->isReleased()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mIsAlreadySpeechKitReleased:Z

    if-nez v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/Recognizer;->cancel()V

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_1

    .line 295
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mSoundPool:Landroid/media/SoundPool;

    .line 299
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->removeOnSpeechKitReleasedListener(Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper$OnSpeechKitReleasedListener;)V

    .line 301
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 302
    return-void
.end method

.method public onError(Lcom/nuance/nmdp/speechkit/Recognizer;Lcom/nuance/nmdp/speechkit/SpeechError;)V
    .locals 5
    .param p1, "recognizer"    # Lcom/nuance/nmdp/speechkit/Recognizer;
    .param p2, "error"    # Lcom/nuance/nmdp/speechkit/SpeechError;

    .prologue
    .line 347
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 348
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 350
    const-string v2, ""

    .line 351
    .local v2, "msg":Ljava/lang/String;
    invoke-interface {p2}, Lcom/nuance/nmdp/speechkit/SpeechError;->getErrorCode()I

    move-result v0

    .line 352
    .local v0, "errorCode":I
    packed-switch v0, :pswitch_data_0

    .line 369
    :pswitch_0
    const v3, 0x7f0e0037

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 374
    :goto_0
    move-object v1, v2

    .line 375
    .local v1, "message":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;

    invoke-direct {v3, p0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$3;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 385
    return-void

    .line 354
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_1
    const v3, 0x7f0e00cd

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 355
    goto :goto_0

    .line 358
    :pswitch_2
    const v3, 0x7f0e0036

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 359
    goto :goto_0

    .line 363
    :pswitch_3
    const v3, 0x7f0e007b

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 364
    goto :goto_0

    .line 352
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 259
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->removeAllHandlerMessage()V

    .line 261
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mState:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/SpeechKitWrapper;->isReleased()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mIsAlreadySpeechKitReleased:Z

    if-nez v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/Recognizer;->cancel()V

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 266
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 268
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mTextViewMessage:Landroid/widget/TextView;

    const v1, 0x7f0e00cd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 269
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;->IDLE:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->setState(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerState;)V

    .line 270
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 275
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonCancel:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 276
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 278
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 279
    return-void
.end method

.method public onRecordingBegin(Lcom/nuance/nmdp/speechkit/Recognizer;)V
    .locals 4
    .param p1, "recognizer"    # Lcom/nuance/nmdp/speechkit/Recognizer;

    .prologue
    .line 389
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 391
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V

    .line 393
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$4;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 399
    return-void
.end method

.method public onRecordingDone(Lcom/nuance/nmdp/speechkit/Recognizer;)V
    .locals 2
    .param p1, "recognizer"    # Lcom/nuance/nmdp/speechkit/Recognizer;

    .prologue
    .line 403
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 404
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 406
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$5;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 412
    return-void
.end method

.method public onResults(Lcom/nuance/nmdp/speechkit/Recognizer;Lcom/nuance/nmdp/speechkit/Recognition;)V
    .locals 1
    .param p1, "recognizer"    # Lcom/nuance/nmdp/speechkit/Recognizer;
    .param p2, "results"    # Lcom/nuance/nmdp/speechkit/Recognition;

    .prologue
    .line 416
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$6;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 423
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->sendResult(Lcom/nuance/nmdp/speechkit/Recognition;)V

    .line 424
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 249
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 252
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageButtonMicrophone:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 253
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonCancel:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 254
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mButtonRetry:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 255
    return-void
.end method

.method public onSpeechKitReleased()V
    .locals 1

    .prologue
    .line 428
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mIsAlreadySpeechKitReleased:Z

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;

    .line 430
    return-void
.end method
