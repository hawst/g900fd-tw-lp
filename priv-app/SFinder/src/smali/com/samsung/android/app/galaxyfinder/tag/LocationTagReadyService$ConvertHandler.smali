.class final Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;
.super Landroid/os/Handler;
.source "LocationTagReadyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ConvertHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    .line 129
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 130
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 134
    if-eqz p1, :cond_0

    .line 135
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    .line 162
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Unknown request message!!!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 137
    :pswitch_0
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    const/4 v8, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->bWaitingDelayedMessage:Z
    invoke-static {v7, v8}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$002(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Z)Z

    .line 138
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->arg1:I

    aget-object v5, v7, v8

    .line 139
    .local v5, "type":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->arg2:I

    aget-object v3, v7, v8

    .line 140
    .local v3, "reqtype":Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    const/4 v1, 0x0

    .line 141
    .local v1, "contentUri":Landroid/net/Uri;
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v7, :cond_1

    .line 142
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v1    # "contentUri":Landroid/net/Uri;
    check-cast v1, Landroid/net/Uri;

    .line 144
    .restart local v1    # "contentUri":Landroid/net/Uri;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->convertAddress(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V
    invoke-static {v7, v3, v5, v1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$100(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/net/Uri;)V

    goto :goto_0

    .line 148
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v3    # "reqtype":Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .end local v5    # "type":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    :pswitch_1
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/net/Uri;

    .line 149
    .local v6, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->arg1:I

    aget-object v4, v7, v8

    .line 150
    .local v4, "requestType":Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 151
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v7, "media_ids"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 153
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 154
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleMessage() RESPONSE_FETCH_DELAY uri : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleMessage() requestType : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", list : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->sendResponse(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/Object;)V
    invoke-static {v7, v4, v6, v2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$300(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
