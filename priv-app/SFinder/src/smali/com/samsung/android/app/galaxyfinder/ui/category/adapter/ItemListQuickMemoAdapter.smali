.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListQuickMemoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 20
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v4

    .line 25
    .local v4, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v1, 0x0

    .line 27
    .local v1, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;
    invoke-virtual {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 29
    if-nez p2, :cond_0

    .line 30
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f030048

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 33
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;

    .end local v1    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;
    invoke-direct {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;-><init>()V

    .line 35
    .restart local v1    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;
    const v5, 0x7f0b004b

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    .line 36
    const v5, 0x7f0b009a

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;->vOverlay:Landroid/view/View;

    .line 38
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 43
    :goto_0
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "album":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionInkData()Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "inkData":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v3

    .line 48
    .local v3, "intent":Landroid/content/Intent;
    iget-object v5, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    const/4 v6, -0x1

    invoke-virtual {p0, v5, v0, v2, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 50
    invoke-virtual {p0, p2, v3, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 52
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    return-object v5

    .line 40
    .end local v0    # "album":Ljava/lang/String;
    .end local v2    # "inkData":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;
    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;

    .restart local v1    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListQuickMemoAdapter$QuickmemoItemListViewHolder;
    goto :goto_0
.end method
