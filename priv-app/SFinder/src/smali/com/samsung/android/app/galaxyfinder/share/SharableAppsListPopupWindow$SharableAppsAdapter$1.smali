.class Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;
.super Ljava/lang/Object;
.source "SharableAppsListPopupWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

.field final synthetic val$className:Ljava/lang/String;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->this$1:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->val$className:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 136
    # getter for: Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "item clicked (action : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->this$1:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    # getter for: Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->access$200(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->this$1:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    # getter for: Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mItemClickListener:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->access$300(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->this$1:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->dismiss()V

    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->this$1:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    # getter for: Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->access$200(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->val$packageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->val$className:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 142
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->this$1:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    # getter for: Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mItemClickListener:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->access$300(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter$1;->this$1:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    # getter for: Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->access$200(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;->onItemClicked(Landroid/content/Intent;)V

    .line 144
    :cond_0
    return-void
.end method
