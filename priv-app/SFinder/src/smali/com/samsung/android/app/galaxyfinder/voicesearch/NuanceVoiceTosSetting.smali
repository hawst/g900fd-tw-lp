.class public Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;
.super Ljava/lang/Object;
.source "NuanceVoiceTosSetting.java"


# static fields
.field public static final DATA_DIALOG_OFF:I = 0x1

.field public static final DATA_DIALOG_ON:I = 0x0

.field public static final KEY_PREFERENCES:Ljava/lang/String; = "nuance_voice_preferences"

.field public static final KEY_SHOW_DATA_INFO:Ljava/lang/String; = "show_data_info"

.field public static final KEY_SHOW_POLICY_INFO:Ljava/lang/String; = "show_policy_info"

.field public static final KEY_SHOW_WIFI_INFO:Ljava/lang/String; = "show_wifi_info"

.field public static final NUANCE_DATA_DIALOG:Ljava/lang/String; = "NUANCE_DATA_DIALOG"

.field public static final NUANCE_DIALOG:Ljava/lang/String; = "NUANCE_DIALOG"

.field public static final NUANCE_WIFI_DIALOG:Ljava/lang/String; = "NUANCE_WIFI_DIALOG"

.field public static final POLICY_DIALOG_OFF:I = 0x1

.field public static final POLICY_DIALOG_ON:I = 0x0

.field private static final TAG:Ljava/lang/String; = "NuanceVoiceTosSetting"

.field public static final WIFI_DIALOG_OFF:I = 0x1

.field public static final WIFI_DIALOG_ON:I

.field private static mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSettings(Landroid/content/Context;Ljava/lang/String;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 59
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->mContext:Landroid/content/Context;

    .line 60
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 65
    :goto_0
    return v1

    .line 63
    :cond_0
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->mContext:Landroid/content/Context;

    const-string v3, "nuance_voice_preferences"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 65
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "0"

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public static initContext(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    const-string v0, "NuanceVoiceTosSetting"

    const-string v1, "initAppContext"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->mContext:Landroid/content/Context;

    .line 45
    return-void
.end method

.method public static isDataConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 70
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 73
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 78
    .local v1, "mobileNetwork":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 79
    const/4 v2, 0x1

    .line 82
    .end local v1    # "mobileNetwork":Landroid/net/NetworkInfo;
    :cond_0
    return v2
.end method

.method public static isWifiConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 87
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 90
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 95
    .local v1, "mobileNetwork":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    .end local v1    # "mobileNetwork":Landroid/net/NetworkInfo;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static setSettings(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 48
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->mContext:Landroid/content/Context;

    .line 49
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 50
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->mContext:Landroid/content/Context;

    const-string v3, "nuance_voice_preferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 52
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 53
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 54
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 56
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method
