.class Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;
.super Ljava/lang/Object;
.source "PeopleTagProbe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FacesCursorHelper"
.end annotation


# instance fields
.field private mCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 427
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    .line 428
    return-void
.end method


# virtual methods
.method public getFaceID()I
    .locals 3

    .prologue
    .line 444
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    const-string v2, "face_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getID()I
    .locals 3

    .prologue
    .line 431
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getImageID()I
    .locals 3

    .prologue
    .line 435
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    const-string v2, "image_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 449
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    const-string v2, "name"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPersonID()I
    .locals 3

    .prologue
    .line 439
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesCursorHelper;->mCursor:Landroid/database/Cursor;

    const-string v2, "person_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method
