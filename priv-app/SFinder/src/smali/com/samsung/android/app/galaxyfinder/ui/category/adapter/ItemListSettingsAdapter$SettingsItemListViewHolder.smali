.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;
.super Ljava/lang/Object;
.source "ItemListSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SettingsItemListViewHolder"
.end annotation


# instance fields
.field public ivCheck:Landroid/widget/CheckBox;

.field public ivIcon:Landroid/widget/ImageView;

.field public ivLink:Landroid/widget/ImageView;

.field public ivSwitch:Landroid/widget/Switch;

.field public tvDesc:Landroid/widget/TextView;

.field public tvName:Landroid/widget/TextView;

.field public vgIconGroup:Landroid/view/ViewGroup;

.field public vgItemGroup:Landroid/view/ViewGroup;

.field public vgSubHeader:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 317
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    .line 319
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 321
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    .line 323
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    .line 325
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivLink:Landroid/widget/ImageView;

    .line 327
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgIconGroup:Landroid/view/ViewGroup;

    .line 329
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    .line 331
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    return-void
.end method
