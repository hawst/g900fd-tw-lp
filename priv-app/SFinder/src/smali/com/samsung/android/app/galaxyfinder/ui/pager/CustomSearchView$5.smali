.class Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;
.super Ljava/lang/Object;
.source "CustomSearchView.java"

# interfaces
.implements Lcom/samsung/android/writingbuddy/WritingBuddyImpl$OnPrivateCommandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrivateCommand(ILjava/lang/CharSequence;Landroid/os/Bundle;)Z
    .locals 5
    .param p1, "cmdId"    # I
    .param p2, "cmdText"    # Ljava/lang/CharSequence;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 289
    if-ne p1, v4, :cond_2

    .line 290
    const-string v2, "OPENED"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddyShown:Z
    invoke-static {v2, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$202(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;Z)Z

    .line 293
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->hideKeypad()V

    .line 294
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 295
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;

    .line 296
    .local v1, "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    const-string v2, "OPENED"

    invoke-interface {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;->onWritingBuddyMode(Ljava/lang/String;)V

    goto :goto_0

    .line 299
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    :cond_0
    const-string v2, "CLOSED"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 300
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 301
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mQueryTextListener:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;

    .line 302
    .restart local v1    # "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    const-string v2, "CLOSED"

    invoke-interface {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;->onWritingBuddyMode(Ljava/lang/String;)V

    goto :goto_1

    .line 305
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$5;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mWritingBuddyShown:Z
    invoke-static {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$202(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;Z)Z

    .line 308
    :cond_2
    return v3
.end method
