.class Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;
.super Ljava/lang/Object;
.source "FilterPanelMainView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "group"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x4

    .line 60
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 61
    const-string v2, "TEST"

    const-string v3, "Now animating"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getSelectedItemIndex(Landroid/view/View;)I
    invoke-static {v2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;Landroid/view/View;)I

    move-result v1

    .line 65
    .local v1, "index":I
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->isExistingOpenedPanel()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getCurrentItem()I

    move-result v2

    if-eq v2, v1, :cond_1

    if-eq v1, v4, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getCurrentItem()I

    move-result v2

    if-eq v2, v4, :cond_1

    .line 69
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f040004

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 71
    .local v0, "ani":Landroid/view/animation/Animation;
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1$1;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 87
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 101
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :goto_0
    return-void

    .line 98
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 99
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->onClickProcess(Landroid/view/View;)V

    goto :goto_0

    .line 98
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
