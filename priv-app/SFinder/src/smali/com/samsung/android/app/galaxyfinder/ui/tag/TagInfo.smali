.class public Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;
.super Ljava/lang/Object;
.source "TagInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;
    }
.end annotation


# static fields
.field public static final TAG_KEY_TAG:Ljava/lang/String; = "tag"


# instance fields
.field public final DEFAULT_ID:Ljava/lang/String;

.field public final TAG:Ljava/lang/String;

.field public final TAG_KEY_CATEGORY:Ljava/lang/String;

.field public final TAG_KEY_ENCODING:Ljava/lang/String;

.field public final TAG_KEY_ID:Ljava/lang/String;

.field public final TAG_KEY_TAGS:Ljava/lang/String;

.field public final TAG_KEY_VALUE:Ljava/lang/String;

.field private mJTags:Lorg/json/JSONArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->TAG:Ljava/lang/String;

    .line 14
    const-string v0, "-"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->DEFAULT_ID:Ljava/lang/String;

    .line 16
    const-string v0, "id"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->TAG_KEY_ID:Ljava/lang/String;

    .line 18
    const-string v0, "category"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->TAG_KEY_CATEGORY:Ljava/lang/String;

    .line 20
    const-string v0, "encoding"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->TAG_KEY_ENCODING:Ljava/lang/String;

    .line 22
    const-string v0, "value"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->TAG_KEY_VALUE:Ljava/lang/String;

    .line 24
    const-string v0, "tags"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->TAG_KEY_TAGS:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->mJTags:Lorg/json/JSONArray;

    .line 31
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->mJTags:Lorg/json/JSONArray;

    .line 32
    return-void
.end method

.method private findJSONObjectById(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 85
    const/4 v3, 0x0

    .line 86
    .local v3, "obj":Lorg/json/JSONObject;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->mJTags:Lorg/json/JSONArray;

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 88
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 90
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->mJTags:Lorg/json/JSONArray;

    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    .line 92
    .local v4, "source":Lorg/json/JSONObject;
    const-string v5, "id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    .line 93
    move-object v3, v4

    .line 101
    .end local v4    # "source":Lorg/json/JSONObject;
    :cond_0
    return-object v3

    .line 96
    :catch_0
    move-exception v1

    .line 97
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 88
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addTag(Ljava/lang/Object;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;

    .prologue
    .line 35
    const-string v0, "-"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->addTag(Ljava/lang/String;Ljava/lang/Object;)V

    .line 36
    return-void
.end method

.method public addTag(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/Object;

    .prologue
    .line 39
    const/4 v1, 0x0

    .line 40
    .local v1, "jarray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .line 42
    .local v2, "jobject":Lorg/json/JSONObject;
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->findJSONObjectById(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 44
    if-nez v2, :cond_1

    .line 45
    new-instance v2, Lorg/json/JSONObject;

    .end local v2    # "jobject":Lorg/json/JSONObject;
    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 48
    .restart local v2    # "jobject":Lorg/json/JSONObject;
    :try_start_0
    const-string v3, "id"

    invoke-virtual {v2, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    new-instance v1, Lorg/json/JSONArray;

    .end local v1    # "jarray":Lorg/json/JSONArray;
    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 56
    .restart local v1    # "jarray":Lorg/json/JSONArray;
    :try_start_1
    const-string v3, "tags"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 61
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->mJTags:Lorg/json/JSONArray;

    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 70
    :goto_2
    if-eqz v1, :cond_0

    .line 71
    instance-of v3, p2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;

    if-eqz v3, :cond_2

    .line 72
    check-cast p2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;

    .end local p2    # "tag":Ljava/lang/Object;
    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->get()Lorg/json/JSONObject;
    invoke-static {p2}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 82
    :cond_0
    :goto_3
    return-void

    .line 49
    .restart local p2    # "tag":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 57
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 58
    .restart local v0    # "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 64
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    :try_start_2
    const-string v3, "tags"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    goto :goto_2

    .line 65
    :catch_2
    move-exception v0

    .line 66
    .restart local v0    # "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 74
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    instance-of v3, p2, Lorg/json/JSONObject;

    if-eqz v3, :cond_3

    .line 75
    invoke-virtual {v1, p2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_3

    .line 77
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->TAG:Ljava/lang/String;

    const-string v4, "unknown object type"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public newTag()Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo$TagBuilder;-><init>()V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 110
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->mJTags:Lorg/json/JSONArray;

    if-nez v2, :cond_0

    .line 111
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 123
    :goto_0
    return-object v2

    .line 116
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->mJTags:Lorg/json/JSONArray;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    .end local v1    # "str":Ljava/lang/String;
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagInfo;->mJTags:Lorg/json/JSONArray;

    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
