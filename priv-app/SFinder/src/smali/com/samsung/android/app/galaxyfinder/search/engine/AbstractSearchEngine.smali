.class public abstract Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;
.super Ljava/lang/Object;
.source "AbstractSearchEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;
    }
.end annotation


# instance fields
.field public final TAG:Ljava/lang/String;

.field public mContext:Landroid/content/Context;

.field public mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

.field public mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, "SearchEngine"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->TAG:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->mContext:Landroid/content/Context;

    .line 15
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    .line 17
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    .line 20
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->mContext:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract prepare(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)I
.end method

.method public setListener(Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;->mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    .line 31
    return-void
.end method

.method public abstract start()V
.end method
