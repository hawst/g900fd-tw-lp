.class Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;
.super Landroid/database/DataSetObserver;
.source "FilterGroupDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 6

    .prologue
    .line 46
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 47
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->removeAllViews()V

    .line 48
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->getCount()I

    move-result v0

    .line 49
    .local v0, "adapterCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 50
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    invoke-virtual {v3, v2, v4, v5}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 52
    .local v1, "child":Landroid/view/View;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    invoke-virtual {v3, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 53
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    invoke-virtual {v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->addView(Landroid/view/View;)V

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 58
    .end local v1    # "child":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->requestLayout()V

    .line 59
    return-void
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    .line 64
    return-void
.end method
