.class Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;
.super Ljava/lang/Object;
.source "MediaFileUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaFileType"
.end annotation


# instance fields
.field private description:Ljava/lang/String;

.field private fileType:I

.field private mimeType:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "fileType"    # I
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "desc"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;->fileType:I

    .line 66
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;->mimeType:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;->description:Ljava/lang/String;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;->mimeType:Ljava/lang/String;

    return-object v0
.end method
