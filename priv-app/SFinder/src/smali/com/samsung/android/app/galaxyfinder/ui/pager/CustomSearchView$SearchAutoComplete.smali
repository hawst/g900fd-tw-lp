.class public Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
.super Landroid/widget/AutoCompleteTextView;
.source "CustomSearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchAutoComplete"
.end annotation


# instance fields
.field private mIsDropDownShowing:Z

.field private mMaxDropDownHeight:I

.field private mMaxDropDownLines:I

.field private mThreshold:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 706
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 697
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownLines:I

    .line 699
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownHeight:I

    .line 701
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mThreshold:I

    .line 703
    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mIsDropDownShowing:Z

    .line 707
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->init()V

    .line 708
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 711
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 697
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownLines:I

    .line 699
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownHeight:I

    .line 701
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mThreshold:I

    .line 703
    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mIsDropDownShowing:Z

    .line 712
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->init()V

    .line 713
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 716
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 697
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownLines:I

    .line 699
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownHeight:I

    .line 701
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mThreshold:I

    .line 703
    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mIsDropDownShowing:Z

    .line 717
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->init()V

    .line 718
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 721
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 723
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0c0023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownLines:I

    .line 724
    const v1, 0x7f0a043a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownLines:I

    mul-int/2addr v1, v2

    const v2, 0x7f0a042b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownHeight:I

    .line 728
    return-void
.end method


# virtual methods
.method public enoughToFilter()Z
    .locals 2

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mThreshold:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public forceDismissDropDown()V
    .locals 1

    .prologue
    .line 811
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mIsDropDownShowing:Z

    .line 813
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 814
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->dismissDropDown()V

    .line 816
    :cond_0
    return-void
.end method

.method public forceUpdateDropDownList(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 819
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->enoughToFilter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 820
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 821
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->performFiltering(Ljava/lang/CharSequence;I)V

    .line 824
    :cond_0
    return-void
.end method

.method public getThreshold()I
    .locals 1

    .prologue
    .line 783
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mThreshold:I

    return v0
.end method

.method public isPopupShowingLogically()Z
    .locals 1

    .prologue
    .line 807
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mIsDropDownShowing:Z

    return v0
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 737
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 738
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 739
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v0

    .line 741
    .local v0, "state":Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_0

    .line 742
    invoke-virtual {v0, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    .line 745
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 746
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->forceDismissDropDown()V

    .line 747
    const/4 v1, 0x0

    .line 752
    .end local v0    # "state":Landroid/view/KeyEvent$DispatcherState;
    :goto_0
    return v1

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 797
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onWindowVisibilityChanged(I)V

    .line 799
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mIsDropDownShowing:Z

    .line 800
    return-void
.end method

.method protected performFiltering(Ljava/lang/CharSequence;I)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "keyCode"    # I

    .prologue
    .line 757
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .line 759
    .local v0, "adapter":Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    if-eqz v0, :cond_0

    .line 760
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->setKeyValue(Ljava/lang/String;)V

    .line 763
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->performFiltering(Ljava/lang/CharSequence;I)V

    .line 764
    return-void
.end method

.method public setPopupState(Z)V
    .locals 0
    .param p1, "showing"    # Z

    .prologue
    .line 803
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mIsDropDownShowing:Z

    .line 804
    return-void
.end method

.method public setThreshold(I)V
    .locals 1
    .param p1, "threshold"    # I

    .prologue
    .line 788
    if-gtz p1, :cond_0

    .line 789
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mThreshold:I

    .line 793
    :goto_0
    return-void

    .line 791
    :cond_0
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mThreshold:I

    goto :goto_0
.end method

.method public showDropDown()V
    .locals 2

    .prologue
    .line 768
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownLines:I

    if-le v0, v1, :cond_1

    .line 769
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mMaxDropDownHeight:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setDropDownHeight(I)V

    .line 774
    :goto_0
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V

    .line 776
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 777
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->mIsDropDownShowing:Z

    .line 779
    :cond_0
    return-void

    .line 771
    :cond_1
    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setDropDownHeight(I)V

    goto :goto_0
.end method
