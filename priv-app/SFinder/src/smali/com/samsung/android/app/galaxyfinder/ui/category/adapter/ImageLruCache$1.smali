.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache$1;
.super Landroid/util/LruCache;
.source "ImageLruCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/drawable/BitmapDrawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;I)V
    .locals 0
    .param p2, "x0"    # I

    .prologue
    .line 17
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/drawable/BitmapDrawable;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache$1;->sizeOf(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)I

    move-result v0

    return v0
.end method

.method protected sizeOf(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)I
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "drawable"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 21
    const/4 v1, 0x0

    .line 22
    .local v1, "spaceTaken":I
    invoke-virtual {p2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 24
    .local v0, "map":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v1

    .line 27
    :cond_0
    return v1
.end method
