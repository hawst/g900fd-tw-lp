.class public Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
.super Ljava/lang/Object;
.source "CPData.java"


# instance fields
.field private icon:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private params:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->name:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->icon:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->url:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->params:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->name:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->icon:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->url:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->params:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "icon"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->name:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->icon:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->url:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->params:Ljava/lang/String;

    .line 22
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->name:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->icon:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->url:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "icon"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "params"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->name:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->icon:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->url:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->params:Ljava/lang/String;

    .line 28
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->name:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->icon:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->url:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->params:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public getIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->icon:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getParams()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->params:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->url:Ljava/lang/String;

    return-object v0
.end method

.method public setIcon(Ljava/lang/String;)V
    .locals 0
    .param p1, "icon"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->icon:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->name:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setParams(Ljava/lang/String;)V
    .locals 0
    .param p1, "params"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->params:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->url:Ljava/lang/String;

    .line 56
    return-void
.end method
