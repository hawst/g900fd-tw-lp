.class Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;
.super Ljava/lang/Object;
.source "BaseCategoryView.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionClicked(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;->onActionItemClicked(Landroid/view/View;)V

    .line 134
    :cond_0
    return-void
.end method

.method public onActionDragDrop(Z)V
    .locals 1
    .param p1, "started"    # Z

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;->onChangeItemDragDropState(Z)V

    .line 127
    :cond_0
    return-void
.end method

.method public onActionLongPressed(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;->onActionItemLongPressed(Landroid/view/View;)V

    .line 141
    :cond_0
    return-void
.end method

.method public onChangeSelected(Z)V
    .locals 3
    .param p1, "isSelected"    # Z

    .prologue
    .line 102
    if-eqz p1, :cond_1

    .line 103
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # operator++ for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$008(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I

    .line 108
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectableItemCount:I
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 109
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Landroid/widget/CheckBox;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 114
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selected item count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mViewStateListener:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;->onChangeSelectedCount(ILjava/lang/String;)V

    .line 120
    :cond_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # operator-- for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mSelectedItemCount:I
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$010(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)I

    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->mCategoryCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;)Landroid/widget/CheckBox;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method
