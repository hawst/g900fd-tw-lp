.class final enum Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
.super Ljava/lang/Enum;
.source "LocationTagReadyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "MediaType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

.field public static final enum AUDIO:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

.field public static final enum IMAGE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

.field public static final enum INVALIDTYPE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

.field public static final enum VIDEO:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->IMAGE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->VIDEO:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->AUDIO:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    const-string v1, "INVALIDTYPE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->INVALIDTYPE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    .line 81
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->IMAGE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->VIDEO:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->AUDIO:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->INVALIDTYPE:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    invoke-virtual {v0}, [Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    return-object v0
.end method
