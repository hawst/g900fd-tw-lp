.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterExpand()V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchExpaningListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchExpaningListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;->onExpandingSearchList(Z)V

    .line 461
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->updateCurrentScreen()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    .line 462
    return-void
.end method

.method public afterHeadUp()V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->updateCurrentScreen()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    .line 467
    return-void
.end method

.method public beforeExpand()V
    .locals 0

    .prologue
    .line 453
    return-void
.end method

.method public beforeFold()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchExpaningListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchExpaningListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;->onExpandingSearchList(Z)V

    .line 449
    :cond_0
    return-void
.end method

.method public beforeHeadUp()V
    .locals 0

    .prologue
    .line 471
    return-void
.end method
