.class public final enum Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;
.super Ljava/lang/Enum;
.source "QueryBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Priority"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

.field public static final enum AND:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

.field public static final enum OR:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    const-string v1, "AND"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->AND:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    const-string v1, "OR"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->OR:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->AND:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->OR:Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    invoke-virtual {v0}, [Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/galaxyfinder/util/QueryBuilder$Priority;

    return-object v0
.end method
