.class public Lcom/samsung/android/app/galaxyfinder/util/ImageUtils;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# static fields
.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/samsung/android/app/galaxyfinder/util/ImageUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/ImageUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static HighLightBitmap(Landroid/graphics/Bitmap;Ljava/util/List;I)Landroid/graphics/Bitmap;
    .locals 34
    .param p0, "originalBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "reduceScale"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;I)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "rect":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v33

    .line 105
    .local v33, "start":Ljava/lang/Long;
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-object/from16 v20, p0

    .line 185
    :cond_1
    :goto_0
    return-object v20

    .line 109
    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 110
    .local v7, "width":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    .line 112
    .local v11, "height":I
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v11, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 113
    .local v20, "destBitmap":Landroid/graphics/Bitmap;
    mul-int v4, v7, v11

    new-array v5, v4, [I

    .line 116
    .local v5, "pixels":[I
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p0

    move v10, v7

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 117
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, v20

    move v10, v7

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 120
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    .local v24, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Landroid/graphics/Rect;

    .line 121
    .local v31, "rec":Landroid/graphics/Rect;
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int v4, v4, p2

    if-lt v7, v4, :cond_3

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int v4, v4, p2

    if-lt v11, v4, :cond_3

    .line 125
    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Rect;->left:I

    div-int v4, v4, p2

    const/4 v6, 0x1

    if-gt v4, v6, :cond_a

    const/4 v4, 0x1

    :goto_2
    move-object/from16 v0, v31

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 126
    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Rect;->top:I

    div-int v4, v4, p2

    const/4 v6, 0x1

    if-gt v4, v6, :cond_b

    const/4 v4, 0x1

    :goto_3
    move-object/from16 v0, v31

    iput v4, v0, Landroid/graphics/Rect;->top:I

    .line 127
    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Rect;->right:I

    div-int v4, v4, p2

    if-lt v4, v7, :cond_c

    move v4, v7

    :goto_4
    move-object/from16 v0, v31

    iput v4, v0, Landroid/graphics/Rect;->right:I

    .line 128
    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    div-int v4, v4, p2

    if-lt v4, v11, :cond_d

    move v4, v11

    :goto_5
    move-object/from16 v0, v31

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 130
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->height()I

    move-result v6

    mul-int/2addr v4, v6

    new-array v13, v4, [I

    .line 133
    .local v13, "highLightpixels":[I
    const/4 v14, 0x0

    :try_start_0
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->width()I

    move-result v15

    move-object/from16 v0, v31

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v16, v0

    move-object/from16 v0, v31

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->width()I

    move-result v18

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->height()I

    move-result v19

    move-object/from16 v12, p0

    invoke-virtual/range {v12 .. v19}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 136
    array-length v0, v13

    move/from16 v29, v0

    .line 138
    .local v29, "nPixelCount":I
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_6
    move/from16 v0, v23

    move/from16 v1, v29

    if-ge v0, v1, :cond_e

    .line 139
    aget v27, v13, v23

    .line 141
    .local v27, "nColor":I
    invoke-static/range {v27 .. v27}, Landroid/graphics/Color;->alpha(I)I

    move-result v25

    .line 142
    .local v25, "nAlpha":I
    invoke-static/range {v27 .. v27}, Landroid/graphics/Color;->red(I)I

    move-result v30

    .line 143
    .local v30, "nRed":I
    invoke-static/range {v27 .. v27}, Landroid/graphics/Color;->green(I)I

    move-result v28

    .line 144
    .local v28, "nGreen":I
    invoke-static/range {v27 .. v27}, Landroid/graphics/Color;->blue(I)I

    move-result v26

    .line 146
    .local v26, "nBlue":I
    move/from16 v0, v30

    int-to-double v8, v0

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v8, v14

    double-to-int v0, v8

    move/from16 v30, v0

    .line 147
    move/from16 v0, v28

    int-to-double v8, v0

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v8, v14

    double-to-int v0, v8

    move/from16 v28, v0

    .line 148
    move/from16 v0, v26

    int-to-double v8, v0

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v8, v14

    double-to-int v0, v8

    move/from16 v26, v0

    .line 150
    const/16 v4, 0xff

    move/from16 v0, v30

    if-le v0, v4, :cond_4

    .line 151
    const/16 v30, 0xff

    .line 152
    :cond_4
    if-gtz v30, :cond_5

    .line 153
    const/16 v30, 0x0

    .line 154
    :cond_5
    const/16 v4, 0xff

    move/from16 v0, v28

    if-le v0, v4, :cond_6

    .line 155
    const/16 v28, 0xff

    .line 156
    :cond_6
    if-gtz v28, :cond_7

    .line 157
    const/16 v28, 0x0

    .line 158
    :cond_7
    const/16 v4, 0xff

    move/from16 v0, v26

    if-le v0, v4, :cond_8

    .line 159
    const/16 v26, 0xff

    .line 160
    :cond_8
    if-gtz v26, :cond_9

    .line 161
    const/16 v26, 0x0

    .line 163
    :cond_9
    move/from16 v0, v25

    move/from16 v1, v30

    move/from16 v2, v28

    move/from16 v3, v26

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v32

    .line 164
    .local v32, "resultColor":I
    aput v32, v13, v23
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 138
    add-int/lit8 v23, v23, 0x1

    goto :goto_6

    .line 125
    .end local v13    # "highLightpixels":[I
    .end local v23    # "i":I
    .end local v25    # "nAlpha":I
    .end local v26    # "nBlue":I
    .end local v27    # "nColor":I
    .end local v28    # "nGreen":I
    .end local v29    # "nPixelCount":I
    .end local v30    # "nRed":I
    .end local v32    # "resultColor":I
    :cond_a
    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Rect;->left:I

    div-int v4, v4, p2

    goto/16 :goto_2

    .line 126
    :cond_b
    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Rect;->top:I

    div-int v4, v4, p2

    goto/16 :goto_3

    .line 127
    :cond_c
    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Rect;->right:I

    div-int v4, v4, p2

    goto/16 :goto_4

    .line 128
    :cond_d
    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    div-int v4, v4, p2

    goto/16 :goto_5

    .line 167
    .restart local v13    # "highLightpixels":[I
    .restart local v23    # "i":I
    .restart local v29    # "nPixelCount":I
    :cond_e
    const/4 v14, 0x0

    :try_start_1
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->width()I

    move-result v15

    move-object/from16 v0, v31

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v16, v0

    move-object/from16 v0, v31

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->width()I

    move-result v18

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->height()I

    move-result v19

    move-object/from16 v12, v20

    invoke-virtual/range {v12 .. v19}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 169
    .end local v23    # "i":I
    .end local v29    # "nPixelCount":I
    :catch_0
    move-exception v21

    .line 170
    .local v21, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 171
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/util/ImageUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "rec.width = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " rec.left = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v31

    iget v8, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " originalBitmap.width = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 173
    .end local v21    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v21

    .line 174
    .local v21, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto/16 :goto_1

    .line 178
    .end local v13    # "highLightpixels":[I
    .end local v21    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .end local v31    # "rec":Landroid/graphics/Rect;
    :cond_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    .line 179
    .local v22, "end":Ljava/lang/Long;
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/util/ImageUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HighLighting process Time : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sub-long/2addr v8, v14

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    if-eqz p0, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 182
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0
.end method

.method public static decodeOrientation(I)I
    .locals 1
    .param p0, "orientationMark"    # I

    .prologue
    .line 46
    packed-switch p0, :pswitch_data_0

    .line 58
    :pswitch_0
    const/4 v0, 0x0

    .line 60
    .local v0, "degrees":I
    :goto_0
    return v0

    .line 48
    .end local v0    # "degrees":I
    :pswitch_1
    const/16 v0, 0x10e

    .line 49
    .restart local v0    # "degrees":I
    goto :goto_0

    .line 51
    .end local v0    # "degrees":I
    :pswitch_2
    const/16 v0, 0xb4

    .line 52
    .restart local v0    # "degrees":I
    goto :goto_0

    .line 54
    .end local v0    # "degrees":I
    :pswitch_3
    const/16 v0, 0x5a

    .line 55
    .restart local v0    # "degrees":I
    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getOrientation(Ljava/lang/String;)I
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 66
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .local v1, "metadata":Landroid/media/ExifInterface;
    const-string v3, "Orientation"

    invoke-virtual {v1, v3, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v2

    .end local v1    # "metadata":Landroid/media/ExifInterface;
    :goto_0
    return v2

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getResizedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "srcBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "newWidth"    # I
    .param p2, "newHeight"    # I

    .prologue
    .line 35
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, p1, p2, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 40
    :goto_0
    return-object v0

    .line 36
    :catch_0
    move-exception v0

    .line 40
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getScaledBitmapFromPath(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "sampleSize"    # I

    .prologue
    .line 21
    :try_start_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 22
    .local v0, "bnd":Landroid/graphics/BitmapFactory$Options;
    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 24
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 29
    .end local v0    # "bnd":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return-object v1

    .line 25
    :catch_0
    move-exception v1

    .line 29
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static rotateImage(Landroid/graphics/Bitmap;ILandroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "source"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;
    .param p3, "recycle"    # Z

    .prologue
    const/4 v1, 0x0

    .line 77
    if-eqz p0, :cond_0

    if-gtz p1, :cond_2

    :cond_0
    move-object v7, p0

    .line 87
    :cond_1
    :goto_0
    return-object v7

    .line 80
    :cond_2
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 81
    .local v5, "rotation":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 82
    iget v3, p2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, p2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object v0, p0

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 84
    .local v7, "rotated":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_1

    invoke-virtual {p0, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 85
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method
