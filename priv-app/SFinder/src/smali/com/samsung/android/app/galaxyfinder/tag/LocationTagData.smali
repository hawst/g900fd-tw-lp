.class Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;
.super Ljava/lang/Object;
.source "LocationTagData.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private _id:I

.field private address:Ljava/lang/String;

.field private langcode:Ljava/lang/String;

.field private latitude:D

.field private longitude:D

.field private prevAddress:Ljava/lang/String;

.field private targeturi:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData$1;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData$1;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public constructor <init>(IDDLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "_id"    # I
    .param p2, "latitude"    # D
    .param p4, "longitude"    # D
    .param p6, "targeturi"    # Ljava/lang/String;
    .param p7, "prevAddr"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->_id:I

    .line 29
    iput-wide p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->latitude:D

    .line 30
    iput-wide p4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->longitude:D

    .line 31
    iput-object p6, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->targeturi:Ljava/lang/String;

    .line 32
    iput-object p7, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->prevAddress:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->readFromParcel(Landroid/os/Parcel;)V

    .line 37
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->_id:I

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->latitude:D

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->longitude:D

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->prevAddress:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->address:Ljava/lang/String;

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->langcode:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->targeturi:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 172
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getLangcode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->langcode:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 140
    iget-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->longitude:D

    return-wide v0
.end method

.method public getPrevAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->prevAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getTargeturi()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->targeturi:Ljava/lang/String;

    return-object v0
.end method

.method public get_id()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->_id:I

    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->address:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public setLangcode(Ljava/lang/String;)V
    .locals 0
    .param p1, "langcode"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->langcode:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setLatitude(D)V
    .locals 1
    .param p1, "latitude"    # D

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->latitude:D

    .line 91
    return-void
.end method

.method public setLongitude(D)V
    .locals 1
    .param p1, "longitude"    # D

    .prologue
    .line 97
    iput-wide p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->longitude:D

    .line 98
    return-void
.end method

.method public setPrevAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->prevAddress:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setTargeturi(Ljava/lang/String;)V
    .locals 0
    .param p1, "targeturi"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->targeturi:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public set_id(I)V
    .locals 0
    .param p1, "_id"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->_id:I

    .line 84
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LocationTagData [_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->latitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->longitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", langcode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->langcode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targeturi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->targeturi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 70
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->_id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    iget-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->latitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 72
    iget-wide v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->longitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 73
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->prevAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->address:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->langcode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->targeturi:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    return-void
.end method
