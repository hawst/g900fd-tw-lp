.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListMyfilesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "ItemListMyfilesAdapter"

.field static final apk:Ljava/lang/String; = "application/apk"

.field static final installFile:Ljava/lang/String; = "application/vnd.android.package-archive"


# instance fields
.field final bSupportThumbnail:Z

.field private mThumbnailLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->bSupportThumbnail:Z

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mThumbnailLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    .line 64
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mThumbnailLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    .line 65
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;)Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mThumbnailLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    return-object v0
.end method

.method private setTaskAction(Landroid/view/View;Landroid/content/Intent;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 161
    if-eqz p1, :cond_0

    .line 162
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$1;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;Landroid/content/Intent;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    :cond_0
    return-void
.end method


# virtual methods
.method protected getCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 251
    const/4 v0, 0x0

    .line 253
    .local v0, "cacheKey":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 254
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 257
    :cond_0
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 70
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v7

    .line 71
    .local v7, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v4, 0x0

    .line 73
    .local v4, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 75
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v9

    .line 76
    .local v9, "name":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v5

    .line 77
    .local v5, "iconUri":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionUri()Ljava/lang/String;

    move-result-object v11

    .line 78
    .local v11, "uri":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionMimeType()Ljava/lang/String;

    move-result-object v8

    .line 79
    .local v8, "mimetype":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionContentHighlight()Ljava/lang/String;

    move-result-object v3

    .line 80
    .local v3, "highlight":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v6

    .line 82
    .local v6, "intent":Landroid/content/Intent;
    if-nez p2, :cond_5

    .line 83
    sget-object v12, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v13, 0x7f030041

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 85
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;

    .end local v4    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;
    invoke-direct {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;-><init>()V

    .line 87
    .restart local v4    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;
    const v12, 0x7f0b0054

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    iput-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 88
    const v12, 0x7f0b007d

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    iput-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->tvContent:Landroid/widget/TextView;

    .line 89
    const v12, 0x7f0b004e

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    iput-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 90
    const v12, 0x7f0b007b

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    iput-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivOverlayIcon:Landroid/widget/ImageView;

    .line 91
    const v12, 0x7f0b005b

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    iput-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    .line 92
    const v12, 0x7f0b004d

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    iput-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    .line 93
    const v12, 0x7f0b005a

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    iput-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    .line 95
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 97
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 98
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->tvName:Landroid/widget/TextView;

    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v9, v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 99
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_2

    .line 100
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->tvContent:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->tvContent:Landroid/widget/TextView;

    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v3, v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 109
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->isDisplayItem()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 110
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v8, v5, v11}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->loadThumbnail(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_0
    if-eqz v11, :cond_4

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_4

    .line 115
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0e0077

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 118
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivTask:Landroid/widget/ImageView;

    const v13, 0x7f0200f6

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 121
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->setTaskAction(Landroid/view/View;Landroid/content/Intent;)V

    .line 123
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e0078

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, ", "

    aput-object v15, v13, v14

    const/4 v14, 0x2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e000c

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setViewDesc(Landroid/view/View;[Ljava/lang/String;)V

    .line 127
    new-instance v6, Landroid/content/Intent;

    .end local v6    # "intent":Landroid/content/Intent;
    const-string v12, "android.intent.action.VIEW"

    invoke-direct {v6, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 128
    .restart local v6    # "intent":Landroid/content/Intent;
    const v12, 0x10008000

    invoke-virtual {v6, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 130
    const-string v12, "application/apk"

    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 131
    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    const-string v13, "application/vnd.android.package-archive"

    invoke-virtual {v6, v12, v13}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    :goto_1
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v12, v6, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 157
    :cond_1
    :goto_2
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    return-object v12

    .line 103
    :cond_2
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->tvContent:Landroid/widget/TextView;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 133
    :cond_3
    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v6, v12, v8}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const-string v12, "search-key"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v6, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 137
    :cond_4
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgCtrlGroup:Landroid/view/ViewGroup;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 138
    iget-object v12, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->vgMainGroup:Landroid/view/ViewGroup;

    invoke-virtual {v12}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 140
    .local v10, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    goto :goto_1

    .line 146
    .end local v10    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_5
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;
    check-cast v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;

    .line 151
    .restart local v4    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->isDisplayItem()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 152
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v8, v5, v11}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->loadThumbnail(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected loadThumbnail(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 8
    .param p1, "vh"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "filePath"    # Ljava/lang/String;
    .param p4, "iconUri"    # Landroid/net/Uri;

    .prologue
    .line 208
    invoke-virtual {p0, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->getCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 209
    .local v3, "key":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getInstance()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getBitmapDrawable(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v4

    .line 211
    .local v4, "thumbnail":Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {p2, p3}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getFileType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 213
    .local v1, "fileType":I
    if-nez v4, :cond_1

    .line 214
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    invoke-direct {v2}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;-><init>()V

    .line 216
    .local v2, "info":Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;
    iput-object v3, v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mKey:Ljava/lang/String;

    .line 217
    iput-object p3, v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mPath:Ljava/lang/String;

    .line 218
    iput v1, v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mFileType:I

    .line 219
    iput-object p2, v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mMimeType:Ljava/lang/String;

    .line 220
    iget-object v5, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    iput-object v5, v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    .line 221
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mContext:Landroid/content/Context;

    iput-object v5, v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mThumbnailInfoContext:Landroid/content/Context;

    .line 222
    iget-object v5, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivOverlayIcon:Landroid/widget/ImageView;

    iput-object v5, v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mOverlayIconImageView:Landroid/widget/ImageView;

    .line 223
    iput-object p4, v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconUri:Landroid/net/Uri;

    .line 231
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mThumbnailLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    if-eqz v5, :cond_0

    .line 232
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->mThumbnailLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->queuePhoto(Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;Ljava/lang/String;)V
    invoke-static {v5, v2, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;Ljava/lang/String;)V

    .line 248
    .end local v2    # "info":Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v5, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 238
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v0, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 239
    .local v0, "anim":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v6, 0x1f4

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 240
    iget-object v5, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 242
    const/16 v5, 0x3ea

    if-ne v1, v5, :cond_2

    .line 243
    iget-object v5, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivOverlayIcon:Landroid/widget/ImageView;

    const v6, 0x7f0200c7

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 245
    :cond_2
    iget-object v5, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivOverlayIcon:Landroid/widget/ImageView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected loadThumbnail(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "vh"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "iconUri"    # Ljava/lang/String;
    .param p4, "uri"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-static {p2, p4}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->getFileType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 182
    .local v2, "fileType":I
    const/4 v0, 0x0

    .line 184
    .local v0, "decodedUri":Ljava/lang/String;
    iget-object v3, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 188
    .local v1, "defaultIcon":Landroid/net/Uri;
    const/16 v3, 0x3e9

    if-eq v2, v3, :cond_2

    const/16 v3, 0x3eb

    if-eq v2, v3, :cond_2

    const/16 v3, 0x3ea

    if-eq v2, v3, :cond_2

    const/16 v3, 0x3ec

    if-ne v2, v3, :cond_3

    .line 191
    :cond_2
    iget-object v3, p1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-nez v3, :cond_0

    .line 192
    invoke-static {p4}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 193
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "file://"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, p2, v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->loadThumbnail(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 201
    :cond_3
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter;->loadThumbnail(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$MyfilesItemListViewHolder;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method
