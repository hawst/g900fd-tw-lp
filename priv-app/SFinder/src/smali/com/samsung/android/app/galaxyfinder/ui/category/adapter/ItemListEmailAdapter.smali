.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListEmailAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$1;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;
    }
.end annotation


# static fields
.field private static final FLAG_ATTACHMENT:I = 0x1000

.field private static final FLAG_READ:I = 0x40


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 26
    return-void
.end method

.method private getAttachmentIconId(IZ)I
    .locals 1
    .param p1, "iconFlagSet"    # I
    .param p2, "isAttachmentMatched"    # Z

    .prologue
    .line 127
    and-int/lit16 v0, p1, 0x1000

    if-eqz v0, :cond_1

    .line 128
    if-eqz p2, :cond_0

    .line 129
    const v0, 0x7f020075

    .line 135
    :goto_0
    return v0

    .line 131
    :cond_0
    const v0, 0x7f020074

    goto :goto_0

    .line 135
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 23
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 30
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v11

    .line 31
    .local v11, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v8, 0x0

    .line 33
    .local v8, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 35
    if-nez p2, :cond_5

    .line 36
    sget-object v19, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v20, 0x7f030038

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-virtual/range {v19 .. v22}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 38
    new-instance v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;

    .end local v8    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$1;)V

    .line 40
    .restart local v8    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;
    const v19, 0x7f0b005f

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->subject:Landroid/widget/TextView;

    .line 41
    const v19, 0x7f0b0060

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->sender:Landroid/widget/TextView;

    .line 42
    const v19, 0x7f0b0052

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->bodyText:Landroid/widget/TextView;

    .line 43
    const v19, 0x7f0b005e

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/ImageView;

    move-object/from16 v0, v19

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->clip:Landroid/widget/ImageView;

    .line 44
    const v19, 0x7f0b0056

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->date:Landroid/widget/TextView;

    .line 45
    const v19, 0x7f0b004d

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v19

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->vgItemGroup:Landroid/widget/RelativeLayout;

    .line 46
    const v19, 0x7f0b0045

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup;

    move-object/from16 v0, v19

    iput-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    .line 48
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 53
    :goto_0
    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v16

    .line 54
    .local v16, "subject":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v12

    .line 55
    .local v12, "sender":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, "body":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText4()Ljava/lang/String;

    move-result-object v6

    .line 57
    .local v6, "date":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v13

    .line 58
    .local v13, "sortKeyValue":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v18

    .line 59
    .local v18, "typeInfoSet":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v17

    .line 61
    .local v17, "targetType":Ljava/lang/String;
    const/4 v10, 0x0

    .line 63
    .local v10, "isAttachmentMatched":Z
    if-eqz v12, :cond_0

    .line 64
    const-string v19, "\\u0002"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 66
    .local v14, "splitted":[Ljava/lang/String;
    array-length v0, v14

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_0

    .line 67
    const/16 v19, 0x1

    aget-object v12, v14, v19

    .line 71
    .end local v14    # "splitted":[Ljava/lang/String;
    :cond_0
    if-eqz v16, :cond_1

    .line 72
    const-string v19, "\\u0002"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 74
    .local v15, "splitted2":[Ljava/lang/String;
    array-length v0, v15

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_1

    .line 75
    const/16 v19, 0x1

    aget-object v16, v15, v19

    .line 79
    .end local v15    # "splitted2":[Ljava/lang/String;
    :cond_1
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->subject:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v16

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 80
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->bodyText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v5, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 81
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->sender:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v12, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 82
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->date:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 84
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_2

    .line 85
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2, v13}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->setSubHeaderView(Landroid/view/ViewGroup;ILjava/lang/String;)V

    .line 88
    :cond_2
    if-eqz v17, :cond_3

    .line 90
    :try_start_0
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    const/4 v10, 0x1

    .line 96
    :cond_3
    :goto_1
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->vgItemGroup:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 98
    if-eqz v18, :cond_4

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_4

    .line 100
    :try_start_1
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    const v20, 0xffff

    and-int v9, v19, v20

    .line 101
    .local v9, "iconFlagSet":I
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter;->getAttachmentIconId(IZ)I

    move-result v4

    .line 103
    .local v4, "attachmentIconId":I
    and-int/lit8 v19, v9, 0x40

    if-eqz v19, :cond_7

    .line 104
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->vgItemGroup:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    .line 109
    :goto_2
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v4, v0, :cond_8

    .line 110
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->clip:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 111
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->clip:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 123
    .end local v4    # "attachmentIconId":I
    .end local v9    # "iconFlagSet":I
    :cond_4
    :goto_3
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v19

    return-object v19

    .line 50
    .end local v5    # "body":Ljava/lang/String;
    .end local v6    # "date":Ljava/lang/String;
    .end local v10    # "isAttachmentMatched":Z
    .end local v12    # "sender":Ljava/lang/String;
    .end local v13    # "sortKeyValue":Ljava/lang/String;
    .end local v16    # "subject":Ljava/lang/String;
    .end local v17    # "targetType":Ljava/lang/String;
    .end local v18    # "typeInfoSet":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;
    check-cast v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;

    .restart local v8    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;
    goto/16 :goto_0

    .line 90
    .restart local v5    # "body":Ljava/lang/String;
    .restart local v6    # "date":Ljava/lang/String;
    .restart local v10    # "isAttachmentMatched":Z
    .restart local v12    # "sender":Ljava/lang/String;
    .restart local v13    # "sortKeyValue":Ljava/lang/String;
    .restart local v16    # "subject":Ljava/lang/String;
    .restart local v17    # "targetType":Ljava/lang/String;
    .restart local v18    # "typeInfoSet":Ljava/lang/String;
    :cond_6
    const/4 v10, 0x0

    goto :goto_1

    .line 91
    :catch_0
    move-exception v7

    .line 92
    .local v7, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 106
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .restart local v4    # "attachmentIconId":I
    .restart local v9    # "iconFlagSet":I
    :cond_7
    :try_start_2
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->vgItemGroup:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setSelected(Z)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 115
    .end local v4    # "attachmentIconId":I
    .end local v9    # "iconFlagSet":I
    :catch_1
    move-exception v7

    .line 116
    .restart local v7    # "e":Ljava/lang/NumberFormatException;
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->vgItemGroup:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    .line 117
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->clip:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_3

    .line 113
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .restart local v4    # "attachmentIconId":I
    .restart local v9    # "iconFlagSet":I
    :cond_8
    :try_start_3
    iget-object v0, v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListEmailAdapter$EmailItemListViewHolder;->clip:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3
.end method
