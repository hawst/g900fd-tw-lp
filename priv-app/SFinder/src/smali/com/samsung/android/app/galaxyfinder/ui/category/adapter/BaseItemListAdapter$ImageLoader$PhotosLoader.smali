.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;
.super Ljava/lang/Object;
.source "BaseItemListAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PhotosLoader"
.end annotation


# instance fields
.field photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)V
    .locals 0
    .param p2, "photoToLoad"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 1005
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1006
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;->photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .line 1007
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1013
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;->photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->imageViewReused(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1024
    :goto_0
    return-void

    .line 1015
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;->photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->getBitmap(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1019
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;->photoToLoad:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    invoke-direct {v0, v3, v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;Landroid/graphics/drawable/Drawable;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)V

    .line 1020
    .local v0, "bd":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$PhotosLoader;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1021
    .end local v0    # "bd":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v2

    .line 1022
    .local v2, "th":Ljava/lang/Throwable;
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
