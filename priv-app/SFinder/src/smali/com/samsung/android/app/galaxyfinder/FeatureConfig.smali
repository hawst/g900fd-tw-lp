.class public Lcom/samsung/android/app/galaxyfinder/FeatureConfig;
.super Ljava/lang/Object;
.source "FeatureConfig.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FeatureConfig"

.field private static mIsEnabledCategoryDisplayOrder:Z

.field private static mIsEnabledHelpMenu:Z

.field private static mIsEnabledMenuRefresh:Z

.field private static mIsEnabledMenuSelect:Z

.field private static mIsEnabledUsaFeature:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledUsaFeature:Z

    .line 15
    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledMenuRefresh:Z

    .line 17
    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledMenuSelect:Z

    .line 19
    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledCategoryDisplayOrder:Z

    .line 21
    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledHelpMenu:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static init()V
    .locals 3

    .prologue
    .line 28
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->isUsaCarrier()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledUsaFeature:Z

    .line 30
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SFINDER_SUPPORT_REFRESH"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledMenuRefresh:Z

    .line 31
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SFINDER_SUPPORT_SHAREVIA"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledMenuSelect:Z

    .line 32
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SFINDER_SUPPORT_CATEGORY_DISPLAY_ORDER"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledCategoryDisplayOrder:Z

    .line 33
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SFINDER_SUPPORT_HELP_MENU"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledHelpMenu:Z

    .line 35
    const-string v0, "FeatureConfig"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init() refresh["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledMenuRefresh:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], Select["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledMenuSelect:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], DisplayOrder["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledCategoryDisplayOrder:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], HelpMenu["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledHelpMenu:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    return-void
.end method

.method public static initConfig()V
    .locals 0

    .prologue
    .line 24
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->init()V

    .line 25
    return-void
.end method

.method public static isEnabledCategoryDisplayOrder()Z
    .locals 1

    .prologue
    .line 48
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledCategoryDisplayOrder:Z

    return v0
.end method

.method public static isEnabledHelpMenu()Z
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledHelpMenu:Z

    return v0
.end method

.method public static isEnabledMenuRefresh()Z
    .locals 1

    .prologue
    .line 40
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledMenuRefresh:Z

    return v0
.end method

.method public static isEnabledMenuSelect()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->mIsEnabledMenuSelect:Z

    return v0
.end method

.method public static isUsaCarrier()Z
    .locals 2

    .prologue
    .line 56
    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "countryCode":Ljava/lang/String;
    const-string v1, "USA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method
