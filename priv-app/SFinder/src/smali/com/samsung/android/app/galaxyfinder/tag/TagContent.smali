.class public Lcom/samsung/android/app/galaxyfinder/tag/TagContent;
.super Ljava/lang/Object;
.source "TagContent.java"


# instance fields
.field mName:Ljava/lang/String;

.field mRawData:Ljava/lang/String;

.field mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "rawData"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagContent;->mName:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagContent;->mType:Ljava/lang/String;

    .line 15
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagContent;->mRawData:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagContent;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getRawData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagContent;->mRawData:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagContent;->mType:Ljava/lang/String;

    return-object v0
.end method
