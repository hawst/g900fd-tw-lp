.class public Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
.super Landroid/widget/LinearLayout;
.source "SearchListLayout.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
.implements Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;
.implements Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;
    }
.end annotation


# static fields
.field private static final HISTORY_LIST_OPEN_DELAY:F = 0.07f

.field private static final THRESHOLD_DIFF_Y_FOR_DETECTING_FLICKING:F = 30.0f

.field private static final THRESHOLD_DIFF_Y_FOR_WORK_FLICKING:F = 300.0f


# instance fields
.field private final HANDWRITING_TYPE_OF_FILTER:I

.field private final TAG:Ljava/lang/String;

.field private final THRESHOLD_SEARCH_INPUT_MIN_SCROLL:I

.field private final THRESHOLD_SEARCH_ON_TOP_AREA:I

.field private isAppStart:Z

.field private mCategoriesListView:Landroid/widget/LinearLayout;

.field private mCategoriesStoringList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mCategoryUpdateHandler:Landroid/os/Handler;

.field private mCurrentQuery:Landroid/os/Bundle;

.field public mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

.field private mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

.field private mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

.field private mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

.field private mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

.field private mHistoryFrame:Landroid/view/ViewGroup;

.field private mHistoryListView:Landroid/widget/ListView;

.field private mHistoryOpenDelay:I

.field private mIsAllowedSavingHistory:Z

.field private mIsInTopFlickingTouch:Z

.field private mIsPosibleToTopFlicking:Z

.field private mIsSearching:Z

.field private mIsTablet:Z

.field private mItemSelectionMode:I

.field private mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

.field private mPinchMode:I

.field private mPreviousScrollY:I

.field private mQueryHelper:Landroid/widget/TextView;

.field private mResponseFrom:Ljava/lang/String;

.field private mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

.field private mScrollViewFrame:Landroid/view/ViewGroup;

.field private mScrollViewFrameHeight:I

.field private mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

.field private mSearchExpaningListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;

.field private mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

.field private mSearchResultTagMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectableCategoriesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedUserTagBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation
.end field

.field private mSymbolHelpImageView:Landroid/widget/ImageView;

.field private mSymbolHelpView:Landroid/widget/RelativeLayout;

.field private mUIModeIsSeperated:Z

.field private mUpdateGuideText:Ljava/lang/Runnable;

.field private mtouchStartFrameY:F

.field private mtouchStartPointY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 179
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 72
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryFrame:Landroid/view/ViewGroup;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrame:Landroid/view/ViewGroup;

    .line 84
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrameHeight:I

    .line 86
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .line 88
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUpdateGuideText:Ljava/lang/Runnable;

    .line 90
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mQueryHelper:Landroid/widget/TextView;

    .line 92
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    .line 94
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    .line 96
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    .line 98
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchExpaningListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;

    .line 100
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I

    .line 106
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I

    .line 108
    const/16 v0, 0x28

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->THRESHOLD_SEARCH_INPUT_MIN_SCROLL:I

    .line 110
    const/16 v0, 0x6e

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->THRESHOLD_SEARCH_ON_TOP_AREA:I

    .line 112
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->HANDWRITING_TYPE_OF_FILTER:I

    .line 114
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    .line 116
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectableCategoriesList:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchResultTagMap:Ljava/util/HashMap;

    .line 120
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    .line 122
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsSearching:Z

    .line 124
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsTablet:Z

    .line 126
    const/16 v0, 0x9b

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPinchMode:I

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectedUserTagBuffer:Ljava/util/ArrayList;

    .line 130
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    .line 132
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpView:Landroid/widget/RelativeLayout;

    .line 134
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpImageView:Landroid/widget/ImageView;

    .line 139
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsPosibleToTopFlicking:Z

    .line 141
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    .line 143
    const v0, 0x461c4000    # 10000.0f

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartPointY:F

    .line 145
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartFrameY:F

    .line 153
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryOpenDelay:I

    .line 155
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mResponseFrom:Ljava/lang/String;

    .line 158
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUIModeIsSeperated:Z

    .line 160
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsAllowedSavingHistory:Z

    .line 162
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    .line 164
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .line 166
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isAppStart:Z

    .line 2286
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$14;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    .line 180
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V

    .line 181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 174
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryFrame:Landroid/view/ViewGroup;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrame:Landroid/view/ViewGroup;

    .line 84
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrameHeight:I

    .line 86
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .line 88
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUpdateGuideText:Ljava/lang/Runnable;

    .line 90
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mQueryHelper:Landroid/widget/TextView;

    .line 92
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    .line 94
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    .line 96
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    .line 98
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchExpaningListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;

    .line 100
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I

    .line 106
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I

    .line 108
    const/16 v0, 0x28

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->THRESHOLD_SEARCH_INPUT_MIN_SCROLL:I

    .line 110
    const/16 v0, 0x6e

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->THRESHOLD_SEARCH_ON_TOP_AREA:I

    .line 112
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->HANDWRITING_TYPE_OF_FILTER:I

    .line 114
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    .line 116
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectableCategoriesList:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchResultTagMap:Ljava/util/HashMap;

    .line 120
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    .line 122
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsSearching:Z

    .line 124
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsTablet:Z

    .line 126
    const/16 v0, 0x9b

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPinchMode:I

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectedUserTagBuffer:Ljava/util/ArrayList;

    .line 130
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    .line 132
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpView:Landroid/widget/RelativeLayout;

    .line 134
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpImageView:Landroid/widget/ImageView;

    .line 139
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsPosibleToTopFlicking:Z

    .line 141
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    .line 143
    const v0, 0x461c4000    # 10000.0f

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartPointY:F

    .line 145
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartFrameY:F

    .line 153
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryOpenDelay:I

    .line 155
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mResponseFrom:Ljava/lang/String;

    .line 158
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUIModeIsSeperated:Z

    .line 160
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsAllowedSavingHistory:Z

    .line 162
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    .line 164
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .line 166
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isAppStart:Z

    .line 2286
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$14;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    .line 175
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V

    .line 176
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 169
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryFrame:Landroid/view/ViewGroup;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrame:Landroid/view/ViewGroup;

    .line 84
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrameHeight:I

    .line 86
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .line 88
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUpdateGuideText:Ljava/lang/Runnable;

    .line 90
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mQueryHelper:Landroid/widget/TextView;

    .line 92
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    .line 94
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    .line 96
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    .line 98
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchExpaningListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;

    .line 100
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I

    .line 106
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I

    .line 108
    const/16 v0, 0x28

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->THRESHOLD_SEARCH_INPUT_MIN_SCROLL:I

    .line 110
    const/16 v0, 0x6e

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->THRESHOLD_SEARCH_ON_TOP_AREA:I

    .line 112
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->HANDWRITING_TYPE_OF_FILTER:I

    .line 114
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    .line 116
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectableCategoriesList:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchResultTagMap:Ljava/util/HashMap;

    .line 120
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    .line 122
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsSearching:Z

    .line 124
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsTablet:Z

    .line 126
    const/16 v0, 0x9b

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPinchMode:I

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectedUserTagBuffer:Ljava/util/ArrayList;

    .line 130
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    .line 132
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpView:Landroid/widget/RelativeLayout;

    .line 134
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpImageView:Landroid/widget/ImageView;

    .line 139
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsPosibleToTopFlicking:Z

    .line 141
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    .line 143
    const v0, 0x461c4000    # 10000.0f

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartPointY:F

    .line 145
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartFrameY:F

    .line 153
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryOpenDelay:I

    .line 155
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mResponseFrom:Ljava/lang/String;

    .line 158
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUIModeIsSeperated:Z

    .line 160
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsAllowedSavingHistory:Z

    .line 162
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    .line 164
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .line 166
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isAppStart:Z

    .line 2286
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$14;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    .line 170
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V

    .line 171
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrameHeight:I

    return v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrameHeight:I

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchExpaningListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/ISearchListExpandingListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->updateCurrentScreen()V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->updatePossibilityTopFlicking()V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I

    return v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPreviousScrollY:I

    return p1
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedFilter()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->makeTagQuery()V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->removeSelectedUserTag(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setCategoryViewMode(Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->makeInternalTagQuery(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedUserTagBuffer()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->addSelectedUserTag(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryText(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$2400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedPanel()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingOpenedPanel()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mQueryHelper:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->completedSearch()V

    return-void
.end method

.method static synthetic access$2900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpSearchResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->startSearch(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->prepareDataForUpdating()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfo()V

    return-void
.end method

.method static synthetic access$3300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfoFrom(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->addCategoryView(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$3500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->refreshCategoryData(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$3600(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpAllTags()V

    return-void
.end method

.method static synthetic access$3700(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpEditText()V

    return-void
.end method

.method static synthetic access$3800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectableCategoriesView()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->rotateCategoryView(II)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    return-object v0
.end method

.method private accumulateSearchedResultTags(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1501
    .local p1, "tags":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1502
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1503
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 1505
    .local v2, "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchResultTagMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1506
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchResultTagMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagCount()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->addTagCount(I)V

    .line 1509
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchResultTagMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1512
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_1
    return-void
.end method

.method private actionMoveFlickDown(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1831
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1863
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Filcking action invalidate with action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1865
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    if-eqz v1, :cond_0

    .line 1866
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->expandSearchList(Z)V

    .line 1867
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 1868
    iput-boolean v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    .line 1872
    :cond_0
    :goto_0
    return-void

    .line 1833
    :pswitch_0
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsPosibleToTopFlicking:Z

    if-eqz v1, :cond_0

    .line 1834
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    const-string v2, "Filcking action possible"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1835
    iput-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    .line 1836
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartPointY:F

    goto :goto_0

    .line 1840
    :pswitch_1
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    if-eqz v1, :cond_0

    .line 1841
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartPointY:F

    sub-float v0, v1, v2

    .line 1842
    .local v0, "diffY":F
    const/high16 v1, 0x41f00000    # 30.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 1843
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v0, v2

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    goto :goto_0

    .line 1848
    .end local v0    # "diffY":F
    :pswitch_2
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    if-eqz v1, :cond_0

    .line 1849
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    const-string v2, "Filcking action ended"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1851
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartPointY:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x43960000    # 300.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 1852
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->headupSearchList()V

    .line 1859
    :goto_1
    iput-boolean v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    goto :goto_0

    .line 1855
    :cond_1
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->expandSearchList(Z)V

    .line 1856
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    goto :goto_1

    .line 1831
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private actionMoveFlickUp(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1875
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1922
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Filcking up action invalidate with action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1924
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    if-eqz v1, :cond_0

    .line 1925
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartFrameY:F

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    .line 1926
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1, v5}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 1927
    iput-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    .line 1931
    :cond_0
    :goto_0
    return-void

    .line 1877
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getY()F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1878
    iput-boolean v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    .line 1879
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartPointY:F

    .line 1880
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getY()F

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartFrameY:F

    .line 1881
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1, v5}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 1882
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1883
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 1884
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    const-string v2, "Filcking up action started"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1888
    :pswitch_1
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    if-eqz v1, :cond_0

    .line 1889
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartPointY:F

    sub-float v0, v1, v2

    .line 1890
    .local v0, "diffY":F
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartFrameY:F

    div-float v2, v0, v6

    add-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    .line 1891
    cmpg-float v1, v0, v3

    if-gez v1, :cond_1

    .line 1892
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartFrameY:F

    div-float v3, v0, v6

    add-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    goto :goto_0

    .line 1894
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollY()I

    move-result v1

    if-gtz v1, :cond_0

    .line 1895
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1, v5}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 1896
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1897
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->setScrollY(I)V

    .line 1898
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    goto :goto_0

    .line 1901
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    goto :goto_0

    .line 1907
    .end local v0    # "diffY":F
    :pswitch_2
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    if-eqz v1, :cond_0

    .line 1908
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    const-string v2, "Filcking up action ended"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1910
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartPointY:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_3

    .line 1911
    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->expandSearchList(Z)V

    .line 1912
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1, v5}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 1918
    :goto_1
    iput-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    goto/16 :goto_0

    .line 1914
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mtouchStartFrameY:F

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    goto :goto_1

    .line 1875
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private addCategoryView(Ljava/lang/Object;)V
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1198
    instance-of v5, p1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    if-eqz v5, :cond_4

    move-object v0, p1

    .line 1199
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .line 1201
    .local v0, "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    .line 1205
    const-string v5, "query_from_tag_cloud"

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getResponseFrom()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1206
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideKeypad()V

    .line 1209
    :cond_0
    const/4 v3, 0x0

    .line 1210
    .local v3, "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedUserTags()Ljava/util/List;

    move-result-object v2

    .line 1211
    .local v2, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 1212
    .local v4, "visibility":I
    const/4 v1, -0x1

    .line 1217
    .local v1, "position":I
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->supportTagSearch()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1218
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1219
    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setTagFilters(Ljava/util/List;)V

    .line 1220
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->updateFilteredItems()V

    .line 1222
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gtz v5, :cond_5

    const/16 v4, 0x8

    .line 1225
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getAllTags()Ljava/util/HashMap;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->accumulateSearchedResultTags(Ljava/util/HashMap;)V

    .line 1228
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryViewFactory;->get(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    move-result-object v3

    .line 1230
    if-eqz v3, :cond_4

    .line 1231
    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewPosition(Landroid/view/View;)I

    move-result v1

    .line 1233
    invoke-virtual {v3, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setViewStateListener(Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;)V

    .line 1234
    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setVisibility(I)V

    .line 1236
    iget v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPinchMode:I

    const/16 v6, 0x9a

    if-ne v5, v6, :cond_3

    .line 1237
    iget v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPinchMode:I

    invoke-virtual {v3, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setViewMode(I)V

    .line 1240
    :cond_3
    invoke-direct {p0, v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->addCategoryViewToMainList(Landroid/view/View;I)V

    .line 1246
    .end local v0    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .end local v1    # "position":I
    .end local v2    # "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v4    # "visibility":I
    .end local p1    # "obj":Ljava/lang/Object;
    :cond_4
    :goto_1
    return-void

    .line 1222
    .restart local v0    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .restart local v1    # "position":I
    .restart local v2    # "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .restart local v4    # "visibility":I
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_5
    const/4 v4, 0x0

    goto :goto_0

    .line 1243
    .end local v1    # "position":I
    .end local v2    # "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v4    # "visibility":I
    :cond_6
    const/4 p1, 0x0

    goto :goto_1
.end method

.method private addCategoryViewToMainList(Landroid/view/View;I)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 1061
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1063
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsTablet:Z

    if-eqz v1, :cond_0

    .line 1064
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 1065
    const v1, 0x7f0b005c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1067
    .local v0, "appDivider":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1068
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1073
    .end local v0    # "appDivider":Landroid/view/View;
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsTablet:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListWaitingForResult()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListExpanded()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUIModeIsSeperated:Z

    if-eqz v1, :cond_2

    .line 1075
    :cond_1
    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->expandSearchList(Z)V

    .line 1081
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1082
    return-void

    .line 1078
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->headupSearchList()V

    goto :goto_0
.end method

.method private addSelectedUserTag(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 1
    .param p1, "tagData"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 809
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectedUserTagBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 810
    return-void
.end method

.method private cancelSearch(Z)V
    .locals 3
    .param p1, "bHideProgress"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1343
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    const-string v1, "cancelSearch"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->removeAllReceivedResults()V

    .line 1347
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    if-eqz v0, :cond_0

    .line 1348
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;->onCancelQuery()V

    .line 1351
    :cond_0
    invoke-direct {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setVisibilityNoResultsText(Z)V

    .line 1352
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideSearchingProgress()V

    .line 1353
    invoke-direct {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setSearching(Z)V

    .line 1354
    return-void
.end method

.method private cleanUpAllTags()V
    .locals 1

    .prologue
    .line 1395
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v0, :cond_1

    .line 1396
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSearchResult()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedFilter()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1397
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->closeAllOpenedPanel()V

    .line 1400
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->initializeAllItems()V

    .line 1403
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfoTags()V

    .line 1404
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfoTargets()V

    .line 1405
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfoTimeSpan()V

    .line 1406
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfoSearchFilters()V

    .line 1407
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearSelectedUserTagBuffer()V

    .line 1408
    return-void
.end method

.method private cleanUpEditText()V
    .locals 2

    .prologue
    .line 1411
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryText(Ljava/lang/String;Z)V

    .line 1413
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfoKey()V

    .line 1414
    return-void
.end method

.method private cleanUpSearchResult(Z)V
    .locals 3
    .param p1, "foldSearchList"    # Z

    .prologue
    .line 1365
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearching()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1366
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cancelSearch(Z)V

    .line 1369
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getCurrentQueryKeyWord()Ljava/lang/String;

    move-result-object v0

    .line 1371
    .local v0, "query":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1372
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->displayAllgroupFIlter()V

    .line 1373
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->clearAnimation()V

    .line 1374
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1378
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setVisibilityNoResultsText(Z)V

    .line 1381
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearTagDatasForSearchResults()V

    .line 1384
    if-eqz p1, :cond_2

    .line 1385
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->foldSearchList(Z)V

    .line 1389
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->removeAllCategoriesView()V

    .line 1391
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V

    .line 1392
    return-void
.end method

.method private clearOueryInfoExtras()V
    .locals 0

    .prologue
    .line 1609
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfoTags()V

    .line 1610
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfoTargets()V

    .line 1611
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfoTimeSpan()V

    .line 1612
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfoSearchFilters()V

    .line 1613
    return-void
.end method

.method private clearQueryInfo()V
    .locals 1

    .prologue
    .line 1605
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 1606
    return-void
.end method

.method private clearQueryInfoKey()V
    .locals 2

    .prologue
    .line 1616
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1617
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1619
    :cond_0
    return-void
.end method

.method private clearQueryInfoSearchFilters()V
    .locals 2

    .prologue
    .line 1640
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_search_filters"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1641
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_search_filters"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1643
    :cond_0
    return-void
.end method

.method private clearQueryInfoTags()V
    .locals 2

    .prologue
    .line 1622
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_tags"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1623
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_tags"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1625
    :cond_0
    return-void
.end method

.method private clearQueryInfoTargets()V
    .locals 2

    .prologue
    .line 1628
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_targets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1629
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_targets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1631
    :cond_0
    return-void
.end method

.method private clearQueryInfoTimeSpan()V
    .locals 2

    .prologue
    .line 1634
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_time_span"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1635
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_time_span"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1637
    :cond_0
    return-void
.end method

.method private clearSelectedUserTagBuffer()V
    .locals 1

    .prologue
    .line 860
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectedUserTagBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 861
    return-void
.end method

.method private clearTagDatasForSearchResults()V
    .locals 1

    .prologue
    .line 1478
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchResultTagMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1479
    return-void
.end method

.method private completedSearch()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1438
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    const-string v4, "completedSearch"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1440
    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setSearching(Z)V

    .line 1442
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1443
    .local v0, "childCount":I
    const/4 v2, 0x0

    .line 1445
    .local v2, "visibleChildCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1446
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 1447
    add-int/lit8 v2, v2, 0x1

    .line 1445
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1451
    :cond_1
    if-gt v2, v6, :cond_2

    .line 1452
    if-nez v2, :cond_2

    .line 1453
    invoke-direct {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setVisibilityNoResultsText(Z)V

    .line 1454
    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->foldSearchList(Z)V

    .line 1458
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setTagDatasForSearchResults()V

    .line 1459
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideSearchingProgress()V

    .line 1461
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V

    .line 1462
    return-void
.end method

.method private expandSearchList(Z)V
    .locals 1
    .param p1, "isAnim"    # Z

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->playScrollViewExpand(Z)V

    .line 1087
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->updatePossibilityTopFlicking()V

    .line 1088
    return-void
.end method

.method private findViewPosition(Landroid/view/View;)I
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1249
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 1250
    .local v0, "count":I
    move v2, v0

    .local v2, "position":I
    move-object v3, p1

    .line 1252
    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 1254
    .local v3, "src":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1255
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 1257
    .local v4, "view":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getOrder()I

    move-result v5

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getOrder()I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 1258
    move v2, v1

    .line 1263
    .end local v4    # "view":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    :cond_0
    return v2

    .line 1254
    .restart local v4    # "view":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private foldSearchList(Z)V
    .locals 1
    .param p1, "anim"    # Z

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->playScrollViewFolding(Z)V

    .line 1100
    return-void
.end method

.method private getAllCategoriesView()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getQueryInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1551
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    return-object v0
.end method

.method private getResponseFrom()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mResponseFrom:Ljava/lang/String;

    return-object v0
.end method

.method private getSelectableCategoriesView()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1121
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectableCategoriesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getSelectedItemCount()I
    .locals 6

    .prologue
    .line 1176
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectableCategoriesView()Ljava/util/List;

    move-result-object v4

    .line 1177
    .local v4, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v1, 0x0

    .line 1179
    .local v1, "total":I
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .local v3, "view":Landroid/view/View;
    move-object v2, v3

    .line 1180
    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 1182
    .local v2, "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1183
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getSelectedItemCount()I

    move-result v5

    add-int/2addr v1, v5

    goto :goto_0

    .line 1187
    .end local v2    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    return v1
.end method

.method private getSelectedUserTagBuffer()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 856
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectedUserTagBuffer:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getSelectedUserTags()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 864
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 865
    .local v3, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedUserTagBuffer()Ljava/util/ArrayList;

    move-result-object v2

    .line 867
    .local v2, "tagDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 868
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 869
    .local v0, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 873
    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v3
.end method

.method private headupSearchList()V
    .locals 1

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isHeadupOmitted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1104
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->expandSearchList(Z)V

    .line 1108
    :goto_0
    return-void

    .line 1107
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->playScrollviewHeadUp()V

    goto :goto_0
.end method

.method private hideSIPWithTagTouched(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 2354
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v0, :cond_0

    .line 2355
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getRight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getBottom()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 2356
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideKeypad()V

    .line 2359
    :cond_0
    return-void
.end method

.method private hideSearchingProgress()V
    .locals 2

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->hideProgressBar()V

    .line 1011
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v0, :cond_0

    .line 1012
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->setFilterState(I)V

    .line 1014
    :cond_0
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, -0x1

    const/4 v10, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 184
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setOrientation(I)V

    .line 186
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    invoke-direct {v0, p0, v10}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    .line 187
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsTablet:Z

    .line 189
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 191
    .local v6, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f030057

    invoke-virtual {v6, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 192
    .local v9, "rootview":Landroid/view/View;
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 194
    .local v8, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v8}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    invoke-virtual {p0, v9, v8}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    .line 198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSelectableCategoriesList:Ljava/util/ArrayList;

    .line 200
    const v0, 0x7f0b00bc

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    .line 202
    const v0, 0x7f0b00d0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpView:Landroid/widget/RelativeLayout;

    .line 204
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0b00d1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpImageView:Landroid/widget/ImageView;

    .line 206
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->setOnTouchEventListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;)V

    .line 306
    const v0, 0x7f0b00be

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    .line 307
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 308
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setMotionEventSplittingEnabled(Z)V

    .line 310
    const v0, 0x7f0b00b9

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryFrame:Landroid/view/ViewGroup;

    .line 312
    const v0, 0x7f0b00bb

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    .line 314
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 315
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getAllHistoryItems()Landroid/database/Cursor;

    move-result-object v2

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;ZILandroid/widget/ListView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .line 318
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 319
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 333
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$3;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 345
    const v0, 0x7f030013

    invoke-virtual {v6, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 347
    .local v7, "mFooterView":Landroid/view/View;
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getHistoryDeleteAllThreshHold()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 348
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;

    invoke-direct {v1, p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$4;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 375
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;

    invoke-direct {v0, p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$5;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Landroid/view/View;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 412
    .end local v7    # "mFooterView":Landroid/view/View;
    :cond_1
    const v0, 0x7f0b00bd

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrame:Landroid/view/ViewGroup;

    .line 413
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrame:Landroid/view/ViewGroup;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 440
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$7;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    .line 475
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0, v4}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 476
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$8;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setOnComputeScrollListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;)V

    .line 501
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->getSearchListLayoutTransitionAnimation()Landroid/animation/LayoutTransition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 504
    const v0, 0x7f0b00ba

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    .line 505
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$9;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->setOnSizeChangedListener(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;)V

    .line 513
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$10;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->setOnActionListener(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;)V

    .line 651
    const v0, 0x7f0b00c0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mQueryHelper:Landroid/widget/TextView;

    .line 652
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mQueryHelper:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 653
    return-void
.end method

.method private isExistingOpenedPanel()Z
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v0, :cond_0

    .line 845
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->isExistingOpenedPanel()Z

    move-result v0

    .line 848
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isExistingSelectedFilter()Z
    .locals 1

    .prologue
    .line 828
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->isExistingSelectedFilter()Z

    move-result v0

    .line 832
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isExistingSelectedPanel()Z
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->isExistingSelectedPanel()Z

    move-result v0

    .line 840
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isQueryEmpty()Z
    .locals 2

    .prologue
    .line 1538
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_time_span"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_search_filters"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_targets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_tags"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1544
    :cond_0
    const/4 v0, 0x1

    .line 1547
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeAvailableViewList()V
    .locals 2

    .prologue
    .line 1155
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectableCategoriesView()Ljava/util/List;

    move-result-object v0

    .line 1156
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1158
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getAllCategoriesView()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1159
    return-void
.end method

.method private makeInternalTagQuery(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 6
    .param p1, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 682
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSearchResult()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isUserTag()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 683
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearTagDatasForSearchResults()V

    .line 685
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getAllCategoriesView()Ljava/util/List;

    move-result-object v4

    .line 687
    .local v4, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .local v3, "view":Landroid/view/View;
    move-object v2, v3

    .line 688
    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 689
    .local v2, "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v0

    .line 691
    .local v0, "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedUserTags()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setTagFilters(Ljava/util/List;)V

    .line 692
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->updateFilteredItems()V

    .line 694
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getAllTags()Ljava/util/HashMap;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->accumulateSearchedResultTags(Ljava/util/HashMap;)V

    .line 696
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->refreshData()I

    move-result v5

    if-nez v5, :cond_1

    .line 697
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setVisibility(I)V

    goto :goto_0

    .line 699
    :cond_1
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setVisibility(I)V

    goto :goto_0

    .line 703
    .end local v0    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .end local v2    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v3    # "view":Landroid/view/View;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setTagDatasForSearchResults()V

    .line 707
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_3
    return-void
.end method

.method private makeSelectionMode()V
    .locals 5

    .prologue
    .line 1162
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getAllCategoriesView()Ljava/util/List;

    move-result-object v3

    .line 1164
    .local v3, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .local v2, "view":Landroid/view/View;
    move-object v1, v2

    .line 1165
    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 1167
    .local v1, "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectableCategoriesView()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1168
    const/16 v4, 0x98

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setViewMode(I)V

    goto :goto_0

    .line 1170
    :cond_0
    const/16 v4, 0x99

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setViewMode(I)V

    goto :goto_0

    .line 1173
    .end local v1    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v2    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private makeTagQuery()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 656
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setTagCloudDataToQuery()Z

    move-result v0

    .line 658
    .local v0, "existFilterItems":Z
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListHeadUp()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUIModeIsSeperated:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListExpanded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 660
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->foldSearchList(Z)V

    .line 663
    :cond_1
    if-eqz v0, :cond_2

    .line 664
    const-string v1, "query_from_tag_cloud"

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfoFrom(Ljava/lang/String;)V

    .line 665
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 678
    :goto_0
    return-void

    .line 667
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v1, :cond_3

    .line 668
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->setFilterState(I)V

    .line 671
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 672
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto :goto_0

    .line 675
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method private prepareDataForUpdating()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 1418
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getQueryInfo()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getQueryInfo()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1419
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    const-string v3, "query has null value. hence, this request will be ignored"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421
    const/4 v0, 0x0

    .line 1434
    :goto_0
    return-object v0

    .line 1424
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getQueryInfo()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1426
    .local v0, "reloadQuery":Landroid/os/Bundle;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1427
    .local v1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "com.sec.providers.settingsearch"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1431
    const-string v2, "extra_query_from"

    const-string v3, "query_from_reload"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1432
    const-string v2, "extra_targets"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private refreshCategoryData(Ljava/lang/Object;)V
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1267
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getQueryInfo()Landroid/os/Bundle;

    move-result-object v7

    if-nez v7, :cond_1

    .line 1295
    :cond_0
    :goto_0
    return-void

    .line 1271
    :cond_1
    instance-of v7, p1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    if-eqz v7, :cond_0

    move-object v1, p1

    .line 1272
    check-cast v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .line 1274
    .local v1, "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    if-eqz v1, :cond_0

    .line 1275
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 1277
    .local v2, "childCount":I
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_1
    if-ge v3, v2, :cond_0

    .line 1278
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1279
    .local v6, "view":Landroid/view/View;
    const/4 v4, 0x0

    .line 1281
    .local v4, "tagKey":Ljava/lang/String;
    instance-of v7, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    if-eqz v7, :cond_2

    move-object v5, v6

    .line 1282
    check-cast v5, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 1283
    .local v5, "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 1286
    .end local v5    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v0, v6

    .line 1287
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 1288
    .local v0, "baseView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->replaceData(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1277
    .end local v0    # "baseView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private removeAllCategoriesView()V
    .locals 1

    .prologue
    .line 1515
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 1516
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1518
    const/16 v0, 0x9b

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPinchMode:I

    .line 1519
    return-void
.end method

.method private removeAllReceivedResults()V
    .locals 2

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->removeMessages(I)V

    .line 1358
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->removeMessages(I)V

    .line 1359
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->removeMessages(I)V

    .line 1360
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->removeMessages(I)V

    .line 1361
    return-void
.end method

.method private removeSelectedUserTag(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 4
    .param p1, "tagData"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 813
    const/4 v0, 0x0

    .line 814
    .local v0, "i":I
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedUserTagBuffer()Ljava/util/ArrayList;

    move-result-object v2

    .line 816
    .local v2, "tags":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 817
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 819
    .local v1, "selectedTag":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 820
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 822
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 825
    .end local v1    # "selectedTag":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_1
    return-void
.end method

.method private rotateCategoryView(II)V
    .locals 8
    .param p1, "focusId"    # I
    .param p2, "firstItemIndex"    # I

    .prologue
    const/4 v7, 0x2

    .line 2183
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 2186
    .local v4, "viewCnt":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_3

    .line 2187
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 2189
    .local v0, "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    const/4 v1, 0x0

    .line 2190
    .local v1, "centerIndex":I
    if-ne p1, v3, :cond_0

    .line 2191
    invoke-virtual {v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCenterItemIndex(I)I

    move-result v1

    .line 2194
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->updateCategoryWhenRotate(Z)V

    .line 2196
    add-int/lit8 v5, p1, -0x1

    if-ne v3, v5, :cond_1

    .line 2197
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryExpandedMode()I

    move-result v5

    if-eq v5, v7, :cond_1

    .line 2198
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->convertVisibleIndex()V

    .line 2199
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->refreshCurrentVisible()V

    .line 2203
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryViewMode()I

    move-result v5

    if-eq v5, v7, :cond_2

    if-ne p1, v3, :cond_2

    .line 2205
    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getItemViewInCategory(I)Landroid/view/View;

    move-result-object v2

    .line 2206
    .local v2, "focusedView":Landroid/view/View;
    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearchListHeadUp()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2207
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 2186
    .end local v2    # "focusedView":Landroid/view/View;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2211
    .end local v0    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v1    # "centerIndex":I
    :cond_3
    return-void
.end method

.method private setCategoryViewMode(Z)V
    .locals 4
    .param p1, "bZoomIn"    # Z

    .prologue
    .line 2165
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2171
    .local v2, "viewCnt":I
    if-eqz p1, :cond_0

    .line 2172
    const/16 v3, 0x9a

    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPinchMode:I

    .line 2176
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 2177
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 2178
    .local v0, "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPinchMode:I

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setViewMode(I)V

    .line 2176
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2174
    .end local v0    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v1    # "i":I
    :cond_0
    const/16 v3, 0x9b

    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mPinchMode:I

    goto :goto_0

    .line 2180
    .restart local v1    # "i":I
    :cond_1
    return-void
.end method

.method private setQueryInfo(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 1555
    const-string v0, "query_from_user"

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    return-void
.end method

.method private setQueryInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "from"    # Ljava/lang/String;

    .prologue
    .line 1559
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1560
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1565
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_from"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    return-void

    .line 1562
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_key"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setQueryInfoFrom(Ljava/lang/String;)V
    .locals 2
    .param p1, "from"    # Ljava/lang/String;

    .prologue
    .line 1601
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_query_from"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1602
    return-void
.end method

.method private setQueryInfoSearchFilters(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "filters"    # Landroid/os/Bundle;

    .prologue
    .line 1593
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_search_filters"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1594
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_search_filters"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1597
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_search_filters"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1598
    return-void
.end method

.method private setQueryInfoTags(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1569
    .local p1, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_tags"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1570
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_tags"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1573
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_tags"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1574
    return-void
.end method

.method private setQueryInfoTargets(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1577
    .local p1, "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_targets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1578
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_targets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1581
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_targets"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1582
    return-void
.end method

.method private setQueryInfoTimeSpan(Ljava/lang/String;)V
    .locals 2
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 1585
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_time_span"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1586
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_time_span"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1589
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCurrentQuery:Landroid/os/Bundle;

    const-string v1, "extra_time_span"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1590
    return-void
.end method

.method private setQueryText(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "update"    # Z

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->setQuery(Ljava/lang/String;ZZ)V

    .line 1316
    return-void
.end method

.method private setResponseFrom(Ljava/lang/String;)V
    .locals 0
    .param p1, "from"    # Ljava/lang/String;

    .prologue
    .line 1650
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mResponseFrom:Ljava/lang/String;

    .line 1651
    return-void
.end method

.method private setSearching(Z)V
    .locals 0
    .param p1, "searching"    # Z

    .prologue
    .line 1049
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsSearching:Z

    .line 1050
    return-void
.end method

.method private setTagDatasForSearchResults()V
    .locals 2

    .prologue
    .line 1465
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1466
    .local v0, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchResultTagMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1469
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->updateUserTagSelection(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1470
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->tagComparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1472
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v1, :cond_0

    .line 1473
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->setCustomFilterItems(Ljava/util/ArrayList;)V

    .line 1475
    :cond_0
    return-void
.end method

.method private setVisibilityGuideText(Z)V
    .locals 4
    .param p1, "show"    # Z

    .prologue
    .line 1025
    if-eqz p1, :cond_2

    .line 1026
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUpdateGuideText:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1027
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$12;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUpdateGuideText:Ljava/lang/Runnable;

    .line 1036
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUpdateGuideText:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1043
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    if-eqz v0, :cond_1

    .line 1044
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;->onRequestWindowInputMode(Z)V

    .line 1046
    :cond_1
    return-void

    .line 1038
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUpdateGuideText:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1040
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mQueryHelper:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setVisibilityNoResultsText(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 1017
    const v1, 0x7f0b00bf

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1019
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1020
    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1022
    :cond_0
    return-void

    .line 1020
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method

.method private showSearchingProgress()V
    .locals 2

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->showProgressBar()V

    .line 1003
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v0, :cond_0

    .line 1004
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->setFilterState(I)V

    .line 1006
    :cond_0
    return-void
.end method

.method private showSymbolHelp(Z)V
    .locals 2
    .param p1, "bShow"    # Z

    .prologue
    .line 2271
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 2284
    :goto_0
    return-void

    .line 2275
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearching()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2276
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpView:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2277
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpImageView:Landroid/widget/ImageView;

    const v1, 0x7f02018f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2280
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpView:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2281
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private startSearch(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "query"    # Landroid/os/Bundle;

    .prologue
    .line 1319
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    const-string v2, "startSearch"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1321
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1322
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1339
    :cond_0
    :goto_0
    return-void

    .line 1326
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setSearching(Z)V

    .line 1327
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setVisibilityNoResultsText(Z)V

    .line 1328
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->showSearchingProgress()V

    .line 1329
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V

    .line 1331
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    if-eqz v1, :cond_0

    .line 1332
    if-nez p1, :cond_2

    .line 1333
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getQueryInfo()Landroid/os/Bundle;

    move-result-object p1

    .line 1336
    :cond_2
    invoke-virtual {p1}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1337
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;->onStartQuery(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private storeHistoryKeyword(Ljava/lang/String;)V
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 2368
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2369
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isAllowedSavingHistory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2370
    invoke-static {p1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->updateHistoryItem(Ljava/lang/String;)Z

    .line 2371
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getRecentHistoryItems(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 2377
    :cond_0
    return-void
.end method

.method private updateCurrentScreen()V
    .locals 6

    .prologue
    .line 2327
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    const-string v5, "updateCurrentScreen"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2328
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrameHeight:I

    .line 2329
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2330
    .local v1, "context":Landroid/content/Context;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2331
    .local v3, "viewCnt":I
    const/4 v0, 0x0

    .line 2333
    .local v0, "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 2334
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 2335
    .restart local v0    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->refreshViewInScreen()V

    .line 2347
    :cond_0
    return-void

    .line 2337
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 2338
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 2340
    .restart local v0    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-static {v1, v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->findCurIndexInScreen(Landroid/content/Context;Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2341
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->refreshViewInScreen()V

    .line 2337
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2343
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->releaseAllItem()V

    goto :goto_1
.end method

.method private updatePossibilityTopFlicking()V
    .locals 2

    .prologue
    .line 1091
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollY()I

    move-result v0

    const/16 v1, 0x24

    if-ge v0, v1, :cond_0

    .line 1092
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsPosibleToTopFlicking:Z

    .line 1096
    :goto_0
    return-void

    .line 1094
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsPosibleToTopFlicking:Z

    goto :goto_0
.end method

.method private updateUserTagSelection(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1482
    .local p1, "searchedResultTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 1483
    .local v3, "resultTagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    const/4 v2, 0x0

    .line 1485
    .local v2, "isContainedAtTagBuffer":Z
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedUserTagBuffer()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 1486
    .local v4, "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1487
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setSelected(Z)V

    .line 1488
    const/4 v2, 0x1

    goto :goto_1

    .line 1492
    .end local v4    # "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_2
    if-nez v2, :cond_0

    .line 1493
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setSelected(Z)V

    goto :goto_0

    .line 1497
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "isContainedAtTagBuffer":Z
    .end local v3    # "resultTagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_3
    return-object p1
.end method


# virtual methods
.method public disableSelectionMode()V
    .locals 5

    .prologue
    .line 1144
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getAllCategoriesView()Ljava/util/List;

    move-result-object v3

    .line 1146
    .local v3, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .local v2, "view":Landroid/view/View;
    move-object v1, v2

    .line 1147
    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 1148
    .local v1, "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    const/16 v4, 0x97

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setViewMode(I)V

    goto :goto_0

    .line 1151
    .end local v1    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    const/4 v4, -0x1

    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I

    .line 1152
    return-void
.end method

.method public enableSelectionMode()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1129
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearchListExpand()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1130
    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->expandSearchList(Z)V

    .line 1133
    :cond_0
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I

    .line 1135
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->makeAvailableViewList()V

    .line 1136
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->makeSelectionMode()V

    .line 1138
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    if-eqz v0, :cond_1

    .line 1139
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedItemCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;->onChangeSelection(I)V

    .line 1141
    :cond_1
    return-void
.end method

.method public focusCustomSearchView()V
    .locals 1

    .prologue
    .line 2472
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    if-eqz v0, :cond_0

    .line 2473
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->requestFocus()Z

    .line 2475
    :cond_0
    return-void
.end method

.method public getCountCategories()I
    .locals 2

    .prologue
    .line 1111
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 1113
    .local v0, "count":I
    return v0
.end method

.method public getHistoryAdapter()Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    .locals 1

    .prologue
    .line 2464
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    return-object v0
.end method

.method public getSelectedItems(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;)V
    .locals 4
    .param p1, "callback"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;

    .prologue
    const/4 v3, 0x0

    .line 1191
    if-eqz p1, :cond_0

    .line 1192
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;)V

    .line 1193
    .local v0, "selectionTask":Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1195
    .end local v0    # "selectionTask":Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SelectionAsyncTask;
    :cond_0
    return-void
.end method

.method public getVisibleCustomSearchView()Z
    .locals 1

    .prologue
    .line 1522
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1523
    :cond_0
    const/4 v0, 0x0

    .line 1525
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hideKeypad()V
    .locals 1

    .prologue
    .line 1823
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->hideKeypad()V

    .line 1824
    return-void
.end method

.method public isAllowedSavingHistory()Z
    .locals 1

    .prologue
    .line 2468
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsAllowedSavingHistory:Z

    return v0
.end method

.method public isAvailableTagUpdate()Z
    .locals 2

    .prologue
    .line 2306
    const/4 v0, 0x0

    .line 2307
    .local v0, "bResult":Z
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearching()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2308
    const/4 v0, 0x1

    .line 2310
    :cond_0
    return v0
.end method

.method public isExistingSearchResult()Z
    .locals 1

    .prologue
    .line 852
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getCountCategories()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchListExpand()Z
    .locals 1

    .prologue
    .line 2161
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListExpanded()Z

    move-result v0

    return v0
.end method

.method public isSearchListHeadUp()Z
    .locals 1

    .prologue
    .line 2157
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListHeadUp()Z

    move-result v0

    return v0
.end method

.method public isSearching()Z
    .locals 1

    .prologue
    .line 1053
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsSearching:Z

    return v0
.end method

.method public isSelectionMode()Z
    .locals 2

    .prologue
    .line 1057
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingKeypad()Z
    .locals 1

    .prologue
    .line 2214
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->isShowingKeypad()Z

    move-result v0

    return v0
.end method

.method public isWritingBuddyShown()Z
    .locals 1

    .prologue
    .line 2314
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->isWritingBuddyShown()Z

    move-result v0

    return v0
.end method

.method public onActionClickDropDownList(Ljava/lang/Object;)V
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 912
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 913
    check-cast p1, Ljava/lang/String;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->storeHistoryKeyword(Ljava/lang/String;)V

    .line 921
    :cond_0
    :goto_0
    return-void

    .line 914
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 915
    check-cast v0, Ljava/util/ArrayList;

    .line 917
    .local v0, "datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v1, :cond_0

    .line 918
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->addSelectedItems(Ljava/util/ArrayList;Z)V

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 2040
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getQueryInfo()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2041
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getQueryInfo()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_query_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->storeHistoryKeyword(Ljava/lang/String;)V

    .line 2043
    :cond_0
    return-void
.end method

.method public onActionItemLongPressed(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 2047
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    if-eqz v0, :cond_0

    .line 2048
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;->onChangeSelectionModeState(Z)V

    .line 2050
    :cond_0
    return-void
.end method

.method public onActionOptionChange(Z)V
    .locals 0
    .param p1, "selected"    # Z

    .prologue
    .line 908
    return-void
.end method

.method public onActionVoiceRecognition()V
    .locals 1

    .prologue
    .line 925
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    if-eqz v0, :cond_0

    .line 926
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;->onRequestVoiceRecognition()V

    .line 928
    :cond_0
    return-void
.end method

.method public onActionWindowModeChange(Z)V
    .locals 1
    .param p1, "minimize"    # Z

    .prologue
    .line 932
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    if-eqz v0, :cond_0

    .line 933
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;->onChangeWindowMode(Z)V

    .line 935
    :cond_0
    return-void
.end method

.method public onChangeExpandMode(Landroid/view/View;Z)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "expanded"    # Z

    .prologue
    .line 2054
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 2055
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    if-eqz v6, :cond_0

    .line 2056
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0029

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 2058
    .local v4, "offset":I
    const/4 v6, 0x2

    new-array v2, v6, [I

    .line 2060
    .local v2, "loc":[I
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->getAppMainWindow()Landroid/view/Window;

    move-result-object v3

    .line 2061
    .local v3, "mainWin":Landroid/view/Window;
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2063
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2064
    .local v1, "layout":Landroid/view/WindowManager$LayoutParams;
    const/4 v6, 0x1

    aget v6, v2, v6

    iget v7, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int v5, v6, v7

    .line 2066
    .local v5, "viewStartY":I
    if-le v5, v4, :cond_0

    .line 2067
    sub-int v0, v5, v4

    .line 2069
    .local v0, "dstYPos":I
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    new-instance v7, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$13;

    invoke-direct {v7, p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$13;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;I)V

    const-wide/16 v8, 0x64

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2081
    .end local v0    # "dstYPos":I
    .end local v1    # "layout":Landroid/view/WindowManager$LayoutParams;
    .end local v2    # "loc":[I
    .end local v3    # "mainWin":Landroid/view/Window;
    .end local v4    # "offset":I
    .end local v5    # "viewStartY":I
    :cond_0
    return-void
.end method

.method public onChangeItemDragDropState(Z)V
    .locals 1
    .param p1, "started"    # Z

    .prologue
    .line 2085
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    if-eqz v0, :cond_0

    .line 2086
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;->onRequestWindowMoveToBack()V

    .line 2088
    :cond_0
    return-void
.end method

.method public onChangeSelectedCount(ILjava/lang/String;)V
    .locals 6
    .param p1, "count"    # I
    .param p2, "packagename"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x12f

    .line 2010
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2011
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 2014
    :cond_0
    iget v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemSelectionMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 2015
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectableCategoriesView()Ljava/util/List;

    move-result-object v3

    .line 2017
    .local v3, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .local v2, "view":Landroid/view/View;
    move-object v1, v2

    .line 2018
    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 2020
    .local v1, "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    if-nez p1, :cond_2

    .line 2022
    const/16 v4, 0x98

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setViewMode(I)V

    goto :goto_0

    .line 2026
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2027
    const/16 v4, 0x99

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->setViewMode(I)V

    goto :goto_0

    .line 2033
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v2    # "view":Landroid/view/View;
    .end local v3    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    if-eqz v4, :cond_4

    .line 2034
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getSelectedItemCount()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;->onChangeSelection(I)V

    .line 2036
    :cond_4
    return-void
.end method

.method public onChangedCategoryState(Landroid/view/View;I)V
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "reduceHeight"    # I

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    .line 2093
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 2094
    .local v6, "viewCnt":I
    const/4 v0, 0x0

    .line 2095
    .local v0, "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 2096
    .local v1, "currentViewIndex":I
    new-array v4, v12, [I

    .line 2097
    .local v4, "loc":[I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "window"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    .line 2098
    .local v8, "wm":Landroid/view/WindowManager;
    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 2099
    .local v2, "display":Landroid/view/Display;
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 2100
    .local v5, "size":Landroid/graphics/Point;
    invoke-virtual {v2, v5}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2102
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_4

    .line 2103
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoriesStoringList:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 2105
    .restart local v0    # "categoryView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryExpandedMode()I

    move-result v9

    if-eq v9, v12, :cond_2

    .line 2106
    invoke-virtual {v0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getLocationOnScreen([I)V

    .line 2107
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getBottom()I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v10}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollY()I

    move-result v10

    sub-int v7, v9, v10

    .line 2108
    .local v7, "viewEnd":I
    if-ge v3, v1, :cond_3

    .line 2109
    aget v9, v4, v11

    add-int/2addr v9, p2

    if-ltz v9, :cond_0

    aget v9, v4, v11

    add-int/2addr v9, p2

    iget v10, v5, Landroid/graphics/Point;->y:I

    if-le v9, v10, :cond_1

    :cond_0
    add-int v9, v7, p2

    if-lez v9, :cond_2

    add-int v9, v7, p2

    iget v10, v5, Landroid/graphics/Point;->y:I

    if-gt v9, v10, :cond_2

    .line 2111
    :cond_1
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->refreshCategoryAfterCollapse(Z)V

    .line 2102
    .end local v7    # "viewEnd":I
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2113
    .restart local v7    # "viewEnd":I
    :cond_3
    if-le v3, v1, :cond_2

    .line 2114
    aget v9, v4, v11

    sub-int/2addr v9, p2

    iget v10, v5, Landroid/graphics/Point;->y:I

    if-ge v9, v10, :cond_2

    .line 2115
    invoke-virtual {v0, v11}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->refreshCategoryAfterCollapse(Z)V

    goto :goto_1

    .line 2120
    .end local v7    # "viewEnd":I
    :cond_4
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 878
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 880
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 882
    .local v0, "imageMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04aa

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 885
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSymbolHelpView:Landroid/widget/RelativeLayout;

    const v4, 0x7f0b00d2

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 886
    .local v1, "symbolText":Landroid/widget/TextView;
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 888
    .local v2, "textMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04b0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 892
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UI Mode is seperated mode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUIModeIsSeperated:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    return-void
.end method

.method public onMandateTouch(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1934
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v1, :cond_1

    .line 1936
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    if-eqz v2, :cond_0

    .line 1937
    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->expandSearchList(Z)V

    .line 1938
    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsInTopFlickingTouch:Z

    .line 1965
    :cond_0
    :goto_0
    return v0

    .line 1943
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUIModeIsSeperated:Z

    if-eqz v2, :cond_2

    .line 1944
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideSIPWithTagTouched(FF)V

    .line 1945
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    goto :goto_0

    .line 1949
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsTablet:Z

    if-eqz v2, :cond_3

    .line 1950
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isHeadupOmitted()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1952
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearchListExpand()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1953
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->actionMoveFlickDown(Landroid/view/MotionEvent;)V

    :cond_3
    :goto_1
    move v0, v1

    .line 1965
    goto :goto_0

    .line 1954
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearchListHeadUp()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1955
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollY()I

    move-result v0

    if-gtz v0, :cond_5

    .line 1956
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->actionMoveFlickUp(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 1958
    :cond_5
    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->expandSearchList(Z)V

    .line 1959
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 1960
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 7
    .param p1, "newQuery"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x20

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 955
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onQueryTextChange : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setTagCloudDataToQuery()Z

    .line 959
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 960
    const-string v1, "\\s+"

    const-string v2, " "

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 963
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_0

    .line 964
    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 968
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 970
    .local v0, "length":I
    if-lez v0, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_1

    .line 971
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 975
    .end local v0    # "length":I
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfo(Ljava/lang/String;)V

    .line 977
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListHeadUp()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUIModeIsSeperated:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListExpanded()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 979
    :cond_2
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->foldSearchList(Z)V

    .line 982
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 983
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 988
    :goto_0
    return v4

    .line 985
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 939
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onQueryTextSubmit : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setTagCloudDataToQuery()Z

    .line 942
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfo(Ljava/lang/String;)V

    .line 944
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->storeHistoryKeyword(Ljava/lang/String;)V

    .line 946
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListHeadUp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 947
    invoke-direct {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->expandSearchList(Z)V

    .line 950
    :cond_0
    return v3
.end method

.method public onWritingBuddyCategoryUpdate(Z)V
    .locals 0
    .param p1, "bHandwriting"    # Z

    .prologue
    .line 2232
    return-void
.end method

.method public onWritingBuddyMode(Ljava/lang/String;)V
    .locals 1
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 2219
    if-nez p1, :cond_1

    .line 2227
    :cond_0
    :goto_0
    return-void

    .line 2222
    :cond_1
    const-string v0, "OPENED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2223
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->showSymbolHelp(Z)V

    goto :goto_0

    .line 2224
    :cond_2
    const-string v0, "CLOSED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2225
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->showSymbolHelp(Z)V

    goto :goto_0
.end method

.method public onWritingBuddySymbolQuery(Ljava/lang/String;)V
    .locals 4
    .param p1, "symbol"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 2236
    const/4 v0, 0x0

    .line 2239
    .local v0, "existSelected":Z
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setTagCloudDataToQuery()Z

    .line 2242
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 2243
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfo(Ljava/lang/String;)V

    .line 2244
    const/4 v0, 0x1

    .line 2248
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListHeadUp()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mUIModeIsSeperated:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchListAnimationController:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isSearchListExpanded()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2250
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->foldSearchList(Z)V

    .line 2254
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->storeHistoryKeyword(Ljava/lang/String;)V

    .line 2257
    if-eqz v0, :cond_3

    .line 2258
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 2268
    :goto_0
    return-void

    .line 2260
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2261
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto :goto_0

    .line 2264
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public pause()V
    .locals 5

    .prologue
    .line 992
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getAllCategoriesView()Ljava/util/List;

    move-result-object v3

    .line 994
    .local v3, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .local v2, "view":Landroid/view/View;
    move-object v1, v2

    .line 995
    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 996
    .local v1, "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->pause()V

    goto :goto_0

    .line 998
    .end local v1    # "v":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public processKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1701
    const/4 v0, 0x0

    .line 1703
    .local v0, "bUsed":Z
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1717
    :cond_0
    :goto_0
    return v0

    .line 1705
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getCountCategories()I

    move-result v1

    if-gtz v1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearching()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedFilter()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedPanel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1707
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->requestCleanupByKey()V

    .line 1708
    const/4 v0, 0x1

    goto :goto_0

    .line 1703
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public refreshCategoryTagInfo()V
    .locals 6

    .prologue
    .line 1298
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getAllCategoriesView()Ljava/util/List;

    move-result-object v4

    .line 1300
    .local v4, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1301
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .local v3, "v":Landroid/view/View;
    move-object v0, v3

    .line 1302
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    .line 1303
    .local v0, "baseView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;->getCategoryInfo()Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v1

    .line 1305
    .local v1, "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->supportTagSearch()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1306
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getAllTags()Ljava/util/HashMap;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->accumulateSearchedResultTags(Ljava/util/HashMap;)V

    goto :goto_0

    .line 1310
    .end local v0    # "baseView":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .end local v1    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setTagDatasForSearchResults()V

    .line 1312
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method public requestCleanupByKey()V
    .locals 2

    .prologue
    .line 1654
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1655
    return-void
.end method

.method public requestReloadData()V
    .locals 2

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1681
    return-void
.end method

.method public requestRequery()V
    .locals 3

    .prologue
    .line 1684
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setTagCloudDataToQuery()Z

    .line 1685
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getQueryInfo()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1686
    return-void
.end method

.method public requestSearchByVoiceRecognition(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 1658
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1659
    return-void
.end method

.method public requestUpdateByKeypad()V
    .locals 4

    .prologue
    const/16 v2, 0x12e

    .line 2318
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2319
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2321
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 2322
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 2323
    return-void
.end method

.method public requestUpdateTagItem(IZ)V
    .locals 1
    .param p1, "tagCloudType"    # I
    .param p2, "neededRequery"    # Z

    .prologue
    .line 1689
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearSelectedUserTagBuffer()V

    .line 1691
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v0, :cond_0

    .line 1692
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->initializeAllItems()V

    .line 1694
    if-eqz p2, :cond_0

    .line 1695
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->requestRequery()V

    .line 1698
    :cond_0
    return-void
.end method

.method public responseSearchCompleted()V
    .locals 2

    .prologue
    .line 1672
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1673
    return-void
.end method

.method public responseSearchResult(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "from"    # Ljava/lang/String;

    .prologue
    .line 1662
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setResponseFrom(Ljava/lang/String;)V

    .line 1664
    if-eqz p2, :cond_0

    const-string v0, "query_from_reload"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1665
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1669
    :goto_0
    return-void

    .line 1667
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public responseSearchTagRetrieved()V
    .locals 2

    .prologue
    .line 1676
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHandler:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1677
    return-void
.end method

.method public setAllowSavingHistory(Z)V
    .locals 2
    .param p1, "allow"    # Z

    .prologue
    const/4 v1, 0x1

    .line 2454
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsAllowedSavingHistory:Z

    if-nez v0, :cond_0

    if-ne p1, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isAppStart:Z

    if-nez v0, :cond_0

    .line 2455
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryOpenDelay:I

    .line 2456
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isAppStart:Z

    .line 2458
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsAllowedSavingHistory:Z

    .line 2460
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isQueryEmpty()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->switchMainView(Z)V

    .line 2461
    return-void
.end method

.method public setCustomSearchView(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V
    .locals 4
    .param p1, "view"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .prologue
    const/4 v1, 0x0

    .line 732
    if-eqz p1, :cond_1

    .line 733
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    .line 735
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsTablet:Z

    if-eqz v3, :cond_0

    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getRecentHistoryItems(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :cond_0
    const/4 v3, 0x1

    invoke-direct {v0, v2, v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 737
    .local v0, "adapter":Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$11;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$11;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->setFilterQueryProvider(Landroid/widget/FilterQueryProvider;)V

    .line 750
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->setSearchAutoCompleteAdapter(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)V

    .line 752
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->setOnCustomActionListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;)V

    .line 753
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->addOnCustomQueryTextListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;)V

    .line 755
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v1, :cond_1

    .line 756
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->addOnCustomQueryTextListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;)V

    .line 759
    .end local v0    # "adapter":Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    :cond_1
    return-void
.end method

.method public setGenericActionListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    .prologue
    .line 1534
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mGenericActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$IGenericActionListener;

    .line 1535
    return-void
.end method

.method public setItemStateListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    .prologue
    .line 1125
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mItemStateListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ICategoryItemStateListener;

    .line 1126
    return-void
.end method

.method public setMultiWindowModeChanged(Z)V
    .locals 0
    .param p1, "isMultiWindowMode"    # Z

    .prologue
    .line 2351
    return-void
.end method

.method public setSearchActionListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    .prologue
    .line 1530
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mSearchActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;

    .line 1531
    return-void
.end method

.method public setTagCloudDataToQuery()Z
    .locals 10

    .prologue
    .line 762
    const/4 v1, 0x0

    .line 763
    .local v1, "existSelected":Z
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-nez v9, :cond_0

    move v2, v1

    .line 805
    .end local v1    # "existSelected":Z
    .local v2, "existSelected":I
    :goto_0
    return v2

    .line 766
    .end local v2    # "existSelected":I
    .restart local v1    # "existSelected":Z
    :cond_0
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getSelectedFilterInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 768
    .local v0, "cloudData":Landroid/os/Bundle;
    if-eqz v0, :cond_4

    .line 769
    const-string v9, "filter_group_time"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 770
    .local v5, "tagTime":Ljava/lang/String;
    const-string v9, "filter_group_category"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 771
    .local v3, "tagCategory":Ljava/lang/String;
    const-string v9, "filter_group_user"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 772
    .local v6, "tagUser":Ljava/lang/String;
    const-string v9, "filter_group_category_filters"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 774
    .local v4, "tagFilters":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearOueryInfoExtras()V

    .line 776
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    .line 777
    invoke-direct {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfoTimeSpan(Ljava/lang/String;)V

    .line 779
    const/4 v1, 0x1

    .line 783
    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 784
    new-instance v8, Ljava/util/ArrayList;

    const-string v9, ","

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 786
    .local v8, "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v8}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfoTargets(Ljava/util/ArrayList;)V

    .line 788
    const/4 v1, 0x1

    .line 791
    .end local v8    # "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/os/Bundle;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 792
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfoSearchFilters(Landroid/os/Bundle;)V

    .line 794
    const/4 v1, 0x1

    .line 797
    :cond_3
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    .line 798
    new-instance v7, Ljava/util/ArrayList;

    const-string v9, "\\|"

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 799
    .local v7, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfoTags(Ljava/util/ArrayList;)V

    .line 801
    const/4 v1, 0x1

    .end local v3    # "tagCategory":Ljava/lang/String;
    .end local v4    # "tagFilters":Landroid/os/Bundle;
    .end local v5    # "tagTime":Ljava/lang/String;
    .end local v6    # "tagUser":Ljava/lang/String;
    .end local v7    # "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    move v2, v1

    .line 805
    .restart local v2    # "existSelected":I
    goto :goto_0
.end method

.method public showKeypad()V
    .locals 1

    .prologue
    .line 1827
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCustomSearchView:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->showKeypad()V

    .line 1828
    return-void
.end method

.method public startSyncTagByLauncher([Ljava/lang/String;I)V
    .locals 1
    .param p1, "category"    # [Ljava/lang/String;
    .param p2, "timeIndex"    # I

    .prologue
    .line 2478
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideKeypad()V

    .line 2480
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpAllTags()V

    .line 2481
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpEditText()V

    .line 2482
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpSearchResult(Z)V

    .line 2483
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->startSyncByLauncher([Ljava/lang/String;I)V

    .line 2484
    return-void
.end method

.method public switchMainView(Z)V
    .locals 8
    .param p1, "bShowHistory"    # Z

    .prologue
    const/4 v4, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 2380
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    if-eqz v2, :cond_2

    .line 2381
    if-eqz p1, :cond_8

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearching()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2382
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-ne v2, v3, :cond_0

    .line 2383
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 2384
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryFrame:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v2, v3, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2387
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2388
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mIsAllowedSavingHistory:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryAdapter:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_6

    .line 2391
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getVisibility()I

    move-result v2

    if-ne v2, v7, :cond_4

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedPanel()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2393
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f040008

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2395
    .local v0, "ani":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/LayoutAnimationController;

    const v2, 0x3d8f5c29    # 0.07f

    invoke-direct {v1, v0, v2}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/view/animation/Animation;F)V

    .line 2398
    .local v1, "controller":Landroid/view/animation/LayoutAnimationController;
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isAppStart:Z

    if-ne v2, v4, :cond_3

    .line 2399
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$15;

    invoke-direct {v3, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$15;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Landroid/view/animation/LayoutAnimationController;)V

    iget v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryOpenDelay:I

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2421
    .end local v0    # "ani":Landroid/view/animation/Animation;
    .end local v1    # "controller":Landroid/view/animation/LayoutAnimationController;
    :goto_0
    invoke-direct {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setVisibilityGuideText(Z)V

    .line 2433
    :goto_1
    iput-boolean v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isAppStart:Z

    .line 2436
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v2, v7}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->setVisibility(I)V

    .line 2451
    :cond_2
    :goto_2
    return-void

    .line 2411
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    .restart local v1    # "controller":Landroid/view/animation/LayoutAnimationController;
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 2412
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    goto :goto_0

    .line 2414
    .end local v0    # "ani":Landroid/view/animation/Animation;
    .end local v1    # "controller":Landroid/view/animation/LayoutAnimationController;
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedPanel()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2415
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->clearAnimation()V

    .line 2416
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v7}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 2418
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 2424
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 2426
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedPanel()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2427
    invoke-direct {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setVisibilityGuideText(Z)V

    goto :goto_1

    .line 2429
    :cond_7
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setVisibilityGuideText(Z)V

    goto :goto_1

    .line 2439
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    if-ne v2, v3, :cond_9

    .line 2440
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 2441
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrame:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mFilterPanelView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    invoke-virtual {v2, v3, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2444
    :cond_9
    invoke-direct {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setVisibilityGuideText(Z)V

    .line 2446
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-virtual {v2, v6}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->setVisibility(I)V

    .line 2447
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->clearAnimation()V

    .line 2448
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v7}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_2
.end method
