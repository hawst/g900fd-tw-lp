.class Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SearchListAnimationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->playScrollviewHeadUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isaHeadUpAnimationCanceled:Z

    .line 210
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 211
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    iget v1, v1, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    .line 216
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isaHeadUpAnimationCanceled:Z

    .line 218
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->access$100(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->access$100(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->forceLayout()V

    .line 220
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->access$100(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 223
    :cond_0
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 224
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->access$200(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->access$200(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;->afterHeadUp()V

    .line 227
    :cond_1
    return-void
.end method
