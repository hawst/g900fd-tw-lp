.class Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SearchListAnimationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->playScrollViewExpand(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$3;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 282
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$3;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    .line 283
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$3;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->access$200(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;->afterExpand()V

    .line 284
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$3;->this$0:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 285
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 286
    return-void
.end method
