.class public Lcom/samsung/android/app/galaxyfinder/tag/TagReady;
.super Ljava/lang/Object;
.source "TagReady.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;
    }
.end annotation


# static fields
.field private static final REQUEST_FETCH_DELAY:I = 0x1

.field private static final RESPONSE_FETCH_DELAY:I = 0x2

.field private static final TAG:Ljava/lang/String; = "TagReady"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mMainHandler:Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;

.field private mSearchableInfo:Landroid/app/SearchableInfo;

.field private mTagProbeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/SearchableInfo;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchableInfo"    # Landroid/app/SearchableInfo;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;

    .line 66
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mHandlerThread:Landroid/os/HandlerThread;

    .line 68
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mMainHandler:Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mContext:Landroid/content/Context;

    .line 73
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mContext:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mSearchableInfo:Landroid/app/SearchableInfo;

    .line 75
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->init()V

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/tag/TagReady;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;

    return-object v0
.end method

.method private addTagProbes(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "probes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 108
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;

    .line 109
    .local v0, "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 112
    .end local v0    # "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method private createProbes()V
    .locals 2

    .prologue
    .line 102
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;

    move-result-object v0

    .line 103
    .local v0, "factory":Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mSearchableInfo:Landroid/app/SearchableInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;->get(Landroid/app/SearchableInfo;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->addTagProbes(Ljava/util/List;)V

    .line 104
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 82
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "TagReady"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mHandlerThread:Landroid/os/HandlerThread;

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 84
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/TagReady;Landroid/os/Looper;Lcom/samsung/android/app/galaxyfinder/tag/TagReady;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mMainHandler:Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;

    .line 86
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->createProbes()V

    .line 87
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->ready()V

    .line 88
    return-void
.end method

.method private ready()V
    .locals 5

    .prologue
    .line 91
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 93
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 94
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;

    .line 95
    .local v0, "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    const-string v2, "TagReady"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ready() Probe name : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-interface {v0, p0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;->monitor(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$TagProbeProcessListener;)V

    goto :goto_0

    .line 99
    .end local v0    # "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;>;"
    :cond_0
    return-void
.end method


# virtual methods
.method public onProcessed(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bResult"    # Z

    .prologue
    .line 135
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 115
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 117
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;

    .line 119
    .local v0, "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;->stop()V

    goto :goto_0

    .line 122
    .end local v0    # "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;>;"
    :cond_0
    return-void
.end method

.method public requestFullSyncTagReady(I)V
    .locals 5
    .param p1, "reqType"    # I

    .prologue
    .line 125
    const-string v0, "TagReady"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestFullSyncTagReady() reqType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", package : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mSearchableInfo:Landroid/app/SearchableInfo;

    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mMainHandler:Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;

    const/4 v1, 0x1

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v4

    aget-object v4, v4, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 131
    return-void
.end method
