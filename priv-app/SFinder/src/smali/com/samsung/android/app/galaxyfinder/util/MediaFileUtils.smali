.class public Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;
.super Ljava/lang/Object;
.source "MediaFileUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;
    }
.end annotation


# static fields
.field public static final FILE_TYPE_ASC:I = 0x4e

.field public static final FILE_TYPE_CSV:I = 0x50

.field public static final FILE_TYPE_DCF:I = 0x12c

.field public static final FILE_TYPE_DOC:I = 0x52

.field public static final FILE_TYPE_EBOOK:I = 0x59

.field public static final FILE_TYPE_GUL:I = 0x57

.field public static final FILE_TYPE_HWP:I = 0x4d

.field public static final FILE_TYPE_ODF:I = 0x12e

.field public static final FILE_TYPE_PDF:I = 0x51

.field public static final FILE_TYPE_PPS:I = 0x4f

.field public static final FILE_TYPE_PPSX:I = 0x55

.field public static final FILE_TYPE_PPT:I = 0x54

.field public static final FILE_TYPE_SM4:I = 0x12d

.field public static final FILE_TYPE_SNB:I = 0x4c

.field public static final FILE_TYPE_SPD:I = 0x4b

.field public static final FILE_TYPE_SSF:I = 0x4a

.field public static final FILE_TYPE_TXT:I = 0x56

.field public static final FILE_TYPE_XLS:I = 0x53

.field public static sExtensionToMediaFileTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x4f

    const/16 v6, 0x54

    const/16 v5, 0x53

    const/16 v4, 0x52

    .line 11
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    .line 73
    const-string v0, "SSF"

    const/16 v1, 0x4a

    const-string v2, "application/ssf"

    const-string v3, "SSF"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v0, "SPD"

    const/16 v1, 0x4b

    const-string v2, "application/spd"

    const-string v3, "SPD"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v0, "SNB"

    const/16 v1, 0x4c

    const-string v2, "application/snb"

    const-string v3, "SNB"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v0, "HWP"

    const/16 v1, 0x4d

    const-string v2, "application/x-hwp"

    const-string v3, "HWP"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v0, "PPS"

    const-string v1, "application/vnd.ms-powerpoint"

    const-string v2, "Microsoft Office PowerPoint"

    invoke-static {v0, v7, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v0, "CSV"

    const/16 v1, 0x50

    const-string v2, "text/comma-separated-values"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v0, "PDF"

    const/16 v1, 0x51

    const-string v2, "application/pdf"

    const-string v3, "Acrobat PDF"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v0, "RTF"

    const-string v1, "application/msword"

    const-string v2, "Microsoft Office WORD"

    invoke-static {v0, v4, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v0, "DOC"

    const-string v1, "application/msword"

    const-string v2, "Microsoft Office WORD"

    invoke-static {v0, v4, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v0, "DOCX"

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    const-string v2, "Microsoft Office WORD"

    invoke-static {v0, v4, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v0, "DOT"

    const-string v1, "application/msword"

    const-string v2, "Microsoft Office WORD"

    invoke-static {v0, v4, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v0, "DOTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    const-string v2, "Microsoft Office WORD"

    invoke-static {v0, v4, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v0, "XLS"

    const-string v1, "application/vnd.ms-excel"

    const-string v2, "Microsoft Office Excel"

    invoke-static {v0, v5, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v0, "XLSX"

    const-string v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    const-string v2, "Microsoft Office Excel"

    invoke-static {v0, v5, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v0, "XLT"

    const-string v1, "application/vnd.ms-excel"

    const-string v2, "Microsoft Office Excel"

    invoke-static {v0, v5, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v0, "XLTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    const-string v2, "Microsoft Office Excel"

    invoke-static {v0, v5, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v0, "PPS"

    const-string v1, "application/vnd.ms-powerpoint"

    const-string v2, "Microsoft Office PowerPoint"

    invoke-static {v0, v7, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v0, "PPT"

    const-string v1, "application/vnd.ms-powerpoint"

    const-string v2, "Microsoft Office PowerPoint"

    invoke-static {v0, v6, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v0, "PPTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    const-string v2, "Microsoft Office PowerPoint"

    invoke-static {v0, v6, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v0, "POT"

    const-string v1, "application/vnd.ms-powerpoint"

    const-string v2, "Microsoft Office PowerPoint"

    invoke-static {v0, v6, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v0, "POTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.presentationml.template"

    const-string v2, "Microsoft Office PowerPoint"

    invoke-static {v0, v6, v1, v2}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v0, "PPSX"

    const/16 v1, 0x55

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v0, "ASC"

    const/16 v1, 0x4e

    const-string v2, "text/plain"

    const-string v3, "Text Document"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v0, "TXT"

    const/16 v1, 0x56

    const-string v2, "text/plain"

    const-string v3, "Text Document"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v0, "GUL"

    const/16 v1, 0x57

    const-string v2, "application/jungumword"

    const-string v3, "Jungum Word"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v0, "DCF"

    const/16 v1, 0x12c

    const-string v2, "application/vnd.oma.drm.content"

    const-string v3, "DRM Content"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v0, "ODF"

    const/16 v1, 0x12e

    const-string v2, "application/vnd.oma.drm.content"

    const-string v3, "DRM Content"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v0, "SM4"

    const/16 v1, 0x12d

    const-string v2, "video/vnd.sdrm-media.sm4"

    const-string v3, "DRM Content"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method

.method private static addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "extension"    # Ljava/lang/String;
    .param p1, "fileType"    # I
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "desc"    # Ljava/lang/String;

    .prologue
    .line 51
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;

    invoke-direct {v0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 53
    .local v0, "mediaFileType":Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method private static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 140
    if-nez p0, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-object v1

    .line 144
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 146
    .local v0, "lastDot":I
    if-ltz v0, :cond_0

    .line 150
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getShareMimeType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 123
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 124
    :cond_0
    const/4 v2, 0x0

    .line 136
    :cond_1
    :goto_0
    return-object v2

    .line 127
    :cond_2
    move-object v2, p1

    .line 128
    .local v2, "mimeType":Ljava/lang/String;
    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "ext":Ljava/lang/String;
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils;->sExtensionToMediaFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;

    .line 132
    .local v1, "mediaFileType":Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;
    if-eqz v1, :cond_1

    .line 133
    # getter for: Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;->mimeType:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;->access$000(Lcom/samsung/android/app/galaxyfinder/util/MediaFileUtils$MediaFileType;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
