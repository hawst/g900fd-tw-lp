.class public final Lcom/samsung/android/app/galaxyfinder/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_dropdown_list_height:I = 0x7f0a0000

.field public static final action_bar_dropdown_list_margin_bottom:I = 0x7f0a0001

.field public static final action_bar_dropdown_list_margin_left:I = 0x7f0a0002

.field public static final action_bar_dropdown_list_margin_top:I = 0x7f0a0003

.field public static final action_bar_dropdown_list_padding_left:I = 0x7f0a0004

.field public static final action_bar_dropdown_list_padding_right:I = 0x7f0a0005

.field public static final action_bar_dropdown_list_text_size:I = 0x7f0a0006

.field public static final action_bar_dropdown_list_width:I = 0x7f0a0007

.field public static final action_bar_text_size:I = 0x7f0a0008

.field public static final anim_searchlist_item_appear_position:I = 0x7f0a0009

.field public static final anim_searchlist_position_folded:I = 0x7f0a000a

.field public static final anim_searchlist_position_headup:I = 0x7f0a000b

.field public static final anim_searchlist_position_headup_3:I = 0x7f0a000c

.field public static final anim_searchlist_top_padding:I = 0x7f0a000d

.field public static final anim_searchlist_top_padding_with_task_suggestion:I = 0x7f0a000e

.field public static final anim_serachlist_min_height:I = 0x7f0a000f

.field public static final app_defaultsize_h:I = 0x7f0a0516

.field public static final app_defaultsize_w:I = 0x7f0a0517

.field public static final app_minimumsize_h:I = 0x7f0a0518

.field public static final app_minimumsize_w:I = 0x7f0a0519

.field public static final card_application_appname_margin_top:I = 0x7f0a0010

.field public static final card_application_appname_text_size:I = 0x7f0a0011

.field public static final card_application_frame_column_height:I = 0x7f0a0012

.field public static final card_application_frame_column_width:I = 0x7f0a0013

.field public static final card_application_frame_horizontal_spacing:I = 0x7f0a0014

.field public static final card_application_frame_margin_bottom:I = 0x7f0a0015

.field public static final card_application_frame_margin_left:I = 0x7f0a0016

.field public static final card_application_frame_margin_right:I = 0x7f0a0017

.field public static final card_application_frame_margin_top:I = 0x7f0a0018

.field public static final card_application_frame_vertical_spacing:I = 0x7f0a0019

.field public static final card_application_help_text_size:I = 0x7f0a001a

.field public static final card_application_icon_container_height:I = 0x7f0a001b

.field public static final card_application_icon_container_width:I = 0x7f0a001c

.field public static final card_application_icon_height:I = 0x7f0a001d

.field public static final card_application_icon_margin:I = 0x7f0a001e

.field public static final card_application_icon_padding:I = 0x7f0a001f

.field public static final card_application_icon_width:I = 0x7f0a0020

.field public static final card_browser_divider_do_icon_height:I = 0x7f0a051a

.field public static final card_browser_divider_do_icon_margin_right:I = 0x7f0a051b

.field public static final card_browser_divider_do_icon_width:I = 0x7f0a051c

.field public static final card_browser_divider_page_icon_height:I = 0x7f0a0021

.field public static final card_browser_frame_margin_divider_height:I = 0x7f0a0022

.field public static final card_browser_item_icon_margin_left:I = 0x7f0a0023

.field public static final card_browser_item_icon_margin_right:I = 0x7f0a0024

.field public static final card_browser_item_view_height:I = 0x7f0a0025

.field public static final card_browser_item_view_margin_left:I = 0x7f0a0026

.field public static final card_browser_item_view_margin_right:I = 0x7f0a0027

.field public static final card_browser_item_view_padding_bottom:I = 0x7f0a0028

.field public static final card_browser_item_view_padding_left:I = 0x7f0a0029

.field public static final card_browser_item_view_padding_right:I = 0x7f0a002a

.field public static final card_browser_item_view_padding_top:I = 0x7f0a002b

.field public static final card_browser_popup_icon_height:I = 0x7f0a051d

.field public static final card_browser_popup_icon_margin_left:I = 0x7f0a051e

.field public static final card_browser_popup_icon_width:I = 0x7f0a051f

.field public static final card_browser_preview_checkbox_margin_left:I = 0x7f0a002c

.field public static final card_browser_preview_item_view_padding_bottom:I = 0x7f0a002d

.field public static final card_browser_preview_item_view_padding_left:I = 0x7f0a002e

.field public static final card_browser_preview_item_view_padding_right:I = 0x7f0a002f

.field public static final card_browser_preview_item_view_padding_top:I = 0x7f0a0030

.field public static final card_browser_preview_text_vertical_spacing:I = 0x7f0a0031

.field public static final card_browser_preview_thumb_height:I = 0x7f0a0032

.field public static final card_browser_preview_thumb_margin_bottom:I = 0x7f0a0033

.field public static final card_browser_thumb_icon_height:I = 0x7f0a0034

.field public static final card_browser_thumb_icon_margin_bottom:I = 0x7f0a0035

.field public static final card_browser_thumb_icon_width:I = 0x7f0a0036

.field public static final card_browser_title_text_margin_left:I = 0x7f0a0037

.field public static final card_browser_title_text_size:I = 0x7f0a0038

.field public static final card_browser_url_text_margin_left:I = 0x7f0a0039

.field public static final card_browser_url_text_margin_top:I = 0x7f0a003a

.field public static final card_browser_url_text_size:I = 0x7f0a003b

.field public static final card_category_checkbox_margin_left:I = 0x7f0a003c

.field public static final card_category_checkbox_margin_right:I = 0x7f0a003d

.field public static final card_category_checkbox_margin_top:I = 0x7f0a003e

.field public static final card_category_container_margin_left:I = 0x7f0a003f

.field public static final card_category_container_margin_right:I = 0x7f0a0040

.field public static final card_category_divider_height:I = 0x7f0a0041

.field public static final card_category_divider_top_margin:I = 0x7f0a0042

.field public static final card_category_group_height:I = 0x7f0a0043

.field public static final card_category_group_height_first_item:I = 0x7f0a0044

.field public static final card_category_group_text_size:I = 0x7f0a0045

.field public static final card_category_group_title_margin_bottom:I = 0x7f0a0046

.field public static final card_category_group_title_margin_left:I = 0x7f0a0047

.field public static final card_category_group_title_margin_right:I = 0x7f0a0048

.field public static final card_category_group_title_padding_left:I = 0x7f0a0520

.field public static final card_category_group_title_padding_start:I = 0x7f0a0049

.field public static final card_category_group_title_padding_top:I = 0x7f0a004a

.field public static final card_category_group_title_underline_height:I = 0x7f0a004b

.field public static final card_category_group_top_margin_first_item:I = 0x7f0a004c

.field public static final card_category_item_focused_bg_width:I = 0x7f0a004d

.field public static final card_category_margin_bottom:I = 0x7f0a004e

.field public static final card_category_margin_left:I = 0x7f0a004f

.field public static final card_category_margin_right:I = 0x7f0a0050

.field public static final card_category_margin_top:I = 0x7f0a0051

.field public static final card_category_padding_bottom:I = 0x7f0a0052

.field public static final card_category_replay_action_width:I = 0x7f0a0053

.field public static final card_category_showmore_btn_height:I = 0x7f0a0054

.field public static final card_category_showmore_btn_margin_bottom:I = 0x7f0a0055

.field public static final card_category_showmore_btn_margin_top:I = 0x7f0a0056

.field public static final card_category_showmore_btn_padding:I = 0x7f0a0057

.field public static final card_category_showmore_btn_width:I = 0x7f0a0058

.field public static final card_category_text_count_margin_bottom:I = 0x7f0a0059

.field public static final card_category_text_count_margin_left:I = 0x7f0a005a

.field public static final card_category_text_count_margin_right:I = 0x7f0a005b

.field public static final card_category_text_title_margin_right:I = 0x7f0a005c

.field public static final card_category_title_checkbox_margin_left:I = 0x7f0a005d

.field public static final card_category_title_checkbox_margin_top:I = 0x7f0a005e

.field public static final card_category_title_count_size:I = 0x7f0a005f

.field public static final card_category_title_height:I = 0x7f0a0060

.field public static final card_category_title_layout_margin_bottom:I = 0x7f0a0061

.field public static final card_category_title_margin_left:I = 0x7f0a0521

.field public static final card_category_title_margin_right:I = 0x7f0a0510

.field public static final card_category_title_padding_left:I = 0x7f0a0062

.field public static final card_category_title_padding_right:I = 0x7f0a0063

.field public static final card_category_title_padding_top:I = 0x7f0a0064

.field public static final card_category_title_show_more_padding_top:I = 0x7f0a0065

.field public static final card_category_title_text_margin_right:I = 0x7f0a0066

.field public static final card_category_title_text_max:I = 0x7f0a0067

.field public static final card_category_title_text_size:I = 0x7f0a0068

.field public static final card_category_width:I = 0x7f0a0069

.field public static final card_chaton_action_layout_expanded_height:I = 0x7f0a006a

.field public static final card_chaton_action_layout_height:I = 0x7f0a006b

.field public static final card_chaton_action_layout_padding_bottom:I = 0x7f0a006c

.field public static final card_chaton_action_layout_padding_top:I = 0x7f0a006d

.field public static final card_chaton_action_layout_width:I = 0x7f0a006e

.field public static final card_chaton_attachment_height:I = 0x7f0a006f

.field public static final card_chaton_attachment_icon_margin_right:I = 0x7f0a0070

.field public static final card_chaton_attachment_margin_bottom:I = 0x7f0a0071

.field public static final card_chaton_attachment_margin_right:I = 0x7f0a0072

.field public static final card_chaton_attachment_text_size:I = 0x7f0a0073

.field public static final card_chaton_attachment_width:I = 0x7f0a0074

.field public static final card_chaton_body_margin_bottom:I = 0x7f0a0075

.field public static final card_chaton_body_margin_top:I = 0x7f0a0076

.field public static final card_chaton_body_width:I = 0x7f0a0077

.field public static final card_chaton_chat_height:I = 0x7f0a0078

.field public static final card_chaton_chat_margin_right:I = 0x7f0a0079

.field public static final card_chaton_chat_width:I = 0x7f0a007a

.field public static final card_chaton_checkbox_margin_left:I = 0x7f0a007b

.field public static final card_chaton_checkbox_margin_right:I = 0x7f0a007c

.field public static final card_chaton_date_height:I = 0x7f0a007d

.field public static final card_chaton_date_margin_left:I = 0x7f0a007e

.field public static final card_chaton_date_margin_right:I = 0x7f0a007f

.field public static final card_chaton_date_margin_top:I = 0x7f0a0080

.field public static final card_chaton_date_text_size:I = 0x7f0a0081

.field public static final card_chaton_divider_icon_big_height:I = 0x7f0a0082

.field public static final card_chaton_divider_icon_height:I = 0x7f0a0083

.field public static final card_chaton_divider_icon_margin_top:I = 0x7f0a0084

.field public static final card_chaton_divider_icon_width:I = 0x7f0a0085

.field public static final card_chaton_frame_margin_big_top:I = 0x7f0a0086

.field public static final card_chaton_frame_margin_bottom:I = 0x7f0a0087

.field public static final card_chaton_frame_margin_left:I = 0x7f0a0088

.field public static final card_chaton_frame_margin_right:I = 0x7f0a0089

.field public static final card_chaton_frame_margin_top:I = 0x7f0a008a

.field public static final card_chaton_frame_margin_vertical_spacing:I = 0x7f0a008b

.field public static final card_chaton_group_height:I = 0x7f0a008c

.field public static final card_chaton_group_text_size:I = 0x7f0a008d

.field public static final card_chaton_message_big_height:I = 0x7f0a008e

.field public static final card_chaton_message_height:I = 0x7f0a008f

.field public static final card_chaton_message_margin_bottom:I = 0x7f0a0090

.field public static final card_chaton_message_margin_right:I = 0x7f0a0091

.field public static final card_chaton_message_text_size:I = 0x7f0a0092

.field public static final card_chaton_name_height:I = 0x7f0a0093

.field public static final card_chaton_name_margin_bottom:I = 0x7f0a0094

.field public static final card_chaton_name_text_size:I = 0x7f0a0095

.field public static final card_chaton_number_height:I = 0x7f0a0096

.field public static final card_chaton_photo_height:I = 0x7f0a0097

.field public static final card_chaton_photo_margin_left:I = 0x7f0a0098

.field public static final card_chaton_photo_margin_right:I = 0x7f0a0099

.field public static final card_chaton_photo_width:I = 0x7f0a009a

.field public static final card_chaton_template_default_body_gap:I = 0x7f0a009b

.field public static final card_chaton_template_default_body_height:I = 0x7f0a009c

.field public static final card_chaton_template_default_height:I = 0x7f0a009d

.field public static final card_chaton_template_name_height:I = 0x7f0a009e

.field public static final card_chaton_template_phone_height:I = 0x7f0a009f

.field public static final card_common_checkbox_listtype_margin_left:I = 0x7f0a00a0

.field public static final card_common_checkbox_listtype_margin_right:I = 0x7f0a00a1

.field public static final card_contact_call_icon_height:I = 0x7f0a00a2

.field public static final card_contact_call_icon_margin_left:I = 0x7f0a00a3

.field public static final card_contact_call_icon_width:I = 0x7f0a00a4

.field public static final card_contact_divider_do_icon_height:I = 0x7f0a00a5

.field public static final card_contact_divider_do_icon_margin_left:I = 0x7f0a00a6

.field public static final card_contact_divider_do_icon_width:I = 0x7f0a00a7

.field public static final card_contact_divider_page_icon_height:I = 0x7f0a00a8

.field public static final card_contact_frame_column_height:I = 0x7f0a00a9

.field public static final card_contact_frame_column_width:I = 0x7f0a00aa

.field public static final card_contact_frame_margin_vertical_spacing:I = 0x7f0a00ab

.field public static final card_contact_item_view_height:I = 0x7f0a00ac

.field public static final card_contact_item_view_padding_bottom:I = 0x7f0a00ad

.field public static final card_contact_item_view_padding_left:I = 0x7f0a00ae

.field public static final card_contact_item_view_padding_right:I = 0x7f0a00af

.field public static final card_contact_item_view_padding_top:I = 0x7f0a00b0

.field public static final card_contact_name_icon_height:I = 0x7f0a00b1

.field public static final card_contact_name_icon_margin_right:I = 0x7f0a00b2

.field public static final card_contact_name_icon_width:I = 0x7f0a00b3

.field public static final card_contact_name_text_margin_top:I = 0x7f0a00b4

.field public static final card_contact_name_text_padding_right:I = 0x7f0a00b5

.field public static final card_contact_name_text_size:I = 0x7f0a00b6

.field public static final card_contact_name_text_width:I = 0x7f0a00b7

.field public static final card_contact_number_text_margin_bottom:I = 0x7f0a00b8

.field public static final card_contact_number_text_margin_top:I = 0x7f0a00b9

.field public static final card_contact_number_text_padding_right:I = 0x7f0a00ba

.field public static final card_contact_number_text_size:I = 0x7f0a00bb

.field public static final card_contact_number_text_width:I = 0x7f0a00bc

.field public static final card_default_desc_text_martin_bottom:I = 0x7f0a00bd

.field public static final card_default_desc_text_size:I = 0x7f0a00be

.field public static final card_default_divider_page_icon_height:I = 0x7f0a00bf

.field public static final card_default_divider_page_margin_left:I = 0x7f0a00c0

.field public static final card_default_divider_page_margin_right:I = 0x7f0a00c1

.field public static final card_default_frame_horizontal_spacing:I = 0x7f0a00c2

.field public static final card_default_frame_layout_height:I = 0x7f0a00c3

.field public static final card_default_frame_margin_bottom:I = 0x7f0a00c4

.field public static final card_default_frame_margin_left:I = 0x7f0a00c5

.field public static final card_default_frame_margin_right:I = 0x7f0a00c6

.field public static final card_default_frame_margin_top:I = 0x7f0a00c7

.field public static final card_default_icon_height:I = 0x7f0a00c8

.field public static final card_default_icon_width:I = 0x7f0a00c9

.field public static final card_default_info_margine_top:I = 0x7f0a00ca

.field public static final card_default_info_text_size:I = 0x7f0a00cb

.field public static final card_default_personal_icon_1_height:I = 0x7f0a00cc

.field public static final card_default_personal_icon_1_width:I = 0x7f0a00cd

.field public static final card_default_personal_icon_2_height:I = 0x7f0a00ce

.field public static final card_default_personal_icon_2_width:I = 0x7f0a00cf

.field public static final card_default_personal_icon_3_height:I = 0x7f0a00d0

.field public static final card_default_personal_icon_3_width:I = 0x7f0a00d1

.field public static final card_default_title_text_martin_top:I = 0x7f0a00d2

.field public static final card_default_title_text_size:I = 0x7f0a00d3

.field public static final card_email_account_color_chip_width:I = 0x7f0a00d4

.field public static final card_email_attach_icon_height:I = 0x7f0a00d5

.field public static final card_email_attach_icon_width:I = 0x7f0a00d6

.field public static final card_email_body_height:I = 0x7f0a00d7

.field public static final card_email_body_text_size:I = 0x7f0a00d8

.field public static final card_email_content_layout_padding_bottom:I = 0x7f0a00d9

.field public static final card_email_content_layout_padding_top:I = 0x7f0a00da

.field public static final card_email_date_margin_bottom:I = 0x7f0a00db

.field public static final card_email_date_margin_right:I = 0x7f0a00dc

.field public static final card_email_date_text_size:I = 0x7f0a00dd

.field public static final card_email_frame_margin_bottom:I = 0x7f0a00de

.field public static final card_email_frame_margin_left:I = 0x7f0a00df

.field public static final card_email_frame_margin_right:I = 0x7f0a00e0

.field public static final card_email_frame_margin_top:I = 0x7f0a00e1

.field public static final card_email_frame_margin_vertical_spacing:I = 0x7f0a00e2

.field public static final card_email_item_height:I = 0x7f0a00e3

.field public static final card_email_layout_padding_left:I = 0x7f0a00e4

.field public static final card_email_layout_padding_right:I = 0x7f0a00e5

.field public static final card_email_reply_margin_right:I = 0x7f0a00e6

.field public static final card_email_sender_height:I = 0x7f0a00e7

.field public static final card_email_sender_margin_right:I = 0x7f0a00e8

.field public static final card_email_sender_text_size:I = 0x7f0a00e9

.field public static final card_email_sort_title_layout_padding_bottom:I = 0x7f0a00ea

.field public static final card_email_sort_title_layout_padding_top:I = 0x7f0a00eb

.field public static final card_email_sort_title_margin_bottom:I = 0x7f0a00ec

.field public static final card_email_sort_title_textsize:I = 0x7f0a00ed

.field public static final card_email_state_icon_height:I = 0x7f0a00ee

.field public static final card_email_state_icon_priority_margin_top:I = 0x7f0a00ef

.field public static final card_email_state_icon_reply_margin_top:I = 0x7f0a00f0

.field public static final card_email_state_icon_width:I = 0x7f0a00f1

.field public static final card_email_sub_title_height:I = 0x7f0a00f2

.field public static final card_email_title_height:I = 0x7f0a00f3

.field public static final card_email_title_margin_right:I = 0x7f0a00f4

.field public static final card_email_title_text_size:I = 0x7f0a00f5

.field public static final card_gallery_check_margin_right:I = 0x7f0a00f6

.field public static final card_gallery_check_margin_top:I = 0x7f0a00f7

.field public static final card_gallery_date_text_height:I = 0x7f0a00f8

.field public static final card_gallery_date_text_margin_top:I = 0x7f0a00f9

.field public static final card_gallery_date_text_size:I = 0x7f0a00fa

.field public static final card_gallery_date_text_width:I = 0x7f0a00fb

.field public static final card_gallery_desc_area_height:I = 0x7f0a00fc

.field public static final card_gallery_desc_area_margin_bottom:I = 0x7f0a00fd

.field public static final card_gallery_desc_area_margin_left:I = 0x7f0a00fe

.field public static final card_gallery_desc_area_margin_right:I = 0x7f0a00ff

.field public static final card_gallery_desc_text_size:I = 0x7f0a0100

.field public static final card_gallery_frame_column_height:I = 0x7f0a0101

.field public static final card_gallery_frame_column_width:I = 0x7f0a0102

.field public static final card_gallery_frame_horizontal_spacing:I = 0x7f0a0103

.field public static final card_gallery_frame_padding_bottom:I = 0x7f0a0104

.field public static final card_gallery_frame_padding_left:I = 0x7f0a0105

.field public static final card_gallery_frame_padding_right:I = 0x7f0a0106

.field public static final card_gallery_frame_padding_top:I = 0x7f0a0107

.field public static final card_gallery_frame_vertical_spacing:I = 0x7f0a0108

.field public static final card_gallery_icon_area_height:I = 0x7f0a0109

.field public static final card_gallery_icon_area_width:I = 0x7f0a010a

.field public static final card_gallery_icon_margin_left:I = 0x7f0a010b

.field public static final card_gallery_item_view_height:I = 0x7f0a010c

.field public static final card_gallery_item_view_width:I = 0x7f0a010d

.field public static final card_gallery_location_text_height:I = 0x7f0a010e

.field public static final card_gallery_location_text_margin_top:I = 0x7f0a010f

.field public static final card_gallery_location_text_size:I = 0x7f0a0110

.field public static final card_gallery_location_text_width:I = 0x7f0a0111

.field public static final card_gallery_people_text_height:I = 0x7f0a0112

.field public static final card_gallery_people_text_margin_top:I = 0x7f0a0113

.field public static final card_gallery_people_text_size:I = 0x7f0a0114

.field public static final card_gallery_people_text_width:I = 0x7f0a0115

.field public static final card_gallery_share_icon_height:I = 0x7f0a0116

.field public static final card_gallery_share_icon_width:I = 0x7f0a0117

.field public static final card_gallery_text_area_height:I = 0x7f0a0118

.field public static final card_gallery_text_area_margin_top:I = 0x7f0a0119

.field public static final card_gallery_thumb_area_gap:I = 0x7f0a011a

.field public static final card_gallery_thumb_area_height:I = 0x7f0a011b

.field public static final card_gallery_thumb_area_width:I = 0x7f0a011c

.field public static final card_gallery_thumb_three_layout_margin_left:I = 0x7f0a011d

.field public static final card_gallery_thumb_two_layout_margin_left:I = 0x7f0a011e

.field public static final card_gallery_thumb_type_2_1_height:I = 0x7f0a011f

.field public static final card_gallery_thumb_type_2_1_width:I = 0x7f0a0120

.field public static final card_gallery_thumb_type_3_height:I = 0x7f0a0121

.field public static final card_gallery_thumb_type_3_width:I = 0x7f0a0122

.field public static final card_gallery_thumb_wide_big_area_height:I = 0x7f0a0123

.field public static final card_gallery_thumb_wide_big_area_width:I = 0x7f0a0124

.field public static final card_gallery_thumb_wide_big_item_view_height:I = 0x7f0a0125

.field public static final card_gallery_thumb_wide_big_item_view_width:I = 0x7f0a0126

.field public static final card_gallery_thumb_wide_small_area_height:I = 0x7f0a0127

.field public static final card_gallery_thumb_wide_small_area_width:I = 0x7f0a0128

.field public static final card_gallery_thumb_wide_small_view_height:I = 0x7f0a0129

.field public static final card_gallery_thumb_wide_small_view_width:I = 0x7f0a012a

.field public static final card_gallery_weather_text_height:I = 0x7f0a012b

.field public static final card_gallery_weather_text_size:I = 0x7f0a012c

.field public static final card_gallery_weather_text_width:I = 0x7f0a012d

.field public static final card_lifetimes_frame_margin_left:I = 0x7f0a012e

.field public static final card_lifetimes_frame_margin_right:I = 0x7f0a012f

.field public static final card_lifetimes_frame_margin_vertical_spacing:I = 0x7f0a0130

.field public static final card_lifetimes_image_thumb_height:I = 0x7f0a0131

.field public static final card_lifetimes_image_thumb_margin_bottom:I = 0x7f0a0132

.field public static final card_lifetimes_image_thumb_margin_top:I = 0x7f0a0133

.field public static final card_lifetimes_text_date_height:I = 0x7f0a0134

.field public static final card_lifetimes_text_date_margin_left:I = 0x7f0a0135

.field public static final card_lifetimes_text_date_margin_top:I = 0x7f0a0136

.field public static final card_lifetimes_text_date_size:I = 0x7f0a0137

.field public static final card_lifetimes_text_desc_height:I = 0x7f0a0138

.field public static final card_lifetimes_text_desc_margin_left:I = 0x7f0a0139

.field public static final card_lifetimes_text_desc_size:I = 0x7f0a013a

.field public static final card_list_item_first_text_size:I = 0x7f0a013b

.field public static final card_list_item_second_text_size:I = 0x7f0a013c

.field public static final card_memo_frame_margin_left:I = 0x7f0a013d

.field public static final card_memo_frame_margin_right:I = 0x7f0a013e

.field public static final card_memo_frame_margin_vertical_spacing:I = 0x7f0a013f

.field public static final card_memo_image_icon_lock_height:I = 0x7f0a0140

.field public static final card_memo_image_icon_lock_width:I = 0x7f0a0141

.field public static final card_memo_image_thumb_height:I = 0x7f0a0142

.field public static final card_memo_image_thumb_width:I = 0x7f0a0143

.field public static final card_memo_item_view_checkbox_margin_left:I = 0x7f0a0144

.field public static final card_memo_text_date_size:I = 0x7f0a0145

.field public static final card_memo_text_desc_margin_top:I = 0x7f0a0146

.field public static final card_memo_text_desc_size:I = 0x7f0a0147

.field public static final card_memo_text_group_height:I = 0x7f0a0148

.field public static final card_memo_text_group_padding_bottom:I = 0x7f0a0149

.field public static final card_memo_text_group_padding_top:I = 0x7f0a014a

.field public static final card_memo_text_title_size:I = 0x7f0a014b

.field public static final card_message_bg_height:I = 0x7f0a014c

.field public static final card_message_bg_margin_profile_to_body:I = 0x7f0a014d

.field public static final card_message_bg_padding_bottm:I = 0x7f0a014e

.field public static final card_message_bg_padding_profile_to_body:I = 0x7f0a014f

.field public static final card_message_bg_padding_profile_to_body_opposite:I = 0x7f0a0150

.field public static final card_message_bg_padding_top:I = 0x7f0a0151

.field public static final card_message_bg_width:I = 0x7f0a0152

.field public static final card_message_body_height:I = 0x7f0a0153

.field public static final card_message_body_max_width:I = 0x7f0a0154

.field public static final card_message_body_text_size:I = 0x7f0a0155

.field public static final card_message_checkBox_view_padding_right:I = 0x7f0a0156

.field public static final card_message_conv_group_height:I = 0x7f0a0157

.field public static final card_message_conv_group_padding_bottom:I = 0x7f0a0158

.field public static final card_message_conv_group_padding_left:I = 0x7f0a0159

.field public static final card_message_conv_group_padding_right:I = 0x7f0a015a

.field public static final card_message_conv_group_padding_top:I = 0x7f0a015b

.field public static final card_message_conv_text_body_height:I = 0x7f0a015c

.field public static final card_message_conv_text_body_size:I = 0x7f0a015d

.field public static final card_message_conv_text_date_size:I = 0x7f0a015e

.field public static final card_message_conv_text_group_height:I = 0x7f0a015f

.field public static final card_message_conv_text_group_padding_bottom:I = 0x7f0a0160

.field public static final card_message_conv_text_group_padding_top:I = 0x7f0a0161

.field public static final card_message_conv_text_sender_size:I = 0x7f0a0162

.field public static final card_message_conv_text_sub_group_height:I = 0x7f0a0163

.field public static final card_message_conv_text_sub_group_horizontal_spacing:I = 0x7f0a0164

.field public static final card_message_conv_text_sub_group_margin_top:I = 0x7f0a0165

.field public static final card_message_conv_thumb_height:I = 0x7f0a0166

.field public static final card_message_conv_thumb_margin:I = 0x7f0a0167

.field public static final card_message_conv_thumb_width:I = 0x7f0a0168

.field public static final card_message_conversation_date_width:I = 0x7f0a0169

.field public static final card_message_conversation_padding_right:I = 0x7f0a016a

.field public static final card_message_conversation_width:I = 0x7f0a016b

.field public static final card_message_date_text_size:I = 0x7f0a016c

.field public static final card_message_fail_margin_top:I = 0x7f0a016d

.field public static final card_message_fail_padding_right:I = 0x7f0a016e

.field public static final card_message_frame_margin_vertical_spacing:I = 0x7f0a016f

.field public static final card_message_ic_attachment_height:I = 0x7f0a0170

.field public static final card_message_ic_attachment_margin_right:I = 0x7f0a0171

.field public static final card_message_ic_attachment_margin_top:I = 0x7f0a0172

.field public static final card_message_ic_attachment_witdh:I = 0x7f0a0173

.field public static final card_message_item_view_height:I = 0x7f0a0174

.field public static final card_message_item_view_padding_left:I = 0x7f0a0175

.field public static final card_message_item_view_padding_right:I = 0x7f0a0176

.field public static final card_message_margin:I = 0x7f0a0177

.field public static final card_message_name_icon_height:I = 0x7f0a0178

.field public static final card_message_name_icon_margin_right:I = 0x7f0a0179

.field public static final card_message_name_icon_width:I = 0x7f0a017a

.field public static final card_message_sender_text_size:I = 0x7f0a017b

.field public static final card_message_sender_width:I = 0x7f0a017c

.field public static final card_message_sender_width_fail:I = 0x7f0a017d

.field public static final card_message_sort_title_margin_top:I = 0x7f0a017e

.field public static final card_message_sort_title_paddinng_bottom:I = 0x7f0a017f

.field public static final card_message_sort_title_target_textsize:I = 0x7f0a0180

.field public static final card_message_thread_group_height:I = 0x7f0a0181

.field public static final card_message_thread_group_padding_bottom:I = 0x7f0a0182

.field public static final card_message_thread_group_padding_left:I = 0x7f0a0183

.field public static final card_message_thread_group_padding_right:I = 0x7f0a0184

.field public static final card_message_thread_group_padding_top:I = 0x7f0a0185

.field public static final card_message_thread_text_body_size:I = 0x7f0a0186

.field public static final card_message_thread_text_date_size:I = 0x7f0a0187

.field public static final card_message_thread_text_group_height:I = 0x7f0a0188

.field public static final card_message_thread_text_group_padding_bottom:I = 0x7f0a0189

.field public static final card_message_thread_text_group_padding_top:I = 0x7f0a018a

.field public static final card_message_thread_text_sender_height:I = 0x7f0a018b

.field public static final card_message_thread_text_sender_size:I = 0x7f0a018c

.field public static final card_message_thread_text_sub_group_height:I = 0x7f0a018d

.field public static final card_message_thread_text_sub_group_horizontal_spacing:I = 0x7f0a018e

.field public static final card_message_thread_text_sub_group_margin_top:I = 0x7f0a018f

.field public static final card_message_thread_thumb_height:I = 0x7f0a0190

.field public static final card_message_thread_thumb_margin:I = 0x7f0a0191

.field public static final card_message_thread_thumb_width:I = 0x7f0a0192

.field public static final card_message_thumb_icon_height:I = 0x7f0a0193

.field public static final card_message_thumb_icon_width:I = 0x7f0a0194

.field public static final card_music_desc_text_margin_top:I = 0x7f0a0195

.field public static final card_music_desc_text_size:I = 0x7f0a0196

.field public static final card_music_frame_column_height:I = 0x7f0a0522

.field public static final card_music_frame_column_width:I = 0x7f0a0523

.field public static final card_music_frame_margin_bottom:I = 0x7f0a0524

.field public static final card_music_frame_margin_horizontal_spacing:I = 0x7f0a0525

.field public static final card_music_frame_margin_left:I = 0x7f0a0526

.field public static final card_music_frame_margin_right:I = 0x7f0a0527

.field public static final card_music_frame_margin_top:I = 0x7f0a0528

.field public static final card_music_frame_margin_vertical_spacing:I = 0x7f0a0529

.field public static final card_music_frame_padding_bottom:I = 0x7f0a0197

.field public static final card_music_frame_padding_left:I = 0x7f0a0198

.field public static final card_music_frame_padding_right:I = 0x7f0a0199

.field public static final card_music_frame_padding_top:I = 0x7f0a019a

.field public static final card_music_image_album_height:I = 0x7f0a019b

.field public static final card_music_image_album_width:I = 0x7f0a019c

.field public static final card_music_personal_icon_margin_left:I = 0x7f0a052a

.field public static final card_music_personal_icon_margin_top:I = 0x7f0a052b

.field public static final card_music_play_icon_height:I = 0x7f0a052c

.field public static final card_music_play_icon_margin_left:I = 0x7f0a052d

.field public static final card_music_play_icon_margin_top:I = 0x7f0a052e

.field public static final card_music_play_icon_width:I = 0x7f0a052f

.field public static final card_music_subtitle_text_margin_top:I = 0x7f0a0530

.field public static final card_music_subtitle_text_size:I = 0x7f0a0531

.field public static final card_music_text_area_height:I = 0x7f0a0532

.field public static final card_music_title_text_margin_top:I = 0x7f0a0533

.field public static final card_music_title_text_size:I = 0x7f0a019d

.field public static final card_music_title_text_width:I = 0x7f0a0534

.field public static final card_myfiles_capacity_text_margin_bottom:I = 0x7f0a0535

.field public static final card_myfiles_capacity_text_padding_left:I = 0x7f0a019e

.field public static final card_myfiles_capacity_text_padding_right:I = 0x7f0a019f

.field public static final card_myfiles_capacity_text_size:I = 0x7f0a01a0

.field public static final card_myfiles_divider_do_icon_height:I = 0x7f0a01a1

.field public static final card_myfiles_divider_do_icon_width:I = 0x7f0a01a2

.field public static final card_myfiles_divider_margin_bottom:I = 0x7f0a01a3

.field public static final card_myfiles_divider_margin_top:I = 0x7f0a01a4

.field public static final card_myfiles_frame_margin_vertical_spacing:I = 0x7f0a01a5

.field public static final card_myfiles_icon_height:I = 0x7f0a01a6

.field public static final card_myfiles_icon_width:I = 0x7f0a01a7

.field public static final card_myfiles_item_view_checkbox_margin_left:I = 0x7f0a01a8

.field public static final card_myfiles_item_view_height:I = 0x7f0a01a9

.field public static final card_myfiles_item_view_padding_bottom:I = 0x7f0a01aa

.field public static final card_myfiles_item_view_padding_left:I = 0x7f0a01ab

.field public static final card_myfiles_item_view_padding_right:I = 0x7f0a01ac

.field public static final card_myfiles_item_view_padding_top:I = 0x7f0a01ad

.field public static final card_myfiles_name_icon_height:I = 0x7f0a01ae

.field public static final card_myfiles_name_icon_margin_left:I = 0x7f0a01af

.field public static final card_myfiles_name_icon_margin_right:I = 0x7f0a01b0

.field public static final card_myfiles_name_icon_overlay_height:I = 0x7f0a01b1

.field public static final card_myfiles_name_icon_overlay_width:I = 0x7f0a01b2

.field public static final card_myfiles_name_icon_width:I = 0x7f0a01b3

.field public static final card_myfiles_name_text_margin_bottom:I = 0x7f0a01b4

.field public static final card_myfiles_name_text_margin_top:I = 0x7f0a01b5

.field public static final card_myfiles_name_text_size:I = 0x7f0a01b6

.field public static final card_myfiles_name_text_width:I = 0x7f0a01b7

.field public static final card_myfiles_text_1st_height:I = 0x7f0a01b8

.field public static final card_myfiles_text_2nd_height:I = 0x7f0a01b9

.field public static final card_myfiles_text_margin_top:I = 0x7f0a01ba

.field public static final card_note_cover_for_shadow_padding_bottom:I = 0x7f0a01bb

.field public static final card_note_cover_for_shadow_padding_end:I = 0x7f0a01bc

.field public static final card_note_cover_image_height:I = 0x7f0a01bd

.field public static final card_note_cover_image_padding_left:I = 0x7f0a01be

.field public static final card_note_cover_image_padding_right:I = 0x7f0a01bf

.field public static final card_note_cover_image_padding_top:I = 0x7f0a01c0

.field public static final card_note_cover_image_width:I = 0x7f0a01c1

.field public static final card_note_date_text_height:I = 0x7f0a01c2

.field public static final card_note_date_text_margin_top:I = 0x7f0a01c3

.field public static final card_note_date_text_size:I = 0x7f0a01c4

.field public static final card_note_date_text_width:I = 0x7f0a01c5

.field public static final card_note_folder_name_margin_top:I = 0x7f0a01c6

.field public static final card_note_frame_column_height:I = 0x7f0a01c7

.field public static final card_note_frame_column_width:I = 0x7f0a01c8

.field public static final card_note_frame_margin_bottom:I = 0x7f0a01c9

.field public static final card_note_frame_margin_horizontal_spacing:I = 0x7f0a01ca

.field public static final card_note_frame_margin_left:I = 0x7f0a01cb

.field public static final card_note_frame_margin_right:I = 0x7f0a01cc

.field public static final card_note_frame_margin_top:I = 0x7f0a01cd

.field public static final card_note_frame_margin_vertical_spacing:I = 0x7f0a01ce

.field public static final card_note_ic_height:I = 0x7f0a01cf

.field public static final card_note_ic_margin_bottom:I = 0x7f0a01d0

.field public static final card_note_ic_margin_left:I = 0x7f0a01d1

.field public static final card_note_ic_margin_right:I = 0x7f0a01d2

.field public static final card_note_ic_margin_top:I = 0x7f0a01d3

.field public static final card_note_ic_width:I = 0x7f0a01d4

.field public static final card_note_item_frame_padding_bottom:I = 0x7f0a01d5

.field public static final card_note_item_frame_padding_left:I = 0x7f0a01d6

.field public static final card_note_item_frame_padding_right:I = 0x7f0a01d7

.field public static final card_note_item_frame_padding_top:I = 0x7f0a01d8

.field public static final card_note_num_book_margin_top:I = 0x7f0a01d9

.field public static final card_note_stroke_view_height:I = 0x7f0a01da

.field public static final card_note_stroke_view_left_margin:I = 0x7f0a01db

.field public static final card_note_stroke_view_top_margin:I = 0x7f0a01dc

.field public static final card_note_stroke_view_width:I = 0x7f0a01dd

.field public static final card_note_subnote_page_padding_bottom:I = 0x7f0a01de

.field public static final card_note_subnote_page_padding_left:I = 0x7f0a01df

.field public static final card_note_subnote_page_padding_right:I = 0x7f0a01e0

.field public static final card_note_subnote_page_padding_top:I = 0x7f0a01e1

.field public static final card_note_time_height:I = 0x7f0a01e2

.field public static final card_note_time_text_size:I = 0x7f0a01e3

.field public static final card_note_title_text_height:I = 0x7f0a01e4

.field public static final card_note_title_text_margin_left:I = 0x7f0a01e5

.field public static final card_note_title_text_margin_right:I = 0x7f0a01e6

.field public static final card_note_title_text_margin_top:I = 0x7f0a01e7

.field public static final card_note_title_text_padding_left:I = 0x7f0a01e8

.field public static final card_note_title_text_padding_right:I = 0x7f0a01e9

.field public static final card_note_title_text_size:I = 0x7f0a01ea

.field public static final card_note_title_text_width:I = 0x7f0a01eb

.field public static final card_note_transparent_title_text_margin_bottom:I = 0x7f0a01ec

.field public static final card_note_transparent_title_text_margin_left:I = 0x7f0a01ed

.field public static final card_note_transparent_title_text_margin_right:I = 0x7f0a01ee

.field public static final card_note_transparent_title_text_padding_bottom:I = 0x7f0a01ef

.field public static final card_note_transparent_title_text_padding_left:I = 0x7f0a01f0

.field public static final card_note_transparent_title_text_padding_right:I = 0x7f0a01f1

.field public static final card_note_transparent_title_text_padding_top:I = 0x7f0a01f2

.field public static final card_phone_call_icon_height:I = 0x7f0a01f3

.field public static final card_phone_call_icon_margin_bottom:I = 0x7f0a01f4

.field public static final card_phone_call_icon_margin_left:I = 0x7f0a01f5

.field public static final card_phone_call_icon_margin_right:I = 0x7f0a01f6

.field public static final card_phone_call_icon_margin_top:I = 0x7f0a01f7

.field public static final card_phone_call_icon_width:I = 0x7f0a01f8

.field public static final card_phone_call_state_icon_height:I = 0x7f0a01f9

.field public static final card_phone_call_state_icon_width:I = 0x7f0a01fa

.field public static final card_phone_call_type_icon_height:I = 0x7f0a01fb

.field public static final card_phone_call_type_icon_width:I = 0x7f0a01fc

.field public static final card_phone_date_text_margin_top:I = 0x7f0a01fd

.field public static final card_phone_date_text_size:I = 0x7f0a01fe

.field public static final card_phone_divider_do_icon_height:I = 0x7f0a01ff

.field public static final card_phone_divider_do_icon_margin_left:I = 0x7f0a0200

.field public static final card_phone_divider_do_icon_width:I = 0x7f0a0201

.field public static final card_phone_divider_page_icon_height:I = 0x7f0a0202

.field public static final card_phone_item_view_height:I = 0x7f0a0203

.field public static final card_phone_item_view_layout_height:I = 0x7f0a0204

.field public static final card_phone_item_view_margin_left:I = 0x7f0a0205

.field public static final card_phone_item_view_margin_top:I = 0x7f0a0206

.field public static final card_phone_item_view_padding_bottom:I = 0x7f0a0207

.field public static final card_phone_item_view_padding_left:I = 0x7f0a0208

.field public static final card_phone_item_view_padding_right:I = 0x7f0a0209

.field public static final card_phone_item_view_padding_top:I = 0x7f0a020a

.field public static final card_phone_name_icon_height:I = 0x7f0a020b

.field public static final card_phone_name_icon_margin_bottom:I = 0x7f0a020c

.field public static final card_phone_name_icon_margin_left:I = 0x7f0a020d

.field public static final card_phone_name_icon_margin_right:I = 0x7f0a020e

.field public static final card_phone_name_icon_margin_top:I = 0x7f0a020f

.field public static final card_phone_name_icon_width:I = 0x7f0a0210

.field public static final card_phone_name_text_height:I = 0x7f0a0211

.field public static final card_phone_name_text_margin:I = 0x7f0a0212

.field public static final card_phone_name_text_padding_top:I = 0x7f0a0213

.field public static final card_phone_name_text_size:I = 0x7f0a0214

.field public static final card_phone_name_text_width:I = 0x7f0a0215

.field public static final card_phone_number_text_padding_right:I = 0x7f0a0216

.field public static final card_phone_number_text_size:I = 0x7f0a0217

.field public static final card_phone_time_text_margin_right:I = 0x7f0a0218

.field public static final card_phone_time_text_margin_top:I = 0x7f0a0219

.field public static final card_pinall_container_height:I = 0x7f0a021a

.field public static final card_pinall_containter_marginTop:I = 0x7f0a021b

.field public static final card_pinall_content_height:I = 0x7f0a021c

.field public static final card_pinall_content_margin:I = 0x7f0a021d

.field public static final card_pinall_date_height:I = 0x7f0a021e

.field public static final card_pinall_date_text_size:I = 0x7f0a021f

.field public static final card_pinall_description_line_spacing:I = 0x7f0a0220

.field public static final card_pinall_description_padding:I = 0x7f0a0221

.field public static final card_pinall_description_text_size:I = 0x7f0a0222

.field public static final card_pinall_frame_column_height:I = 0x7f0a0223

.field public static final card_pinall_frame_column_width:I = 0x7f0a0224

.field public static final card_pinall_frame_margin_bottom:I = 0x7f0a0225

.field public static final card_pinall_frame_margin_horizontal_spacing:I = 0x7f0a0226

.field public static final card_pinall_frame_margin_left:I = 0x7f0a0227

.field public static final card_pinall_frame_margin_right:I = 0x7f0a0228

.field public static final card_pinall_frame_margin_top:I = 0x7f0a0229

.field public static final card_pinall_frame_margin_vertical_spacing:I = 0x7f0a022a

.field public static final card_pinall_image_height:I = 0x7f0a022b

.field public static final card_pinall_image_padding_left:I = 0x7f0a022c

.field public static final card_pinall_image_padding_right:I = 0x7f0a022d

.field public static final card_pinall_image_padding_top:I = 0x7f0a022e

.field public static final card_pinall_image_width:I = 0x7f0a022f

.field public static final card_pinall_item_shadow_img_height:I = 0x7f0a0230

.field public static final card_pinall_text_margin_left:I = 0x7f0a0231

.field public static final card_pinall_text_margin_right:I = 0x7f0a0232

.field public static final card_pinall_text_margin_top:I = 0x7f0a0536

.field public static final card_pinall_text_padding_bottom:I = 0x7f0a0233

.field public static final card_pinall_text_padding_left:I = 0x7f0a0234

.field public static final card_pinall_text_padding_right:I = 0x7f0a0235

.field public static final card_pinall_text_padding_top:I = 0x7f0a0236

.field public static final card_pinall_text_size:I = 0x7f0a0237

.field public static final card_pinall_title_height:I = 0x7f0a0238

.field public static final card_pinall_title_padding_left:I = 0x7f0a0239

.field public static final card_pinall_title_padding_right:I = 0x7f0a023a

.field public static final card_pinall_title_padding_top:I = 0x7f0a023b

.field public static final card_pinall_title_text_line_space:I = 0x7f0a023c

.field public static final card_pinall_title_text_size:I = 0x7f0a023d

.field public static final card_planner_body_big_height:I = 0x7f0a023e

.field public static final card_planner_body_height:I = 0x7f0a023f

.field public static final card_planner_body_margin_left:I = 0x7f0a0240

.field public static final card_planner_body_margin_right:I = 0x7f0a0241

.field public static final card_planner_body_small_height:I = 0x7f0a0242

.field public static final card_planner_checkbox_height:I = 0x7f0a0243

.field public static final card_planner_checkbox_margin_bottom:I = 0x7f0a0244

.field public static final card_planner_checkbox_margin_left:I = 0x7f0a0245

.field public static final card_planner_checkbox_margin_right:I = 0x7f0a0246

.field public static final card_planner_checkbox_margin_top:I = 0x7f0a0247

.field public static final card_planner_checkbox_width:I = 0x7f0a0248

.field public static final card_planner_color_bar_height:I = 0x7f0a0249

.field public static final card_planner_color_bar_width:I = 0x7f0a024a

.field public static final card_planner_event_margin_bottom:I = 0x7f0a024b

.field public static final card_planner_event_margin_top:I = 0x7f0a024c

.field public static final card_planner_icon_group_margin_left:I = 0x7f0a024d

.field public static final card_planner_icon_height:I = 0x7f0a024e

.field public static final card_planner_icon_margin_bottom:I = 0x7f0a024f

.field public static final card_planner_icon_margin_left:I = 0x7f0a0250

.field public static final card_planner_icon_margin_right:I = 0x7f0a0251

.field public static final card_planner_icon_margin_top:I = 0x7f0a0252

.field public static final card_planner_icon_width:I = 0x7f0a0253

.field public static final card_planner_ink_album_height:I = 0x7f0a0254

.field public static final card_planner_ink_album_horizontal_spacing:I = 0x7f0a0255

.field public static final card_planner_ink_album_image_margin_bottom:I = 0x7f0a0256

.field public static final card_planner_ink_album_image_margin_left:I = 0x7f0a0257

.field public static final card_planner_ink_album_margin_left:I = 0x7f0a0258

.field public static final card_planner_ink_album_margin_top:I = 0x7f0a0537

.field public static final card_planner_ink_album_padding_top:I = 0x7f0a0259

.field public static final card_planner_ink_album_vertical_spacing:I = 0x7f0a025a

.field public static final card_planner_ink_album_width:I = 0x7f0a025b

.field public static final card_planner_ink_body_height:I = 0x7f0a025c

.field public static final card_planner_ink_image_height:I = 0x7f0a025d

.field public static final card_planner_ink_image_width:I = 0x7f0a025e

.field public static final card_planner_ink_title_height:I = 0x7f0a025f

.field public static final card_planner_ink_title_margin_top:I = 0x7f0a0260

.field public static final card_planner_ink_title_text_size:I = 0x7f0a0261

.field public static final card_planner_item_frame_padding_left:I = 0x7f0a0262

.field public static final card_planner_item_frame_padding_right:I = 0x7f0a0263

.field public static final card_planner_other_item_gap:I = 0x7f0a0264

.field public static final card_planner_other_margin_bottom:I = 0x7f0a0265

.field public static final card_planner_other_margin_top:I = 0x7f0a0266

.field public static final card_planner_page_divider_height:I = 0x7f0a0267

.field public static final card_planner_page_margin_left:I = 0x7f0a0268

.field public static final card_planner_page_margin_right:I = 0x7f0a0269

.field public static final card_planner_small_color_bar_height:I = 0x7f0a026a

.field public static final card_planner_task_margin_bottom:I = 0x7f0a026b

.field public static final card_planner_task_margin_top:I = 0x7f0a026c

.field public static final card_planner_task_text_body_height:I = 0x7f0a026d

.field public static final card_planner_text_body_height:I = 0x7f0a026e

.field public static final card_planner_text_day_height:I = 0x7f0a026f

.field public static final card_planner_text_day_padding_left:I = 0x7f0a0270

.field public static final card_planner_text_day_text_size:I = 0x7f0a0271

.field public static final card_planner_text_gap:I = 0x7f0a0272

.field public static final card_planner_text_group_margin_top:I = 0x7f0a0273

.field public static final card_planner_text_height:I = 0x7f0a0274

.field public static final card_planner_text_layout_margin_top:I = 0x7f0a0275

.field public static final card_planner_text_margin_bottom:I = 0x7f0a0276

.field public static final card_planner_text_margin_left:I = 0x7f0a0277

.field public static final card_planner_text_margin_right:I = 0x7f0a0278

.field public static final card_planner_text_margin_top:I = 0x7f0a0279

.field public static final card_planner_text_time_margin_right:I = 0x7f0a027a

.field public static final card_planner_text_width:I = 0x7f0a027b

.field public static final card_planner_time_text_size:I = 0x7f0a027c

.field public static final card_planner_time_text_width:I = 0x7f0a027d

.field public static final card_planner_title_text_margin_right:I = 0x7f0a027e

.field public static final card_planner_title_text_padding_right:I = 0x7f0a0538

.field public static final card_planner_title_text_size:I = 0x7f0a027f

.field public static final card_quickmemo_frame_column_height:I = 0x7f0a0280

.field public static final card_quickmemo_frame_column_width:I = 0x7f0a0281

.field public static final card_quickmemo_frame_margin_horizontal_spacing:I = 0x7f0a0282

.field public static final card_quickmemo_frame_margin_left:I = 0x7f0a0283

.field public static final card_quickmemo_frame_margin_right:I = 0x7f0a0284

.field public static final card_quickmemo_frame_margin_vertical_spacing:I = 0x7f0a0285

.field public static final card_quickmemo_image_album_height:I = 0x7f0a0286

.field public static final card_quickmemo_image_album_padding:I = 0x7f0a0287

.field public static final card_quickmemo_image_album_shadow_height:I = 0x7f0a0288

.field public static final card_quickmemo_image_album_width:I = 0x7f0a0289

.field public static final card_samsung_link_frame_margin_vertical_spacing:I = 0x7f0a028a

.field public static final card_samsung_link_info_text_size:I = 0x7f0a028b

.field public static final card_samsung_link_item_divider_vertical_spacing:I = 0x7f0a028c

.field public static final card_samsung_link_item_group_margin_left:I = 0x7f0a028d

.field public static final card_samsung_link_item_info_margin_top:I = 0x7f0a028e

.field public static final card_samsung_link_item_main_margin_top:I = 0x7f0a028f

.field public static final card_samsung_link_item_view_full_height:I = 0x7f0a0290

.field public static final card_samsung_link_item_view_height:I = 0x7f0a0291

.field public static final card_samsung_link_item_view_padding_bottom:I = 0x7f0a0292

.field public static final card_samsung_link_item_view_padding_left:I = 0x7f0a0293

.field public static final card_samsung_link_item_view_padding_right:I = 0x7f0a0294

.field public static final card_samsung_link_item_view_padding_top:I = 0x7f0a0295

.field public static final card_samsung_link_layout_padding_left:I = 0x7f0a0296

.field public static final card_samsung_link_layout_padding_right:I = 0x7f0a0297

.field public static final card_samsung_link_name_icon_height:I = 0x7f0a0298

.field public static final card_samsung_link_name_icon_margin_right:I = 0x7f0a0299

.field public static final card_samsung_link_name_icon_padding_bottom:I = 0x7f0a029a

.field public static final card_samsung_link_name_icon_padding_left:I = 0x7f0a029b

.field public static final card_samsung_link_name_icon_padding_right:I = 0x7f0a029c

.field public static final card_samsung_link_name_icon_padding_top:I = 0x7f0a029d

.field public static final card_samsung_link_name_icon_width:I = 0x7f0a029e

.field public static final card_samsung_link_sort_target_textsize:I = 0x7f0a029f

.field public static final card_samsung_link_sort_title_layout_padding_top:I = 0x7f0a02a0

.field public static final card_samsung_link_title_text_size:I = 0x7f0a02a1

.field public static final card_search_web_help_text_height:I = 0x7f0a02a2

.field public static final card_settings_divider_page_icon_height:I = 0x7f0a02a3

.field public static final card_settings_frame_margin_vertical_spacing:I = 0x7f0a02a4

.field public static final card_settings_frame_min_height:I = 0x7f0a02a5

.field public static final card_settings_frame_padding_bottom:I = 0x7f0a02a6

.field public static final card_settings_frame_padding_left:I = 0x7f0a02a7

.field public static final card_settings_frame_padding_right:I = 0x7f0a02a8

.field public static final card_settings_frame_padding_top:I = 0x7f0a02a9

.field public static final card_settings_group_text_area_height:I = 0x7f0a02aa

.field public static final card_settings_group_text_area_margin_left:I = 0x7f0a02ab

.field public static final card_settings_group_text_area_margin_right:I = 0x7f0a02ac

.field public static final card_settings_group_text_area_padding_bottom:I = 0x7f0a02ad

.field public static final card_settings_group_text_area_padding_top:I = 0x7f0a02ae

.field public static final card_settings_group_text_size:I = 0x7f0a02af

.field public static final card_settings_menu_checkbox_height:I = 0x7f0a02b0

.field public static final card_settings_menu_checkbox_width:I = 0x7f0a02b1

.field public static final card_settings_menu_desc_margin_bottom:I = 0x7f0a02b2

.field public static final card_settings_menu_desc_margin_top:I = 0x7f0a02b3

.field public static final card_settings_menu_desc_text_size:I = 0x7f0a02b4

.field public static final card_settings_menu_icon_gruop_margin_left:I = 0x7f0a02b5

.field public static final card_settings_menu_icon_height:I = 0x7f0a02b6

.field public static final card_settings_menu_icon_margin_left:I = 0x7f0a02b7

.field public static final card_settings_menu_icon_margin_right:I = 0x7f0a02b8

.field public static final card_settings_menu_icon_margin_top:I = 0x7f0a02b9

.field public static final card_settings_menu_icon_width:I = 0x7f0a02ba

.field public static final card_settings_menu_link_height:I = 0x7f0a02bb

.field public static final card_settings_menu_link_width:I = 0x7f0a02bc

.field public static final card_settings_menu_name_margin_top:I = 0x7f0a02bd

.field public static final card_settings_menu_name_text_size:I = 0x7f0a02be

.field public static final card_settings_menu_switch_height:I = 0x7f0a02bf

.field public static final card_settings_menu_switch_width:I = 0x7f0a02c0

.field public static final card_settings_menu_text_1line_margin_bottom:I = 0x7f0a02c1

.field public static final card_settings_menu_text_1line_margin_top:I = 0x7f0a02c2

.field public static final card_settings_menu_text_area_margin_left:I = 0x7f0a02c3

.field public static final card_storyalbum_date_text_size:I = 0x7f0a02c4

.field public static final card_storyalbum_frame_column_height:I = 0x7f0a02c5

.field public static final card_storyalbum_frame_column_width:I = 0x7f0a02c6

.field public static final card_storyalbum_frame_margin_bottom:I = 0x7f0a02c7

.field public static final card_storyalbum_frame_margin_horizontal_spacing:I = 0x7f0a02c8

.field public static final card_storyalbum_frame_margin_left:I = 0x7f0a02c9

.field public static final card_storyalbum_frame_margin_right:I = 0x7f0a02ca

.field public static final card_storyalbum_frame_margin_top:I = 0x7f0a02cb

.field public static final card_storyalbum_frame_margin_vertical_spacing:I = 0x7f0a02cc

.field public static final card_storyalbum_image_album_height:I = 0x7f0a02cd

.field public static final card_storyalbum_image_album_margin_left:I = 0x7f0a02ce

.field public static final card_storyalbum_image_album_margin_top:I = 0x7f0a02cf

.field public static final card_storyalbum_image_album_overlay_height:I = 0x7f0a02d0

.field public static final card_storyalbum_image_album_overlay_margin_left:I = 0x7f0a02d1

.field public static final card_storyalbum_image_album_overlay_width:I = 0x7f0a02d2

.field public static final card_storyalbum_image_album_padding_bottom:I = 0x7f0a02d3

.field public static final card_storyalbum_image_album_padding_right:I = 0x7f0a02d4

.field public static final card_storyalbum_image_album_shodow_height:I = 0x7f0a02d5

.field public static final card_storyalbum_image_album_shodow_width:I = 0x7f0a02d6

.field public static final card_storyalbum_image_album_width:I = 0x7f0a02d7

.field public static final card_storyalbum_text_margin_left:I = 0x7f0a02d8

.field public static final card_storyalbum_text_margin_right:I = 0x7f0a02d9

.field public static final card_storyalbum_text_margin_top:I = 0x7f0a02da

.field public static final card_storyalbum_title_text_size:I = 0x7f0a02db

.field public static final card_video_contextual_date_margin_bottom:I = 0x7f0a02dc

.field public static final card_video_contextual_date_text_size:I = 0x7f0a02dd

.field public static final card_video_contextual_text1_margin_bottom:I = 0x7f0a02de

.field public static final card_video_contextual_text1_size:I = 0x7f0a02df

.field public static final card_video_contextual_text2_margin_bottom:I = 0x7f0a02e0

.field public static final card_video_contextual_text2_size:I = 0x7f0a02e1

.field public static final card_video_contextual_text_margin_left:I = 0x7f0a02e2

.field public static final card_video_date_text_size:I = 0x7f0a02e3

.field public static final card_video_frame_horizontal_spacing:I = 0x7f0a02e4

.field public static final card_video_frame_padding_bottom:I = 0x7f0a02e5

.field public static final card_video_frame_padding_left:I = 0x7f0a02e6

.field public static final card_video_frame_padding_right:I = 0x7f0a02e7

.field public static final card_video_frame_padding_top:I = 0x7f0a02e8

.field public static final card_video_frame_vertical_spacing:I = 0x7f0a02e9

.field public static final card_video_item_view_height:I = 0x7f0a02ea

.field public static final card_video_item_view_width:I = 0x7f0a02eb

.field public static final card_video_play_icon_height:I = 0x7f0a02ec

.field public static final card_video_play_icon_margin_left:I = 0x7f0a02ed

.field public static final card_video_play_icon_margin_top:I = 0x7f0a02ee

.field public static final card_video_play_icon_width:I = 0x7f0a02ef

.field public static final card_video_progressive_bar_height:I = 0x7f0a02f0

.field public static final card_video_progressive_bar_width:I = 0x7f0a02f1

.field public static final card_video_thumb_area_height:I = 0x7f0a02f2

.field public static final card_video_thumb_area_width:I = 0x7f0a02f3

.field public static final card_video_time_text_height:I = 0x7f0a02f4

.field public static final card_video_time_text_margin_top:I = 0x7f0a02f5

.field public static final card_video_time_text_size:I = 0x7f0a02f6

.field public static final card_video_time_text_width:I = 0x7f0a02f7

.field public static final card_video_title_text_height:I = 0x7f0a02f8

.field public static final card_video_title_text_margin_top:I = 0x7f0a02f9

.field public static final card_video_title_text_size:I = 0x7f0a02fa

.field public static final card_video_title_text_width:I = 0x7f0a02fb

.field public static final card_vnote_checkbox_margin_left:I = 0x7f0a02fc

.field public static final card_vnote_checkbox_margin_right:I = 0x7f0a02fd

.field public static final card_vnote_color_bar_icon_height:I = 0x7f0a02fe

.field public static final card_vnote_color_bar_icon_width:I = 0x7f0a02ff

.field public static final card_vnote_divider_page_icon_height:I = 0x7f0a0300

.field public static final card_vnote_frame_margin_vertical_spacing:I = 0x7f0a0301

.field public static final card_vnote_item_view_height:I = 0x7f0a0302

.field public static final card_vnote_item_view_padding_left:I = 0x7f0a0303

.field public static final card_vnote_item_view_padding_right:I = 0x7f0a0304

.field public static final card_vnote_memo_icon_height:I = 0x7f0a0305

.field public static final card_vnote_memo_icon_height_margin_top:I = 0x7f0a0306

.field public static final card_vnote_memo_icon_width:I = 0x7f0a0307

.field public static final card_vnote_name_text_margin_right:I = 0x7f0a0308

.field public static final card_vnote_name_text_margin_top:I = 0x7f0a0309

.field public static final card_vnote_name_text_size:I = 0x7f0a030a

.field public static final card_vnote_number_text_margin_top:I = 0x7f0a030b

.field public static final card_vnote_number_text_size:I = 0x7f0a030c

.field public static final card_vnote_text_layout_icon_padding_left:I = 0x7f0a030d

.field public static final card_vnote_text_layout_icon_padding_right:I = 0x7f0a030e

.field public static final card_web_link_cpname_margin_left:I = 0x7f0a030f

.field public static final card_web_link_cpname_size:I = 0x7f0a0310

.field public static final card_web_link_icon_height:I = 0x7f0a0311

.field public static final card_web_link_icon_width:I = 0x7f0a0312

.field public static final card_web_link_item_height:I = 0x7f0a0313

.field public static final card_web_link_item_padding_left:I = 0x7f0a0314

.field public static final card_web_link_item_padding_right:I = 0x7f0a0315

.field public static final card_webpreview_browser_height:I = 0x7f0a0316

.field public static final card_webpreview_browser_width:I = 0x7f0a0317

.field public static final card_webpreview_frame_margin_horizontal_spacing:I = 0x7f0a0318

.field public static final card_webpreview_frame_margin_left:I = 0x7f0a0319

.field public static final card_webpreview_frame_margin_right:I = 0x7f0a031a

.field public static final card_webpreview_frame_margin_vertical_spacing:I = 0x7f0a031b

.field public static final card_webpreview_icon_view_height:I = 0x7f0a0539

.field public static final card_webpreview_icon_view_margin_right:I = 0x7f0a031c

.field public static final card_webpreview_icon_view_margin_top:I = 0x7f0a031d

.field public static final card_webpreview_inner_stroke_width:I = 0x7f0a031e

.field public static final card_webpreview_item_focused_bg_width:I = 0x7f0a031f

.field public static final card_webpreview_item_view_height:I = 0x7f0a0511

.field public static final card_webpreview_item_view_width:I = 0x7f0a0512

.field public static final card_webpreview_name_text_size:I = 0x7f0a0320

.field public static final card_webpreview_title_height:I = 0x7f0a0321

.field public static final card_webpreview_web_column_height:I = 0x7f0a0322

.field public static final card_webpreview_web_column_width:I = 0x7f0a0323

.field public static final card_webpreview_web_thumbnail_height:I = 0x7f0a0324

.field public static final card_webpreview_web_view_height:I = 0x7f0a0325

.field public static final card_webpreview_web_view_padding_bottom:I = 0x7f0a0326

.field public static final card_webpreview_web_view_padding_left:I = 0x7f0a0327

.field public static final card_webpreview_web_view_padding_right:I = 0x7f0a0328

.field public static final card_webpreview_web_view_width:I = 0x7f0a0329

.field public static final category_expand_fully_height:I = 0x7f0a032a

.field public static final category_expand_fully_icon_height:I = 0x7f0a032b

.field public static final category_expand_fully_icon_left_margin:I = 0x7f0a032c

.field public static final category_expand_fully_icon_width:I = 0x7f0a032d

.field public static final category_expand_fully_margin_left:I = 0x7f0a032e

.field public static final category_expand_fully_margin_right:I = 0x7f0a032f

.field public static final category_expand_fully_text_margin_bottom:I = 0x7f0a0330

.field public static final category_expand_fully_text_margin_top:I = 0x7f0a0331

.field public static final category_expand_fully_text_padding_bottom:I = 0x7f0a0513

.field public static final category_expand_fully_text_padding_left:I = 0x7f0a0332

.field public static final category_expand_fully_text_padding_top:I = 0x7f0a0514

.field public static final category_expand_fully_text_size:I = 0x7f0a0333

.field public static final category_header_height:I = 0x7f0a0334

.field public static final category_list_checkbox_height:I = 0x7f0a0335

.field public static final category_list_checkbox_margin_left:I = 0x7f0a0336

.field public static final category_list_checkbox_margin_right:I = 0x7f0a0337

.field public static final category_list_checkbox_width:I = 0x7f0a0338

.field public static final category_list_divider_height:I = 0x7f0a0339

.field public static final category_list_icon_height:I = 0x7f0a033a

.field public static final category_list_icon_margin_left:I = 0x7f0a033b

.field public static final category_list_icon_margin_right:I = 0x7f0a033c

.field public static final category_list_icon_width:I = 0x7f0a033d

.field public static final category_list_item_height:I = 0x7f0a033e

.field public static final category_list_item_padding_left:I = 0x7f0a033f

.field public static final category_list_item_padding_right:I = 0x7f0a0340

.field public static final category_list_select_all_text_size:I = 0x7f0a053a

.field public static final category_list_selectall_item_height:I = 0x7f0a0341

.field public static final category_list_text_margin_right:I = 0x7f0a0342

.field public static final category_list_text_padding_left:I = 0x7f0a0343

.field public static final category_list_text_padding_right:I = 0x7f0a0344

.field public static final category_list_text_size:I = 0x7f0a0345

.field public static final category_option_arrow_height:I = 0x7f0a0346

.field public static final category_option_arrow_margin_right:I = 0x7f0a0347

.field public static final category_option_arrow_width:I = 0x7f0a0348

.field public static final category_option_bottom_image_height:I = 0x7f0a0349

.field public static final category_option_checkbox_height:I = 0x7f0a034a

.field public static final category_option_checkbox_margin_left:I = 0x7f0a034b

.field public static final category_option_checkbox_width:I = 0x7f0a034c

.field public static final category_option_child_padding_bottom:I = 0x7f0a034d

.field public static final category_option_child_padding_left:I = 0x7f0a034e

.field public static final category_option_child_padding_right:I = 0x7f0a034f

.field public static final category_option_child_padding_top:I = 0x7f0a0350

.field public static final category_option_device_item_width:I = 0x7f0a0351

.field public static final category_option_first_row_height:I = 0x7f0a0352

.field public static final category_option_last_child_padding_bottom:I = 0x7f0a0353

.field public static final category_option_last_child_padding_top:I = 0x7f0a0354

.field public static final category_option_last_row_height:I = 0x7f0a0355

.field public static final category_option_listview_padding_left:I = 0x7f0a0356

.field public static final category_option_listview_padding_right:I = 0x7f0a0357

.field public static final category_option_radio_height:I = 0x7f0a0358

.field public static final category_option_radio_margin_left:I = 0x7f0a0359

.field public static final category_option_radio_margin_top:I = 0x7f0a035a

.field public static final category_option_radio_width:I = 0x7f0a035b

.field public static final category_option_right_margin:I = 0x7f0a035c

.field public static final category_option_row_height:I = 0x7f0a035d

.field public static final category_option_row_padding_left:I = 0x7f0a035e

.field public static final category_option_text_margin_left:I = 0x7f0a035f

.field public static final category_option_text_margin_right:I = 0x7f0a0360

.field public static final category_option_time_item_margin_left:I = 0x7f0a0361

.field public static final category_option_time_item_text_margin_left:I = 0x7f0a0362

.field public static final category_option_time_item_text_margin_top:I = 0x7f0a0363

.field public static final category_option_time_line_height:I = 0x7f0a0364

.field public static final category_option_time_line_margin_left:I = 0x7f0a0365

.field public static final category_option_time_line_width:I = 0x7f0a0366

.field public static final category_option_time_lower_line_margin_top:I = 0x7f0a0367

.field public static final category_option_title_text_size:I = 0x7f0a0368

.field public static final category_option_top_image_height:I = 0x7f0a0369

.field public static final category_option_top_margin:I = 0x7f0a036a

.field public static final category_option_width:I = 0x7f0a036b

.field public static final category_order_dnd_view_height:I = 0x7f0a036c

.field public static final category_order_dnd_view_width:I = 0x7f0a036d

.field public static final checkbox_gallery_margin_left:I = 0x7f0a036e

.field public static final checkbox_gallery_margin_right:I = 0x7f0a053b

.field public static final checkbox_gallery_margin_top:I = 0x7f0a036f

.field public static final checkbox_music_margin_left:I = 0x7f0a0370

.field public static final checkbox_music_margin_right:I = 0x7f0a053c

.field public static final checkbox_music_margin_top:I = 0x7f0a0371

.field public static final checkbox_note_margin_left:I = 0x7f0a0372

.field public static final checkbox_note_margin_right:I = 0x7f0a053d

.field public static final checkbox_note_margin_top:I = 0x7f0a0373

.field public static final checkbox_pinall_margin_left:I = 0x7f0a0374

.field public static final checkbox_pinall_margin_right:I = 0x7f0a053e

.field public static final checkbox_pinall_margin_top:I = 0x7f0a0375

.field public static final checkbox_quickmemo_margin_left:I = 0x7f0a0376

.field public static final checkbox_quickmemo_margin_right:I = 0x7f0a053f

.field public static final checkbox_quickmemo_margin_top:I = 0x7f0a0377

.field public static final crop_shape_shadow_offset_y:I = 0x7f0a0378

.field public static final crop_shape_shadow_size:I = 0x7f0a0379

.field public static final custom_action_bar_done_margin_end:I = 0x7f0a037a

.field public static final custom_action_bar_text_margin_right:I = 0x7f0a037b

.field public static final custom_action_bar_text_size:I = 0x7f0a037c

.field public static final custom_category_list_text_padding_right:I = 0x7f0a037d

.field public static final custom_search_autocompletelist_margin_top:I = 0x7f0a037e

.field public static final custom_search_edittext_tag_padding:I = 0x7f0a037f

.field public static final custom_search_edittext_tag_textsize:I = 0x7f0a0380

.field public static final custom_search_group_height:I = 0x7f0a0381

.field public static final custom_search_group_padding_bottom:I = 0x7f0a0382

.field public static final custom_search_group_padding_left:I = 0x7f0a0383

.field public static final custom_search_group_padding_right:I = 0x7f0a0384

.field public static final custom_search_group_padding_top:I = 0x7f0a0385

.field public static final custom_search_group_width:I = 0x7f0a0386

.field public static final custom_search_view_edit_bg_width:I = 0x7f0a0387

.field public static final custom_search_view_edit_height:I = 0x7f0a0388

.field public static final custom_search_view_edit_margin_right:I = 0x7f0a0540

.field public static final custom_search_view_edit_margin_top:I = 0x7f0a0515

.field public static final custom_search_view_edit_padding_left:I = 0x7f0a0389

.field public static final custom_search_view_edit_padding_right:I = 0x7f0a038a

.field public static final custom_search_view_edit_text_size:I = 0x7f0a038b

.field public static final custom_search_view_edit_width:I = 0x7f0a038c

.field public static final custom_search_view_hint_text_height:I = 0x7f0a038d

.field public static final custom_search_view_hint_text_margin_top:I = 0x7f0a038e

.field public static final custom_search_view_hint_text_padding_top:I = 0x7f0a0541

.field public static final custom_search_view_hint_text_size:I = 0x7f0a038f

.field public static final custom_search_view_icon_cancel_height:I = 0x7f0a0390

.field public static final custom_search_view_icon_cancel_margin_right:I = 0x7f0a0391

.field public static final custom_search_view_icon_cancel_margin_top:I = 0x7f0a0392

.field public static final custom_search_view_icon_cancel_padding:I = 0x7f0a0393

.field public static final custom_search_view_icon_cancel_width:I = 0x7f0a0394

.field public static final custom_search_view_icon_divider_height:I = 0x7f0a0395

.field public static final custom_search_view_icon_divider_margin_right:I = 0x7f0a0396

.field public static final custom_search_view_icon_divider_width:I = 0x7f0a0397

.field public static final custom_search_view_icon_done_height:I = 0x7f0a0398

.field public static final custom_search_view_icon_done_margin_left:I = 0x7f0a0399

.field public static final custom_search_view_icon_done_margin_right:I = 0x7f0a039a

.field public static final custom_search_view_icon_done_width:I = 0x7f0a039b

.field public static final custom_search_view_icon_margin_right:I = 0x7f0a039c

.field public static final custom_search_view_icon_option_height:I = 0x7f0a039d

.field public static final custom_search_view_icon_option_width:I = 0x7f0a039e

.field public static final custom_search_view_icon_pin_height:I = 0x7f0a039f

.field public static final custom_search_view_icon_pin_width:I = 0x7f0a03a0

.field public static final custom_search_view_icon_settings_height:I = 0x7f0a03a1

.field public static final custom_search_view_icon_settings_padding_top:I = 0x7f0a03a2

.field public static final custom_search_view_icon_settings_width:I = 0x7f0a03a3

.field public static final custom_search_view_icon_voice_height:I = 0x7f0a03a4

.field public static final custom_search_view_icon_voice_margin_right:I = 0x7f0a03a5

.field public static final custom_search_view_icon_voice_margin_top:I = 0x7f0a03a6

.field public static final custom_search_view_icon_voice_padding:I = 0x7f0a03a7

.field public static final custom_search_view_icon_voice_width:I = 0x7f0a03a8

.field public static final custom_search_view_option_button_padding_left:I = 0x7f0a03a9

.field public static final custom_search_view_option_button_padding_right:I = 0x7f0a03aa

.field public static final custom_search_view_option_button_width:I = 0x7f0a03ab

.field public static final custom_search_view_option_margin_right:I = 0x7f0a03ac

.field public static final custom_search_view_search_progress_height:I = 0x7f0a03ad

.field public static final custom_search_view_search_progress_margin_right:I = 0x7f0a03ae

.field public static final custom_search_view_search_progress_margin_top:I = 0x7f0a03af

.field public static final custom_search_view_search_progress_width:I = 0x7f0a03b0

.field public static final custom_search_view_suggest_popup_margin_top:I = 0x7f0a03b1

.field public static final custom_search_view_suggest_popup_padding:I = 0x7f0a03b2

.field public static final custom_search_view_text_padding_top:I = 0x7f0a0542

.field public static final default_hint_margin_left:I = 0x7f0a03b3

.field public static final default_hint_margin_right:I = 0x7f0a03b4

.field public static final default_hint_margin_top:I = 0x7f0a03b5

.field public static final default_hint_margin_top_no_keypad:I = 0x7f0a03b6

.field public static final default_hint_size:I = 0x7f0a03b7

.field public static final divider_browser_inset_margin_start:I = 0x7f0a03b8

.field public static final divider_contact_inset_margin_start:I = 0x7f0a03b9

.field public static final divider_height:I = 0x7f0a03ba

.field public static final divider_inset_margin:I = 0x7f0a03bb

.field public static final divider_music_inset_margin_start:I = 0x7f0a03bc

.field public static final divider_myfiles_inset_margin_start:I = 0x7f0a03bd

.field public static final divider_phone_inset_margin_start:I = 0x7f0a03be

.field public static final divider_samsunglink_inset_margin_start:I = 0x7f0a03bf

.field public static final divider_width:I = 0x7f0a03c0

.field public static final dnd_list_padding_left:I = 0x7f0a03c1

.field public static final dnd_list_padding_right:I = 0x7f0a03c2

.field public static final export_dialog_item_padding_end:I = 0x7f0a03c3

.field public static final export_dialog_item_padding_start:I = 0x7f0a03c4

.field public static final filter_panel_button_group_height:I = 0x7f0a03c5

.field public static final filter_panel_button_group_icon_height:I = 0x7f0a03c6

.field public static final filter_panel_button_group_icon_width:I = 0x7f0a03c7

.field public static final filter_panel_button_group_margin_bottom:I = 0x7f0a03c8

.field public static final filter_panel_button_group_padding_bottom:I = 0x7f0a03c9

.field public static final filter_panel_button_group_padding_left:I = 0x7f0a03ca

.field public static final filter_panel_button_group_padding_right:I = 0x7f0a03cb

.field public static final filter_panel_detail_bg_height:I = 0x7f0a03cc

.field public static final filter_panel_detail_bg_margin_bottom:I = 0x7f0a03cd

.field public static final filter_panel_detail_bg_margin_top:I = 0x7f0a03ce

.field public static final filter_panel_detail_horizontal_spacing:I = 0x7f0a03cf

.field public static final filter_panel_detail_item_height:I = 0x7f0a03d0

.field public static final filter_panel_detail_item_text_size:I = 0x7f0a03d1

.field public static final filter_panel_detail_min_height:I = 0x7f0a03d2

.field public static final filter_panel_detail_padding_bottom:I = 0x7f0a03d3

.field public static final filter_panel_detail_padding_left:I = 0x7f0a03d4

.field public static final filter_panel_detail_padding_right:I = 0x7f0a03d5

.field public static final filter_panel_detail_padding_top:I = 0x7f0a03d6

.field public static final filter_panel_detail_progressbar_height:I = 0x7f0a03d7

.field public static final filter_panel_detail_progressbar_width:I = 0x7f0a03d8

.field public static final filter_panel_detail_rectangle_height:I = 0x7f0a03d9

.field public static final filter_panel_detail_rectangle_margin_top:I = 0x7f0a03da

.field public static final filter_panel_detail_vertical_spacing:I = 0x7f0a03db

.field public static final filter_panel_horizontal_scroll_height:I = 0x7f0a03dc

.field public static final filter_panel_horizontal_scroll_inner_margin:I = 0x7f0a03dd

.field public static final filter_panel_horizontal_scroll_padding_bottom:I = 0x7f0a03de

.field public static final filter_panel_horizontal_scroll_padding_top:I = 0x7f0a03df

.field public static final filter_panel_main_padding_bottom:I = 0x7f0a03e0

.field public static final filter_panel_main_padding_left:I = 0x7f0a03e1

.field public static final filter_panel_main_padding_right:I = 0x7f0a03e2

.field public static final filter_panel_main_padding_top:I = 0x7f0a03e3

.field public static final filter_panel_selected_item_height:I = 0x7f0a03e4

.field public static final filter_panel_selected_item_horizontal_spacing:I = 0x7f0a03e5

.field public static final filter_panel_selected_item_padding_left:I = 0x7f0a03e6

.field public static final filter_panel_selected_item_padding_right:I = 0x7f0a03e7

.field public static final filter_panel_selected_item_remove_button_height:I = 0x7f0a03e8

.field public static final filter_panel_selected_item_remove_button_margin_right:I = 0x7f0a03e9

.field public static final filter_panel_selected_item_remove_button_width:I = 0x7f0a03ea

.field public static final filter_panel_selected_item_text_size:I = 0x7f0a03eb

.field public static final gallery_index_land_pivot_value:I = 0x7f0a03ec

.field public static final gallery_index_potrait_pivot_value:I = 0x7f0a03ed

.field public static final guide_bubble_button_help_between_margin:I = 0x7f0a03ee

.field public static final guide_bubble_button_help_height:I = 0x7f0a03ef

.field public static final guide_bubble_button_help_margin_bottom:I = 0x7f0a03f0

.field public static final guide_bubble_button_help_margin_left:I = 0x7f0a03f1

.field public static final guide_bubble_button_help_padding_bottom:I = 0x7f0a03f2

.field public static final guide_bubble_button_help_text_size:I = 0x7f0a03f3

.field public static final guide_bubble_button_help_width:I = 0x7f0a03f4

.field public static final guide_bubble_button_ok_margin_right:I = 0x7f0a03f5

.field public static final guide_bubble_button_only_ok_margin_left:I = 0x7f0a03f6

.field public static final guide_bubble_button_setting_margin_bottom:I = 0x7f0a03f7

.field public static final guide_bubble_button_setting_width:I = 0x7f0a03f8

.field public static final guide_bubble_icon_layout_horizontal_spacing:I = 0x7f0a03f9

.field public static final guide_bubble_icon_layout_text_height:I = 0x7f0a03fa

.field public static final guide_bubble_icon_layout_text_size:I = 0x7f0a03fb

.field public static final guide_bubble_icon_layout_vertical_spacing:I = 0x7f0a03fc

.field public static final guide_bubble_icon_layout_width:I = 0x7f0a03fd

.field public static final guide_bubble_icons_layout_height:I = 0x7f0a03fe

.field public static final guide_bubble_icons_layout_margin_bottom:I = 0x7f0a03ff

.field public static final guide_bubble_icons_layout_margin_top:I = 0x7f0a0400

.field public static final guide_bubble_summary_layout_height:I = 0x7f0a0401

.field public static final guide_bubble_summary_layout_padding_bottom:I = 0x7f0a0402

.field public static final guide_bubble_summary_layout_width:I = 0x7f0a0403

.field public static final guide_bubble_summary_margin_top:I = 0x7f0a0404

.field public static final guide_do_not_show_again_margin_bottom:I = 0x7f0a0405

.field public static final guide_do_not_show_again_margin_left:I = 0x7f0a0406

.field public static final guide_do_not_show_again_margin_right:I = 0x7f0a0407

.field public static final guide_do_not_show_again_margin_top:I = 0x7f0a0408

.field public static final guide_do_not_show_again_padding_left:I = 0x7f0a0409

.field public static final guide_hand_pointer_layout_height:I = 0x7f0a040a

.field public static final guide_hand_pointer_layout_width:I = 0x7f0a040b

.field public static final guide_hand_pointer_margin_top:I = 0x7f0a040c

.field public static final guide_help_icon_height:I = 0x7f0a040d

.field public static final guide_help_icon_margin_top:I = 0x7f0a0543

.field public static final guide_help_icon_margin_top_in_multiwindow:I = 0x7f0a040e

.field public static final guide_help_icon_width:I = 0x7f0a040f

.field public static final guide_help_layout_height:I = 0x7f0a0410

.field public static final guide_help_layout_width:I = 0x7f0a0411

.field public static final guide_help_popup_background_width:I = 0x7f0a0412

.field public static final guide_help_popup_background_y_position:I = 0x7f0a0413

.field public static final guide_text_box_bottom_padding_with_tail:I = 0x7f0a0414

.field public static final guide_text_box_left_padding_with_tail:I = 0x7f0a0415

.field public static final guide_text_box_max_width_with_tail:I = 0x7f0a0416

.field public static final guide_text_box_right_padding_with_tail:I = 0x7f0a0417

.field public static final guide_text_box_text_size:I = 0x7f0a0418

.field public static final guide_text_box_top_padding_with_tail:I = 0x7f0a0419

.field public static final guide_text_height:I = 0x7f0a041a

.field public static final guide_text_line_height:I = 0x7f0a041b

.field public static final guide_text_line_spacing:I = 0x7f0a041c

.field public static final guide_text_margin_top:I = 0x7f0a041d

.field public static final guide_text_size:I = 0x7f0a041e

.field public static final guide_text_width_gap:I = 0x7f0a041f

.field public static final guide_try_it_ltr_margin_left:I = 0x7f0a0420

.field public static final guide_try_it_rtl_margin_left:I = 0x7f0a0421

.field public static final help_popup_try_it_scrollview_height:I = 0x7f0a0422

.field public static final help_popup_try_it_scrollview_margin_left:I = 0x7f0a0423

.field public static final help_popup_try_it_scrollview_margin_right:I = 0x7f0a0424

.field public static final help_popup_try_it_scrollview_margin_top:I = 0x7f0a0425

.field public static final help_text_padding_bottom:I = 0x7f0a0426

.field public static final help_text_width:I = 0x7f0a0427

.field public static final helplayout_top_padding_with_no_filter:I = 0x7f0a0428

.field public static final history_item_list_date_text_margin_right:I = 0x7f0a0429

.field public static final history_item_list_date_text_size:I = 0x7f0a042a

.field public static final history_item_list_extra_paddings:I = 0x7f0a042b

.field public static final history_item_list_fullscreen_date_text_margin_right:I = 0x7f0a042c

.field public static final history_item_list_fullscreen_date_text_size:I = 0x7f0a042d

.field public static final history_item_list_fullscreen_delete_all_height:I = 0x7f0a042e

.field public static final history_item_list_fullscreen_delete_all_text_size:I = 0x7f0a042f

.field public static final history_item_list_fullscreen_delete_icon_margin_left:I = 0x7f0a0430

.field public static final history_item_list_fullscreen_delete_icon_size:I = 0x7f0a0431

.field public static final history_item_list_fullscreen_divider_inset:I = 0x7f0a0432

.field public static final history_item_list_fullscreen_keyword_text_margin_left:I = 0x7f0a0433

.field public static final history_item_list_fullscreen_keyword_text_padding_right:I = 0x7f0a0434

.field public static final history_item_list_fullscreen_keyword_text_size:I = 0x7f0a0435

.field public static final history_item_list_fullscreen_list_margin:I = 0x7f0a0436

.field public static final history_item_list_fullscreen_text_layout_height:I = 0x7f0a0437

.field public static final history_item_list_keyword_text_margin_left:I = 0x7f0a0438

.field public static final history_item_list_keyword_text_size:I = 0x7f0a0439

.field public static final history_item_list_text_layout_height:I = 0x7f0a043a

.field public static final history_list_background_img_padding:I = 0x7f0a043b

.field public static final history_list_padding_top:I = 0x7f0a043c

.field public static final hover_content_list_imageview_height:I = 0x7f0a043d

.field public static final hover_content_list_imageview_margin_left:I = 0x7f0a043e

.field public static final hover_content_list_imageview_width:I = 0x7f0a043f

.field public static final intro_layout_item_group_height:I = 0x7f0a0440

.field public static final intro_layout_item_group_width:I = 0x7f0a0441

.field public static final intro_layout_item_guide_margin_top:I = 0x7f0a0442

.field public static final intro_layout_item_guide_padding_left:I = 0x7f0a0443

.field public static final intro_layout_item_guide_padding_right:I = 0x7f0a0444

.field public static final intro_layout_item_guide_size:I = 0x7f0a0445

.field public static final intro_layout_item_icon_height:I = 0x7f0a0446

.field public static final intro_layout_item_icon_margin_bottom:I = 0x7f0a0447

.field public static final intro_layout_item_icon_width:I = 0x7f0a0448

.field public static final intro_layout_item_name_height:I = 0x7f0a0449

.field public static final intro_layout_item_name_size:I = 0x7f0a044a

.field public static final intro_layout_margin_top:I = 0x7f0a044b

.field public static final multi_line_checkbox_margin_left:I = 0x7f0a044c

.field public static final multi_line_primary_text_size:I = 0x7f0a044d

.field public static final popup_button_height:I = 0x7f0a044e

.field public static final popup_dialog_text_margin_left:I = 0x7f0a044f

.field public static final related_tag_item_layout_height:I = 0x7f0a0450

.field public static final related_tag_item_margin_right:I = 0x7f0a0451

.field public static final search_bar_bottom:I = 0x7f0a0452

.field public static final search_guide_text:I = 0x7f0a0453

.field public static final search_guide_text_margin_top:I = 0x7f0a0454

.field public static final search_view_height:I = 0x7f0a0455

.field public static final search_view_left_margin:I = 0x7f0a0456

.field public static final search_view_right_margin:I = 0x7f0a0457

.field public static final searching_progress_layout_height:I = 0x7f0a0458

.field public static final searching_progress_layout_padding:I = 0x7f0a0459

.field public static final searching_progress_text_size:I = 0x7f0a045a

.field public static final searchlist_category_padding_left:I = 0x7f0a045b

.field public static final searchlist_category_padding_right:I = 0x7f0a045c

.field public static final searchlist_category_padding_top:I = 0x7f0a045d

.field public static final searchlist_category_top_margin:I = 0x7f0a045e

.field public static final searchlist_width_with_no_filter:I = 0x7f0a045f

.field public static final seperated_ui_left_pane_width:I = 0x7f0a0460

.field public static final settings_delete_history_list_divider_height:I = 0x7f0a0461

.field public static final settings_delete_history_list_item_height:I = 0x7f0a0462

.field public static final settings_delete_history_list_item_min_height:I = 0x7f0a0463

.field public static final settings_delete_history_list_item_padding_left:I = 0x7f0a0464

.field public static final settings_delete_history_list_item_padding_right:I = 0x7f0a0465

.field public static final settings_delete_history_list_item_text_date_maring_right:I = 0x7f0a0466

.field public static final settings_delete_history_list_item_text_date_size:I = 0x7f0a0467

.field public static final settings_delete_history_list_item_text_name_padding_left:I = 0x7f0a0468

.field public static final settings_delete_history_list_item_text_name_padding_right:I = 0x7f0a0469

.field public static final settings_delete_history_list_item_text_name_size:I = 0x7f0a046a

.field public static final settings_grouped_list_padding_left:I = 0x7f0a046b

.field public static final settings_grouped_list_padding_right:I = 0x7f0a046c

.field public static final settings_grouped_list_padding_top:I = 0x7f0a046d

.field public static final settings_list_checkbox_margin_right:I = 0x7f0a046e

.field public static final settings_list_dnd_style_padding_left:I = 0x7f0a046f

.field public static final settings_list_item_checkbox_margin_right:I = 0x7f0a0544

.field public static final settings_list_item_height:I = 0x7f0a0470

.field public static final settings_list_item_min_height:I = 0x7f0a0471

.field public static final settings_list_padding_left:I = 0x7f0a0472

.field public static final settings_list_padding_right:I = 0x7f0a0473

.field public static final settings_list_selectall_item_height:I = 0x7f0a0474

.field public static final settings_list_selectall_text_size:I = 0x7f0a0475

.field public static final settings_select_filter_item_height:I = 0x7f0a0476

.field public static final settings_select_filter_item_icon_width:I = 0x7f0a0477

.field public static final settings_select_filter_item_label_margin_left:I = 0x7f0a0478

.field public static final settings_select_filter_item_label_margin_right:I = 0x7f0a0479

.field public static final settings_select_filter_item_label_textsize:I = 0x7f0a047a

.field public static final settings_select_filter_item_padding_left:I = 0x7f0a047b

.field public static final settings_select_filter_item_padding_right:I = 0x7f0a047c

.field public static final settings_select_filter_list_divider_height:I = 0x7f0a047d

.field public static final share_icon_width:I = 0x7f0a047e

.field public static final share_popup_blank_height:I = 0x7f0a047f

.field public static final share_popup_item_height:I = 0x7f0a0480

.field public static final share_popup_item_padding:I = 0x7f0a0481

.field public static final share_popup_item_width:I = 0x7f0a0482

.field public static final share_popup_max_height:I = 0x7f0a0483

.field public static final share_popup_vertical_offset:I = 0x7f0a0484

.field public static final share_text_left_padding:I = 0x7f0a0485

.field public static final share_via_text_size:I = 0x7f0a0486

.field public static final start_guide_text_box_bottom_padding:I = 0x7f0a0487

.field public static final start_guide_text_box_left_padding:I = 0x7f0a0488

.field public static final start_guide_text_box_right_padding:I = 0x7f0a0489

.field public static final start_guide_text_box_top_padding:I = 0x7f0a048a

.field public static final suggestion_divider_height:I = 0x7f0a048b

.field public static final suggestion_divider_margin_top:I = 0x7f0a048c

.field public static final suggestion_divider_width:I = 0x7f0a048d

.field public static final suggestion_icon_height:I = 0x7f0a048e

.field public static final suggestion_icon_width_in_three_items:I = 0x7f0a048f

.field public static final suggestion_icon_width_in_three_items_center:I = 0x7f0a0490

.field public static final suggestion_icon_width_in_three_items_center_icon_padding:I = 0x7f0a0491

.field public static final suggestion_icon_width_in_two_items:I = 0x7f0a0492

.field public static final suggestion_item_layout_height:I = 0x7f0a0493

.field public static final suggestion_item_layout_margin_top:I = 0x7f0a0494

.field public static final suggestion_item_layout_width:I = 0x7f0a0495

.field public static final suggestion_layout_center_bg_width:I = 0x7f0a0496

.field public static final suggestion_layout_height:I = 0x7f0a0497

.field public static final suggestion_layout_left_bg_width:I = 0x7f0a0498

.field public static final suggestion_layout_margin_left:I = 0x7f0a0499

.field public static final suggestion_layout_margin_top:I = 0x7f0a049a

.field public static final suggestion_layout_right_bg_width:I = 0x7f0a049b

.field public static final suggestion_layout_view_margin_top:I = 0x7f0a049c

.field public static final suggestion_layout_width:I = 0x7f0a049d

.field public static final suggestion_three_item_divider1_margin_left:I = 0x7f0a049e

.field public static final suggestion_three_item_divider2_margin_left:I = 0x7f0a049f

.field public static final suggestion_three_item_icon1_margin_left:I = 0x7f0a04a0

.field public static final suggestion_three_item_icon2_margin_left:I = 0x7f0a04a1

.field public static final suggestion_three_item_icon3_margin_left:I = 0x7f0a04a2

.field public static final suggestion_two_item_divider_margin_left:I = 0x7f0a04a3

.field public static final suggestion_two_item_icon1_margin_left:I = 0x7f0a04a4

.field public static final suggestion_two_item_icon2_margin_left:I = 0x7f0a04a5

.field public static final symbol_divider_margin_top:I = 0x7f0a04a6

.field public static final symbol_help_gap:I = 0x7f0a04a7

.field public static final symbol_help_height:I = 0x7f0a04a8

.field public static final symbol_help_image_height:I = 0x7f0a04a9

.field public static final symbol_help_image_margin_bottom:I = 0x7f0a04aa

.field public static final symbol_help_image_padding:I = 0x7f0a04ab

.field public static final symbol_help_image_width:I = 0x7f0a04ac

.field public static final symbol_help_string_height:I = 0x7f0a04ad

.field public static final symbol_help_string_line_height:I = 0x7f0a04ae

.field public static final symbol_help_string_line_spacing:I = 0x7f0a04af

.field public static final symbol_help_string_margin_bottom:I = 0x7f0a04b0

.field public static final symbol_help_string_padding:I = 0x7f0a04b1

.field public static final symbol_help_string_padding_top:I = 0x7f0a04b2

.field public static final symbol_help_string_size:I = 0x7f0a04b3

.field public static final symbol_help_string_width:I = 0x7f0a04b4

.field public static final symbol_help_width:I = 0x7f0a04b5

.field public static final tag_cloud_vertical_style_height_multi_window_mode:I = 0x7f0a04b6

.field public static final tag_expand_button_margin_right:I = 0x7f0a04b7

.field public static final tag_expandable_child_padding_left:I = 0x7f0a0545

.field public static final tag_expandable_child_padding_right:I = 0x7f0a0546

.field public static final tag_expandable_group_icon_height:I = 0x7f0a0547

.field public static final tag_expandable_group_icon_margin_left:I = 0x7f0a0548

.field public static final tag_expandable_group_icon_margin_right:I = 0x7f0a0549

.field public static final tag_expandable_group_icon_width:I = 0x7f0a054a

.field public static final tag_expandable_group_margin_left:I = 0x7f0a054b

.field public static final tag_expandable_group_margin_right:I = 0x7f0a054c

.field public static final tag_expandable_group_margin_top:I = 0x7f0a054d

.field public static final tag_expandable_group_title_size:I = 0x7f0a054e

.field public static final tag_expandable_tag_title_size:I = 0x7f0a04b8

.field public static final tag_expandalbe_group_height:I = 0x7f0a054f

.field public static final tag_list_divider_height:I = 0x7f0a04b9

.field public static final tag_list_divider_width:I = 0x7f0a04ba

.field public static final tag_list_icon_height:I = 0x7f0a04bb

.field public static final tag_list_icon_left_margin:I = 0x7f0a04bc

.field public static final tag_list_icon_right_margin:I = 0x7f0a04bd

.field public static final tag_list_icon_width:I = 0x7f0a04be

.field public static final tag_list_item_padding_left:I = 0x7f0a04bf

.field public static final tag_list_item_padding_right:I = 0x7f0a04c0

.field public static final tag_list_item_textsize:I = 0x7f0a04c1

.field public static final tag_list_view_height:I = 0x7f0a04c2

.field public static final tag_list_view_padding_right:I = 0x7f0a04c3

.field public static final tag_list_view_width:I = 0x7f0a04c4

.field public static final tag_scrollview_horizontal_content_margin_bottom:I = 0x7f0a04c5

.field public static final tag_scrollview_horizontal_content_margin_top:I = 0x7f0a04c6

.field public static final tag_scrollview_horizontal_icon_margin_bottom:I = 0x7f0a04c7

.field public static final tag_scrollview_horizontal_icon_margin_right:I = 0x7f0a04c8

.field public static final tag_scrollview_horizontal_icon_margin_top:I = 0x7f0a04c9

.field public static final tag_scrollview_horizontal_margin_left:I = 0x7f0a04ca

.field public static final tag_scrollview_horizontal_margin_right:I = 0x7f0a04cb

.field public static final tag_scrollview_vertical_content_margin_bottom:I = 0x7f0a0550

.field public static final tag_scrollview_vertical_content_margin_left:I = 0x7f0a0551

.field public static final tag_scrollview_vertical_content_margin_right:I = 0x7f0a0552

.field public static final tag_scrollview_vertical_title_height:I = 0x7f0a0553

.field public static final tag_scrollview_vertical_title_icon_margin_left:I = 0x7f0a0554

.field public static final tag_scrollview_vertical_title_icon_margin_right:I = 0x7f0a0555

.field public static final tag_scrollview_vertical_title_margin_bottom:I = 0x7f0a0556

.field public static final tag_scrollview_vertical_title_margin_left:I = 0x7f0a0557

.field public static final tag_scrollview_vertical_title_margin_right:I = 0x7f0a0558

.field public static final tag_scrollview_vertical_title_text_margin_left:I = 0x7f0a0559

.field public static final tag_scrollview_vertical_title_text_size:I = 0x7f0a055a

.field public static final tagcloud_item_height:I = 0x7f0a04cc

.field public static final tagcloud_item_horizontal_spacing:I = 0x7f0a04cd

.field public static final tagcloud_item_margin_bottom:I = 0x7f0a04ce

.field public static final tagcloud_item_margin_right:I = 0x7f0a055b

.field public static final tagcloud_item_margin_top:I = 0x7f0a04cf

.field public static final tagcloud_item_padding_side:I = 0x7f0a04d0

.field public static final tagcloud_list_group_padding_right:I = 0x7f0a04d1

.field public static final tagcloud_list_margin_height:I = 0x7f0a04d2

.field public static final tagcloud_list_margin_left:I = 0x7f0a04d3

.field public static final tagcloud_list_margin_right:I = 0x7f0a04d4

.field public static final tagcloud_list_padding_left:I = 0x7f0a04d5

.field public static final tagcloud_list_padding_right:I = 0x7f0a04d6

.field public static final tagcloud_list_padding_top:I = 0x7f0a04d7

.field public static final tagcloud_list_row_height:I = 0x7f0a04d8

.field public static final tagcloud_scroll_margin_top:I = 0x7f0a055c

.field public static final tagcloud_user_icon_margin_right:I = 0x7f0a04d9

.field public static final tagcloud_user_icon_padding_top:I = 0x7f0a04da

.field public static final tagcloud_user_margin_top:I = 0x7f0a04db

.field public static final tagcloud_user_vertical_scroll_max_height:I = 0x7f0a04dc

.field public static final try_it_guide_arrow_extra_left:I = 0x7f0a04dd

.field public static final try_it_guide_five_bubble_text_margin_bottom:I = 0x7f0a04de

.field public static final try_it_guide_four_bubble_text_margin_bottom:I = 0x7f0a04df

.field public static final try_it_guide_four_bubble_text_width:I = 0x7f0a04e0

.field public static final try_it_guide_four_layout_margin_bottom:I = 0x7f0a04e1

.field public static final try_it_guide_four_layout_margin_left:I = 0x7f0a04e2

.field public static final try_it_guide_four_layout_margin_top:I = 0x7f0a04e3

.field public static final try_it_guide_four_popup_picker_margin_left:I = 0x7f0a04e4

.field public static final try_it_guide_four_popup_picker_margin_top:I = 0x7f0a04e5

.field public static final try_it_guide_one_bubble_layout_margin_top:I = 0x7f0a04e6

.field public static final try_it_guide_one_bubble_text_margin_top:I = 0x7f0a04e7

.field public static final try_it_guide_one_drag_frame_margin_top:I = 0x7f0a04e8

.field public static final try_it_guide_one_drag_image_margin_left_03:I = 0x7f0a04e9

.field public static final try_it_guide_one_drag_image_margin_left_04:I = 0x7f0a04ea

.field public static final try_it_guide_one_drag_image_margin_right_01:I = 0x7f0a04eb

.field public static final try_it_guide_one_drag_image_margin_right_02:I = 0x7f0a04ec

.field public static final try_it_guide_one_drag_image_width:I = 0x7f0a04ed

.field public static final try_it_guide_one_help_drag_location:I = 0x7f0a04ee

.field public static final try_it_guide_one_punch_view_height:I = 0x7f0a04ef

.field public static final try_it_guide_one_punch_view_margin_top:I = 0x7f0a04f0

.field public static final try_it_guide_pointer_extra_left:I = 0x7f0a04f1

.field public static final try_it_guide_tag_cloud_left_margin:I = 0x7f0a04f2

.field public static final try_it_guide_text_box_bottom_padding:I = 0x7f0a04f3

.field public static final try_it_guide_text_box_left_padding:I = 0x7f0a04f4

.field public static final try_it_guide_text_box_max_width:I = 0x7f0a04f5

.field public static final try_it_guide_text_box_right_padding:I = 0x7f0a04f6

.field public static final try_it_guide_text_box_top_padding:I = 0x7f0a04f7

.field public static final try_it_guide_text_extra_left:I = 0x7f0a04f8

.field public static final try_it_guide_text_size:I = 0x7f0a04f9

.field public static final try_it_guide_two_bubble_image_margin_right:I = 0x7f0a04fa

.field public static final try_it_guide_two_bubble_image_margin_top:I = 0x7f0a04fb

.field public static final try_it_guide_two_bubble_text_margin_top:I = 0x7f0a04fc

.field public static final try_it_guide_two_hand_pointer_height:I = 0x7f0a04fd

.field public static final try_it_guide_two_hand_pointer_margin_left_bubble:I = 0x7f0a055d

.field public static final try_it_guide_two_hand_pointer_margin_right:I = 0x7f0a04fe

.field public static final try_it_guide_two_hand_pointer_margin_right_bubble:I = 0x7f0a055e

.field public static final try_it_guide_two_hand_pointer_margin_top:I = 0x7f0a04ff

.field public static final try_it_guide_two_hand_pointer_width:I = 0x7f0a0500

.field public static final try_it_guide_two_layout_margin_extra_top:I = 0x7f0a0501

.field public static final try_it_guide_two_layout_margin_top:I = 0x7f0a0502

.field public static final tutorial_punch_view_alpha:I = 0x7f0a0503

.field public static final voice_input_dialog_button_font_size:I = 0x7f0a0504

.field public static final voice_input_dialog_button_layout_height:I = 0x7f0a0505

.field public static final voice_input_dialog_button_layout_padding:I = 0x7f0a0506

.field public static final voice_input_dialog_button_min_height:I = 0x7f0a0507

.field public static final voice_input_dialog_title_big_font_size:I = 0x7f0a0508

.field public static final voice_input_dialog_title_min_height:I = 0x7f0a0509

.field public static final voice_input_dialog_title_small_font_size:I = 0x7f0a050a

.field public static final voice_input_dialog_view_effect_pane_height:I = 0x7f0a050b

.field public static final voice_input_dialog_view_height:I = 0x7f0a050c

.field public static final voice_input_dialog_window_width:I = 0x7f0a050d

.field public static final voice_input_text_padding_left:I = 0x7f0a050e

.field public static final writing_buddy_guide_text_margin_top:I = 0x7f0a050f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
