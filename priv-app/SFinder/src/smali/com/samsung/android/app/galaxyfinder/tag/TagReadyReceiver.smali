.class public Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TagReadyReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$ReceiveType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 19
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->mHandler:Landroid/os/Handler;

    .line 150
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->setMediaScannerState(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->getTagServiceState(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->setTagServiceState(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/Class;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->startTagService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->getMediaScannerState(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method private getMediaScannerState(Landroid/content/Context;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 142
    const-string v2, "pref_tagservice_state"

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 144
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v2, "pref_media_scanner_ready_once"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 146
    .local v0, "mediaScannerStateFlag":I
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMediaScannerState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    return v0
.end method

.method private getTagServiceState(Landroid/content/Context;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 123
    const-string v2, "pref_tagservice_state"

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 125
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v2, "pref_tagservice_state"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 127
    .local v1, "tagServiceStateFlag":I
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTagServiceState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    return v1
.end method

.method private setMediaScannerState(Landroid/content/Context;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceState"    # I

    .prologue
    .line 132
    const-string v3, "pref_tagservice_state"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 134
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 135
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "pref_media_scanner_ready_once"

    invoke-interface {v1, v3, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 136
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 137
    .local v0, "bResult":Z
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setMediaScannerState : input flag (0 : init, 1: ready) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", commit : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    return-void
.end method

.method private setTagServiceState(Landroid/content/Context;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceState"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 106
    const-string v3, "pref_tagservice_state"

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 108
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 109
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "pref_tagservice_state"

    invoke-interface {v1, v3, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 110
    if-ne p2, v5, :cond_0

    .line 111
    const-string v3, "pref_media_scanner_ready_once"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 113
    const-string v3, "pref_tagservice_fullsync_boot"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 116
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 117
    .local v0, "bResult":Z
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setTagServiceState : input flag (0 : init, 1: boot(service launch), 2: sdcard mount, 3: sdcard unmount) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " commit :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    return-void
.end method

.method private startTagService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "state"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p2, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v1, "com.samsung.android.app.galaxyfinder.TagServiceState"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 94
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 95
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startTagService() intent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 100
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 101
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$ReceiveType;

    invoke-direct {v1, p1, p2}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$ReceiveType;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 102
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 103
    return-void
.end method
