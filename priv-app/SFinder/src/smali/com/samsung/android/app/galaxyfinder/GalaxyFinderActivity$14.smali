.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$14;
.super Ljava/lang/Object;
.source "GalaxyFinderActivity.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ItemSelectionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->copyToClipboard()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0

    .prologue
    .line 1282
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$14;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRetrieveSelectedItems(Ljava/lang/Object;)V
    .locals 14
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v13, 0x0

    .line 1286
    if-eqz p1, :cond_3

    instance-of v11, p1, Ljava/util/HashMap;

    if-eqz v11, :cond_3

    .line 1287
    iget-object v11, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$14;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "clipboardEx"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/sec/clipboard/ClipboardExManager;

    .line 1289
    .local v2, "clipEx":Landroid/sec/clipboard/ClipboardExManager;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .local v10, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object v7, p1

    .line 1290
    check-cast v7, Ljava/util/HashMap;

    .line 1292
    .local v7, "itemsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 1294
    .local v8, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;>;"
    const/4 v1, 0x0

    .line 1295
    .local v1, "aInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1296
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1298
    .local v0, "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "aInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    check-cast v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .line 1299
    .restart local v1    # "aInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    if-eqz v1, :cond_0

    .line 1302
    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->supportCopyContent:Ljava/util/Set;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1900()Ljava/util/Set;

    move-result-object v11

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1305
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 1306
    .local v6, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_0

    .line 1309
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 1310
    .local v5, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionDataToShare()Ljava/lang/String;

    move-result-object v9

    .line 1311
    .local v9, "uri":Ljava/lang/String;
    if-eqz v9, :cond_1

    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isValidUri(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1312
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1317
    .end local v0    # "aEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v6    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    .end local v9    # "uri":Ljava/lang/String;
    :cond_2
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    .line 1318
    new-instance v3, Landroid/sec/clipboard/data/list/ClipboardDataMultipleUri;

    invoke-direct {v3}, Landroid/sec/clipboard/data/list/ClipboardDataMultipleUri;-><init>()V

    .line 1319
    .local v3, "clipdata":Landroid/sec/clipboard/data/list/ClipboardDataMultipleUri;
    invoke-virtual {v3, v10}, Landroid/sec/clipboard/data/list/ClipboardDataMultipleUri;->SetMultipleUri(Ljava/util/ArrayList;)Z

    .line 1320
    iget-object v11, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$14;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v2, v11, v3}, Landroid/sec/clipboard/ClipboardExManager;->setData(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z

    .line 1322
    iget-object v11, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$14;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$200(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/os/Handler;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 1323
    iget-object v11, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$14;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$200(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/os/Handler;

    move-result-object v11

    const/4 v12, 0x3

    invoke-virtual {v11, v12, v13, v13}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v11

    invoke-virtual {v11}, Landroid/os/Message;->sendToTarget()V

    .line 1327
    .end local v1    # "aInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .end local v2    # "clipEx":Landroid/sec/clipboard/ClipboardExManager;
    .end local v3    # "clipdata":Landroid/sec/clipboard/data/list/ClipboardDataMultipleUri;
    .end local v7    # "itemsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    .end local v8    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;>;"
    .end local v10    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_3
    return-void
.end method
