.class public Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;
.super Ljava/lang/Object;
.source "FilterItemLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$1;,
        Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;,
        Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$FilterType;
    }
.end annotation


# static fields
.field private static predefinedTimeTags:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 62
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    .line 65
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_MONTH:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const v2, 0x7f0e0017

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const v2, 0x7f0e0018

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->YESTERDAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const v2, 0x7f0e00d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->TODAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const v2, 0x7f0e00c7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->NEXT_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const v2, 0x7f0e0019

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mContext:Landroid/content/Context;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    .line 39
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mContext:Landroid/content/Context;

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    .line 42
    return-void
.end method

.method private getCategoryItems()Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x5

    .line 104
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v6, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    const/4 v0, 0x0

    .line 106
    .local v0, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    const/4 v4, 0x0

    .line 109
    .local v4, "helperPackages":Ljava/lang/String;
    const-string v7, "notes"

    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 111
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0e007c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v7, v8, v4}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v0, v11}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setTimeTagSupport(Z)V

    .line 114
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->isContainSelectedCategory(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setEnabled(Z)V

    .line 116
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    const-string v7, "communication"

    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 121
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0e0022

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "communication"

    invoke-direct {p0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v7, v8, v9}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v0, v11}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setTimeTagSupport(Z)V

    .line 125
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->isContainSelectedCategory(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setEnabled(Z)V

    .line 127
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    const-string v7, "help"

    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 133
    :try_start_0
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v8, "com.samsung.helphub"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 135
    .local v5, "info":Landroid/content/pm/PackageInfo;
    if-eqz v5, :cond_1

    .line 136
    iget v7, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v3, v7, 0xa

    .line 137
    .local v3, "helpVersionCode":I
    const/4 v7, 0x2

    if-ne v3, v7, :cond_2

    .line 138
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0e004d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x5

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "help"

    invoke-direct {p0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v7, v8, v9}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .local v1, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    move-object v0, v1

    .line 147
    .end local v1    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_0
    :goto_0
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setTimeTagSupport(Z)V

    .line 148
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->isContainSelectedCategory(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setEnabled(Z)V

    .line 150
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    .end local v3    # "helpVersionCode":I
    .end local v5    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    :goto_1
    const-string v7, "images"

    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 159
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0e0057

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "images"

    invoke-direct {p0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v7, v8, v9}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v0, v11}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setTimeTagSupport(Z)V

    .line 162
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->isContainSelectedCategory(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setEnabled(Z)V

    .line 164
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    const-string v7, "music"

    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 168
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0e0075

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "music"

    invoke-direct {p0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v7, v8, v9}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setTimeTagSupport(Z)V

    .line 171
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->isContainSelectedCategory(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setEnabled(Z)V

    .line 173
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    const-string v7, "videos"

    invoke-direct {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 178
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0e00cf

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "videos"

    invoke-direct {p0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v7, v8, v9}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v0, v11}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setTimeTagSupport(Z)V

    .line 181
    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->isContainSelectedCategory(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setEnabled(Z)V

    .line 183
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    return-object v6

    .line 141
    .restart local v3    # "helpVersionCode":I
    .restart local v5    # "info":Landroid/content/pm/PackageInfo;
    :cond_2
    const/4 v7, 0x3

    if-ne v3, v7, :cond_0

    .line 142
    :try_start_1
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mContext:Landroid/content/Context;

    const v8, 0x7f0e00ae

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x5

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "help"

    invoke-direct {p0, v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v7, v8, v9}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .restart local v1    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    move-object v0, v1

    .end local v1    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    goto/16 :goto_0

    .line 152
    .end local v3    # "helpVersionCode":I
    .end local v5    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v2

    .line 153
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method private getHandwritingItems()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    const/4 v1, 0x0

    .line 344
    .local v1, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    const/4 v2, 0x0

    .line 345
    .local v2, "helperPackages":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v5, "com.sec.feature.spen_usp"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 347
    .local v0, "bSPenEnabled":Z
    if-eqz v0, :cond_0

    .line 349
    const-string v4, "handwriting"

    invoke-direct {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 351
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v1    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0e004a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "handwriting"

    invoke-direct {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v4, v5, v6}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    .restart local v1    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setTimeTagSupport(Z)V

    .line 355
    const-string v4, "handwriting"

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setSearchFilter(Ljava/lang/String;)V

    .line 356
    invoke-direct {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->isContainSelectedCategory(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setEnabled(Z)V

    .line 358
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    :cond_0
    return-object v3
.end method

.method private getHelperPackages(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "helperCategory"    # Ljava/lang/String;

    .prologue
    .line 365
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getHelperPackages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 367
    .local v0, "packages":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 368
    :cond_0
    const/4 v0, 0x0

    .line 370
    .end local v0    # "packages":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private getLocationItems()Ljava/util/ArrayList;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 286
    .local v18, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    const-string v7, "alltag"

    invoke-static {v5, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 287
    .local v4, "TAG_URI":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 288
    .local v3, "resolver":Landroid/content/ContentResolver;
    const/4 v9, 0x0

    .line 289
    .local v9, "argList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v5

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getTagSupportedPackageList(Z)Ljava/util/ArrayList;

    move-result-object v9

    .line 290
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v5, "appname in ("

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 291
    .local v20, "queryBuilder":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .line 293
    .local v6, "selection":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 294
    .local v19, "pkg":Ljava/lang/String;
    const/16 v5, 0x27

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0x27

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0x2c

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 297
    .end local v19    # "pkg":Ljava/lang/String;
    :cond_0
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 298
    const/4 v5, 0x0

    const-string v7, ","

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 300
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0x29

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 303
    :cond_1
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 305
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_7

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_7

    .line 306
    const-string v5, "data"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 307
    .local v15, "idx_data":I
    const-string v5, "tag_type"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 308
    .local v17, "idx_type":I
    const-string v5, "rawdata"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 309
    .local v16, "idx_rawData":I
    const-string v5, "tag_count"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 311
    .local v14, "idx_count":I
    :cond_2
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 312
    const/16 v22, 0x0

    .local v22, "tag":Ljava/lang/String;
    const/16 v23, 0x0

    .local v23, "type":Ljava/lang/String;
    const/16 v21, 0x0

    .local v21, "rawData":Ljava/lang/String;
    const/4 v10, 0x0

    .line 314
    .local v10, "count":Ljava/lang/String;
    if-ltz v15, :cond_3

    invoke-interface {v11, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 315
    :cond_3
    if-ltz v17, :cond_4

    move/from16 v0, v17

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 316
    :cond_4
    if-ltz v16, :cond_5

    move/from16 v0, v16

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 317
    :cond_5
    if-ltz v14, :cond_6

    invoke-interface {v11, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 319
    :cond_6
    if-eqz v22, :cond_2

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 320
    new-instance v12, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    invoke-direct {v12, v0, v1, v2, v10}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    .local v12, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 323
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 330
    .end local v10    # "count":Ljava/lang/String;
    .end local v12    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .end local v14    # "idx_count":I
    .end local v15    # "idx_data":I
    .end local v16    # "idx_rawData":I
    .end local v17    # "idx_type":I
    .end local v21    # "rawData":Ljava/lang/String;
    .end local v22    # "tag":Ljava/lang/String;
    .end local v23    # "type":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    .line 331
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->tagComparator:Ljava/util/Comparator;

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 334
    :cond_8
    if-eqz v11, :cond_9

    .line 335
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 338
    :cond_9
    return-object v18
.end method

.method private getTimeItems()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 189
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 190
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    const/4 v0, 0x0

    .line 193
    .local v0, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_MONTH:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->setPredefinedTimeInfo(Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 196
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->setPredefinedTimeInfo(Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 202
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->YESTERDAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->setPredefinedTimeInfo(Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 208
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->TODAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->setPredefinedTimeInfo(Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 214
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    .restart local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->NEXT_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->setPredefinedTimeInfo(Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 220
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    return-object v1
.end method

.method private getUsertagItems()Ljava/util/ArrayList;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 228
    .local v18, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    const-string v7, "alltag"

    invoke-static {v5, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 229
    .local v4, "TAG_URI":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 230
    .local v3, "resolver":Landroid/content/ContentResolver;
    const/4 v9, 0x0

    .line 231
    .local v9, "argList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v5

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getTagSupportedPackageList(Z)Ljava/util/ArrayList;

    move-result-object v9

    .line 232
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v5, "appname in ("

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 233
    .local v20, "queryBuilder":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .line 235
    .local v6, "selection":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 236
    .local v19, "pkg":Ljava/lang/String;
    const/16 v5, 0x27

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0x27

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0x2c

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 239
    .end local v19    # "pkg":Ljava/lang/String;
    :cond_0
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 240
    const/4 v5, 0x0

    const-string v7, ","

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 242
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0x29

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 245
    :cond_1
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 247
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_7

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_7

    .line 248
    const-string v5, "data"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 249
    .local v15, "idx_data":I
    const-string v5, "tag_type"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 250
    .local v17, "idx_type":I
    const-string v5, "rawdata"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 251
    .local v16, "idx_rawData":I
    const-string v5, "tag_count"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 253
    .local v14, "idx_count":I
    :cond_2
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 254
    const/16 v22, 0x0

    .local v22, "tag":Ljava/lang/String;
    const/16 v23, 0x0

    .local v23, "type":Ljava/lang/String;
    const/16 v21, 0x0

    .local v21, "rawData":Ljava/lang/String;
    const/4 v10, 0x0

    .line 256
    .local v10, "count":Ljava/lang/String;
    if-ltz v15, :cond_3

    invoke-interface {v11, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 257
    :cond_3
    if-ltz v17, :cond_4

    move/from16 v0, v17

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 258
    :cond_4
    if-ltz v16, :cond_5

    move/from16 v0, v16

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 259
    :cond_5
    if-ltz v14, :cond_6

    invoke-interface {v11, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 261
    :cond_6
    if-eqz v22, :cond_2

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 262
    new-instance v12, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    invoke-direct {v12, v0, v1, v2, v10}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    .local v12, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isUserTag()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 265
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 272
    .end local v10    # "count":Ljava/lang/String;
    .end local v12    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .end local v14    # "idx_count":I
    .end local v15    # "idx_data":I
    .end local v16    # "idx_rawData":I
    .end local v17    # "idx_type":I
    .end local v21    # "rawData":Ljava/lang/String;
    .end local v22    # "tag":Ljava/lang/String;
    .end local v23    # "type":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    .line 273
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->tagComparator:Ljava/util/Comparator;

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 276
    :cond_8
    if-eqz v11, :cond_9

    .line 277
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 280
    :cond_9
    return-object v18
.end method

.method private isContainSelectedCategory(Ljava/lang/String;)Z
    .locals 7
    .param p1, "helperPackages"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 375
    if-eqz p1, :cond_1

    .line 376
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mContext:Landroid/content/Context;

    const-string v6, "pref_package"

    invoke-virtual {v5, v6, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 378
    .local v2, "pref":Landroid/content/SharedPreferences;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, ","

    invoke-direct {v3, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    .local v3, "st":Ljava/util/StringTokenizer;
    :cond_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 381
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 382
    .local v0, "category":Ljava/lang/String;
    invoke-interface {v2, v0, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 383
    .local v1, "isContained":Z
    if-eqz v1, :cond_0

    .line 384
    const/4 v4, 0x1

    .line 389
    .end local v0    # "category":Ljava/lang/String;
    .end local v1    # "isContained":Z
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    .end local v3    # "st":Ljava/util/StringTokenizer;
    :cond_1
    return v4
.end method

.method private setPredefinedTimeInfo(Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 7
    .param p1, "time"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;
    .param p2, "tagData"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 393
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 394
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;-><init>()V

    .line 396
    .local v0, "converter":Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;
    const/4 v3, 0x0

    .line 397
    .local v3, "typeName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 398
    .local v2, "startTime":Ljava/lang/String;
    const/4 v1, 0x0

    .line 400
    .local v1, "endTime":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$1;->$SwitchMap$com$samsung$android$app$galaxyfinder$ui$filter$data$FilterItemLoader$PREDEFINED_TIME:[I

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 439
    :goto_0
    if-eqz v3, :cond_0

    .line 440
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setTagDatas(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    invoke-virtual {p2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->setTag(Ljava/lang/Object;)V

    .line 444
    .end local v0    # "converter":Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;
    .end local v1    # "endTime":Ljava/lang/String;
    .end local v2    # "startTime":Ljava/lang/String;
    .end local v3    # "typeName":Ljava/lang/String;
    :cond_0
    return-void

    .line 402
    .restart local v0    # "converter":Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;
    .restart local v1    # "endTime":Ljava/lang/String;
    .restart local v2    # "startTime":Ljava/lang/String;
    .restart local v3    # "typeName":Ljava/lang/String;
    :pswitch_0
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->YESTERDAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-virtual {v4, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 404
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->startOfYesterday()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 405
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->endOfYesterday()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 406
    goto :goto_0

    .line 409
    :pswitch_1
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->TODAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-virtual {v4, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 410
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->startOfToday()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 411
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->endOfToday()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 412
    goto :goto_0

    .line 415
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-virtual {v4, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 417
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->startOf7DaysAgo()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 418
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->now()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 419
    goto/16 :goto_0

    .line 422
    :pswitch_3
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->NEXT_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-virtual {v4, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 424
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->now()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 425
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->endOf7DaysNext()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 426
    goto/16 :goto_0

    .line 429
    :pswitch_4
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->mResources:Landroid/content/res/Resources;

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_MONTH:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-virtual {v4, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 431
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->startOf30DaysAgo()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 432
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->now()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 433
    goto/16 :goto_0

    .line 400
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getItems(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 75
    .local v0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    packed-switch p1, :pswitch_data_0

    .line 100
    :goto_0
    return-object v0

    .line 77
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getCategoryItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 78
    goto :goto_0

    .line 81
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getTimeItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 82
    goto :goto_0

    .line 85
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getUsertagItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 86
    goto :goto_0

    .line 89
    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getLocationItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 90
    goto :goto_0

    .line 93
    :pswitch_4
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getHandwritingItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 94
    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
