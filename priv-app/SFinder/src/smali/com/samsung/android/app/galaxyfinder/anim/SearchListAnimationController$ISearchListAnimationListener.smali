.class public interface abstract Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;
.super Ljava/lang/Object;
.source "SearchListAnimationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ISearchListAnimationListener"
.end annotation


# virtual methods
.method public abstract afterExpand()V
.end method

.method public abstract afterHeadUp()V
.end method

.method public abstract beforeExpand()V
.end method

.method public abstract beforeFold()V
.end method

.method public abstract beforeHeadUp()V
.end method
