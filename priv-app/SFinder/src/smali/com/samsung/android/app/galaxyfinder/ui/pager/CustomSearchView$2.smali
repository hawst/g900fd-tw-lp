.class Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;
.super Ljava/lang/Object;
.source "CustomSearchView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 225
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->hideKeypad()V

    .line 227
    if-eqz p2, :cond_2

    .line 228
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 229
    .local v1, "tagData":Ljava/lang/Object;
    const/4 v0, 0x0

    .line 231
    .local v0, "str":Ljava/lang/String;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 232
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 235
    .restart local v0    # "str":Ljava/lang/String;
    :cond_0
    if-eqz v0, :cond_1

    .line 236
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->enoughToFilter()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 237
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->forceUpdateDropDownList(Ljava/lang/CharSequence;)V

    .line 241
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->forceDismissDropDown()V

    .line 243
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 244
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomActionListener;->onActionClickDropDownList(Ljava/lang/Object;)V

    .line 247
    .end local v0    # "str":Ljava/lang/String;
    .end local v1    # "tagData":Ljava/lang/Object;
    :cond_2
    return-void
.end method
