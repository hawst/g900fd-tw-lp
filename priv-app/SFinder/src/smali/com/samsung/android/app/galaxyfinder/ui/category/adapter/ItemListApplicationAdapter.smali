.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListApplicationAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 23
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    .line 27
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v4

    .line 28
    .local v4, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v1, 0x0

    .line 30
    .local v1, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;
    invoke-virtual {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 32
    if-nez p2, :cond_0

    .line 33
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f030031

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 36
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;

    .end local v1    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;
    invoke-direct {v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;-><init>()V

    .line 38
    .restart local v1    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;
    const v5, 0x7f0b004c

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 39
    const v5, 0x7f0b004b

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 41
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 46
    :goto_0
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "applicationName":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v2

    .line 48
    .local v2, "iconUri":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v3

    .line 51
    .local v3, "intent":Landroid/content/Intent;
    iget-object v5, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;->tvName:Landroid/widget/TextView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 52
    iget-object v5, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;->tvName:Landroid/widget/TextView;

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 55
    iget-object v5, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const v6, 0x106000d

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 56
    iget-object v5, v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/4 v6, -0x1

    invoke-virtual {p0, v5, v2, v8, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 59
    invoke-virtual {p0, p2, v3, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 61
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    return-object v5

    .line 43
    .end local v0    # "applicationName":Ljava/lang/String;
    .end local v2    # "iconUri":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;
    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;

    .restart local v1    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListApplicationAdapter$ApplicationItemListViewHolder;
    goto :goto_0
.end method
