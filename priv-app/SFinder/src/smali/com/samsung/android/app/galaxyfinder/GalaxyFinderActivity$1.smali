.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;
.super Landroid/os/Handler;
.source "GalaxyFinderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 162
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 200
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 164
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 165
    iget v4, p1, Landroid/os/Message;->arg2:I

    if-nez v4, :cond_1

    move v1, v3

    .line 167
    .local v1, "neededRequery":Z
    :cond_1
    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isAvailableTagUpdate()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 168
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->bWaitingDelayedMessage:Z
    invoke-static {v4, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$102(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Z)Z

    .line 169
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->requestUpdateTagItem(IZ)V

    goto :goto_0

    .line 171
    :cond_3
    iget v3, p1, Landroid/os/Message;->what:I

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    .line 172
    .local v2, "newMsg":Landroid/os/Message;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$200(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0xbb8

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 178
    .end local v1    # "neededRequery":Z
    .end local v2    # "newMsg":Landroid/os/Message;
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 179
    iget v4, p1, Landroid/os/Message;->arg2:I

    if-nez v4, :cond_4

    move v1, v3

    .line 180
    .restart local v1    # "neededRequery":Z
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->requestUpdateTagItem(IZ)V

    goto :goto_0

    .line 185
    .end local v1    # "neededRequery":Z
    :pswitch_2
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getInstance()Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->init()V

    goto :goto_0

    .line 189
    :pswitch_3
    iget v4, p1, Landroid/os/Message;->arg1:I

    if-ne v4, v1, :cond_6

    move v0, v1

    .line 191
    .local v0, "enter":Z
    :goto_1
    if-nez v0, :cond_0

    .line 192
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 193
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->disableSelectionMode()V

    .line 196
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$1;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->stopActionMode()V
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$300(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    goto/16 :goto_0

    .end local v0    # "enter":Z
    :cond_6
    move v0, v3

    .line 189
    goto :goto_1

    .line 162
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
