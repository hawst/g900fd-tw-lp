.class public final enum Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TagType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/Constants$TagType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

.field public static final enum LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

.field public static final enum PEOPLE:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

.field public static final enum USERDEF:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

.field public static final enum WEATHER:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 329
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    const-string v1, "PEOPLE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->PEOPLE:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    const-string v1, "WEATHER"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->WEATHER:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    const-string v1, "USERDEF"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->USERDEF:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    .line 328
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->PEOPLE:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->WEATHER:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->USERDEF:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 328
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 328
    const-class v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .locals 1

    .prologue
    .line 328
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v0}, [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    return-object v0
.end method
