.class public abstract Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;
.super Landroid/graphics/drawable/AnimationDrawable;
.source "CustomAnimationDrawable.java"


# instance fields
.field mAnimationHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/AnimationDrawable;)V
    .locals 4
    .param p1, "aniDrawable"    # Landroid/graphics/drawable/AnimationDrawable;

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    .line 12
    invoke-virtual {p1}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v0

    .line 13
    .local v0, "aniCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 14
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/AnimationDrawable;->getFrame(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/AnimationDrawable;->getDuration(I)I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    .line 13
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 16
    :cond_0
    return-void
.end method


# virtual methods
.method public getTotalDuration()I
    .locals 4

    .prologue
    .line 35
    const/4 v2, 0x0

    .line 36
    .local v2, "iDuration":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;->getNumberOfFrames()I

    move-result v0

    .line 37
    .local v0, "frameSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 38
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;->getDuration(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    :cond_0
    return v2
.end method

.method abstract onAnimationFinish()V
.end method

.method public start()V
    .locals 4

    .prologue
    .line 20
    invoke-super {p0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;->mAnimationHandler:Landroid/os/Handler;

    .line 23
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;->mAnimationHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable$1;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;)V

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;->getTotalDuration()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 32
    return-void
.end method
