.class public interface abstract Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnViewStateListener;
.super Ljava/lang/Object;
.source "BaseCategoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnViewStateListener"
.end annotation


# virtual methods
.method public abstract onActionItemClicked(Landroid/view/View;)V
.end method

.method public abstract onActionItemLongPressed(Landroid/view/View;)V
.end method

.method public abstract onChangeExpandMode(Landroid/view/View;Z)V
.end method

.method public abstract onChangeItemDragDropState(Z)V
.end method

.method public abstract onChangeSelectedCount(ILjava/lang/String;)V
.end method

.method public abstract onChangedCategoryState(Landroid/view/View;I)V
.end method
