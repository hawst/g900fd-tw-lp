.class public Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
.super Landroid/widget/ScrollView;
.source "SearchScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScrollState;,
        Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;
    }
.end annotation


# instance fields
.field private mDVFSHelper:Landroid/os/DVFSHelper;

.field private mDVFSHelperCore:Landroid/os/DVFSHelper;

.field private mDVFSLockAcquired:Z

.field private mDVFSReleaseRunnable:Ljava/lang/Runnable;

.field private mFling:Z

.field private mLastScrollViewState:I

.field private mLastT:I

.field private mNeedUpdate:Z

.field private mPinchable:Z

.field private mPreviousScrollY:F

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;

.field private mScrollListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;

.field private mScrollable:Z

.field private mStartTouch:F

.field private mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 245
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollable:Z

    .line 23
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mFling:Z

    .line 25
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mNeedUpdate:Z

    .line 31
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mLastScrollViewState:I

    .line 33
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 35
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    .line 37
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSLockAcquired:Z

    .line 39
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSReleaseRunnable:Ljava/lang/Runnable;

    .line 114
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPinchable:Z

    .line 226
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;

    .line 228
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    .line 246
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->init(Landroid/content/Context;)V

    .line 247
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 240
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollable:Z

    .line 23
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mFling:Z

    .line 25
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mNeedUpdate:Z

    .line 31
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mLastScrollViewState:I

    .line 33
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 35
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    .line 37
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSLockAcquired:Z

    .line 39
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSReleaseRunnable:Ljava/lang/Runnable;

    .line 114
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPinchable:Z

    .line 226
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;

    .line 228
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    .line 241
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->init(Landroid/content/Context;)V

    .line 242
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 235
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollable:Z

    .line 23
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mFling:Z

    .line 25
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mNeedUpdate:Z

    .line 31
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mLastScrollViewState:I

    .line 33
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 35
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    .line 37
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSLockAcquired:Z

    .line 39
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSReleaseRunnable:Ljava/lang/Runnable;

    .line 114
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPinchable:Z

    .line 226
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;

    .line 228
    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    .line 236
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->init(Landroid/content/Context;)V

    .line 237
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
    .param p1, "x1"    # I

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->reportScrollState(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPinchable:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPinchable:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x0

    .line 250
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$1;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScaleListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;

    .line 251
    new-instance v0, Landroid/view/ScaleGestureDetector;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScaleListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 252
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, v8}, Landroid/view/ScaleGestureDetector;->setQuickScaleEnabled(Z)V

    .line 254
    new-instance v0, Landroid/os/DVFSHelper;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xc

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 257
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v7

    .line 259
    .local v7, "supportedCPUFreqTable":[I
    if-eqz v7, :cond_0

    .line 260
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    aget v2, v7, v8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 263
    :cond_0
    new-instance v0, Landroid/os/DVFSHelper;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xe

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    .line 266
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v6

    .line 268
    .local v6, "supportedCPUCoreTable":[I
    if-eqz v6, :cond_1

    .line 269
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    aget v2, v6, v8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 286
    :cond_1
    return-void
.end method

.method private reportScrollState(I)V
    .locals 3
    .param p1, "newState"    # I

    .prologue
    const/4 v2, 0x0

    .line 414
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mLastScrollViewState:I

    if-eq p1, v0, :cond_7

    .line 416
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSLockAcquired:Z

    if-eqz v0, :cond_1

    .line 417
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 419
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 423
    :cond_0
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSLockAcquired:Z

    .line 426
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_6

    .line 428
    if-eqz p1, :cond_3

    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mLastScrollViewState:I

    if-nez v0, :cond_3

    .line 430
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    .line 432
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_2

    .line 433
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    .line 436
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSLockAcquired:Z

    .line 438
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 439
    const-string v0, "Scroll"

    const-string v1, "DVFS enable"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :cond_3
    if-nez p1, :cond_6

    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mLastScrollViewState:I

    if-eqz v0, :cond_6

    .line 446
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSLockAcquired:Z

    if-eqz v0, :cond_5

    .line 447
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 449
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_4

    .line 450
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 453
    :cond_4
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSLockAcquired:Z

    .line 456
    :cond_5
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 457
    const-string v0, "Scroll"

    const-string v1, "DVFS disable"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :cond_6
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mLastScrollViewState:I

    .line 464
    :cond_7
    return-void
.end method


# virtual methods
.method public computeScroll()V
    .locals 3

    .prologue
    .line 324
    invoke-super {p0}, Landroid/widget/ScrollView;->computeScroll()V

    .line 326
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;

    if-eqz v0, :cond_1

    .line 327
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPreviousScrollY:F

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollY()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;->onComputeScroll(II)V

    .line 331
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPreviousScrollY:F

    .line 333
    :cond_1
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 405
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 411
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 374
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 375
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mStartTouch:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x41200000    # 10.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 376
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mNeedUpdate:Z

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mNeedUpdate:Z

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollY()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;->onMoveScroll(II)V

    .line 381
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mNeedUpdate:Z

    .line 399
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 400
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 383
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 384
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mStartTouch:F

    .line 386
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    if-eqz v0, :cond_3

    .line 387
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;->cancelUpdateRequest()V

    .line 390
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mFling:Z

    if-eqz v0, :cond_1

    .line 391
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mNeedUpdate:Z

    goto :goto_0
.end method

.method public fling(I)V
    .locals 2
    .param p1, "velocity"    # I

    .prologue
    .line 345
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->fling(I)V

    .line 347
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x258

    if-le v0, v1, :cond_0

    .line 348
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;->cancelUpdateRequest()V

    .line 352
    :cond_0
    return-void
.end method

.method public getScrollable()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollable:Z

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 290
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    .line 292
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->reportScrollState(I)V

    .line 293
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 297
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ScrollView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 299
    if-nez p1, :cond_0

    .line 300
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->reportScrollState(I)V

    .line 302
    :cond_0
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 137
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollable:Z

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 131
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method protected onScrollChanged(IIII)V
    .locals 4
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 356
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 358
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    const-string v0, "Scroll"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScrollChanged isFling:?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mFling:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ t:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " /old "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    if-eqz v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getScrollY()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;->onMoveScroll(II)V

    .line 368
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSReleaseRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 369
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mDVFSReleaseRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 370
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 177
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onSizeChanged(IIII)V

    .line 178
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 142
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollable:Z

    if-nez v0, :cond_0

    .line 143
    const/4 v0, 0x0

    .line 166
    :goto_0
    return v0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 149
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 166
    :goto_1
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 151
    :pswitch_1
    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->reportScrollState(I)V

    goto :goto_1

    .line 159
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->reportScrollState(I)V

    goto :goto_1

    .line 149
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 315
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 317
    if-eqz p2, :cond_0

    .line 318
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->reportScrollState(I)V

    .line 320
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 306
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onWindowFocusChanged(Z)V

    .line 308
    if-nez p1, :cond_0

    .line 309
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->reportScrollState(I)V

    .line 311
    :cond_0
    return-void
.end method

.method public scrollTo(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 182
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 183
    return-void
.end method

.method public setOnComputeScrollListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;

    .prologue
    .line 336
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;

    .line 337
    return-void
.end method

.method public setOnTouchEventListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    .prologue
    .line 707
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    .line 708
    return-void
.end method

.method public setScrollable(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mScrollable:Z

    .line 120
    return-void
.end method
