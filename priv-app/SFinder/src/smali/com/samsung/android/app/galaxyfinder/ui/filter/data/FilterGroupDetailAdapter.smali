.class public Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;
.super Landroid/widget/BaseAdapter;
.source "FilterGroupDetailAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    .line 26
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method

.method private getTextColorByType(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)Landroid/content/res/ColorStateList;
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 91
    const/4 v0, 0x0

    .line 93
    .local v0, "csl":Landroid/content/res/ColorStateList;
    const v1, 0x7f020043

    .line 99
    .local v1, "selector":I
    const v1, 0x7f020043

    .line 110
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 112
    return-object v0
.end method


# virtual methods
.method public clearData()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 43
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 57
    int-to-long v0, p1

    return-wide v0
.end method

.method public getTagType(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)Ljava/lang/String;
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 116
    const-string v0, ""

    .line 118
    .local v0, "type":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isCategoryTag()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e001f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 128
    :cond_0
    :goto_0
    return-object v0

    .line 120
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isTimeTag()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isUserTag()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 123
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 124
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0065

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 62
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 64
    .local v0, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    if-nez p2, :cond_0

    .line 65
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 66
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030008

    invoke-virtual {v1, v3, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 68
    if-eqz v0, :cond_0

    move-object v2, p2

    .line 69
    check-cast v2, Landroid/widget/TextView;

    .line 71
    .local v2, "title":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->getTextColorByType(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 74
    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    const-string v4, ", "

    aput-object v4, v3, v7

    const/4 v4, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->getTagType(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, ", "

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e002f

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setViewDesc(Landroid/view/View;[Ljava/lang/String;)V

    .line 83
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isEnabled()Z

    move-result v3

    invoke-virtual {p2, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 87
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "title":Landroid/widget/TextView;
    :cond_0
    return-object p2
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 33
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 37
    :cond_0
    return-void
.end method
