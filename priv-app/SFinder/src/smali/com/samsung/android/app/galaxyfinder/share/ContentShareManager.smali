.class public Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager;
.super Ljava/lang/Object;
.source "ContentShareManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public static doShareVia(Landroid/app/Activity;Ljava/lang/Object;I)Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;
    .locals 9
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "requestCode"    # I

    .prologue
    const/4 v8, 0x0

    .line 30
    const/4 v3, 0x0

    .line 31
    .local v3, "result":Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 33
    .local v0, "context":Landroid/content/Context;
    if-eqz p1, :cond_0

    instance-of v4, p1, Ljava/util/HashMap;

    if-eqz v4, :cond_0

    .line 34
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 36
    .local v2, "intent":Landroid/content/Intent;
    invoke-static {v2, p1}, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager;->makeIntentToShare(Landroid/content/Intent;Ljava/lang/Object;)Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    move-result-object v3

    .line 38
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->AvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    invoke-virtual {v4, v3}, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 40
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0072

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-object v3

    .line 44
    .restart local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 45
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 47
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->NotAvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    invoke-virtual {v4, v3}, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 48
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0079

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 52
    :cond_2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0038

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v7, 0x64

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private static makeIntentToShare(Landroid/content/Intent;Ljava/lang/Object;)Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;
    .locals 14
    .param p0, "baseIntent"    # Landroid/content/Intent;
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 63
    move-object v5, p1

    check-cast v5, Ljava/util/HashMap;

    .line 64
    .local v5, "itemsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v11, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v8, 0x0

    .line 67
    .local v8, "sharedCnt":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .local v9, "textBuilder":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .line 70
    .local v7, "overallMimetype":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .line 71
    .local v0, "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 73
    .local v4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 74
    .local v3, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionDataToShare()Ljava/lang/String;

    move-result-object v10

    .line 75
    .local v10, "uri":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionMimeType()Ljava/lang/String;

    move-result-object v6

    .line 77
    .local v6, "mimetype":Ljava/lang/String;
    if-eqz v10, :cond_1

    if-eqz v6, :cond_1

    .line 79
    const-string v12, "content"

    invoke-virtual {v10, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "file"

    invoke-virtual {v10, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    :cond_2
    const-string v12, ".txt"

    invoke-virtual {v10, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 81
    const-string v6, "application/text"

    .line 84
    :cond_3
    const-string v12, "text/plain"

    invoke-virtual {v6, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 85
    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    const-string v12, "\n"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 88
    :cond_4
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    invoke-static {v6, v7}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->getOverallMimeType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    .line 96
    .end local v3    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v6    # "mimetype":Ljava/lang/String;
    .end local v10    # "uri":Ljava/lang/String;
    :cond_5
    const-string v12, "suggest_template_content_file"

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getTemplateStyle()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 97
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_0

    const-string v12, "*/"

    invoke-virtual {v7, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 98
    const-string v12, "*/"

    const-string v13, "application/"

    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 103
    .end local v0    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    :cond_6
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_c

    .line 104
    :cond_7
    const/16 v12, 0x64

    if-gt v8, v12, :cond_b

    .line 105
    if-nez v7, :cond_9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_9

    .line 106
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 108
    const-string v12, "android.intent.action.SEND"

    invoke-virtual {p0, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const-string v12, "text/plain"

    invoke-virtual {p0, v12}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    const-string v12, "android.intent.extra.TEXT"

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {p0, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    :cond_8
    :goto_3
    const/high16 v12, 0x10000000

    invoke-virtual {p0, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 127
    const/4 v12, 0x1

    invoke-virtual {p0, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 128
    sget-object v12, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->AvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    .line 134
    :goto_4
    return-object v12

    .line 112
    :cond_9
    invoke-virtual {p0, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_a

    .line 115
    const-string v12, "android.intent.action.SEND"

    invoke-virtual {p0, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    const-string v13, "android.intent.extra.STREAM"

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/os/Parcelable;

    invoke-virtual {p0, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 122
    :goto_5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_8

    .line 123
    const-string v12, "android.intent.extra.TEXT"

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {p0, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    .line 118
    :cond_a
    const-string v12, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {p0, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    const-string v12, "android.intent.extra.STREAM"

    invoke-virtual {p0, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_5

    .line 130
    :cond_b
    sget-object v12, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->ExceedToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    goto :goto_4

    .line 134
    :cond_c
    sget-object v12, Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;->NotAvailableToShare:Lcom/samsung/android/app/galaxyfinder/share/ContentShareManager$ResultType;

    goto :goto_4
.end method
