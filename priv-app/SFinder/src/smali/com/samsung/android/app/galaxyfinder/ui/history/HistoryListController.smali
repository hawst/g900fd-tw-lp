.class public Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;
.super Ljava/lang/Object;
.source "HistoryListController.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mEnabledHistorySuggestions:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->mEnabledHistorySuggestions:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteAllHistoryItem()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 102
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    .line 103
    .local v0, "gfa":Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDb$History;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 106
    :cond_0
    return-void
.end method

.method public static deleteHistoryItem(Ljava/lang/String;)I
    .locals 7
    .param p0, "keyword"    # Ljava/lang/String;

    .prologue
    .line 80
    const/4 v0, -0x1

    .line 82
    .local v0, "ret":I
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v3, "history_keyword"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    const/16 v3, 0x3d

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 87
    const/16 v3, 0x3f

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 88
    const-string v3, " COLLATE NOCASE"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDb$History;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 98
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "selection":Ljava/lang/String;
    :cond_0
    return v0
.end method

.method public static getAllHistoryItems()Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 192
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDb$History;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "added_date DESC LIMIT 30"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 195
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 196
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 199
    :cond_0
    return-object v6
.end method

.method private static getHistoryData(Landroid/database/MatrixCursor;Ljava/lang/String;)V
    .locals 14
    .param p0, "matrixCursor"    # Landroid/database/MatrixCursor;
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 219
    sget-object v11, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v12, "getHistoryData() history start"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    invoke-static {p1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getRecentHistoryItems(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 222
    .local v2, "historyCursor":Landroid/database/Cursor;
    if-nez v2, :cond_0

    .line 223
    sget-object v11, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v12, "getHistoryData() historyCursor is NULL"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :goto_0
    return-void

    .line 227
    :cond_0
    sget-object v11, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getHistoryData() historyCursor count : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v11

    if-nez v11, :cond_1

    .line 229
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 230
    const/4 v2, 0x0

    .line 231
    goto :goto_0

    .line 234
    :cond_1
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 235
    const/4 v9, 0x1

    .line 236
    .local v9, "row":I
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 238
    :goto_1
    const-string v7, ""

    .line 239
    .local v7, "log":Ljava/lang/String;
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnCount()I

    move-result v1

    .line 240
    .local v1, "historyCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v1, :cond_2

    .line 241
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "; "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 240
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 243
    :cond_2
    sget-object v11, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getHistoryData() History no."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "row":I
    .local v10, "row":I
    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", row = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-nez v11, :cond_6

    .line 248
    .end local v1    # "historyCount":I
    .end local v4    # "i":I
    .end local v7    # "log":Ljava/lang/String;
    .end local v10    # "row":I
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 249
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->getCount()I

    move-result v8

    .line 250
    .local v8, "matrixCursorCnt":I
    const-string v11, "history_keyword"

    invoke-interface {v2, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 251
    .local v6, "idx_keyword":I
    const-string v11, "added_date"

    invoke-interface {v2, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 253
    .local v5, "idx_date":I
    :cond_4
    invoke-interface {v2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 254
    .local v3, "historyKeyword":Ljava/lang/String;
    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 256
    .local v0, "addedDate":Ljava/lang/String;
    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const/16 v13, 0x80

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    aput-object v3, v11, v12

    const/4 v12, 0x3

    aput-object v0, v11, v12

    invoke-virtual {p0, v11}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 260
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-nez v11, :cond_4

    .line 263
    .end local v0    # "addedDate":Ljava/lang/String;
    .end local v3    # "historyKeyword":Ljava/lang/String;
    .end local v5    # "idx_date":I
    .end local v6    # "idx_keyword":I
    .end local v8    # "matrixCursorCnt":I
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 264
    const/4 v2, 0x0

    .line 266
    sget-object v11, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v12, "getHistoryData() history end"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .restart local v1    # "historyCount":I
    .restart local v4    # "i":I
    .restart local v7    # "log":Ljava/lang/String;
    .restart local v10    # "row":I
    :cond_6
    move v9, v10

    .end local v10    # "row":I
    .restart local v9    # "row":I
    goto/16 :goto_1
.end method

.method public static getMergedHistoryTagsItems(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p0, "keyword"    # Ljava/lang/String;

    .prologue
    .line 133
    new-instance v3, Landroid/database/MatrixCursor;

    sget-object v6, Lcom/samsung/android/app/galaxyfinder/Constants;->HISTORY_PROJECTION:[Ljava/lang/String;

    invoke-direct {v3, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 135
    .local v3, "resultCursor":Landroid/database/MatrixCursor;
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getMergedHistoryTagsItems() keyword : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getMergedHistoryTagsItems() enable History ? "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-boolean v8, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->mEnabledHistorySuggestions:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    sget-boolean v6, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->mEnabledHistorySuggestions:Z

    if-eqz v6, :cond_0

    .line 139
    invoke-static {v3, p0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getHistoryData(Landroid/database/MatrixCursor;Ljava/lang/String;)V

    .line 141
    :cond_0
    invoke-static {v3, p0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getTagData(Landroid/database/MatrixCursor;Ljava/lang/String;)V

    .line 143
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 144
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getMergedHistoryTagsItems() resultCursor count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Landroid/database/MatrixCursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    const/4 v4, 0x1

    .line 147
    .local v4, "row":I
    invoke-virtual {v3}, Landroid/database/MatrixCursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 149
    :goto_0
    const-string v1, ""

    .line 150
    .local v1, "log":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/database/MatrixCursor;->getColumnCount()I

    move-result v2

    .line 151
    .local v2, "resultCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 152
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 154
    :cond_1
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getMergedHistoryTagsItems() resultCursor no."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "row":I
    .local v5, "row":I
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", row = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-virtual {v3}, Landroid/database/MatrixCursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 160
    .end local v0    # "i":I
    .end local v1    # "log":Ljava/lang/String;
    .end local v2    # "resultCount":I
    .end local v5    # "row":I
    :cond_2
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v7, "getMergedHistoryTagsItems() result cursor end"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    return-object v3

    .restart local v0    # "i":I
    .restart local v1    # "log":Ljava/lang/String;
    .restart local v2    # "resultCount":I
    .restart local v5    # "row":I
    :cond_3
    move v4, v5

    .end local v5    # "row":I
    .restart local v4    # "row":I
    goto :goto_0
.end method

.method public static getRecentHistoryItems(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p0, "keyword"    # Ljava/lang/String;

    .prologue
    .line 165
    new-instance v7, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;

    invoke-direct {v7}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;-><init>()V

    .line 167
    .local v7, "convert":Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;
    const-string v8, " LIMIT 30"

    .line 168
    .local v8, "limit":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "added_date >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->startOf30DaysAgo()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 169
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 171
    .local v4, "selectionArg":[Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND history_keyword LIKE ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 173
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArg":[Ljava/lang/String;
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 178
    .restart local v4    # "selectionArg":[Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDb$History;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "added_date DESC"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 184
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 185
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 188
    :cond_1
    return-object v6
.end method

.method private static getTagData(Landroid/database/MatrixCursor;Ljava/lang/String;)V
    .locals 24
    .param p0, "matrixCursor"    # Landroid/database/MatrixCursor;
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 270
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v22, "getTagData() TAG start"

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const-string v3, "0"

    .line 272
    .local v3, "NO_DATE":Ljava/lang/String;
    const/4 v5, 0x0

    .line 274
    .local v5, "bExactMatched":Z
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v22, "getTagData() matchedTagCursor start"

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getTagSupportedPackageList(Z)Ljava/util/ArrayList;

    move-result-object v4

    .line 277
    .local v4, "argList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 278
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 279
    .local v12, "packageName":Ljava/lang/String;
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getTagData() Searchable UNchecked packageName : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 283
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v12    # "packageName":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v4, v2}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getMatchedTag(Ljava/lang/String;Ljava/util/ArrayList;Z)Landroid/database/Cursor;

    move-result-object v10

    .line 284
    .local v10, "matchedTagCursor":Landroid/database/Cursor;
    if-nez v10, :cond_1

    .line 285
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v22, "getTagData() matchedTagCursor is NULL"

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :goto_1
    return-void

    .line 289
    :cond_1
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getTagData() matchedTagCursor count : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v21

    if-nez v21, :cond_2

    .line 291
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 292
    const/4 v10, 0x0

    .line 293
    goto :goto_1

    .line 296
    :cond_2
    const/4 v15, 0x1

    .line 297
    .local v15, "row":I
    const/16 v19, 0x0

    .line 298
    .local v19, "tagType":I
    const/4 v13, 0x0

    .line 299
    .local v13, "rawData":Ljava/lang/String;
    const-string v8, ""

    .line 300
    .local v8, "log":Ljava/lang/String;
    :cond_3
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v21

    if-eqz v21, :cond_4

    .line 301
    const-string v21, "type"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 302
    const-string v21, "rawdata"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 303
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 304
    const/4 v5, 0x1

    .line 305
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getTagData() exactly matched tag data is found. rawData : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_4
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v22, "getTagData() matched end, related start"

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    invoke-virtual/range {p0 .. p0}, Landroid/database/MatrixCursor;->getCount()I

    move-result v11

    .line 322
    .local v11, "matrixCount":I
    if-eqz v5, :cond_b

    .line 323
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2, v4}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getRelatedTag(Ljava/lang/String;ILjava/util/ArrayList;)Landroid/database/Cursor;

    move-result-object v18

    .line 324
    .local v18, "tagCursor":Landroid/database/Cursor;
    if-eqz v18, :cond_a

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v21

    if-lez v21, :cond_a

    .line 325
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getTagData() tagCursor count : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    const/4 v15, 0x1

    .line 328
    :cond_5
    :goto_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v21

    if-eqz v21, :cond_a

    .line 329
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v21

    if-eqz v21, :cond_9

    .line 330
    const-string v8, ""

    .line 331
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getColumnCount()I

    move-result v17

    .line 332
    .local v17, "tagCount":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_4
    move/from16 v0, v17

    if-ge v6, v0, :cond_8

    .line 333
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v18

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "; "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 332
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 310
    .end local v6    # "i":I
    .end local v11    # "matrixCount":I
    .end local v17    # "tagCount":I
    .end local v18    # "tagCursor":Landroid/database/Cursor;
    :cond_6
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v21

    if-eqz v21, :cond_3

    .line 311
    const-string v8, ""

    .line 312
    invoke-interface {v10}, Landroid/database/Cursor;->getColumnCount()I

    move-result v9

    .line 313
    .local v9, "matchedCount":I
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_5
    if-ge v6, v9, :cond_7

    .line 314
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "; "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 313
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 316
    :cond_7
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getTagData() Matched Tag no."

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    add-int/lit8 v16, v15, 0x1

    .end local v15    # "row":I
    .local v16, "row":I
    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", row = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v15, v16

    .line 317
    .end local v16    # "row":I
    .restart local v15    # "row":I
    goto/16 :goto_2

    .line 335
    .end local v9    # "matchedCount":I
    .restart local v11    # "matrixCount":I
    .restart local v17    # "tagCount":I
    .restart local v18    # "tagCursor":Landroid/database/Cursor;
    :cond_8
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "getTagData() Tag no."

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    add-int/lit8 v16, v15, 0x1

    .end local v15    # "row":I
    .restart local v16    # "row":I
    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", row = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v15, v16

    .line 338
    .end local v6    # "i":I
    .end local v16    # "row":I
    .end local v17    # "tagCount":I
    .restart local v15    # "row":I
    :cond_9
    const-string v21, "type"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 339
    const-string v21, "rawdata"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 340
    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_5

    .line 341
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    add-int/lit8 v11, v11, 0x1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    add-int/lit8 v23, v19, 0x10

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    aput-object v13, v21, v22

    const/16 v22, 0x3

    const-string v23, "0"

    aput-object v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 348
    :cond_a
    if-eqz v18, :cond_b

    .line 349
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 354
    .end local v18    # "tagCursor":Landroid/database/Cursor;
    :cond_b
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v22, "getTagData() Related tag end"

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    const/16 v20, 0x0

    .local v20, "type":Ljava/lang/String;
    const/4 v14, 0x0

    .line 356
    .local v14, "rdata":Ljava/lang/String;
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v21

    if-eqz v21, :cond_d

    .line 358
    :cond_c
    const-string v21, "type"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 359
    const-string v21, "rawdata"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 361
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    add-int/lit8 v11, v11, 0x1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    aput-object v20, v21, v22

    const/16 v22, 0x2

    aput-object v14, v21, v22

    const/16 v22, 0x3

    const-string v23, "0"

    aput-object v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 364
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v21

    if-nez v21, :cond_c

    .line 367
    :cond_d
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 368
    const/4 v10, 0x0

    .line 369
    goto/16 :goto_1
.end method

.method public static isExistingHistoryItems()Z
    .locals 3

    .prologue
    .line 203
    const/4 v1, 0x0

    .line 205
    .local v1, "exist":Z
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getAllHistoryItems()Landroid/database/Cursor;

    move-result-object v0

    .line 207
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 208
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 209
    const/4 v1, 0x1

    .line 212
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 215
    :cond_1
    return v1
.end method

.method public static setHistoryState(Z)V
    .locals 0
    .param p0, "historyState"    # Z

    .prologue
    .line 372
    sput-boolean p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->mEnabledHistorySuggestions:Z

    .line 373
    return-void
.end method

.method public static updateHistoryItem(Ljava/lang/String;)Z
    .locals 14
    .param p0, "keyword"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 30
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 31
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 33
    .local v12, "value":Landroid/content/ContentValues;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 35
    .local v6, "currentTime":J
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .local v10, "sb":Ljava/lang/StringBuilder;
    const-string v0, "history_keyword"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    const/16 v0, 0x3d

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 39
    const/16 v0, 0x3f

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 40
    const-string v0, " COLLATE NOCASE"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 44
    .local v3, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDb$History;->CONTENT_URI:Landroid/net/Uri;

    new-array v4, v13, [Ljava/lang/String;

    aput-object p0, v4, v5

    const-string v5, "added_date DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 51
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 52
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 54
    .local v9, "id":I
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDb$History;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v4, v9

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v11

    .line 55
    .local v11, "updateUri":Landroid/net/Uri;
    const-string v0, "history_keyword"

    invoke-virtual {v12, v0, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v0, "added_date"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 57
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v11, v12, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 67
    .end local v9    # "id":I
    .end local v11    # "updateUri":Landroid/net/Uri;
    :cond_0
    :goto_0
    if-eqz v8, :cond_1

    .line 68
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 71
    :cond_1
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->TAG:Ljava/lang/String;

    const-string v1, "Updated History keyword"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v13

    .line 76
    .end local v3    # "selection":Ljava/lang/String;
    .end local v6    # "currentTime":J
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "sb":Ljava/lang/StringBuilder;
    .end local v12    # "value":Landroid/content/ContentValues;
    :goto_1
    return v0

    .line 61
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v6    # "currentTime":J
    .restart local v8    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "sb":Ljava/lang/StringBuilder;
    .restart local v12    # "value":Landroid/content/ContentValues;
    :cond_2
    const-string v0, "history_keyword"

    invoke-virtual {v12, v0, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v0, "added_date"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 63
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/history/db/HistoryDb$History;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    .end local v3    # "selection":Ljava/lang/String;
    .end local v6    # "currentTime":J
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "sb":Ljava/lang/StringBuilder;
    .end local v12    # "value":Landroid/content/ContentValues;
    :cond_3
    move v0, v5

    .line 76
    goto :goto_1
.end method
