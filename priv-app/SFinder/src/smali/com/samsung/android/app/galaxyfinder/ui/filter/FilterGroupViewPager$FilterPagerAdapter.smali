.class public Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "FilterGroupViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FilterPagerAdapter"
.end annotation


# instance fields
.field private mCategoryPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

.field private mHandwritingPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

.field private mLocationPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

.field private mTimePage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

.field private mUsertagPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 426
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    .line 427
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 416
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mCategoryPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 418
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 420
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    .line 422
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    .line 424
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mHandwritingPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 428
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0
    .param p1, "pager"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "view"    # Ljava/lang/Object;

    .prologue
    .line 519
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "pager":Landroid/view/View;
    check-cast p3, Landroid/view/View;

    .end local p3    # "view":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 520
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 437
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/PagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 438
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 432
    sget v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->FILTER_PAGE_INDEX_MAX:I

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 442
    const/4 v1, 0x0

    .line 444
    .local v1, "view":Landroid/view/View;
    packed-switch p2, :pswitch_data_0

    .line 505
    :goto_0
    if-eqz p1, :cond_5

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_5

    .line 506
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewScrollView;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewScrollView;-><init>(Landroid/content/Context;)V

    .line 507
    .local v0, "sv":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewScrollView;
    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewScrollView;->addView(Landroid/view/View;)V

    .line 508
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 510
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v0, v6}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 513
    .end local v0    # "sv":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewScrollView;
    :goto_1
    return-object v0

    .line 447
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mCategoryPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    if-nez v2, :cond_0

    .line 448
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mCategoryPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 449
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mCategoryPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setCallback(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;)V

    .line 450
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mCategoryPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 453
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mCategoryPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 454
    goto :goto_0

    .line 458
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    if-nez v2, :cond_1

    .line 459
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 460
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setCallback(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;)V

    .line 461
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 464
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 465
    goto/16 :goto_0

    .line 469
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    if-nez v2, :cond_2

    .line 470
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    .line 471
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    const v3, 0x7f0e003e

    invoke-virtual {v2, v3, v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->setHintText(ILandroid/view/View$OnClickListener;)V

    .line 472
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->setCallback(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;)V

    .line 473
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->setData(Ljava/util/ArrayList;)V

    .line 476
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    .line 477
    goto/16 :goto_0

    .line 481
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    if-nez v2, :cond_3

    .line 482
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    .line 483
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    const v3, 0x7f0e003b

    invoke-virtual {v2, v3, v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->setHintText(ILandroid/view/View$OnClickListener;)V

    .line 484
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->setCallback(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;)V

    .line 485
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->setData(Ljava/util/ArrayList;)V

    .line 488
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    .line 489
    goto/16 :goto_0

    .line 493
    :pswitch_4
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mHandwritingPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    if-nez v2, :cond_4

    .line 494
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mHandwritingPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 495
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mHandwritingPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setCallback(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;)V

    .line 496
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mHandwritingPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    invoke-virtual {v2, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setToggleMode(Z)V

    .line 497
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mHandwritingPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 501
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;->mHandwritingPage:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    .line 513
    goto/16 :goto_1

    .line 444
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "pager"    # Landroid/view/View;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 524
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
