.class public final Lcom/samsung/android/app/galaxyfinder/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_bar_bg_color:I = 0x7f090000

.field public static final action_bar_cursor_color:I = 0x7f090001

.field public static final action_bar_text_color:I = 0x7f090002

.field public static final card_application_appname_text:I = 0x7f090003

.field public static final card_application_help_text_color:I = 0x7f090004

.field public static final card_browser_divider:I = 0x7f090005

.field public static final card_browser_title_text:I = 0x7f09006a

.field public static final card_browser_url_text:I = 0x7f09006b

.field public static final card_category_control_btn_color:I = 0x7f090006

.field public static final card_category_count_text:I = 0x7f090007

.field public static final card_category_group_text:I = 0x7f090008

.field public static final card_category_group_underline_color:I = 0x7f090009

.field public static final card_category_sub_bg:I = 0x7f09006c

.field public static final card_category_title_text:I = 0x7f09000a

.field public static final card_category_title_text_color_selector:I = 0x7f090075

.field public static final card_category_title_text_pressed:I = 0x7f09000b

.field public static final card_chaton_date:I = 0x7f09000c

.field public static final card_chaton_message:I = 0x7f09000d

.field public static final card_chaton_name:I = 0x7f09000e

.field public static final card_email_read_background_color:I = 0x7f09000f

.field public static final card_email_sort_layout_background_color:I = 0x7f09006d

.field public static final card_gallery_common_text:I = 0x7f090010

.field public static final card_gallery_date_text_color:I = 0x7f090011

.field public static final card_gallery_desc_text_color:I = 0x7f090012

.field public static final card_gallery_effect_text_shadow:I = 0x7f090013

.field public static final card_gallery_thumbnail_press_color:I = 0x7f090014

.field public static final card_lifetimes_text_date_color:I = 0x7f090015

.field public static final card_lifetimes_text_desc_color:I = 0x7f090016

.field public static final card_memo_text_date_color:I = 0x7f090017

.field public static final card_message_body_text:I = 0x7f090018

.field public static final card_message_conv_text_body:I = 0x7f090019

.field public static final card_message_conv_text_date:I = 0x7f09001a

.field public static final card_message_conv_text_failed:I = 0x7f09001b

.field public static final card_message_conv_text_sender:I = 0x7f09001c

.field public static final card_message_date_text:I = 0x7f09001d

.field public static final card_message_sender_text:I = 0x7f09001e

.field public static final card_message_thread_text_body:I = 0x7f09001f

.field public static final card_message_thread_text_date:I = 0x7f090020

.field public static final card_message_thread_text_sender:I = 0x7f090021

.field public static final card_note_common_text:I = 0x7f090022

.field public static final card_note_common_text_title:I = 0x7f090023

.field public static final card_note_common_text_title_shadow:I = 0x7f090024

.field public static final card_phone_name_text:I = 0x7f090025

.field public static final card_phone_number_text:I = 0x7f090026

.field public static final card_pinall_base_background:I = 0x7f090027

.field public static final card_pinall_content_background:I = 0x7f090028

.field public static final card_pinall_no_content_background:I = 0x7f090029

.field public static final card_pinall_text:I = 0x7f09002a

.field public static final card_pinall_title_text:I = 0x7f09002b

.field public static final card_planner_color_bar:I = 0x7f09002c

.field public static final card_planner_time_text:I = 0x7f09006e

.field public static final card_settings_group_text:I = 0x7f09006f

.field public static final card_settings_menu_desc_text:I = 0x7f090070

.field public static final card_settings_menu_name_text:I = 0x7f090071

.field public static final card_storyalbum_date_text:I = 0x7f09002d

.field public static final card_stroyalbum_title_text:I = 0x7f09002e

.field public static final card_video_contextual_date_text:I = 0x7f09002f

.field public static final card_video_contextual_text:I = 0x7f090030

.field public static final card_video_effect_text_shadow:I = 0x7f090031

.field public static final card_video_thumbnail_press_color:I = 0x7f090032

.field public static final card_web_link_cpname_color:I = 0x7f090033

.field public static final card_webpreview_divider:I = 0x7f090034

.field public static final card_webpreview_inner_stroke:I = 0x7f090072

.field public static final category_category_background:I = 0x7f090035

.field public static final category_footer_color:I = 0x7f090036

.field public static final category_item_default_bg_color:I = 0x7f090037

.field public static final category_list_select_all_text:I = 0x7f090038

.field public static final category_list_text:I = 0x7f090039

.field public static final common_related_text_highlight:I = 0x7f09003a

.field public static final common_text_highlight:I = 0x7f09003b

.field public static final custom_search_edittext_divider:I = 0x7f09003c

.field public static final custom_search_hint_text:I = 0x7f09003d

.field public static final custom_search_view_edit_hinttext_color:I = 0x7f09003e

.field public static final custom_search_view_edit_text_color:I = 0x7f09003f

.field public static final expand_fully_show_more_text:I = 0x7f090040

.field public static final filter_panel_default_background_color:I = 0x7f090041

.field public static final filter_panel_detail_item_hint_text_color:I = 0x7f090042

.field public static final filter_panel_detail_item_text_default_color:I = 0x7f090043

.field public static final filter_panel_detail_item_text_selected_color:I = 0x7f090044

.field public static final filter_panel_selected_item_text_color:I = 0x7f090045

.field public static final guide_bubble_button_help_text_color:I = 0x7f090046

.field public static final guide_bubble_button_help_text_pressed_color:I = 0x7f090047

.field public static final guide_help_layout_background:I = 0x7f090048

.field public static final guide_start_text_box_text_color:I = 0x7f090049

.field public static final guide_text_box_text_color:I = 0x7f09004a

.field public static final help_popup_start_up_button:I = 0x7f090076

.field public static final history_item_list_date_text_color:I = 0x7f09004b

.field public static final history_item_list_fullscreen_date_text_color:I = 0x7f09004c

.field public static final history_item_list_fullscreen_delete_all_color:I = 0x7f09004d

.field public static final history_item_list_fullscreen_divider_color:I = 0x7f09004e

.field public static final history_item_list_fullscreen_keyword_text_color:I = 0x7f09004f

.field public static final history_item_list_keyword_text_color:I = 0x7f090050

.field public static final image_border:I = 0x7f090051

.field public static final image_pressed_selector:I = 0x7f090077

.field public static final layer_mask_color:I = 0x7f090052

.field public static final light_theme_text_color:I = 0x7f090053

.field public static final list_item_email_body_text_color_selector:I = 0x7f090078

.field public static final list_item_email_date_text_color_selector:I = 0x7f090079

.field public static final list_item_email_subject_text_color_selector:I = 0x7f09007a

.field public static final list_item_expand_text_color_selector:I = 0x7f09007b

.field public static final list_item_sub_title_text_color_selector:I = 0x7f09007c

.field public static final list_item_title_text_color_selector:I = 0x7f09007d

.field public static final location_tag_hint_text:I = 0x7f090073

.field public static final ripple_background_color:I = 0x7f090054

.field public static final root_layout_background:I = 0x7f090055

.field public static final search_result_background_color:I = 0x7f090056

.field public static final search_web_keyword_title:I = 0x7f090057

.field public static final settings_delete_history_list_item_text_date:I = 0x7f090058

.field public static final settings_delete_history_list_item_text_name:I = 0x7f090059

.field public static final settings_list_selectall_text_color:I = 0x7f09005a

.field public static final settings_select_filter_list_divider:I = 0x7f09005b

.field public static final settings_select_filter_list_item_text:I = 0x7f09005c

.field public static final share_text_color:I = 0x7f09005d

.field public static final symbol_help_string_text_color:I = 0x7f09005e

.field public static final tag_cloud_background_color:I = 0x7f09005f

.field public static final tag_expandable_bg_color:I = 0x7f090060

.field public static final tag_expandable_group_title_color:I = 0x7f090074

.field public static final tag_scrollview_vertical_title_color:I = 0x7f090061

.field public static final tagcloud_item_text_color_selector:I = 0x7f09007e

.field public static final tagcloud_title_text_color_selector:I = 0x7f09007f

.field public static final tutorial_category_tag_view:I = 0x7f090062

.field public static final tutorial_punch_view:I = 0x7f090063

.field public static final user_tag_hint_text:I = 0x7f090064

.field public static final voice_input_button_disabled_text_color:I = 0x7f090065

.field public static final voice_input_button_pressed_text_color:I = 0x7f090066

.field public static final voice_input_button_text_color:I = 0x7f090067

.field public static final voice_input_popup_title_shadow_color:I = 0x7f090068

.field public static final voice_input_title_text_color:I = 0x7f090069


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
