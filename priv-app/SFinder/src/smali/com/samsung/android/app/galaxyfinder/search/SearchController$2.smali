.class Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;
.super Landroid/os/AsyncTask;
.source "SearchController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/search/SearchController;->removeInvalidTag(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field checkedTagPackageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

.field final synthetic val$tag:Ljava/lang/String;

.field final synthetic val$targets:Ljava/util/ArrayList;

.field final synthetic val$type:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 480
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->val$targets:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->val$tag:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->val$type:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 482
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->checkedTagPackageList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 480
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 14
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    const/16 v4, 0x27

    const/4 v13, 0x0

    .line 487
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "apptag"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 490
    .local v1, "TAG_URI":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$1000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 492
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->val$targets:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->val$targets:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 493
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->val$targets:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->checkedTagPackageList:Ljava/util/ArrayList;

    .line 498
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->val$tag:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->checkedTagPackageList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->checkedTagPackageList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_3

    .line 500
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "removeInvalidTag is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    :cond_1
    :goto_1
    return-object v13

    .line 495
    :cond_2
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getTagSupportedPackageList(Z)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->checkedTagPackageList:Ljava/util/ArrayList;

    goto :goto_0

    .line 504
    :cond_3
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v2, "appname in ("

    invoke-direct {v11, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 505
    .local v11, "queryBuilder":Ljava/lang/StringBuilder;
    const/4 v6, 0x1

    .line 507
    .local v6, "bFirst":Z
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->checkedTagPackageList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 508
    .local v10, "pkg":Ljava/lang/String;
    if-nez v6, :cond_4

    .line 509
    const/16 v2, 0x2c

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 511
    :cond_4
    const/4 v6, 0x0

    .line 512
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 514
    .end local v10    # "pkg":Ljava/lang/String;
    :cond_5
    const/16 v2, 0x29

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    const-string v2, "type"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->val$type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->val$tag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 518
    const/4 v7, 0x0

    .line 521
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 522
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->checkInconsitentRecord(Landroid/database/Cursor;)V
    invoke-static {v2, v7}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$1200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Landroid/database/Cursor;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 528
    if-eqz v7, :cond_1

    .line 529
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 523
    :catch_0
    move-exception v12

    .line 524
    .local v12, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528
    if-eqz v7, :cond_1

    .line 529
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 525
    .end local v12    # "se":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 526
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 528
    if-eqz v7, :cond_1

    .line 529
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 528
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_6

    .line 529
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 480
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 537
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "removeInvalidTag is finished"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    return-void
.end method
