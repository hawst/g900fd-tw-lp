.class public Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;
.super Ljava/lang/Object;
.source "PeopleTagProbe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FacesInfo"
.end annotation


# instance fields
.field mFaceID:I

.field mID:I

.field mImageID:I

.field mName:Ljava/lang/String;

.field mPersonID:I


# direct methods
.method constructor <init>(II)V
    .locals 1
    .param p1, "imageID"    # I
    .param p2, "personID"    # I

    .prologue
    .line 464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mName:Ljava/lang/String;

    .line 465
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mImageID:I

    .line 466
    iput p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mPersonID:I

    .line 467
    return-void
.end method

.method constructor <init>(IIIILjava/lang/String;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "faceID"    # I
    .param p3, "imageID"    # I
    .param p4, "personID"    # I
    .param p5, "name"    # Ljava/lang/String;

    .prologue
    .line 480
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;-><init>(IIILjava/lang/String;)V

    .line 481
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mID:I

    .line 482
    return-void
.end method

.method constructor <init>(IIILjava/lang/String;)V
    .locals 0
    .param p1, "faceID"    # I
    .param p2, "imageID"    # I
    .param p3, "personID"    # I
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 475
    invoke-direct {p0, p2, p3, p4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;-><init>(IILjava/lang/String;)V

    .line 476
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mFaceID:I

    .line 477
    return-void
.end method

.method constructor <init>(IILjava/lang/String;)V
    .locals 0
    .param p1, "imageID"    # I
    .param p2, "personID"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 470
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;-><init>(II)V

    .line 471
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mName:Ljava/lang/String;

    .line 472
    return-void
.end method


# virtual methods
.method getFaceID()I
    .locals 1

    .prologue
    .line 489
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mFaceID:I

    return v0
.end method

.method getID()I
    .locals 1

    .prologue
    .line 485
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mID:I

    return v0
.end method

.method getImageID()I
    .locals 1

    .prologue
    .line 493
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mImageID:I

    return v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method getPersonID()I
    .locals 1

    .prologue
    .line 497
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mPersonID:I

    return v0
.end method

.method setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 505
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/PeopleTagProbe$FacesInfo;->mName:Ljava/lang/String;

    .line 506
    return-void
.end method
