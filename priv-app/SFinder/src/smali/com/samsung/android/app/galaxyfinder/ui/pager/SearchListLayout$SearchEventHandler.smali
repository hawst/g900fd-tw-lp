.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;
.super Landroid/os/Handler;
.source "SearchListLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchEventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0

    .prologue
    .line 1720
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    .param p2, "x1"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$1;

    .prologue
    .line 1720
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1724
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 1819
    :cond_0
    :goto_0
    return-void

    .line 1726
    :sswitch_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_INPUT_TIMER_EXPIRED"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1730
    :sswitch_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_COMPLETE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1732
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->completedSearch()V
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2800(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    goto :goto_0

    .line 1736
    :sswitch_2
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_REQUEST"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1737
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    move-result-object v2

    invoke-virtual {v2, v4, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->smoothScrollTo(II)V

    .line 1739
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpSearchResult(Z)V
    invoke-static {v2, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Z)V

    .line 1740
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/os/Bundle;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->startSearch(Landroid/os/Bundle;)V
    invoke-static {v3, v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1744
    :sswitch_3
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_REQUEST_RELOAD"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1746
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isSearching()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1747
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->prepareDataForUpdating()Landroid/os/Bundle;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Landroid/os/Bundle;

    move-result-object v1

    .line 1749
    .local v1, "reloadBundledQuery":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 1750
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->startSearch(Landroid/os/Bundle;)V
    invoke-static {v2, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3000(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1757
    .end local v1    # "reloadBundledQuery":Landroid/os/Bundle;
    :sswitch_4
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_REQUEST"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1759
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1760
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1762
    .local v0, "query":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpSearchResult(Z)V
    invoke-static {v2, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Z)V

    .line 1764
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->clearQueryInfo()V
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    .line 1765
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    const-string v3, "query_from_vr"

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryInfoFrom(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Ljava/lang/String;)V

    .line 1767
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->setQueryText(Ljava/lang/String;Z)V
    invoke-static {v2, v0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1773
    .end local v0    # "query":Ljava/lang/String;
    :sswitch_5
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1775
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->addCategoryView(Ljava/lang/Object;)V
    invoke-static {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1779
    :sswitch_6
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_RESPONSE_RELOAD"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1781
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->refreshCategoryData(Ljava/lang/Object;)V
    invoke-static {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3500(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1785
    :sswitch_7
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_RESPONSE_TAG_RETRIEVED"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1787
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->refreshCategoryTagInfo()V

    goto/16 :goto_0

    .line 1791
    :sswitch_8
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_CLEAR"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1793
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpSearchResult(Z)V
    invoke-static {v2, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Z)V

    goto/16 :goto_0

    .line 1797
    :sswitch_9
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_CLEAR"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1799
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpAllTags()V
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3600(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    .line 1800
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpEditText()V
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$3700(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V

    .line 1802
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpSearchResult(Z)V
    invoke-static {v2, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Z)V

    .line 1804
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->isExistingSelectedPanel()Z
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2400(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1805
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->showKeypad()V

    goto/16 :goto_0

    .line 1810
    :sswitch_a
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage : MSG_SEARCH_INPUT_EMPTY"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1812
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$SearchEventHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->cleanUpSearchResult(Z)V
    invoke-static {v2, v5}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$2900(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;Z)V

    goto/16 :goto_0

    .line 1724
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_8
        0x4 -> :sswitch_9
        0x5 -> :sswitch_4
        0x6 -> :sswitch_3
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_a
        0x67 -> :sswitch_0
    .end sparse-switch
.end method
