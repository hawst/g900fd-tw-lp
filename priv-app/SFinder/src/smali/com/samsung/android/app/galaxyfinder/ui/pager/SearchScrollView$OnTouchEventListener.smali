.class public interface abstract Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;
.super Ljava/lang/Object;
.source "SearchScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnTouchEventListener"
.end annotation


# virtual methods
.method public abstract cancelUpdateRequest()V
.end method

.method public abstract onMoveScroll(II)V
.end method

.method public abstract onMultiSelectionChanged(IIIIII)V
.end method

.method public abstract onMultiSelectionEnd()V
.end method

.method public abstract onMultiSelectionStart()V
.end method

.method public abstract onPinchEvent(ZI)V
.end method

.method public abstract onTouchEvent(Landroid/view/MotionEvent;)V
.end method
