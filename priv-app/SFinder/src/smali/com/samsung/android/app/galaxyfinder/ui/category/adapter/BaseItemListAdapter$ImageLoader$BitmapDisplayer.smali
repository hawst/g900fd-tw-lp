.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;
.super Ljava/lang/Object;
.source "BaseItemListAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BitmapDisplayer"
.end annotation


# instance fields
.field drawable:Landroid/graphics/drawable/Drawable;

.field imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;Landroid/graphics/drawable/Drawable;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)V
    .locals 0
    .param p2, "d"    # Landroid/graphics/drawable/Drawable;
    .param p3, "i"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .prologue
    .line 1238
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1239
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->drawable:Landroid/graphics/drawable/Drawable;

    .line 1240
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    .line 1241
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 1244
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 1245
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->imageViewReused(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1276
    :cond_0
    :goto_0
    return-void

    .line 1249
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mBgResId:I
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$1500(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 1250
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mBgResId:I
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$1500(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1251
    .local v1, "bg":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1262
    .end local v1    # "bg":Landroid/graphics/drawable/Drawable;
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->drawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_5

    .line 1263
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1264
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->mContext:Landroid/content/Context;

    const v3, 0x7f04000c

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1265
    .local v0, "ani":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 1253
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 1254
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_1

    .line 1255
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mContentType:I
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$800(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    .line 1258
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1266
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mAltUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1600(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1267
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mAltUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$1600(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_0

    .line 1268
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mSchemeType:I
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->access$900(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;)I

    move-result v2

    if-ne v2, v4, :cond_7

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_7

    .line 1270
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1272
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$BitmapDisplayer;->imageInfo:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader$ImageHolder;->imageView:Landroid/widget/ImageView;

    const v3, 0x7f090037

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method
