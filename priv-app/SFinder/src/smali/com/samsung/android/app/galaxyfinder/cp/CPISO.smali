.class public Lcom/samsung/android/app/galaxyfinder/cp/CPISO;
.super Ljava/lang/Object;
.source "CPISO.java"


# static fields
.field private static RO_CSC_COUNTRY_ISO:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "ro.csc.countryiso_code"

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/cp/CPISO;->RO_CSC_COUNTRY_ISO:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentISO(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/cp/CPISO;->RO_CSC_COUNTRY_ISO:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 16
    .local v1, "roISO":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 34
    .end local v1    # "roISO":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 20
    .restart local v1    # "roISO":Ljava/lang/String;
    :cond_0
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 23
    .local v3, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v2

    .line 24
    .local v2, "simISO":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "networkISO":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v4, ""

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object v1, v2

    .line 27
    goto :goto_0

    .line 30
    :cond_1
    if-eqz v0, :cond_2

    const-string v4, ""

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object v1, v0

    .line 31
    goto :goto_0

    .line 34
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
