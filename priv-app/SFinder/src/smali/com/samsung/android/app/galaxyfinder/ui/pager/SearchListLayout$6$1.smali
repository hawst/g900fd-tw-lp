.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6$1;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->onLayoutChange(Landroid/view/View;IIIIIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/16 v3, 0x12f

    .line 423
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onLayoutChange:: sendMsg"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 426
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 429
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 431
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6$1;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mCategoryUpdateHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 432
    return-void
.end method
