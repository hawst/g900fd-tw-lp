.class public final enum Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
.super Ljava/lang/Enum;
.source "TagConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/TagConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TagServiceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

.field public static final enum TAG_FULLSYNC_BOOT_COMPLETED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

.field public static final enum TAG_FULLSYNC_MEDIA_SCANNER_FINISHED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

.field public static final enum TAG_FULLSYNC_SDCARD_MOUNTED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

.field public static final enum TAG_FULLSYNC_SDCARD_UNMOUNTED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

.field public static final enum TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 83
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    const-string v1, "TAG_PARTIALLY_SYNC"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    const-string v1, "TAG_FULLSYNC_BOOT_COMPLETED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_FULLSYNC_BOOT_COMPLETED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    const-string v1, "TAG_FULLSYNC_SDCARD_MOUNTED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_FULLSYNC_SDCARD_MOUNTED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    const-string v1, "TAG_FULLSYNC_SDCARD_UNMOUNTED"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_FULLSYNC_SDCARD_UNMOUNTED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    const-string v1, "TAG_FULLSYNC_MEDIA_SCANNER_FINISHED"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_FULLSYNC_MEDIA_SCANNER_FINISHED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    .line 82
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_FULLSYNC_BOOT_COMPLETED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_FULLSYNC_SDCARD_MOUNTED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_FULLSYNC_SDCARD_UNMOUNTED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_FULLSYNC_MEDIA_SCANNER_FINISHED:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 82
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual {v0}, [Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    return-object v0
.end method
