.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;
.super Ljava/lang/Object;
.source "GalaxyFinderActivity.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0

    .prologue
    .line 475
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchCanceled()V
    .locals 0

    .prologue
    .line 495
    return-void
.end method

.method public onSearchCompleted()V
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->responseSearchCompleted()V

    .line 486
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 488
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$602(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 490
    :cond_0
    return-void
.end method

.method public onSearchResultUpdated(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "from"    # Ljava/lang/String;

    .prologue
    .line 479
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->responseSearchResult(Ljava/lang/Object;Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method public onSearchTagRetrieved()V
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$7;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->responseSearchTagRetrieved()V

    .line 500
    return-void
.end method
