.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;
.super Ljava/lang/Object;
.source "GalaxyFinderActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->requestKeypad(ZJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

.field final synthetic val$bTurnOn:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Z)V
    .locals 0

    .prologue
    .line 717
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    iput-boolean p2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->val$bTurnOn:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 720
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 721
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->val$bTurnOn:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 722
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->showKeypad()V

    .line 723
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mIsTablet:Z
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 724
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCustomActionBarSearchbox:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->showDropDown()V

    .line 733
    :cond_0
    :goto_0
    return-void

    .line 727
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->hideKeypad()V

    .line 728
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mIsTablet:Z
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 729
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$11;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mCustomActionBarSearchbox:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->forceDismissDropDown()V

    goto :goto_0
.end method
