.class Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;
.super Landroid/os/AsyncTask;
.source "LocationTagReadyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConvertAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final LOCATION_BOUNDARY_IN_METER:F

.field private final mLock:Ljava/lang/Object;

.field private mParentHandler:Landroid/os/Handler;

.field private final mRequest:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;",
            ">;"
        }
    .end annotation
.end field

.field private mType:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

.field private mWaiting:Z

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;Landroid/os/Handler;)V
    .locals 2
    .param p2, "type"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;
    .param p3, "parentHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x0

    .line 544
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 532
    const/high16 v0, 0x447a0000    # 1000.0f

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->LOCATION_BOUNDARY_IN_METER:F

    .line 534
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mType:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    .line 536
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mParentHandler:Landroid/os/Handler;

    .line 538
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mWaiting:Z

    .line 540
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mLock:Ljava/lang/Object;

    .line 542
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mRequest:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 545
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mType:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    .line 546
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mParentHandler:Landroid/os/Handler;

    .line 547
    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # [Ljava/lang/String;

    .prologue
    .line 530
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->requestConvertAddress(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private geocoderTask(Landroid/net/Uri;Ljava/util/ArrayList;)I
    .locals 32
    .param p1, "targetUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 756
    .local p2, "tagInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;>;"
    new-instance v11, Landroid/location/Geocoder;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 757
    .local v11, "geocoder":Landroid/location/Geocoder;
    const/16 v30, 0x0

    .local v30, "successCount":I
    const/16 v24, 0x0

    .line 758
    .local v24, "failCount":I
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LogFileManager;->getInstance()Lcom/samsung/android/app/galaxyfinder/tag/LogFileManager;

    move-result-object v28

    .line 759
    .local v28, "logFileManager":Lcom/samsung/android/app/galaxyfinder/tag/LogFileManager;
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/app/galaxyfinder/tag/LogFileManager;->isLogEnabled()Z

    move-result v31

    .line 761
    .local v31, "writeGeoLog":Z
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lez v12, :cond_0

    if-nez v11, :cond_1

    .line 762
    :cond_0
    const-string v12, "GeoTag"

    const-string v13, "GeocoderTask : TagInfo is null"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v12, v30

    .line 880
    :goto_0
    return v12

    .line 765
    :cond_1
    new-instance v29, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;

    invoke-direct/range {v29 .. v29}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;-><init>()V

    .line 766
    .local v29, "prevLocationTagData":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    .line 768
    .local v21, "builder":Ljava/lang/StringBuilder;
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .local v25, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_10

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;

    .line 770
    .local v26, "info":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;
    const/4 v12, 0x0

    :try_start_0
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 771
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getLatitude()D

    move-result-wide v6

    .line 772
    .local v6, "lat":D
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getLongitude()D

    move-result-wide v8

    .line 774
    .local v8, "logi":D
    const-string v12, "GeoTag"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "1. LocationInfo : latitude = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " , longitude = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    if-eqz v31, :cond_2

    .line 777
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "1. LocationInfo : latitude = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " , longitude = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/tag/LogFileManager;->writeLog(Ljava/lang/String;)V

    .line 781
    :cond_2
    const/high16 v22, 0x447a0000    # 1000.0f

    .line 782
    .local v22, "distance":F
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_3

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_3

    .line 784
    const/4 v12, 0x1

    new-array v10, v12, [F

    .line 785
    .local v10, "results":[F
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getLatitude()D

    move-result-wide v2

    .line 786
    .local v2, "lastLatitude":D
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getLongitude()D

    move-result-wide v4

    .line 787
    .local v4, "lastLongitude":D
    invoke-static/range {v2 .. v10}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 788
    const/4 v12, 0x0

    aget v22, v10, v12

    .line 791
    .end local v2    # "lastLatitude":D
    .end local v4    # "lastLongitude":D
    .end local v10    # "results":[F
    :cond_3
    const/high16 v12, 0x447a0000    # 1000.0f

    cmpl-float v12, v22, v12

    if-ltz v12, :cond_e

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move-wide v14, v6

    move-wide/from16 v16, v8

    .line 792
    invoke-direct/range {v12 .. v17}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->getAddress(Landroid/net/Uri;DD)Ljava/lang/String;

    move-result-object v19

    .line 793
    .local v19, "address":Ljava/lang/String;
    if-eqz v19, :cond_5

    .line 794
    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->setAddress(Ljava/lang/String;)V

    .line 795
    add-int/lit8 v30, v30, 0x1

    .line 796
    const-string v12, "GeoTag"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "2. Set from DB : Address is within boundary(1000.0m) about location in DB.\n   Address is set from DB = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    if-eqz v31, :cond_4

    .line 799
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "2. Set from DB : Address is within boundary(1000.0m) about location in DB.\n   Address is set from DB = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/tag/LogFileManager;->writeLog(Ljava/lang/String;)V

    .line 869
    .end local v19    # "address":Ljava/lang/String;
    :cond_4
    :goto_2
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->clone()Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;

    move-object/from16 v29, v0

    goto/16 :goto_1

    .line 804
    .restart local v19    # "address":Ljava/lang/String;
    :cond_5
    const/16 v16, 0x1

    move-wide v12, v6

    move-wide v14, v8

    invoke-virtual/range {v11 .. v16}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v20

    .line 805
    .local v20, "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v20, :cond_c

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_c

    .line 806
    const/4 v12, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/location/Address;

    .line 807
    .local v18, "addr":Landroid/location/Address;
    if-eqz v18, :cond_6

    .line 808
    invoke-virtual/range {v18 .. v18}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v27

    .line 809
    .local v27, "loc":Ljava/lang/String;
    if-eqz v27, :cond_8

    .line 810
    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 814
    :goto_3
    const/16 v12, 0x7c

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 816
    invoke-virtual/range {v18 .. v18}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v27

    .line 817
    if-eqz v27, :cond_9

    .line 818
    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 822
    :goto_4
    const/16 v12, 0x7c

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 824
    invoke-virtual/range {v18 .. v18}, Landroid/location/Address;->getSubAdminArea()Ljava/lang/String;

    move-result-object v27

    .line 825
    if-eqz v27, :cond_a

    .line 826
    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 830
    :goto_5
    const/16 v12, 0x7c

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 832
    invoke-virtual/range {v18 .. v18}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v27

    .line 833
    if-eqz v27, :cond_b

    .line 834
    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 838
    :goto_6
    const/16 v12, 0x7c

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 840
    .end local v27    # "loc":Ljava/lang/String;
    :cond_6
    const-string v12, "GeoTag"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "3. GeoCoder request\n   Address from GeoCoder = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    if-eqz v31, :cond_7

    .line 843
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "3. GeoCoder request\n   Address from GeoCoder = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/tag/LogFileManager;->writeLog(Ljava/lang/String;)V

    .line 847
    :cond_7
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->setAddress(Ljava/lang/String;)V

    .line 848
    add-int/lit8 v30, v30, 0x1

    .line 849
    goto/16 :goto_2

    .line 812
    .restart local v27    # "loc":Ljava/lang/String;
    :cond_8
    const-string v12, " "

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_3

    .line 870
    .end local v6    # "lat":D
    .end local v8    # "logi":D
    .end local v18    # "addr":Landroid/location/Address;
    .end local v19    # "address":Ljava/lang/String;
    .end local v20    # "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v22    # "distance":F
    .end local v27    # "loc":Ljava/lang/String;
    :catch_0
    move-exception v23

    .line 871
    .local v23, "ex":Ljava/io/IOException;
    const-string v12, "GeoTag"

    const-string v13, "Geocoder exception: "

    move-object/from16 v0, v23

    invoke-static {v12, v13, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 820
    .end local v23    # "ex":Ljava/io/IOException;
    .restart local v6    # "lat":D
    .restart local v8    # "logi":D
    .restart local v18    # "addr":Landroid/location/Address;
    .restart local v19    # "address":Ljava/lang/String;
    .restart local v20    # "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .restart local v22    # "distance":F
    .restart local v27    # "loc":Ljava/lang/String;
    :cond_9
    :try_start_1
    const-string v12, " "

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_4

    .line 872
    .end local v6    # "lat":D
    .end local v8    # "logi":D
    .end local v18    # "addr":Landroid/location/Address;
    .end local v19    # "address":Ljava/lang/String;
    .end local v20    # "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v22    # "distance":F
    .end local v27    # "loc":Ljava/lang/String;
    :catch_1
    move-exception v23

    .line 873
    .local v23, "ex":Ljava/lang/RuntimeException;
    const-string v12, "GeoTag"

    const-string v13, "Geocoder exception: "

    move-object/from16 v0, v23

    invoke-static {v12, v13, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 828
    .end local v23    # "ex":Ljava/lang/RuntimeException;
    .restart local v6    # "lat":D
    .restart local v8    # "logi":D
    .restart local v18    # "addr":Landroid/location/Address;
    .restart local v19    # "address":Ljava/lang/String;
    .restart local v20    # "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .restart local v22    # "distance":F
    .restart local v27    # "loc":Ljava/lang/String;
    :cond_a
    :try_start_2
    const-string v12, " "

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_5

    .line 874
    .end local v6    # "lat":D
    .end local v8    # "logi":D
    .end local v18    # "addr":Landroid/location/Address;
    .end local v19    # "address":Ljava/lang/String;
    .end local v20    # "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v22    # "distance":F
    .end local v27    # "loc":Ljava/lang/String;
    :catch_2
    move-exception v23

    .line 875
    .local v23, "ex":Ljava/lang/Exception;
    const-string v12, "GeoTag"

    const-string v13, "Geocoder exception: "

    move-object/from16 v0, v23

    invoke-static {v12, v13, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 836
    .end local v23    # "ex":Ljava/lang/Exception;
    .restart local v6    # "lat":D
    .restart local v8    # "logi":D
    .restart local v18    # "addr":Landroid/location/Address;
    .restart local v19    # "address":Ljava/lang/String;
    .restart local v20    # "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .restart local v22    # "distance":F
    .restart local v27    # "loc":Ljava/lang/String;
    :cond_b
    :try_start_3
    const-string v12, " "

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 850
    .end local v18    # "addr":Landroid/location/Address;
    .end local v27    # "loc":Ljava/lang/String;
    :cond_c
    const-string v12, "GeoTag"

    const-string v13, "3. GeoCoder request\n   GeoCoder result = Failed"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    if-eqz v31, :cond_d

    .line 852
    const-string v12, "3. GeoCoder request\n   GeoCoder result = Failed\n"

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/tag/LogFileManager;->writeLog(Ljava/lang/String;)V

    .line 854
    :cond_d
    const-string v12, "INVALID_GPS"

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->setAddress(Ljava/lang/String;)V

    .line 855
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_2

    .line 859
    .end local v19    # "address":Ljava/lang/String;
    .end local v20    # "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_e
    const-string v12, "GeoTag"

    const-string v13, "4. Set from Memory : Address is within boundary(1000.0) of previous location.\n   Use previous result"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    if-eqz v31, :cond_f

    .line 863
    const-string v12, "4. Set from Memory : Address is within boundary(1000.0) of previous location.\n   Use previous result\n"

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/tag/LogFileManager;->writeLog(Ljava/lang/String;)V

    .line 866
    :cond_f
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->getAddress()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;->setAddress(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 867
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_2

    .line 879
    .end local v6    # "lat":D
    .end local v8    # "logi":D
    .end local v22    # "distance":F
    .end local v26    # "info":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;
    :cond_10
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "geocoderTask() convert address - success count["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, v30

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "], fail count["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    add-int v12, v30, v24

    goto/16 :goto_0
.end method

.method private getAddress(Landroid/net/Uri;DD)Ljava/lang/String;
    .locals 30
    .param p1, "targetUri"    # Landroid/net/Uri;
    .param p2, "lati"    # D
    .param p4, "longi"    # D

    .prologue
    .line 688
    const-wide v18, 0x3f7eb851eb851eb8L    # 0.0075

    .line 689
    .local v18, "VAF":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 690
    .local v2, "resolver":Landroid/content/ContentResolver;
    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "distinct latitude, longitude, addr"

    aput-object v5, v4, v3

    .line 694
    .local v4, "project":[Ljava/lang/String;
    const/16 v17, 0x0

    .line 695
    .local v17, "address":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 696
    const/4 v3, 0x0

    .line 745
    :goto_0
    return-object v3

    .line 699
    :cond_0
    const/4 v7, 0x0

    .line 701
    .local v7, "orderClause":Ljava/lang/String;
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 702
    const-string v7, "date_added DESC, _id DESC"

    .line 707
    :goto_1
    const/16 v22, 0x0

    .line 709
    .local v22, "cursor":Landroid/database/Cursor;
    const-wide v8, 0x3f7eb851eb851eb8L    # 0.0075

    sub-double v8, p2, v8

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v28

    .line 710
    .local v28, "smallerLati":Ljava/lang/String;
    const-wide v8, 0x3f7eb851eb851eb8L    # 0.0075

    add-double v8, v8, p2

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v20

    .line 711
    .local v20, "biggerLati":Ljava/lang/String;
    const-wide v8, 0x3f7eb851eb851eb8L    # 0.0075

    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    div-double/2addr v8, v10

    sub-double v8, p4, v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v29

    .line 713
    .local v29, "smallerLongi":Ljava/lang/String;
    const-wide v8, 0x3f7eb851eb851eb8L    # 0.0075

    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    div-double/2addr v8, v10

    add-double v8, v8, p4

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v21

    .line 714
    .local v21, "biggerLongi":Ljava/lang/String;
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->MEDIA_PROVIDER_FILE_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$1400()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "latitude is not null and longitude is not null and (addr is not null and addr is not \'INVALID_GPS\') and (latitude between ? and ?) and (longitude between ? and ?)"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v28, v6, v8

    const/4 v8, 0x1

    aput-object v20, v6, v8

    const/4 v8, 0x2

    aput-object v29, v6, v8

    const/4 v8, 0x3

    aput-object v21, v6, v8

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 718
    const/16 v27, 0x0

    .line 719
    .local v27, "minDiff":F
    if-eqz v22, :cond_4

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_4

    .line 720
    const/4 v3, 0x1

    new-array v0, v3, [F

    move-object/from16 v16, v0

    .line 721
    .local v16, "diff":[F
    const-wide/16 v12, 0x0

    .line 722
    .local v12, "dLati":D
    const-wide/16 v14, 0x0

    .line 723
    .local v14, "dLongi":D
    const-string v3, "latitude"

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 724
    .local v25, "idx_lati":I
    const-string v3, "longitude"

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 725
    .local v26, "idx_longi":I
    const-string v3, "addr"

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 726
    .local v24, "idx_addr":I
    :cond_1
    :goto_2
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 727
    move-object/from16 v0, v22

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v12

    .line 728
    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v14

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    .line 729
    invoke-static/range {v8 .. v16}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 730
    if-eqz v17, :cond_2

    if-eqz v17, :cond_1

    const/4 v3, 0x0

    aget v3, v16, v3

    cmpl-float v3, v27, v3

    if-lez v3, :cond_1

    .line 731
    :cond_2
    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 732
    const/4 v3, 0x0

    aget v27, v16, v3
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 704
    .end local v12    # "dLati":D
    .end local v14    # "dLongi":D
    .end local v16    # "diff":[F
    .end local v20    # "biggerLati":Ljava/lang/String;
    .end local v21    # "biggerLongi":Ljava/lang/String;
    .end local v22    # "cursor":Landroid/database/Cursor;
    .end local v24    # "idx_addr":I
    .end local v25    # "idx_lati":I
    .end local v26    # "idx_longi":I
    .end local v27    # "minDiff":F
    .end local v28    # "smallerLati":Ljava/lang/String;
    .end local v29    # "smallerLongi":Ljava/lang/String;
    :cond_3
    const-string v7, "datetaken DESC, _id DESC"

    goto/16 :goto_1

    .line 739
    .restart local v20    # "biggerLati":Ljava/lang/String;
    .restart local v21    # "biggerLongi":Ljava/lang/String;
    .restart local v22    # "cursor":Landroid/database/Cursor;
    .restart local v27    # "minDiff":F
    .restart local v28    # "smallerLati":Ljava/lang/String;
    .restart local v29    # "smallerLongi":Ljava/lang/String;
    :cond_4
    if-eqz v22, :cond_5

    .line 740
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 741
    const/16 v22, 0x0

    .end local v20    # "biggerLati":Ljava/lang/String;
    .end local v21    # "biggerLongi":Ljava/lang/String;
    .end local v27    # "minDiff":F
    .end local v28    # "smallerLati":Ljava/lang/String;
    .end local v29    # "smallerLongi":Ljava/lang/String;
    :cond_5
    :goto_3
    move-object/from16 v3, v17

    .line 745
    goto/16 :goto_0

    .line 736
    :catch_0
    move-exception v23

    .line 737
    .local v23, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual/range {v23 .. v23}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 739
    if-eqz v22, :cond_5

    .line 740
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 741
    const/16 v22, 0x0

    goto :goto_3

    .line 739
    .end local v23    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v3

    if-eqz v22, :cond_6

    .line 740
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 741
    const/16 v22, 0x0

    :cond_6
    throw v3
.end method

.method private processTask(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "task"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 646
    const/4 v3, 0x0

    .line 647
    .local v3, "updatedIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-nez p1, :cond_0

    .line 648
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v5

    const-string v6, "processTask() queryTask is null"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :goto_0
    return-object v4

    .line 652
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    .line 653
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v5

    const-string v6, "processTask() Network is disconnected"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 657
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getTargetUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getSelection()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->getData(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    invoke-static {v5, v6, v7, v8}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$1100(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 659
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_2

    .line 660
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v5

    const-string v6, "processTask() getData cursor is NULL"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 664
    :cond_2
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processTask() uri = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getTargetUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", cursor.getCount() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_3

    .line 667
    const/4 v1, 0x0

    .line 668
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;>;"
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getTargetUri()Landroid/net/Uri;

    move-result-object v5

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->convert(Landroid/net/Uri;Landroid/database/Cursor;)Ljava/util/ArrayList;
    invoke-static {v4, v5, v0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$1200(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Landroid/net/Uri;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v1

    .line 670
    if-eqz v1, :cond_3

    .line 671
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getTargetUri()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {p0, v4, v1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->geocoderTask(Landroid/net/Uri;Ljava/util/ArrayList;)I

    move-result v2

    .line 672
    .local v2, "processCount":I
    if-lez v2, :cond_3

    .line 673
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v4

    const-string v5, "processTask() geocoderTask finished!!"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getTargetUri()Landroid/net/Uri;

    move-result-object v6

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->updateResult(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    invoke-static {v4, v5, v6, v1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$1300(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 678
    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/LocationTagData;>;"
    .end local v2    # "processCount":I
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 679
    const/4 v0, 0x0

    move-object v4, v3

    .line 681
    goto/16 :goto_0
.end method

.method private requestConvertAddress(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 9
    .param p1, "requestType"    # Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 618
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 619
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "requestConvertAddress isCancelled!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    :goto_0
    return v0

    .line 623
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 624
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestConvertAddress() requestType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestConvertAddress() selection = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    if-eqz p4, :cond_2

    .line 627
    const-string v6, ""

    .line 628
    .local v6, "args":Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v0, p4

    if-ge v7, v0, :cond_1

    .line 629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, p4, v7

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 628
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 631
    :cond_1
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestConvertAddress() selectionArgs = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    .end local v6    # "args":Ljava/lang/String;
    .end local v7    # "i":I
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mRequest:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 636
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 637
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mWaiting:Z

    if-eqz v0, :cond_3

    .line 638
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 640
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mWaiting:Z

    .line 641
    monitor-exit v1

    .line 642
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 641
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private responseTagReady(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;Ljava/util/ArrayList;)V
    .locals 8
    .param p1, "task"    # Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 592
    .local p2, "updatedIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 593
    :cond_0
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "responseTagReady() updatedIdList is null or empty"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :goto_0
    return-void

    .line 597
    :cond_1
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 598
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 599
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 600
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getTargetUri()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 602
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 603
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "media_ids"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 604
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 605
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 606
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConvertAsyncTask:Finished processTask(), maxUpdatedId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mType:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v6, v2

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->setPrefsUpdateId(Ljava/lang/String;J)V
    invoke-static {v3, v4, v6, v7}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$1000(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;Ljava/lang/String;J)V

    .line 612
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mParentHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 530
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Void;
    .locals 7
    .param p1, "uris"    # [Ljava/lang/Object;

    .prologue
    .line 556
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 557
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mRequest:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 558
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 559
    :try_start_0
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mType:Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$MediaType;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " asyncTask is waiting...."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mRequest:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 561
    monitor-exit v4

    goto :goto_0

    .line 572
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 563
    :cond_1
    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mWaiting:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565
    :try_start_2
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V

    .line 566
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->isCancelled()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-eqz v3, :cond_3

    .line 567
    :try_start_3
    monitor-exit v4

    .line 588
    :cond_2
    const/4 v3, 0x0

    return-object v3

    .line 569
    :catch_0
    move-exception v0

    .line 570
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 572
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 574
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->mRequest:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;

    .line 575
    .local v1, "task":Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;
    if-eqz v1, :cond_0

    .line 579
    const/4 v2, 0x0

    .line 580
    .local v2, "updatedIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->processTask(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;)Ljava/util/ArrayList;

    move-result-object v2

    .line 582
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;->access$200()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Finished processTask(), task\'s convert Type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", target uri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;->getTargetUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    invoke-direct {p0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask;->responseTagReady(Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService$ConvertAsyncTask$ConvertTask;Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 551
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 552
    return-void
.end method
