.class public Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "FilterGroupViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$State;,
        Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;
    }
.end annotation


# static fields
.field public static final FILTER_PAGE_INDEX_CATEGORY:I = 0x0

.field public static final FILTER_PAGE_INDEX_HANDWRITING:I = 0x4

.field public static final FILTER_PAGE_INDEX_LOCATION:I = 0x3

.field public static FILTER_PAGE_INDEX_MAX:I = 0x0

.field public static final FILTER_PAGE_INDEX_TIME:I = 0x1

.field public static final FILTER_PAGE_INDEX_USERTAG:I = 0x2


# instance fields
.field private mAnchorView:Landroid/view/View;

.field private mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

.field private mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

.field private mNeedToRestorePanel:Z

.field private mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mParentFrame:Landroid/view/View;

.field private mSPenEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x5

    sput v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->FILTER_PAGE_INDEX_MAX:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 41
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    .line 43
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    .line 45
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mAnchorView:Landroid/view/View;

    .line 47
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    .line 49
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mNeedToRestorePanel:Z

    .line 53
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mSPenEnabled:Z

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    .line 43
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    .line 45
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mAnchorView:Landroid/view/View;

    .line 47
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    .line 49
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mNeedToRestorePanel:Z

    .line 53
    iput-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mSPenEnabled:Z

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    return-object v0
.end method

.method private findViewAndSetState(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;Z)V
    .locals 2
    .param p1, "parent"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .param p2, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .param p3, "selected"    # Z

    .prologue
    .line 375
    invoke-virtual {p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->findViewByTagData(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)Landroid/view/View;

    move-result-object v0

    .line 377
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 378
    invoke-virtual {v0, p3}, Landroid/view/View;->setSelected(Z)V

    .line 380
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 381
    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "child":Landroid/view/View;
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->getFontTypeface(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 387
    :cond_0
    return-void
.end method

.method private onNotifyPageShowing(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 562
    sget v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->FILTER_PAGE_INDEX_MAX:I

    if-ge p1, v1, :cond_0

    .line 563
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 565
    .local v0, "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->onNotifyShowing()V

    .line 567
    .end local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    :cond_0
    return-void
.end method

.method private setVisibilityByCurrentItem(I)Z
    .locals 6
    .param p1, "item"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 105
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    if-eqz v4, :cond_0

    sget v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->FILTER_PAGE_INDEX_MAX:I

    if-ge p1, v4, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5, p1}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 109
    .local v1, "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->isToggleMode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 166
    .end local v1    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    :cond_0
    :goto_0
    return v2

    .line 113
    .restart local v1    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    :cond_1
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mNeedToRestorePanel:Z

    .line 115
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getCurrentItem()I

    move-result v4

    if-ne v4, p1, :cond_3

    .line 118
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->isDisplaying()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 120
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f040007

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 121
    .local v0, "ani":Landroid/view/animation/Animation;
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$1;

    invoke-direct {v4, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;)V

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 141
    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setDisplay(Z)V

    .line 147
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 148
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .end local v0    # "ani":Landroid/view/animation/Animation;
    :goto_2
    move v2, v3

    .line 163
    goto :goto_0

    .line 143
    :cond_2
    invoke-virtual {v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setDisplay(Z)V

    .line 144
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f040006

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 145
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setVisibility(I)V

    goto :goto_1

    .line 151
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :cond_3
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->isEnabledAnchorView(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 152
    invoke-virtual {v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setDisplay(Z)V

    .line 153
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f040005

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 155
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 156
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setVisibility(I)V

    goto :goto_2

    .line 158
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :cond_4
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mNeedToRestorePanel:Z

    .line 159
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setVisibility(I)V

    goto :goto_2
.end method

.method private updateAnchorView()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 330
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mAnchorView:Landroid/view/View;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mAnchorView:Landroid/view/View;

    instance-of v4, v4, Landroid/view/ViewGroup;

    if-eqz v4, :cond_5

    .line 331
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mAnchorView:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    .line 333
    .local v3, "parent":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->FILTER_PAGE_INDEX_MAX:I

    if-ge v1, v4, :cond_5

    .line 334
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v7, v1}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 339
    .local v2, "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 340
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->isToggleMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 341
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-le v4, v1, :cond_0

    .line 342
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->hasSelectedItems()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_1
    invoke-virtual {v7, v4}, Landroid/view/View;->setSelected(Z)V

    .line 333
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v4, v6

    .line 342
    goto :goto_1

    .line 346
    :cond_2
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v6

    .line 350
    .local v0, "enabled":Z
    :goto_3
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 352
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getCurrentItem()I

    move-result v4

    if-ne v4, v1, :cond_0

    .line 353
    if-nez v0, :cond_4

    .line 354
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setSelected(Z)V

    .line 356
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 357
    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setVisibility(I)V

    .line 359
    iput-boolean v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mNeedToRestorePanel:Z

    goto :goto_2

    .end local v0    # "enabled":Z
    :cond_3
    move v0, v5

    .line 346
    goto :goto_3

    .line 361
    .restart local v0    # "enabled":Z
    :cond_4
    iget-boolean v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mNeedToRestorePanel:Z

    if-eqz v4, :cond_0

    .line 362
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setSelected(Z)V

    .line 363
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setVisibility(I)V

    .line 365
    iput-boolean v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mNeedToRestorePanel:Z

    goto :goto_2

    .line 372
    .end local v0    # "enabled":Z
    .end local v1    # "i":I
    .end local v2    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v3    # "parent":Landroid/view/ViewGroup;
    :cond_5
    return-void
.end method


# virtual methods
.method public getTagDataList(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592
    packed-switch p1, :pswitch_data_0

    .line 597
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 595
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 592
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getVisibility()I
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    .line 395
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->getVisibility()I

    move-result v0

    goto :goto_0
.end method

.method public init(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;)V
    .locals 2
    .param p1, "callback"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.spen_usp"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mSPenEnabled:Z

    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mSPenEnabled:Z

    if-nez v0, :cond_0

    .line 68
    const/4 v0, 0x4

    sput v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->FILTER_PAGE_INDEX_MAX:I

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    if-nez v0, :cond_1

    .line 72
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;

    .line 74
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    .line 76
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$FilterPagerAdapter;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    .line 80
    return-void
.end method

.method public isEnabledAnchorView(I)Z
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 570
    const/4 v1, 0x0

    .line 572
    .local v1, "enabled":Z
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mAnchorView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mAnchorView:Landroid/view/View;

    instance-of v3, v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 573
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mAnchorView:Landroid/view/View;

    check-cast v2, Landroid/view/ViewGroup;

    .line 575
    .local v2, "parent":Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-le v3, p1, :cond_0

    .line 576
    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 578
    .local v0, "anchor":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v1

    .line 582
    .end local v0    # "anchor":Landroid/view/View;
    .end local v2    # "parent":Landroid/view/ViewGroup;
    :cond_0
    return v1
.end method

.method public isExistingPager(I)Z
    .locals 3
    .param p1, "item"    # I

    .prologue
    .line 409
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 411
    .local v0, "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->isDisplaying()Z

    move-result v1

    return v1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 588
    const/4 v0, 0x0

    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 531
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/support/v4/view/ViewPager;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 532
    return-void
.end method

.method public reloadAllItems()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 213
    const/4 v0, 0x0

    .line 216
    .local v0, "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 218
    .restart local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 221
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 222
    .restart local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 225
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    invoke-virtual {v1, v3, v5}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 227
    .restart local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    invoke-virtual {v1, v5}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 230
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    invoke-virtual {v1, v3, v6}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 232
    .restart local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    invoke-virtual {v1, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 234
    iget-boolean v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mSPenEnabled:Z

    if-eqz v1, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 238
    .restart local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mLoader:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;->getItems(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 242
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->updateAnchorView()V

    .line 243
    return-void
.end method

.method public setAnchorView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mAnchorView:Landroid/view/View;

    .line 84
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 0
    .param p1, "item"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setVisibilityByCurrentItem(I)Z

    .line 99
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 101
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->onNotifyPageShowing(I)V

    .line 102
    return-void
.end method

.method public setCurrentItem(IZ)V
    .locals 1
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setVisibilityByCurrentItem(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 92
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->onNotifyPageShowing(I)V

    .line 93
    return-void
.end method

.method public setCustomFilterItems(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    const/4 v7, 0x0

    .line 188
    if-eqz p1, :cond_3

    .line 189
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 190
    .local v4, "userItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v2, "locationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 193
    .local v0, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 194
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 195
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isUserTag()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 196
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 201
    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v7, v6}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 203
    .local v3, "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 206
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v7, v6}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 208
    .restart local v3    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v3, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 210
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "locationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    .end local v3    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v4    # "userItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    :cond_3
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    .line 536
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 538
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)V

    invoke-super {p0, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 559
    return-void
.end method

.method public setState(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    const/4 v3, 0x0

    .line 171
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    .line 174
    .local v0, "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;
    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->setState(I)V

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;

    .line 182
    .restart local v0    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;
    if-eqz v0, :cond_1

    .line 183
    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->setState(I)V

    .line 185
    :cond_1
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 400
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 406
    :goto_0
    return-void

    .line 405
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateItemState(Ljava/util/ArrayList;Z)V
    .locals 17
    .param p2, "selected"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    if-eqz p1, :cond_a

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_a

    .line 247
    const/4 v13, 0x0

    .line 248
    .local v13, "selectedTimeFilter":Z
    const/4 v12, 0x0

    .line 250
    .local v12, "selectedHelpCategory":Z
    const/4 v7, 0x0

    .local v7, "pageCategory":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    const/4 v10, 0x0

    .local v10, "pageTime":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    const/4 v11, 0x0

    .local v11, "pageUsertag":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    const/4 v9, 0x0

    .local v9, "pageLocation":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    const/4 v8, 0x0

    .line 252
    .local v8, "pageHandwriting":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "pageCategory":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v7, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 254
    .restart local v7    # "pageCategory":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setUnselectedState()V

    .line 255
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "pageTime":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v10, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 257
    .restart local v10    # "pageTime":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v10}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setUnselectedState()V

    .line 258
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x2

    invoke-virtual/range {v14 .. v16}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "pageUsertag":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v11, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 260
    .restart local v11    # "pageUsertag":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setUnselectedState()V

    .line 261
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x3

    invoke-virtual/range {v14 .. v16}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "pageLocation":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v9, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 263
    .restart local v9    # "pageLocation":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setUnselectedState()V

    .line 265
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mSPenEnabled:Z

    if-eqz v14, :cond_0

    .line 266
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x4

    invoke-virtual/range {v14 .. v16}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "pageHandwriting":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v8, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 268
    .restart local v8    # "pageHandwriting":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setUnselectedState()V

    .line 271
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 272
    .local v3, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isCategoryTag()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 274
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v7, v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->findViewAndSetState(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;Z)V

    .line 276
    if-eqz p2, :cond_2

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->supportTimeTag()Z

    move-result v14

    if-nez v14, :cond_2

    .line 277
    const/4 v12, 0x1

    .line 281
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mSPenEnabled:Z

    if-eqz v14, :cond_3

    .line 282
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isFilterTag()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 284
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v8, v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->findViewAndSetState(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;Z)V

    .line 288
    :cond_3
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isTimeTag()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 290
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v10, v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->findViewAndSetState(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;Z)V

    .line 292
    if-eqz p2, :cond_4

    .line 293
    const/4 v13, 0x1

    .line 297
    :cond_4
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isUserTag()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 299
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v11, v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->findViewAndSetState(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;Z)V

    .line 302
    :cond_5
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 304
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v9, v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->findViewAndSetState(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;Z)V

    goto :goto_0

    .line 310
    .end local v3    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_6
    if-eqz v12, :cond_8

    const/4 v14, 0x0

    :goto_1
    invoke-virtual {v10, v14}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setItemState(Z)V

    .line 314
    if-eqz v13, :cond_9

    const/4 v14, 0x0

    :goto_2
    invoke-virtual {v7, v14}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setItemStateUnrelatedTimeFilter(Z)V

    .line 326
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "pageCategory":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v8    # "pageHandwriting":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v9    # "pageLocation":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v10    # "pageTime":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v11    # "pageUsertag":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v12    # "selectedHelpCategory":Z
    .end local v13    # "selectedTimeFilter":Z
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->updateAnchorView()V

    .line 327
    return-void

    .line 310
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v7    # "pageCategory":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .restart local v8    # "pageHandwriting":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .restart local v9    # "pageLocation":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .restart local v10    # "pageTime":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .restart local v11    # "pageUsertag":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .restart local v12    # "selectedHelpCategory":Z
    .restart local v13    # "selectedTimeFilter":Z
    :cond_8
    const/4 v14, 0x1

    goto :goto_1

    .line 314
    :cond_9
    const/4 v14, 0x1

    goto :goto_2

    .line 316
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "pageCategory":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v8    # "pageHandwriting":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v9    # "pageLocation":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v10    # "pageTime":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v11    # "pageUsertag":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    .end local v12    # "selectedHelpCategory":Z
    .end local v13    # "selectedTimeFilter":Z
    :cond_a
    const/4 v6, 0x0

    .line 317
    .local v6, "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v14

    invoke-virtual {v14}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v2

    .line 319
    .local v2, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    if-ge v4, v2, :cond_7

    .line 320
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15, v4}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    check-cast v6, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;

    .line 321
    .restart local v6    # "page":Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setUnselectedState()V

    .line 319
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method
