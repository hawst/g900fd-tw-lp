.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListMusicAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 22
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v11, 0x0

    .line 26
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v5

    .line 27
    .local v5, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v3, 0x0

    .line 29
    .local v3, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;
    invoke-virtual {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 31
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v6

    .line 33
    .local v6, "subTitle":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 34
    sget-object v8, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f030040

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 36
    new-instance v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;

    .end local v3    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;
    invoke-direct {v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;-><init>()V

    .line 38
    .restart local v3    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;
    const v8, 0x7f0b0078

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 39
    const v8, 0x7f0b0079

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    .line 40
    const v8, 0x7f0b004b

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    .line 41
    const v8, 0x7f0b0045

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    .line 43
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    :goto_0
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "album":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "basic":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v4

    .line 54
    .local v4, "intent":Landroid/content/Intent;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v7

    .line 55
    .local v7, "title":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "desc":Ljava/lang/String;
    iget-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v9, 0x1

    invoke-virtual {p0, v8, v7, v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 58
    iget-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    invoke-virtual {p0, v8, v2, v11}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 60
    iget-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 63
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 64
    iget-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    invoke-virtual {p0, v8, p1, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;->setSubHeaderView(Landroid/view/ViewGroup;ILjava/lang/String;)V

    .line 67
    :cond_0
    iget-object v8, v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    const/4 v9, -0x1

    invoke-virtual {p0, v8, v0, v9, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;ILjava/lang/String;)V

    .line 69
    invoke-virtual {p0, p2, v4, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    return-object v8

    .line 46
    .end local v0    # "album":Ljava/lang/String;
    .end local v1    # "basic":Ljava/lang/String;
    .end local v2    # "desc":Ljava/lang/String;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v7    # "title":Ljava/lang/String;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;
    check-cast v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;

    .restart local v3    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMusicAdapter$MusicItemListViewHolder;
    goto :goto_0
.end method
