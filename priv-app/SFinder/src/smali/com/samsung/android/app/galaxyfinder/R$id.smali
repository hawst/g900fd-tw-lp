.class public final Lcom/samsung/android/app/galaxyfinder/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final app_icon:I = 0x7f0b00b4

.field public static final app_name:I = 0x7f0b00b5

.field public static final base_layout:I = 0x7f0b00c6

.field public static final btn_delete:I = 0x7f0b003f

.field public static final btn_history_delete_all:I = 0x7f0b003d

.field public static final bubble:I = 0x7f0b0074

.field public static final button_show_more:I = 0x7f0b0019

.field public static final button_voice_input_cancel:I = 0x7f0b000f

.field public static final button_voice_input_retry:I = 0x7f0b0011

.field public static final category_container:I = 0x7f0b0012

.field public static final category_icon:I = 0x7f0b00c9

.field public static final category_name:I = 0x7f0b00ca

.field public static final category_selection_indicator:I = 0x7f0b00c8

.field public static final category_tag_cloud_view:I = 0x7f0b00d8

.field public static final category_title_layout:I = 0x7f0b0015

.field public static final checkBox:I = 0x7f0b0016

.field public static final check_box:I = 0x7f0b00cb

.field public static final check_warning_show_again:I = 0x7f0b00b3

.field public static final check_warning_show_again_data:I = 0x7f0b00ae

.field public static final checkbox_layout:I = 0x7f0b00b2

.field public static final checkbox_layout_data:I = 0x7f0b00ad

.field public static final colorBar:I = 0x7f0b0098

.field public static final content_container:I = 0x7f0b0091

.field public static final content_new:I = 0x7f0b00cc

.field public static final cpname:I = 0x7f0b00a8

.field public static final custom_actionbar:I = 0x7f0b001a

.field public static final custom_done:I = 0x7f0b001b

.field public static final custom_title:I = 0x7f0b001c

.field public static final description:I = 0x7f0b0092

.field public static final deselect_all:I = 0x7f0b00e3

.field public static final distance_text:I = 0x7f0b00b0

.field public static final distance_text_data:I = 0x7f0b00ab

.field public static final divider:I = 0x7f0b00d6

.field public static final divider_voice_input_bottom:I = 0x7f0b0010

.field public static final duration_time:I = 0x7f0b00a3

.field public static final edit_group:I = 0x7f0b001d

.field public static final edit_search:I = 0x7f0b001f

.field public static final edit_search_frame:I = 0x7f0b001e

.field public static final empty_history:I = 0x7f0b00c0

.field public static final expand_fully_layout:I = 0x7f0b0013

.field public static final filter_details:I = 0x7f0b0029

.field public static final filter_group_category:I = 0x7f0b0000

.field public static final filter_group_handwriting:I = 0x7f0b0001

.field public static final filter_group_location:I = 0x7f0b0002

.field public static final filter_group_pager:I = 0x7f0b002a

.field public static final filter_group_time:I = 0x7f0b0003

.field public static final filter_group_usertag:I = 0x7f0b0004

.field public static final filter_groups:I = 0x7f0b0028

.field public static final filter_lists:I = 0x7f0b0026

.field public static final filter_panel:I = 0x7f0b00ba

.field public static final filter_title:I = 0x7f0b00d7

.field public static final gallery_area:I = 0x7f0b0061

.field public static final gallery_date_text:I = 0x7f0b006b

.field public static final gallery_desc_area:I = 0x7f0b0067

.field public static final gallery_ic_area:I = 0x7f0b0064

.field public static final gallery_location_text:I = 0x7f0b0069

.field public static final gallery_note_icon:I = 0x7f0b0065

.field public static final gallery_people_text:I = 0x7f0b006a

.field public static final gallery_thumb:I = 0x7f0b0062

.field public static final gallery_weather_text:I = 0x7f0b0068

.field public static final guide_bubble:I = 0x7f0b0030

.field public static final guide_bubble_arrow:I = 0x7f0b003c

.field public static final guide_bubble_layout:I = 0x7f0b003a

.field public static final guide_bubble_summary:I = 0x7f0b0031

.field public static final guide_hand_pointer:I = 0x7f0b003b

.field public static final guide_popup_do_not_show_again:I = 0x7f0b0032

.field public static final guide_punch:I = 0x7f0b0039

.field public static final guide_view_container:I = 0x7f0b0033

.field public static final help_layout:I = 0x7f0b00c3

.field public static final help_start_now:I = 0x7f0b0034

.field public static final help_start_now_01:I = 0x7f0b0035

.field public static final help_start_now_02:I = 0x7f0b0036

.field public static final help_start_now_03:I = 0x7f0b0037

.field public static final help_start_now_04:I = 0x7f0b0038

.field public static final hint:I = 0x7f0b0025

.field public static final history_container:I = 0x7f0b00b9

.field public static final history_layout:I = 0x7f0b003e

.field public static final history_scroll_view:I = 0x7f0b00b8

.field public static final history_tag:I = 0x7f0b0042

.field public static final ic_cloud:I = 0x7f0b0086

.field public static final ic_favorite:I = 0x7f0b0085

.field public static final ic_folder:I = 0x7f0b0089

.field public static final ic_lock:I = 0x7f0b0087

.field public static final ic_message_attachment:I = 0x7f0b0073

.field public static final ic_record:I = 0x7f0b0084

.field public static final ic_tag:I = 0x7f0b0083

.field public static final icon:I = 0x7f0b004b

.field public static final icon_alarm:I = 0x7f0b0096

.field public static final icon_group:I = 0x7f0b0095

.field public static final icon_helper:I = 0x7f0b00c4

.field public static final icon_overlay:I = 0x7f0b009a

.field public static final icon_second:I = 0x7f0b0097

.field public static final image:I = 0x7f0b008f

.field public static final image_area:I = 0x7f0b0099

.field public static final image_btn_clear:I = 0x7f0b0022

.field public static final image_btn_mic:I = 0x7f0b0021

.field public static final image_button_voice_input_microphone:I = 0x7f0b000e

.field public static final image_divider:I = 0x7f0b005c

.field public static final image_divider_history:I = 0x7f0b0041

.field public static final image_divider_page:I = 0x7f0b0072

.field public static final image_lock:I = 0x7f0b0071

.field public static final image_prev:I = 0x7f0b006e

.field public static final image_tag:I = 0x7f0b0044

.field public static final image_thumb:I = 0x7f0b006d

.field public static final image_view_voice_input_animation_listening:I = 0x7f0b000a

.field public static final image_view_voice_input_animation_recognizing:I = 0x7f0b000b

.field public static final image_view_voice_input_microphone:I = 0x7f0b000d

.field public static final image_view_voice_input_microphone_circle:I = 0x7f0b000c

.field public static final item_attach:I = 0x7f0b005e

.field public static final item_body:I = 0x7f0b0052

.field public static final item_body_frame:I = 0x7f0b0051

.field public static final item_color_bar:I = 0x7f0b00a7

.field public static final item_container:I = 0x7f0b00c7

.field public static final item_content:I = 0x7f0b007d

.field public static final item_control_checkbox:I = 0x7f0b009e

.field public static final item_control_link:I = 0x7f0b009f

.field public static final item_control_switch:I = 0x7f0b009d

.field public static final item_date:I = 0x7f0b0056

.field public static final item_desc:I = 0x7f0b005d

.field public static final item_fail:I = 0x7f0b0076

.field public static final item_failed:I = 0x7f0b0075

.field public static final item_icon:I = 0x7f0b004e

.field public static final item_icon_call_state:I = 0x7f0b008a

.field public static final item_icon_call_type:I = 0x7f0b008b

.field public static final item_icon_layout:I = 0x7f0b007a

.field public static final item_icon_overlay:I = 0x7f0b007b

.field public static final item_icon_reply:I = 0x7f0b005b

.field public static final item_icon_reply_layout:I = 0x7f0b005a

.field public static final item_info:I = 0x7f0b0055

.field public static final item_main_layout:I = 0x7f0b0058

.field public static final item_memo:I = 0x7f0b00a6

.field public static final item_menu_desc:I = 0x7f0b00a1

.field public static final item_menu_icon:I = 0x7f0b009c

.field public static final item_menu_name:I = 0x7f0b00a0

.field public static final item_message:I = 0x7f0b0057

.field public static final item_name:I = 0x7f0b0054

.field public static final item_number:I = 0x7f0b0059

.field public static final item_photo:I = 0x7f0b0053

.field public static final item_sender:I = 0x7f0b0060

.field public static final item_subject:I = 0x7f0b005f

.field public static final item_text_area:I = 0x7f0b0077

.field public static final item_text_group:I = 0x7f0b007c

.field public static final item_time:I = 0x7f0b008c

.field public static final item_title:I = 0x7f0b004f

.field public static final item_url:I = 0x7f0b0050

.field public static final layout_voice_input_effect_pane:I = 0x7f0b0008

.field public static final layout_voice_input_popup:I = 0x7f0b0007

.field public static final list_frame:I = 0x7f0b0048

.field public static final list_history:I = 0x7f0b00bb

.field public static final list_item_section_text:I = 0x7f0b004a

.field public static final main_area:I = 0x7f0b008d

.field public static final main_group:I = 0x7f0b004d

.field public static final menu_cancel:I = 0x7f0b00e4

.field public static final menu_done:I = 0x7f0b00e1

.field public static final menu_help:I = 0x7f0b00e0

.field public static final menu_refresh:I = 0x7f0b00de

.field public static final menu_select:I = 0x7f0b00dd

.field public static final menu_settings:I = 0x7f0b00df

.field public static final menu_share:I = 0x7f0b00e5

.field public static final no_results:I = 0x7f0b00bf

.field public static final outline:I = 0x7f0b007f

.field public static final personal_icon:I = 0x7f0b0066

.field public static final progress:I = 0x7f0b0024

.field public static final remove:I = 0x7f0b002b

.field public static final scrap_touch_intercept_overlay:I = 0x7f0b0090

.field public static final scroll_container:I = 0x7f0b00bd

.field public static final scroll_frame_adapter_single_ui:I = 0x7f0b00c2

.field public static final search_list_layout:I = 0x7f0b00be

.field public static final search_list_page:I = 0x7f0b00b6

.field public static final search_list_scroll_view:I = 0x7f0b00bc

.field public static final search_progress:I = 0x7f0b0020

.field public static final searchlist_root_layout:I = 0x7f0b00b7

.field public static final select_all:I = 0x7f0b00e2

.field public static final select_filter_checkBox:I = 0x7f0b00ce

.field public static final select_filter_icon:I = 0x7f0b00cd

.field public static final select_filter_label:I = 0x7f0b00cf

.field public static final selected_droplist_button:I = 0x7f0b0005

.field public static final selected_filter_items:I = 0x7f0b0027

.field public static final settings_list_divider:I = 0x7f0b0049

.field public static final shadow:I = 0x7f0b007e

.field public static final sort_title:I = 0x7f0b0047

.field public static final sort_title_layout:I = 0x7f0b0045

.field public static final story_album_bg:I = 0x7f0b00a2

.field public static final stroke:I = 0x7f0b0088

.field public static final symbol_divider_image:I = 0x7f0b00d3

.field public static final symbol_help_image:I = 0x7f0b00d1

.field public static final symbol_help_layout:I = 0x7f0b00d0

.field public static final symbol_help_string:I = 0x7f0b00d2

.field public static final tag_cloud_scroll_view:I = 0x7f0b00dc

.field public static final tag_container:I = 0x7f0b00db

.field public static final tag_list_expand_button:I = 0x7f0b002f

.field public static final tag_list_icon:I = 0x7f0b002d

.field public static final tag_list_layout:I = 0x7f0b00d5

.field public static final tag_list_scrollview:I = 0x7f0b00d4

.field public static final tag_list_title:I = 0x7f0b002e

.field public static final tag_list_title_layout:I = 0x7f0b002c

.field public static final tagcloud_group:I = 0x7f0b00c1

.field public static final task_checkbox:I = 0x7f0b0093

.field public static final text:I = 0x7f0b004c

.field public static final text1:I = 0x7f0b009b

.field public static final textDesc:I = 0x7f0b0079

.field public static final textTitle:I = 0x7f0b0078

.field public static final text_category_title:I = 0x7f0b0017

.field public static final text_count:I = 0x7f0b0018

.field public static final text_date:I = 0x7f0b0043

.field public static final text_desc:I = 0x7f0b006c

.field public static final text_expand_fully:I = 0x7f0b0014

.field public static final text_folder_name:I = 0x7f0b0081

.field public static final text_group:I = 0x7f0b006f

.field public static final text_helper:I = 0x7f0b00c5

.field public static final text_name:I = 0x7f0b0040

.field public static final text_num_book:I = 0x7f0b0082

.field public static final text_pinall:I = 0x7f0b008e

.field public static final text_time:I = 0x7f0b0094

.field public static final text_title:I = 0x7f0b0070

.field public static final text_transparent_title:I = 0x7f0b0080

.field public static final text_view_voice_input_message:I = 0x7f0b0009

.field public static final thumb:I = 0x7f0b00a9

.field public static final time_tag_cloud_view:I = 0x7f0b00d9

.field public static final title:I = 0x7f0b0023

.field public static final underline:I = 0x7f0b0046

.field public static final user_tag_cloud_view:I = 0x7f0b00da

.field public static final video_thumb:I = 0x7f0b00a4

.field public static final video_touch_intercept_overlay:I = 0x7f0b0063

.field public static final vnote_body:I = 0x7f0b00a5

.field public static final warning_image:I = 0x7f0b00af

.field public static final warning_image_data:I = 0x7f0b00aa

.field public static final warning_text:I = 0x7f0b00b1

.field public static final warning_text_data:I = 0x7f0b00ac

.field public static final webview:I = 0x7f0b0006


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
