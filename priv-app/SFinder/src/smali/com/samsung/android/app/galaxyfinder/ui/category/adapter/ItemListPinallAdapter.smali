.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListPinallAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;
    }
.end annotation


# static fields
.field static final CURVED_IMAGE:I = 0x0

.field static final RECTANGULAR_IMAGE:I = 0x1


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 27
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 31
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v9

    .line 32
    .local v9, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v6, 0x0

    .line 34
    .local v6, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 36
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v7

    .line 38
    .local v7, "icon":Ljava/lang/String;
    if-nez p2, :cond_2

    .line 39
    sget-object v13, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v14, 0x7f030044

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 41
    new-instance v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;

    .end local v6    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;
    invoke-direct {v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;-><init>()V

    .line 43
    .restart local v6    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;
    const v13, 0x7f0b0070

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    iput-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 44
    const v13, 0x7f0b008f

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    iput-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 45
    const v13, 0x7f0b008e

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    iput-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->tvDescription:Landroid/widget/TextView;

    .line 47
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 52
    :goto_0
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v12

    .line 53
    .local v12, "title":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v11

    .line 54
    .local v11, "textDescription":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v3

    .line 56
    .local v3, "date":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v5

    .line 57
    .local v5, "extraFlags":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v8

    .line 59
    .local v8, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v12, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->setGridViewDesc(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const/4 v10, 0x0

    .line 65
    .local v10, "shapeType":I
    if-nez v7, :cond_3

    .line 66
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 67
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->tvDescription:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090029

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 69
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->tvDescription:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 70
    if-eqz v11, :cond_0

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_0

    .line 71
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->tvDescription:Landroid/widget/TextView;

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v11, v14}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 73
    :cond_0
    if-eqz v12, :cond_1

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_1

    .line 74
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12, v14}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 103
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-virtual {v0, v1, v8, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 105
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    return-object v13

    .line 49
    .end local v3    # "date":Ljava/lang/String;
    .end local v5    # "extraFlags":Ljava/lang/String;
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v10    # "shapeType":I
    .end local v11    # "textDescription":Ljava/lang/String;
    .end local v12    # "title":Ljava/lang/String;
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;
    check-cast v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;

    .restart local v6    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;
    goto :goto_0

    .line 80
    .restart local v3    # "date":Ljava/lang/String;
    .restart local v5    # "extraFlags":Ljava/lang/String;
    .restart local v8    # "intent":Landroid/content/Intent;
    .restart local v10    # "shapeType":I
    .restart local v11    # "textDescription":Ljava/lang/String;
    .restart local v12    # "title":Ljava/lang/String;
    :cond_3
    if-eqz v12, :cond_4

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_4

    .line 81
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12, v14}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 85
    :cond_4
    if-eqz v5, :cond_1

    .line 87
    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 89
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    if-eqz v13, :cond_1

    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    if-nez v13, :cond_1

    .line 90
    const/4 v13, 0x1

    if-ne v10, v13, :cond_5

    .line 91
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    sget-object v14, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 92
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/4 v14, 0x0

    const/4 v15, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v7, v14, v15}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 97
    :catch_0
    move-exception v4

    .line 98
    .local v4, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v4}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 94
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    :cond_5
    :try_start_1
    iget-object v13, v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter$PinallItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/16 v14, 0xca

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v7, v14}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPinallAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
