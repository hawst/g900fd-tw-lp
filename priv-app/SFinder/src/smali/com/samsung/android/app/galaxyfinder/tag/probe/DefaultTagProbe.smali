.class public Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;
.super Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;
.source "DefaultTagProbe.java"


# static fields
.field private static final mTakeAddressLevel:[Z


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0x9

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mTakeAddressLevel:[Z

    return-void

    :array_0
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagType"    # [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .param p3, "attr"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;-><init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V

    .line 29
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->getLogTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    .line 39
    return-void
.end method

.method private isValidTagType(I)Z
    .locals 2
    .param p1, "tagType"    # I

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mSupportTagList:Ljava/util/ArrayList;

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->values()[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    const/4 v0, 0x1

    .line 264
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadAppTag(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 21
    .param p1, "appName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 269
    .local v16, "storedTagMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getAllContentTagData(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 270
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "loadAppTag() getAllContentTagData size ["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v18

    if-lez v18, :cond_1

    .line 273
    const-string v18, "cid"

    move-object/from16 v0, v18

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 274
    .local v11, "idx_cid":I
    const-string v18, "tid"

    move-object/from16 v0, v18

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 275
    .local v15, "idx_tid":I
    const-string v18, "type"

    move-object/from16 v0, v18

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 276
    .local v14, "idx_tagType":I
    const-string v18, "data"

    move-object/from16 v0, v18

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 277
    .local v13, "idx_tagData":I
    const-string v18, "contenturi"

    move-object/from16 v0, v18

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 279
    .local v12, "idx_contentId":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 280
    .local v10, "hashKeyBuilder":Ljava/lang/StringBuilder;
    :cond_0
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 281
    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 282
    .local v3, "id":Ljava/lang/String;
    invoke-interface {v8, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 283
    .local v4, "tid":Ljava/lang/String;
    invoke-interface {v8, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 284
    .local v17, "tagType":Ljava/lang/String;
    invoke-interface {v8, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 285
    .local v6, "tagData":Ljava/lang/String;
    invoke-interface {v8, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 287
    .local v7, "contentId":Ljava/lang/String;
    :try_start_0
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 288
    .local v5, "type":I
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->isValidTagType(I)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 291
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 292
    .local v2, "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    invoke-static {v7}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->getID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x7c

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x7c

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 297
    .end local v2    # "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v5    # "type":I
    :catch_0
    move-exception v9

    .line 298
    .local v9, "e":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "invalid tag type ("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ")"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 302
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "tid":Ljava/lang/String;
    .end local v6    # "tagData":Ljava/lang/String;
    .end local v7    # "contentId":Ljava/lang/String;
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    .end local v10    # "hashKeyBuilder":Ljava/lang/StringBuilder;
    .end local v11    # "idx_cid":I
    .end local v12    # "idx_contentId":I
    .end local v13    # "idx_tagData":I
    .end local v14    # "idx_tagType":I
    .end local v15    # "idx_tid":I
    .end local v17    # "tagType":Ljava/lang/String;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 303
    const/4 v8, 0x0

    .line 305
    :cond_2
    return-object v16
.end method


# virtual methods
.method public collect(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;)Ljava/lang/Object;
    .locals 35
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

    .prologue
    .line 111
    new-instance v29, Ljava/util/HashMap;

    invoke-direct/range {v29 .. v29}, Ljava/util/HashMap;-><init>()V

    .line 112
    .local v29, "tagResultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->getQueryUri()Landroid/net/Uri;

    move-result-object v3

    .line 113
    .local v3, "queryUri":Landroid/net/Uri;
    if-nez v3, :cond_1

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    const-string v4, "collect() queryUri is NULL"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_0
    :goto_0
    return-object v29

    .line 117
    :cond_1
    const/4 v13, 0x0

    .line 118
    .local v13, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 119
    .local v11, "contentUri":Ljava/lang/String;
    const/16 v30, -0x1

    .line 120
    .local v30, "tagType":I
    const/16 v16, -0x1

    .line 121
    .local v16, "encType":I
    const/16 v32, 0x0

    .line 122
    .local v32, "timestamp":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 124
    .local v26, "queryTime":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 125
    if-nez v13, :cond_5

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collect() cursor = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", No EXIST DATA ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :goto_1
    if-eqz v13, :cond_16

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_16

    .line 133
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .local v18, "hashKeyBuilder":Ljava/lang/StringBuilder;
    const/16 v17, 0x0

    .line 135
    .local v17, "hashKey":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    const-string v4, "collect() [START in query] ------------------------------------------------------"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_2
    const-string v2, "suggest_tag_content_uri"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 139
    .local v20, "idx_contentUri":I
    const-string v2, "suggest_tag_type"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 140
    .local v23, "idx_tagType":I
    const-string v2, "suggest_tag_encode"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 141
    .local v22, "idx_tagEncode":I
    const-string v2, "suggest_tag_create_time"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 142
    .local v21, "idx_tagCreate":I
    const-string v2, "suggest_tag_value"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 144
    .local v24, "idx_tagValue":I
    :cond_3
    :goto_2
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 145
    if-ltz v20, :cond_6

    .line 146
    move/from16 v0, v20

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 147
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collect() suggest_tag_content_uri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_4
    if-nez v11, :cond_6

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    const-string v4, "collect() [contentUri] contentUri is null"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 247
    .end local v17    # "hashKey":Ljava/lang/String;
    .end local v18    # "hashKeyBuilder":Ljava/lang/StringBuilder;
    .end local v20    # "idx_contentUri":I
    .end local v21    # "idx_tagCreate":I
    .end local v22    # "idx_tagEncode":I
    .end local v23    # "idx_tagType":I
    .end local v24    # "idx_tagValue":I
    :catch_0
    move-exception v28

    .line 248
    .local v28, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 252
    if-eqz v13, :cond_0

    .line 253
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 254
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 128
    .end local v28    # "se":Landroid/database/sqlite/SQLiteException;
    :cond_5
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v26, v4, v26

    .line 129
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collect() Query\'s data COUNT = ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], QueryTime = ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v26

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 249
    :catch_1
    move-exception v15

    .line 250
    .local v15, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 252
    if-eqz v13, :cond_0

    .line 253
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 254
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 155
    .end local v15    # "e":Ljava/lang/Exception;
    .restart local v17    # "hashKey":Ljava/lang/String;
    .restart local v18    # "hashKeyBuilder":Ljava/lang/StringBuilder;
    .restart local v20    # "idx_contentUri":I
    .restart local v21    # "idx_tagCreate":I
    .restart local v22    # "idx_tagEncode":I
    .restart local v23    # "idx_tagType":I
    .restart local v24    # "idx_tagValue":I
    :cond_6
    if-ltz v23, :cond_8

    .line 156
    :try_start_4
    move/from16 v0, v23

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    .line 157
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collect() suggest_tag_type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->isValidTagType(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 165
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->WEATHER:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v2

    move/from16 v0, v30

    if-eq v2, v0, :cond_3

    .line 171
    :cond_8
    if-ltz v22, :cond_9

    .line 172
    move/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 173
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 174
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collect() suggest_tag_encode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_9
    if-ltz v21, :cond_a

    .line 178
    move/from16 v0, v21

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 179
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collect() suggest_tag_create_time = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_a
    if-ltz v24, :cond_3

    .line 184
    const/4 v12, 0x0

    .line 185
    .local v12, "convertedTagValue":Ljava/lang/String;
    move/from16 v0, v24

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 186
    .local v31, "tagValue":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 187
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collect() suggest_tag_value = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_b
    if-eqz v31, :cond_c

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 190
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    const-string v4, "collect() [tagValue] tagValue is null"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 252
    .end local v12    # "convertedTagValue":Ljava/lang/String;
    .end local v17    # "hashKey":Ljava/lang/String;
    .end local v18    # "hashKeyBuilder":Ljava/lang/StringBuilder;
    .end local v20    # "idx_contentUri":I
    .end local v21    # "idx_tagCreate":I
    .end local v22    # "idx_tagEncode":I
    .end local v23    # "idx_tagType":I
    .end local v24    # "idx_tagValue":I
    .end local v31    # "tagValue":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v13, :cond_d

    .line 253
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 254
    const/4 v13, 0x0

    :cond_d
    throw v2

    .line 193
    .restart local v12    # "convertedTagValue":Ljava/lang/String;
    .restart local v17    # "hashKey":Ljava/lang/String;
    .restart local v18    # "hashKeyBuilder":Ljava/lang/StringBuilder;
    .restart local v20    # "idx_contentUri":I
    .restart local v21    # "idx_tagCreate":I
    .restart local v22    # "idx_tagEncode":I
    .restart local v23    # "idx_tagType":I
    .restart local v24    # "idx_tagValue":I
    .restart local v31    # "tagValue":Ljava/lang/String;
    :cond_e
    :try_start_5
    const-string v2, "INVALID_GPS"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getData() [tagValue] tagValue is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 198
    :cond_f
    const-string v2, "\\|"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v34

    .line 199
    .local v34, "values":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 200
    .local v8, "addressLevel":I
    move-object/from16 v10, v34

    .local v10, "arr$":[Ljava/lang/String;
    array-length v0, v10

    move/from16 v25, v0

    .local v25, "len$":I
    const/16 v19, 0x0

    .local v19, "i$":I
    move v9, v8

    .end local v8    # "addressLevel":I
    .local v9, "addressLevel":I
    :goto_3
    move/from16 v0, v19

    move/from16 v1, v25

    if-ge v0, v1, :cond_3

    aget-object v33, v10, v19

    .line 201
    .local v33, "value":Ljava/lang/String;
    const-string v2, " "

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 202
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    const-string v4, "collect() BLANK_TAG Skips"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 200
    .end local v9    # "addressLevel":I
    .restart local v8    # "addressLevel":I
    :cond_10
    :goto_4
    add-int/lit8 v19, v19, 0x1

    move v9, v8

    .end local v8    # "addressLevel":I
    .restart local v9    # "addressLevel":I
    goto :goto_3

    .line 205
    :cond_11
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v2

    move/from16 v0, v30

    if-ne v2, v0, :cond_14

    .line 206
    move-object/from16 v0, v34

    array-length v2, v0

    const/16 v4, 0x9

    if-ne v2, v4, :cond_12

    .line 207
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mTakeAddressLevel:[Z

    add-int/lit8 v8, v9, 0x1

    .end local v9    # "addressLevel":I
    .restart local v8    # "addressLevel":I
    aget-boolean v2, v2, v9

    if-nez v2, :cond_13

    .line 208
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collect() This location tag value["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] is useless address level"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .end local v8    # "addressLevel":I
    .restart local v9    # "addressLevel":I
    :cond_12
    move v8, v9

    .line 213
    .end local v9    # "addressLevel":I
    .restart local v8    # "addressLevel":I
    :cond_13
    const-string v2, "null"

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    const-string v4, "collect() NULL_TAG Skips"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .end local v8    # "addressLevel":I
    .restart local v9    # "addressLevel":I
    :cond_14
    move v8, v9

    .line 219
    .end local v9    # "addressLevel":I
    .restart local v8    # "addressLevel":I
    :cond_15
    move-object/from16 v12, v33

    .line 220
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 223
    new-instance v14, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;

    invoke-direct {v14}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;-><init>()V

    .line 224
    .local v14, "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    const-string v2, "-10"

    invoke-virtual {v14, v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setId(Ljava/lang/String;)V

    .line 225
    const-string v2, "-10"

    invoke-virtual {v14, v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setTid(Ljava/lang/String;)V

    .line 226
    invoke-virtual {v14, v11}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setContenturi(Ljava/lang/String;)V

    .line 227
    move/from16 v0, v30

    invoke-virtual {v14, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setType(I)V

    .line 228
    move/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setEncoded(I)V

    .line 229
    move-object/from16 v0, v32

    invoke-virtual {v14, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setCreateTime(Ljava/lang/String;)V

    .line 230
    invoke-virtual {v14, v12}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->setTagData(Ljava/lang/String;)V

    .line 231
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 232
    invoke-static {v11}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->getID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x7c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x7c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collect() tagResultMap : added : tag key = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " tagValue = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    move-object/from16 v0, v29

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 245
    .end local v8    # "addressLevel":I
    .end local v10    # "arr$":[Ljava/lang/String;
    .end local v12    # "convertedTagValue":Ljava/lang/String;
    .end local v14    # "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v17    # "hashKey":Ljava/lang/String;
    .end local v18    # "hashKeyBuilder":Ljava/lang/StringBuilder;
    .end local v19    # "i$":I
    .end local v20    # "idx_contentUri":I
    .end local v21    # "idx_tagCreate":I
    .end local v22    # "idx_tagEncode":I
    .end local v23    # "idx_tagType":I
    .end local v24    # "idx_tagValue":I
    .end local v25    # "len$":I
    .end local v31    # "tagValue":Ljava/lang/String;
    .end local v33    # "value":Ljava/lang/String;
    .end local v34    # "values":[Ljava/lang/String;
    :cond_16
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 246
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    const-string v4, "collect() [ END ] ------------------------------------------------------"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 252
    :cond_17
    if-eqz v13, :cond_0

    .line 253
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 254
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method protected getRequest(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;
    .locals 5
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->makeTagQueryUri(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)Landroid/net/Uri;

    move-result-object v1

    .line 60
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getSearchableInfo()Landroid/app/SearchableInfo;

    move-result-object v0

    .line 61
    .local v0, "searchable":Landroid/app/SearchableInfo;
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Ljava/lang/String;Landroid/net/Uri;)V

    return-object v2
.end method

.method protected makeTagQueryUri(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)Landroid/net/Uri;
    .locals 14
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    .prologue
    .line 68
    iget-object v11, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getSearchableInfo()Landroid/app/SearchableInfo;

    move-result-object v3

    .line 69
    .local v3, "searchableInfo":Landroid/app/SearchableInfo;
    const/4 v5, 0x0

    .line 70
    .local v5, "uri":Landroid/net/Uri;
    new-instance v10, Landroid/net/Uri$Builder;

    invoke-direct {v10}, Landroid/net/Uri$Builder;-><init>()V

    .line 71
    .local v10, "uriBuilder":Landroid/net/Uri$Builder;
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 72
    const-string v11, "content"

    invoke-virtual {v10, v11}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 73
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestPath()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 74
    new-instance v4, Ljava/util/StringTokenizer;

    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestPath()Ljava/lang/String;

    move-result-object v11

    const-string v12, "/"

    invoke-direct {v4, v11, v12}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .local v4, "st":Ljava/util/StringTokenizer;
    if-eqz v4, :cond_0

    .line 76
    :goto_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 77
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "path":Ljava/lang/String;
    invoke-virtual {v10, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 82
    .end local v2    # "path":Ljava/lang/String;
    .end local v4    # "st":Ljava/util/StringTokenizer;
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->getStartId()J

    move-result-wide v6

    .line 83
    .local v6, "start":J
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->getEndId()J

    move-result-wide v0

    .line 84
    .local v0, "end":J
    const-string v11, "search_suggest_tag_query"

    invoke-virtual {v10, v11}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 85
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 86
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;->getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v11

    sget-object v12, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v12

    if-ne v11, v12, :cond_3

    .line 87
    cmp-long v11, v6, v0

    if-lez v11, :cond_1

    .line 88
    move-wide v8, v6

    .line 89
    .local v8, "temp":J
    move-wide v6, v0

    .line 90
    move-wide v0, v8

    .line 93
    .end local v8    # "temp":J
    :cond_1
    const-wide/16 v12, 0x0

    cmp-long v11, v6, v12

    if-lez v11, :cond_2

    .line 94
    const-string v11, "startid"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 98
    :cond_2
    const-wide/16 v12, -0x1

    cmp-long v11, v0, v12

    if-eqz v11, :cond_3

    .line 99
    const-string v11, "endid"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 104
    :cond_3
    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 105
    iget-object v11, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "makeTagQueryUri() uri : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-object v5
.end method

.method protected onChangeInternal(Landroid/net/Uri;)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 44
    :try_start_0
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 45
    .local v2, "id":J
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v6}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 46
    .local v4, "selfUri":Landroid/net/Uri;
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    invoke-virtual {p1, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 47
    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->prepare(J)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 56
    .end local v2    # "id":J
    .end local v4    # "selfUri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v5

    .line 50
    .local v5, "uoe":Ljava/lang/UnsupportedOperationException;
    invoke-virtual {v5}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0

    .line 51
    .end local v5    # "uoe":Ljava/lang/UnsupportedOperationException;
    :catch_1
    move-exception v1

    .line 52
    .local v1, "nfe":Ljava/lang/NumberFormatException;
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    const-string v7, "onChangeInternal() Invalid id format"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 53
    .end local v1    # "nfe":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v0

    .line 54
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public store(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;Ljava/lang/Object;)Z
    .locals 12
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;
    .param p2, "result"    # Ljava/lang/Object;

    .prologue
    .line 310
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->getPkgName()Ljava/lang/String;

    move-result-object v0

    .line 311
    .local v0, "appName":Ljava/lang/String;
    const/4 v1, 0x1

    .line 312
    .local v1, "bResult":Z
    const/4 v2, 0x0

    .line 314
    .local v2, "bSync":Z
    instance-of v9, p2, Ljava/util/HashMap;

    if-nez v9, :cond_1

    .line 315
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    const-string v10, "store() wrong result type"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const/4 v1, 0x0

    .line 354
    .end local v1    # "bResult":Z
    :cond_0
    :goto_0
    return v1

    .line 318
    .restart local v1    # "bResult":Z
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "store() RequestType = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;->getRequestType()Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v9

    sget-object v10, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual {v10}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v10

    if-le v9, v10, :cond_2

    .line 321
    const/4 v2, 0x1

    :cond_2
    move-object v5, p2

    .line 325
    check-cast v5, Ljava/util/HashMap;

    .line 326
    .local v5, "fromApp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 327
    .local v7, "shouldDelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    if-eqz v2, :cond_5

    .line 328
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->loadAppTag(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v8

    .line 329
    .local v8, "storedTagMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 330
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 332
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 335
    :cond_4
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;

    .line 336
    .local v3, "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    if-eqz v3, :cond_3

    .line 337
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 342
    .end local v3    # "data":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "storedTagMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;>;"
    :cond_5
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 343
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "store() db operation start - fromApp.keySet() = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v9

    invoke-virtual {v9, v5, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->bulkInsert(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 350
    if-eqz v2, :cond_0

    .line 351
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DefaultTagProbe;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v9

    invoke-virtual {v9, v7, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->bulkDelete(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
