.class public Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryViewFactory;
.super Ljava/lang/Object;
.source "CategoryViewFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryViewFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryViewFactory;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createCategoryContainer(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    .line 26
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getTemplateStyle()Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "template":Ljava/lang/String;
    const-string v1, "suggest_template_default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryDefaultView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryDefaultView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    .line 77
    :goto_0
    return-object v1

    .line 30
    :cond_0
    const-string v1, "suggest_template_application"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 31
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryApplicationView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryApplicationView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto :goto_0

    .line 32
    :cond_1
    const-string v1, "suggest_template_content_music"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 33
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMusicView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto :goto_0

    .line 34
    :cond_2
    const-string v1, "suggest_template_content_video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 35
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryVideoView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryVideoView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto :goto_0

    .line 36
    :cond_3
    const-string v1, "suggest_template_content_voice"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 37
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryVNoteView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryVNoteView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto :goto_0

    .line 38
    :cond_4
    const-string v1, "suggest_template_email"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 39
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryEmailView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryEmailView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto :goto_0

    .line 40
    :cond_5
    const-string v1, "suggest_template_contact"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 41
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryContactView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryContactView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto :goto_0

    .line 42
    :cond_6
    const-string v1, "suggest_template_coverpage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 43
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryStoryalbumView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryStoryalbumView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto :goto_0

    .line 44
    :cond_7
    const-string v1, "suggest_template_ink_memo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 45
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryQiuckMemoView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryQiuckMemoView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto :goto_0

    .line 46
    :cond_8
    const-string v1, "suggest_template_history_web"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 47
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryBrowserView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryBrowserView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto :goto_0

    .line 48
    :cond_9
    const-string v1, "suggest_template_message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 49
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMessageView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMessageView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 50
    :cond_a
    const-string v1, "suggest_template_pinall"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 51
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPinallView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPinallView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 52
    :cond_b
    const-string v1, "suggest_template_content_image"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 53
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryGalleryView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryGalleryView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 54
    :cond_c
    const-string v1, "suggest_template_history_call"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 55
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPhoneView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPhoneView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 56
    :cond_d
    const-string v1, "suggest_template_content_file"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 57
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMyfilesView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 58
    :cond_e
    const-string v1, "suggest_template_planner"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 59
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryPlannerView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 60
    :cond_f
    const-string v1, "suggest_template_note"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 61
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryNoteView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryNoteView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 62
    :cond_10
    const-string v1, "suggest_template_chat"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 63
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryChatonView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 64
    :cond_11
    const-string v1, "suggest_template_settings"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 65
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategorySettingsView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategorySettingsView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 66
    :cond_12
    const-string v1, "suggest_template_content_file_cloud"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 67
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategorySamsungLinkView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategorySamsungLinkView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 68
    :cond_13
    const-string v1, "suggest_template_memo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 69
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMemoView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryMemoView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 70
    :cond_14
    const-string v1, "suggest_template_web_link"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 71
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryWebLinkView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryWebLinkView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 72
    :cond_15
    const-string v1, "suggest_template_lifetime"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 73
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0

    .line 77
    :cond_16
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryDefaultView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryDefaultView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    goto/16 :goto_0
.end method

.method public static get(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    .line 15
    invoke-static {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryViewFactory;->createCategoryContainer(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;

    move-result-object v0

    .line 17
    .local v0, "container":Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
    if-nez v0, :cond_0

    .line 18
    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryViewFactory;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported template type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getTemplateStyle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    :cond_0
    return-object v0
.end method
