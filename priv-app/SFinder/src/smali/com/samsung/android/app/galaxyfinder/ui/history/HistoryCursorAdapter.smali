.class public Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
.super Landroid/support/v4/widget/CursorAdapter;
.source "HistoryCursorAdapter.java"


# static fields
.field private static final DELETE_ALPHA_ANIM_DURATION:I = 0x10e

.field private static final DELETE_TRANS_ANIM_DURATION:I = 0x1f4

.field private static final DELETE_TRANS_ANIM_START_DELAY:I = 0x154

.field public static final DISP_MODE_FULLSCREEN:I = 0x0

.field public static final DISP_MODE_POPUP:I = 0x1

.field private static final LIMIT_OF_HISTORY_LIST:I = 0x1e


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final THRESHOLD_HISTORY_DELETE_ALL:I

.field private disabledPosition:I

.field private mDispMode:I

.field private mIsDoingAnim:Z

.field private mKeyTagValue:Ljava/lang/String;

.field private mKeyValue:Ljava/lang/String;

.field private mKeyValueTagType:I

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "requery"    # Z

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 47
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->TAG:Ljava/lang/String;

    .line 53
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->THRESHOLD_HISTORY_DELETE_ALL:I

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mDispMode:I

    .line 57
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValue:Ljava/lang/String;

    .line 59
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValueTagType:I

    .line 71
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;

    .line 73
    const/16 v0, 0x1f

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->disabledPosition:I

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mIsDoingAnim:Z

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ZILandroid/widget/ListView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "requery"    # Z
    .param p4, "mode"    # I
    .param p5, "listView"    # Landroid/widget/ListView;

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 47
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->TAG:Ljava/lang/String;

    .line 53
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->THRESHOLD_HISTORY_DELETE_ALL:I

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mDispMode:I

    .line 57
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValue:Ljava/lang/String;

    .line 59
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValueTagType:I

    .line 71
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;

    .line 73
    const/16 v0, 0x1f

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->disabledPosition:I

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mIsDoingAnim:Z

    .line 84
    iput p4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mDispMode:I

    .line 85
    iput-object p5, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mIsDoingAnim:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mIsDoingAnim:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->disabledPosition:I

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private convertToDateTimeFormat(J)Ljava/lang/String;
    .locals 9
    .param p1, "millis"    # J

    .prologue
    .line 421
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;

    invoke-direct {v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;-><init>()V

    .line 423
    .local v1, "converter":Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "date_format"

    invoke-static {v5, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 425
    .local v4, "sysDate":Ljava/lang/String;
    const/4 v3, 0x0

    .line 427
    .local v3, "dateStr":Ljava/lang/String;
    const-string v5, "dd-MM-yyyy"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 428
    const-string v3, "dd/MM"

    .line 433
    :goto_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 436
    .local v2, "dateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->today()J

    move-result-wide v6

    .line 438
    .local v6, "today":J
    cmp-long v5, p1, v6

    if-ltz v5, :cond_1

    .line 439
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    const/16 v8, 0x101

    invoke-static {v5, p1, p2, v8}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 445
    .local v0, "converted":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 430
    .end local v0    # "converted":Ljava/lang/String;
    .end local v2    # "dateFormat":Ljava/text/SimpleDateFormat;
    .end local v6    # "today":J
    :cond_0
    const-string v3, "MM/dd"

    goto :goto_0

    .line 442
    .restart local v2    # "dateFormat":Ljava/text/SimpleDateFormat;
    .restart local v6    # "today":J
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "converted":Ljava/lang/String;
    goto :goto_1
.end method

.method private getHistoryText(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "tagType"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 575
    const/4 v1, 0x0

    .line 576
    .local v1, "historyText":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0092

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p2, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 577
    .local v2, "location":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0093

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p2, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 578
    .local v3, "userdef":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0091

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p2, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 580
    .local v0, "face":Ljava/lang/String;
    if-nez p3, :cond_1

    .line 581
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 588
    :cond_0
    :goto_0
    return-object v1

    .line 582
    :cond_1
    const/4 v4, 0x3

    if-ne p3, v4, :cond_2

    .line 583
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 584
    :cond_2
    if-ne p3, v7, :cond_0

    .line 585
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getRelatedStringIndex(I)I
    .locals 5
    .param p1, "type"    # I

    .prologue
    .line 562
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0092

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 563
    .local v1, "location":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0093

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 564
    .local v2, "userdef":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0091

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 565
    .local v0, "face":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 566
    const-string v3, "%s"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 572
    :goto_0
    return v3

    .line 567
    :cond_0
    const/4 v3, 0x3

    if-ne p1, v3, :cond_1

    .line 568
    const-string v3, "%s"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 569
    :cond_1
    const/4 v3, 0x1

    if-ne p1, v3, :cond_2

    .line 570
    const-string v3, "%s"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 572
    :cond_2
    const/4 v3, -0x1

    goto :goto_0
.end method

.method private makeSpanString(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Ljava/lang/String;ZI)Landroid/text/SpannableString;
    .locals 26
    .param p1, "queryKey"    # Ljava/lang/String;
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "view"    # Landroid/widget/TextView;
    .param p4, "keyword"    # Ljava/lang/String;
    .param p5, "isRelated"    # Z
    .param p6, "type"    # I

    .prologue
    .line 449
    new-instance v15, Landroid/text/SpannableString;

    move-object/from16 v0, p2

    invoke-direct {v15, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 451
    .local v15, "spanStr":Landroid/text/SpannableString;
    if-eqz p1, :cond_9

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_9

    .line 452
    new-instance v13, Ljava/util/StringTokenizer;

    const-string v22, " "

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-direct {v13, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    .local v13, "queryKeyToken":Ljava/util/StringTokenizer;
    move-object/from16 v9, p2

    .line 455
    .local v9, "inputStr":Ljava/lang/String;
    const/4 v4, 0x0

    .line 457
    .local v4, "addOffset":I
    :cond_0
    :goto_0
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v22

    if-eqz v22, :cond_4

    .line 459
    :try_start_0
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v11

    .line 462
    .local v11, "keyStr":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v0, v9, v11}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->getPrefixForIndian(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 464
    .local v10, "key":Ljava/lang/String;
    if-nez v10, :cond_1

    .line 465
    move-object v10, v11

    .line 468
    :cond_1
    const/16 v17, 0x0

    .line 469
    .local v17, "startOffset":I
    const/4 v7, 0x0

    .line 471
    .local v7, "endOffset":I
    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    .line 472
    .local v12, "lowserStr":Ljava/lang/String;
    move-object/from16 v21, v9

    .line 474
    .local v21, "upperStr":Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v22

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_2

    .line 475
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    .line 476
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v22

    add-int v7, v17, v22

    .line 482
    :goto_1
    if-ltz v17, :cond_0

    .line 483
    new-instance v22, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f09003b

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getColor(I)I

    move-result v23

    invoke-direct/range {v22 .. v23}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v23, v17, v4

    add-int v24, v7, v4

    const/16 v25, 0x21

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 487
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v23, v7, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_3

    .line 488
    add-int/lit8 v22, v7, 0x1

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 489
    add-int/lit8 v22, v7, 0x1

    add-int v4, v4, v22

    goto :goto_0

    .line 478
    :cond_2
    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    .line 479
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v22

    add-int v7, v17, v22

    goto :goto_1

    .line 491
    :cond_3
    invoke-virtual {v9, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 492
    add-int/2addr v4, v7

    goto/16 :goto_0

    .line 497
    .end local v7    # "endOffset":I
    .end local v10    # "key":Ljava/lang/String;
    .end local v11    # "keyStr":Ljava/lang/String;
    .end local v12    # "lowserStr":Ljava/lang/String;
    .end local v17    # "startOffset":I
    .end local v21    # "upperStr":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 498
    .local v5, "e":Ljava/util/NoSuchElementException;
    invoke-virtual {v5}, Ljava/util/NoSuchElementException;->printStackTrace()V

    goto/16 :goto_0

    .line 499
    .end local v5    # "e":Ljava/util/NoSuchElementException;
    :catch_1
    move-exception v5

    .line 500
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 503
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_4
    if-eqz p5, :cond_9

    .line 505
    :try_start_1
    new-instance v19, Ljava/util/StringTokenizer;

    const-string v22, " "

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v9, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    .local v19, "strToken":Ljava/util/StringTokenizer;
    const/16 v20, 0x0

    .line 508
    .local v20, "tagAddOffset":I
    move-object/from16 v0, p0

    move/from16 v1, p6

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getRelatedStringIndex(I)I

    move-result v8

    .line 509
    .local v8, "index":I
    :cond_5
    :goto_2
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result v22

    if-eqz v22, :cond_9

    .line 511
    :try_start_2
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v14

    .line 513
    .local v14, "related":Ljava/lang/String;
    invoke-virtual {v9, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    .line 514
    .local v18, "strIndex":I
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v8, v0, :cond_7

    add-int v22, v18, v20

    move/from16 v0, v22

    if-eq v8, v0, :cond_7

    .line 516
    move/from16 v16, v18

    .line 517
    .local v16, "start":I
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v22

    add-int v6, v16, v22

    .line 519
    .local v6, "end":I
    if-ltz v16, :cond_5

    .line 520
    new-instance v22, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f09003a

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getColor(I)I

    move-result v23

    invoke-direct/range {v22 .. v23}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v23, v16, v4

    add-int v24, v6, v4

    const/16 v25, 0x21

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 524
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v22

    move/from16 v0, v22

    if-le v0, v6, :cond_6

    .line 525
    add-int/lit8 v22, v6, 0x1

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 526
    add-int/lit8 v22, v6, 0x1

    add-int v20, v20, v22

    .line 527
    add-int/lit8 v22, v6, 0x1

    add-int v4, v4, v22

    goto :goto_2

    .line 529
    :cond_6
    invoke-virtual {v9, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 530
    add-int v20, v20, v6

    .line 531
    add-int/2addr v4, v6

    goto :goto_2

    .line 536
    .end local v6    # "end":I
    .end local v16    # "start":I
    :cond_7
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v22

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v23

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_8

    .line 537
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 538
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    add-int v20, v20, v22

    .line 539
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    add-int v4, v4, v22

    goto/16 :goto_2

    .line 541
    :cond_8
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 542
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v22

    add-int v20, v20, v22

    .line 543
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_3

    move-result v22

    add-int v4, v4, v22

    goto/16 :goto_2

    .line 546
    .end local v14    # "related":Ljava/lang/String;
    .end local v18    # "strIndex":I
    :catch_2
    move-exception v5

    .line 547
    .restart local v5    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_2

    .line 550
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v8    # "index":I
    .end local v19    # "strToken":Ljava/util/StringTokenizer;
    .end local v20    # "tagAddOffset":I
    :catch_3
    move-exception v5

    .line 551
    .local v5, "e":Ljava/util/NoSuchElementException;
    invoke-virtual {v5}, Ljava/util/NoSuchElementException;->printStackTrace()V

    .line 558
    .end local v4    # "addOffset":I
    .end local v5    # "e":Ljava/util/NoSuchElementException;
    .end local v9    # "inputStr":Ljava/lang/String;
    .end local v13    # "queryKeyToken":Ljava/util/StringTokenizer;
    :cond_9
    :goto_3
    return-object v15

    .line 552
    .restart local v4    # "addOffset":I
    .restart local v9    # "inputStr":Ljava/lang/String;
    .restart local v13    # "queryKeyToken":Ljava/util/StringTokenizer;
    :catch_4
    move-exception v5

    .line 553
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method private setTagType(Ljava/lang/String;)V
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 388
    const/4 v4, -0x1

    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValueTagType:I

    .line 389
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    .line 391
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getTagSupportedPackageList(Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 393
    .local v0, "argList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 394
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 395
    .local v3, "packageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setTagType() Searchable UNchecked packageName : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 399
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, p1, v0, v5}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getMatchedTag(Ljava/lang/String;Ljava/util/ArrayList;Z)Landroid/database/Cursor;

    move-result-object v2

    .line 401
    .local v2, "matchedTagCursor":Landroid/database/Cursor;
    if-nez v2, :cond_1

    .line 418
    :goto_1
    return-void

    .line 404
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_2

    .line 405
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 408
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 409
    const-string v4, "rawdata"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    .line 411
    const-string v4, "type"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValueTagType:I

    .line 414
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setTagType() mKeyTagValue : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mKeyValueTagType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValueTagType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 417
    const/4 v2, 0x0

    .line 418
    goto :goto_1
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    return v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 24
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 90
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValue:Ljava/lang/String;

    .line 91
    .local v3, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValueTagType:I

    .line 92
    .local v8, "tagType":I
    const-string v2, "history_keyword"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 94
    .local v4, "keyword":Ljava/lang/String;
    const v2, 0x7f0b0040

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 96
    .local v5, "tvKeyword":Landroid/widget/TextView;
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object v6, v4

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->makeSpanString(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Ljava/lang/String;ZI)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v4, v2, v6

    const/4 v6, 0x1

    const-string v7, ", "

    aput-object v7, v2, v6

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0e0031

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    invoke-static {v5, v2}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setViewDesc(Landroid/view/View;[Ljava/lang/String;)V

    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c0022

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->getFontTypeface(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 102
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mDispMode:I

    if-nez v2, :cond_0

    .line 103
    const v2, 0x7f0b0041

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    .line 104
    .local v18, "divider":Landroid/widget/ImageView;
    const/16 v2, 0x8

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    const v2, 0x7f0b003f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    .line 107
    .local v16, "btnDelete":Landroid/widget/ImageView;
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 108
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 109
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 342
    .end local v16    # "btnDelete":Landroid/widget/ImageView;
    .end local v18    # "divider":Landroid/widget/ImageView;
    :goto_0
    return-void

    .line 282
    :cond_0
    const v2, 0x7f0b0044

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/ImageView;

    .line 283
    .local v19, "imageTag":Landroid/widget/ImageView;
    const v2, 0x7f0b0043

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 284
    .local v23, "tvDate":Landroid/widget/TextView;
    const-string v2, "added_date"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    .line 285
    .local v22, "time":Ljava/lang/Long;
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->convertToDateTimeFormat(J)Ljava/lang/String;

    move-result-object v17

    .line 287
    .local v17, "date":Ljava/lang/String;
    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    const/16 v2, 0x8

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 290
    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c0021

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->getFontTypeface(I)Landroid/graphics/Typeface;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 294
    const-string v2, "type"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->TAG:Ljava/lang/String;

    const-string v6, "bindView() Wrong type"

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 300
    :cond_1
    const/16 v15, 0x80

    .line 302
    .local v15, "type":I
    :try_start_0
    const-string v2, "type"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v15

    .line 306
    :goto_1
    and-int/lit8 v2, v15, 0x10

    if-lez v2, :cond_3

    const/4 v14, 0x1

    .line 307
    .local v14, "bRelated":Z
    :goto_2
    if-eqz v14, :cond_2

    .line 308
    add-int/lit8 v15, v15, -0x10

    .line 311
    :cond_2
    packed-switch v15, :pswitch_data_0

    .line 332
    :goto_3
    :pswitch_0
    if-eqz v14, :cond_5

    .line 333
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 334
    .local v21, "tagArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    invoke-direct {v2, v6, v7, v9}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v4, v6, v4}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 340
    .end local v21    # "tagArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v15}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getHistoryText(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 303
    .end local v14    # "bRelated":Z
    :catch_0
    move-exception v20

    .line 304
    .local v20, "nfe":Ljava/lang/NumberFormatException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 306
    .end local v20    # "nfe":Ljava/lang/NumberFormatException;
    :cond_3
    const/4 v14, 0x0

    goto :goto_2

    .line 315
    .restart local v14    # "bRelated":Z
    :pswitch_1
    if-eqz v14, :cond_4

    .line 316
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyTagValue:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v15}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getHistoryText(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v9, p0

    move-object v12, v5

    move-object v13, v4

    invoke-direct/range {v9 .. v15}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->makeSpanString(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Ljava/lang/String;ZI)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 324
    :goto_5
    const/16 v2, 0x8

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_4
    move-object/from16 v9, p0

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    move-object v13, v4

    .line 321
    invoke-direct/range {v9 .. v15}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->makeSpanString(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Ljava/lang/String;ZI)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    const/16 v2, 0x8

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 338
    :cond_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_4

    .line 311
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 374
    const-string v0, ""

    .line 375
    .local v0, "keyword":Ljava/lang/String;
    const-string v1, "type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-nez v1, :cond_0

    .line 376
    const-string v1, "history_keyword"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 378
    :cond_0
    return-object v0
.end method

.method public getHistoryDeleteAllThreshHold()I
    .locals 1

    .prologue
    .line 592
    const/16 v0, 0xa

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 365
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->disabledPosition:I

    if-ne p1, v0, :cond_0

    .line 366
    const/4 v0, 0x0

    .line 368
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 346
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 347
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .line 349
    .local v1, "view":Landroid/view/View;
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mDispMode:I

    if-nez v2, :cond_0

    .line 350
    const v2, 0x7f030014

    invoke-virtual {v0, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 355
    :goto_0
    return-object v1

    .line 352
    :cond_0
    const v2, 0x7f030015

    invoke-virtual {v0, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public setKeyValue(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setKeyValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mKeyValue:Ljava/lang/String;

    .line 384
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->setTagType(Ljava/lang/String;)V

    .line 385
    return-void
.end method
