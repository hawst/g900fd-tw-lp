.class public Lcom/samsung/android/app/galaxyfinder/Constants$Package;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Package"
.end annotation


# static fields
.field public static final AMAZON_APPS:Ljava/lang/String; = ".amazon."

.field public static final AMAZON_IMDB:Ljava/lang/String; = "com.imdb.mobile"

.field public static final APPS:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder"

.field public static final BAIDU:Ljava/lang/String; = "com.baidu.searchbox_samsung"

.field public static final BAIDU_NETDISK:Ljava/lang/String; = "com.baidu.netdisk_ss"

.field public static final BAIDU_SEARCH:Ljava/lang/String; = "com.baidu.searchbox"

.field public static final CALENDAR:Ljava/lang/String; = "com.android.calendar"

.field public static final CALL_LOG:Ljava/lang/String; = "com.samsung.dialer.calllog.LogsSearchResultsActivity"

.field public static final CONTACTS:Ljava/lang/String; = "com.android.contacts"

.field public static final CONTACTS_PROVIDER:Ljava/lang/String; = "com.android.providers.contacts"

.field public static final DCM_CONTACTS:Ljava/lang/String; = "com.samsung.contacts"

.field public static final DRIVE:Ljava/lang/String; = "com.google.android.apps.docs"

.field public static final EBAY_APPS:Ljava/lang/String; = ".ebay."

.field public static final GALAXYFINDER:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder"

.field public static final GOOGLE_SEARCH:Ljava/lang/String; = "com.google.android.googlequicksearchbox"

.field public static final JCONTACTS:Ljava/lang/String; = "com.android.jcontacts"

.field public static final LOG:Ljava/lang/String; = "com.sec.android.app.dialertab.calllog.LogsSearchResultsActivity"

.field public static final ONE_PASSWORD:Ljava/lang/String; = "com.onepassword.passwordmanager"

.field public static final PAYPAL:Ljava/lang/String; = "com.paypal.android.p2pmobile"

.field public static final PLAY_BOOKS:Ljava/lang/String; = "com.google.android.apps.books"

.field public static final PLAY_MUSIC:Ljava/lang/String; = "com.google.android.music"

.field public static final PLAY_VIDEOS:Ljava/lang/String; = "com.google.android.videos"

.field public static final SAMSUNG_LINK:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink"

.field public static final SETTINGS:Ljava/lang/String; = "com.android.settings"

.field public static final TRIP_ADVISOR:Ljava/lang/String; = "com.tripadvisor.tripadvisor"

.field public static final TWITTER:Ljava/lang/String; = "com.twitter.android"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
