.class public Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;
.super Ljava/lang/Object;
.source "TagDataConverter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mConverterTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mContext:Landroid/content/Context;

    .line 16
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mConverterTable:Ljava/util/HashMap;

    .line 19
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mContext:Landroid/content/Context;

    .line 20
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->init()V

    .line 21
    return-void
.end method


# virtual methods
.method public convert(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 12
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 34
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mConverterTable:Ljava/util/HashMap;

    if-eqz v7, :cond_0

    .line 35
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mConverterTable:Ljava/util/HashMap;

    const-string v8, "category"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;

    .line 37
    .local v4, "converter":Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;
    if-eqz v4, :cond_0

    .line 38
    const-string v7, "encoding"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 40
    .local v5, "isEncoding":Z
    if-eqz v5, :cond_0

    .line 41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 42
    .local v2, "before":J
    invoke-virtual {v4, p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;->convertToMeaningValue(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v6

    .line 43
    .local v6, "result":Landroid/os/Bundle;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 45
    .local v0, "after":J
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "category : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "category"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", timegap : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v10, v0, v2

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " milliseconds"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    .end local v0    # "after":J
    .end local v2    # "before":J
    .end local v4    # "converter":Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;
    .end local v5    # "isEncoding":Z
    .end local v6    # "result":Landroid/os/Bundle;
    :goto_0
    return-object v6

    :cond_0
    move-object v6, p1

    goto :goto_0
.end method

.method public init()V
    .locals 4

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mConverterTable:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mConverterTable:Ljava/util/HashMap;

    .line 28
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mConverterTable:Ljava/util/HashMap;

    const-string v1, "weather"

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterWeather;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterWeather;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mConverterTable:Ljava/util/HashMap;

    const-string v1, "people"

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterPeople;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagDataConverter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterPeople;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    :cond_0
    return-void
.end method
