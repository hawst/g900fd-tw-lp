.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;
.super Ljava/lang/Object;
.source "GalaxyFinderActivity.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$ISearchActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelQuery()V
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->requestCancel()V

    .line 426
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 428
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$602(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 430
    :cond_1
    return-void
.end method

.method public onRequestVoiceRecognition()V
    .locals 5

    .prologue
    .line 434
    sget-boolean v3, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v3, :cond_2

    .line 435
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const-string v4, "show_policy_info"

    invoke-static {v3, v4}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->getSettings(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 437
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 438
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;

    invoke-direct {v1}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;-><init>()V

    .line 439
    .local v1, "dialog":Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "NUANCE_DIALOG"

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 455
    .end local v1    # "dialog":Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoDialogFragment;
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const-string v4, "show_policy_info"

    invoke-static {v3, v4}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoiceTosSetting;->getSettings(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 444
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startNuanceVoiceRecognition()V

    goto :goto_0

    .line 447
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 448
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const-string v3, "com.google.android.googlequicksearchbox"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    .line 449
    .local v0, "appState":I
    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    .line 450
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const-string v4, "com.google.android.googlequicksearchbox"

    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->showDisabledAppDialog(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$800(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 452
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->startVoiceRecognition()V
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$900(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    goto :goto_0
.end method

.method public onStartQuery(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "query"    # Landroid/os/Bundle;

    .prologue
    .line 407
    const-string v1, "extra_query_from"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 409
    .local v0, "from":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "query_from_reload"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 410
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const v4, 0x7f0e0090

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const v5, 0x7f0e008f

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v2

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mProgressDlg:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$602(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 415
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 416
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$6;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchController:Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->requestSearch(Landroid/os/Bundle;)I

    .line 418
    :cond_1
    return-void
.end method
