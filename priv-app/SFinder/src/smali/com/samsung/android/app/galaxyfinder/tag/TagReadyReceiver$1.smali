.class Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;
.super Landroid/os/Handler;
.source "TagReadyReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v10, 0x1

    .line 22
    const/4 v3, 0x0

    .line 23
    .local v3, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    .line 24
    .local v1, "context":Landroid/content/Context;
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v7, v7, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$ReceiveType;

    if-eqz v7, :cond_1

    .line 25
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$ReceiveType;

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$ReceiveType;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 26
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$ReceiveType;

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$ReceiveType;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 32
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[onReceive] Action : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " recevied"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const-string v7, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 35
    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 36
    .local v2, "dat":Landroid/net/Uri;
    if-eqz v2, :cond_2

    .line 37
    sget-object v7, Lcom/samsung/android/app/galaxyfinder/Constants;->INTERNAL_SYSTEM_STORAGE_DIRECTORY:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 38
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[onReceive] "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ready. SKIP"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "dat":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "[onReceive] Wrong received msg type"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 43
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v2    # "dat":Landroid/net/Uri;
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->setMediaScannerState(Landroid/content/Context;I)V
    invoke-static {v7, v1, v10}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$100(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;I)V

    .line 44
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->getTagServiceState(Landroid/content/Context;)I
    invoke-static {v7, v1}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$200(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;)I

    move-result v6

    .line 45
    .local v6, "state":I
    if-le v6, v10, :cond_0

    .line 46
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    const/4 v8, 0x4

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->setTagServiceState(Landroid/content/Context;I)V
    invoke-static {v7, v1, v8}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$300(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;I)V

    .line 47
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    const-class v8, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    const-string v9, "com.samsung.android.app.galaxyfinder.tag.start_full_sync"

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->startTagService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V
    invoke-static {v7, v1, v8, v9, v6}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$400(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V

    .line 49
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    const-class v8, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;

    const-string v9, "com.samsung.android.app.galaxyfinder.tag.start_full_sync"

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->startTagService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V
    invoke-static {v7, v1, v8, v9, v6}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$400(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V

    .line 51
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    const/4 v8, 0x0

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->setTagServiceState(Landroid/content/Context;I)V
    invoke-static {v7, v1, v8}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$300(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;I)V

    goto :goto_0

    .line 53
    .end local v2    # "dat":Landroid/net/Uri;
    .end local v6    # "state":I
    :cond_3
    const-string v7, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 55
    :cond_4
    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 56
    .restart local v2    # "dat":Landroid/net/Uri;
    if-eqz v2, :cond_0

    .line 57
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[onReceive] intent.getData() = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", getPath() = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const-string v7, "/storage/extSdCard"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 61
    const-string v7, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 62
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    const/4 v8, 0x2

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->setTagServiceState(Landroid/content/Context;I)V
    invoke-static {v7, v1, v8}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$300(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 64
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    const/4 v8, 0x3

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->setTagServiceState(Landroid/content/Context;I)V
    invoke-static {v7, v1, v8}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$300(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 68
    .end local v2    # "dat":Landroid/net/Uri;
    :cond_6
    const-string v7, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 69
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->setTagServiceState(Landroid/content/Context;I)V
    invoke-static {v7, v1, v10}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$300(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;I)V

    .line 70
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[onReceive] "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " received. and Set TagServiceState : TAG_SERVICE_BOOTUP"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 72
    :cond_7
    const-string v7, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 73
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->getMediaScannerState(Landroid/content/Context;)I
    invoke-static {v7, v1}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$500(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;)I

    move-result v4

    .line 74
    .local v4, "mediaScannerState":I
    if-ge v4, v10, :cond_8

    .line 75
    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MediaScannerState = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", Media Scanner not yet called once"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 80
    :cond_8
    const-string v7, "networkInfo"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/NetworkInfo;

    .line 81
    .local v5, "net":Landroid/net/NetworkInfo;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v7

    if-ne v7, v10, :cond_0

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 83
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    const-class v8, Lcom/samsung/android/app/galaxyfinder/tag/LocationTagReadyService;

    const-string v9, "com.samsung.android.app.galaxyfinder.tag.start_full_sync"

    iget-object v10, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->getTagServiceState(Landroid/content/Context;)I
    invoke-static {v10, v1}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$200(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;)I

    move-result v10

    # invokes: Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->startTagService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V
    invoke-static {v7, v1, v8, v9, v10}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;->access$400(Lcom/samsung/android/app/galaxyfinder/tag/TagReadyReceiver;Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method
