.class public Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;
.super Landroid/app/Activity;
.source "NuanceVoicePolicyInfoActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NuanceVoicePolicyInfoActivity"


# instance fields
.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 19
    const-string v0, "NuanceVoicePolicyInfoActivity"

    const-string v1, "onCreate() "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->setContentView(I)V

    .line 22
    const v0, 0x7f0b0006

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    .line 24
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 29
    const-string v0, "NuanceVoicePolicyInfoActivity"

    const-string v2, "onResume()"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "nuancevoiceinfo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    const-string v0, "NuanceVoicePolicyInfoActivity"

    const-string v2, "onResume() - 1 "

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const v0, 0x7f0e0082

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    const v2, 0x7f0e0081

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/NuanceVoicePolicyInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/text"

    const-string v4, "UTF-8"

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 45
    return-void
.end method
