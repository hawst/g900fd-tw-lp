.class public abstract Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;
.super Ljava/lang/Object;
.source "GalaxyFinderCategoryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "CategoryItem"
.end annotation


# instance fields
.field categoryIcon:Ljava/lang/Object;

.field categoryLabel:Ljava/lang/String;

.field catetoryId:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "categoryIcon"    # Ljava/lang/Object;

    .prologue
    .line 542
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->catetoryId:Ljava/lang/String;

    .line 544
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->categoryLabel:Ljava/lang/String;

    .line 545
    iput-object p4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->categoryIcon:Ljava/lang/Object;

    .line 546
    return-void
.end method


# virtual methods
.method public getCategoryIcon()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->categoryIcon:Ljava/lang/Object;

    return-object v0
.end method

.method public getCategoryLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->categoryLabel:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getCategoryType()I
.end method

.method public getCatetoryId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->catetoryId:Ljava/lang/String;

    return-object v0
.end method
