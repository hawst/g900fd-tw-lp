.class public Lcom/samsung/android/app/galaxyfinder/util/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field static final CONTENT_BASE:Ljava/lang/String; = "content:"

.field static final FILE_BASE:Ljava/lang/String; = "file://"

.field private static final TAG:Ljava/lang/String; = "Utils"

.field public static mActionId:I

.field public static mAppMainWindow:Landroid/view/Window;

.field private static mContextProviderVersion:I

.field public static mDebugLog:Z

.field private static mQuickSearchAvailable:Z

.field private static mQuickSearchChecked:Z

.field private static mToast:Landroid/widget/Toast;

.field private static mbContextServiceSurveyMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mDebugLog:Z

    .line 65
    sput v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mActionId:I

    .line 69
    const/4 v0, -0x1

    sput v0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mContextProviderVersion:I

    .line 71
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mbContextServiceSurveyMode:Z

    .line 73
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchAvailable:Z

    .line 75
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchChecked:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$002(Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Landroid/widget/Toast;

    .prologue
    .line 59
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mToast:Landroid/widget/Toast;

    return-object p0
.end method

.method public static executeRelatedTask(Landroid/content/Context;IILjava/lang/String;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "taskType"    # I
    .param p2, "dataType"    # I
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 180
    invoke-static {p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->getIntentForRelatedTask(IILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 182
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 184
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :goto_0
    const/4 v2, 0x1

    .line 192
    :goto_1
    return v2

    .line 185
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 192
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static findCurIndexInScreen(Landroid/content/Context;Landroid/view/View;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x1

    .line 382
    const/4 v8, 0x2

    new-array v4, v8, [I

    .line 383
    .local v4, "loc":[I
    const-string v8, "window"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    .line 384
    .local v7, "wm":Landroid/view/WindowManager;
    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    .line 385
    .local v3, "display":Landroid/view/Display;
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 386
    .local v5, "size":Landroid/graphics/Point;
    invoke-virtual {v3, v5}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 388
    const/4 v0, 0x0

    .line 389
    .local v0, "FIRST_START_LINE":I
    iget v1, v5, Landroid/graphics/Point;->y:I

    .line 390
    .local v1, "LAST_END_LINE":I
    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 391
    aget v8, v4, v10

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int v6, v8, v9

    .line 392
    .local v6, "viewEndY":I
    const/4 v2, 0x0

    .line 394
    .local v2, "bShowItem":Z
    aget v8, v4, v10

    if-gez v8, :cond_1

    if-lez v6, :cond_1

    .line 395
    const/4 v2, 0x1

    .line 407
    :cond_0
    :goto_0
    return v2

    .line 396
    :cond_1
    aget v8, v4, v10

    if-lez v8, :cond_2

    if-gt v6, v1, :cond_2

    .line 397
    if-nez v2, :cond_0

    .line 398
    const/4 v2, 0x1

    goto :goto_0

    .line 400
    :cond_2
    aget v8, v4, v10

    if-ge v8, v1, :cond_3

    if-le v6, v1, :cond_3

    .line 401
    const/4 v2, 0x1

    goto :goto_0

    .line 403
    :cond_3
    if-eqz v2, :cond_0

    .line 404
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static findCurIndexInScreenCenter(Landroid/content/Context;Landroid/view/View;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 413
    const/4 v7, 0x2

    new-array v2, v7, [I

    .line 414
    .local v2, "loc":[I
    const-string v7, "window"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 415
    .local v5, "wm":Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 416
    .local v1, "display":Landroid/view/Display;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 417
    .local v3, "size":Landroid/graphics/Point;
    invoke-virtual {v1, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 419
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 420
    aget v7, v2, v6

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int v4, v7, v8

    .line 421
    .local v4, "viewEndY":I
    iget v7, v3, Landroid/graphics/Point;->x:I

    shr-int/lit8 v0, v7, 0x1

    .line 423
    .local v0, "centerScreen":I
    aget v7, v2, v6

    if-ge v7, v0, :cond_0

    if-le v4, v0, :cond_0

    .line 426
    :goto_0
    return v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static findIndexInScreenByY(Landroid/view/View;I)Z
    .locals 5
    .param p0, "view"    # Landroid/view/View;
    .param p1, "y"    # I

    .prologue
    const/4 v2, 0x1

    .line 431
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 433
    .local v0, "loc":[I
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 434
    aget v3, v0, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int v1, v3, v4

    .line 436
    .local v1, "viewEndY":I
    if-le v1, p1, :cond_0

    .line 439
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getActionId()I
    .locals 2

    .prologue
    .line 102
    sget v0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mActionId:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mActionId:I

    return v0
.end method

.method public static getAppMainWindow()Landroid/view/Window;
    .locals 1

    .prologue
    .line 511
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mAppMainWindow:Landroid/view/Window;

    return-object v0
.end method

.method public static getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "MAX_LENGTH"    # I

    .prologue
    .line 452
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/text/InputFilter;

    .line 454
    .local v0, "FilterArray":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/util/Utils$1;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/Utils$1;-><init>(Landroid/content/Context;I)V

    aput-object v2, v0, v1

    .line 503
    return-object v0
.end method

.method public static getIntentForRelatedTask(IILjava/lang/String;)Landroid/content/Intent;
    .locals 10
    .param p0, "taskType"    # I
    .param p1, "dataType"    # I
    .param p2, "data"    # Ljava/lang/String;

    .prologue
    .line 196
    const/4 v4, 0x0

    .line 198
    .local v4, "intent":Landroid/content/Intent;
    packed-switch p0, :pswitch_data_0

    .line 281
    :goto_0
    :pswitch_0
    if-eqz v4, :cond_0

    .line 282
    const/high16 v6, 0x10000000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 285
    :cond_0
    return-object v4

    .line 200
    :pswitch_1
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.CALL"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 201
    .restart local v4    # "intent":Landroid/content/Intent;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "tel:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    .line 205
    :pswitch_2
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.SENDTO"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 206
    .restart local v4    # "intent":Landroid/content/Intent;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "smsto:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    .line 210
    :pswitch_3
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.INSERT"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 212
    .restart local v4    # "intent":Landroid/content/Intent;
    if-nez p1, :cond_1

    .line 213
    const-string v6, "phone"

    invoke-virtual {v4, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    :goto_1
    const-string v6, "vnd.android.cursor.dir/contact"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 215
    :cond_1
    const-string v6, "email"

    invoke-virtual {v4, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 222
    :pswitch_4
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.SENDTO"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 223
    .restart local v4    # "intent":Landroid/content/Intent;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mailto:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 227
    :pswitch_5
    move-object v5, p2

    .line 229
    .local v5, "url":Ljava/lang/String;
    const-string v6, "http://"

    invoke-virtual {p2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "https://"

    invoke-virtual {p2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 230
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "http://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 233
    :cond_2
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 234
    .restart local v4    # "intent":Landroid/content/Intent;
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 239
    .end local v5    # "url":Ljava/lang/String;
    :pswitch_6
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.INSERT"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 240
    .restart local v4    # "intent":Landroid/content/Intent;
    sget-object v6, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 241
    const-string v6, "url"

    invoke-virtual {v4, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v6, "vnd.android.cursor.dir/bookmark"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 246
    :pswitch_7
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 247
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "geo:0,0?q="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 255
    :pswitch_8
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.EDIT"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 256
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string v6, "vnd.android.cursor.item/event"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 257
    const-string v6, "beginTime"

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v4, v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto/16 :goto_0

    .line 262
    :pswitch_9
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 264
    .local v1, "date":Ljava/util/Date;
    :try_start_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy-MM-dd"

    invoke-direct {v3, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 265
    .local v3, "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v3, p2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 269
    .end local v3    # "format":Ljava/text/SimpleDateFormat;
    :goto_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 270
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 271
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 272
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    const-string v6, "beginTime"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-virtual {v4, v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 274
    const-string v6, "content://com.android.calendar/time/"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 266
    .end local v0    # "calendar":Ljava/util/Calendar;
    :catch_0
    move-exception v2

    .line 267
    .local v2, "e":Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_2

    .line 198
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_7
        :pswitch_9
    .end packed-switch
.end method

.method public static getOverallMimeType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "mimetype"    # Ljava/lang/String;
    .param p1, "overallMimetype"    # Ljava/lang/String;

    .prologue
    .line 289
    if-nez p1, :cond_0

    .line 317
    .end local p0    # "mimetype":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 293
    .restart local p0    # "mimetype":Ljava/lang/String;
    :cond_0
    const-string v3, "*"

    .line 294
    .local v3, "resultMimetype":Ljava/lang/String;
    const-string v4, "*"

    .line 296
    .local v4, "resultSubtype":Ljava/lang/String;
    new-instance v7, Ljava/util/StringTokenizer;

    const-string v9, "/"

    invoke-direct {v7, p0, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    .local v7, "stMimetype":Ljava/util/StringTokenizer;
    new-instance v8, Ljava/util/StringTokenizer;

    const-string v9, "/"

    invoke-direct {v8, p1, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    .local v8, "stOverall":Ljava/util/StringTokenizer;
    :try_start_0
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 300
    .local v5, "srcMimetype":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 302
    .local v0, "dstMimetype":Ljava/lang/String;
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 303
    move-object v3, v5

    .line 306
    :cond_1
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 307
    .local v6, "srcSubtype":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 309
    .local v1, "dstSubtype":Ljava/lang/String;
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v9

    if-eqz v9, :cond_2

    .line 310
    move-object v4, v6

    .line 317
    .end local v0    # "dstMimetype":Ljava/lang/String;
    .end local v1    # "dstSubtype":Ljava/lang/String;
    .end local v5    # "srcMimetype":Ljava/lang/String;
    .end local v6    # "srcSubtype":Ljava/lang/String;
    :cond_2
    :goto_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 312
    :catch_0
    move-exception v2

    .line 313
    .local v2, "e":Ljava/util/NoSuchElementException;
    invoke-virtual {v2}, Ljava/util/NoSuchElementException;->printStackTrace()V

    goto :goto_1

    .line 314
    .end local v2    # "e":Ljava/util/NoSuchElementException;
    :catch_1
    move-exception v2

    .line 315
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static getPrefixForIndian(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "paint"    # Landroid/text/TextPaint;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 166
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-object v1

    .line 169
    :cond_1
    const/4 v0, 0x0

    .line 170
    .local v0, "prefixForIndian":[C
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-static {p0, p1, v2}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v0

    .line 172
    if-eqz v0, :cond_0

    .line 175
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    goto :goto_0
.end method

.method public static getRelatedTask(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;
    .locals 14
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 106
    new-instance v6, Landroid/util/secutil/SmartParser;

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v6, p0, v11}, Landroid/util/secutil/SmartParser;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 109
    .local v6, "parser":Landroid/util/secutil/SmartParser;
    if-eqz v6, :cond_4

    .line 111
    invoke-virtual {v6}, Landroid/util/secutil/SmartParser;->getTotalData()Landroid/util/secutil/SmartParsingData;

    move-result-object v9

    .line 113
    .local v9, "totalData":Landroid/util/secutil/SmartParsingData;
    if-eqz v9, :cond_4

    .line 115
    const/4 v11, 0x3

    invoke-virtual {v9, v11}, Landroid/util/secutil/SmartParsingData;->getInfo(I)Ljava/util/ArrayList;

    move-result-object v7

    .line 116
    .local v7, "phoneData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x4

    invoke-virtual {v9, v11}, Landroid/util/secutil/SmartParsingData;->getInfo(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 117
    .local v1, "emailData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x5

    invoke-virtual {v9, v11}, Landroid/util/secutil/SmartParsingData;->getInfo(I)Ljava/util/ArrayList;

    move-result-object v10

    .line 118
    .local v10, "urlData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x6

    invoke-virtual {v9, v11}, Landroid/util/secutil/SmartParsingData;->getInfo(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 120
    .local v0, "dateMillisResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x7

    invoke-virtual {v9, v11}, Landroid/util/secutil/SmartParsingData;->getInfo(I)Ljava/util/ArrayList;

    move-result-object v8

    .line 123
    .local v8, "timeMillisResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, ""

    .line 126
    .local v5, "matchingString":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_0

    .line 127
    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "matchingString":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 128
    .restart local v5    # "matchingString":Ljava/lang/String;
    new-instance v11, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;

    const/4 v12, 0x0

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;-><init>(ILjava/lang/String;)V

    .line 161
    .end local v0    # "dateMillisResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "emailData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "matchingString":Ljava/lang/String;
    .end local v7    # "phoneData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "timeMillisResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "totalData":Landroid/util/secutil/SmartParsingData;
    .end local v10    # "urlData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-object v11

    .line 132
    .restart local v0    # "dateMillisResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v1    # "emailData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "matchingString":Ljava/lang/String;
    .restart local v7    # "phoneData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8    # "timeMillisResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v9    # "totalData":Landroid/util/secutil/SmartParsingData;
    .restart local v10    # "urlData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_1

    .line 133
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "matchingString":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 134
    .restart local v5    # "matchingString":Ljava/lang/String;
    new-instance v11, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;

    const/4 v12, 0x1

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 138
    :cond_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_2

    .line 139
    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "matchingString":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 140
    .restart local v5    # "matchingString":Ljava/lang/String;
    new-instance v11, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;

    const/4 v12, 0x2

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 144
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_3

    .line 145
    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 146
    .local v4, "gmtMillsec":Ljava/lang/String;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v11

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v11

    int-to-long v2, v11

    .line 147
    .local v2, "gmtHour":J
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    sub-long/2addr v12, v2

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 148
    new-instance v11, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;

    const/4 v12, 0x3

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 152
    .end local v2    # "gmtHour":J
    .end local v4    # "gmtMillsec":Ljava/lang/String;
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_4

    .line 153
    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 154
    .restart local v4    # "gmtMillsec":Ljava/lang/String;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v11

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v11

    int-to-long v2, v11

    .line 155
    .restart local v2    # "gmtHour":J
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    sub-long/2addr v12, v2

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 156
    new-instance v11, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;

    const/4 v12, 0x5

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 161
    .end local v0    # "dateMillisResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "emailData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "gmtHour":J
    .end local v4    # "gmtMillsec":Ljava/lang/String;
    .end local v5    # "matchingString":Ljava/lang/String;
    .end local v7    # "phoneData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "timeMillisResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "totalData":Landroid/util/secutil/SmartParsingData;
    .end local v10    # "urlData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    const/4 v11, 0x0

    goto/16 :goto_0
.end method

.method private static getVersionOfContextProviders(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 583
    sget v2, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mContextProviderVersion:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 585
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.samsung.android.providers.context"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 587
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    sput v2, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mContextProviderVersion:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 592
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mContextProviderVersion:I

    return v2

    .line 588
    :catch_0
    move-exception v0

    .line 589
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "Utils"

    const-string v3, "[SW] Could not find ContextProvider"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static hasPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 350
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 351
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    .line 353
    .local v1, "hasPkg":Z
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :goto_0
    return v1

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    .line 356
    const-string v3, "Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Package not found : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static insertCategoryLog(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "checked"    # Z

    .prologue
    .line 541
    sget-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mbContextServiceSurveyMode:Z

    if-nez v1, :cond_0

    .line 556
    :goto_0
    return-void

    .line 545
    :cond_0
    const/4 v0, 0x0

    .line 546
    .local v0, "feature":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 547
    const-string v0, "ADCA"

    .line 552
    :goto_1
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 553
    const-string v1, "Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertCategoryLog() package : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", checked? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    :cond_1
    invoke-static {p0, v0, p1}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertSurveyLogInternal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 549
    :cond_2
    const-string v0, "RMCA"

    goto :goto_1
.end method

.method private static insertFeatureLog(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "feature"    # Ljava/lang/String;

    .prologue
    .line 614
    const-string v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 619
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 620
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 621
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    const-string v4, "feature"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 624
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertFeatureLog() feature : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", insertion operation is performed."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 630
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 626
    :catch_0
    move-exception v1

    .line 627
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "Utils"

    const-string v5, "Error while using the ContextProvider (feature)"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    const-string v4, "Utils"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static insertKeywordLog(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 515
    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->getVersionOfContextProviders(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 532
    :goto_0
    return-void

    .line 518
    :cond_0
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 520
    :pswitch_0
    invoke-static {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertKeywordLogInternal(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 526
    :pswitch_1
    invoke-static {p0, p1}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertFeatureLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 518
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static insertKeywordLogInternal(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "_keyword"    # Ljava/lang/String;

    .prologue
    .line 596
    const-string v4, "content://com.samsung.android.providers.context.log.search_keyword"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 600
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 601
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 602
    .local v2, "row1":Landroid/content/ContentValues;
    const-string v4, "app_id"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const-string v4, "keyword"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 605
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertKeywordLogInternal() keyword : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", insertion operation is performed."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 611
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row1":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 607
    :catch_0
    move-exception v1

    .line 608
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "Utils"

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    const-string v4, "Utils"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static insertSurveryLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 536
    invoke-static {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertSurveyLogInternal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    return-void
.end method

.method private static insertSurveyLogInternal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 560
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 561
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "app_id"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const-string v2, "feature"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    const-string v2, "extra"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 566
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 568
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 569
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 570
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 571
    const-string v2, "Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insertSurveyLogInternal() feature : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Package : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " operation is performed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    return-void
.end method

.method public static isDRMContent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "mimetype"    # Ljava/lang/String;

    .prologue
    .line 696
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 697
    const-string v1, "file"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "oma.drm"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 698
    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    .line 700
    .local v0, "drmManager":Landroid/drm/DrmManagerClient;
    invoke-virtual {v0, p1, p2}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 701
    const/4 v1, 0x1

    .line 706
    .end local v0    # "drmManager":Landroid/drm/DrmManagerClient;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isInstalledHelp(Landroid/content/pm/PackageManager;)Z
    .locals 3
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 676
    const-string v1, "com.samsung.helphub"

    .line 678
    .local v1, "helpPackageName":Ljava/lang/String;
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {p0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 683
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 679
    :catch_0
    move-exception v0

    .line 680
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 681
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isLoggable()Z
    .locals 1

    .prologue
    .line 443
    sget-boolean v0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mDebugLog:Z

    return v0
.end method

.method public static isNetworkConnected(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 321
    if-nez p0, :cond_1

    .line 346
    :cond_0
    :goto_0
    return v3

    .line 325
    :cond_1
    const-string v5, "connectivity"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 327
    .local v0, "connect":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 332
    .local v1, "mobile":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_2

    .line 333
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v5

    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v5, v6, :cond_2

    .line 334
    const-string v3, "Utils"

    const-string v5, "isNetworkConnected(), TYPE_MOBILE NetworkState = CONNECTED"

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 335
    goto :goto_0

    .line 338
    :cond_2
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 339
    .local v2, "wifi":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_3

    .line 340
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v5

    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v5, v6, :cond_3

    .line 341
    const-string v3, "Utils"

    const-string v5, "isNetworkConnected(), TYPE_WIFI NetworkState = CONNECTED"

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 342
    goto :goto_0

    .line 345
    :cond_3
    const-string v4, "Utils"

    const-string v5, "isNetworkConnected(), NetworkState = DISCONNECTED"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static declared-synchronized isQuickSearchAvailable(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    const-class v2, Lcom/samsung/android/app/galaxyfinder/util/Utils;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchChecked:Z

    if-eqz v1, :cond_0

    .line 81
    sget-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchAvailable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :goto_0
    monitor-exit v2

    return v1

    .line 84
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v3, "com.google.android.googlequicksearchbox"

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v1

    const/4 v3, 0x2

    if-lt v1, v3, :cond_1

    .line 86
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchAvailable:Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    :goto_1
    const/4 v1, 0x1

    :try_start_2
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchChecked:Z

    .line 94
    sget-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchAvailable:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 88
    :cond_1
    const/4 v1, 0x1

    :try_start_3
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchAvailable:Z
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    :try_start_4
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchAvailable:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 80
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static isRestrictedProfile(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 669
    const-string v3, "user"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    .line 670
    .local v1, "um":Landroid/os/UserManager;
    invoke-virtual {v1}, Landroid/os/UserManager;->getUserRestrictions()Landroid/os/Bundle;

    move-result-object v0

    .line 671
    .local v0, "restrictions":Landroid/os/Bundle;
    const-string v3, "no_modify_accounts"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public static isSpenEnable(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 692
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.spen_usp"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isValidUri(Ljava/lang/String;)Z
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 637
    if-eqz p0, :cond_1

    const-string v0, "file://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "content:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVoiceCapable(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 651
    invoke-static {p0}, Landroid/util/GeneralUtil;->isVoiceCapable(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static isVoiceInputAvailable(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 655
    const-string v0, "com.google.android.googlequicksearchbox"

    .line 656
    .local v0, "GoogleQuickSearchBox":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 657
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    .line 659
    .local v2, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    const-string v6, "com.google.android.googlequicksearchbox"

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 663
    :goto_0
    sget-boolean v6, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v6, :cond_1

    .line 665
    :cond_0
    :goto_1
    return v4

    .line 660
    :catch_0
    move-exception v1

    .line 661
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 665
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    if-nez v2, :cond_0

    move v4, v5

    goto :goto_1
.end method

.method public static isWifiDevice(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 363
    if-nez p0, :cond_1

    .line 377
    :cond_0
    :goto_0
    return v1

    .line 367
    :cond_1
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 369
    .local v0, "connect":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 373
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    if-nez v2, :cond_0

    .line 377
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static readLocationMode(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 687
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static setAppMainWindow(Landroid/view/Window;)V
    .locals 0
    .param p0, "win"    # Landroid/view/Window;

    .prologue
    .line 507
    sput-object p0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mAppMainWindow:Landroid/view/Window;

    .line 508
    return-void
.end method

.method public static setContextServiceSurveyMode()V
    .locals 3

    .prologue
    .line 576
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mbContextServiceSurveyMode:Z

    .line 578
    const-string v0, "Utils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContextServiceSurveyMode? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mbContextServiceSurveyMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    return-void
.end method

.method public static setForegroundService(Z)V
    .locals 4
    .param p0, "isForeground"    # Z

    .prologue
    .line 641
    new-instance v2, Landroid/os/Binder;

    invoke-direct {v2}, Landroid/os/Binder;-><init>()V

    .line 642
    .local v2, "mForegroundToken":Landroid/os/IBinder;
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    .line 644
    .local v1, "mAm":Landroid/app/IActivityManager;
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-interface {v1, v2, v3, p0}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 648
    :goto_0
    return-void

    .line 645
    :catch_0
    move-exception v0

    .line 646
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V
    .locals 9
    .param p0, "listView"    # Landroid/widget/ListView;

    .prologue
    const/4 v8, 0x0

    .line 710
    if-eqz p0, :cond_0

    .line 711
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 713
    .local v1, "listAdapter":Landroid/widget/ListAdapter;
    if-nez v1, :cond_1

    .line 735
    .end local v1    # "listAdapter":Landroid/widget/ListAdapter;
    :cond_0
    :goto_0
    return-void

    .line 717
    .restart local v1    # "listAdapter":Landroid/widget/ListAdapter;
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v7

    add-int v5, v6, v7

    .line 718
    .local v5, "totalHeight":I
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    .line 720
    .local v2, "listCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 721
    const/4 v6, 0x0

    invoke-interface {v1, v0, v6, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 723
    .local v3, "listItem":Landroid/view/View;
    if-eqz v3, :cond_2

    .line 724
    invoke-virtual {v3, v8, v8}, Landroid/view/View;->measure(II)V

    .line 725
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    .line 720
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 729
    .end local v3    # "listItem":Landroid/view/View;
    :cond_3
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 730
    .local v4, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v6

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    mul-int/2addr v6, v7

    add-int/2addr v6, v5

    iput v6, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 732
    invoke-virtual {p0, v4}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 733
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    goto :goto_0
.end method

.method public static setLoggable(Z)V
    .locals 0
    .param p0, "loggable"    # Z

    .prologue
    .line 447
    sput-boolean p0, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mDebugLog:Z

    .line 448
    return-void
.end method

.method public static declared-synchronized setQuickSeachRefresh()V
    .locals 2

    .prologue
    .line 98
    const-class v0, Lcom/samsung/android/app/galaxyfinder/util/Utils;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-boolean v1, Lcom/samsung/android/app/galaxyfinder/util/Utils;->mQuickSearchChecked:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    monitor-exit v0

    return-void

    .line 98
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static varargs setViewDesc(Landroid/view/View;[Ljava/lang/String;)V
    .locals 6
    .param p0, "v"    # Landroid/view/View;
    .param p1, "strs"    # [Ljava/lang/String;

    .prologue
    .line 738
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 739
    .local v3, "sb":Ljava/lang/StringBuilder;
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 740
    .local v4, "str":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 739
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 742
    .end local v4    # "str":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 743
    return-void
.end method
