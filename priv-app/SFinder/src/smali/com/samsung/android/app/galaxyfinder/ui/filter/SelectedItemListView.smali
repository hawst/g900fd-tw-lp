.class public Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;
.super Landroid/widget/LinearLayout;
.source "SelectedItemListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

.field private mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;

.field private mHorizontalSpacing:I

.field private mInnerMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 22
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;

    .line 24
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    .line 26
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mHorizontalSpacing:I

    .line 28
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mInnerMargin:I

    .line 33
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->init()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;

    .line 24
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    .line 26
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mHorizontalSpacing:I

    .line 28
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mInnerMargin:I

    .line 39
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->init()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;

    .line 24
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    .line 26
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mHorizontalSpacing:I

    .line 28
    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mInnerMargin:I

    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->init()V

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;IILandroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/view/View;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->setChildMargin(IILandroid/view/View;)V

    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mHorizontalSpacing:I

    .line 51
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mInnerMargin:I

    .line 54
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    .line 55
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 146
    return-void
.end method

.method private setChildMargin(IILandroid/view/View;)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "count"    # I
    .param p3, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 149
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 151
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez p1, :cond_0

    .line 152
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mInnerMargin:I

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 160
    :goto_0
    invoke-virtual {p3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    return-void

    .line 153
    :cond_0
    add-int/lit8 v1, p2, -0x1

    if-ne p1, v1, :cond_1

    .line 154
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mHorizontalSpacing:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 155
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mInnerMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_0

    .line 157
    :cond_1
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mHorizontalSpacing:I

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 177
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 178
    .local v1, "parent":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 179
    .local v0, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->removeView(Landroid/view/View;)V

    .line 181
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;

    if-eqz v2, :cond_0

    .line 182
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;

    invoke-interface {v2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;->onActionItemDelete(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 184
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mCallback:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;

    .line 165
    return-void
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 168
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;->setData(Ljava/util/ArrayList;)V

    .line 170
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;->notifyDataSetChanged()V

    .line 173
    :cond_0
    return-void
.end method
