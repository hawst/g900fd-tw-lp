.class final Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData$1;
.super Ljava/lang/Object;
.source "TagData.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)I
    .locals 6
    .param p1, "lhs"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .param p2, "rhs"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 279
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagTypeFlags()I

    move-result v1

    .line 280
    .local v1, "lhsType":I
    invoke-virtual {p2}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagTypeFlags()I

    move-result v3

    .line 282
    .local v3, "rhsType":I
    if-ne v1, v3, :cond_1

    .line 285
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagCount()I

    move-result v0

    .line 286
    .local v0, "lhsCount":I
    invoke-virtual {p2}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagCount()I

    move-result v2

    .line 288
    .local v2, "rhsCount":I
    if-eq v0, v2, :cond_0

    .line 289
    sub-int v4, v2, v0

    .line 295
    .end local v0    # "lhsCount":I
    .end local v2    # "rhsCount":I
    :goto_0
    return v4

    .line 291
    .restart local v0    # "lhsCount":I
    .restart local v2    # "rhsCount":I
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    .line 295
    .end local v0    # "lhsCount":I
    .end local v2    # "rhsCount":I
    :cond_1
    sub-int v4, v3, v1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 271
    check-cast p1, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData$1;->compare(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)I

    move-result v0

    return v0
.end method
