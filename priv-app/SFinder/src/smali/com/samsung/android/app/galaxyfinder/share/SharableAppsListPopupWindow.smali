.class public Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;
.super Landroid/widget/ListPopupWindow;
.source "SharableAppsListPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;,
        Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;
    }
.end annotation


# static fields
.field private static final MAX_DISPLAYED_ITEM_COUNT:I = 0xa

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

.field private mAnchorView:Landroid/view/View;

.field private mIntent:Landroid/content/Intent;

.field private mItemClickListener:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mResources:Landroid/content/res/Resources;

.field private mSharableAppsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchorView"    # Landroid/view/View;
    .param p3, "baseIntent"    # Landroid/content/Intent;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    .line 50
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mIntent:Landroid/content/Intent;

    .line 51
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mAnchorView:Landroid/view/View;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mResources:Landroid/content/res/Resources;

    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->init(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Landroid/content/pm/PackageManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mItemClickListener:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;

    return-object v0
.end method

.method private createSharableAppsList(Landroid/content/Intent;)Ljava/util/List;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 94
    .local v0, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    return-object v0
.end method

.method private getMaxHeight()I
    .locals 3

    .prologue
    .line 86
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0a0483

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 87
    .local v0, "maxHeight":I
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mSharableAppsList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mSharableAppsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xa

    if-gt v1, v2, :cond_1

    :cond_0
    const/4 v0, -0x2

    .end local v0    # "maxHeight":I
    :cond_1
    return v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mIntent:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->createSharableAppsList(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mSharableAppsList:Ljava/util/List;

    .line 64
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    const v1, 0x7f030053

    invoke-direct {v0, p0, p1, v1}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;-><init>(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mAdapter:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mAdapter:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mSharableAppsList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;->setAppList(Ljava/util/List;)V

    .line 67
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->getMaxHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->setHeight(I)V

    .line 68
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0484

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->setVerticalOffset(I)V

    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0482

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->setWidth(I)V

    .line 70
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mAdapter:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$SharableAppsAdapter;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 71
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mAnchorView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 72
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->setModal(Z)V

    .line 73
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$1;-><init>(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 83
    return-void
.end method


# virtual methods
.method public setListItemClickListener(Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow;->mItemClickListener:Lcom/samsung/android/app/galaxyfinder/share/SharableAppsListPopupWindow$ListItemClickListener;

    .line 60
    return-void
.end method
