.class Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$1;
.super Landroid/database/ContentObserver;
.source "AbstractTagProbe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->registerContentObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 135
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->access$200(Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChange() mDbObserver uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe$1;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;

    invoke-virtual {v0, p2}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;->onChangeInternal(Landroid/net/Uri;)V

    .line 139
    return-void
.end method
