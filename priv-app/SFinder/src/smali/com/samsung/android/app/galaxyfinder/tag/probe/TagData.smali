.class public Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;
.super Ljava/lang/Object;
.source "TagData.java"


# instance fields
.field private cid:Ljava/lang/String;

.field private contenturi:Ljava/lang/String;

.field private createTime:Ljava/lang/String;

.field private encoded:I

.field private tagdata:Ljava/lang/String;

.field private tid:Ljava/lang/String;

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "tid"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "tagdata"    # Ljava/lang/String;
    .param p5, "contenturi"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->cid:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->tid:Ljava/lang/String;

    .line 26
    iput p3, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->type:I

    .line 27
    iput-object p4, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->tagdata:Ljava/lang/String;

    .line 28
    iput-object p5, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->contenturi:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getContenturi()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->contenturi:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->createTime:Ljava/lang/String;

    return-object v0
.end method

.method public getEncoded()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->encoded:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->cid:Ljava/lang/String;

    return-object v0
.end method

.method public getTagData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->tagdata:Ljava/lang/String;

    return-object v0
.end method

.method public getTid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->tid:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->type:I

    return v0
.end method

.method public setContenturi(Ljava/lang/String;)V
    .locals 0
    .param p1, "contenturi"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->contenturi:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setCreateTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "createTime"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->createTime:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setEncoded(I)V
    .locals 0
    .param p1, "encoded"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->encoded:I

    .line 99
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->cid:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setTagData(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->tagdata:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setTid(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->tid:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagData;->type:I

    .line 92
    return-void
.end method
