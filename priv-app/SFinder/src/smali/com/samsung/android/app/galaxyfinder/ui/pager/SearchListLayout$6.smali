.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;
.super Ljava/lang/Object;
.source "SearchListLayout.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)V
    .locals 0

    .prologue
    .line 413
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 418
    if-eq p9, p5, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrameHeight:I
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1100(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 419
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;)V

    const-wide/16 v2, 0x96

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 435
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->getHeight()I

    move-result v1

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->mScrollViewFrameHeight:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->access$1102(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;I)I

    .line 437
    :cond_0
    return-void
.end method
