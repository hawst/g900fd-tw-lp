.class Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$8;
.super Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;
.source "VoiceInputActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->updateProcessing()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;Landroid/graphics/drawable/AnimationDrawable;)V
    .locals 0
    .param p2, "x0"    # Landroid/graphics/drawable/AnimationDrawable;

    .prologue
    .line 616
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/voicesearch/CustomAnimationDrawable;-><init>(Landroid/graphics/drawable/AnimationDrawable;)V

    return-void
.end method


# virtual methods
.method onAnimationFinish()V
    .locals 4

    .prologue
    .line 619
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$700(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 620
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$8;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mImageViewAnimationRecognizing:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$700(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    # setter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorRecognizing:Landroid/view/ViewPropertyAnimator;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$802(Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;

    .line 621
    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorRecognizing:Landroid/view/ViewPropertyAnimator;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$800()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$8$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$8$1;-><init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$8;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 639
    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mAnimatorRecognizing:Landroid/view/ViewPropertyAnimator;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$800()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 640
    return-void
.end method
