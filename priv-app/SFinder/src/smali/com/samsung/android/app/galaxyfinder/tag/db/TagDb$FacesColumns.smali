.class public interface abstract Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$FacesColumns;
.super Ljava/lang/Object;
.source "TagDb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacesColumns"
.end annotation


# static fields
.field public static final FACES_ID:Ljava/lang/String; = "face_id"

.field public static final IMAGE_ID:Ljava/lang/String; = "image_id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PERSON_ID:Ljava/lang/String; = "person_id"
