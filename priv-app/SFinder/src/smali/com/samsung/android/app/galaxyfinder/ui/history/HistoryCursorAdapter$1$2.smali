.class Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "HistoryCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;

.field final synthetic val$delbtn:Landroid/view/View;

.field final synthetic val$dismissView:Landroid/view/View;

.field final synthetic val$dismissViewLp:Landroid/view/ViewGroup$LayoutParams;

.field final synthetic val$originalHeight:I

.field final synthetic val$prevDiv:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;ILandroid/view/View;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$dismissView:Landroid/view/View;

    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$dismissViewLp:Landroid/view/ViewGroup$LayoutParams;

    iput p4, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$originalHeight:I

    iput-object p5, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$delbtn:Landroid/view/View;

    iput-object p6, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$prevDiv:Landroid/widget/ImageView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 219
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$dismissView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 220
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$dismissViewLp:Landroid/view/ViewGroup$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$originalHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 221
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$dismissView:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$dismissViewLp:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 223
    const-string v0, "animator"

    const-string v1, "onAnimationEnd"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$delbtn:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 225
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mIsDoingAnim:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$002(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;Z)Z

    .line 226
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    const/16 v1, 0x1f

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->disabledPosition:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$502(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;I)I

    .line 228
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$prevDiv:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->val$prevDiv:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->val$keyword:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->deleteHistoryItem(Ljava/lang/String;)I

    .line 233
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->getRecentHistoryItems(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 235
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 236
    return-void
.end method
