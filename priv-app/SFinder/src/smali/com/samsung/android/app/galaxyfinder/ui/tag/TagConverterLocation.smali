.class public Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterLocation;
.super Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;
.source "TagConverterLocation.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/tag/AbstractTagConverter;-><init>(Landroid/content/Context;)V

    .line 18
    return-void
.end method


# virtual methods
.method public convertToMeaningValue(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 18
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 52
    const/4 v11, 0x0

    .line 54
    .local v11, "result":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 55
    const-string v8, "value"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 57
    .local v16, "value":Ljava/lang/String;
    new-instance v15, Ljava/util/StringTokenizer;

    const-string v8, ","

    move-object/from16 v0, v16

    invoke-direct {v15, v0, v8}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .local v15, "tokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v13

    .line 60
    .local v13, "sLatitude":Ljava/lang/String;
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v14

    .line 62
    .local v14, "sLongitude":Ljava/lang/String;
    new-instance v3, Landroid/location/Geocoder;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterLocation;->mContext:Landroid/content/Context;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v3, v8, v0}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 65
    .local v3, "geocoder":Landroid/location/Geocoder;
    :try_start_0
    invoke-static {v13}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 66
    .local v4, "dLatitude":D
    invoke-static {v14}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 68
    .local v6, "dLongitude":D
    const/4 v8, 0x1

    invoke-virtual/range {v3 .. v8}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v2

    .line 70
    .local v2, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 71
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/location/Address;

    invoke-virtual {v8}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v10

    .line 73
    .local v10, "geoaddress":Ljava/lang/String;
    new-instance v12, Landroid/os/Bundle;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 75
    .end local v11    # "result":Landroid/os/Bundle;
    .local v12, "result":Landroid/os/Bundle;
    :try_start_1
    const-string v8, "encoding"

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v8, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 76
    const-string v8, "value"

    invoke-virtual {v12, v8, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v11, v12

    .line 85
    .end local v2    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v3    # "geocoder":Landroid/location/Geocoder;
    .end local v4    # "dLatitude":D
    .end local v6    # "dLongitude":D
    .end local v10    # "geoaddress":Ljava/lang/String;
    .end local v12    # "result":Landroid/os/Bundle;
    .end local v13    # "sLatitude":Ljava/lang/String;
    .end local v14    # "sLongitude":Ljava/lang/String;
    .end local v15    # "tokenizer":Ljava/util/StringTokenizer;
    .end local v16    # "value":Ljava/lang/String;
    .restart local v11    # "result":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-object v11

    .line 78
    .restart local v3    # "geocoder":Landroid/location/Geocoder;
    .restart local v13    # "sLatitude":Ljava/lang/String;
    .restart local v14    # "sLongitude":Ljava/lang/String;
    .restart local v15    # "tokenizer":Ljava/util/StringTokenizer;
    .restart local v16    # "value":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 79
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :goto_1
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 80
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v9

    .line 81
    .local v9, "e":Ljava/io/IOException;
    :goto_2
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 80
    .end local v9    # "e":Ljava/io/IOException;
    .end local v11    # "result":Landroid/os/Bundle;
    .restart local v2    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .restart local v4    # "dLatitude":D
    .restart local v6    # "dLongitude":D
    .restart local v10    # "geoaddress":Ljava/lang/String;
    .restart local v12    # "result":Landroid/os/Bundle;
    :catch_2
    move-exception v9

    move-object v11, v12

    .end local v12    # "result":Landroid/os/Bundle;
    .restart local v11    # "result":Landroid/os/Bundle;
    goto :goto_2

    .line 78
    .end local v11    # "result":Landroid/os/Bundle;
    .restart local v12    # "result":Landroid/os/Bundle;
    :catch_3
    move-exception v9

    move-object v11, v12

    .end local v12    # "result":Landroid/os/Bundle;
    .restart local v11    # "result":Landroid/os/Bundle;
    goto :goto_1
.end method

.method public convertToRawValue(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 12
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 22
    const/4 v4, 0x0

    .line 24
    .local v4, "result":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 25
    const-string v7, "value"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 27
    .local v6, "value":Ljava/lang/String;
    new-instance v2, Landroid/location/Geocoder;

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagConverterLocation;->mContext:Landroid/content/Context;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v2, v7, v8}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 30
    .local v2, "geocoder":Landroid/location/Geocoder;
    const/4 v7, 0x1

    :try_start_0
    invoke-virtual {v2, v6, v7}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 32
    .local v0, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 33
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/Address;

    invoke-virtual {v7}, Landroid/location/Address;->getLatitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v7, 0x0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/Address;

    invoke-virtual {v7}, Landroid/location/Address;->getLongitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 36
    .local v3, "geolocation":Ljava/lang/String;
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .end local v4    # "result":Landroid/os/Bundle;
    .local v5, "result":Landroid/os/Bundle;
    :try_start_1
    const-string v7, "encoding"

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 39
    const-string v7, "value"

    invoke-virtual {v5, v7, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v4, v5

    .line 47
    .end local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v2    # "geocoder":Landroid/location/Geocoder;
    .end local v3    # "geolocation":Ljava/lang/String;
    .end local v5    # "result":Landroid/os/Bundle;
    .end local v6    # "value":Ljava/lang/String;
    .restart local v4    # "result":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-object v4

    .line 41
    .restart local v2    # "geocoder":Landroid/location/Geocoder;
    .restart local v6    # "value":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 42
    .local v1, "e":Ljava/io/IOException;
    :goto_1
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 41
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "result":Landroid/os/Bundle;
    .restart local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .restart local v3    # "geolocation":Ljava/lang/String;
    .restart local v5    # "result":Landroid/os/Bundle;
    :catch_1
    move-exception v1

    move-object v4, v5

    .end local v5    # "result":Landroid/os/Bundle;
    .restart local v4    # "result":Landroid/os/Bundle;
    goto :goto_1
.end method
