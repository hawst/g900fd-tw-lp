.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;
.super Ljava/lang/Object;
.source "ItemListNoteAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SNoteItemListViewHolder"
.end annotation


# instance fields
.field public ivCloud:Landroid/widget/ImageView;

.field public ivFavorite:Landroid/widget/ImageView;

.field public ivIcon:Landroid/widget/ImageView;

.field public ivLock:Landroid/widget/ImageView;

.field public ivRecord:Landroid/widget/ImageView;

.field public ivTag:Landroid/widget/ImageView;

.field public tvDesc:Landroid/widget/TextView;

.field public tvTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 153
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 155
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    .line 157
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivTag:Landroid/widget/ImageView;

    .line 159
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivRecord:Landroid/widget/ImageView;

    .line 161
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivFavorite:Landroid/widget/ImageView;

    .line 163
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivCloud:Landroid/widget/ImageView;

    .line 165
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListNoteAdapter$SNoteItemListViewHolder;->ivLock:Landroid/widget/ImageView;

    return-void
.end method
