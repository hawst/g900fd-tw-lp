.class public Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;
.super Landroid/app/Service;
.source "TagReadyService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mSearchManager:Landroid/app/SearchManager;

.field private mTagSearchApp:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/tag/TagReady;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mSearchManager:Landroid/app/SearchManager;

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mTagSearchApp:Ljava/util/HashMap;

    return-void
.end method

.method private addTagReady(Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/TagReady;)V
    .locals 1
    .param p1, "pkgKey"    # Ljava/lang/String;
    .param p2, "aTagReady"    # Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mTagSearchApp:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mSearchManager:Landroid/app/SearchManager;

    .line 33
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->startTagReady()V

    .line 34
    return-void
.end method

.method private isSupportedTag(Landroid/app/SearchableInfo;)Z
    .locals 2
    .param p1, "searchable"    # Landroid/app/SearchableInfo;

    .prologue
    .line 96
    if-eqz p1, :cond_0

    .line 97
    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getAdvancedSearchType()Ljava/lang/String;

    move-result-object v0

    .line 98
    .local v0, "tagSearch":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "tagSearch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getTagProviderUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getTagFilters()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 100
    const/4 v1, 0x1

    .line 103
    .end local v0    # "tagSearch":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startTagReady()V
    .locals 6

    .prologue
    .line 70
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mSearchManager:Landroid/app/SearchManager;

    if-nez v4, :cond_1

    .line 89
    :cond_0
    return-void

    .line 73
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mSearchManager:Landroid/app/SearchManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/SearchManager;->getSearchablesInInsightSearch(Z)Ljava/util/List;

    move-result-object v3

    .line 74
    .local v3, "searchables":Ljava/util/List;, "Ljava/util/List<Landroid/app/SearchableInfo;>;"
    if-eqz v3, :cond_0

    .line 78
    invoke-static {p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;

    move-result-object v0

    .line 79
    .local v0, "factory":Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/SearchableInfo;

    .line 80
    .local v2, "searchable":Landroid/app/SearchableInfo;
    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 82
    invoke-direct {p0, v2}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->isSupportedTag(Landroid/app/SearchableInfo;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 83
    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyFactory;->getTagReady(Landroid/app/SearchableInfo;)Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->addTagReady(Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/TagReady;)V

    goto :goto_0

    .line 86
    :cond_3
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->TAG:Ljava/lang/String;

    const-string v5, "startTagReady() searchable SuggestPackage is NULL"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopTagReady()V
    .locals 3

    .prologue
    .line 107
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mTagSearchApp:Ljava/util/HashMap;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mTagSearchApp:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 108
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mTagSearchApp:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 109
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/TagReady;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 111
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/TagReady;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 112
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->onStop()V

    goto :goto_0

    .line 115
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/TagReady;>;"
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mTagSearchApp:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 117
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/galaxyfinder/tag/TagReady;>;>;"
    :cond_2
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 128
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 27
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->init()V

    .line 28
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setForegroundService(Z)V

    .line 29
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->stopTagReady()V

    .line 122
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setForegroundService(Z)V

    .line 123
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 124
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x1

    .line 38
    if-nez p1, :cond_1

    .line 39
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->TAG:Ljava/lang/String;

    const-string v5, "onStartCommand() intent is NULL"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    :cond_0
    :goto_0
    return v7

    .line 43
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 45
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->TAG:Ljava/lang/String;

    const-string v5, "onStartCommand() action is NULL"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 48
    :cond_2
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onStartCommand() action = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const-string v4, "com.samsung.android.app.galaxyfinder.tag.start_service"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.samsung.android.app.galaxyfinder.tag.reload_service"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 52
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->startTagReady()V

    goto :goto_0

    .line 53
    :cond_4
    const-string v4, "com.samsung.android.app.galaxyfinder.tag.start_full_sync"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 54
    const-string v4, "com.samsung.android.app.galaxyfinder.TagServiceState"

    invoke-virtual {p1, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 55
    .local v3, "state":I
    if-ne v3, v8, :cond_5

    .line 56
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->TAG:Ljava/lang/String;

    const-string v5, "onStartCommand() WRONG tag service state"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 59
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mTagSearchApp:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 60
    .local v2, "packageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->mTagSearchApp:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    invoke-virtual {v4, v3}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->requestFullSyncTagReady(I)V

    goto :goto_1

    .line 63
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "state":I
    :cond_6
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;->TAG:Ljava/lang/String;

    const-string v5, "onStartCommand() action is WRONG value"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
