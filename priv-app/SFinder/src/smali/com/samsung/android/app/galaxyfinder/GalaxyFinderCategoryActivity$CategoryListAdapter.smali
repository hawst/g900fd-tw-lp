.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "GalaxyFinderCategoryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CategoryListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 465
    .local p4, "objects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;>;"
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    .line 466
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 467
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 468
    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 530
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v11, 0x0

    const v10, 0x7f0b00c8

    const/4 v8, 0x0

    .line 473
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCategoryList:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$500(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;

    .line 475
    .local v3, "item":Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;
    if-eqz v3, :cond_3

    .line 476
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->getCategoryType()I

    move-result v0

    .line 478
    .local v0, "categoryType":I
    const/4 v7, 0x3

    if-ne v7, v0, :cond_1

    .line 479
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f03005b

    invoke-virtual {v7, v9, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 482
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCheckBox:Landroid/view/View;
    invoke-static {v7, v9}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$602(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Landroid/view/View;)Landroid/view/View;

    .line 483
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCheckBox:Landroid/view/View;
    invoke-static {v7}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Checkable;

    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectedCount:I
    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mSelectableCategorySize:I
    invoke-static {v10}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$100(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)I

    move-result v10

    if-ne v9, v10, :cond_0

    const/4 v8, 0x1

    :cond_0
    invoke-interface {v7, v8}, Landroid/widget/Checkable;->setChecked(Z)V

    move-object v1, p2

    .line 525
    .end local v0    # "categoryType":I
    .end local p2    # "convertView":Landroid/view/View;
    .local v1, "convertView":Landroid/view/View;
    :goto_0
    return-object v1

    .line 486
    .end local v1    # "convertView":Landroid/view/View;
    .restart local v0    # "categoryType":I
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_1
    const/4 v7, 0x2

    if-ne v0, v7, :cond_2

    move-object v5, v3

    .line 487
    check-cast v5, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SeparatorCategory;

    .line 489
    .local v5, "separator":Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SeparatorCategory;
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f030030

    invoke-virtual {v7, v9, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 491
    invoke-virtual {p2, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 492
    invoke-virtual {p2, v11}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 493
    invoke-virtual {p2, v8}, Landroid/view/View;->setLongClickable(Z)V

    .line 495
    const v7, 0x7f0b004a

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 498
    .local v4, "sectionView":Landroid/widget/TextView;
    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SeparatorCategory;->getCategoryLabel()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, p2

    .line 500
    .end local p2    # "convertView":Landroid/view/View;
    .restart local v1    # "convertView":Landroid/view/View;
    goto :goto_0

    .line 502
    .end local v1    # "convertView":Landroid/view/View;
    .end local v4    # "sectionView":Landroid/widget/TextView;
    .end local v5    # "separator":Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$SeparatorCategory;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f03005a

    invoke-virtual {v7, v9, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 506
    const v7, 0x7f0b00ca

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 507
    .local v6, "title":Landroid/widget/TextView;
    const v7, 0x7f0b00c9

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 510
    .local v2, "icon":Landroid/widget/ImageView;
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->getCategoryLabel()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    if-nez v0, :cond_4

    .line 514
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->getCategoryIcon()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 520
    :goto_1
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCheckBox:Landroid/view/View;
    invoke-static {v7, v8}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$602(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Landroid/view/View;)Landroid/view/View;

    .line 521
    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->mCheckBox:Landroid/view/View;
    invoke-static {v7}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$600(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Checkable;

    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryListAdapter;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->getCatetoryId()Ljava/lang/String;

    move-result-object v9

    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->isCheckedPackage(Ljava/lang/String;)Z
    invoke-static {v8, v9}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;->access$700(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;Ljava/lang/String;)Z

    move-result v8

    invoke-interface {v7, v8}, Landroid/widget/Checkable;->setChecked(Z)V

    .end local v0    # "categoryType":I
    .end local v2    # "icon":Landroid/widget/ImageView;
    .end local v6    # "title":Landroid/widget/TextView;
    :cond_3
    move-object v1, p2

    .line 525
    .end local p2    # "convertView":Landroid/view/View;
    .restart local v1    # "convertView":Landroid/view/View;
    goto :goto_0

    .line 516
    .end local v1    # "convertView":Landroid/view/View;
    .restart local v0    # "categoryType":I
    .restart local v2    # "icon":Landroid/widget/ImageView;
    .restart local v6    # "title":Landroid/widget/TextView;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_4
    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity$CategoryItem;->getCategoryIcon()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_1
.end method
