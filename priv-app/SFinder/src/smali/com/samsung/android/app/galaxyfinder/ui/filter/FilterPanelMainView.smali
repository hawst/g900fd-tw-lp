.class public Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
.super Landroid/widget/LinearLayout;
.source "FilterPanelMainView.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;
.implements Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;
.implements Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$FilterGroupInfo;,
        Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;,
        Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$FilterState;
    }
.end annotation


# instance fields
.field private mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

.field private mFilterDetailView:Landroid/view/ViewGroup;

.field private mFilterGroupView:Landroid/view/ViewGroup;

.field private mFilterHorizontalView:Landroid/view/ViewGroup;

.field private mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

.field private mGroupClickListener:Landroid/view/View$OnClickListener;

.field private mQuery:Ljava/lang/String;

.field private mRootView:Landroid/view/ViewGroup;

.field private mSelectedFilterItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedItemView:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

.field private mSizeChagnedListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    .line 38
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mQuery:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedItemView:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterHorizontalView:Landroid/view/ViewGroup;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;

    .line 50
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSizeChagnedListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    .line 56
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mGroupClickListener:Landroid/view/View$OnClickListener;

    .line 107
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->init()V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    .line 38
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mQuery:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedItemView:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterHorizontalView:Landroid/view/ViewGroup;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;

    .line 50
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSizeChagnedListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    .line 56
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mGroupClickListener:Landroid/view/View$OnClickListener;

    .line 113
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->init()V

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 117
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    .line 38
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mQuery:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedItemView:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterHorizontalView:Landroid/view/ViewGroup;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;

    .line 50
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSizeChagnedListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    .line 56
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mGroupClickListener:Landroid/view/View$OnClickListener;

    .line 119
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->init()V

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getSelectedItemIndex(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;)Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->startFilterGroupAnimation(I)V

    return-void
.end method

.method private addSelectedItem(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 1
    .param p1, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    :cond_0
    return-void
.end method

.method private clearSelectedItems()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 294
    :cond_0
    return-void
.end method

.method private getSelectedItemIndex(Landroid/view/View;)I
    .locals 2
    .param p1, "group"    # Landroid/view/View;

    .prologue
    .line 123
    const/4 v0, -0x1

    .line 124
    .local v0, "index":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 143
    :goto_0
    return v0

    .line 126
    :pswitch_0
    const/4 v0, 0x0

    .line 127
    goto :goto_0

    .line 129
    :pswitch_1
    const/4 v0, 0x1

    .line 130
    goto :goto_0

    .line 132
    :pswitch_2
    const/4 v0, 0x2

    .line 133
    goto :goto_0

    .line 135
    :pswitch_3
    const/4 v0, 0x3

    .line 136
    goto :goto_0

    .line 138
    :pswitch_4
    const/4 v0, 0x4

    .line 139
    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0000
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getTagDataIndex(Ljava/lang/String;)I
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 750
    const-string v0, "notes"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 751
    const/4 v0, 0x0

    .line 763
    :goto_0
    return v0

    .line 752
    :cond_0
    const-string v0, "communication"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 753
    const/4 v0, 0x1

    goto :goto_0

    .line 754
    :cond_1
    const-string v0, "help"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 755
    const/4 v0, 0x2

    goto :goto_0

    .line 756
    :cond_2
    const-string v0, "images"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 757
    const/4 v0, 0x3

    goto :goto_0

    .line 758
    :cond_3
    const-string v0, "music"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 759
    const/4 v0, 0x4

    goto :goto_0

    .line 760
    :cond_4
    const-string v0, "videos"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 761
    const/4 v0, 0x5

    goto :goto_0

    .line 763
    :cond_5
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private init()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 165
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.spen_usp"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 167
    .local v0, "bSpenEnabled":Z
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 169
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03000a

    invoke-virtual {v1, v2, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    .line 170
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f0b0003

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f0b0004

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f0b0002

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    if-eqz v0, :cond_0

    .line 179
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f0b0001

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f0b0026

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterHorizontalView:Landroid/view/ViewGroup;

    .line 184
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f0b0028

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    .line 185
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f0b0029

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;

    .line 187
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f0b0027

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedItemView:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    .line 190
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedItemView:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->setCallback(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$Callback;)V

    .line 191
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedItemView:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->setData(Ljava/util/ArrayList;)V

    .line 193
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f0b002a

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    .line 194
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    sget v3, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->FILTER_PAGE_INDEX_MAX:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setOffscreenPageLimit(I)V

    .line 195
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setAnchorView(Landroid/view/View;)V

    .line 196
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->init(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView$Callback;)V

    .line 197
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v2, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 199
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->addView(Landroid/view/View;)V

    .line 201
    invoke-direct {p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->startFilterGroupAnimation(I)V

    .line 203
    return-void
.end method

.method private isInitialState()Z
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeSelectedItem(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 1
    .param p1, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 288
    :cond_0
    return-void
.end method

.method private startFilterGroupAnimation(I)V
    .locals 7
    .param p1, "i"    # I

    .prologue
    const/4 v6, 0x0

    .line 206
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-lt p1, v4, :cond_0

    .line 238
    :goto_0
    return-void

    .line 209
    :cond_0
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v4, 0x1

    invoke-direct {v1, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 211
    .local v1, "rootSet":Landroid/view/animation/AnimationSet;
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v4, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 212
    .local v3, "view":Landroid/view/View;
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 214
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/high16 v4, -0x3f000000    # -8.0f

    invoke-direct {v2, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 215
    .local v2, "trans":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v4, 0x82

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 217
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v6, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 218
    .local v0, "alpha":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v4, 0x5a

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 219
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 220
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 221
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$2;

    invoke-direct {v4, p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;I)V

    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 235
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 236
    invoke-virtual {v3, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private updateItemsInDetailPagerView()V
    .locals 3

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->reloadAllItems()V

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->updateItemState(Ljava/util/ArrayList;Z)V

    goto :goto_0
.end method

.method private updateSelectedItems()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedItemView:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->setData(Ljava/util/ArrayList;)V

    .line 298
    return-void
.end method


# virtual methods
.method public addSelectedItems(Ljava/util/ArrayList;Z)V
    .locals 3
    .param p2, "notify"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 328
    .local p1, "datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 329
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 330
    .local v0, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->addSelectedItem(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    goto :goto_0

    .line 333
    .end local v0    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->updateSelectedItems()V

    .line 335
    if-eqz p2, :cond_1

    .line 336
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    if-eqz v2, :cond_1

    .line 337
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    invoke-interface {v2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;->onActionItemsAdded(Ljava/util/ArrayList;)V

    .line 341
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->updateItemsInDetailPagerView()V

    .line 343
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method public closeAllOpenedPanel()V
    .locals 4

    .prologue
    .line 316
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 318
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 319
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 321
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 322
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v3, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 318
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 325
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public displayAllgroupFIlter()V
    .locals 3

    .prologue
    .line 309
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 310
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 311
    .local v1, "view":Landroid/view/View;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 309
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 313
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public getDetailPanelHeight()I
    .locals 3

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getPaddingBottom()I

    move-result v2

    add-int v0, v1, v2

    .line 487
    .local v0, "height":I
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v0, v1

    .line 489
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->isExistingSelectedPanel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v0, v1

    .line 493
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->isExistingSelectedFilter()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 494
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterHorizontalView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v0, v1

    .line 497
    :cond_1
    return v0
.end method

.method public getSelectedFilterInfo()Landroid/os/Bundle;
    .locals 25

    .prologue
    .line 511
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 513
    .local v2, "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v23

    if-nez v23, :cond_16

    .line 514
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 515
    .local v5, "categoryBuilder":Ljava/lang/StringBuilder;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 516
    .local v17, "tagBuilder":Ljava/lang/StringBuilder;
    const-string v22, ""

    .line 517
    .local v22, "timeSpan":Ljava/lang/String;
    const/4 v6, 0x0

    .line 518
    .local v6, "categoryList":Ljava/lang/String;
    const/16 v18, 0x0

    .line 520
    .local v18, "tagList":Ljava/lang/String;
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 522
    .local v10, "filterBundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_f

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 523
    .local v7, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isCategoryTag()Z

    move-result v23

    if-eqz v23, :cond_2

    .line 525
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v23

    if-lez v23, :cond_0

    .line 526
    const-string v23, ","

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 529
    :cond_0
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getRawTagData()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    :cond_1
    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 598
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 599
    goto :goto_0

    .line 530
    :cond_2
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isFilterTag()Z

    move-result v23

    if-eqz v23, :cond_3

    .line 532
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getSearchFilter()Ljava/lang/String;

    move-result-object v9

    .line 533
    .local v9, "filter":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getRawTagData()Ljava/lang/String;

    move-result-object v3

    .line 535
    .local v3, "categories":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 536
    new-instance v15, Ljava/util/StringTokenizer;

    const-string v23, ","

    move-object/from16 v0, v23

    invoke-direct {v15, v3, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    .local v15, "st":Ljava/util/StringTokenizer;
    if-eqz v9, :cond_1

    .line 540
    :goto_2
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v23

    if-eqz v23, :cond_1

    .line 541
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 542
    .local v4, "category":Ljava/lang/String;
    invoke-virtual {v10, v4, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 546
    .end local v3    # "categories":Ljava/lang/String;
    .end local v4    # "category":Ljava/lang/String;
    .end local v9    # "filter":Ljava/lang/String;
    .end local v15    # "st":Ljava/util/StringTokenizer;
    :cond_3
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isTimeTag()Z

    move-result v23

    if-eqz v23, :cond_7

    .line 548
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getRawTagData()Ljava/lang/String;

    move-result-object v23

    const-string v24, "\\,"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 549
    .local v21, "time":[Ljava/lang/String;
    const/16 v23, 0x0

    aget-object v16, v21, v23

    .line 550
    .local v16, "start":Ljava/lang/String;
    const/16 v23, 0x1

    aget-object v8, v21, v23

    .line 552
    .local v8, "end":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_4

    .line 553
    const-string v23, ","

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 555
    .local v13, "prev":[Ljava/lang/String;
    const/16 v23, 0x0

    aget-object v23, v13, v23

    const/16 v24, 0x0

    aget-object v24, v21, v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v23

    if-lez v23, :cond_5

    const/16 v23, 0x0

    aget-object v16, v21, v23

    .line 556
    :goto_3
    const/16 v23, 0x1

    aget-object v23, v13, v23

    const/16 v24, 0x1

    aget-object v24, v21, v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v23

    if-gez v23, :cond_6

    const/16 v23, 0x1

    aget-object v8, v21, v23

    .line 559
    .end local v13    # "prev":[Ljava/lang/String;
    :cond_4
    :goto_4
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const/16 v24, 0x2c

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 560
    goto/16 :goto_1

    .line 555
    .restart local v13    # "prev":[Ljava/lang/String;
    :cond_5
    const/16 v23, 0x0

    aget-object v16, v13, v23

    goto :goto_3

    .line 556
    :cond_6
    const/16 v23, 0x1

    aget-object v8, v13, v23

    goto :goto_4

    .line 562
    .end local v8    # "end":Ljava/lang/String;
    .end local v13    # "prev":[Ljava/lang/String;
    .end local v16    # "start":Ljava/lang/String;
    .end local v21    # "time":[Ljava/lang/String;
    :cond_7
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagTypeFlags()I

    move-result v23

    and-int/lit8 v23, v23, 0x8

    if-lez v23, :cond_9

    .line 563
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->length()I

    move-result v23

    if-lez v23, :cond_8

    .line 564
    const/16 v23, 0x7c

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 566
    :cond_8
    const-string v23, "userdef="

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    :cond_9
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagTypeFlags()I

    move-result v23

    and-int/lit8 v23, v23, 0x1

    if-lez v23, :cond_b

    .line 571
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->length()I

    move-result v23

    if-lez v23, :cond_a

    .line 572
    const/16 v23, 0x7c

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 574
    :cond_a
    const-string v23, "weather="

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 575
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ","

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getRawTagData()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 579
    :cond_b
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagTypeFlags()I

    move-result v23

    and-int/lit8 v23, v23, 0x2

    if-lez v23, :cond_d

    .line 580
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->length()I

    move-result v23

    if-lez v23, :cond_c

    .line 581
    const/16 v23, 0x7c

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 583
    :cond_c
    const-string v23, "people="

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getRawTagData()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ","

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getRawTagData()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    :cond_d
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->isLocationTag()Z

    move-result v23

    if-eqz v23, :cond_1

    .line 589
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->length()I

    move-result v23

    if-lez v23, :cond_e

    .line 590
    const/16 v23, 0x7c

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 592
    :cond_e
    const-string v23, "location="

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 593
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 601
    .end local v7    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_f
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_10

    .line 602
    const-string v23, "filter_group_time"

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_10
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_11

    .line 606
    const-string v23, "filter_group_category"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_11
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_12

    .line 610
    const-string v23, "filter_group_user"

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :cond_12
    invoke-virtual {v10}, Landroid/os/Bundle;->isEmpty()Z

    move-result v23

    if-nez v23, :cond_16

    .line 614
    const-string v23, "filter_group_category_filters"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0, v10}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 616
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 617
    .local v20, "targetList":Ljava/lang/StringBuilder;
    new-instance v19, Ljava/lang/String;

    invoke-direct/range {v19 .. v19}, Ljava/lang/String;-><init>()V

    .line 618
    .local v19, "targetCategories":Ljava/lang/String;
    new-instance v14, Ljava/lang/String;

    invoke-direct {v14}, Ljava/lang/String;-><init>()V

    .line 620
    .local v14, "refinedCategories":Ljava/lang/String;
    invoke-virtual {v10}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_14

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 621
    .local v12, "key":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->length()I

    move-result v23

    if-lez v23, :cond_13

    .line 622
    const/16 v23, 0x2c

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 625
    :cond_13
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 628
    .end local v12    # "key":Ljava/lang/String;
    :cond_14
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 630
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_17

    .line 631
    move-object/from16 v14, v19

    .line 651
    :cond_15
    :goto_6
    const-string v23, "filter_group_category"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    .end local v5    # "categoryBuilder":Ljava/lang/StringBuilder;
    .end local v6    # "categoryList":Ljava/lang/String;
    .end local v10    # "filterBundle":Landroid/os/Bundle;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v14    # "refinedCategories":Ljava/lang/String;
    .end local v17    # "tagBuilder":Ljava/lang/StringBuilder;
    .end local v18    # "tagList":Ljava/lang/String;
    .end local v19    # "targetCategories":Ljava/lang/String;
    .end local v20    # "targetList":Ljava/lang/StringBuilder;
    .end local v22    # "timeSpan":Ljava/lang/String;
    :cond_16
    return-object v2

    .line 633
    .restart local v5    # "categoryBuilder":Ljava/lang/StringBuilder;
    .restart local v6    # "categoryList":Ljava/lang/String;
    .restart local v10    # "filterBundle":Landroid/os/Bundle;
    .restart local v11    # "i$":Ljava/util/Iterator;
    .restart local v14    # "refinedCategories":Ljava/lang/String;
    .restart local v17    # "tagBuilder":Ljava/lang/StringBuilder;
    .restart local v18    # "tagList":Ljava/lang/String;
    .restart local v19    # "targetCategories":Ljava/lang/String;
    .restart local v20    # "targetList":Ljava/lang/StringBuilder;
    .restart local v22    # "timeSpan":Ljava/lang/String;
    :cond_17
    new-instance v15, Ljava/util/StringTokenizer;

    const-string v23, ","

    move-object/from16 v0, v23

    invoke-direct {v15, v6, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    .restart local v15    # "st":Ljava/util/StringTokenizer;
    :cond_18
    :goto_7
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v23

    if-eqz v23, :cond_1a

    .line 636
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 637
    .restart local v12    # "key":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_18

    .line 638
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v23

    if-lez v23, :cond_19

    .line 639
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const/16 v24, 0x2c

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 642
    :cond_19
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto :goto_7

    .line 646
    .end local v12    # "key":Ljava/lang/String;
    :cond_1a
    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_15

    .line 647
    const-string v14, "none"

    goto :goto_6
.end method

.method public getTagPanelHeight()I
    .locals 4

    .prologue
    .line 501
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getPaddingBottom()I

    move-result v2

    add-int v0, v1, v2

    .line 503
    .local v0, "height":I
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a03ce

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 506
    return v0
.end method

.method public hideGroupPager(IZ)V
    .locals 4
    .param p1, "item"    # I
    .param p2, "bGone"    # Z

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x4

    .line 768
    if-eqz p2, :cond_2

    move v0, v1

    .line 770
    .local v0, "visibility":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->isOnlySelectedHandwriting()Z

    move-result v3

    if-nez v3, :cond_0

    if-ne p1, v2, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->isExistingSelectedPanel()Z

    move-result v2

    if-nez v2, :cond_3

    .line 772
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setVisibility(I)V

    .line 779
    :cond_1
    :goto_1
    return-void

    .end local v0    # "visibility":I
    :cond_2
    move v0, v2

    .line 768
    goto :goto_0

    .line 776
    .restart local v0    # "visibility":I
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->isExistingPager(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getCurrentItem()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 777
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setVisibility(I)V

    goto :goto_1
.end method

.method public initializeAllItems()V
    .locals 2

    .prologue
    .line 400
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->clearSelectedItems()V

    .line 401
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->updateSelectedItems()V

    .line 403
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setState(I)V

    .line 404
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->reloadAllItems()V

    .line 405
    return-void
.end method

.method public isExistingOpenedPanel()Z
    .locals 4

    .prologue
    .line 451
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 453
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 454
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 456
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 458
    const/4 v3, 0x1

    .line 462
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v3

    .line 453
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 462
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public isExistingSelectedFilter()Z
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExistingSelectedPanel()Z
    .locals 4

    .prologue
    .line 437
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 439
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 440
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 442
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 443
    const/4 v3, 0x1

    .line 447
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v3

    .line 439
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 447
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public isOnlySelectedHandwriting()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 466
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 468
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 469
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 471
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 472
    const/4 v4, 0x4

    if-eq v2, v4, :cond_1

    .line 480
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    :goto_1
    return v3

    .line 475
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1

    .line 468
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onActionHandwriting(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 378
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;->onActionHandwriting(Ljava/lang/String;)V

    .line 383
    :cond_0
    return-void
.end method

.method public onActionItemClick(Landroid/view/View;Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 357
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->addSelectedItem(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 363
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    if-eqz v0, :cond_0

    .line 364
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    invoke-interface {v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;->onActionItemAdded(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 370
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    invoke-interface {v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;->onActionItemChanged(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 372
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->updateSelectedItems()V

    .line 373
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->updateItemsInDetailPagerView()V

    .line 374
    return-void

    .line 360
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->removeSelectedItem(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    goto :goto_0

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    invoke-interface {v0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;->onActionItemRemoved(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    goto :goto_1
.end method

.method public onActionItemDelete(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
    .locals 1
    .param p1, "data"    # Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .prologue
    .line 388
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->removeSelectedItem(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 390
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;->onActionItemRemoved(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 392
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;->onActionItemChanged(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V

    .line 395
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->updateItemsInDetailPagerView()V

    .line 396
    return-void
.end method

.method public onClickProcess(Landroid/view/View;)V
    .locals 3
    .param p1, "group"    # Landroid/view/View;

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getSelectedItemIndex(Landroid/view/View;)I

    move-result v0

    .line 149
    .local v0, "index":I
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setCurrentItem(IZ)V

    .line 151
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;->onActionGroupChanged(I)V

    .line 154
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 11
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v10, 0x0

    .line 243
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a03e3

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 244
    .local v4, "t":I
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a03e0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 246
    .local v0, "b":I
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mRootView:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v8

    invoke-virtual {v6, v7, v4, v8, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 249
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a03de

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 252
    .local v3, "padding":I
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterHorizontalView:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterHorizontalView:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterHorizontalView:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterHorizontalView:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v9

    invoke-virtual {v6, v7, v8, v9, v3}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 256
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a03ca

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a03cb

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    invoke-virtual {v6, v7, v10, v8, v10}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 261
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a03cc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 264
    .local v5, "totalHeight":I
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 265
    .local v1, "filterDetailViewLp":Landroid/widget/LinearLayout$LayoutParams;
    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 268
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterDetailView:Landroid/view/ViewGroup;

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a03d9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 273
    .local v2, "minHeight":I
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v6, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setMinimumHeight(I)V

    .line 275
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 276
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 661
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 666
    return-void
.end method

.method public onPageSelected(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 670
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 672
    .local v0, "count":I
    if-ge p1, v0, :cond_2

    .line 674
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 675
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 677
    .local v1, "group":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f0b0001

    if-eq v3, v4, :cond_0

    .line 678
    if-ne v2, p1, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 674
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 678
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 682
    .end local v1    # "group":Landroid/view/View;
    .end local v2    # "i":I
    :cond_2
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 2
    .param p1, "newQuery"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 693
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mQuery:Ljava/lang/String;

    .line 695
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setState(I)V

    .line 698
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->reloadAllItems()V

    .line 702
    :cond_0
    return v1
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 686
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mQuery:Ljava/lang/String;

    .line 688
    const/4 v0, 0x0

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 347
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 349
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSizeChagnedListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSizeChagnedListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;->onSizeChanged(IIII)V

    .line 352
    :cond_0
    return-void
.end method

.method public onWritingBuddyCategoryUpdate(Z)V
    .locals 0
    .param p1, "bHandwriting"    # Z

    .prologue
    .line 714
    return-void
.end method

.method public onWritingBuddyMode(Ljava/lang/String;)V
    .locals 0
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 708
    return-void
.end method

.method public onWritingBuddySymbolQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "symbol"    # Ljava/lang/String;

    .prologue
    .line 719
    return-void
.end method

.method public setCustomFilterItems(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 409
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setCustomFilterItems(Ljava/util/ArrayList;)V

    .line 410
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSelectedFilterItems:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->updateItemState(Ljava/util/ArrayList;Z)V

    .line 411
    return-void
.end method

.method public setFilterState(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 414
    packed-switch p1, :pswitch_data_0

    .line 430
    :goto_0
    return-void

    .line 417
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->isInitialState()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setState(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    goto :goto_1

    .line 423
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setState(I)V

    goto :goto_0

    .line 414
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnActionListener(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mActionListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;

    .line 162
    return-void
.end method

.method public setOnSizeChangedListener(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mSizeChagnedListener:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnSizeChangedListener;

    .line 158
    return-void
.end method

.method public startSyncByLauncher([Ljava/lang/String;I)V
    .locals 11
    .param p1, "category"    # [Ljava/lang/String;
    .param p2, "time"    # I

    .prologue
    const/4 v10, 0x1

    .line 722
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 724
    .local v3, "datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getTagDataList(I)Ljava/util/ArrayList;

    move-result-object v7

    .line 727
    .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    move-object v1, p1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v0, v1, v4

    .line 728
    .local v0, "app":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->getTagDataIndex(Ljava/lang/String;)I

    move-result v5

    .line 730
    .local v5, "index":I
    const/4 v8, -0x1

    if-eq v5, v8, :cond_0

    .line 731
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 732
    .local v2, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 727
    .end local v2    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 736
    .end local v0    # "app":Ljava/lang/String;
    .end local v5    # "index":I
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->mFilterPagerView:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-virtual {v8, v10}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->getTagDataList(I)Ljava/util/ArrayList;

    move-result-object v7

    .line 738
    invoke-virtual {v7, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 739
    .restart local v2    # "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 741
    invoke-virtual {p0, v3, v10}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;->addSelectedItems(Ljava/util/ArrayList;Z)V

    .line 742
    return-void
.end method
