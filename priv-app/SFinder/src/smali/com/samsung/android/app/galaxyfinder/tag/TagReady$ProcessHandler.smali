.class final Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;
.super Landroid/os/Handler;
.source "TagReady.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/TagReady;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ProcessHandler"
.end annotation


# instance fields
.field private final mTagReady:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/tag/TagReady;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReady;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/TagReady;Landroid/os/Looper;Lcom/samsung/android/app/galaxyfinder/tag/TagReady;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "owner"    # Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    .line 38
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 39
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;->mTagReady:Ljava/lang/ref/WeakReference;

    .line 40
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 44
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;->mTagReady:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    .line 45
    .local v2, "owner":Lcom/samsung/android/app/galaxyfinder/tag/TagReady;
    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 50
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->access$000(Lcom/samsung/android/app/galaxyfinder/tag/TagReady;)Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->access$000(Lcom/samsung/android/app/galaxyfinder/tag/TagReady;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 51
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/tag/TagReady$ProcessHandler;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/TagReady;

    # getter for: Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->mTagProbeMap:Ljava/util/Map;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/tag/TagReady;->access$000(Lcom/samsung/android/app/galaxyfinder/tag/TagReady;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 52
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;

    .line 54
    .local v0, "aProbe":Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-interface {v0, v3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe;->requestFullSyncTagProbe(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;)V

    goto :goto_1

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
