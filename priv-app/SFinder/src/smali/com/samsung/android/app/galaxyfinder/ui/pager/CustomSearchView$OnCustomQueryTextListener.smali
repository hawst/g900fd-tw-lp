.class public interface abstract Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$OnCustomQueryTextListener;
.super Ljava/lang/Object;
.source "CustomSearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnCustomQueryTextListener"
.end annotation


# virtual methods
.method public abstract onQueryTextChange(Ljava/lang/String;)Z
.end method

.method public abstract onQueryTextSubmit(Ljava/lang/String;)Z
.end method

.method public abstract onWritingBuddyCategoryUpdate(Z)V
.end method

.method public abstract onWritingBuddyMode(Ljava/lang/String;)V
.end method

.method public abstract onWritingBuddySymbolQuery(Ljava/lang/String;)V
.end method
