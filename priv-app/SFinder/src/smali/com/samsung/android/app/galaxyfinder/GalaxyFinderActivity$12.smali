.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;
.super Ljava/lang/Object;
.source "GalaxyFinderActivity.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->setActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mCustomView:Landroid/view/View;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 1

    .prologue
    .line 1063
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->mCustomView:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1107
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1116
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1109
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # invokes: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->doShareVia()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1800(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V

    goto :goto_0

    .line 1107
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b00e5
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1085
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030006

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->mCustomView:Landroid/view/View;

    .line 1087
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->mCustomView:Landroid/view/View;

    const v2, 0x7f0b001a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12$1;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1097
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->mCustomView:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 1099
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1100
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f100005

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1102
    const/4 v1, 0x1

    return v1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1102(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 1080
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSearchListView:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchListLayout;->disableSelectionMode()V

    .line 1081
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 6
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x0

    .line 1069
    invoke-virtual {p1}, Landroid/view/ActionMode;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0b001c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1071
    .local v0, "title":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const v2, 0x7f0e006a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$12;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mSelectedItemCount:I
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$1000(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1073
    return v5
.end method
