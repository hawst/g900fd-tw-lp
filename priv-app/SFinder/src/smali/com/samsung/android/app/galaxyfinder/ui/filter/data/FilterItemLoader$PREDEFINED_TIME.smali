.class final enum Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;
.super Ljava/lang/Enum;
.source "FilterItemLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PREDEFINED_TIME"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

.field public static final enum NEXT_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

.field public static final enum PAST_MONTH:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

.field public static final enum PAST_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

.field public static final enum TODAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

.field public static final enum YESTERDAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const-string v1, "PAST_MONTH"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_MONTH:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const-string v1, "PAST_WEEK"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const-string v1, "YESTERDAY"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->YESTERDAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const-string v1, "TODAY"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->TODAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    const-string v1, "NEXT_WEEK"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->NEXT_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    .line 58
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_MONTH:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->PAST_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->YESTERDAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->TODAY:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->NEXT_WEEK:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    invoke-virtual {v0}, [Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterItemLoader$PREDEFINED_TIME;

    return-object v0
.end method
