.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListVideoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$1;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 22
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->TAG:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 18
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 31
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v8

    .line 32
    .local v8, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v5, 0x0

    .line 34
    .local v5, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 35
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v12

    .line 37
    .local v12, "title":Ljava/lang/String;
    if-nez p2, :cond_3

    .line 38
    sget-object v14, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v15, 0x7f03004c

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v14 .. v17}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 40
    new-instance v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;

    .end local v5    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;
    const/4 v14, 0x0

    invoke-direct {v5, v14}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$1;)V

    .line 42
    .restart local v5    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;
    const v14, 0x7f0b004f

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 43
    const v14, 0x7f0b00a3

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->tvDurationTime:Landroid/widget/TextView;

    .line 44
    const v14, 0x7f0b00a4

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    iput-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    .line 46
    sget-boolean v14, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v14, :cond_0

    .line 47
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v15, 0x1

    const/high16 v16, 0x41600000    # 14.0f

    invoke-virtual/range {v14 .. v16}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 48
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->tvDurationTime:Landroid/widget/TextView;

    const/4 v15, 0x1

    const/high16 v16, 0x41300000    # 11.0f

    invoke-virtual/range {v14 .. v16}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 51
    :cond_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 53
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->tvTitle:Landroid/widget/TextView;

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v12, v15}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 58
    :goto_0
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v6

    .line 59
    .local v6, "icon":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v7

    .line 60
    .local v7, "intent":Landroid/content/Intent;
    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v11

    .line 62
    .local v11, "time":Ljava/lang/String;
    const/4 v3, 0x0

    .line 63
    .local v3, "aTime":[Ljava/lang/String;
    const-string v13, ""

    .line 65
    .local v13, "totalTime":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .local v9, "sb":Ljava/lang/StringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .local v10, "sbTTS":Ljava/lang/StringBuilder;
    :try_start_0
    const-string v14, " / "

    invoke-virtual {v11, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 71
    array-length v14, v3

    const/4 v15, 0x1

    if-le v14, v15, :cond_4

    .line 72
    const/4 v14, 0x1

    aget-object v14, v3, v14

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 82
    :goto_1
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->tvDurationTime:Landroid/widget/TextView;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v13, v15}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 85
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    const-string v14, "file"

    invoke-virtual {v6, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 88
    const-string v14, "file:/"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v6, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 91
    :cond_1
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    if-eqz v14, :cond_2

    .line 92
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    invoke-virtual {v14, v6}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 94
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    invoke-virtual {v14}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    if-nez v14, :cond_2

    .line 95
    iget-object v14, v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;->ivThumb:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->setImageViewForVideo(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 99
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0e0030

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 101
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 103
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    return-object v14

    .line 55
    .end local v3    # "aTime":[Ljava/lang/String;
    .end local v6    # "icon":Ljava/lang/String;
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    .end local v10    # "sbTTS":Ljava/lang/StringBuilder;
    .end local v11    # "time":Ljava/lang/String;
    .end local v13    # "totalTime":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;
    check-cast v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;

    .restart local v5    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter$VideoItemListViewHolder;
    goto/16 :goto_0

    .line 75
    .restart local v3    # "aTime":[Ljava/lang/String;
    .restart local v6    # "icon":Ljava/lang/String;
    .restart local v7    # "intent":Landroid/content/Intent;
    .restart local v9    # "sb":Ljava/lang/StringBuilder;
    .restart local v10    # "sbTTS":Ljava/lang/StringBuilder;
    .restart local v11    # "time":Ljava/lang/String;
    .restart local v13    # "totalTime":Ljava/lang/String;
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListVideoAdapter;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ItemListVideoAdapter : there is wrong time value is entered : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 78
    :catch_0
    move-exception v4

    .line 79
    .local v4, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v4}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_1
.end method
