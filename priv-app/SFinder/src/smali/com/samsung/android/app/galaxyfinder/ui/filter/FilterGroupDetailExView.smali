.class public Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;
.super Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;
.source "FilterGroupDetailExView.java"


# instance fields
.field private mDefaultView:Landroid/view/View;

.field private mHintText:Landroid/widget/TextView;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;-><init>(Landroid/content/Context;I)V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mState:I

    .line 21
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mDefaultView:Landroid/view/View;

    .line 23
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mHintText:Landroid/widget/TextView;

    .line 25
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mProgressBar:Landroid/widget/ProgressBar;

    .line 30
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->initDefaultView()V

    .line 31
    return-void
.end method

.method private addDefaultView()V
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getDefaultView()Landroid/view/View;

    move-result-object v0

    .line 69
    .local v0, "defaultView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->addView(Landroid/view/View;)V

    .line 73
    .end local v0    # "defaultView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private getDefaultView()Landroid/view/View;
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mDefaultView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 85
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->initDefaultView()V

    .line 88
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getHintText()Landroid/widget/TextView;

    move-result-object v3

    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mState:I

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getProgressBar()Landroid/widget/ProgressBar;

    move-result-object v0

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mState:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mDefaultView:Landroid/view/View;

    return-object v0

    :cond_1
    move v0, v2

    .line 88
    goto :goto_0

    :cond_2
    move v2, v1

    .line 89
    goto :goto_1
.end method

.method private getHintText()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mHintText:Landroid/widget/TextView;

    return-object v0
.end method

.method private getProgressBar()Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private initDefaultView()V
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030009

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mDefaultView:Landroid/view/View;

    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mDefaultView:Landroid/view/View;

    const v1, 0x7f0b0025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mHintText:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mDefaultView:Landroid/view/View;

    const v1, 0x7f0b0024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mProgressBar:Landroid/widget/ProgressBar;

    .line 81
    return-void
.end method


# virtual methods
.method public isEmpty()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 104
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getChildCount()I

    move-result v0

    .line 106
    .local v0, "count":I
    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mState:I

    if-eqz v3, :cond_1

    if-eqz v0, :cond_0

    if-ne v0, v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getDefaultView()Landroid/view/View;

    move-result-object v4

    if-ne v3, v4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;>;"
    invoke-super {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 37
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->addDefaultView()V

    .line 40
    :cond_1
    return-void
.end method

.method public setHintText(ILandroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "resHintId"    # I
    .param p2, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getHintText()Landroid/widget/TextView;

    move-result-object v0

    .line 61
    .local v0, "textView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    return-void
.end method

.method public setState(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v1, 0x1

    .line 43
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mState:I

    if-eq v2, p1, :cond_1

    move v0, v1

    .line 45
    .local v0, "changed":Z
    :goto_0
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mState:I

    .line 47
    if-eqz v0, :cond_0

    if-ne p1, v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->clearData()V

    .line 51
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/FilterGroupDetailAdapter;->notifyDataSetChanged()V

    .line 53
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupDetailExView;->addDefaultView()V

    .line 56
    :cond_0
    return-void

    .line 43
    .end local v0    # "changed":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
