.class public interface abstract Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$ContentsColumns;
.super Ljava/lang/Object;
.source "TagDb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContentsColumns"
.end annotation


# static fields
.field public static final APPNAME:Ljava/lang/String; = "appname"

.field public static final CONTENT_ID:Ljava/lang/String; = "contenturi"

.field public static final FILEPATH:Ljava/lang/String; = "filepath"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"
