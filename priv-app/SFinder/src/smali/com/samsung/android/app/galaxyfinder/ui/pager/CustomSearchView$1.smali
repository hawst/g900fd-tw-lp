.class Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;
.super Ljava/lang/Object;
.source "CustomSearchView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 205
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setFocusable(Z)V

    .line 207
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->setFocusableInTouchMode(Z)V

    .line 208
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->requestFocus()Z

    .line 210
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->enoughToFilter()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->isPopupShowingLogically()Z

    move-result v0

    if-nez v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->showDropDown()V

    .line 217
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->mEditQuery:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$SearchAutoComplete;->forceDismissDropDown()V

    goto :goto_0
.end method
