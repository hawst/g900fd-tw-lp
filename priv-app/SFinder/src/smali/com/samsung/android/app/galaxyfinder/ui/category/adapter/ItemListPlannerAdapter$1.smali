.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;
.super Ljava/lang/Object;
.source "ItemListPlannerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;

.field final synthetic val$holder:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;

.field final synthetic val$item:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->val$item:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->val$holder:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 205
    instance-of v3, p1, Landroid/widget/CheckBox;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 206
    check-cast v0, Landroid/widget/CheckBox;

    .line 207
    .local v0, "cb":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 208
    .local v1, "checked":Z
    const/4 v2, -0x1

    .line 209
    .local v2, "extraFlags":I
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->val$item:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->convertStringToInt(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;)I
    invoke-static {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;)I

    move-result v2

    .line 211
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->val$holder:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;

    iget-object v4, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$PlannerItemListViewHolder;->tv1:Landroid/widget/TextView;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setTextStrike(Landroid/widget/TextView;Z)V
    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;Landroid/widget/TextView;Z)V

    .line 212
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->val$item:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->val$item:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->setTaskStatus(Landroid/content/ComponentName;Ljava/lang/String;Z)V
    invoke-static {v3, v4, v5, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter;Landroid/content/ComponentName;Ljava/lang/String;Z)V

    .line 215
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListPlannerAdapter$1;->val$item:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    and-int/lit8 v4, v2, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setSuggestionExtraFlags(Ljava/lang/String;)V

    .line 218
    .end local v0    # "cb":Landroid/widget/CheckBox;
    .end local v1    # "checked":Z
    .end local v2    # "extraFlags":I
    :cond_0
    return-void
.end method
