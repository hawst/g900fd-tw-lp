.class Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$2;
.super Ljava/lang/Object;
.source "FilterGroupViewPager.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 554
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 557
    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 550
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 542
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 545
    :cond_0
    return-void
.end method
