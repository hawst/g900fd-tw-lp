.class public interface abstract Lcom/samsung/android/app/galaxyfinder/Constants$HistoryColumns;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "HistoryColumns"
.end annotation


# static fields
.field public static final ADDED_DATE:Ljava/lang/String; = "added_date"

.field public static final HISTORY_KEYWORD:Ljava/lang/String; = "history_keyword"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final TYPE:Ljava/lang/String; = "type"
