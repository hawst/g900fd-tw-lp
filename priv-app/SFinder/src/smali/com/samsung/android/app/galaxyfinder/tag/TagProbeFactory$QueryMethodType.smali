.class final enum Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;
.super Ljava/lang/Enum;
.source "TagProbeFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "QueryMethodType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

.field public static final enum DEFAULT_Query:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

.field public static final enum DIRECT_Query:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    const-string v1, "DEFAULT_Query"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->DEFAULT_Query:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    new-instance v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    const-string v1, "DIRECT_Query"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->DIRECT_Query:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->DEFAULT_Query:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->DIRECT_Query:Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->$VALUES:[Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    invoke-virtual {v0}, [Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/galaxyfinder/tag/TagProbeFactory$QueryMethodType;

    return-object v0
.end method
