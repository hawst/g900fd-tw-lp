.class public Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "GalaxyFinderSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SettingsFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;,
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;,
        Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$Accessibility;
    }
.end annotation


# instance fields
.field private mSearchCategory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;

.field private mUseSearchHistory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 178
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const v1, 0x7f050004

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->addPreferencesFromResource(I)V

    .line 93
    const-string v1, "setting_search"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 96
    .local v0, "prefsCategory":Landroid/preference/PreferenceCategory;
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0098

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "setting_search_category"

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->mSearchCategory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;

    .line 98
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->mSearchCategory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;

    invoke-virtual {v1, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 99
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->mSearchCategory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0099

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;->setContentDescription(Ljava/lang/String;)V

    .line 103
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;-><init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->mUseSearchHistory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;

    .line 104
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->mUseSearchHistory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;

    invoke-virtual {v1, p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 105
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->mUseSearchHistory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00cc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;->setContentDescription(Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->mSearchCategory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceDefault;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 109
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->mUseSearchHistory:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment$SettingsPreferenceUseSearchHistory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 110
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 114
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 115
    .local v0, "key":Ljava/lang/String;
    const/4 v1, 0x0

    .line 117
    .local v1, "settingsIntent":Landroid/content/Intent;
    const-string v2, "setting_search_category"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "settingsIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderCategoryActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 121
    .restart local v1    # "settingsIntent":Landroid/content/Intent;
    :cond_0
    if-eqz v1, :cond_1

    .line 122
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderSettingsActivity$SettingsFragment;->startActivity(Landroid/content/Intent;)V

    .line 125
    :cond_1
    const/4 v2, 0x0

    return v2
.end method
