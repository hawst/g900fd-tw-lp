.class Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$7;
.super Ljava/lang/Object;
.source "VoiceInputActivity.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->startSacleAnimation(FJLandroid/view/animation/Interpolator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V
    .locals 0

    .prologue
    .line 542
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$7;->this$0:Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 545
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 549
    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isRestore:Z
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$500()Z

    move-result v0

    if-nez v0, :cond_0

    .line 550
    const/4 v0, 0x1

    # setter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isRestore:Z
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$502(Z)Z

    .line 552
    const/high16 v0, 0x3f800000    # 1.0f

    const-wide/16 v2, 0xc8

    # invokes: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->startSacleAnimationReverse(FJ)V
    invoke-static {v0, v2, v3}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$600(FJ)V

    .line 556
    :goto_0
    return-void

    .line 554
    :cond_0
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isRestore:Z
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$502(Z)Z

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 560
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 564
    return-void
.end method
