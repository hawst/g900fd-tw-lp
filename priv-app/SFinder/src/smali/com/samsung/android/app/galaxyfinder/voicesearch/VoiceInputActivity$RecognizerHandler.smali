.class Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;
.super Landroid/os/Handler;
.source "VoiceInputActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecognizerHandler"
.end annotation


# instance fields
.field private final mTarget:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V
    .locals 1
    .param p1, "target"    # Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .prologue
    .line 821
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 822
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->mTarget:Ljava/lang/ref/WeakReference;

    .line 823
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 827
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity$RecognizerHandler;->mTarget:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;

    .line 829
    .local v0, "target":Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 865
    :cond_0
    :goto_0
    return-void

    .line 833
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 835
    :pswitch_0
    # invokes: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->startRecognizer()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$1000(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    goto :goto_0

    .line 839
    :pswitch_1
    # invokes: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->updateRms()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$1100(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    .line 851
    :pswitch_2
    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$1200(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Lcom/nuance/nmdp/speechkit/Recognizer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 852
    # getter for: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->mRecognizer:Lcom/nuance/nmdp/speechkit/Recognizer;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$1200(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)Lcom/nuance/nmdp/speechkit/Recognizer;

    move-result-object v1

    invoke-interface {v1}, Lcom/nuance/nmdp/speechkit/Recognizer;->start()V

    goto :goto_0

    .line 857
    :pswitch_3
    # invokes: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->updateProcessing()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$1300(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    goto :goto_0

    .line 861
    :pswitch_4
    # invokes: Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->getAudioLevel()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;->access$1400(Lcom/samsung/android/app/galaxyfinder/voicesearch/VoiceInputActivity;)V

    goto :goto_0

    .line 833
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
