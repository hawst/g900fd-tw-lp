.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;
    }
.end annotation


# instance fields
.field private final ACTION_CHANGE_SETTING_VALUE:Ljava/lang/String;

.field private final CTRL_MODE_CHECKBOX:I

.field private final CTRL_MODE_LINK:I

.field private final CTRL_MODE_NONE:I

.field private final CTRL_MODE_SWITCH:I

.field private final FLAG_CTRL_CHECKBOX:I

.field private final FLAG_CTRL_LINK:I

.field private final FLAG_CTRL_NONE:I

.field private final FLAG_CTRL_SWITCH:I

.field private final FLAG_CTRL_UNKNOWN:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 49
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 27
    const-string v0, "android.settings.REQUEST_FINDO_SEARCH_CHANGE_SETTING_VALUE"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->ACTION_CHANGE_SETTING_VALUE:Ljava/lang/String;

    .line 29
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->CTRL_MODE_NONE:I

    .line 31
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->CTRL_MODE_SWITCH:I

    .line 33
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->CTRL_MODE_CHECKBOX:I

    .line 35
    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->CTRL_MODE_LINK:I

    .line 37
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->FLAG_CTRL_UNKNOWN:I

    .line 39
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->FLAG_CTRL_NONE:I

    .line 41
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->FLAG_CTRL_SWITCH:I

    .line 43
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->FLAG_CTRL_CHECKBOX:I

    .line 45
    iput v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->FLAG_CTRL_LINK:I

    .line 50
    return-void
.end method

.method private getCtrlMode(I)I
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 121
    .local v0, "ctrlMode":I
    packed-switch p1, :pswitch_data_0

    .line 142
    :goto_0
    return v0

    .line 123
    :pswitch_0
    const/4 v0, 0x1

    .line 124
    goto :goto_0

    .line 127
    :pswitch_1
    const/4 v0, 0x2

    .line 128
    goto :goto_0

    .line 131
    :pswitch_2
    const/4 v0, 0x3

    .line 132
    goto :goto_0

    .line 135
    :pswitch_3
    const/4 v0, 0x0

    .line 136
    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setCtrlModeAction(Landroid/view/View;II)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "mode"    # I
    .param p3, "id"    # I

    .prologue
    .line 62
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.settings.REQUEST_FINDO_SEARCH_CHANGE_SETTING_VALUE"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "rowId"

    invoke-virtual {v1, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 65
    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 67
    packed-switch p2, :pswitch_data_0

    .line 116
    :goto_0
    return-void

    :pswitch_0
    move-object v3, p1

    .line 69
    check-cast v3, Landroid/widget/Switch;

    .line 70
    .local v3, "switchCtrl":Landroid/widget/Switch;
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$1;

    invoke-direct {v4, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;Landroid/content/Intent;)V

    invoke-virtual {v3, v4}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .end local v3    # "switchCtrl":Landroid/widget/Switch;
    :pswitch_1
    move-object v0, p1

    .line 86
    check-cast v0, Landroid/widget/CheckBox;

    .line 87
    .local v0, "checkboxCtrl":Landroid/widget/CheckBox;
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$2;

    invoke-direct {v4, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;Landroid/content/Intent;)V

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .end local v0    # "checkboxCtrl":Landroid/widget/CheckBox;
    :pswitch_2
    move-object v2, p1

    .line 102
    check-cast v2, Landroid/widget/ImageView;

    .line 103
    .local v2, "linkCtrl":Landroid/widget/ImageView;
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$3;

    invoke-direct {v4, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$3;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;Landroid/content/Intent;)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setDataItem(Landroid/view/View;I)Landroid/view/View;
    .locals 25
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 146
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v13

    .line 147
    .local v13, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v10, 0x0

    .line 149
    .local v10, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;
    if-nez p1, :cond_a

    .line 150
    sget-object v21, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v22, 0x7f03004a

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v21 .. v24}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 153
    new-instance v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;

    .end local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;
    invoke-direct {v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;-><init>()V

    .line 155
    .restart local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;
    const v21, 0x7f0b009c

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 156
    const v21, 0x7f0b00a0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 157
    const v21, 0x7f0b00a1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    .line 159
    const v21, 0x7f0b009d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/Switch;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    .line 160
    const v21, 0x7f0b009e

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/CheckBox;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    .line 161
    const v21, 0x7f0b009f

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivLink:Landroid/widget/ImageView;

    .line 163
    const v21, 0x7f0b0095

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/view/ViewGroup;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgIconGroup:Landroid/view/ViewGroup;

    .line 164
    const v21, 0x7f0b004d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/view/ViewGroup;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    .line 165
    const v21, 0x7f0b0045

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/view/ViewGroup;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    .line 167
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 184
    :goto_0
    const/4 v5, 0x0

    .line 186
    .local v5, "ctrlMode":I
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v15

    .line 187
    .local v15, "name":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v7

    .line 188
    .local v7, "desc":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v18

    .line 189
    .local v18, "type":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v20

    .line 190
    .local v20, "value":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v11

    .line 191
    .local v11, "iconUri":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v16

    .line 192
    .local v16, "sortKeyValue":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v12

    .line 194
    .local v12, "intent":Landroid/content/Intent;
    const/4 v6, 0x0

    .line 195
    .local v6, "dataId":I
    const/16 v19, 0x0

    .line 196
    .local v19, "typeValue":I
    const/4 v4, 0x0

    .line 197
    .local v4, "checked":Z
    const/4 v8, 0x0

    .line 199
    .local v8, "disabled":Z
    if-eqz v18, :cond_0

    .line 201
    :try_start_0
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    .line 203
    .local v17, "temp":I
    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    .line 204
    const v21, 0xff00

    and-int v21, v21, v17

    const/16 v22, 0x100

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    const/4 v8, 0x1

    .line 209
    .end local v17    # "temp":I
    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->getCtrlMode(I)I

    move-result v5

    .line 211
    if-eqz v20, :cond_0

    .line 213
    :try_start_1
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    if-lez v21, :cond_c

    const/4 v4, 0x1

    .line 214
    :goto_2
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIntentDataId()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    .line 225
    :cond_0
    :goto_3
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_1

    const-string v21, "-1"

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 226
    :cond_1
    const/4 v11, 0x0

    .line 229
    :cond_2
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_3

    const-string v21, "null"

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 230
    :cond_3
    const/4 v15, 0x0

    .line 233
    :cond_4
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_5

    const-string v21, "null"

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 234
    :cond_5
    const/4 v7, 0x0

    .line 238
    :cond_6
    if-eqz v15, :cond_7

    .line 239
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v15, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 242
    :cond_7
    if-eqz v7, :cond_d

    .line 243
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setVisibility(I)V

    .line 244
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->tvDesc:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 251
    :goto_4
    if-eqz v11, :cond_8

    .line 252
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v1, v11, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 256
    :cond_8
    packed-switch v5, :pswitch_data_0

    .line 304
    :goto_5
    :pswitch_0
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_9

    .line 305
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, p2

    move-object/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setSubHeaderView(Landroid/view/ViewGroup;ILjava/lang/String;)V

    .line 308
    :cond_9
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, p2

    invoke-virtual {v0, v1, v12, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 310
    return-object p1

    .line 169
    .end local v4    # "checked":Z
    .end local v5    # "ctrlMode":I
    .end local v6    # "dataId":I
    .end local v7    # "desc":Ljava/lang/String;
    .end local v8    # "disabled":Z
    .end local v11    # "iconUri":Ljava/lang/String;
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "sortKeyValue":Ljava/lang/String;
    .end local v18    # "type":Ljava/lang/String;
    .end local v19    # "typeValue":I
    .end local v20    # "value":Ljava/lang/String;
    :cond_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;
    check-cast v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;

    .line 171
    .restart local v10    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/Switch;->setVisibility(I)V

    .line 172
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 173
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivLink:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 176
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 177
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivLink:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 179
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 180
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 181
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivLink:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 204
    .restart local v4    # "checked":Z
    .restart local v5    # "ctrlMode":I
    .restart local v6    # "dataId":I
    .restart local v7    # "desc":Ljava/lang/String;
    .restart local v8    # "disabled":Z
    .restart local v11    # "iconUri":Ljava/lang/String;
    .restart local v12    # "intent":Landroid/content/Intent;
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v16    # "sortKeyValue":Ljava/lang/String;
    .restart local v17    # "temp":I
    .restart local v18    # "type":Ljava/lang/String;
    .restart local v19    # "typeValue":I
    .restart local v20    # "value":Ljava/lang/String;
    :cond_b
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 205
    .end local v17    # "temp":I
    :catch_0
    move-exception v9

    .line 206
    .local v9, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_1

    .line 213
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 215
    :catch_1
    move-exception v9

    .line 216
    .restart local v9    # "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 218
    const/4 v4, 0x0

    .line 219
    const/4 v6, -0x1

    goto/16 :goto_3

    .line 246
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    :cond_d
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/RelativeLayout$LayoutParams;

    .line 247
    .local v14, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v21, 0xf

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 248
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->tvName:Landroid/widget/TextView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    .line 258
    .end local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_1
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/Switch;->setVisibility(I)V

    .line 259
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setChecked(Z)V

    .line 260
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/Switch;->setFocusable(Z)V

    .line 261
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Landroid/widget/Switch;->setTag(Ljava/lang/Object;)V

    .line 263
    if-eqz v8, :cond_e

    .line 264
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgIconGroup:Landroid/view/ViewGroup;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 265
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 268
    :cond_e
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivSwitch:Landroid/widget/Switch;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v5, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setCtrlModeAction(Landroid/view/View;II)V

    goto/16 :goto_5

    .line 272
    :pswitch_2
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 273
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 274
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 275
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 277
    if-eqz v8, :cond_f

    .line 278
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgIconGroup:Landroid/view/ViewGroup;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 279
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 282
    :cond_f
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivCheck:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v5, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setCtrlModeAction(Landroid/view/View;II)V

    goto/16 :goto_5

    .line 286
    :pswitch_3
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivLink:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 288
    if-eqz v8, :cond_10

    .line 289
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->vgIconGroup:Landroid/view/ViewGroup;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 290
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivLink:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 293
    :cond_10
    iget-object v0, v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter$SettingsItemListViewHolder;->ivLink:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move/from16 v3, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    goto/16 :goto_5

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 54
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->mPosition:I

    .line 55
    invoke-virtual {p0, p1, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 56
    invoke-direct {p0, p2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSettingsAdapter;->setDataItem(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    .line 58
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
