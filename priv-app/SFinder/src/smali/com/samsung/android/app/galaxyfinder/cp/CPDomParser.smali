.class public Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;
.super Ljava/lang/Object;
.source "CPDomParser.java"


# static fields
.field private static final CP_VALUE_ICON:Ljava/lang/String; = "icon"

.field private static final CP_VALUE_NAME:Ljava/lang/String; = "name"

.field private static final CP_VALUE_URL:Ljava/lang/String; = "url"

.field private static final ITEM_ATTRIBUTE:Ljava/lang/String; = "iso"

.field private static final ITEM_DOM:Ljava/lang/String; = "item"

.field private static final TAG:Ljava/lang/String; = "CPDomParser"

.field private static mClientId:Ljava/lang/String;

.field private static mCpDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;",
            ">;"
        }
    .end annotation
.end field

.field private static mCurrentIso:Ljava/lang/String;

.field private static mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mClientId:Ljava/lang/String;

    .line 43
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    .line 47
    sput-object v1, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCurrentIso:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCPData(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 32
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "currentIso"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 51
    :cond_0
    const/16 v28, 0x0

    .line 176
    :cond_1
    :goto_0
    return-object v28

    .line 54
    :cond_2
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCurrentIso:Ljava/lang/String;

    if-eqz v28, :cond_3

    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCurrentIso:Ljava/lang/String;

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 55
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    goto :goto_0

    .line 58
    :cond_3
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->clear()V

    .line 59
    sput-object p1, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCurrentIso:Ljava/lang/String;

    .line 61
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    if-nez v28, :cond_5

    .line 62
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mClientId:Ljava/lang/String;

    if-nez v28, :cond_4

    .line 63
    const/4 v4, 0x0

    .line 64
    .local v4, "builder":Ljava/lang/StringBuilder;
    const-string v28, "ro.com.google.clientidbase.ms"

    invoke-static/range {v28 .. v28}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 66
    .local v5, "clientId":Ljava/lang/String;
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v28

    if-nez v28, :cond_4

    .line 67
    new-instance v4, Ljava/lang/StringBuilder;

    .end local v4    # "builder":Ljava/lang/StringBuilder;
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .restart local v4    # "builder":Ljava/lang/StringBuilder;
    const-string v28, "&client="

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    sput-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mClientId:Ljava/lang/String;

    .line 76
    .end local v4    # "builder":Ljava/lang/StringBuilder;
    .end local v5    # "clientId":Ljava/lang/String;
    :cond_4
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "android.resource://"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "/"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const v29, 0x7f02018d

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 79
    .local v9, "cpIcon":Ljava/lang/String;
    new-instance v28, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    const-string v29, "Google"

    const-string v30, "http://www.google.com/search?q="

    sget-object v31, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mClientId:Ljava/lang/String;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    .line 83
    .end local v9    # "cpIcon":Ljava/lang/String;
    :cond_5
    const/16 v18, 0x0

    .line 86
    .local v18, "inputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v16

    .line 87
    .local v16, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual/range {v16 .. v16}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v4

    .line 88
    .local v4, "builder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/cp/CPFileLoad;->loadDataFile()Ljava/io/InputStream;

    move-result-object v18

    .line 90
    if-nez v18, :cond_7

    .line 91
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    sget-object v29, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->isEmpty()Z

    move-result v29

    if-eqz v29, :cond_6

    .line 162
    sget-object v29, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    sget-object v30, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    invoke-interface/range {v29 .. v30}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_6
    if-eqz v18, :cond_1

    .line 167
    :try_start_1
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 172
    :goto_1
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 168
    :catch_0
    move-exception v15

    .line 169
    .local v15, "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 94
    .end local v15    # "e":Ljava/io/IOException;
    :cond_7
    :try_start_2
    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v14

    .line 97
    .local v14, "doc":Lorg/w3c/dom/Document;
    invoke-interface {v14}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v25

    .line 98
    .local v25, "root":Lorg/w3c/dom/Element;
    const-string v28, "item"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v21

    .line 99
    .local v21, "itemList":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    .line 101
    .local v23, "itemSize":I
    const/16 v20, 0x0

    .local v20, "itemIndex":I
    :goto_2
    move/from16 v0, v20

    move/from16 v1, v23

    if-ge v0, v1, :cond_14

    .line 103
    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    .line 104
    .local v22, "itemNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, v22

    check-cast v0, Lorg/w3c/dom/Element;

    move-object/from16 v19, v0

    .line 106
    .local v19, "itemElement":Lorg/w3c/dom/Element;
    const-string v28, "iso"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-nez v28, :cond_9

    .line 101
    :cond_8
    :goto_3
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 110
    :cond_9
    invoke-interface/range {v22 .. v22}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v11

    .line 111
    .local v11, "cpList":Lorg/w3c/dom/NodeList;
    invoke-interface {v11}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    .line 113
    .local v13, "cpSize":I
    const/4 v10, 0x0

    .local v10, "cpIndex":I
    :goto_4
    if-ge v10, v13, :cond_8

    .line 114
    new-instance v8, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    invoke-direct {v8}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;-><init>()V

    .line 116
    .local v8, "cpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    invoke-interface {v11, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    .line 118
    .local v12, "cpNode":Lorg/w3c/dom/Node;
    invoke-interface {v12}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_13

    .line 119
    invoke-interface {v12}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 120
    .local v6, "cpChild":Lorg/w3c/dom/NodeList;
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    .line 122
    .local v7, "cpCount":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_5
    move/from16 v0, v17

    if-ge v0, v7, :cond_10

    .line 124
    move/from16 v0, v17

    invoke-interface {v6, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v27

    .line 126
    .local v27, "valueNode":Lorg/w3c/dom/Node;
    invoke-interface/range {v27 .. v27}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_a

    .line 127
    invoke-interface/range {v27 .. v27}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v24

    .line 128
    .local v24, "name":Ljava/lang/String;
    invoke-interface/range {v27 .. v27}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v28

    const/16 v29, 0x0

    invoke-interface/range {v28 .. v29}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v26

    .line 130
    .local v26, "value":Ljava/lang/String;
    const-string v28, "name"

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 131
    move-object/from16 v0, v26

    invoke-virtual {v8, v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->setName(Ljava/lang/String;)V

    .line 122
    .end local v24    # "name":Ljava/lang/String;
    .end local v26    # "value":Ljava/lang/String;
    :cond_a
    :goto_6
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 132
    .restart local v24    # "name":Ljava/lang/String;
    .restart local v26    # "value":Ljava/lang/String;
    :cond_b
    const-string v28, "icon"

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 133
    move-object/from16 v0, v26

    invoke-virtual {v8, v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->setIcon(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    .line 154
    .end local v4    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v6    # "cpChild":Lorg/w3c/dom/NodeList;
    .end local v7    # "cpCount":I
    .end local v8    # "cpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .end local v10    # "cpIndex":I
    .end local v11    # "cpList":Lorg/w3c/dom/NodeList;
    .end local v12    # "cpNode":Lorg/w3c/dom/Node;
    .end local v13    # "cpSize":I
    .end local v14    # "doc":Lorg/w3c/dom/Document;
    .end local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v17    # "i":I
    .end local v19    # "itemElement":Lorg/w3c/dom/Element;
    .end local v20    # "itemIndex":I
    .end local v21    # "itemList":Lorg/w3c/dom/NodeList;
    .end local v22    # "itemNode":Lorg/w3c/dom/Node;
    .end local v23    # "itemSize":I
    .end local v24    # "name":Ljava/lang/String;
    .end local v25    # "root":Lorg/w3c/dom/Element;
    .end local v26    # "value":Ljava/lang/String;
    .end local v27    # "valueNode":Lorg/w3c/dom/Node;
    :catch_1
    move-exception v15

    .line 155
    .local v15, "e":Lorg/xml/sax/SAXException;
    :try_start_3
    invoke-virtual {v15}, Lorg/xml/sax/SAXException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 161
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->isEmpty()Z

    move-result v28

    if-eqz v28, :cond_c

    .line 162
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    sget-object v29, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    invoke-interface/range {v28 .. v29}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_c
    if-eqz v18, :cond_d

    .line 167
    :try_start_4
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 172
    .end local v15    # "e":Lorg/xml/sax/SAXException;
    :goto_7
    const/16 v18, 0x0

    .line 176
    :cond_d
    :goto_8
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    goto/16 :goto_0

    .line 134
    .restart local v4    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v6    # "cpChild":Lorg/w3c/dom/NodeList;
    .restart local v7    # "cpCount":I
    .restart local v8    # "cpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .restart local v10    # "cpIndex":I
    .restart local v11    # "cpList":Lorg/w3c/dom/NodeList;
    .restart local v12    # "cpNode":Lorg/w3c/dom/Node;
    .restart local v13    # "cpSize":I
    .restart local v14    # "doc":Lorg/w3c/dom/Document;
    .restart local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v17    # "i":I
    .restart local v19    # "itemElement":Lorg/w3c/dom/Element;
    .restart local v20    # "itemIndex":I
    .restart local v21    # "itemList":Lorg/w3c/dom/NodeList;
    .restart local v22    # "itemNode":Lorg/w3c/dom/Node;
    .restart local v23    # "itemSize":I
    .restart local v24    # "name":Ljava/lang/String;
    .restart local v25    # "root":Lorg/w3c/dom/Element;
    .restart local v26    # "value":Ljava/lang/String;
    .restart local v27    # "valueNode":Lorg/w3c/dom/Node;
    :cond_e
    :try_start_5
    const-string v28, "url"

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 135
    move-object/from16 v0, v26

    invoke-virtual {v8, v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->setUrl(Ljava/lang/String;)V
    :try_end_5
    .catch Lorg/xml/sax/SAXException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_6

    .line 156
    .end local v4    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v6    # "cpChild":Lorg/w3c/dom/NodeList;
    .end local v7    # "cpCount":I
    .end local v8    # "cpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .end local v10    # "cpIndex":I
    .end local v11    # "cpList":Lorg/w3c/dom/NodeList;
    .end local v12    # "cpNode":Lorg/w3c/dom/Node;
    .end local v13    # "cpSize":I
    .end local v14    # "doc":Lorg/w3c/dom/Document;
    .end local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v17    # "i":I
    .end local v19    # "itemElement":Lorg/w3c/dom/Element;
    .end local v20    # "itemIndex":I
    .end local v21    # "itemList":Lorg/w3c/dom/NodeList;
    .end local v22    # "itemNode":Lorg/w3c/dom/Node;
    .end local v23    # "itemSize":I
    .end local v24    # "name":Ljava/lang/String;
    .end local v25    # "root":Lorg/w3c/dom/Element;
    .end local v26    # "value":Ljava/lang/String;
    .end local v27    # "valueNode":Lorg/w3c/dom/Node;
    :catch_2
    move-exception v15

    .line 157
    .local v15, "e":Ljavax/xml/parsers/ParserConfigurationException;
    :try_start_6
    invoke-virtual {v15}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 161
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->isEmpty()Z

    move-result v28

    if-eqz v28, :cond_f

    .line 162
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    sget-object v29, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    invoke-interface/range {v28 .. v29}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_f
    if-eqz v18, :cond_d

    .line 167
    :try_start_7
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 172
    .end local v15    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :goto_9
    const/16 v18, 0x0

    goto :goto_8

    .line 140
    .restart local v4    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v6    # "cpChild":Lorg/w3c/dom/NodeList;
    .restart local v7    # "cpCount":I
    .restart local v8    # "cpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .restart local v10    # "cpIndex":I
    .restart local v11    # "cpList":Lorg/w3c/dom/NodeList;
    .restart local v12    # "cpNode":Lorg/w3c/dom/Node;
    .restart local v13    # "cpSize":I
    .restart local v14    # "doc":Lorg/w3c/dom/Document;
    .restart local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v17    # "i":I
    .restart local v19    # "itemElement":Lorg/w3c/dom/Element;
    .restart local v20    # "itemIndex":I
    .restart local v21    # "itemList":Lorg/w3c/dom/NodeList;
    .restart local v22    # "itemNode":Lorg/w3c/dom/Node;
    .restart local v23    # "itemSize":I
    .restart local v25    # "root":Lorg/w3c/dom/Element;
    :cond_10
    :try_start_8
    const-string v28, "Google"

    invoke-virtual {v8}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_12

    .line 141
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mClientId:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->setParams(Ljava/lang/String;)V

    .line 143
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->clear()V

    .line 144
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    move-object/from16 v0, v28

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Lorg/xml/sax/SAXException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_3

    .line 158
    .end local v4    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v6    # "cpChild":Lorg/w3c/dom/NodeList;
    .end local v7    # "cpCount":I
    .end local v8    # "cpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .end local v10    # "cpIndex":I
    .end local v11    # "cpList":Lorg/w3c/dom/NodeList;
    .end local v12    # "cpNode":Lorg/w3c/dom/Node;
    .end local v13    # "cpSize":I
    .end local v14    # "doc":Lorg/w3c/dom/Document;
    .end local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v17    # "i":I
    .end local v19    # "itemElement":Lorg/w3c/dom/Element;
    .end local v20    # "itemIndex":I
    .end local v21    # "itemList":Lorg/w3c/dom/NodeList;
    .end local v22    # "itemNode":Lorg/w3c/dom/Node;
    .end local v23    # "itemSize":I
    .end local v25    # "root":Lorg/w3c/dom/Element;
    :catch_3
    move-exception v15

    .line 159
    .local v15, "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 161
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->isEmpty()Z

    move-result v28

    if-eqz v28, :cond_11

    .line 162
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    sget-object v29, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    invoke-interface/range {v28 .. v29}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_11
    if-eqz v18, :cond_d

    .line 167
    :try_start_a
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 172
    :goto_a
    const/16 v18, 0x0

    goto :goto_8

    .line 148
    .end local v15    # "e":Ljava/io/IOException;
    .restart local v4    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v6    # "cpChild":Lorg/w3c/dom/NodeList;
    .restart local v7    # "cpCount":I
    .restart local v8    # "cpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .restart local v10    # "cpIndex":I
    .restart local v11    # "cpList":Lorg/w3c/dom/NodeList;
    .restart local v12    # "cpNode":Lorg/w3c/dom/Node;
    .restart local v13    # "cpSize":I
    .restart local v14    # "doc":Lorg/w3c/dom/Document;
    .restart local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v17    # "i":I
    .restart local v19    # "itemElement":Lorg/w3c/dom/Element;
    .restart local v20    # "itemIndex":I
    .restart local v21    # "itemList":Lorg/w3c/dom/NodeList;
    .restart local v22    # "itemNode":Lorg/w3c/dom/Node;
    .restart local v23    # "itemSize":I
    .restart local v25    # "root":Lorg/w3c/dom/Element;
    :cond_12
    :try_start_b
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    move-object/from16 v0, v28

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    .end local v6    # "cpChild":Lorg/w3c/dom/NodeList;
    .end local v7    # "cpCount":I
    .end local v17    # "i":I
    :cond_13
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_4

    .line 153
    .end local v8    # "cpData":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .end local v10    # "cpIndex":I
    .end local v11    # "cpList":Lorg/w3c/dom/NodeList;
    .end local v12    # "cpNode":Lorg/w3c/dom/Node;
    .end local v13    # "cpSize":I
    .end local v19    # "itemElement":Lorg/w3c/dom/Element;
    .end local v22    # "itemNode":Lorg/w3c/dom/Node;
    :cond_14
    const-string v28, "CPDomParser"

    const-string v29, "parsing success"

    invoke-static/range {v28 .. v29}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catch Lorg/xml/sax/SAXException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 161
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->isEmpty()Z

    move-result v28

    if-eqz v28, :cond_15

    .line 162
    sget-object v28, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    sget-object v29, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    invoke-interface/range {v28 .. v29}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_15
    if-eqz v18, :cond_d

    .line 167
    :try_start_c
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4

    .line 172
    :goto_b
    const/16 v18, 0x0

    goto/16 :goto_8

    .line 168
    :catch_4
    move-exception v15

    .line 169
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 168
    .end local v4    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v14    # "doc":Lorg/w3c/dom/Document;
    .end local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v20    # "itemIndex":I
    .end local v21    # "itemList":Lorg/w3c/dom/NodeList;
    .end local v23    # "itemSize":I
    .end local v25    # "root":Lorg/w3c/dom/Element;
    .local v15, "e":Lorg/xml/sax/SAXException;
    :catch_5
    move-exception v15

    .line 169
    .local v15, "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_7

    .line 168
    .local v15, "e":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_6
    move-exception v15

    .line 169
    .local v15, "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 168
    :catch_7
    move-exception v15

    .line 169
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 161
    .end local v15    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v28

    sget-object v29, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->isEmpty()Z

    move-result v29

    if-eqz v29, :cond_16

    .line 162
    sget-object v29, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mCpDataList:Ljava/util/List;

    sget-object v30, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->mDefaultCPData:Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    invoke-interface/range {v29 .. v30}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_16
    if-eqz v18, :cond_17

    .line 167
    :try_start_d
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    .line 172
    :goto_c
    const/16 v18, 0x0

    :cond_17
    throw v28

    .line 168
    :catch_8
    move-exception v15

    .line 169
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c
.end method
