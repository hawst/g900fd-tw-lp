.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListSamsungLinkAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 23
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 18
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 27
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v13

    .line 28
    .local v13, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v11, 0x0

    .line 30
    .local v11, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 32
    if-nez p2, :cond_3

    .line 33
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030049

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 36
    new-instance v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;

    .end local v11    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;
    invoke-direct {v11}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;-><init>()V

    .line 38
    .restart local v11    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;
    const v3, 0x7f0b004f

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 39
    const v3, 0x7f0b004e

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 40
    const v3, 0x7f0b004d

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    .line 41
    const v3, 0x7f0b0045

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    .line 43
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    :goto_0
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v10

    .line 50
    .local v10, "filename":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v5

    .line 51
    .local v5, "iconUri":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v16

    .line 52
    .local v16, "sortKeyValue":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v12

    .line 53
    .local v12, "intent":Landroid/content/Intent;
    invoke-virtual {v13}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIconOrientation()I

    move-result v8

    .line 55
    .local v8, "orientation":I
    iget-object v3, v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;->tvName:Landroid/widget/TextView;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V

    .line 57
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 58
    iget-object v3, v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;->vgSubHeader:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v16

    invoke-virtual {v0, v3, v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->setSubHeaderView(Landroid/view/ViewGroup;ILjava/lang/String;)V

    .line 61
    :cond_0
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 62
    const-string v3, "android.resource"

    invoke-virtual {v5, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 63
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a029b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v14

    .line 65
    .local v14, "l":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a029d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v17

    .line 67
    .local v17, "t":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a029c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v15

    .line 69
    .local v15, "r":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a029a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v9

    .line 72
    .local v9, "b":I
    iget-object v3, v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    move/from16 v0, v17

    invoke-virtual {v3, v14, v0, v15, v9}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 75
    .end local v9    # "b":I
    .end local v14    # "l":I
    .end local v15    # "r":I
    .end local v17    # "t":I
    :cond_1
    iget-object v4, v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/4 v6, 0x0

    const/4 v7, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;II)V

    .line 78
    :cond_2
    iget-object v3, v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;->vgItemGroup:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v3, v12, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 80
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    return-object v3

    .line 46
    .end local v5    # "iconUri":Ljava/lang/String;
    .end local v8    # "orientation":I
    .end local v10    # "filename":Ljava/lang/String;
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v16    # "sortKeyValue":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;
    check-cast v11, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;

    .restart local v11    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListSamsungLinkAdapter$SamsungLinkItemListViewHolder;
    goto/16 :goto_0
.end method
