.class public Lcom/samsung/android/app/galaxyfinder/search/SearchController;
.super Landroid/os/HandlerThread;
.source "SearchController.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;,
        Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;,
        Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    }
.end annotation


# static fields
.field private static final COLUMNS:[Ljava/lang/String;


# instance fields
.field private ACTION_ID_CANCELED:I

.field private DELAY_TIME_FOR_QUERY:I

.field final LOCATION:Ljava/lang/String;

.field private final MSG_SEARCH_CANCEL:I

.field private final MSG_SEARCH_COMPLETED:I

.field private final MSG_SEARCH_REQUEST:I

.field private final MSG_SEARCH_TAG_RETRIEVED:I

.field final PEOPLE:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field final TAGDELIMITER:Ljava/lang/String;

.field final TAGDELIMITER_SUB:Ljava/lang/String;

.field private final TAG_TYPE:I

.field private final TAG_VALUE:I

.field final USERDEF:Ljava/lang/String;

.field final WEATHER:Ljava/lang/String;

.field private bStopTagRetrieve:Z

.field private mActionTable:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/search/QueryAction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCPDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentRunnable:Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

.field private mLastQueryId:I

.field private mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

.field public mQueryHandler:Landroid/os/Handler;

.field private mRunnableList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;",
            ">;"
        }
    .end annotation
.end field

.field public mTagLoaderHandler:Landroid/os/Handler;

.field private mTagLoaderThread:Landroid/os/HandlerThread;

.field private mUseAsyncTagLoader:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0, p1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 53
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    .line 55
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderHandler:Landroid/os/Handler;

    .line 57
    const/16 v0, 0x190

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->DELAY_TIME_FOR_QUERY:I

    .line 59
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->ACTION_ID_CANCELED:I

    .line 61
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderThread:Landroid/os/HandlerThread;

    .line 63
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCurrentRunnable:Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;

    .line 67
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;

    .line 69
    const-class v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    .line 75
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mUseAsyncTagLoader:Z

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    .line 79
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->MSG_SEARCH_REQUEST:I

    .line 81
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->MSG_SEARCH_CANCEL:I

    .line 83
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->MSG_SEARCH_COMPLETED:I

    .line 85
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->MSG_SEARCH_TAG_RETRIEVED:I

    .line 87
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCPDataList:Ljava/util/List;

    .line 94
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->bStopTagRetrieve:Z

    .line 96
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG_TYPE:I

    .line 98
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG_VALUE:I

    .line 644
    const-string v0, "location"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->LOCATION:Ljava/lang/String;

    .line 646
    const-string v0, "people"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->PEOPLE:Ljava/lang/String;

    .line 648
    const-string v0, "weather"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->WEATHER:Ljava/lang/String;

    .line 650
    const-string v0, "userdef"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->USERDEF:Ljava/lang/String;

    .line 652
    const-string v0, "="

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAGDELIMITER:Ljava/lang/String;

    .line 654
    const-string v0, ","

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAGDELIMITER_SUB:Ljava/lang/String;

    .line 102
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->init()V

    .line 103
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0, p1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 53
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    .line 55
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderHandler:Landroid/os/Handler;

    .line 57
    const/16 v0, 0x190

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->DELAY_TIME_FOR_QUERY:I

    .line 59
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->ACTION_ID_CANCELED:I

    .line 61
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderThread:Landroid/os/HandlerThread;

    .line 63
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCurrentRunnable:Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;

    .line 67
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;

    .line 69
    const-class v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    .line 75
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mUseAsyncTagLoader:Z

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    .line 79
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->MSG_SEARCH_REQUEST:I

    .line 81
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->MSG_SEARCH_CANCEL:I

    .line 83
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->MSG_SEARCH_COMPLETED:I

    .line 85
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->MSG_SEARCH_TAG_RETRIEVED:I

    .line 87
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCPDataList:Ljava/util/List;

    .line 94
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->bStopTagRetrieve:Z

    .line 96
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG_TYPE:I

    .line 98
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG_VALUE:I

    .line 644
    const-string v0, "location"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->LOCATION:Ljava/lang/String;

    .line 646
    const-string v0, "people"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->PEOPLE:Ljava/lang/String;

    .line 648
    const-string v0, "weather"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->WEATHER:Ljava/lang/String;

    .line 650
    const-string v0, "userdef"

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->USERDEF:Ljava/lang/String;

    .line 652
    const-string v0, "="

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAGDELIMITER:Ljava/lang/String;

    .line 654
    const-string v0, ","

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAGDELIMITER_SUB:Ljava/lang/String;

    .line 107
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    .line 108
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->init()V

    .line 109
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->getAvailableEngines(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->startWebSearchQuery(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->checkInconsitentRecord(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->retrieveTagsFromTagService(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->DELAY_TIME_FOR_QUERY:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->bStopTagRetrieve:Z

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->cancelTagLoaderThread()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->getCurrentRunnable()Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController;
    .param p1, "x1"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->setCurrentRunnable(Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;)V

    return-void
.end method

.method private cancelTagLoaderThread()V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 286
    :cond_0
    return-void
.end method

.method private declared-synchronized checkInconsitentRecord(Landroid/database/Cursor;)V
    .locals 17
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 547
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    .line 549
    .local v11, "resolver":Landroid/content/ContentResolver;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 551
    .local v9, "operationlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz p1, :cond_5

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-lez v14, :cond_5

    .line 552
    const-string v14, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 553
    .local v5, "idx_id":I
    const-string v14, "content_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 554
    .local v4, "idx_cid":I
    const-string v14, "tag_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 555
    .local v7, "idx_tid":I
    const-string v14, "type"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 556
    .local v8, "idx_ttype":I
    const-string v14, "rawdata"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 557
    .local v6, "idx_raw":I
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 558
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 559
    .local v1, "_id":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 560
    .local v2, "content_id":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 561
    .local v12, "tag_id":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 562
    .local v13, "tag_type":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 564
    .local v10, "rawdata":Ljava/lang/String;
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    if-eqz v12, :cond_1

    if-eqz v13, :cond_1

    if-nez v10, :cond_2

    .line 566
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v15, "checkInconsitentRecord() : value is wrong"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 547
    .end local v1    # "_id":Ljava/lang/String;
    .end local v2    # "content_id":Ljava/lang/String;
    .end local v4    # "idx_cid":I
    .end local v5    # "idx_id":I
    .end local v6    # "idx_raw":I
    .end local v7    # "idx_tid":I
    .end local v8    # "idx_ttype":I
    .end local v9    # "operationlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v10    # "rawdata":Ljava/lang/String;
    .end local v11    # "resolver":Landroid/content/ContentResolver;
    .end local v12    # "tag_id":Ljava/lang/String;
    .end local v13    # "tag_type":Ljava/lang/String;
    :catchall_0
    move-exception v14

    monitor-exit p0

    throw v14

    .line 571
    .restart local v1    # "_id":Ljava/lang/String;
    .restart local v2    # "content_id":Ljava/lang/String;
    .restart local v4    # "idx_cid":I
    .restart local v5    # "idx_id":I
    .restart local v6    # "idx_raw":I
    .restart local v7    # "idx_tid":I
    .restart local v8    # "idx_ttype":I
    .restart local v9    # "operationlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v10    # "rawdata":Ljava/lang/String;
    .restart local v11    # "resolver":Landroid/content/ContentResolver;
    .restart local v12    # "tag_id":Ljava/lang/String;
    .restart local v13    # "tag_type":Ljava/lang/String;
    :cond_2
    :try_start_1
    sget-object v14, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tagging;->CONTENT_URI:Landroid/net/Uri;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "_id="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v14, v15, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 577
    :try_start_2
    const-string v14, "content_id"

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v2}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->checkValidRecord(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_3

    .line 578
    sget-object v14, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Contents;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v14}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "_id="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v14

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 582
    :cond_3
    const-string v14, "tag_id"

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v12}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->checkValidRecord(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_0

    .line 583
    sget-object v14, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v14}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "_id="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v14

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    sget-object v14, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$TagsFts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v14}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "type="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " AND "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "rawdata"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "= \'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v14

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 572
    :catch_0
    move-exception v3

    .line 573
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto/16 :goto_0

    .line 598
    .end local v1    # "_id":Ljava/lang/String;
    .end local v2    # "content_id":Ljava/lang/String;
    .end local v3    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v10    # "rawdata":Ljava/lang/String;
    .end local v12    # "tag_id":Ljava/lang/String;
    .end local v13    # "tag_type":Ljava/lang/String;
    :cond_4
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v14

    if-lez v14, :cond_5

    .line 600
    :try_start_3
    const-string v14, "com.samsung.android.app.galaxyfinder.tag"

    invoke-virtual {v11, v14, v9}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 608
    .end local v4    # "idx_cid":I
    .end local v5    # "idx_id":I
    .end local v6    # "idx_raw":I
    .end local v7    # "idx_tid":I
    .end local v8    # "idx_ttype":I
    :cond_5
    :goto_1
    if-eqz p1, :cond_6

    .line 609
    :try_start_4
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 612
    :cond_6
    monitor-exit p0

    return-void

    .line 601
    .restart local v4    # "idx_cid":I
    .restart local v5    # "idx_id":I
    .restart local v6    # "idx_raw":I
    .restart local v7    # "idx_tid":I
    .restart local v8    # "idx_ttype":I
    :catch_1
    move-exception v3

    .line 602
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_5
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 603
    .end local v3    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v3

    .line 604
    .local v3, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v3}, Landroid/content/OperationApplicationException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized checkValidRecord(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 617
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "tagcount"

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 619
    .local v1, "TAG_URI":Landroid/net/Uri;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 621
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 622
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 623
    .local v7, "isValid":Z
    const/4 v8, 0x0

    .line 624
    .local v8, "neverUsed":I
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 640
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .line 628
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 629
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 630
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 631
    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-lez v2, :cond_2

    .line 632
    const/4 v7, 0x1

    .line 637
    :cond_2
    if-eqz v6, :cond_3

    .line 638
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    :goto_1
    move v2, v7

    .line 640
    goto :goto_0

    .line 634
    :catch_0
    move-exception v9

    .line 635
    .local v9, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 637
    if-eqz v6, :cond_3

    .line 638
    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 617
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    .end local v1    # "TAG_URI":Landroid/net/Uri;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "isValid":Z
    .end local v8    # "neverUsed":I
    .end local v9    # "se":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 637
    .restart local v0    # "resolver":Landroid/content/ContentResolver;
    .restart local v1    # "TAG_URI":Landroid/net/Uri;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "isValid":Z
    .restart local v8    # "neverUsed":I
    :catchall_1
    move-exception v2

    if-eqz v6, :cond_4

    .line 638
    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private decodeTagQuery(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p1, "tagQuery"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x2

    .line 657
    if-nez p1, :cond_1

    .line 658
    const/4 v2, 0x0

    .line 685
    :cond_0
    :goto_0
    return-object v2

    .line 660
    :cond_1
    new-array v2, v4, [Ljava/lang/String;

    .line 662
    .local v2, "result":[Ljava/lang/String;
    :try_start_0
    const-string v3, "="

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 663
    .local v1, "firstStep":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v3, v1

    if-lt v3, v4, :cond_0

    .line 664
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-object v4, v1, v4

    aput-object v4, v2, v3

    .line 665
    const/4 v3, 0x1

    aget-object v3, v1, v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    aget-object v3, v1, v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 666
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-object v4, v1, v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    aput-object v4, v2, v3

    .line 668
    :cond_2
    const-string v3, "location"

    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 669
    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 677
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "decodeTagQuery() : tagType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v2, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " tagVale = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v5, v2, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 680
    .end local v1    # "firstStep":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 681
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 670
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "firstStep":[Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v3, "people"

    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 671
    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->PEOPLE:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 682
    .end local v1    # "firstStep":[Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 683
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 672
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "firstStep":[Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v3, "weather"

    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 673
    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->WEATHER:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    goto :goto_1

    .line 675
    :cond_5
    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->USERDEF:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1
.end method

.method private ensureHandler()Z
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;-><init>(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getAvailableEngines(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "from"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v0, "engines":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine;>;"
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    invoke-direct {v1}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    return-object v0
.end method

.method private getCurrentRunnable()Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCurrentRunnable:Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    return-object v0
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getTagMap(Landroid/database/Cursor;)Ljava/util/HashMap;
    .locals 12
    .param p1, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 837
    const/4 v9, 0x0

    .line 838
    .local v9, "tagMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;>;>;"
    const/4 v2, 0x0

    .local v2, "id":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "data":Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "type":Ljava/lang/String;
    const/4 v8, 0x0

    .line 839
    .local v8, "rawdata":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 840
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 841
    .local v0, "count":I
    if-lez v0, :cond_1

    .line 842
    new-instance v9, Ljava/util/HashMap;

    .end local v9    # "tagMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;>;>;"
    invoke-direct {v9, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 843
    .restart local v9    # "tagMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;>;>;"
    const-string v11, "contenturi"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 844
    .local v4, "idx_id":I
    const-string v11, "data"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 845
    .local v3, "idx_data":I
    const-string v11, "type"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 846
    .local v6, "idx_type":I
    const-string v11, "rawdata"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 847
    .local v5, "idx_raw":I
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 848
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 849
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 850
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 851
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 852
    invoke-virtual {v9, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 853
    .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;>;"
    if-nez v7, :cond_0

    .line 854
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;>;"
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 856
    .restart local v7    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;>;"
    :cond_0
    new-instance v11, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;

    invoke-direct {v11, v1, v10, v8}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 857
    invoke-virtual {v9, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 861
    .end local v0    # "count":I
    .end local v3    # "idx_data":I
    .end local v4    # "idx_id":I
    .end local v5    # "idx_raw":I
    .end local v6    # "idx_type":I
    .end local v7    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;>;"
    :cond_1
    return-object v9
.end method

.method private init()V
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;

    .line 113
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;

    .line 114
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mUseAsyncTagLoader:Z

    .line 116
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mUseAsyncTagLoader:Z

    if-eqz v0, :cond_0

    .line 117
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->prepareTagLoaderThread()V

    .line 120
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->initSearchCPList()V

    .line 121
    return-void
.end method

.method private initSearchCPList()V
    .locals 2

    .prologue
    .line 124
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/app/galaxyfinder/cp/CPISO;->getCurrentISO(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "ISO":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCPDataList:Ljava/util/List;

    .line 128
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/samsung/android/app/galaxyfinder/cp/CPDomParser;->getCPData(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCPDataList:Ljava/util/List;

    .line 129
    return-void
.end method

.method private makeWebSearchResult(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;Z)Ljava/lang/Object;
    .locals 19
    .param p1, "qaction"    # Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    .param p2, "defaultCPData"    # Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .param p3, "bOneCategory"    # Z

    .prologue
    .line 690
    const/4 v13, 0x0

    .line 691
    .local v13, "result":Ljava/lang/Object;
    if-nez p1, :cond_1

    .line 692
    const/4 v13, 0x0

    .line 796
    .end local v13    # "result":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v13

    .line 695
    .restart local v13    # "result":Ljava/lang/Object;
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const/high16 v16, 0x7f080000

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    .line 696
    .local v9, "isTablet":Z
    const/4 v6, 0x0

    .line 697
    .local v6, "enableWebSearch":Z
    if-eqz v9, :cond_3

    .line 698
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v15

    const-string v16, "websearch"

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->isCheckedApplication(Ljava/lang/String;)Z

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_2

    .line 700
    const/4 v13, 0x0

    goto :goto_0

    .line 702
    :cond_2
    const/4 v6, 0x1

    .line 706
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v12

    .line 708
    .local v12, "query":Ljava/lang/String;
    if-eqz v12, :cond_4

    .line 709
    invoke-static {v12}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 712
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    if-eqz v15, :cond_6

    if-eqz p1, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getQuery()Landroid/os/Bundle;

    move-result-object v15

    if-eqz v15, :cond_6

    .line 713
    const-string v15, "query_from_reload"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getFrom()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v15

    if-nez v15, :cond_6

    .line 714
    :cond_5
    const/4 v13, 0x0

    goto :goto_0

    .line 718
    :cond_6
    new-instance v4, Landroid/database/MatrixCursor;

    sget-object v15, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->COLUMNS:[Ljava/lang/String;

    invoke-direct {v4, v15}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 721
    .local v4, "cursor":Landroid/database/MatrixCursor;
    if-nez p2, :cond_a

    if-nez p3, :cond_a

    .line 723
    :try_start_0
    sget-boolean v15, Lcom/samsung/android/app/galaxyfinder/Constants;->C_CHINA:Z

    if-eqz v15, :cond_8

    .line 724
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e000b

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 726
    .local v10, "key":Ljava/lang/String;
    if-nez v6, :cond_7

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v15

    invoke-virtual {v15, v10}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->isCheckedApplication(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 728
    :cond_7
    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v10, v15, v16

    const/16 v16, 0x1

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "android.resource://"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const v18, 0x7f02018c

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const-string v17, "android.intent.action.VIEW"

    aput-object v17, v15, v16

    const/16 v16, 0x3

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "http://www.baidu.com/s?wd="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v4, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 772
    .end local v10    # "key":Ljava/lang/String;
    :cond_8
    if-eqz v4, :cond_0

    .line 774
    :try_start_1
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->getCount()I

    move-result v15

    if-lez v15, :cond_9

    .line 775
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-direct {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;-><init>(Landroid/content/Context;)V

    .line 777
    .local v1, "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setQuery(Ljava/lang/String;)V

    .line 778
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e00a4

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setCategoryName(Ljava/lang/String;)V

    .line 780
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setPackageName(Ljava/lang/String;)V

    .line 781
    new-instance v15, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v16

    const-string v17, "GalaxyFinderActivity"

    invoke-direct/range {v15 .. v17}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setSearchActivity(Landroid/content/ComponentName;)V

    .line 783
    const-string v15, "suggest_template_web_link"

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setTemplateStyle(Ljava/lang/String;)V

    .line 784
    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setData(Landroid/database/Cursor;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 786
    move-object v13, v1

    .line 791
    .end local v1    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .end local v13    # "result":Ljava/lang/Object;
    :cond_9
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_0

    .line 738
    .restart local v13    # "result":Ljava/lang/Object;
    :cond_a
    const/4 v3, 0x0

    .line 740
    .local v3, "cpname":Ljava/lang/String;
    if-eqz p3, :cond_8

    .line 741
    :try_start_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCPDataList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_b
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;

    .line 742
    .local v2, "cp":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getName()Ljava/lang/String;

    move-result-object v3

    .line 743
    if-nez v6, :cond_c

    if-eqz v3, :cond_b

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v15

    sget-object v16, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->isCheckedApplication(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 746
    :cond_c
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getName()Ljava/lang/String;

    move-result-object v11

    .line 747
    .local v11, "name":Ljava/lang/String;
    const/4 v8, 0x0

    .line 748
    .local v8, "icon":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getParams()Ljava/lang/String;

    move-result-object v15

    if-nez v15, :cond_f

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getUrl()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 751
    .local v14, "url":Ljava/lang/String;
    :goto_2
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getIcon()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_d

    .line 752
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getIcon()Ljava/lang/String;

    move-result-object v15

    const-string v16, "android.resource"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_10

    .line 754
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getIcon()Ljava/lang/String;

    move-result-object v8

    .line 760
    :cond_d
    :goto_3
    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v11, v15, v16

    const/16 v16, 0x1

    aput-object v8, v15, v16

    const/16 v16, 0x2

    const-string v17, "android.intent.action.VIEW"

    aput-object v17, v15, v16

    const/16 v16, 0x3

    aput-object v14, v15, v16

    invoke-virtual {v4, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    goto :goto_1

    .line 767
    .end local v2    # "cp":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .end local v3    # "cpname":Ljava/lang/String;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "icon":Ljava/lang/String;
    .end local v11    # "name":Ljava/lang/String;
    .end local v14    # "url":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 768
    .local v5, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    invoke-virtual {v5}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 772
    if-eqz v4, :cond_0

    .line 774
    :try_start_4
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->getCount()I

    move-result v15

    if-lez v15, :cond_e

    .line 775
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-direct {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;-><init>(Landroid/content/Context;)V

    .line 777
    .restart local v1    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setQuery(Ljava/lang/String;)V

    .line 778
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e00a4

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setCategoryName(Ljava/lang/String;)V

    .line 780
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setPackageName(Ljava/lang/String;)V

    .line 781
    new-instance v15, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v16

    const-string v17, "GalaxyFinderActivity"

    invoke-direct/range {v15 .. v17}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setSearchActivity(Landroid/content/ComponentName;)V

    .line 783
    const-string v15, "suggest_template_web_link"

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setTemplateStyle(Ljava/lang/String;)V

    .line 784
    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setData(Landroid/database/Cursor;)V
    :try_end_4
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 786
    move-object v13, v1

    .line 791
    .end local v1    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .end local v13    # "result":Ljava/lang/Object;
    :cond_e
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_0

    .line 748
    .end local v5    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v2    # "cp":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .restart local v3    # "cpname":Ljava/lang/String;
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v8    # "icon":Ljava/lang/String;
    .restart local v11    # "name":Ljava/lang/String;
    .restart local v13    # "result":Ljava/lang/Object;
    :cond_f
    :try_start_5
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getUrl()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getParams()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_2

    .line 756
    .restart local v14    # "url":Ljava/lang/String;
    :cond_10
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;->getIcon()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/samsung/android/app/galaxyfinder/cp/CPFileLoad;->getIconURI(Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v8

    goto/16 :goto_3

    .line 788
    .end local v2    # "cp":Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;
    .end local v3    # "cpname":Ljava/lang/String;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "icon":Ljava/lang/String;
    .end local v11    # "name":Ljava/lang/String;
    .end local v14    # "url":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 789
    .local v5, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v16, "resource not found exception"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 791
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_0

    .end local v5    # "e":Landroid/content/res/Resources$NotFoundException;
    :catchall_0
    move-exception v15

    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    throw v15

    .line 788
    .local v5, "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v5

    .line 789
    .local v5, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v16, "resource not found exception"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 791
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_0

    .end local v5    # "e":Landroid/content/res/Resources$NotFoundException;
    :catchall_1
    move-exception v15

    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    throw v15

    .line 769
    :catch_3
    move-exception v5

    .line 770
    .restart local v5    # "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_8
    invoke-virtual {v5}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 772
    if-eqz v4, :cond_0

    .line 774
    :try_start_9
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->getCount()I

    move-result v15

    if-lez v15, :cond_11

    .line 775
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-direct {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;-><init>(Landroid/content/Context;)V

    .line 777
    .restart local v1    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setQuery(Ljava/lang/String;)V

    .line 778
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e00a4

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setCategoryName(Ljava/lang/String;)V

    .line 780
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setPackageName(Ljava/lang/String;)V

    .line 781
    new-instance v15, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v16

    const-string v17, "GalaxyFinderActivity"

    invoke-direct/range {v15 .. v17}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setSearchActivity(Landroid/content/ComponentName;)V

    .line 783
    const-string v15, "suggest_template_web_link"

    invoke-virtual {v1, v15}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setTemplateStyle(Ljava/lang/String;)V

    .line 784
    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setData(Landroid/database/Cursor;)V
    :try_end_9
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 786
    move-object v13, v1

    .line 791
    .end local v1    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .end local v13    # "result":Ljava/lang/Object;
    :cond_11
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_0

    .line 788
    .restart local v13    # "result":Ljava/lang/Object;
    :catch_4
    move-exception v5

    .line 789
    :try_start_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v16, "resource not found exception"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 791
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_0

    :catchall_2
    move-exception v15

    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    throw v15

    .line 772
    .end local v5    # "e":Landroid/content/res/Resources$NotFoundException;
    :catchall_3
    move-exception v15

    if-eqz v4, :cond_13

    .line 774
    :try_start_b
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->getCount()I

    move-result v16

    if-lez v16, :cond_12

    .line 775
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v1, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;-><init>(Landroid/content/Context;)V

    .line 777
    .restart local v1    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setQuery(Ljava/lang/String;)V

    .line 778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0e00a4

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setCategoryName(Ljava/lang/String;)V

    .line 780
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setPackageName(Ljava/lang/String;)V

    .line 781
    new-instance v16, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "GalaxyFinderActivity"

    invoke-direct/range {v16 .. v18}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setSearchActivity(Landroid/content/ComponentName;)V

    .line 783
    const-string v16, "suggest_template_web_link"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setTemplateStyle(Ljava/lang/String;)V

    .line 784
    invoke-virtual {v1, v4}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setData(Landroid/database/Cursor;)V
    :try_end_b
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 786
    move-object v13, v1

    .line 791
    .end local v1    # "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    .end local v13    # "result":Ljava/lang/Object;
    :cond_12
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    :cond_13
    :goto_4
    throw v15

    .line 788
    .restart local v13    # "result":Ljava/lang/Object;
    :catch_5
    move-exception v5

    .line 789
    .restart local v5    # "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string v17, "resource not found exception"

    invoke-static/range {v16 .. v17}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 791
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    goto :goto_4

    .end local v5    # "e":Landroid/content/res/Resources$NotFoundException;
    :catchall_4
    move-exception v15

    invoke-virtual {v4}, Landroid/database/MatrixCursor;->close()V

    throw v15
.end method

.method private postTagLoaderObject(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 271
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;-><init>(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Landroid/os/Handler;Ljava/lang/Object;)V

    .line 272
    .local v0, "r":Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagLoaderRunnable;
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 273
    return-void
.end method

.method private prepareTagLoaderThread()V
    .locals 2

    .prologue
    .line 276
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "TagLoaderThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderThread:Landroid/os/HandlerThread;

    .line 277
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 279
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderHandler:Landroid/os/Handler;

    .line 280
    return-void
.end method

.method private declared-synchronized removeInvalidTag(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 480
    .local p3, "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;

    invoke-direct {v0, p0, p3, p2, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$2;-><init>(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    .local v0, "dbTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542
    monitor-exit p0

    return-void

    .line 480
    .end local v0    # "dbTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private retrieveTagsFromTagService(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Z
    .locals 18
    .param p1, "category"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    .line 865
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->supportTagSearch()Z

    move-result v3

    if-nez v3, :cond_1

    .line 866
    const/4 v9, 0x0

    .line 905
    :cond_0
    :goto_0
    return v9

    .line 869
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 870
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDb$Tags;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "apptag"

    invoke-static {v3, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 872
    .local v2, "TAG_URI":Landroid/net/Uri;
    const/4 v9, 0x0

    .line 874
    .local v9, "hasTags":Z
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v13

    .line 875
    .local v13, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    const/16 v17, 0x0

    .line 876
    .local v17, "tagMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;>;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getPackageName()Ljava/lang/String;

    move-result-object v15

    .line 877
    .local v15, "packageName":Ljava/lang/String;
    if-nez v15, :cond_2

    .line 878
    const/4 v9, 0x0

    goto :goto_0

    .line 880
    :cond_2
    new-instance v16, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "appname =\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 882
    .local v16, "queryBuilder":Ljava/lang/StringBuilder;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 883
    .local v4, "selection":Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 885
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_3

    .line 886
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->getTagMap(Landroid/database/Cursor;)Ljava/util/HashMap;

    move-result-object v17

    .line 887
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 890
    :cond_3
    if-eqz v17, :cond_0

    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 891
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    .line 892
    .local v12, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionUri()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 893
    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionUri()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/ArrayList;

    .line 894
    .local v14, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;>;"
    if-eqz v14, :cond_4

    .line 896
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;

    .line 897
    .local v7, "aInfo":Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->bStopTagRetrieve:Z

    if-nez v3, :cond_5

    .line 898
    iget-object v3, v7, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;->data:Ljava/lang/String;

    iget-object v5, v7, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;->type:Ljava/lang/String;

    iget-object v6, v7, Lcom/samsung/android/app/galaxyfinder/search/SearchController$TagInfo;->rawData:Ljava/lang/String;

    invoke-virtual {v12, v3, v5, v6}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->addTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    const/4 v9, 0x1

    goto :goto_1
.end method

.method private setCurrentRunnable(Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;)V
    .locals 0
    .param p1, "r"    # Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    .prologue
    .line 296
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCurrentRunnable:Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    .line 297
    return-void
.end method

.method private startWebSearchQuery(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)V
    .locals 8
    .param p1, "qaction"    # Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 445
    const/4 v0, 0x0

    .line 447
    .local v0, "defaultResult":Ljava/lang/Object;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCPDataList:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mCPDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 448
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v4, "cp list is null or empty"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    invoke-direct {p0, p1, v7, v5}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->makeWebSearchResult(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;Z)Ljava/lang/Object;

    move-result-object v0

    .line 454
    :goto_0
    if-eqz v0, :cond_4

    .line 455
    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 456
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getFrom()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;->onSearchResultUpdated(Ljava/lang/Object;Ljava/lang/String;)V

    .line 468
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 470
    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 471
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    invoke-interface {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;->onSearchCompleted()V

    .line 474
    :cond_2
    iput-boolean v6, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->bStopTagRetrieve:Z

    .line 475
    return-void

    .line 451
    :cond_3
    invoke-direct {p0, p1, v7, v6}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->makeWebSearchResult(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;Lcom/samsung/android/app/galaxyfinder/cp/cpdata/CPData;Z)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 459
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->isValidResult()Z

    move-result v3

    if-eq v3, v6, :cond_1

    .line 460
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTags()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 461
    .local v2, "tagQuery":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->decodeTagQuery(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 462
    .local v1, "result":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 463
    aget-object v3, v1, v5

    aget-object v4, v1, v6

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getTargets()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->removeInvalidTag(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_1
.end method

.method private stopTagLoaderThread()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 291
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mTagLoaderThread:Landroid/os/HandlerThread;

    .line 293
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceiveSearchResponse(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;Ljava/lang/Object;)V
    .locals 5
    .param p1, "qaction"    # Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    .param p2, "category"    # Ljava/lang/Object;

    .prologue
    .line 389
    if-nez p1, :cond_0

    .line 390
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v3, "qaction is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    :goto_0
    return-void

    .line 394
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive response (id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 398
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    if-ltz v2, :cond_1

    .line 399
    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    if-ge v2, v3, :cond_1

    .line 400
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ignored (lastest id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    const/4 p1, 0x0

    .line 405
    goto :goto_0

    .line 409
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    .line 410
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->bStopTagRetrieve:Z

    .line 412
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mUseAsyncTagLoader:Z

    if-nez v2, :cond_3

    move-object v0, p2

    .line 413
    check-cast v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .line 416
    .local v0, "categoryInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->retrieveTagsFromTagService(Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)Z

    .line 418
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 419
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getFrom()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p2, v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;->onSearchResultUpdated(Ljava/lang/Object;Ljava/lang/String;)V

    .line 427
    .end local v0    # "categoryInfo":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 429
    .local v1, "remainingAction":I
    if-nez v1, :cond_4

    .line 430
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v3, "all actions are removed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->startWebSearchQuery(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)V

    goto/16 :goto_0

    .line 422
    .end local v1    # "remainingAction":I
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    invoke-virtual {p1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getFrom()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p2, v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;->onSearchResultUpdated(Ljava/lang/Object;Ljava/lang/String;)V

    .line 423
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->postTagLoaderObject(Ljava/lang/Object;)V

    goto :goto_1

    .line 437
    .restart local v1    # "remainingAction":I
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 440
    .end local v1    # "remainingAction":I
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v3, "removed action"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public quit()Z
    .locals 1

    .prologue
    .line 910
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->stopTagLoaderThread()V

    .line 911
    invoke-super {p0}, Landroid/os/HandlerThread;->quit()Z

    move-result v0

    return v0
.end method

.method public requestCancel()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 252
    iput-boolean v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->bStopTagRetrieve:Z

    .line 254
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v1, "requestCancel"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lastest action (id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->ACTION_ID_CANCELED:I

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    .line 259
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 260
    return-void
.end method

.method public requestSearch(Landroid/os/Bundle;)I
    .locals 6
    .param p1, "query"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 223
    const/4 v0, -0x1

    .line 225
    .local v0, "id":I
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v3, "request search"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->ensureHandler()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 228
    if-eqz p1, :cond_0

    .line 229
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-direct {v1, p1, v5}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;-><init>(Landroid/os/Bundle;I)V

    .line 231
    .local v1, "qaction":Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v0

    .line 234
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 238
    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mLastQueryId:I

    .line 239
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 241
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "created query action (id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    .end local v1    # "qaction":Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    :cond_0
    :goto_0
    return v0

    .line 245
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;

    const-string v3, "handler is not prepared yet"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setListener(Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    .prologue
    .line 263
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    .line 264
    return-void
.end method
