.class public interface abstract Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView$OnActionListener;
.super Ljava/lang/Object;
.source "FilterPanelMainView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterPanelMainView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnActionListener"
.end annotation


# virtual methods
.method public abstract onActionGroupChanged(I)V
.end method

.method public abstract onActionHandwriting(Ljava/lang/String;)V
.end method

.method public abstract onActionItemAdded(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
.end method

.method public abstract onActionItemChanged(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
.end method

.method public abstract onActionItemRemoved(Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;)V
.end method

.method public abstract onActionItemsAdded(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;)V"
        }
    .end annotation
.end method
