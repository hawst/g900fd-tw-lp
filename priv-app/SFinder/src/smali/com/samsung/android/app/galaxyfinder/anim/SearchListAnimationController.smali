.class public Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;
.super Ljava/lang/Object;
.source "SearchListAnimationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;
    }
.end annotation


# static fields
.field public static final APPEARING_DURATION:I = 0xc8

.field public static final DISAPPEARING_DURATION:I = 0xc8


# instance fields
.field public final SCROLLVIEW_ANIM_STATE_EXPANDED:I

.field public final SCROLLVIEW_ANIM_STATE_FOLDED:I

.field public final SCROLLVIEW_ANIM_STATE_HEADUP:I

.field public final SCROLLVIEW_ANIM_STATE_WAITING_FOR_RESULT_TO_EXPAND:I

.field SEARCHLIST_ITEM_APPEARING_POSITION:I

.field SEARCHLIST_POSITION_FOLDED:I

.field SEARCHLIST_POSITION_HEADUP:I

.field private TAG:Ljava/lang/String;

.field isaHeadUpAnimationCanceled:Z

.field private mAnimSearchItemTransition:Landroid/animation/LayoutTransition;

.field private mContext:Landroid/content/Context;

.field private mHeadUpOmittedFlag:Z

.field private mScrollView:Landroid/view/ViewGroup;

.field private mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

.field private mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

.field private mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

.field private mScrollViewFrame:Landroid/view/ViewGroup;

.field private mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

.field private mStateScrollViewExpand:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "animRootView"    # Landroid/view/ViewGroup;
    .param p3, "searchListAnimationListener"    # Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-class v0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->TAG:Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    .line 24
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    .line 26
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollView:Landroid/view/ViewGroup;

    .line 28
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    .line 30
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    .line 34
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mAnimSearchItemTransition:Landroid/animation/LayoutTransition;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    .line 38
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SCROLLVIEW_ANIM_STATE_FOLDED:I

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SCROLLVIEW_ANIM_STATE_HEADUP:I

    .line 42
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SCROLLVIEW_ANIM_STATE_EXPANDED:I

    .line 44
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SCROLLVIEW_ANIM_STATE_WAITING_FOR_RESULT_TO_EXPAND:I

    .line 46
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    .line 48
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->isaHeadUpAnimationCanceled:Z

    .line 50
    const/16 v0, 0x4b0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_FOLDED:I

    .line 52
    const/16 v0, 0x258

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    .line 54
    const/16 v0, 0x384

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_ITEM_APPEARING_POSITION:I

    .line 56
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mHeadUpOmittedFlag:Z

    .line 64
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    .line 66
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    .line 68
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->init()V

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    return-object v0
.end method

.method private getScrollViewTopMargin()I
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    return v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_FOLDED:I

    .line 74
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_ITEM_APPEARING_POSITION:I

    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    const v1, 0x7f0b00be

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollView:Landroid/view/ViewGroup;

    .line 79
    return-void
.end method

.method private setTransitAnimation(Landroid/animation/LayoutTransition;)V
    .locals 10
    .param p1, "searchItemTransition"    # Landroid/animation/LayoutTransition;

    .prologue
    const-wide/16 v2, 0xc8

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    .line 314
    invoke-virtual {p1, v6, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 315
    invoke-virtual {p1, v7, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 316
    invoke-virtual {p1, v8}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    .line 319
    const-string v2, "alpha"

    new-array v3, v6, [F

    fill-array-data v3, :array_0

    invoke-static {v9, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {p1, v7}, Landroid/animation/LayoutTransition;->getDuration(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 322
    .local v1, "animOut":Landroid/animation/ObjectAnimator;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    const v3, 0x10a0005

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 324
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$4;-><init>(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 333
    invoke-virtual {p1, v7, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 336
    const-string v2, "translationY"

    new-array v3, v6, [F

    iget v4, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_ITEM_APPEARING_POSITION:I

    int-to-float v4, v4

    aput v4, v3, v8

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    invoke-static {v9, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {p1, v6}, Landroid/animation/LayoutTransition;->getDuration(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 340
    .local v0, "animIn":Landroid/animation/ObjectAnimator;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    const v3, 0x10a0006

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 342
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$5;-><init>(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 355
    const-wide/16 v2, 0x0

    invoke-virtual {p1, v6, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 356
    invoke-virtual {p1, v6, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 357
    return-void

    .line 319
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public getSearchListLayoutTransitionAnimation()Landroid/animation/LayoutTransition;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mAnimSearchItemTransition:Landroid/animation/LayoutTransition;

    if-nez v0, :cond_0

    .line 306
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mAnimSearchItemTransition:Landroid/animation/LayoutTransition;

    .line 307
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mAnimSearchItemTransition:Landroid/animation/LayoutTransition;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setTransitAnimation(Landroid/animation/LayoutTransition;)V

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mAnimSearchItemTransition:Landroid/animation/LayoutTransition;

    return-object v0
.end method

.method public isHeadupOmitted()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mHeadUpOmittedFlag:Z

    return v0
.end method

.method public isSearchListExpanded()Z
    .locals 2

    .prologue
    .line 82
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchListFolded()Z
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchListHeadUp()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 86
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchListWaitingForResult()Z
    .locals 2

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public playScrollViewExpand(Z)V
    .locals 7
    .param p1, "anim"    # Z

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 235
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    if-ne v1, v6, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->getScrollViewTopMargin()I

    move-result v1

    if-nez v1, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 247
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 250
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 251
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->end()V

    .line 254
    :cond_4
    iput v6, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    .line 255
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    invoke-interface {v1}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;->beforeExpand()V

    .line 256
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 258
    if-nez p1, :cond_5

    .line 259
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    .line 261
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    goto :goto_0

    .line 265
    :cond_5
    new-array v0, v5, [Landroid/animation/PropertyValuesHolder;

    .line 266
    .local v0, "arrayOfPropertyValuesHolder":[Landroid/animation/PropertyValuesHolder;
    const-string v1, "ScrollViewTopMargin"

    new-array v2, v6, [I

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->getScrollViewTopMargin()I

    move-result v3

    aput v3, v2, v4

    aput v4, v2, v5

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v4

    .line 269
    const-string v1, "ScrollViewTopMargin"

    new-array v2, v6, [I

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->getScrollViewTopMargin()I

    move-result v3

    aput v3, v2, v4

    aput v4, v2, v5

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    .line 271
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0c0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 273
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 275
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    const v3, 0x10a0006

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 277
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 278
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    new-instance v2, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$3;-><init>(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 289
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_0
.end method

.method public playScrollViewFolding(Z)V
    .locals 7
    .param p1, "anim"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 120
    const/4 v1, 0x0

    .line 122
    .local v1, "duration":I
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    if-nez v2, :cond_0

    .line 176
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 127
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 130
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 131
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimExpanding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->end()V

    .line 134
    :cond_2
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    if-ne v2, v5, :cond_3

    .line 135
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 142
    :goto_1
    iput v6, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    .line 144
    if-nez p1, :cond_4

    .line 145
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 146
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    invoke-interface {v2}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;->beforeFold()V

    .line 147
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 148
    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_FOLDED:I

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    goto :goto_0

    .line 138
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    goto :goto_1

    .line 152
    :cond_4
    new-array v0, v5, [Landroid/animation/PropertyValuesHolder;

    .line 153
    .local v0, "arrayOfPropertyValuesHolder":[Landroid/animation/PropertyValuesHolder;
    const-string v2, "ScrollViewTopMargin"

    const/4 v3, 0x2

    new-array v3, v3, [I

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->getScrollViewTopMargin()I

    move-result v4

    add-int/lit16 v4, v4, 0x1f4

    aput v4, v3, v6

    iget v4, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_FOLDED:I

    aput v4, v3, v5

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    aput-object v2, v0, v6

    .line 156
    invoke-static {p0, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    .line 158
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 159
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    const v4, 0x10a0005

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 161
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    new-instance v3, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$1;-><init>(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 172
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollviewScrollable(Z)V

    .line 174
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    invoke-interface {v2}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;->beforeFold()V

    .line 175
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_0
.end method

.method public playScrollviewHeadUp()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 179
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    if-ne v0, v3, :cond_0

    .line 232
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimFolding:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 191
    :cond_2
    iput v3, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    .line 193
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    if-eqz v0, :cond_3

    .line 194
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mSearchListAnimationListener:Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;

    invoke-interface {v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$ISearchListAnimationListener;->beforeHeadUp()V

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 198
    const-string v0, "ScrollViewTopMargin"

    const/4 v1, 0x2

    new-array v1, v1, [I

    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->getScrollViewTopMargin()I

    move-result v2

    aput v2, v1, v4

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    aput v2, v1, v3

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    .line 200
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 203
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    const v2, 0x10a0006

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 205
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController$2;-><init>(Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 231
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewControlAnimHeadUp:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method public resetHeadUpAnimPostion()I
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    .line 373
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 374
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    .line 377
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    return v0
.end method

.method public setHeadUpAnimPosition(I)I
    .locals 2
    .param p1, "headUpPosition"    # I

    .prologue
    .line 360
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    .line 362
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 363
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->setScrollViewTopMargin(I)V

    .line 366
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->SEARCHLIST_POSITION_HEADUP:I

    return v0
.end method

.method public setHeadUpAnimationOmit(Z)V
    .locals 3
    .param p1, "isOmittedflag"    # Z

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set headUp animation omitted flag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mHeadUpOmittedFlag:Z

    .line 104
    return-void
.end method

.method public setOnComputeScrollListener(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnComputeScrollListener;

    .prologue
    .line 117
    return-void
.end method

.method public setScrollViewTopMargin(I)V
    .locals 2
    .param p1, "paramInt"    # I

    .prologue
    .line 297
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 299
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 300
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollViewFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 301
    return-void
.end method

.method public setScrollviewScrollable(Z)V
    .locals 1
    .param p1, "scrollable"    # Z

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVerticalScrollBarEnabled(Z)V

    .line 113
    return-void
.end method

.method public setSearchListStateToWaitingForResult()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/anim/SearchListAnimationController;->mStateScrollViewExpand:I

    return v0
.end method
