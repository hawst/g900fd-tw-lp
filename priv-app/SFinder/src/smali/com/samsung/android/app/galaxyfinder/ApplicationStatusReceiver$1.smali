.class Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;
.super Landroid/os/Handler;
.source "ApplicationStatusReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 27
    const/4 v4, 0x0

    .line 28
    .local v4, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    .line 30
    .local v2, "context":Landroid/content/Context;
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$ReceiveType;

    if-eqz v9, :cond_2

    .line 31
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$ReceiveType;

    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$ReceiveType;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 32
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$ReceiveType;

    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$ReceiveType;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 37
    const/4 v5, 0x0

    .line 38
    .local v5, "pkgName":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "action":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->TAG:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->access$000(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[onReceive] Action : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " recevied"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 43
    .local v6, "pm":Landroid/content/pm/PackageManager;
    if-eqz v6, :cond_3

    .line 44
    const-string v9, "com.samsung.android.app.galaxyfinder"

    invoke-virtual {v6, v9}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v1

    .line 45
    .local v1, "appState":I
    const/4 v9, 0x2

    if-eq v1, v9, :cond_0

    const/4 v9, 0x3

    if-ne v1, v9, :cond_3

    .line 47
    :cond_0
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->TAG:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->access$000(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "GALAXYFINDER application(state : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") is disabled. Therefore skip this action."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "appState":I
    .end local v5    # "pkgName":Ljava/lang/String;
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    :goto_0
    return-void

    .line 34
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->TAG:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->access$000(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "[onReceive] Wrong received msg type"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 52
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v5    # "pkgName":Ljava/lang/String;
    .restart local v6    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 53
    .local v3, "cr":Landroid/content/ContentResolver;
    if-eqz v3, :cond_4

    const-string v9, "emergency_mode"

    const/4 v10, 0x0

    invoke-static {v3, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    .line 54
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->TAG:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->access$000(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "Emergency mode is ON. Therefore skip this action."

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 59
    :cond_4
    const-string v9, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    const-string v9, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    const-string v9, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    const-string v9, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 62
    :cond_5
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->TAG:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->access$000(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "Package changed"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v5

    .line 64
    iget-object v9, p0, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->checkSLinkPackagee(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v9, v2, v0, v5}, Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;->access$100(Lcom/samsung/android/app/galaxyfinder/ApplicationStatusReceiver;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->getManager()Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/SearchableAppManager;->setSearchableAppChanged()V

    .line 66
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->getInstance()Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/util/CategoryPreferences;->refreshSearchCategory()V

    .line 67
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->getInstance()Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderApplication;->makeHelperPackagesMap()V

    .line 69
    const-string v9, "com.google.android.googlequicksearchbox"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 70
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->setQuickSeachRefresh()V

    .line 74
    :cond_6
    :try_start_0
    new-instance v7, Landroid/content/Intent;

    const-class v9, Lcom/samsung/android/app/galaxyfinder/tag/TagReadyService;

    invoke-direct {v7, v2, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    .local v7, "request":Landroid/content/Intent;
    const-string v9, "com.samsung.android.app.galaxyfinder.tag.reload_service"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    invoke-virtual {v2, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 77
    .end local v7    # "request":Landroid/content/Intent;
    :catch_0
    move-exception v8

    .line 78
    .local v8, "se":Ljava/lang/SecurityException;
    invoke-virtual {v8}, Ljava/lang/SecurityException;->printStackTrace()V

    goto/16 :goto_0
.end method
