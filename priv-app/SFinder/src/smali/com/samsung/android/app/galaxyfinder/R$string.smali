.class public final Lcom/samsung/android/app/galaxyfinder/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final A_month_ago:I = 0x7f0e0000

.field public static final A_week_ago:I = 0x7f0e0001

.field public static final Initial_access_to_s_finder_body_text:I = 0x7f0e0002

.field public static final Save_search_history:I = 0x7f0e0003

.field public static final Save_search_history_tts:I = 0x7f0e0004

.field public static final activity_not_found:I = 0x7f0e0005

.field public static final agree:I = 0x7f0e0006

.field public static final alphabetical:I = 0x7f0e0007

.field public static final app_info:I = 0x7f0e0008

.field public static final app_name:I = 0x7f0e0009

.field public static final applications:I = 0x7f0e000a

.field public static final baidu:I = 0x7f0e000b

.field public static final button:I = 0x7f0e000c

.field public static final card_application_help_text:I = 0x7f0e000d

.field public static final category:I = 0x7f0e000e

.field public static final category_device:I = 0x7f0e000f

.field public static final category_expand_fully_btn_text:I = 0x7f0e0010

.field public static final category_filter:I = 0x7f0e0011

.field public static final category_filter_settings:I = 0x7f0e0012

.field public static final category_order:I = 0x7f0e0013

.field public static final category_slink:I = 0x7f0e0014

.field public static final category_time:I = 0x7f0e0015

.field public static final category_time_anytime:I = 0x7f0e0016

.field public static final category_time_last_month:I = 0x7f0e0017

.field public static final category_time_last_week:I = 0x7f0e0018

.field public static final category_time_next_week:I = 0x7f0e0019

.field public static final category_time_past_24:I = 0x7f0e001a

.field public static final category_time_past_month:I = 0x7f0e001b

.field public static final category_time_past_week:I = 0x7f0e001c

.field public static final category_time_past_year:I = 0x7f0e001d

.field public static final category_titme_story_album:I = 0x7f0e001e

.field public static final category_type_filter:I = 0x7f0e001f

.field public static final character_exceedded:I = 0x7f0e0020

.field public static final chinese:I = 0x7f0e0021

.field public static final communication:I = 0x7f0e0022

.field public static final contacts_action_browser:I = 0x7f0e0023

.field public static final contacts_action_call:I = 0x7f0e0024

.field public static final contacts_action_date:I = 0x7f0e0025

.field public static final contacts_action_email:I = 0x7f0e0026

.field public static final contacts_action_map:I = 0x7f0e0027

.field public static final content_type_filter:I = 0x7f0e0028

.field public static final current_app:I = 0x7f0e0029

.field public static final custom:I = 0x7f0e002a

.field public static final custom_type_filter:I = 0x7f0e002b

.field public static final decline:I = 0x7f0e002c

.field public static final default_search_filters:I = 0x7f0e002d

.field public static final deselect_all:I = 0x7f0e002e

.field public static final double_tap_to_filter_by_tts:I = 0x7f0e002f

.field public static final double_tap_to_open_tts:I = 0x7f0e0030

.field public static final double_tap_to_search_tts:I = 0x7f0e0031

.field public static final enable_app:I = 0x7f0e0032

.field public static final error_message_failed:I = 0x7f0e0033

.field public static final error_message_unavailable_during_call:I = 0x7f0e0034

.field public static final error_msg_video_play:I = 0x7f0e0035

.field public static final error_server_error:I = 0x7f0e0036

.field public static final error_unknown_error:I = 0x7f0e0037

.field public static final exceed_select_file_number:I = 0x7f0e0038

.field public static final filter:I = 0x7f0e0039

.field public static final filter_location_enable_service:I = 0x7f0e003a

.field public static final filter_location_hint_text:I = 0x7f0e003b

.field public static final filter_select_tts:I = 0x7f0e003c

.field public static final filter_uppercase:I = 0x7f0e003d

.field public static final filter_user_hint_text:I = 0x7f0e003e

.field public static final first_alert_text_1:I = 0x7f0e003f

.field public static final first_alert_text_2:I = 0x7f0e0040

.field public static final first_alert_text_3:I = 0x7f0e0041

.field public static final floating_view:I = 0x7f0e0042

.field public static final frequently_used:I = 0x7f0e0043

.field public static final gallery_type:I = 0x7f0e00da

.field public static final guide_bubble_summary_text:I = 0x7f0e0044

.field public static final guide_bubble_summary_text1:I = 0x7f0e0045

.field public static final guide_bubble_summary_text1_1:I = 0x7f0e0046

.field public static final guide_bubble_summary_text2:I = 0x7f0e0047

.field public static final guide_bubble_summary_text3:I = 0x7f0e0048

.field public static final guide_point_filter_name:I = 0x7f0e0049

.field public static final handwriting:I = 0x7f0e004a

.field public static final handwriting_type_filter:I = 0x7f0e004b

.field public static final header_tts:I = 0x7f0e004c

.field public static final help:I = 0x7f0e004d

.field public static final help_popup_try_it_guide_text_1:I = 0x7f0e004e

.field public static final help_popup_try_it_guide_text_2:I = 0x7f0e004f

.field public static final help_popup_try_it_guide_text_4:I = 0x7f0e0050

.field public static final help_popup_try_it_guide_text_5:I = 0x7f0e0051

.field public static final help_popup_try_it_guide_text_music_2:I = 0x7f0e0052

.field public static final help_popup_try_it_guide_text_music_4:I = 0x7f0e0053

.field public static final help_wrong_touch_event:I = 0x7f0e0054

.field public static final history_delete_all:I = 0x7f0e0055

.field public static final history_delete_all_popup:I = 0x7f0e0056

.field public static final images:I = 0x7f0e0057

.field public static final information_provision_agreement:I = 0x7f0e0058

.field public static final information_provision_agreement_mgs:I = 0x7f0e0059

.field public static final intro_layout_guide:I = 0x7f0e005a

.field public static final intro_layout_item_category:I = 0x7f0e005b

.field public static final intro_layout_item_date:I = 0x7f0e005c

.field public static final intro_layout_item_location:I = 0x7f0e005d

.field public static final intro_layout_item_tag:I = 0x7f0e005e

.field public static final item_delete:I = 0x7f0e005f

.field public static final language:I = 0x7f0e0060

.field public static final location_filter:I = 0x7f0e0061

.field public static final location_filter_settings:I = 0x7f0e0062

.field public static final location_tag_hint:I = 0x7f0e0063

.field public static final location_tags:I = 0x7f0e0064

.field public static final location_type_filter:I = 0x7f0e0065

.field public static final maximum_selection_number:I = 0x7f0e0066

.field public static final me:I = 0x7f0e0067

.field public static final menu_all_selected:I = 0x7f0e0068

.field public static final menu_cancel:I = 0x7f0e0069

.field public static final menu_d_selected:I = 0x7f0e006a

.field public static final menu_delete:I = 0x7f0e006b

.field public static final menu_delete_history:I = 0x7f0e006c

.field public static final menu_done:I = 0x7f0e006d

.field public static final menu_help:I = 0x7f0e006e

.field public static final menu_select:I = 0x7f0e006f

.field public static final menu_send_to:I = 0x7f0e0070

.field public static final menu_settings:I = 0x7f0e0071

.field public static final menu_share_via:I = 0x7f0e0072

.field public static final menu_un_selected:I = 0x7f0e0073

.field public static final microphone:I = 0x7f0e0074

.field public static final music:I = 0x7f0e0075

.field public static final my_device:I = 0x7f0e0076

.field public static final myfiles_action_myfiles:I = 0x7f0e0077

.field public static final myfiles_reply_action:I = 0x7f0e0078

.field public static final no_available_items:I = 0x7f0e0079

.field public static final no_results_found:I = 0x7f0e007a

.field public static final no_speech_detected:I = 0x7f0e007b

.field public static final notes:I = 0x7f0e007c

.field public static final nuancevoice_data_connection_body:I = 0x7f0e007d

.field public static final nuancevoice_data_connection_ok:I = 0x7f0e007e

.field public static final nuancevoice_data_connection_title:I = 0x7f0e007f

.field public static final nuancevoice_link1:I = 0x7f0e0080

.field public static final nuancevoice_link1_mgs:I = 0x7f0e0081

.field public static final nuancevoice_link1_title:I = 0x7f0e0082

.field public static final nuancevoice_link2:I = 0x7f0e0083

.field public static final nuancevoice_link3:I = 0x7f0e0084

.field public static final nuancevoice_wifi_connection_body:I = 0x7f0e0085

.field public static final nuancevoice_wifi_connection_title:I = 0x7f0e0086

.field public static final past_year:I = 0x7f0e0087

.field public static final personal_information:I = 0x7f0e0088

.field public static final phone_action_call:I = 0x7f0e0089

.field public static final phone_action_message:I = 0x7f0e008a

.field public static final planner_subtitle_undated:I = 0x7f0e008b

.field public static final popup_do_not_show_again:I = 0x7f0e008c

.field public static final preparing:I = 0x7f0e008d

.field public static final recognizing:I = 0x7f0e008e

.field public static final refresh_popup_body:I = 0x7f0e008f

.field public static final refresh_popup_title:I = 0x7f0e0090

.field public static final related_face:I = 0x7f0e0091

.field public static final related_location:I = 0x7f0e0092

.field public static final related_userdef:I = 0x7f0e0093

.field public static final result_display_order:I = 0x7f0e0094

.field public static final result_display_order_tts:I = 0x7f0e0095

.field public static final retry:I = 0x7f0e0096

.field public static final search:I = 0x7f0e0097

.field public static final search_category:I = 0x7f0e0098

.field public static final search_category_tts:I = 0x7f0e0099

.field public static final search_history:I = 0x7f0e009a

.field public static final search_uppercase:I = 0x7f0e009b

.field public static final search_view_category_options:I = 0x7f0e009c

.field public static final search_view_edit_clear:I = 0x7f0e009d

.field public static final search_view_edit_voice_input:I = 0x7f0e009e

.field public static final search_view_filter_off:I = 0x7f0e009f

.field public static final search_view_filter_on:I = 0x7f0e00a0

.field public static final search_view_guide_text:I = 0x7f0e00a1

.field public static final search_view_multi_options:I = 0x7f0e00a2

.field public static final search_view_no_result_info:I = 0x7f0e00a3

.field public static final search_web:I = 0x7f0e00a4

.field public static final search_web_uppercase:I = 0x7f0e00a5

.field public static final searchable_symbols:I = 0x7f0e00a6

.field public static final searching_Recent_history:I = 0x7f0e00a7

.field public static final searching_progress_text:I = 0x7f0e00a8

.field public static final searching_tag_cloud:I = 0x7f0e00a9

.field public static final searching_try_again:I = 0x7f0e00aa

.field public static final select_all:I = 0x7f0e00ab

.field public static final select_filter:I = 0x7f0e00ac

.field public static final selected_filter_tts:I = 0x7f0e00ad

.field public static final settings:I = 0x7f0e00ae

.field public static final show_more_results:I = 0x7f0e00af

.field public static final show_more_results_expand:I = 0x7f0e00b0

.field public static final show_more_results_fold:I = 0x7f0e00b1

.field public static final show_noti:I = 0x7f0e00b2

.field public static final show_search_history:I = 0x7f0e00b3

.field public static final show_search_prediction:I = 0x7f0e00b4

.field public static final speak_in_now:I = 0x7f0e00b5

.field public static final stms_appgroup:I = 0x7f0e00b6

.field public static final symbol_help_string:I = 0x7f0e00b7

.field public static final symbol_help_string2:I = 0x7f0e00b8

.field public static final tag_filter:I = 0x7f0e00b9

.field public static final tag_filter_settings:I = 0x7f0e00ba

.field public static final task_suggestion_content_alram:I = 0x7f0e00bb

.field public static final task_suggestion_content_bookmark:I = 0x7f0e00bc

.field public static final task_suggestion_content_call:I = 0x7f0e00bd

.field public static final task_suggestion_content_contact:I = 0x7f0e00be

.field public static final task_suggestion_content_email:I = 0x7f0e00bf

.field public static final task_suggestion_content_message:I = 0x7f0e00c0

.field public static final task_suggestion_content_schedule:I = 0x7f0e00c1

.field public static final task_suggestion_content_web:I = 0x7f0e00c2

.field public static final three_days_ago:I = 0x7f0e00c3

.field public static final time_filter:I = 0x7f0e00c4

.field public static final time_filter_settings:I = 0x7f0e00c5

.field public static final time_type_filter:I = 0x7f0e00c6

.field public static final today:I = 0x7f0e00c7

.field public static final tomorrow:I = 0x7f0e00c8

.field public static final unable_to_find:I = 0x7f0e00c9

.field public static final update_popup_body:I = 0x7f0e00ca

.field public static final update_popup_title:I = 0x7f0e00cb

.field public static final use_search_history:I = 0x7f0e00cc

.field public static final user_cancelled:I = 0x7f0e00cd

.field public static final user_tag_hint:I = 0x7f0e00ce

.field public static final videos:I = 0x7f0e00cf

.field public static final voice_input_label:I = 0x7f0e00d0

.field public static final weather_clear_night:I = 0x7f0e00d1

.field public static final weather_cloudy_day:I = 0x7f0e00d2

.field public static final weather_rainny_day:I = 0x7f0e00d3

.field public static final weather_snowy_day:I = 0x7f0e00d4

.field public static final weather_sunny_day:I = 0x7f0e00d5

.field public static final web_link_title_baidu:I = 0x7f0e00d6

.field public static final web_link_title_google:I = 0x7f0e00d7

.field public static final webview_type:I = 0x7f0e00d8

.field public static final yesterday:I = 0x7f0e00d9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
