.class Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider$1;
.super Landroid/content/BroadcastReceiver;
.source "AppProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider$1;->this$0:Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 136
    if-nez p2, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider$1;->this$0:Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;->postUpdatePackage(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;->access$000(Lcom/samsung/android/app/galaxyfinder/applicationprovider/AppProvider;Ljava/lang/String;)V

    goto :goto_0
.end method
