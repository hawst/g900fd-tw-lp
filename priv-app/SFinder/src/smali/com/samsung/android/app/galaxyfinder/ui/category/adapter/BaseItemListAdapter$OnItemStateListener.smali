.class public interface abstract Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;
.super Ljava/lang/Object;
.source "BaseItemListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnItemStateListener"
.end annotation


# virtual methods
.method public abstract onActionClicked(Landroid/view/View;)V
.end method

.method public abstract onActionDragDrop(Z)V
.end method

.method public abstract onActionLongPressed(Landroid/view/View;)V
.end method

.method public abstract onChangeSelected(Z)V
.end method
