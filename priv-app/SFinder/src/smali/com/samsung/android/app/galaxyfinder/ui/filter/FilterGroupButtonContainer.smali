.class public Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;
.super Landroid/widget/LinearLayout;
.source "FilterGroupButtonContainer.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 19
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->init()V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->init()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->init()V

    .line 30
    return-void
.end method

.method private calculateChildPosition()V
    .locals 9

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getChildCount()I

    move-result v3

    .line 107
    .local v3, "count":I
    const/4 v7, 0x2

    if-le v3, v7, :cond_1

    .line 108
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getPaddingStart()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getPaddingRight()I

    move-result v8

    sub-int v0, v7, v8

    .line 109
    .local v0, "availableWidth":I
    const/4 v6, 0x0

    .line 110
    .local v6, "spacing":I
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 112
    .local v2, "childWidth":I
    mul-int v7, v2, v3

    sub-int/2addr v0, v7

    .line 113
    add-int/lit8 v7, v3, -0x1

    div-int v6, v0, v7

    .line 115
    if-gez v6, :cond_0

    .line 116
    const/4 v6, 0x0

    .line 119
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    add-int/lit8 v7, v3, -0x1

    if-ge v4, v7, :cond_1

    .line 120
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 122
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 123
    .local v5, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    .line 125
    invoke-virtual {v1, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 119
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 128
    .end local v0    # "availableWidth":I
    .end local v1    # "child":Landroid/view/View;
    .end local v2    # "childWidth":I
    .end local v4    # "i":I
    .end local v5    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "spacing":I
    :cond_1
    return-void
.end method

.method private init()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    .line 39
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.sec.feature.spen_usp"

    invoke-virtual {v9, v10}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 41
    .local v0, "bSpenEnabled":Z
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 43
    .local v7, "res":Landroid/content/res/Resources;
    const v9, 0x7f0a03c7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 44
    .local v8, "width":I
    const v9, 0x7f0a03c6

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 46
    .local v6, "height":I
    new-instance v1, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v1, v9}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 48
    .local v1, "btnCategory":Landroid/widget/ImageButton;
    const/high16 v9, 0x7f0b0000

    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setId(I)V

    .line 49
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v8, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    const v9, 0x7f0e001f

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 51
    const v9, 0x7f020042

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 52
    sget-object v9, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 53
    invoke-virtual {v1, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 55
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->addView(Landroid/view/View;)V

    .line 57
    new-instance v5, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v5, v9}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 59
    .local v5, "btnTime":Landroid/widget/ImageButton;
    const v9, 0x7f0b0003

    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setId(I)V

    .line 60
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v8, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    const v9, 0x7f0e00c6

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 62
    const v9, 0x7f020044

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 63
    sget-object v9, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 64
    invoke-virtual {v5, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 66
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->addView(Landroid/view/View;)V

    .line 68
    new-instance v2, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v2, v9}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 70
    .local v2, "btnCustom":Landroid/widget/ImageButton;
    const v9, 0x7f0b0004

    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setId(I)V

    .line 71
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v8, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    const v9, 0x7f0e002b

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 73
    const v9, 0x7f020046

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 74
    sget-object v9, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 75
    invoke-virtual {v2, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 77
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->addView(Landroid/view/View;)V

    .line 79
    new-instance v4, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v4, v9}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 81
    .local v4, "btnLocation":Landroid/widget/ImageButton;
    const v9, 0x7f0b0002

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setId(I)V

    .line 82
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v8, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    const v9, 0x7f0e0065

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 84
    const v9, 0x7f020048

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 85
    sget-object v9, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 86
    invoke-virtual {v4, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 88
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->addView(Landroid/view/View;)V

    .line 90
    if-eqz v0, :cond_0

    .line 91
    new-instance v3, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v3, v9}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 93
    .local v3, "btnHandwriting":Landroid/widget/ImageButton;
    const v9, 0x7f0b0001

    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setId(I)V

    .line 94
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v8, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 95
    const v9, 0x7f0e004b

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 96
    const v9, 0x7f02004a

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 97
    sget-object v9, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 98
    invoke-virtual {v3, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 100
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->addView(Landroid/view/View;)V

    .line 102
    .end local v3    # "btnHandwriting":Landroid/widget/ImageButton;
    :cond_0
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/FilterGroupButtonContainer;->calculateChildPosition()V

    .line 35
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 36
    return-void
.end method
