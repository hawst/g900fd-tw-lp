.class Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "SearchScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# instance fields
.field final PINCH_ZOOM_THRESHOLD:F

.field private mCurScale:F

.field private mPreScale:F

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)V
    .locals 1

    .prologue
    .line 736
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 742
    const v0, 0x3ba3d70a    # 0.005f

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->PINCH_ZOOM_THRESHOLD:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;
    .param p2, "x1"    # Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$1;

    .prologue
    .line 736
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 5
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 747
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPinchable:Z
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 775
    :cond_0
    :goto_0
    return v3

    .line 750
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mPreScale:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 753
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mPreScale:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 754
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mPreScale:F

    goto :goto_0

    .line 758
    :cond_2
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mCurScale:F

    .line 759
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 760
    const-string v0, "TEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scale pre: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mPreScale:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  Cur: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mCurScale:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    :cond_3
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mPreScale:F

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mCurScale:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3ba3d70a    # 0.005f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 763
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mCurScale:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 764
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 765
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    float-to-int v1, v1

    invoke-interface {v0, v4, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;->onPinchEvent(ZI)V

    .line 774
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPinchable:Z
    invoke-static {v0, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->access$202(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;Z)Z

    goto :goto_0

    .line 768
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 769
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mTouchEventListener:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;)Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    float-to-int v1, v1

    invoke-interface {v0, v3, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$OnTouchEventListener;->onPinchEvent(ZI)V

    goto :goto_1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v1, 0x1

    .line 780
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    .line 781
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPinchable:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->access$202(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;Z)Z

    .line 782
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->mPreScale:F

    .line 783
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 788
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    .line 789
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView$ScaleListener;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->mPinchable:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;->access$202(Lcom/samsung/android/app/galaxyfinder/ui/pager/SearchScrollView;Z)Z

    .line 790
    return-void
.end method
