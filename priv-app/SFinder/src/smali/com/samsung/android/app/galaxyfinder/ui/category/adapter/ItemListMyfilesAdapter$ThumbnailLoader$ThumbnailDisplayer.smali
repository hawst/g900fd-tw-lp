.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;
.super Ljava/lang/Object;
.source "ItemListMyfilesAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ThumbnailDisplayer"
.end annotation


# instance fields
.field drawable:Landroid/graphics/drawable/Drawable;

.field mThumbHolder:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

.field mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

.field final synthetic this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;Landroid/graphics/drawable/Drawable;Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;)V
    .locals 1
    .param p2, "d"    # Landroid/graphics/drawable/Drawable;
    .param p3, "h"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    .prologue
    .line 394
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->drawable:Landroid/graphics/drawable/Drawable;

    .line 396
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbHolder:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    .line 397
    iget-object v0, p3, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    .line 398
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f4

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 401
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 402
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->this$1:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader;->imageViewReused(Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 403
    iput-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->drawable:Landroid/graphics/drawable/Drawable;

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 408
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->drawable:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    .line 409
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconUri:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 410
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 411
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 416
    :goto_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 417
    .local v0, "anim":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 418
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 413
    .end local v0    # "anim":Landroid/view/animation/AlphaAnimation;
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbHolder:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailHolder;->mMimeType:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mPath:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil;->setDefaultImage(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 420
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 421
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 423
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 424
    .restart local v0    # "anim":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 425
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 427
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mFileType:I

    const/16 v2, 0x3ea

    if-ne v1, v2, :cond_4

    .line 428
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mOverlayIconImageView:Landroid/widget/ImageView;

    const v2, 0x7f0200c7

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 431
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListMyfilesAdapter$ThumbnailLoader$ThumbnailDisplayer;->mThumbInfo:Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/util/ThumbnailUtil$ThumbnailInfo;->mOverlayIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
