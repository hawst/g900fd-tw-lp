.class Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;
.super Landroid/database/DataSetObserver;
.source "SelectedItemListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 15

    .prologue
    .line 59
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 61
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->getChildCount()I

    move-result v11

    .line 62
    .local v11, "viewCount":I
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;
    invoke-static {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;->getCount()I

    move-result v8

    .line 64
    .local v8, "itemCount":I
    if-ne v11, v8, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    if-nez v8, :cond_2

    .line 69
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->removeAllViews()V

    goto :goto_0

    .line 73
    :cond_2
    if-le v11, v8, :cond_4

    move v3, v11

    .line 74
    .local v3, "count":I
    :goto_1
    if-le v11, v8, :cond_5

    const/4 v10, 0x1

    .line 76
    .local v10, "unselect":Z
    :goto_2
    if-eqz v10, :cond_8

    .line 77
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    if-ge v6, v3, :cond_3

    .line 79
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 81
    .local v2, "child":Landroid/view/View;
    if-ge v6, v8, :cond_6

    .line 82
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 83
    .local v9, "src":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;
    invoke-static {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 85
    .local v4, "dst":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    if-eqz v9, :cond_7

    invoke-virtual {v9, v4}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    .line 86
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->removeViewAt(I)V

    .line 131
    .end local v2    # "child":Landroid/view/View;
    .end local v4    # "dst":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    .end local v9    # "src":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_3
    :goto_4
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->getChildCount()I

    move-result v3

    .line 133
    if-lez v3, :cond_0

    .line 134
    const/4 v6, 0x0

    :goto_5
    if-ge v6, v3, :cond_0

    .line 135
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    iget-object v13, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v13, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->setChildMargin(IILandroid/view/View;)V
    invoke-static {v12, v6, v3, v13}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;IILandroid/view/View;)V

    .line 134
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .end local v3    # "count":I
    .end local v6    # "i":I
    .end local v10    # "unselect":Z
    :cond_4
    move v3, v8

    .line 73
    goto :goto_1

    .line 74
    .restart local v3    # "count":I
    :cond_5
    const/4 v10, 0x0

    goto :goto_2

    .line 90
    .restart local v2    # "child":Landroid/view/View;
    .restart local v6    # "i":I
    .restart local v10    # "unselect":Z
    :cond_6
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12, v6}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->removeViewAt(I)V

    .line 77
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 96
    .end local v2    # "child":Landroid/view/View;
    .end local v6    # "i":I
    :cond_8
    sub-int v7, v8, v11

    .line 98
    .local v7, "increasedCount":I
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_6
    if-ge v6, v7, :cond_9

    .line 99
    add-int v5, v11, v6

    .line 101
    .local v5, "dstPosition":I
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;
    invoke-static {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    move-result-object v12

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12, v5, v13, v14}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 102
    .restart local v2    # "child":Landroid/view/View;
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->mAdapter:Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;
    invoke-static {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;)Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;

    move-result-object v12

    invoke-virtual {v12, v5}, Lcom/samsung/android/app/galaxyfinder/ui/filter/data/SelectedItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v2, v12}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 104
    const v12, 0x7f0b002b

    invoke-virtual {v2, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 105
    .local v1, "button":Landroid/view/View;
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0e005f

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v1, v12}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v1, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12, v2, v5}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->addView(Landroid/view/View;I)V

    .line 109
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    invoke-virtual {v12}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->getContext()Landroid/content/Context;

    move-result-object v12

    const v13, 0x7f04000f

    invoke-static {v12, v13}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 110
    .local v0, "ani":Landroid/view/animation/Animation;
    invoke-virtual {v2, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 111
    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 98
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 115
    .end local v0    # "ani":Landroid/view/animation/Animation;
    .end local v1    # "button":Landroid/view/View;
    .end local v2    # "child":Landroid/view/View;
    .end local v5    # "dstPosition":I
    :cond_9
    iget-object v12, p0, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;

    new-instance v13, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1$1;

    invoke-direct {v13, p0}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView$1;)V

    invoke-virtual {v12, v13}, Lcom/samsung/android/app/galaxyfinder/ui/filter/SelectedItemListView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_4
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    .line 143
    return-void
.end method
