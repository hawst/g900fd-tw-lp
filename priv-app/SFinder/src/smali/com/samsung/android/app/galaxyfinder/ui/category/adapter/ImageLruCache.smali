.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;
.super Ljava/lang/Object;
.source "ImageLruCache.java"


# static fields
.field private static final CACHE_SIZE:I = 0x1e00000

.field static _this:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;


# instance fields
.field mImageCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/BitmapDrawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    invoke-direct {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;-><init>()V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->_this:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache$1;

    const/high16 v1, 0x1e00000

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;I)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    return-void
.end method

.method public static getInstance()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->_this:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    return-object v0
.end method


# virtual methods
.method public clearLruCache()V
    .locals 2

    .prologue
    .line 44
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->_this:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    iget-object v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    monitor-enter v1

    .line 45
    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->_this:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 46
    monitor-exit v1

    .line 47
    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBitmapDrawable(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 32
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->_this:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    iget-object v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    monitor-enter v1

    .line 33
    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->_this:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    monitor-exit v1

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public putBitmapDrawable(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bitmapDrawable"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->_this:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    iget-object v1, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    monitor-enter v1

    .line 39
    :try_start_0
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->_this:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->mImageCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    monitor-exit v1

    .line 41
    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
