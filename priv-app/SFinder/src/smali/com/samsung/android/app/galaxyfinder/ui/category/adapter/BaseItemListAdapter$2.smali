.class Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;
.super Ljava/lang/Object;
.source "BaseItemListAdapter.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLongPressEvent(Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;I)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iput p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 344
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->isEnabledMenuSelect()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isNormalMode()Z
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 345
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mListener:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

    if-eqz v2, :cond_0

    .line 346
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget-object v2, v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mListener:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

    invoke-interface {v2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;->onActionLongPressed(Landroid/view/View;)V

    .line 347
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->val$position:I

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setCheckedChange(Landroid/view/View;I)V
    invoke-static {v2, p1, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/view/View;I)V

    .line 358
    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 350
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDragNDropEnabled:Z
    invoke-static {v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 351
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->val$position:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionDataToShare()Ljava/lang/String;

    move-result-object v0

    .line 352
    .local v0, "dragUri":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->val$position:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionMimeType()Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, "mimetype":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->executeDragDrop(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, p1, v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
