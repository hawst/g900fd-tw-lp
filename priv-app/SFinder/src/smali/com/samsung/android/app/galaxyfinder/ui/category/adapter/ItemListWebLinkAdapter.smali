.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.source "ItemListWebLinkAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 28
    return-void
.end method

.method private findResourceId(Ljava/lang/String;)I
    .locals 2
    .param p1, "cpName"    # Ljava/lang/String;

    .prologue
    .line 89
    const v0, 0x7f0e00d7

    .line 91
    .local v0, "resId":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Google"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    const v0, 0x7f0e00d6

    .line 95
    :cond_0
    return v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v7

    .line 33
    .local v7, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    const/4 v4, 0x0

    .line 35
    .local v4, "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;
    move-object/from16 v0, p3

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->checkItemDispaly(ILandroid/view/ViewGroup;)V

    .line 37
    if-nez p2, :cond_3

    .line 38
    sget-object v10, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f03004e

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 40
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;

    .end local v4    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;
    invoke-direct {v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;-><init>()V

    .line 42
    .restart local v4    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;
    const v10, 0x7f0b004c

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;->tvName:Landroid/widget/TextView;

    .line 43
    const v10, 0x7f0b004b

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 45
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 50
    :goto_0
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v2

    .line 51
    .local v2, "cpName":Ljava/lang/String;
    const v10, 0x7f0b00a8

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 52
    .local v3, "cpNameTextView":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 53
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    :cond_0
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, "iconUri":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v6

    .line 57
    .local v6, "intent":Landroid/content/Intent;
    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getBaseQuery()Ljava/lang/String;

    move-result-object v8

    .line 59
    .local v8, "query":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<font color="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->mResources:Landroid/content/res/Resources;

    const v12, 0x7f09003b

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</font>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "coloredQuery":Ljava/lang/String;
    iget-object v10, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->mResources:Landroid/content/res/Resources;

    invoke-direct {p0, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->findResourceId(Ljava/lang/String;)I

    move-result v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v1, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 63
    .local v9, "titleSrc":Ljava/lang/String;
    iget-object v10, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;->tvName:Landroid/widget/TextView;

    invoke-static {v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v11

    sget-object v12, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v10, v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 64
    iget-object v11, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;->tvName:Landroid/widget/TextView;

    iget-boolean v10, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->mIsTablet:Z

    if-eqz v10, :cond_4

    const/4 v10, 0x1

    :goto_1
    invoke-static {v10}, Lcom/samsung/android/app/galaxyfinder/util/FontLoader;->getFontTypeface(I)Landroid/graphics/Typeface;

    move-result-object v10

    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 68
    iget-object v10, v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;->ivIcon:Landroid/widget/ImageView;

    const/4 v11, 0x0

    const/4 v12, -0x1

    invoke-virtual {p0, v10, v5, v11, v12}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 70
    if-eqz v6, :cond_2

    .line 71
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 73
    const-string v10, "Google"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 74
    iget-object v10, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isQuickSearchAvailable(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 75
    const-string v10, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-string v10, "query"

    invoke-virtual {v6, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    new-instance v10, Landroid/content/ComponentName;

    const-string v11, "com.google.android.googlequicksearchbox"

    const-string v12, "com.google.android.search.core.google.GoogleSearch"

    invoke-direct {v10, v11, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 78
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 82
    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {p0, v0, v6, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter;->setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z

    .line 85
    :cond_2
    invoke-super/range {p0 .. p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    return-object v10

    .line 47
    .end local v1    # "coloredQuery":Ljava/lang/String;
    .end local v2    # "cpName":Ljava/lang/String;
    .end local v3    # "cpNameTextView":Landroid/widget/TextView;
    .end local v5    # "iconUri":Ljava/lang/String;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v8    # "query":Ljava/lang/String;
    .end local v9    # "titleSrc":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;
    check-cast v4, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;

    .restart local v4    # "holder":Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListWebLinkAdapter$WebLinkItemListViewHolder;
    goto/16 :goto_0

    .line 64
    .restart local v1    # "coloredQuery":Ljava/lang/String;
    .restart local v2    # "cpName":Ljava/lang/String;
    .restart local v3    # "cpNameTextView":Landroid/widget/TextView;
    .restart local v5    # "iconUri":Ljava/lang/String;
    .restart local v6    # "intent":Landroid/content/Intent;
    .restart local v8    # "query":Ljava/lang/String;
    .restart local v9    # "titleSrc":Ljava/lang/String;
    :cond_4
    const/4 v10, 0x3

    goto :goto_1
.end method
