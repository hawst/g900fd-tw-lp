.class Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$5;
.super Ljava/lang/Object;
.source "GalaxyFinderActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$5;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 240
    const-string v0, "setting_use_search_history"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$5;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    const/4 v1, 0x1

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mEnabledSavingHistory:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$502(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;Z)Z

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity$5;->this$0:Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;

    # getter for: Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->mEnabledSavingHistory:Z
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->access$500(Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;)Z

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryListController;->setHistoryState(Z)V

    .line 245
    return-void
.end method
