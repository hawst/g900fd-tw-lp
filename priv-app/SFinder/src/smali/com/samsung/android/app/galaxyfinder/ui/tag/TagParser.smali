.class public Lcom/samsung/android/app/galaxyfinder/ui/tag/TagParser;
.super Ljava/lang/Object;
.source "TagParser.java"


# static fields
.field public static final TAG:Ljava/lang/String;

.field public static final TAG_KEY_CATEGORY:Ljava/lang/String; = "category"

.field public static final TAG_KEY_ENCODING:Ljava/lang/String; = "encoding"

.field public static final TAG_KEY_ID:Ljava/lang/String; = "id"

.field public static final TAG_KEY_TAG:Ljava/lang/String; = "tag"

.field public static final TAG_KEY_TAGS:Ljava/lang/String; = "tags"

.field public static final TAG_KEY_VALUE:Ljava/lang/String; = "value"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Ljava/util/List;
    .locals 21
    .param p0, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    const/4 v15, 0x0

    .line 32
    .local v15, "mTagList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const/4 v10, 0x0

    .line 34
    .local v10, "jArray":Lorg/json/JSONArray;
    if-eqz p0, :cond_3

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_3

    .line 37
    :try_start_0
    new-instance v11, Lorg/json/JSONArray;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .end local v10    # "jArray":Lorg/json/JSONArray;
    .local v11, "jArray":Lorg/json/JSONArray;
    :try_start_1
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 40
    .end local v15    # "mTagList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .local v16, "mTagList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :try_start_2
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 42
    .local v4, "count":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v4, :cond_2

    .line 43
    invoke-virtual {v11, v7}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/json/JSONObject;

    .line 44
    .local v12, "jobject":Lorg/json/JSONObject;
    const-string v19, "tags"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    .line 45
    .local v14, "jtags":Lorg/json/JSONArray;
    const-string v19, "id"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 47
    .local v8, "id":Ljava/lang/String;
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v17

    .line 49
    .local v17, "tagcount":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    move/from16 v0, v17

    if-ge v9, v0, :cond_1

    .line 50
    invoke-virtual {v14, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v19

    const-string v20, "tag"

    invoke-virtual/range {v19 .. v20}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 52
    .local v13, "jtagobject":Lorg/json/JSONObject;
    const-string v19, "category"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 53
    .local v3, "category":Ljava/lang/String;
    const-string v19, "encoding"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 54
    .local v6, "encoding":Z
    const-string v19, "value"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 56
    .local v18, "value":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v19

    if-lez v19, :cond_0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->charAt(I)C

    move-result v19

    const/16 v20, 0x5b

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    const-string v19, "]"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 57
    const-string v19, "["

    const-string v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    .line 58
    const-string v19, "]"

    const-string v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    .line 61
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v19, "id"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v19, "category"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v19, "encoding"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 66
    const-string v19, "value"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    sget-object v19, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagParser;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 49
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 42
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "category":Ljava/lang/String;
    .end local v6    # "encoding":Z
    .end local v13    # "jtagobject":Lorg/json/JSONObject;
    .end local v18    # "value":Ljava/lang/String;
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .end local v8    # "id":Ljava/lang/String;
    .end local v9    # "j":I
    .end local v12    # "jobject":Lorg/json/JSONObject;
    .end local v14    # "jtags":Lorg/json/JSONArray;
    .end local v17    # "tagcount":I
    :cond_2
    move-object v10, v11

    .end local v11    # "jArray":Lorg/json/JSONArray;
    .restart local v10    # "jArray":Lorg/json/JSONArray;
    move-object/from16 v15, v16

    .line 78
    .end local v4    # "count":I
    .end local v7    # "i":I
    .end local v16    # "mTagList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .restart local v15    # "mTagList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :cond_3
    :goto_2
    return-object v15

    .line 73
    :catch_0
    move-exception v5

    .line 74
    .local v5, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v5}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 73
    .end local v5    # "e":Lorg/json/JSONException;
    .end local v10    # "jArray":Lorg/json/JSONArray;
    .restart local v11    # "jArray":Lorg/json/JSONArray;
    :catch_1
    move-exception v5

    move-object v10, v11

    .end local v11    # "jArray":Lorg/json/JSONArray;
    .restart local v10    # "jArray":Lorg/json/JSONArray;
    goto :goto_3

    .end local v10    # "jArray":Lorg/json/JSONArray;
    .end local v15    # "mTagList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .restart local v11    # "jArray":Lorg/json/JSONArray;
    .restart local v16    # "mTagList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :catch_2
    move-exception v5

    move-object v10, v11

    .end local v11    # "jArray":Lorg/json/JSONArray;
    .restart local v10    # "jArray":Lorg/json/JSONArray;
    move-object/from16 v15, v16

    .end local v16    # "mTagList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .restart local v15    # "mTagList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    goto :goto_3
.end method
