.class Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;
.super Ljava/lang/Object;
.source "HistoryCursorAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

.field final synthetic val$keyword:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->val$keyword:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 40
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 112
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mIsDoingAnim:Z
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$000(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    const/16 v20, 0x0

    .line 117
    .local v20, "dismissPosition":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    .line 118
    .local v18, "dismiss":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ListView;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v24

    .line 119
    .local v24, "lastChildView":Landroid/view/View;
    move-object/from16 v9, p1

    .line 121
    .local v9, "delbtn":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$200(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Landroid/view/WindowManager;

    .line 123
    .local v34, "wm":Landroid/view/WindowManager;
    invoke-interface/range {v34 .. v34}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v21

    .line 124
    .local v21, "display":Landroid/view/Display;
    new-instance v33, Landroid/graphics/Point;

    invoke-direct/range {v33 .. v33}, Landroid/graphics/Point;-><init>()V

    .line 125
    .local v33, "size":Landroid/graphics/Point;
    move-object/from16 v0, v21

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 127
    const/4 v4, 0x2

    new-array v0, v4, [I

    move-object/from16 v22, v0

    .line 128
    .local v22, "dtnViewCoords":[I
    const/4 v4, 0x2

    new-array v0, v4, [I

    move-object/from16 v27, v0

    .line 129
    .local v27, "listViewCoords":[I
    const/4 v4, 0x2

    new-array v0, v4, [I

    move-object/from16 v25, v0

    .line 131
    .local v25, "lastChildViewCoods":[I
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 132
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->getLocationOnScreen([I)V

    .line 133
    invoke-virtual/range {v24 .. v25}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 135
    const/4 v4, 0x0

    aget v4, v22, v4

    const/4 v5, 0x0

    aget v5, v27, v5

    sub-int v35, v4, v5

    .line 136
    .local v35, "x":I
    const/4 v4, 0x1

    aget v4, v22, v4

    const/4 v5, 0x1

    aget v5, v27, v5

    sub-int v36, v4, v5

    .line 139
    .local v36, "y":I
    new-instance v32, Landroid/graphics/Rect;

    invoke-direct/range {v32 .. v32}, Landroid/graphics/Rect;-><init>()V

    .line 140
    .local v32, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getChildCount()I

    move-result v16

    .line 143
    .local v16, "childCount":I
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_1
    move/from16 v0, v23

    move/from16 v1, v16

    if-ge v0, v1, :cond_2

    .line 144
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    .line 145
    .local v15, "child":Landroid/view/View;
    move-object/from16 v0, v32

    invoke-virtual {v15, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 146
    move-object/from16 v0, v32

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 147
    move-object/from16 v18, v15

    .line 148
    move/from16 v20, v23

    .line 153
    .end local v15    # "child":Landroid/view/View;
    :cond_2
    move-object/from16 v6, v18

    .line 154
    .local v6, "dismissView":Landroid/view/View;
    move/from16 v19, v20

    .line 155
    .local v19, "dismissPos":I
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    .line 156
    .local v7, "dismissViewLp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v8

    .line 157
    .local v8, "originalHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v29

    .line 158
    .local v29, "listViewLp":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getHeight()I

    move-result v30

    .line 160
    .local v30, "listViewOriginalHeight":I
    move-object/from16 v0, v33

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$300(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v37, 0x7f0a0436

    move/from16 v0, v37

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int/2addr v4, v5

    const/4 v5, 0x1

    aget v5, v27, v5

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v37, 0x7f0a043b

    move/from16 v0, v37

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int v28, v4, v5

    .line 167
    .local v28, "listViewHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v17

    .line 170
    .local v17, "childViewheight":I
    sub-int v26, v30, v8

    .line 172
    .local v26, "listViewAfterHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_6

    .line 173
    const/4 v4, 0x1

    aget v4, v27, v4

    add-int v4, v4, v30

    const/4 v5, 0x1

    aget v5, v25, v5

    sub-int/2addr v4, v5

    sub-int v26, v26, v4

    .line 182
    :cond_3
    :goto_2
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v8, v4, v5

    const/4 v5, 0x1

    const/16 v37, 0x0

    aput v37, v4, v5

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v4

    const-wide/16 v38, 0x1f4

    move-wide/from16 v0, v38

    invoke-virtual {v4, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v13

    .line 184
    .local v13, "animator":Landroid/animation/ValueAnimator;
    const-wide/16 v4, 0x154

    invoke-virtual {v13, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 186
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v30, v4, v5

    const/4 v5, 0x1

    aput v26, v4, v5

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v4

    const-wide/16 v38, 0x1f4

    move-wide/from16 v0, v38

    invoke-virtual {v4, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v14

    .line 188
    .local v14, "animator1":Landroid/animation/ValueAnimator;
    const-wide/16 v4, 0x154

    invoke-virtual {v14, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 190
    const-string v4, "alpha"

    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-static {v6, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v11

    .line 191
    .local v11, "alpha":Landroid/animation/ObjectAnimator;
    const-wide/16 v4, 0x10e

    invoke-virtual {v11, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 193
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$1;

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v4, v0, v9, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;Landroid/view/View;I)V

    invoke-virtual {v11, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 203
    const/16 v31, 0x0

    .line 205
    .local v31, "prevDivider":Landroid/widget/ImageView;
    add-int/lit8 v4, v19, -0x1

    if-ltz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v19

    if-ne v0, v4, :cond_4

    .line 206
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$100(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/widget/ListView;

    move-result-object v4

    add-int/lit8 v5, v19, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0b0041

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    .end local v31    # "prevDivider":Landroid/widget/ImageView;
    check-cast v31, Landroid/widget/ImageView;

    .line 208
    .restart local v31    # "prevDivider":Landroid/widget/ImageView;
    const-string v4, "alpha"

    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_1

    move-object/from16 v0, v31

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 210
    .local v12, "alpha2":Landroid/animation/ObjectAnimator;
    const-wide/16 v4, 0x10e

    invoke-virtual {v12, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 211
    invoke-virtual {v12}, Landroid/animation/ObjectAnimator;->start()V

    .line 214
    .end local v12    # "alpha2":Landroid/animation/ObjectAnimator;
    :cond_4
    move-object/from16 v10, v31

    .line 216
    .local v10, "prevDiv":Landroid/widget/ImageView;
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v10}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;ILandroid/view/View;Landroid/widget/ImageView;)V

    invoke-virtual {v13, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 239
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v7, v6}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$3;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;Landroid/view/ViewGroup$LayoutParams;Landroid/view/View;)V

    invoke-virtual {v13, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 247
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$4;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$4;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;)V

    invoke-virtual {v14, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 256
    new-instance v4, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$5;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v4, v0, v1}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1$5;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v14, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 264
    invoke-virtual {v11}, Landroid/animation/ObjectAnimator;->start()V

    .line 265
    invoke-virtual {v13}, Landroid/animation/ValueAnimator;->start()V

    .line 267
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v4

    const/16 v5, 0xa

    if-lt v4, v5, :cond_7

    .line 268
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    mul-int v4, v4, v17

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    # getter for: Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->access$600(Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v37, 0x7f0a042e

    move/from16 v0, v37

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    add-int/2addr v4, v5

    move/from16 v0, v28

    if-ge v4, v0, :cond_0

    .line 272
    invoke-virtual {v14}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_0

    .line 143
    .end local v6    # "dismissView":Landroid/view/View;
    .end local v7    # "dismissViewLp":Landroid/view/ViewGroup$LayoutParams;
    .end local v8    # "originalHeight":I
    .end local v10    # "prevDiv":Landroid/widget/ImageView;
    .end local v11    # "alpha":Landroid/animation/ObjectAnimator;
    .end local v13    # "animator":Landroid/animation/ValueAnimator;
    .end local v14    # "animator1":Landroid/animation/ValueAnimator;
    .end local v17    # "childViewheight":I
    .end local v19    # "dismissPos":I
    .end local v26    # "listViewAfterHeight":I
    .end local v28    # "listViewHeight":I
    .end local v29    # "listViewLp":Landroid/view/ViewGroup$LayoutParams;
    .end local v30    # "listViewOriginalHeight":I
    .end local v31    # "prevDivider":Landroid/widget/ImageView;
    .restart local v15    # "child":Landroid/view/View;
    :cond_5
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_1

    .line 175
    .end local v15    # "child":Landroid/view/View;
    .restart local v6    # "dismissView":Landroid/view/View;
    .restart local v7    # "dismissViewLp":Landroid/view/ViewGroup$LayoutParams;
    .restart local v8    # "originalHeight":I
    .restart local v17    # "childViewheight":I
    .restart local v19    # "dismissPos":I
    .restart local v26    # "listViewAfterHeight":I
    .restart local v28    # "listViewHeight":I
    .restart local v29    # "listViewLp":Landroid/view/ViewGroup$LayoutParams;
    .restart local v30    # "listViewOriginalHeight":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v4

    const/16 v5, 0xa

    if-ge v4, v5, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v4

    mul-int v4, v4, v17

    move/from16 v0, v28

    if-le v4, v0, :cond_3

    .line 177
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v4

    mul-int v4, v4, v17

    sub-int v4, v4, v28

    add-int v26, v26, v4

    goto/16 :goto_2

    .line 275
    .restart local v10    # "prevDiv":Landroid/widget/ImageView;
    .restart local v11    # "alpha":Landroid/animation/ObjectAnimator;
    .restart local v13    # "animator":Landroid/animation/ValueAnimator;
    .restart local v14    # "animator1":Landroid/animation/ValueAnimator;
    .restart local v31    # "prevDivider":Landroid/widget/ImageView;
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter$1;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/ui/history/HistoryCursorAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    mul-int v4, v4, v17

    move/from16 v0, v28

    if-ge v4, v0, :cond_0

    .line 276
    invoke-virtual {v14}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_0

    .line 190
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 208
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method
