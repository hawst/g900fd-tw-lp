.class public Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
.super Ljava/lang/Object;
.source "SuggestionItemInfo.java"


# instance fields
.field private mBaseQuery:Ljava/lang/String;

.field private mChecked:Z

.field private mIndicationIndexOfFirstItem:I

.field private mIndicationOfFirstItem:Ljava/lang/String;

.field private mItemTagMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation
.end field

.field private mSecretMode:Z

.field private mSuggestionActionIntent:Landroid/content/Intent;

.field private mSuggestionContentHighlight:Ljava/lang/String;

.field private mSuggestionDataToShare:Ljava/lang/String;

.field private mSuggestionExtraFlags:Ljava/lang/String;

.field private mSuggestionGroup:Ljava/lang/String;

.field private mSuggestionIcon1:Ljava/lang/String;

.field private mSuggestionIcon2:Ljava/lang/String;

.field private mSuggestionIcon3:Ljava/lang/String;

.field private mSuggestionIcon4:Ljava/lang/String;

.field private mSuggestionIconBlob:[B

.field private mSuggestionIconOrientation:I

.field private mSuggestionInkData:Ljava/lang/String;

.field private mSuggestionIntentAction:Ljava/lang/String;

.field private mSuggestionIntentData:Ljava/lang/String;

.field private mSuggestionIntentDataId:Ljava/lang/String;

.field private mSuggestionIntentExtraData:Ljava/lang/String;

.field private mSuggestionMimeType:Ljava/lang/String;

.field private mSuggestionTargetType:Ljava/lang/String;

.field private mSuggestionText1:Ljava/lang/String;

.field private mSuggestionText2:Ljava/lang/String;

.field private mSuggestionText3:Ljava/lang/String;

.field private mSuggestionText4:Ljava/lang/String;

.field private mSuggestionText5:Ljava/lang/String;

.field private mSuggestionUri:Ljava/lang/String;

.field private mValidity:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionContentHighlight:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionInkData:Ljava/lang/String;

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mValidity:Z

    .line 72
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    .line 74
    iput-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mChecked:Z

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mIndicationOfFirstItem:Ljava/lang/String;

    .line 78
    iput v2, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mIndicationIndexOfFirstItem:I

    .line 82
    return-void
.end method


# virtual methods
.method public addTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "textTagData"    # Ljava/lang/String;
    .param p2, "tagType"    # Ljava/lang/String;
    .param p3, "tagRawData"    # Ljava/lang/String;

    .prologue
    .line 483
    if-eqz p1, :cond_1

    .line 484
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    if-nez v2, :cond_0

    .line 485
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    .line 488
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 489
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 490
    .local v1, "storedTag":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    invoke-virtual {v1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->accumulateTagType(Ljava/lang/String;)V

    .line 496
    .end local v1    # "storedTag":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :cond_1
    :goto_0
    return-void

    .line 492
    :cond_2
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    invoke-direct {v0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    .local v0, "data":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTextTagData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public doesHaveTag(Ljava/lang/String;)Z
    .locals 2
    .param p1, "selectTag"    # Ljava/lang/String;

    .prologue
    .line 471
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 472
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;

    .line 473
    .local v0, "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    if-eqz v0, :cond_0

    .line 474
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;->getTagCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 475
    const/4 v1, 0x1

    .line 479
    .end local v0    # "tagData":Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAllTags()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/galaxyfinder/ui/tag/TagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 507
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    .line 510
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBaseQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mBaseQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getIndicationIndex()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mIndicationIndexOfFirstItem:I

    return v0
.end method

.method public getIndicationOfFirstItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mIndicationOfFirstItem:Ljava/lang/String;

    return-object v0
.end method

.method public getSecretMode()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSecretMode:Z

    return v0
.end method

.method public getSuggestionActionIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionActionIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getSuggestionContentHighlight()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionContentHighlight:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionDataToShare()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionDataToShare:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionExtraFlags()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionExtraFlags:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionGroup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionGroup:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIcon1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIcon1:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIcon2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIcon2:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIcon3()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIcon3:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIcon4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIcon4:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIconBlob()[B
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIconBlob:[B

    return-object v0
.end method

.method public getSuggestionIconOrientation()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIconOrientation:I

    return v0
.end method

.method public getSuggestionInkData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionInkData:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIntentAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIntentAction:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIntentData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIntentData:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIntentDataId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIntentDataId:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIntentExtraData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIntentExtraData:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionTargetType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionTargetType:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionText1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText1:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionText2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText2:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionText3()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText3:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionText4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText4:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionText5()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText5:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionUri:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mChecked:Z

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 323
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mValidity:Z

    return v0
.end method

.method public removeTag(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mItemTagMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    :cond_0
    return-void
.end method

.method public setBaseQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mBaseQuery:Ljava/lang/String;

    .line 206
    return-void
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 311
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mChecked:Z

    .line 312
    return-void
.end method

.method public setIndicationIndex(I)V
    .locals 0
    .param p1, "indicationIndex"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mIndicationIndexOfFirstItem:I

    .line 98
    return-void
.end method

.method public setIndicationOfFirstItem(Ljava/lang/String;)V
    .locals 0
    .param p1, "indicationString"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mIndicationOfFirstItem:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setSecretmode(Z)V
    .locals 0
    .param p1, "secret"    # Z

    .prologue
    .line 209
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSecretMode:Z

    .line 210
    return-void
.end method

.method public setSuggestionActionIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionActionIntent:Landroid/content/Intent;

    .line 270
    return-void
.end method

.method public setSuggestionContentHighLight(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 307
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionContentHighlight:Ljava/lang/String;

    .line 308
    return-void
.end method

.method public setSuggestionDataToShare(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionDataToShare:Ljava/lang/String;

    .line 278
    return-void
.end method

.method public setSuggestionExtraFlags(Ljava/lang/String;)V
    .locals 0
    .param p1, "extraflags"    # Ljava/lang/String;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionExtraFlags:Ljava/lang/String;

    .line 298
    return-void
.end method

.method public setSuggestionGroup(Ljava/lang/String;)V
    .locals 0
    .param p1, "group"    # Ljava/lang/String;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionGroup:Ljava/lang/String;

    .line 294
    return-void
.end method

.method public setSuggestionIcon1(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIcon1:Ljava/lang/String;

    .line 234
    return-void
.end method

.method public setSuggestionIcon2(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIcon2:Ljava/lang/String;

    .line 238
    return-void
.end method

.method public setSuggestionIcon3(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIcon3:Ljava/lang/String;

    .line 242
    return-void
.end method

.method public setSuggestionIcon4(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 245
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIcon4:Ljava/lang/String;

    .line 246
    return-void
.end method

.method public setSuggestionIconBlob([B)V
    .locals 0
    .param p1, "blob"    # [B

    .prologue
    .line 289
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIconBlob:[B

    .line 290
    return-void
.end method

.method public setSuggestionIconOrientation(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 249
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIconOrientation:I

    .line 250
    return-void
.end method

.method public setSuggestionInkData(Ljava/lang/String;)V
    .locals 0
    .param p1, "inkData"    # Ljava/lang/String;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionInkData:Ljava/lang/String;

    .line 304
    return-void
.end method

.method public setSuggestionIntentAction(Ljava/lang/String;)V
    .locals 0
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIntentAction:Ljava/lang/String;

    .line 254
    return-void
.end method

.method public setSuggestionIntentData(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIntentData:Ljava/lang/String;

    .line 258
    return-void
.end method

.method public setSuggestionIntentDataId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIntentDataId:Ljava/lang/String;

    .line 262
    return-void
.end method

.method public setSuggestionIntentExtraData(Ljava/lang/String;)V
    .locals 0
    .param p1, "extra"    # Ljava/lang/String;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionIntentExtraData:Ljava/lang/String;

    .line 266
    return-void
.end method

.method public setSuggestionMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mimetype"    # Ljava/lang/String;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionMimeType:Ljava/lang/String;

    .line 282
    return-void
.end method

.method public setSuggestionTargetType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 285
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionTargetType:Ljava/lang/String;

    .line 286
    return-void
.end method

.method public setSuggestionText1(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText1:Ljava/lang/String;

    .line 214
    return-void
.end method

.method public setSuggestionText2(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText2:Ljava/lang/String;

    .line 218
    return-void
.end method

.method public setSuggestionText3(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText3:Ljava/lang/String;

    .line 222
    return-void
.end method

.method public setSuggestionText4(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText4:Ljava/lang/String;

    .line 226
    return-void
.end method

.method public setSuggestionText5(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionText5:Ljava/lang/String;

    .line 230
    return-void
.end method

.method public setSuggestionUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 273
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mSuggestionUri:Ljava/lang/String;

    .line 274
    return-void
.end method

.method public setValidity(Z)V
    .locals 0
    .param p1, "valid"    # Z

    .prologue
    .line 315
    iput-boolean p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->mValidity:Z

    .line 316
    return-void
.end method

.method public toContentValues()Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 390
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 392
    .local v0, "cv":Landroid/content/ContentValues;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 393
    const-string v1, "suggest_text_1"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 397
    const-string v1, "suggest_text_2"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 401
    const-string v1, "suggest_text_3"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText4()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 405
    const-string v1, "suggest_text_4"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText4()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 409
    const-string v1, "suggest_icon_1"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 413
    const-string v1, "suggest_icon_2"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon3()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 417
    const-string v1, "suggest_icon_3"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon3()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon4()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 421
    const-string v1, "suggest_icon_4"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon4()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 427
    const-string v1, "suggest_intent_action"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    const-string v1, "suggest_intent_data"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIntentData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const-string v1, "suggest_intent_data_id"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIntentDataId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string v1, "suggest_intent_extra_data"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIntentExtraData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 434
    const-string v1, "suggest_uri"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionMimeType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 438
    const-string v1, "suggest_mime_type"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 443
    const-string v1, "suggest_target_type"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIconBlob()[B

    move-result-object v1

    if-eqz v1, :cond_c

    .line 447
    const-string v1, "suggest_icon_1_blob"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIconBlob()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 450
    :cond_c
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 451
    const-string v1, "suggest_group"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    :cond_d
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 455
    const-string v1, "suggest_extra_flags"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionExtraFlags()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_e
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionInkData()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 460
    const-string v1, "suggest_ink_data"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionInkData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    :cond_f
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionContentHighlight()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 464
    const-string v1, "highlight_content"

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionContentHighlight()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_10
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 330
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 331
    const-string v1, "TEXT1 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 334
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 335
    const-string v1, "TEXT2 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 338
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 339
    const-string v1, "TEXT3 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText3()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 342
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText4()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 343
    const-string v1, "TEXT4 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionText4()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 346
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 347
    const-string v1, "ICON1 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 350
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 351
    const-string v1, "ICON2 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 354
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon3()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 355
    const-string v1, "ICON3 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon3()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 358
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon4()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 359
    const-string v1, "ICON4 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIcon4()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 362
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 363
    const-string v1, "INTENT : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionActionIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 366
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 367
    const-string v1, "URI : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 370
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionMimeType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 371
    const-string v1, "MIMETYPE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 374
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 375
    const-string v1, "TYPE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionTargetType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 378
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionIconBlob()[B

    move-result-object v1

    if-eqz v1, :cond_c

    .line 379
    const-string v1, "BLOB : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "BLOB column has value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 382
    :cond_c
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 383
    const-string v1, "GROUP : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getSuggestionGroup()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 386
    :cond_d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
