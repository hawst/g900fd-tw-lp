.class Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;
.super Landroid/os/Handler;
.source "SearchController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/search/SearchController;->ensureHandler()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 137
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 139
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "MSG_SEARCH_REQUEST"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    .line 142
    .local v1, "qaction":Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    if-eqz v1, :cond_1

    .line 143
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getFrom()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->getAvailableEngines(Ljava/lang/String;)Ljava/util/List;
    invoke-static {v4, v5}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$100(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4, v1}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;-><init>(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Ljava/util/List;Lcom/samsung/android/app/galaxyfinder/search/QueryAction;)V

    .line 146
    .local v2, "r":Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->DELAY_TIME_FOR_QUERY:I
    invoke-static {v4}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$300(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "action id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "remained action : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v5}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$400(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 157
    .end local v2    # "r":Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "qaction has null value"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 163
    .end local v1    # "qaction":Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "MSG_SEARCH_CANCEL"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->removeMessages(I)V

    .line 166
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->removeMessages(I)V

    .line 168
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->bStopTagRetrieve:Z
    invoke-static {v3, v4}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$502(Lcom/samsung/android/app/galaxyfinder/search/SearchController;Z)Z

    .line 170
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->cancelTagLoaderThread()V
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$600(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)V

    .line 172
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 173
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    .line 175
    .restart local v2    # "r":Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;->cancel()Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    move-result-object v1

    .line 177
    .restart local v1    # "qaction":Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$400(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 178
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mActionTable:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$400(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "action canceled "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    iget-object v3, v3, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mQueryHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 187
    .end local v1    # "qaction":Lcom/samsung/android/app/galaxyfinder/search/QueryAction;
    .end local v2    # "r":Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto/16 :goto_0

    .line 192
    .end local v0    # "i$":Ljava/util/Iterator;
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "MSG_SEARCH_COMPLETED"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->getCurrentRunnable()Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$700(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;

    move-result-object v2

    .line 196
    .restart local v2    # "r":Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 197
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 202
    .end local v2    # "r":Lcom/samsung/android/app/galaxyfinder/search/SearchController$QueryRunnable;
    :pswitch_3
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$000(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "MSG_SEARCH_TAG_RETRIEVED"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mRunnableList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$200(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 204
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$800(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 205
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/SearchController$1;->this$0:Lcom/samsung/android/app/galaxyfinder/search/SearchController;

    # getter for: Lcom/samsung/android/app/galaxyfinder/search/SearchController;->mListener:Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;
    invoke-static {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchController;->access$800(Lcom/samsung/android/app/galaxyfinder/search/SearchController;)Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/app/galaxyfinder/search/SearchStateCallback;->onSearchTagRetrieved()V

    goto/16 :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
