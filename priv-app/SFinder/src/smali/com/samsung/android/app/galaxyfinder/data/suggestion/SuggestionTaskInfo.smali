.class public Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;
.super Ljava/lang/Object;
.source "SuggestionTaskInfo.java"


# instance fields
.field private mData:Ljava/lang/String;

.field private mType:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "data"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;->mType:I

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;->mData:Ljava/lang/String;

    .line 13
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;->mType:I

    .line 14
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;->mData:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;->mData:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionTaskInfo;->mType:I

    return v0
.end method
