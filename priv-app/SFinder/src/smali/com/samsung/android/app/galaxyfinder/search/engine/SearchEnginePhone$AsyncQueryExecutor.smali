.class public Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;
.super Landroid/os/AsyncTask;
.source "SearchEnginePhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AsyncQueryExecutor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mDisplayOrder:I

.field private mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

.field private mObject:Ljava/lang/Object;

.field private mQuery:Ljava/lang/String;

.field private mSelection:Ljava/lang/String;

.field private mSelectionArgs:[Ljava/lang/String;

.field private mUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Landroid/content/ContentResolver;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/Object;Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;I)V
    .locals 1
    .param p2, "cr"    # Landroid/content/ContentResolver;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "uri"    # Landroid/net/Uri;
    .param p5, "selection"    # Ljava/lang/String;
    .param p6, "selectionArgs"    # [Ljava/lang/String;
    .param p7, "obj"    # Ljava/lang/Object;
    .param p8, "l"    # Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;
    .param p9, "order"    # I

    .prologue
    const/4 v0, 0x0

    .line 644
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 627
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mContentResolver:Landroid/content/ContentResolver;

    .line 629
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    .line 631
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mUri:Landroid/net/Uri;

    .line 633
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mQuery:Ljava/lang/String;

    .line 635
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mObject:Ljava/lang/Object;

    .line 637
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mSelection:Ljava/lang/String;

    .line 639
    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mSelectionArgs:[Ljava/lang/String;

    .line 641
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mDisplayOrder:I

    .line 645
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mContentResolver:Landroid/content/ContentResolver;

    .line 646
    iput-object p4, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mUri:Landroid/net/Uri;

    .line 647
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mQuery:Ljava/lang/String;

    .line 648
    iput-object p5, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mSelection:Ljava/lang/String;

    .line 649
    iput-object p6, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mSelectionArgs:[Ljava/lang/String;

    .line 650
    iput-object p7, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mObject:Ljava/lang/Object;

    .line 651
    iput-object p8, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    .line 652
    iput p9, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mDisplayOrder:I

    .line 653
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "arg0"    # [Ljava/lang/Object;

    .prologue
    .line 657
    const/4 v7, 0x0

    .line 659
    .local v7, "category":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_1

    .line 660
    const/4 v8, 0x0

    .line 663
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v10, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mObject:Ljava/lang/Object;

    check-cast v10, Landroid/app/SearchableInfo;

    .line 664
    .local v10, "searchable":Landroid/app/SearchableInfo;
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    invoke-virtual {v10}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->getAppLog(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->access$100(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 666
    .local v6, "APP_TAG":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mUri:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mSelection:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mSelectionArgs:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 667
    if-eqz v8, :cond_2

    .line 668
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SearchEngine"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doInBackground() search result size ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mQuery:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->makeCategory(Ljava/lang/String;Landroid/app/SearchableInfo;Landroid/database/Cursor;)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;
    invoke-static {v0, v1, v10, v8}, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->access$200(Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;Ljava/lang/String;Landroid/app/SearchableInfo;Landroid/database/Cursor;)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    move-result-object v7

    .line 674
    if-nez v7, :cond_3

    .line 675
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SearchEngine"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "doInBackground() returns null cursor"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    :goto_1
    if-eqz v7, :cond_0

    .line 681
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v0, v0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->getResultCount()I

    move-result v1

    invoke-virtual {v7}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/galaxyfinder/search/QueryAction;->setResultCount(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 687
    :cond_0
    if-eqz v8, :cond_1

    .line 688
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 693
    .end local v6    # "APP_TAG":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "searchable":Landroid/app/SearchableInfo;
    :cond_1
    :goto_2
    return-object v7

    .line 670
    .restart local v6    # "APP_TAG":Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "searchable":Landroid/app/SearchableInfo;
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SearchEngine"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "doInBackground() search result cursor has null value"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 684
    .end local v6    # "APP_TAG":Ljava/lang/String;
    .end local v10    # "searchable":Landroid/app/SearchableInfo;
    :catch_0
    move-exception v9

    .line 685
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v0, "SearchEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doInBackground() query exception (uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 687
    if-eqz v8, :cond_1

    .line 688
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 677
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v6    # "APP_TAG":Ljava/lang/String;
    .restart local v10    # "searchable":Landroid/app/SearchableInfo;
    :cond_3
    :try_start_3
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mDisplayOrder:I

    invoke-virtual {v7, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->setOrder(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 687
    .end local v6    # "APP_TAG":Ljava/lang/String;
    .end local v10    # "searchable":Landroid/app/SearchableInfo;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 688
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 698
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->mListener:Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone$AsyncQueryExecutor;->this$0:Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;

    iget-object v1, v1, Lcom/samsung/android/app/galaxyfinder/search/engine/SearchEnginePhone;->mQueryAction:Lcom/samsung/android/app/galaxyfinder/search/QueryAction;

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/app/galaxyfinder/search/engine/AbstractSearchEngine$ISearchEngineStateListener;->onReceiveSearchResponse(Lcom/samsung/android/app/galaxyfinder/search/QueryAction;Ljava/lang/Object;)V

    .line 701
    :cond_0
    return-void
.end method
