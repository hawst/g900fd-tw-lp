.class public Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
.super Landroid/widget/BaseAdapter;
.source "BaseItemListAdapter.java"

# interfaces
.implements Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView$OnMultiSelectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;,
        Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;
    }
.end annotation


# static fields
.field public static final EXPAND_MODE_ALL_CLOSED:I = 0x2

.field public static final EXPAND_MODE_FOLDED:I = 0x0

.field public static final EXPAND_MODE_PARTIALLY_EXPANDED:I = 0x1

.field protected static final INVALID_VALUE:I = -0x1

.field public static final ITEM_TYPE_1:I = 0x0

.field public static final ITEM_TYPE_1_1:I = 0x1

.field public static final ITEM_TYPE_2:I = 0x2

.field public static final ITEM_TYPE_2_1:I = 0x3

.field public static final ITEM_TYPE_3:I = 0x4

.field public static final MAXIMUM_HIGHLIGHT_STRING_LENGTH:I = 0xc8

.field public static final PARTIALLY_EXPAND_COUNT:I = 0x14

.field private static TAG:Ljava/lang/String;

.field protected static mLayoutInflater:Landroid/view/LayoutInflater;


# instance fields
.field private final GALAXY_FINDER_DRAGANDROP_LABEL:Ljava/lang/String;

.field private mBgResId:I

.field protected mColumnCnt:I

.field protected mContext:Landroid/content/Context;

.field public mCurrentExpandedCount:I

.field protected mDefaultDisplayedItemCount:I

.field protected mDragListener:Landroid/view/View$OnDragListener;

.field private mDragNDropEnabled:Z

.field protected mExpandedMode:I

.field protected mFirstVisibleIndex:I

.field private mImageLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

.field protected mIsDisplay:Z

.field protected mIsTablet:Z

.field protected mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mLastVisibleIndex:I

.field public mListener:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

.field public mOutlinePaint:Landroid/graphics/Paint;

.field protected mPackageName:Ljava/lang/String;

.field protected mPosition:I

.field protected mPrevViewMode:I

.field protected mQuery:Ljava/lang/String;

.field protected mResources:Landroid/content/res/Resources;

.field private mVibrator:Landroid/os/Vibrator;

.field protected mViewMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->TAG:Ljava/lang/String;

    .line 85
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packagename"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;>;"
    const/16 v8, 0x97

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 203
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 83
    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    .line 87
    const/4 v5, 0x4

    iput v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDefaultDisplayedItemCount:I

    .line 89
    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    .line 91
    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPackageName:Ljava/lang/String;

    .line 93
    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mQuery:Ljava/lang/String;

    .line 116
    iput v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mExpandedMode:I

    .line 118
    iget v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDefaultDisplayedItemCount:I

    iput v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    .line 120
    iput v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPrevViewMode:I

    .line 122
    iput v8, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mViewMode:I

    .line 138
    const-string v5, "cropUri"

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->GALAXY_FINDER_DRAGANDROP_LABEL:Ljava/lang/String;

    .line 140
    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mVibrator:Landroid/os/Vibrator;

    .line 146
    const/4 v5, -0x1

    iput v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mBgResId:I

    .line 148
    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    .line 150
    iput v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPosition:I

    .line 152
    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mImageLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    .line 154
    iput-boolean v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mIsTablet:Z

    .line 156
    new-instance v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$1;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)V

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDragListener:Landroid/view/View$OnDragListener;

    .line 196
    iput-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mListener:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

    .line 204
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    .line 205
    iput p4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDefaultDisplayedItemCount:I

    .line 206
    iput-object p5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    .line 207
    iput-object p2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPackageName:Ljava/lang/String;

    .line 208
    iput-object p3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mQuery:Ljava/lang/String;

    .line 210
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    if-nez v5, :cond_0

    .line 211
    new-instance v5, Landroid/view/ContextThemeWrapper;

    const v6, 0x7f0f0006

    invoke-direct {v5, p1, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    sput-object v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 215
    :cond_0
    iput v7, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mFirstVisibleIndex:I

    .line 216
    add-int/lit8 v5, p4, -0x1

    iput v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mLastVisibleIndex:I

    .line 218
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    const-string v6, "vibrator"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Vibrator;

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mVibrator:Landroid/os/Vibrator;

    .line 220
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    .line 221
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    const/high16 v6, 0x7f080000

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    iput-boolean v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mIsTablet:Z

    .line 222
    new-instance v5, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v5, p0, v6}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mImageLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    .line 224
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f070003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 226
    .local v1, "dndPackages":[Ljava/lang/String;
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 227
    .local v4, "pkg":Ljava/lang/String;
    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 228
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDragNDropEnabled:Z

    .line 226
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 238
    .end local v4    # "pkg":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isNormalMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/view/View;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setCheckedChange(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .prologue
    .line 79
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mBgResId:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDragNDropEnabled:Z

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->executeDragDrop(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isSelectableMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->sendLogInfo(Landroid/view/View;)V

    return-void
.end method

.method private calculateOptimumPositionForHighlight(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "findStr"    # Ljava/lang/String;
    .param p2, "srcStr"    # Ljava/lang/String;

    .prologue
    .line 521
    const/4 v0, -0x1

    .line 523
    .local v0, "offset":I
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 524
    invoke-direct {p0, p2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->findOptimumTextKeywordPosition(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 527
    :cond_0
    return v0
.end method

.method private executeDragDrop(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "dragUri"    # Ljava/lang/String;
    .param p3, "mimetype"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 391
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/GalaxyFinderActivity;->getMultiWindowInstance()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    .line 393
    .local v3, "multiWin":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    if-nez v3, :cond_1

    .line 394
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->TAG:Ljava/lang/String;

    const-string v7, "SMultiwindow instance is null"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 397
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v6}, Landroid/os/Vibrator;->cancel()V

    .line 398
    invoke-virtual {p1, v7}, Landroid/view/View;->setHapticFeedbackEnabled(Z)V

    goto :goto_0

    .line 401
    :cond_2
    invoke-virtual {p1, v10}, Landroid/view/View;->setHapticFeedbackEnabled(Z)V

    .line 402
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v8, 0xc8

    invoke-virtual {v6, v8, v9}, Landroid/os/Vibrator;->vibrate(J)V

    .line 404
    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz p2, :cond_0

    .line 405
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 407
    .local v1, "imageUri":Landroid/net/Uri;
    if-eqz v1, :cond_3

    if-eqz p3, :cond_3

    .line 408
    new-array v5, v10, [Ljava/lang/String;

    aput-object p3, v5, v7

    .line 411
    .local v5, "strs":[Ljava/lang/String;
    new-instance v2, Landroid/content/ClipData$Item;

    invoke-direct {v2, v1}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    .line 412
    .local v2, "item":Landroid/content/ClipData$Item;
    new-instance v0, Landroid/content/ClipData;

    const-string v6, "cropUri"

    invoke-direct {v0, v6, v5, v2}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    .line 414
    .local v0, "dragData":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    .line 415
    const/4 v4, 0x0

    .line 416
    .local v4, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    new-instance v4, Landroid/view/View$DragShadowBuilder;

    .end local v4    # "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    invoke-direct {v4, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 418
    .restart local v4    # "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    invoke-virtual {p1, v0, v4, p1, v7}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 419
    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 420
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {p1, v6}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    goto :goto_0

    .line 423
    .end local v0    # "dragData":Landroid/content/ClipData;
    .end local v2    # "item":Landroid/content/ClipData$Item;
    .end local v4    # "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    .end local v5    # "strs":[Ljava/lang/String;
    :cond_3
    sget-object v6, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "invalid uri or mimetype. uri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mimetype : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private findOptimumTextKeywordPosition(Ljava/lang/String;Ljava/lang/String;)I
    .locals 15
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "queryStr"    # Ljava/lang/String;

    .prologue
    .line 531
    new-instance v11, Ljava/util/StringTokenizer;

    const-string v13, " "

    move-object/from16 v0, p2

    invoke-direct {v11, v0, v13}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    .local v11, "tokenizerQueryStr":Ljava/util/StringTokenizer;
    const-string v6, ""

    .line 534
    .local v6, "pastTokenStr":Ljava/lang/String;
    const/4 v3, -0x1

    .line 535
    .local v3, "optimumOffset":I
    const/4 v2, -0x1

    .line 536
    .local v2, "currentTempLength":I
    const/4 v5, -0x1

    .line 537
    .local v5, "pastTempLength":I
    const/4 v4, -0x1

    .line 538
    .local v4, "pastOffset":I
    const/4 v1, -0x1

    .line 540
    .local v1, "bestMatchedOffset":I
    :cond_0
    :goto_0
    invoke-virtual {v11}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 541
    invoke-virtual {v11}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 543
    .local v9, "tokenQueryStr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_0

    .line 544
    const/4 v7, 0x0

    .line 545
    .local v7, "startOffset":I
    move-object/from16 v8, p1

    .line 547
    .local v8, "tempStr":Ljava/lang/String;
    :goto_1
    const-string v13, " "

    invoke-virtual {v8, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 548
    add-int/lit8 v7, v7, 0x1

    .line 549
    const/4 v13, 0x1

    invoke-virtual {v8, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 552
    :cond_1
    new-instance v12, Ljava/util/StringTokenizer;

    const-string v13, " "

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v13}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    .local v12, "tokenizerStr":Ljava/util/StringTokenizer;
    :goto_2
    invoke-virtual {v12}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 555
    invoke-virtual {v12}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v10

    .line 557
    .local v10, "tokenStr":Ljava/lang/String;
    invoke-virtual {v10, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 558
    if-ge v1, v7, :cond_2

    .line 559
    move v1, v7

    .line 580
    .end local v10    # "tokenStr":Ljava/lang/String;
    :cond_2
    const/4 v13, -0x1

    if-eq v1, v13, :cond_7

    .line 581
    move v3, v1

    .line 589
    .end local v7    # "startOffset":I
    .end local v8    # "tempStr":Ljava/lang/String;
    .end local v9    # "tokenQueryStr":Ljava/lang/String;
    .end local v12    # "tokenizerStr":Ljava/util/StringTokenizer;
    :cond_3
    return v3

    .line 563
    .restart local v7    # "startOffset":I
    .restart local v8    # "tempStr":Ljava/lang/String;
    .restart local v9    # "tokenQueryStr":Ljava/lang/String;
    .restart local v10    # "tokenStr":Ljava/lang/String;
    .restart local v12    # "tokenizerStr":Ljava/util/StringTokenizer;
    :cond_4
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_6

    .line 564
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v2

    .line 566
    const/4 v13, -0x1

    if-eq v5, v13, :cond_5

    if-lt v5, v2, :cond_6

    .line 567
    :cond_5
    invoke-direct {p0, v9, v6, v10}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isCurrentStrHasPriority(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 568
    move v5, v2

    .line 569
    move-object v6, v10

    .line 570
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    add-int v4, v7, v13

    .line 577
    :cond_6
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    add-int/2addr v7, v13

    .line 578
    goto :goto_2

    .line 583
    .end local v10    # "tokenStr":Ljava/lang/String;
    :cond_7
    const/4 v13, -0x1

    if-eq v3, v13, :cond_8

    if-le v3, v4, :cond_0

    .line 584
    :cond_8
    move v3, v4

    goto/16 :goto_0
.end method

.method private getCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 858
    const/4 v0, 0x0

    .line 860
    .local v0, "cacheKey":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 861
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPackageName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 862
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 868
    :cond_0
    :goto_0
    return-object v0

    .line 864
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private isCurrentStrHasPriority(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "before"    # Ljava/lang/String;
    .param p3, "current"    # Ljava/lang/String;

    .prologue
    const/4 v9, -0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 593
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 594
    .local v3, "positionA":I
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 596
    .local v4, "positionB":I
    if-eq v3, v9, :cond_0

    if-ne v4, v9, :cond_1

    .line 615
    :cond_0
    :goto_0
    return v5

    .line 600
    :cond_1
    if-gt v3, v4, :cond_0

    .line 602
    if-ne v3, v4, :cond_5

    .line 603
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v7, v8, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    .line 605
    .local v0, "count":I
    :goto_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    add-int/lit8 v7, v0, -0x1

    if-ge v1, v7, :cond_4

    .line 606
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {p3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v8

    sub-int v2, v7, v8

    .line 607
    .local v2, "index":I
    if-gez v2, :cond_3

    move v5, v6

    .line 608
    goto :goto_0

    .line 603
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "index":I
    :cond_2
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    .line 609
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    .restart local v2    # "index":I
    :cond_3
    if-gtz v2, :cond_0

    .line 605
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v2    # "index":I
    :cond_4
    move v5, v6

    .line 613
    goto :goto_0

    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_5
    move v5, v6

    .line 615
    goto :goto_0
.end method

.method private isNormalMode()Z
    .locals 2

    .prologue
    .line 850
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mViewMode:I

    const/16 v1, 0x97

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSelectableMode()Z
    .locals 2

    .prologue
    .line 846
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mViewMode:I

    const/16 v1, 0x98

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidItemIndex(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 854
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getEntireCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendLogInfo(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 757
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPackageName:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 758
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    const-string v3, "SRRS"

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPackageName:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertSurveryLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    :cond_0
    :goto_0
    return-void

    .line 761
    :cond_1
    const v2, 0x7f0b00a8

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 762
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v2, v1, Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 763
    check-cast v1, Landroid/widget/TextView;

    .end local v1    # "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 764
    .local v0, "cpName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 765
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    const-string v3, "SRRS"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertSurveryLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setCheckedChange(Landroid/view/View;I)V
    .locals 5
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 317
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isValidItemIndex(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 318
    sget-object v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 323
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isChecked()Z

    move-result v1

    .line 324
    .local v1, "prevState":Z
    if-nez v1, :cond_2

    const/4 v0, 0x1

    .line 326
    .local v0, "nextState":Z
    :goto_1
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setCheckedState(Landroid/view/View;IZ)V

    .line 327
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->setChecked(Z)V

    .line 329
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isSelectableMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 330
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mListener:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

    invoke-interface {v2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;->onChangeSelected(Z)V

    goto :goto_0

    .line 324
    .end local v0    # "nextState":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private setCheckedState(Landroid/view/View;IZ)V
    .locals 5
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "checked"    # Z

    .prologue
    const/4 v4, 0x0

    .line 293
    const v3, 0x7f0b0016

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 294
    .local v1, "checkbox":Landroid/view/View;
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isValid()Z

    move-result v2

    .line 296
    .local v2, "isSelectable":Z
    if-eqz v2, :cond_3

    .line 297
    if-eqz v1, :cond_1

    .line 298
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isSelectableMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 299
    instance-of v3, v1, Landroid/widget/CheckBox;

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 300
    check-cast v0, Landroid/widget/CheckBox;

    .line 301
    .local v0, "check":Landroid/widget/CheckBox;
    invoke-virtual {v0, p3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 304
    .end local v0    # "check":Landroid/widget/CheckBox;
    :cond_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 314
    :cond_1
    :goto_0
    return-void

    .line 306
    :cond_2
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 310
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isSelectableMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 311
    invoke-direct {p0, p1, v4}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setEnableDisableView(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method private setDivider(ILandroid/view/View;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 271
    const v3, 0x7f0b0072

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 272
    .local v0, "divider":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq p1, v3, :cond_1

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->hasSubTitle(I)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v1, 0x1

    .line 274
    .local v1, "hasDivider":Z
    :goto_0
    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 276
    .end local v1    # "hasDivider":Z
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 273
    goto :goto_0

    .line 274
    .restart local v1    # "hasDivider":Z
    :cond_2
    const/16 v2, 0x8

    goto :goto_1
.end method

.method private setEnableDisableView(Landroid/view/View;Z)V
    .locals 2
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "enable"    # Z

    .prologue
    .line 280
    if-eqz p2, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 281
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 282
    invoke-virtual {p1, p2}, Landroid/view/View;->setFocusable(Z)V

    .line 284
    const v1, 0x7f0b004d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 286
    .local v0, "subGroup":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 287
    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 288
    invoke-virtual {v0, p2}, Landroid/view/View;->setFocusable(Z)V

    .line 290
    :cond_0
    return-void

    .line 280
    .end local v0    # "subGroup":Landroid/view/View;
    :cond_1
    const v1, 0x3e99999a    # 0.3f

    goto :goto_0
.end method

.method private setFocusableView(Landroid/view/View;)V
    .locals 3
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 260
    if-eqz p1, :cond_0

    .line 261
    const v1, 0x7f0b004d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 263
    .local v0, "subGroup":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 265
    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 268
    .end local v0    # "subGroup":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private setLongPressEvent(Landroid/view/View;I)V
    .locals 3
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 336
    iget-boolean v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDragNDropEnabled:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 337
    const v2, 0x7f0b004d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 338
    .local v0, "mainGroup":Landroid/view/View;
    if-nez v0, :cond_1

    move-object v1, p1

    .line 340
    .local v1, "target":Landroid/view/View;
    :goto_0
    new-instance v2, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;

    invoke-direct {v2, p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$2;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 363
    .end local v0    # "mainGroup":Landroid/view/View;
    .end local v1    # "target":Landroid/view/View;
    :cond_0
    return-void

    .restart local v0    # "mainGroup":Landroid/view/View;
    :cond_1
    move-object v1, v0

    .line 338
    goto :goto_0
.end method


# virtual methods
.method public checkItemDispaly(ILandroid/view/ViewGroup;)V
    .locals 1
    .param p1, "i"    # I
    .param p2, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 1736
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPosition:I

    .line 1737
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mFirstVisibleIndex:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mLastVisibleIndex:I

    if-gt p1, v0, :cond_0

    .line 1738
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mIsDisplay:Z

    .line 1741
    :goto_0
    return-void

    .line 1740
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mIsDisplay:Z

    goto :goto_0
.end method

.method public expandMoreItem()I
    .locals 2

    .prologue
    .line 820
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mExpandedMode:I

    .line 821
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    add-int/lit8 v0, v0, 0x14

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    .line 823
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 824
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    .line 828
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->notifyDataSetInvalidated()V

    .line 829
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    return v0
.end method

.method public getBgResId()I
    .locals 1

    .prologue
    .line 1767
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mBgResId:I

    return v0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 1703
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mColumnCnt:I

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 242
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mExpandedMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 243
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 245
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDefaultDisplayedItemCount:I

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public getEntireCount()I
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getExpandedMode()I
    .locals 1

    .prologue
    .line 812
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mExpandedMode:I

    return v0
.end method

.method public getFirstItemIndexInRow(II)I
    .locals 1
    .param p1, "row"    # I
    .param p2, "orientation"    # I

    .prologue
    .line 1759
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mColumnCnt:I

    mul-int/2addr v0, p1

    return v0
.end method

.method public getFirstVisibleIndex()I
    .locals 1

    .prologue
    .line 1727
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mFirstVisibleIndex:I

    return v0
.end method

.method public getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 256
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLastVisibleIndex()I
    .locals 1

    .prologue
    .line 1731
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mLastVisibleIndex:I

    return v0
.end method

.method public getRowByItemIndex(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1763
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mColumnCnt:I

    div-int v0, p1, v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parents"    # Landroid/view/ViewGroup;

    .prologue
    .line 367
    if-eqz p2, :cond_1

    .line 368
    invoke-direct {p0, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setFocusableView(Landroid/view/View;)V

    .line 369
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setDivider(ILandroid/view/View;)V

    .line 371
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/FeatureConfig;->isEnabledMenuSelect()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setEnableDisableView(Landroid/view/View;Z)V

    .line 373
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->isChecked()Z

    move-result v0

    invoke-direct {p0, p2, p1, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setCheckedState(Landroid/view/View;IZ)V

    .line 376
    :cond_0
    invoke-direct {p0, p2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setLongPressEvent(Landroid/view/View;I)V

    .line 379
    :cond_1
    return-object p2
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 842
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mViewMode:I

    return v0
.end method

.method public getViewSubType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 387
    const/4 v0, 0x0

    return v0
.end method

.method public hasSubTitle(I)Z
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 1775
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge p1, v3, :cond_0

    .line 1776
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getItem(I)Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;

    move-result-object v0

    .line 1777
    .local v0, "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    invoke-virtual {v0}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;->getIndicationOfFirstItem()Ljava/lang/String;

    move-result-object v1

    .line 1778
    .local v1, "sortTitle":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1779
    const/4 v2, 0x1

    .line 1785
    .end local v0    # "item":Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionItemInfo;
    .end local v1    # "sortTitle":Ljava/lang/String;
    :cond_0
    return v2
.end method

.method public isDIsplayItem(I)Z
    .locals 1
    .param p1, "row"    # I

    .prologue
    .line 1744
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mFirstVisibleIndex:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mLastVisibleIndex:I

    if-gt p1, v0, :cond_0

    .line 1745
    const/4 v0, 0x1

    .line 1747
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDisplayItem()Z
    .locals 1

    .prologue
    .line 1751
    iget-boolean v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mIsDisplay:Z

    return v0
.end method

.method public isFullyExpanded()Z
    .locals 2

    .prologue
    .line 833
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected makeSpanString(Ljava/lang/String;II)Landroid/text/SpannableString;
    .locals 6
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "startOffset"    # I
    .param p3, "length"    # I

    .prologue
    .line 431
    add-int v0, p2, p3

    .line 432
    .local v0, "endOffset":I
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 434
    .local v1, "spanStr":Landroid/text/SpannableString;
    if-ltz p2, :cond_0

    .line 435
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const/16 v3, 0x2c

    const/16 v4, 0x9a

    const/16 v5, 0xd5

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v3, 0x21

    invoke-virtual {v1, v2, p2, v0, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 439
    :cond_0
    return-object v1
.end method

.method protected makeSpanString(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;)Landroid/text/SpannableString;
    .locals 13
    .param p1, "inputSentence"    # Ljava/lang/String;
    .param p2, "targetSentence"    # Ljava/lang/String;
    .param p3, "view"    # Landroid/widget/TextView;

    .prologue
    .line 444
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 445
    .local v4, "spanStr":Landroid/text/SpannableString;
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v9, " "

    invoke-direct {v5, p1, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    .local v5, "st":Ljava/util/StringTokenizer;
    :cond_0
    :goto_0
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 448
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    .line 449
    .local v8, "token":Ljava/lang/String;
    move-object v7, p2

    .line 450
    .local v7, "targetStr":Ljava/lang/String;
    const/4 v0, 0x0

    .line 453
    .local v0, "addOffset":I
    :cond_1
    invoke-virtual/range {p3 .. p3}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-static {v9, v7, v8}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->getPrefixForIndian(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 454
    .local v2, "keyword":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 455
    move-object v8, v2

    .line 457
    :cond_2
    const/4 v6, 0x0

    .line 458
    .local v6, "startOffset":I
    const/4 v1, 0x0

    .line 460
    .local v1, "endOffset":I
    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 462
    .local v3, "lowerCase":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    if-ne v9, v10, :cond_3

    .line 463
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 464
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    add-int v1, v6, v9

    .line 470
    :goto_1
    if-ltz v6, :cond_0

    .line 473
    new-instance v9, Landroid/text/style/ForegroundColorSpan;

    iget-object v10, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f09003b

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-direct {v9, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v10, v6, v0

    add-int v11, v1, v0

    const/16 v12, 0x21

    invoke-virtual {v4, v9, v10, v11, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 479
    invoke-virtual {v7, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 480
    add-int/2addr v0, v1

    .line 481
    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    const/4 v10, -0x1

    if-ne v9, v10, :cond_1

    goto :goto_0

    .line 466
    :cond_3
    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 467
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    add-int v1, v6, v9

    goto :goto_1

    .line 484
    .end local v0    # "addOffset":I
    .end local v1    # "endOffset":I
    .end local v2    # "keyword":Ljava/lang/String;
    .end local v3    # "lowerCase":Ljava/lang/String;
    .end local v6    # "startOffset":I
    .end local v7    # "targetStr":Ljava/lang/String;
    .end local v8    # "token":Ljava/lang/String;
    :cond_4
    return-object v4
.end method

.method public onChangeMultiSelection(Landroid/view/View;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 1792
    return-void
.end method

.method public refreshDataInItem(Landroid/view/View;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "index"    # I

    .prologue
    .line 1756
    return-void
.end method

.method public refreshExpandedCount(II)V
    .locals 1
    .param p1, "entireItemCount"    # I
    .param p2, "defaultItemCount"    # I

    .prologue
    .line 1695
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    .line 1696
    return-void
.end method

.method public resetImageLoader()V
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mImageLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->resetThreadPool()V
    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->access$700(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;)V

    .line 873
    return-void
.end method

.method public setBgResId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 1771
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mBgResId:I

    .line 1772
    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .param p1, "cnt"    # I

    .prologue
    .line 1699
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mColumnCnt:I

    .line 1700
    return-void
.end method

.method public setDefaultCount(I)V
    .locals 0
    .param p1, "cnt"    # I

    .prologue
    .line 1707
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDefaultDisplayedItemCount:I

    .line 1708
    return-void
.end method

.method public setExpandState(I)V
    .locals 2
    .param p1, "expandstate"    # I

    .prologue
    const/16 v1, 0x14

    .line 795
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mExpandedMode:I

    .line 797
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 798
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    if-ge v0, v1, :cond_0

    .line 799
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    .line 801
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 802
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    .line 808
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->notifyDataSetInvalidated()V

    .line 809
    return-void

    .line 804
    :cond_2
    if-nez p1, :cond_1

    .line 805
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mDefaultDisplayedItemCount:I

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mCurrentExpandedCount:I

    goto :goto_0
.end method

.method public setExpandedMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 816
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mExpandedMode:I

    .line 817
    return-void
.end method

.method public setFirstVisibleIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 1711
    if-gez p1, :cond_0

    .line 1712
    const/4 p1, 0x0

    .line 1714
    :cond_0
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mFirstVisibleIndex:I

    .line 1715
    return-void
.end method

.method protected setGenericAction(Landroid/view/View;Landroid/content/Intent;I)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "position"    # I

    .prologue
    .line 727
    if-eqz p1, :cond_0

    .line 728
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;

    invoke-direct {v0, p0, p2, p3}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$3;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/content/Intent;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 750
    const/4 v0, 0x1

    .line 753
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setGridViewDesc(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "subText"    # Ljava/lang/String;

    .prologue
    .line 1867
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1869
    .local v0, "sb":Ljava/lang/StringBuilder;
    if-eqz p2, :cond_0

    .line 1870
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1873
    :cond_0
    if-eqz p3, :cond_1

    .line 1874
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1877
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1878
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1879
    return-void
.end method

.method protected setImageView(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 9
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 720
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 721
    const/16 v6, 0xc9

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move v8, v5

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 724
    :cond_0
    return-void
.end method

.method protected setImageView(Landroid/widget/ImageView;Ljava/lang/String;I)V
    .locals 9
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "effect"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 705
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 706
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move v6, p3

    move v8, v5

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 709
    :cond_0
    return-void
.end method

.method protected setImageView(Landroid/widget/ImageView;Ljava/lang/String;ILjava/lang/String;)V
    .locals 9
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "bgId"    # I
    .param p4, "altUri"    # Ljava/lang/String;

    .prologue
    .line 713
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 714
    const/4 v4, 0x0

    const/16 v6, 0xc9

    const/4 v7, 0x0

    const/4 v8, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move v5, p3

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 717
    :cond_0
    return-void
.end method

.method protected setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "highLightRects"    # Ljava/lang/String;
    .param p4, "bgId"    # I

    .prologue
    .line 690
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 691
    const/4 v3, 0x0

    const/16 v6, 0xc9

    const/4 v7, 0x0

    const/4 v8, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 694
    :cond_0
    return-void
.end method

.method protected setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 9
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "highLightRects"    # Ljava/lang/String;
    .param p4, "bgId"    # I
    .param p5, "orientation"    # I

    .prologue
    .line 698
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 699
    const/4 v3, 0x0

    const/16 v6, 0xc9

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move v8, p5

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 702
    :cond_0
    return-void
.end method

.method protected setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 10
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "altUri"    # Ljava/lang/String;
    .param p4, "highLightRects"    # Ljava/lang/String;
    .param p5, "bgId"    # I
    .param p6, "effect"    # I
    .param p7, "contentType"    # I
    .param p8, "orientation"    # I

    .prologue
    .line 647
    const/4 v9, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V

    .line 649
    return-void
.end method

.method protected setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V
    .locals 14
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "altUri"    # Ljava/lang/String;
    .param p4, "highLightRects"    # Ljava/lang/String;
    .param p5, "bgId"    # I
    .param p6, "effect"    # I
    .param p7, "contentType"    # I
    .param p8, "orientation"    # I
    .param p9, "bgColor"    # I

    .prologue
    .line 654
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->isDisplayItem()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 656
    :try_start_0
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getInstance()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->getCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ImageLruCache;->getBitmapDrawable(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v12

    .line 657
    .local v12, "drawable":Landroid/graphics/drawable/Drawable;
    move/from16 v0, p5

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mBgResId:I

    .line 659
    if-eqz v12, :cond_3

    .line 660
    const/4 v1, -0x1

    move/from16 v0, p5

    if-eq v0, v1, :cond_1

    .line 661
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mResources:Landroid/content/res/Resources;

    move/from16 v0, p5

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 662
    .local v11, "bg":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1, v11}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 671
    .end local v11    # "bg":Landroid/graphics/drawable/Drawable;
    :goto_0
    invoke-virtual {p1, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 673
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f04000c

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v10

    .line 674
    .local v10, "ani":Landroid/view/animation/Animation;
    invoke-virtual {p1, v10}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 686
    .end local v10    # "ani":Landroid/view/animation/Animation;
    .end local v12    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_1
    return-void

    .line 664
    .restart local v12    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    const/4 v1, 0x3

    move/from16 v0, p7

    if-ne v0, v1, :cond_2

    .line 665
    const/high16 v1, -0x1000000

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 682
    .end local v12    # "drawable":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v13

    .line 683
    .local v13, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v13}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 667
    .end local v13    # "e":Ljava/lang/NumberFormatException;
    .restart local v12    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 676
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mImageLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    if-eqz v1, :cond_0

    .line 677
    invoke-virtual/range {p1 .. p2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 678
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mImageLoader:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;

    move-object/from16 v2, p2

    move-object v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->queuePhoto(Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;IIII)V
    invoke-static/range {v1 .. v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$ImageLoader;Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;IIII)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method protected setImageViewForContact(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 9
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 633
    const/16 v6, 0xc9

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move v8, v5

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 635
    return-void
.end method

.method protected setImageViewForContactRoundedStyle(Landroid/widget/ImageView;Ljava/lang/String;I)V
    .locals 10
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "bgColor"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 639
    const/16 v6, 0xc9

    const/4 v7, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move v8, v5

    move v9, p3

    invoke-virtual/range {v0 .. v9}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V

    .line 642
    return-void
.end method

.method protected setImageViewForVideo(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 9
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 628
    const/16 v6, 0xc9

    const/4 v7, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move v8, v5

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->setImageView(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 630
    return-void
.end method

.method public setItemStateListener(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;)V
    .locals 0
    .param p1, "cb"    # Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mListener:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$OnItemStateListener;

    .line 200
    return-void
.end method

.method public setLastVisibleIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 1722
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mLastVisibleIndex:I

    .line 1724
    return-void
.end method

.method protected setSubHeaderView(Landroid/view/ViewGroup;ILjava/lang/String;)V
    .locals 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    const v6, 0x7f0b0047

    .line 1795
    if-eqz p1, :cond_3

    .line 1796
    if-nez p2, :cond_1

    .line 1798
    const v4, 0x7f0b0046

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1800
    .local v3, "underline":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 1801
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1805
    :cond_0
    invoke-virtual {p1, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1807
    .local v2, "title":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 1808
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1810
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz v0, :cond_1

    .line 1811
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1813
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1818
    .end local v0    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v2    # "title":Landroid/widget/TextView;
    .end local v3    # "underline":Landroid/view/View;
    :cond_1
    invoke-virtual {p1, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1820
    .restart local v2    # "title":Landroid/widget/TextView;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1822
    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1825
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1827
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1828
    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1830
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1833
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1835
    .end local v2    # "title":Landroid/widget/TextView;
    :cond_3
    return-void
.end method

.method protected setTaskSuggestionAction(Landroid/view/View;Ljava/lang/String;II)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "taskType"    # I
    .param p4, "dataType"    # I

    .prologue
    .line 773
    invoke-static {p3, p4, p2}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->getIntentForRelatedTask(IILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 775
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 776
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$4;

    invoke-direct {v1, p0, v0}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter$4;-><init>(Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;Landroid/content/Intent;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 788
    const/4 v1, 0x1

    .line 791
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected setTextView(Landroid/widget/TextView;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "highlight"    # Z

    .prologue
    const/16 v5, 0xc8

    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 488
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 489
    if-eqz p3, :cond_4

    .line 490
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextAlignment()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 491
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 494
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v5, :cond_3

    .line 495
    const/4 v1, 0x0

    .line 497
    .local v1, "spanStr":Landroid/text/SpannableString;
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mQuery:Ljava/lang/String;

    invoke-direct {p0, v2, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->calculateOptimumPositionForHighlight(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 499
    .local v0, "offset":I
    if-ltz v0, :cond_2

    .line 500
    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {p0, v2, p2, p1}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->makeSpanString(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;)Landroid/text/SpannableString;

    move-result-object v1

    .line 501
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->KEYWORD:Landroid/text/TextUtils$TruncateAt;

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v2, v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;II)V

    .line 506
    :goto_0
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 518
    .end local v0    # "offset":I
    .end local v1    # "spanStr":Landroid/text/SpannableString;
    :cond_1
    :goto_1
    return-void

    .line 503
    .restart local v0    # "offset":I
    .restart local v1    # "spanStr":Landroid/text/SpannableString;
    :cond_2
    new-instance v1, Landroid/text/SpannableString;

    .end local v1    # "spanStr":Landroid/text/SpannableString;
    invoke-direct {v1, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .restart local v1    # "spanStr":Landroid/text/SpannableString;
    goto :goto_0

    .line 508
    .end local v0    # "offset":I
    .end local v1    # "spanStr":Landroid/text/SpannableString;
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {p2, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 511
    :cond_4
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextAlignment()I

    move-result v2

    if-ne v2, v3, :cond_5

    .line 512
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 515
    :cond_5
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setViewMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 837
    iget v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mViewMode:I

    iput v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mPrevViewMode:I

    .line 838
    iput p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;->mViewMode:I

    .line 839
    return-void
.end method
