.class public Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;
.super Ljava/lang/Object;
.source "TimeUtils.java"


# instance fields
.field private day:I

.field private month:I

.field private year:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    .line 19
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    .line 21
    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    .line 24
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 26
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    .line 27
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    .line 28
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    .line 29
    return-void
.end method


# virtual methods
.method public endOf7DaysNext()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 233
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 234
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 235
    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 237
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfNextWeek()J
    .locals 8

    .prologue
    const/16 v5, 0x3b

    const/4 v7, 0x1

    .line 197
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 198
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 199
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v7}, Ljava/util/Calendar;->set(II)V

    .line 200
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, Ljava/util/Calendar;->add(II)V

    .line 202
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfPastMonth()J
    .locals 8

    .prologue
    const/16 v7, 0x3b

    const/4 v4, 0x0

    .line 153
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 154
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 155
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v3

    .line 156
    .local v3, "days":I
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    add-int/lit8 v2, v2, -0x1

    const/16 v4, 0x17

    move v5, v7

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 158
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v4

    return-wide v4
.end method

.method public endOfPastWeek()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 175
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 176
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 177
    const/4 v1, 0x7

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 178
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 180
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfPastYear()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 217
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 218
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0xb

    const/16 v3, 0x1f

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 219
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfToday()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 121
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 122
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 124
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfTomorrow()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 135
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 136
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    add-int/lit8 v3, v3, 0x1

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 138
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfYesterday()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 107
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 108
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    add-int/lit8 v3, v3, -0x1

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 110
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public floor3digits(Ljava/lang/Long;)J
    .locals 4
    .param p1, "source"    # Ljava/lang/Long;

    .prologue
    const-wide v2, 0x408f400000000000L    # 1000.0

    .line 95
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-double v0, v0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getConvertTimeToDate(Landroid/content/Context;J)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "milliseconds"    # J

    .prologue
    .line 258
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 259
    .local v1, "dateFormat":Ljava/text/DateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p2, p3}, Ljava/util/Date;-><init>(J)V

    .line 260
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public last_24hours()J
    .locals 4

    .prologue
    .line 46
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 47
    .local v0, "c":Ljava/util/Calendar;
    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 49
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public month_ago()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 74
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 75
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 77
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public now()J
    .locals 4

    .prologue
    .line 32
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 33
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 35
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public startOf30DaysAgo()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 242
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 243
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 244
    const/4 v1, 0x5

    const/16 v2, -0x1e

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 246
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOf7DaysAgo()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 224
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 225
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 226
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 228
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfAYearAgo()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 251
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 252
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 254
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfNextWeek()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 186
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 187
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 188
    const/4 v1, 0x7

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 189
    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 191
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfPastMonth()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 144
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 145
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 147
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfPastWeek()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 164
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 165
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 166
    const/4 v1, 0x7

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 167
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 169
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfPastYear()J
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 208
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 209
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    add-int/lit8 v1, v1, -0x1

    const/4 v3, 0x1

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 211
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfToday()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 114
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 115
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 117
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfTomorrow()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 128
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 129
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    add-int/lit8 v3, v3, 0x1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 131
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfYesterday()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 100
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 101
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    add-int/lit8 v3, v3, -0x1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 103
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public threeday()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 59
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 60
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    add-int/lit8 v3, v3, -0x3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 62
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public today()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 39
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 40
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 42
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public week_ago()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 66
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 67
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 68
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 70
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public year_ago()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 81
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 82
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 84
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public years_ago()J
    .locals 4

    .prologue
    .line 88
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 89
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    add-int/lit8 v1, v1, -0x5

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 91
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public yesterday()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 53
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 54
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->year:I

    iget v2, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->month:I

    iget v3, p0, Lcom/samsung/android/app/galaxyfinder/util/TimeUtils;->day:I

    add-int/lit8 v3, v3, -0x1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 55
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method
