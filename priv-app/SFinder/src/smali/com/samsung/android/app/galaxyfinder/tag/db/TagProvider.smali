.class public Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;
.super Landroid/content/ContentProvider;
.source "TagProvider.java"


# static fields
.field static final CONTENTS:I = 0x1

.field static final FACES:I = 0xb

.field private static final LOCATION_TYPE:I

.field public static final PARAM_PACKAGENAME:Ljava/lang/String; = "appname"

.field public static final PARAM_STARTID:Ljava/lang/String; = "start_id"

.field private static final PRIORITY_FREQUENCY_TAG_QUERY:Ljava/lang/String; = "SELECT tags.[type] as tag_type, tags.[data], tags.[rawdata], count(tagging.[_id]) as tag_count from tags inner join tagging on tags.[_id] = tagging.[tag_id] inner join contents on contents.[_id] = tagging.[content_id] where %s group by tagging.[tag_id] order by tag_type desc, tag_count desc"

.field public static final PROVIDER_URI:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder.tag"

.field private static final QUERY_LIMIT:I = 0x32

.field private static final SELECTED_APP_TAGCOUNT_QUERY:Ljava/lang/String; = "select count(_id) as recordcnt from tagging where %s "

.field private static final SELECTED_APP_TAG_QUERY:Ljava/lang/String; = "SELECT tagging.[_id], tagging.[content_id], tagging.[tag_id], tags.[type], tags.[rawdata], tags.[data], contents.[contenturi] from tags inner join tagging on tags.[_id] = tagging.[tag_id] inner join contents on contents.[_id] = tagging.[content_id] where %s "

.field private static final TAG:Ljava/lang/String;

.field static final TAGCOUNT_BY_APPS:I = 0xa

.field public static final TAGCOUNT_SELECTED_APP_PATH:Ljava/lang/String; = "tagcount"

.field static final TAGGING:I = 0x2

.field static final TAGS:I = 0x3

.field static final TAGSFTS:I = 0x4

.field static final TAGS_BY_ALLCONTENT:I = 0x8

.field static final TAGS_BY_APPS:I = 0x9

.field static final TAGS_BY_CONTENT:I = 0x7

.field static final TAGS_BY_FREQUENCY:I = 0x6

.field static final TAGS_BY_LOCATION:I = 0xc

.field static final TAGS_BY_TIME:I = 0x5

.field public static final TAG_ALL_PATH:Ljava/lang/String; = "alltag"

.field public static final TAG_LOCATIONS:Ljava/lang/String; = "location"

.field public static final TAG_MINE_PATH:Ljava/lang/String; = "mytag"

.field public static final TAG_RECENT_PATH:Ljava/lang/String; = "recent"

.field public static final TAG_SELECTED_APP_PATH:Ljava/lang/String; = "apptag"

.field static final sUrimatcher:Landroid/content/UriMatcher;


# instance fields
.field private mDbHelper:Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    const-class v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    .line 71
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/Constants$TagType;->LOCATION:Lcom/samsung/android/app/galaxyfinder/Constants$TagType;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    sput v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->LOCATION_TYPE:I

    .line 76
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    .line 77
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "contents"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 78
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "tagging"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 79
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "tags"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 80
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "tags_fts"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 81
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "contents/recent"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 83
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "faces"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 84
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "tags/mytag"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "tags/alltag"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 88
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "tags/apptag"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 90
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "tags/tagcount"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 94
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.app.galaxyfinder.tag"

    const-string v2, "tags/location"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 96
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->mDbHelper:Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    return-void
.end method

.method private doRawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 16
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 459
    const-string v2, "[PERFORMANCE] "

    .line 461
    .local v2, "PERFORMANCE":Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 462
    .local v8, "queryTime":J
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 463
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v10

    .line 464
    .local v10, "stacktrace":[Ljava/lang/StackTraceElement;
    const/4 v11, 0x3

    aget-object v5, v10, v11

    .line 465
    .local v5, "e":Ljava/lang/StackTraceElement;
    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v6

    .line 466
    .local v6, "methodName":Ljava/lang/String;
    sget-object v11, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[PERFORMANCE] doRawQuery() called by "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 468
    sget-object v11, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[PERFORMANCE] doRawQuery() SQL > "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    .end local v5    # "e":Ljava/lang/StackTraceElement;
    .end local v6    # "methodName":Ljava/lang/String;
    .end local v10    # "stacktrace":[Ljava/lang/StackTraceElement;
    :cond_0
    const/4 v3, 0x0

    .line 473
    .local v3, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getDbHelper()Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 474
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 476
    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->isLoggable()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 477
    sget-object v11, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[PERFORMANCE] doRawQuery() Query\'s data COUNT = ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "], QueryTime = ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long/2addr v14, v8

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " ms]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    :goto_0
    return-object v3

    .line 480
    :catch_0
    move-exception v7

    .line 481
    .local v7, "se":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method private declared-synchronized getAllTagData(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 441
    monitor-enter p0

    :try_start_0
    const-string v1, "SELECT tags.[type] as tag_type, tags.[data], tags.[rawdata], count(tagging.[_id]) as tag_count from tags inner join tagging on tags.[_id] = tagging.[tag_id] inner join contents on contents.[_id] = tagging.[content_id] where %s group by tagging.[tag_id] order by tag_type desc, tag_count desc"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 443
    .local v0, "sql":Ljava/lang/String;
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->doRawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 441
    .end local v0    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized getAppsTagCount(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 453
    monitor-enter p0

    :try_start_0
    const-string v1, "select count(_id) as recordcnt from tagging where %s "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 455
    .local v0, "sql":Ljava/lang/String;
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->doRawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 453
    .end local v0    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized getAppsTagData(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1, "selection"    # Ljava/lang/String;

    .prologue
    .line 447
    monitor-enter p0

    :try_start_0
    const-string v1, "SELECT tagging.[_id], tagging.[content_id], tagging.[tag_id], tags.[type], tags.[rawdata], tags.[data], contents.[contenturi] from tags inner join tagging on tags.[_id] = tagging.[tag_id] inner join contents on contents.[_id] = tagging.[content_id] where %s "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 449
    .local v0, "sql":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->doRawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 447
    .end local v0    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized getDbHelper()Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;
    .locals 1

    .prologue
    .line 488
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->mDbHelper:Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    if-nez v0, :cond_0

    .line 489
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->mDbHelper:Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->mDbHelper:Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 488
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getMyTagData(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 423
    monitor-enter p0

    const/4 v3, 0x0

    .line 424
    .local v3, "sql":Ljava/lang/String;
    const/4 v0, 0x0

    .line 425
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 426
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "select tags.[type],tags.[rawdata], tags.[data]  from tags inner join tagging on tags.[_id] = tagging.[tag_id] inner join contents on tagging.[content_id] = contents.[_id]  where "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 431
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getDbHelper()Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 432
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 437
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 433
    :catch_0
    move-exception v2

    .line 434
    .local v2, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 423
    .end local v2    # "se":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 375
    const/4 v2, 0x0

    .line 376
    .local v2, "dbTableName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 378
    .local v0, "affectedRows":I
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 400
    :pswitch_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 380
    :pswitch_1
    const-string v2, "tags"

    .line 403
    :goto_0
    monitor-enter p0

    .line 405
    const/4 v1, 0x0

    .line 407
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getDbHelper()Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 408
    if-eqz v2, :cond_0

    .line 409
    invoke-virtual {v1, v2, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 411
    :cond_0
    sget-object v4, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "delete(), dbTableName : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", affectedRows = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " deleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    :goto_1
    if-lez v0, :cond_1

    .line 416
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 418
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 419
    return v0

    .line 384
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :pswitch_2
    const-string v2, "tagging"

    .line 385
    goto :goto_0

    .line 388
    :pswitch_3
    const-string v2, "contents"

    .line 389
    goto :goto_0

    .line 392
    :pswitch_4
    const-string v2, "faces"

    .line 393
    goto :goto_0

    .line 396
    :pswitch_5
    const-string v2, "tags_fts"

    .line 397
    goto :goto_0

    .line 412
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v3

    .line 413
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1

    .line 418
    .end local v3    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 378
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 113
    sget-object v0, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 128
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :pswitch_0
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.app.galaxyfinder.tag.tags"

    return-object v0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 23
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 134
    if-nez p2, :cond_1

    .line 135
    const/4 v9, 0x0

    .line 225
    :cond_0
    :goto_0
    return-object v9

    .line 137
    :cond_1
    const/16 v16, 0x0

    .line 138
    .local v16, "table":Ljava/lang/String;
    const/4 v15, 0x0

    .line 139
    .local v15, "sql":Ljava/lang/String;
    const/4 v14, 0x0

    .line 140
    .local v14, "selectionarg":[Ljava/lang/String;
    const-wide/16 v10, -0x1

    .line 141
    .local v10, "rowId":J
    const/4 v9, 0x0

    .line 143
    .local v9, "resultUri":Landroid/net/Uri;
    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v19

    sparse-switch v19, :sswitch_data_0

    .line 187
    new-instance v19, Ljava/lang/IllegalArgumentException;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Unknown Uri : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 145
    :sswitch_0
    const-string v16, "tags"

    .line 146
    const-string v19, "type"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 147
    .local v18, "type":Ljava/lang/String;
    const-string v19, "rawdata"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 148
    .local v7, "data":Ljava/lang/String;
    if-eqz v18, :cond_2

    if-eqz v7, :cond_2

    .line 149
    const-string v15, "select _id from tags where type= ? and rawdata= ?"

    .line 151
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v14, v0, [Ljava/lang/String;

    .end local v14    # "selectionarg":[Ljava/lang/String;
    const/16 v19, 0x0

    aput-object v18, v14, v19

    const/16 v19, 0x1

    aput-object v7, v14, v19

    .line 190
    .end local v7    # "data":Ljava/lang/String;
    .end local v18    # "type":Ljava/lang/String;
    .restart local v14    # "selectionarg":[Ljava/lang/String;
    :cond_2
    :goto_1
    if-eqz v16, :cond_0

    .line 191
    monitor-enter p0

    .line 192
    const/4 v8, 0x0

    .line 194
    .local v8, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getDbHelper()Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 195
    const/16 v19, 0x0

    const/16 v20, 0x3

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    move/from16 v3, v20

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v10

    .line 197
    const-string v19, "tags"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    const-wide/16 v20, 0x0

    cmp-long v19, v10, v20

    if-lez v19, :cond_3

    .line 198
    const-string v19, "tags_fts"

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, p2

    invoke-virtual {v8, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 199
    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    const-string v20, "Tag Fts inserted!!"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 216
    :cond_3
    const-wide/16 v20, 0x0

    cmp-long v19, v10, v20

    if-lez v19, :cond_4

    .line 217
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 218
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 219
    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "insert(), dbTableName : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", resultUri : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " inserted."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :cond_4
    :goto_2
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v19

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v19

    .line 158
    .end local v8    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :sswitch_1
    const-string v16, "contents"

    .line 159
    const-string v19, "contenturi"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 160
    .local v5, "curi":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 161
    const-string v15, "select _id from contents where contenturi= ?"

    .line 162
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v14, v0, [Ljava/lang/String;

    .end local v14    # "selectionarg":[Ljava/lang/String;
    const/16 v19, 0x0

    aput-object v5, v14, v19

    .restart local v14    # "selectionarg":[Ljava/lang/String;
    goto/16 :goto_1

    .line 169
    .end local v5    # "curi":Ljava/lang/String;
    :sswitch_2
    const-string v16, "tagging"

    .line 170
    const-string v19, "content_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 171
    .local v4, "cid":Ljava/lang/String;
    const-string v19, "tag_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 173
    .local v17, "tid":Ljava/lang/String;
    if-eqz v4, :cond_2

    if-eqz v17, :cond_2

    .line 174
    const-string v15, "select _id from tagging where content_id=? and tag_id=?"

    .line 176
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v14, v0, [Ljava/lang/String;

    .end local v14    # "selectionarg":[Ljava/lang/String;
    const/16 v19, 0x0

    aput-object v4, v14, v19

    const/16 v19, 0x1

    aput-object v17, v14, v19

    .restart local v14    # "selectionarg":[Ljava/lang/String;
    goto/16 :goto_1

    .line 183
    .end local v4    # "cid":Ljava/lang/String;
    .end local v17    # "tid":Ljava/lang/String;
    :sswitch_3
    const-string v16, "faces"

    .line 184
    goto/16 :goto_1

    .line 201
    .restart local v8    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v12

    .line 202
    .local v12, "sce":Landroid/database/sqlite/SQLiteConstraintException;
    :try_start_2
    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    const-string v20, "already inserted value"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const-wide/16 v20, -0x1

    cmp-long v19, v10, v20

    if-nez v19, :cond_6

    if-eqz v15, :cond_6

    if-eqz v14, :cond_6

    .line 204
    invoke-virtual {v8, v15, v14}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 205
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_6

    .line 206
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v19

    if-lez v19, :cond_5

    .line 207
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 208
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    move/from16 v0, v19

    int-to-long v10, v0

    .line 210
    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_6
    const-wide/16 v20, 0x0

    cmp-long v19, v10, v20

    if-lez v19, :cond_4

    .line 217
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 218
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 219
    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "insert(), dbTableName : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", resultUri : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " inserted."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 213
    .end local v12    # "sce":Landroid/database/sqlite/SQLiteConstraintException;
    :catch_1
    move-exception v13

    .line 214
    .local v13, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 216
    const-wide/16 v20, 0x0

    cmp-long v19, v10, v20

    if-lez v19, :cond_4

    .line 217
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 218
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 219
    sget-object v19, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "insert(), dbTableName : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", resultUri : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " inserted."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 216
    .end local v13    # "se":Landroid/database/sqlite/SQLiteException;
    :catchall_1
    move-exception v19

    const-wide/16 v20, 0x0

    cmp-long v20, v10, v20

    if-lez v20, :cond_7

    .line 217
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 218
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 219
    sget-object v20, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "insert(), dbTableName : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", resultUri : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " inserted."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    throw v19
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 143
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_0
        0xb -> :sswitch_3
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 14
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 237
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "query() - uri: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", selection: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", args: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {p4 .. p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 240
    .local v2, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v11, 0x0

    .line 241
    .local v11, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 242
    .local v7, "groupBy":Ljava/lang/String;
    const/4 v9, 0x0

    .line 243
    .local v9, "sortBy":Ljava/lang/String;
    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    .line 244
    .local v10, "queryLimit":Ljava/lang/String;
    const/4 v4, 0x0

    .line 246
    .local v4, "returnValues":[Ljava/lang/String;
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 296
    :pswitch_0
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    const-string v6, "Operation not supported for uri:"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 248
    :pswitch_1
    const-string v5, "tags"

    invoke-virtual {v2, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 249
    move-object/from16 v4, p2

    .line 300
    :goto_0
    monitor-enter p0

    .line 301
    const/4 v3, 0x0

    .line 303
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getDbHelper()Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 304
    const/4 v8, 0x0

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 309
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    if-eqz v11, :cond_0

    .line 312
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-interface {v11, v5, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    :cond_0
    move-object v5, v11

    .line 314
    .end local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_2
    return-object v5

    .line 253
    :pswitch_2
    const-string v5, "tagging"

    invoke-virtual {v2, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 254
    move-object/from16 v4, p2

    .line 255
    goto :goto_0

    .line 258
    :pswitch_3
    const-string v5, "contents"

    invoke-virtual {v2, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 259
    move-object/from16 v4, p2

    .line 260
    goto :goto_0

    .line 263
    :pswitch_4
    const-string v5, "tags_fts"

    invoke-virtual {v2, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 264
    move-object/from16 v4, p2

    .line 265
    goto :goto_0

    .line 268
    :pswitch_5
    const-string v5, "faces"

    invoke-virtual {v2, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 269
    move-object/from16 v4, p2

    .line 270
    const/4 v10, 0x0

    .line 271
    goto :goto_0

    .line 274
    :pswitch_6
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getMyTagData(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_2

    .line 277
    :pswitch_7
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getAllTagData(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_2

    .line 280
    :pswitch_8
    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getAppsTagData(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_2

    .line 283
    :pswitch_9
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getAppsTagCount(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_2

    .line 286
    :pswitch_a
    const-string v5, "tags_fts"

    invoke-virtual {v2, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 287
    const-string v5, "start_id"

    invoke-virtual {p1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 288
    .local v12, "queryParam":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->LOCATION_TYPE:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 289
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 290
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " AND rowid >"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 292
    :cond_1
    const/4 v5, 0x2

    new-array v4, v5, [Ljava/lang/String;

    .end local v4    # "returnValues":[Ljava/lang/String;
    const/4 v5, 0x0

    const-string v6, "rowid"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "data"

    aput-object v6, v4, v5

    .line 293
    .restart local v4    # "returnValues":[Ljava/lang/String;
    goto/16 :goto_0

    .line 306
    .end local v12    # "queryParam":Ljava/lang/String;
    .restart local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v13

    .line 307
    .local v13, "se":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto/16 :goto_1

    .line 309
    .end local v13    # "se":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 246
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_5
        :pswitch_a
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 319
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "update() - uri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", selection: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", args: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    const/4 v2, 0x0

    .line 323
    .local v2, "dbTableName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 324
    .local v0, "affectedRows":I
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->sUrimatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 342
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown URI : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 326
    :sswitch_0
    const-string v2, "tags"

    .line 345
    :goto_0
    monitor-enter p0

    .line 346
    const/4 v1, 0x0

    .line 348
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getDbHelper()Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 349
    if-eqz v2, :cond_0

    .line 350
    invoke-virtual {v1, v2, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 351
    const-string v5, "tags"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-lez v0, :cond_0

    .line 352
    const-string v5, "tags_fts"

    invoke-virtual {v1, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 354
    .local v4, "fts_updated_rows":I
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "update(), dbTableName : tags_fts, affectedRows = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " updated"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    .end local v4    # "fts_updated_rows":I
    :cond_0
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "update(), dbTableName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", affectedRows = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " updated"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366
    if-lez v0, :cond_1

    .line 367
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/tag/db/TagProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 370
    :cond_1
    return v0

    .line 330
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :sswitch_1
    const-string v2, "tagging"

    .line 331
    goto :goto_0

    .line 334
    :sswitch_2
    const-string v2, "contents"

    .line 335
    goto :goto_0

    .line 338
    :sswitch_3
    const-string v2, "faces"

    .line 339
    goto :goto_0

    .line 361
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v3

    .line 362
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1

    .line 364
    .end local v3    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 324
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0xb -> :sswitch_3
    .end sparse-switch
.end method
