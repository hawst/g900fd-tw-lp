.class Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$6;
.super Ljava/lang/Object;
.source "CustomSearchView.java"

# interfaces
.implements Lcom/samsung/android/writingbuddy/WritingBuddyImpl$OnTextWritingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextReceived(Ljava/lang/CharSequence;)V
    .locals 6
    .param p1, "arg0"    # Ljava/lang/CharSequence;

    .prologue
    .line 317
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 319
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->createURL(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->access$400(Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 321
    .local v2, "url":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 323
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "WLFR"

    const/4 v5, 0x2

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/galaxyfinder/util/Utils;->insertKeywordLog(Landroid/content/Context;Ljava/lang/String;I)V

    .line 327
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 328
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 329
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 330
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 331
    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView$6;->this$0:Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/ui/pager/CustomSearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 335
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "url":Ljava/lang/String;
    :cond_0
    return-void
.end method
