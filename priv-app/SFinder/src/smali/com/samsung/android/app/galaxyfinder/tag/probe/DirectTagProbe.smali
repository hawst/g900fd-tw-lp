.class public abstract Lcom/samsung/android/app/galaxyfinder/tag/probe/DirectTagProbe;
.super Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;
.source "DirectTagProbe.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagType"    # [Lcom/samsung/android/app/galaxyfinder/Constants$TagType;
    .param p3, "attr"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    .prologue
    .line 11
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AbstractTagProbe;-><init>(Landroid/content/Context;[Lcom/samsung/android/app/galaxyfinder/Constants$TagType;Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;)V

    .line 12
    return-void
.end method


# virtual methods
.method protected getRequest(Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;)Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;
    .locals 5
    .param p1, "info"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$ContentInfo;

    .prologue
    .line 16
    iget-object v1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DirectTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getSearchableInfo()Landroid/app/SearchableInfo;

    move-result-object v0

    .line 17
    .local v0, "searchable":Landroid/app/SearchableInfo;
    new-instance v1, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;

    sget-object v2, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/DirectTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/app/galaxyfinder/tag/probe/TagProbe$RequestInfo;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1
.end method
