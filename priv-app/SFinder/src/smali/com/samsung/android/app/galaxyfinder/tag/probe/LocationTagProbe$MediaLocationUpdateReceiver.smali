.class Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocationTagProbe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaLocationUpdateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;
    .param p2, "x1"    # Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$1;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;-><init>(Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;)V

    return-void
.end method

.method private getConvertType(I)Ljava/lang/String;
    .locals 1
    .param p1, "convertType"    # I

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 94
    .local v0, "type":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 108
    const-string v0, "WRONG CONVERT TYPE"

    .line 111
    :goto_0
    return-object v0

    .line 96
    :pswitch_0
    const-string v0, "LOCATION_TAG PARTIALLY sync from Samsung link"

    .line 97
    goto :goto_0

    .line 99
    :pswitch_1
    const-string v0, "LOCATION_TAG FULL SYNC by bootup"

    .line 100
    goto :goto_0

    .line 102
    :pswitch_2
    const-string v0, "LOCATION_TAG FULL SYNC by sdcard mounted"

    .line 103
    goto :goto_0

    .line 105
    :pswitch_3
    const-string v0, "LOCATION_TAG FULL SYNC by sdcard UNmounted"

    .line 106
    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 61
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;

    iget-object v5, v5, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getReceivedAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 65
    .local v0, "b":Landroid/os/Bundle;
    if-nez v0, :cond_2

    .line 66
    const-string v5, "LocationTagProbe"

    const-string v6, "MediaLocationUpdateReceiver:onReceive() Extras bundle is NULL"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 69
    :cond_2
    const-string v5, "media_provider_uri"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 70
    .local v4, "uriStr":Ljava/lang/String;
    if-nez v4, :cond_3

    .line 71
    const-string v5, "LocationTagProbe"

    const-string v6, "MediaLocationUpdateReceiver:onReceive() Uri String is NULL"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 74
    :cond_3
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 75
    .local v3, "uri":Landroid/net/Uri;
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;

    iget-object v5, v5, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->mTagAppAttr:Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/tag/probe/AppAttribute;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 76
    const-string v5, "media_convert_type"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 77
    .local v1, "convertType":I
    const-string v5, "LocationTagProbe"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaLocationUpdateReceiver:onReceive() "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", action = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", convertType = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct {p0, v1}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;->getConvertType(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    sget-object v5, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->TAG_PARTIALLY_SYNC:Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->ordinal()I

    move-result v5

    if-ne v1, v5, :cond_4

    .line 80
    const-string v5, "media_ids"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 81
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 82
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 83
    iget-object v6, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v8, v5

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v10, v5

    invoke-virtual {v6, v8, v9, v10, v11}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->prepare(JJ)V

    goto/16 :goto_0

    .line 86
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe$MediaLocationUpdateReceiver;->this$0:Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;

    invoke-static {}, Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;->values()[Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/galaxyfinder/tag/probe/LocationTagProbe;->requestFullSyncTagProbe(Lcom/samsung/android/app/galaxyfinder/tag/TagConstants$TagServiceType;)V

    goto/16 :goto_0
.end method
