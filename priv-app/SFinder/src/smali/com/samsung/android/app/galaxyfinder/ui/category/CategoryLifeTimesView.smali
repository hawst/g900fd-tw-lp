.class public Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;
.super Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;
.source "CategoryLifeTimesView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/galaxyfinder/ui/category/BaseCategoryView;-><init>(Landroid/content/Context;Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getAdapter()Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;
    .locals 6

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v3}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;->getDefaultItemCount()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;->mCategoryInfo:Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/galaxyfinder/data/suggestion/SuggestionCategoryInfo;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/ItemListLifeTimesAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;->mListAdapter:Lcom/samsung/android/app/galaxyfinder/ui/category/adapter/BaseItemListAdapter;

    return-object v0
.end method

.method protected getDefaultItemCount()I
    .locals 2

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/samsung/android/app/galaxyfinder/ui/category/CategoryLifeTimesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method protected getTemplateLayoutId()I
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f03001f

    return v0
.end method
