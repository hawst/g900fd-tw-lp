.class public Lcom/samsung/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/samsung/a/a/c;

.field public b:I

.field public c:[I

.field private d:Lcom/samsung/a/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    .line 23
    iput-object v0, p0, Lcom/samsung/a/a/a;->d:Lcom/samsung/a/a/c;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/a/a/a;->b:I

    .line 25
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/samsung/a/a/c;

    invoke-direct {v0}, Lcom/samsung/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    .line 34
    new-instance v0, Lcom/samsung/a/a/c;

    invoke-direct {v0}, Lcom/samsung/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/samsung/a/a/a;->d:Lcom/samsung/a/a/c;

    .line 35
    iget-object v0, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    invoke-virtual {v0, p1}, Lcom/samsung/a/a/c;->a(I)Z

    .line 36
    iget-object v0, p0, Lcom/samsung/a/a/a;->d:Lcom/samsung/a/a/c;

    invoke-virtual {v0, p1}, Lcom/samsung/a/a/c;->a(I)Z

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/a/a/a;->b:I

    .line 38
    new-array v0, p2, [I

    iput-object v0, p0, Lcom/samsung/a/a/a;->c:[I

    .line 39
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    if-nez v0, :cond_0

    .line 91
    const-string v0, "ART2Cluster"

    const-string v1, "Cluster does not created"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const/4 v0, 0x0

    .line 94
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/a/a/a;->d:Lcom/samsung/a/a/c;

    iget-object v1, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    invoke-virtual {v0, v1}, Lcom/samsung/a/a/c;->a(Lcom/samsung/a/a/c;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcom/samsung/a/a/c;I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 48
    iget-object v2, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    if-nez v2, :cond_0

    .line 49
    const-string v1, "ART2Cluster"

    const-string v2, "Cluster does not created"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :goto_0
    return v0

    .line 52
    :cond_0
    iget-object v2, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    invoke-virtual {v2, p1}, Lcom/samsung/a/a/c;->a(Lcom/samsung/a/a/c;)Z

    .line 53
    iget-object v2, p0, Lcom/samsung/a/a/a;->c:[I

    aput p2, v2, v0

    .line 54
    iput v1, p0, Lcom/samsung/a/a/a;->b:I

    move v0, v1

    .line 55
    goto :goto_0
.end method

.method public b()F
    .locals 6

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    if-nez v0, :cond_1

    .line 103
    const-string v0, "ART2Cluster"

    const-string v1, "Cluster does not created"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 111
    :cond_0
    return v0

    .line 106
    :cond_1
    sget v3, Lcom/samsung/a/a/c;->a:I

    .line 107
    const/4 v1, 0x0

    .line 108
    const/4 v0, 0x0

    move v5, v0

    move v0, v1

    move v1, v5

    :goto_0
    if-ge v1, v3, :cond_0

    .line 109
    iget-object v2, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    iget-object v2, v2, Lcom/samsung/a/a/c;->b:[F

    aget v2, v2, v1

    iget-object v4, p0, Lcom/samsung/a/a/a;->d:Lcom/samsung/a/a/c;

    iget-object v4, v4, Lcom/samsung/a/a/c;->b:[F

    aget v4, v4, v1

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v2, v0

    .line 108
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method public b(Lcom/samsung/a/a/c;I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    if-nez v1, :cond_0

    .line 66
    const-string v1, "ART2Cluster"

    const-string v2, "Cluster does not created"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :goto_0
    return v0

    .line 69
    :cond_0
    if-nez p1, :cond_1

    .line 70
    const-string v1, "ART2Cluster"

    const-string v2, "add feature is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 74
    :cond_1
    sget v1, Lcom/samsung/a/a/c;->a:I

    .line 75
    :goto_1
    if-lt v0, v1, :cond_2

    .line 80
    iget-object v0, p0, Lcom/samsung/a/a/a;->c:[I

    iget v1, p0, Lcom/samsung/a/a/a;->b:I

    aput p2, v0, v1

    .line 81
    iget v0, p0, Lcom/samsung/a/a/a;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/a/a/a;->b:I

    .line 82
    const/4 v0, 0x1

    goto :goto_0

    .line 77
    :cond_2
    iget-object v2, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    iget-object v2, v2, Lcom/samsung/a/a/c;->b:[F

    iget-object v3, p0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    iget-object v3, v3, Lcom/samsung/a/a/c;->b:[F

    aget v3, v3, v0

    iget v4, p0, Lcom/samsung/a/a/a;->b:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p1, Lcom/samsung/a/a/c;->b:[F

    aget v4, v4, v0

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/a/a/a;->b:I

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    div-float/2addr v3, v4

    aput v3, v2, v0

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
