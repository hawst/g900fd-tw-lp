.class public Lcom/samsung/a/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/samsung/a/a/d;

.field public b:[Lcom/samsung/a/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-direct {p0}, Lcom/samsung/a/a/e;->e()V

    .line 22
    return-void
.end method

.method private a(ILcom/samsung/a/a/c;I)Z
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2, p3}, Lcom/samsung/a/a/a;->b(Lcom/samsung/a/a/c;I)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/samsung/a/a/c;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 313
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v1, v1, Lcom/samsung/a/a/d;->k:I

    .line 314
    iget-object v2, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v2, v2, Lcom/samsung/a/a/d;->b:I

    .line 315
    if-lt v1, v2, :cond_0

    .line 316
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iput-boolean v0, v1, Lcom/samsung/a/a/d;->m:Z

    .line 317
    const/4 v0, 0x0

    .line 323
    :goto_0
    return v0

    .line 321
    :cond_0
    iget-object v2, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v1, v2, v1

    invoke-virtual {v1, p1, p2}, Lcom/samsung/a/a/a;->a(Lcom/samsung/a/a/c;I)Z

    .line 322
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v2, v1, Lcom/samsung/a/a/d;->k:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/samsung/a/a/d;->k:I

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/samsung/a/a/d;

    invoke-direct {v0}, Lcom/samsung/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/samsung/a/a/c;)Lcom/samsung/a/a/b;
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 148
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    if-nez v1, :cond_1

    .line 149
    const-string v1, "ART2Net"

    const-string v2, "ART2Net does not created"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_0
    :goto_0
    return-object v0

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v8, v1, Lcom/samsung/a/a/d;->a:I

    .line 154
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v9, v1, Lcom/samsung/a/a/d;->k:I

    .line 157
    const/4 v2, -0x1

    .line 159
    mul-int/lit16 v1, v8, 0x2710

    int-to-float v4, v1

    .line 160
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v1, v1, Lcom/samsung/a/a/d;->e:I

    if-nez v1, :cond_5

    move v1, v6

    move v0, v2

    move v3, v4

    .line 162
    :goto_1
    if-lt v1, v9, :cond_3

    .line 210
    :cond_2
    :goto_2
    new-instance v1, Lcom/samsung/a/a/b;

    invoke-direct {v1}, Lcom/samsung/a/a/b;-><init>()V

    .line 211
    iput v3, v1, Lcom/samsung/a/a/b;->b:F

    .line 212
    iput v0, v1, Lcom/samsung/a/a/b;->a:I

    move-object v0, v1

    .line 213
    goto :goto_0

    .line 164
    :cond_3
    iget-object v2, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v2, v2, v1

    iget-object v10, v2, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    move v4, v6

    move v2, v7

    .line 166
    :goto_3
    if-lt v4, v8, :cond_4

    .line 168
    cmpg-float v4, v2, v3

    if-gez v4, :cond_d

    move v0, v1

    .line 162
    :goto_4
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_1

    .line 167
    :cond_4
    iget-object v5, v10, Lcom/samsung/a/a/c;->b:[F

    aget v5, v5, v4

    iget-object v11, p1, Lcom/samsung/a/a/c;->b:[F

    aget v11, v11, v4

    sub-float/2addr v5, v11

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    add-float/2addr v5, v2

    .line 166
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v5

    goto :goto_3

    .line 175
    :cond_5
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v1, v1, Lcom/samsung/a/a/d;->e:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_7

    move v1, v6

    move v0, v2

    move v3, v4

    .line 177
    :goto_5
    if-ge v1, v9, :cond_2

    .line 179
    iget-object v2, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v2, v2, v1

    iget-object v5, v2, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    move v2, v6

    move v4, v7

    .line 181
    :goto_6
    if-lt v2, v8, :cond_6

    .line 183
    invoke-static {v4}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    .line 184
    cmpg-float v4, v2, v3

    if-gez v4, :cond_c

    move v0, v1

    .line 177
    :goto_7
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_5

    .line 182
    :cond_6
    iget-object v10, v5, Lcom/samsung/a/a/c;->b:[F

    aget v10, v10, v2

    iget-object v11, p1, Lcom/samsung/a/a/c;->b:[F

    aget v11, v11, v2

    sub-float/2addr v10, v11

    iget-object v11, v5, Lcom/samsung/a/a/c;->b:[F

    aget v11, v11, v2

    iget-object v12, p1, Lcom/samsung/a/a/c;->b:[F

    aget v12, v12, v2

    sub-float/2addr v11, v12

    mul-float/2addr v10, v11

    add-float/2addr v4, v10

    .line 181
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 191
    :cond_7
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v1, v1, Lcom/samsung/a/a/d;->e:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    move v1, v6

    .line 193
    :goto_8
    if-lt v1, v9, :cond_8

    move v0, v2

    move v3, v4

    .line 205
    goto :goto_2

    .line 195
    :cond_8
    iget-object v0, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v0, v0, v1

    iget-object v10, v0, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    move v5, v6

    move v3, v7

    .line 197
    :goto_9
    if-lt v5, v8, :cond_9

    .line 199
    cmpg-float v0, v3, v4

    if-gez v0, :cond_b

    move v0, v1

    move v2, v3

    .line 193
    :goto_a
    add-int/lit8 v1, v1, 0x1

    move v4, v2

    move v2, v0

    goto :goto_8

    .line 198
    :cond_9
    iget-object v0, v10, Lcom/samsung/a/a/c;->b:[F

    aget v0, v0, v5

    iget-object v11, p1, Lcom/samsung/a/a/c;->b:[F

    aget v11, v11, v5

    cmpg-float v0, v0, v11

    if-gez v0, :cond_a

    iget-object v0, v10, Lcom/samsung/a/a/c;->b:[F

    aget v0, v0, v5

    :goto_b
    add-float/2addr v3, v0

    .line 197
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_9

    .line 198
    :cond_a
    iget-object v0, p1, Lcom/samsung/a/a/c;->b:[F

    aget v0, v0, v5

    goto :goto_b

    :cond_b
    move v0, v2

    move v2, v4

    goto :goto_a

    :cond_c
    move v2, v3

    goto :goto_7

    :cond_d
    move v2, v3

    goto/16 :goto_4
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    if-nez v0, :cond_0

    .line 78
    const-string v0, "ART2Net"

    const-string v1, "ART2Net does not created"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    invoke-virtual {v0}, Lcom/samsung/a/a/d;->a()V

    .line 82
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/samsung/a/a/c;I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 92
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    if-nez v1, :cond_1

    .line 93
    const-string v1, "ART2Net"

    const-string v2, "ART2Net does not created"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_0
    :goto_0
    return v0

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v1, v1, Lcom/samsung/a/a/d;->k:I

    .line 100
    if-nez v1, :cond_2

    .line 101
    invoke-direct {p0, p1, p2}, Lcom/samsung/a/a/e;->b(Lcom/samsung/a/a/c;I)Z

    move-result v0

    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {p0, p1}, Lcom/samsung/a/a/e;->a(Lcom/samsung/a/a/c;)Lcom/samsung/a/a/b;

    move-result-object v2

    .line 106
    if-eqz v2, :cond_0

    .line 108
    iget v3, v2, Lcom/samsung/a/a/b;->a:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 115
    iget v0, v2, Lcom/samsung/a/a/b;->b:F

    iget-object v3, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v3, v3, Lcom/samsung/a/a/d;->g:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_3

    .line 117
    iget v0, v2, Lcom/samsung/a/a/b;->a:I

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/a/a/e;->a(ILcom/samsung/a/a/c;I)Z

    move-result v0

    goto :goto_0

    .line 121
    :cond_3
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget-boolean v0, v0, Lcom/samsung/a/a/d;->j:Z

    if-eqz v0, :cond_5

    .line 124
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v0, v0, Lcom/samsung/a/a/d;->b:I

    if-lt v1, v0, :cond_4

    .line 126
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/a/a/d;->m:Z

    .line 127
    iget v0, v2, Lcom/samsung/a/a/b;->a:I

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/a/a/e;->a(ILcom/samsung/a/a/c;I)Z

    move-result v0

    goto :goto_0

    .line 131
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/samsung/a/a/e;->b(Lcom/samsung/a/a/c;I)Z

    move-result v0

    goto :goto_0

    .line 136
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/samsung/a/a/e;->b(Lcom/samsung/a/a/c;I)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcom/samsung/a/a/d;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 34
    if-nez p1, :cond_0

    .line 35
    const-string v1, "ART2Net"

    const-string v2, "Invalide ART2Info"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :goto_0
    return v0

    .line 39
    :cond_0
    iget v1, p1, Lcom/samsung/a/a/d;->b:I

    .line 40
    iget v2, p1, Lcom/samsung/a/a/d;->a:I

    .line 41
    iget v3, p1, Lcom/samsung/a/a/d;->d:I

    .line 42
    if-gtz v1, :cond_1

    .line 43
    const-string v1, "ART2Net"

    const-string v2, "Invalid max cluster num in ART2Info"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 46
    :cond_1
    if-gtz v2, :cond_2

    .line 47
    const-string v1, "ART2Net"

    const-string v2, "Invalid feature size in ART2Info"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 50
    :cond_2
    if-gtz v3, :cond_3

    .line 51
    const-string v1, "ART2Net"

    const-string v2, "Invalid max feature pattern num in ART2Info"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 57
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/a/a/e;->a()Z

    .line 60
    iput-object p1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    .line 64
    new-array v4, v1, [Lcom/samsung/a/a/a;

    iput-object v4, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    .line 65
    :goto_1
    if-lt v0, v1, :cond_4

    .line 69
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    :cond_4
    iget-object v4, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    new-instance v5, Lcom/samsung/a/a/a;

    invoke-direct {v5}, Lcom/samsung/a/a/a;-><init>()V

    aput-object v5, v4, v0

    .line 67
    iget-object v4, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v4, v4, v0

    invoke-virtual {v4, v2, v3}, Lcom/samsung/a/a/a;->a(II)V

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public b()F
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 223
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    if-nez v0, :cond_0

    .line 224
    const-string v0, "ART2Net"

    const-string v1, "ART2Net does not created"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    const/high16 v0, -0x40800000    # -1.0f

    .line 243
    :goto_0
    return v0

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v4, v0, Lcom/samsung/a/a/d;->k:I

    .line 229
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v5, v0, Lcom/samsung/a/a/d;->a:I

    .line 231
    const/4 v0, 0x0

    move v3, v1

    .line 232
    :goto_1
    if-lt v3, v4, :cond_1

    .line 241
    int-to-float v1, v4

    div-float/2addr v0, v1

    .line 242
    iget-object v1, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iput v0, v1, Lcom/samsung/a/a/d;->l:F

    goto :goto_0

    :cond_1
    move v2, v0

    move v0, v1

    .line 235
    :goto_2
    if-lt v0, v5, :cond_2

    .line 232
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_1

    .line 237
    :cond_2
    iget-object v6, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Lcom/samsung/a/a/a;->b()F

    move-result v6

    add-float/2addr v2, v6

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 249
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    if-nez v0, :cond_1

    .line 250
    const-string v0, "ART2Net"

    const-string v1, "ART2Net does not created"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_0
    return-void

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v2, v0, Lcom/samsung/a/a/d;->k:I

    move v0, v1

    .line 255
    :goto_0
    if-ge v0, v2, :cond_0

    .line 258
    iget-object v3, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v3, v3, v0

    iput v1, v3, Lcom/samsung/a/a/a;->b:I

    .line 260
    iget-object v3, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/samsung/a/a/a;->a()Z

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public d()I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 271
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    if-nez v0, :cond_0

    .line 272
    const-string v0, "ART2Net"

    const-string v1, "ART2Net does not created"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 306
    :goto_0
    return v2

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v4, v0, Lcom/samsung/a/a/d;->k:I

    move v1, v3

    move v2, v3

    .line 277
    :goto_1
    if-lt v1, v4, :cond_1

    .line 304
    iget-object v0, p0, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iput v2, v0, Lcom/samsung/a/a/d;->k:I

    goto :goto_0

    .line 279
    :cond_1
    iget-object v0, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v0, v0, v1

    iget v0, v0, Lcom/samsung/a/a/a;->b:I

    if-nez v0, :cond_4

    .line 281
    add-int/lit8 v0, v1, 0x1

    :goto_2
    if-lt v0, v4, :cond_2

    move v0, v1

    move v1, v2

    .line 277
    :goto_3
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 284
    :cond_2
    iget-object v5, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v5, v5, v0

    iget v5, v5, Lcom/samsung/a/a/a;->b:I

    if-eqz v5, :cond_3

    .line 286
    iget-object v5, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v5, v5, v1

    .line 287
    iget-object v6, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    iget-object v7, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v7, v7, v0

    aput-object v7, v6, v1

    .line 288
    iget-object v1, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aput-object v5, v1, v0

    .line 289
    iget-object v1, p0, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    aget-object v1, v1, v0

    iput v3, v1, Lcom/samsung/a/a/a;->b:I

    .line 291
    add-int/lit8 v1, v2, 0x1

    .line 294
    goto :goto_3

    .line 281
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 300
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_3
.end method
