.class public Lcom/samsung/a/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:I


# instance fields
.field public b:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/a/a/c;->b:[F

    .line 21
    const/4 v0, 0x0

    sput v0, Lcom/samsung/a/a/c;->a:I

    .line 22
    return-void
.end method


# virtual methods
.method public a(I)Z
    .locals 2

    .prologue
    .line 30
    if-gtz p1, :cond_0

    .line 31
    const-string v0, "ART2Feature"

    const-string v1, "Invalid feature size"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    const/4 v0, 0x0

    .line 36
    :goto_0
    return v0

    .line 34
    :cond_0
    new-array v0, p1, [F

    iput-object v0, p0, Lcom/samsung/a/a/c;->b:[F

    .line 35
    sput p1, Lcom/samsung/a/a/c;->a:I

    .line 36
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/samsung/a/a/c;)Z
    .locals 2

    .prologue
    .line 64
    if-nez p1, :cond_0

    .line 65
    const-string v0, "ART2Feature"

    const-string v1, "Invalid feature (null)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const/4 v0, 0x0

    .line 68
    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/samsung/a/a/c;->b:[F

    invoke-virtual {p0, v0}, Lcom/samsung/a/a/c;->a([F)Z

    move-result v0

    goto :goto_0
.end method

.method public a([F)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45
    if-nez p1, :cond_0

    .line 46
    const-string v1, "ART2Feature"

    const-string v2, "Invalid feature value"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :goto_0
    return v0

    .line 49
    :cond_0
    sget v1, Lcom/samsung/a/a/c;->a:I

    array-length v2, p1

    if-eq v1, v2, :cond_1

    .line 50
    const-string v1, "ART2Feature"

    const-string v2, "Feature size is not equal"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/samsung/a/a/c;->b:[F

    array-length v2, p1

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    const/4 v0, 0x1

    goto :goto_0
.end method
