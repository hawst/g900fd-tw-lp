.class public Lcom/samsung/samm/b/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/samm/b/a/f$a;
    }
.end annotation


# instance fields
.field private a:Lcom/samsung/samm/b/a/i;

.field private b:Lcom/samsung/samm/b/a/g;

.field private c:Lcom/samsung/samm/b/a/h;

.field private d:[Lcom/samsung/samm/b/a/f$a;

.field private e:Z

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-direct {p0}, Lcom/samsung/samm/b/a/f;->e()V

    .line 98
    return-void
.end method

.method public static a(Ljava/io/RandomAccessFile;)J
    .locals 10

    .prologue
    const-wide/16 v8, 0x10

    const-wide/16 v0, -0x1

    .line 180
    :try_start_0
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->length()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    long-to-int v2, v2

    int-to-long v4, v2

    .line 194
    sub-long v2, v4, v8

    .line 195
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-gez v6, :cond_1

    .line 196
    :try_start_1
    const-string v2, "SAMMLibraryCore"

    const-string v3, "Not AMS File(Not enough data size)"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 251
    :cond_0
    :goto_0
    return-wide v0

    .line 181
    :catch_0
    move-exception v2

    .line 182
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 183
    const-string v2, "SAMMLibraryCore"

    const-string v3, "Error while getting file length"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 204
    :cond_1
    :try_start_2
    invoke-virtual {p0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 205
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v2

    add-long/2addr v2, v8

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 206
    const-string v2, "SAMMLibraryCore"

    const-string v3, "Skip to the tail of AMS Data Fail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 244
    :catch_1
    move-exception v2

    .line 245
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Tag Error : FileNotFoundException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 213
    :cond_2
    :try_start_3
    new-instance v2, Lcom/samsung/samm/b/a/g;

    invoke-direct {v2}, Lcom/samsung/samm/b/a/g;-><init>()V

    .line 214
    invoke-virtual {v2, p0}, Lcom/samsung/samm/b/a/g;->a(Ljava/io/RandomAccessFile;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 221
    invoke-virtual {v2}, Lcom/samsung/samm/b/a/g;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/lit8 v2, v2, 0x10

    int-to-long v6, v2

    .line 222
    sub-long v2, v4, v6

    .line 229
    invoke-virtual {p0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 230
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v4, v6, v4

    if-lez v4, :cond_3

    .line 231
    const-string v2, "SAMMLibraryCore"

    const-string v3, "Skip to the head of AMS Data Fail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 247
    :catch_2
    move-exception v2

    .line 248
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Tag Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 239
    :cond_3
    :try_start_4
    new-instance v4, Lcom/samsung/samm/b/a/i;

    invoke-direct {v4}, Lcom/samsung/samm/b/a/i;-><init>()V

    .line 240
    invoke-virtual {v4, p0}, Lcom/samsung/samm/b/a/i;->a(Ljava/io/RandomAccessFile;)Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move-result v4

    if-eqz v4, :cond_0

    move-wide v0, v2

    .line 243
    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;)J
    .locals 7

    .prologue
    const-wide/16 v0, -0x1

    .line 152
    if-nez p0, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-wide v0

    .line 154
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 155
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 158
    const/4 v4, 0x0

    .line 160
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v5, "r"

    invoke-direct {v3, v2, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :try_start_1
    invoke-static {v3}, Lcom/samsung/samm/b/a/f;->a(Ljava/io/RandomAccessFile;)J
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    .line 167
    if-eqz v3, :cond_0

    .line 168
    :try_start_2
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 169
    :catch_0
    move-exception v2

    .line 170
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 162
    :catch_1
    move-exception v2

    move-object v3, v4

    .line 163
    :goto_1
    :try_start_3
    const-string v4, "SAMMLibraryCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Read AMS Header Error : FileNotFoundException : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 167
    if-eqz v3, :cond_0

    .line 168
    :try_start_4
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 169
    :catch_2
    move-exception v2

    .line 170
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    move-object v3, v4

    .line 167
    :goto_2
    if-eqz v3, :cond_2

    .line 168
    :try_start_5
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 173
    :cond_2
    :goto_3
    throw v0

    .line 169
    :catch_3
    move-exception v1

    .line 170
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Read AMS Header Error : IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 165
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 162
    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method private e()V
    .locals 4

    .prologue
    const/16 v3, 0xff

    const/4 v0, 0x0

    .line 101
    new-instance v1, Lcom/samsung/samm/b/a/i;

    invoke-direct {v1}, Lcom/samsung/samm/b/a/i;-><init>()V

    iput-object v1, p0, Lcom/samsung/samm/b/a/f;->a:Lcom/samsung/samm/b/a/i;

    .line 102
    new-instance v1, Lcom/samsung/samm/b/a/g;

    invoke-direct {v1}, Lcom/samsung/samm/b/a/g;-><init>()V

    iput-object v1, p0, Lcom/samsung/samm/b/a/f;->b:Lcom/samsung/samm/b/a/g;

    .line 103
    new-instance v1, Lcom/samsung/samm/b/a/h;

    invoke-direct {v1}, Lcom/samsung/samm/b/a/h;-><init>()V

    iput-object v1, p0, Lcom/samsung/samm/b/a/f;->c:Lcom/samsung/samm/b/a/h;

    .line 104
    iput-boolean v0, p0, Lcom/samsung/samm/b/a/f;->e:Z

    .line 106
    new-array v1, v3, [Lcom/samsung/samm/b/a/f$a;

    iput-object v1, p0, Lcom/samsung/samm/b/a/f;->d:[Lcom/samsung/samm/b/a/f$a;

    .line 107
    :goto_0
    if-lt v0, v3, :cond_0

    .line 121
    return-void

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/f;->d:[Lcom/samsung/samm/b/a/f$a;

    new-instance v2, Lcom/samsung/samm/b/a/f$a;

    invoke-direct {v2, p0}, Lcom/samsung/samm/b/a/f$a;-><init>(Lcom/samsung/samm/b/a/f;)V

    aput-object v2, v1, v0

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/samsung/samm/b/a/i;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/samm/b/a/f;->a:Lcom/samsung/samm/b/a/i;

    return-object v0
.end method

.method public b()Lcom/samsung/samm/b/a/g;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/samm/b/a/f;->b:Lcom/samsung/samm/b/a/g;

    return-object v0
.end method

.method public b(Ljava/io/RandomAccessFile;)Z
    .locals 10

    .prologue
    const-wide/16 v6, 0x10

    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 293
    :try_start_0
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->length()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 307
    sub-long v4, v2, v6

    :try_start_1
    iput-wide v4, p0, Lcom/samsung/samm/b/a/f;->f:J

    .line 308
    iget-wide v4, p0, Lcom/samsung/samm/b/a/f;->f:J

    cmp-long v4, v4, v8

    if-gez v4, :cond_1

    .line 309
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Not AMS File(Not enough data size)"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 368
    :cond_0
    :goto_0
    return v0

    .line 294
    :catch_0
    move-exception v1

    .line 295
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 296
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Error while getting file length"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 317
    :cond_1
    :try_start_2
    iget-wide v4, p0, Lcom/samsung/samm/b/a/f;->f:J

    invoke-virtual {p1, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 318
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    add-long/2addr v4, v6

    cmp-long v4, v4, v2

    if-lez v4, :cond_2

    .line 319
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Skip to the tail of AMS Data Fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 361
    :catch_1
    move-exception v1

    .line 362
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Read AMS Tag Error : FileNotFoundException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 326
    :cond_2
    :try_start_3
    iget-object v4, p0, Lcom/samsung/samm/b/a/f;->b:Lcom/samsung/samm/b/a/g;

    invoke-virtual {v4, p1}, Lcom/samsung/samm/b/a/g;->a(Ljava/io/RandomAccessFile;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 333
    iget-object v4, p0, Lcom/samsung/samm/b/a/f;->b:Lcom/samsung/samm/b/a/g;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/g;->a()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    add-int/lit8 v4, v4, 0x10

    int-to-long v4, v4

    .line 334
    sub-long v6, v2, v4

    .line 336
    cmp-long v8, v6, v8

    if-gez v8, :cond_3

    .line 337
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Not AMS File(Not enough data size)"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 364
    :catch_2
    move-exception v1

    .line 365
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Read AMS Tag Error : IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 345
    :cond_3
    :try_start_4
    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 346
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    add-long/2addr v4, v6

    cmp-long v2, v4, v2

    if-lez v2, :cond_4

    .line 347
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Skip to the head of AMS Data Fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 354
    :cond_4
    iget-object v2, p0, Lcom/samsung/samm/b/a/f;->a:Lcom/samsung/samm/b/a/i;

    invoke-virtual {v2, p1}, Lcom/samsung/samm/b/a/i;->a(Ljava/io/RandomAccessFile;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 357
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/samm/b/a/f;->e:Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move v0, v1

    .line 359
    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 256
    if-nez p1, :cond_1

    .line 286
    :cond_0
    :goto_0
    return v0

    .line 258
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 262
    const/4 v3, 0x0

    .line 264
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v4, "r"

    invoke-direct {v2, v1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/samsung/samm/b/a/f;->b(Ljava/io/RandomAccessFile;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-nez v1, :cond_3

    .line 278
    if-eqz v2, :cond_0

    .line 279
    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 280
    :catch_0
    move-exception v1

    .line 281
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Read AMS Header Error : IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 272
    :catch_1
    move-exception v1

    move-object v2, v3

    .line 273
    :goto_1
    :try_start_3
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : FileNotFoundException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 278
    if-eqz v2, :cond_0

    .line 279
    :try_start_4
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 280
    :catch_2
    move-exception v1

    .line 281
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Read AMS Header Error : IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v0

    move-object v2, v3

    .line 278
    :goto_2
    if-eqz v2, :cond_2

    .line 279
    :try_start_5
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 284
    :cond_2
    :goto_3
    throw v0

    .line 280
    :catch_3
    move-exception v1

    .line 281
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Read AMS Header Error : IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 278
    :cond_3
    if-eqz v2, :cond_4

    .line 279
    :try_start_6
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 286
    :cond_4
    :goto_4
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 280
    :catch_4
    move-exception v0

    .line 281
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Read AMS Header Error : IOException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 276
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 272
    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method public c()Lcom/samsung/samm/b/a/h;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/samm/b/a/f;->c:Lcom/samsung/samm/b/a/h;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/samsung/samm/b/a/f;->e:Z

    return v0
.end method
