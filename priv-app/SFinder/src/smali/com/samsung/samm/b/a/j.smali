.class public abstract Lcom/samsung/samm/b/a/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:I

.field protected b:I

.field protected c:I

.field protected d:Lcom/samsung/samm/a/d;

.field protected e:Lcom/samsung/samm/b/a/j;

.field private f:I

.field private g:Lcom/samsung/samm/a/d;

.field private h:I

.field private i:I

.field private j:Lcom/samsung/samm/b/a/e;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/samsung/samm/b/a/j;->j:Lcom/samsung/samm/b/a/e;

    .line 48
    iput-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    .line 49
    iput-object v0, p0, Lcom/samsung/samm/b/a/j;->e:Lcom/samsung/samm/b/a/j;

    .line 51
    iput v1, p0, Lcom/samsung/samm/b/a/j;->f:I

    .line 52
    iput-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    .line 53
    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 54
    iput v1, p0, Lcom/samsung/samm/b/a/j;->i:I

    .line 56
    new-instance v0, Lcom/samsung/samm/b/a/e;

    invoke-direct {v0}, Lcom/samsung/samm/b/a/e;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/b/a/j;->j:Lcom/samsung/samm/b/a/e;

    .line 57
    return-void
.end method

.method private a(F)V
    .locals 7

    .prologue
    const v6, 0xffff

    const/high16 v5, 0x42c80000    # 100.0f

    const/4 v4, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    .line 190
    div-float v0, p1, v3

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0x168

    int-to-float v0, v0

    sub-float v0, p1, v0

    .line 191
    cmpg-float v1, v0, v4

    if-gez v1, :cond_0

    add-float/2addr v0, v3

    .line 192
    :cond_0
    mul-float/2addr v0, v5

    float-to-int v0, v0

    and-int v1, v0, v6

    .line 194
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_1

    .line 195
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 207
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->e()F

    move-result v0

    .line 198
    div-float v2, v0, v3

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0x168

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 199
    cmpg-float v2, v0, v4

    if-gez v2, :cond_2

    add-float/2addr v0, v3

    .line 200
    :cond_2
    mul-float/2addr v0, v5

    float-to-int v0, v0

    and-int/2addr v0, v6

    .line 201
    if-eq v0, v1, :cond_3

    .line 202
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 204
    :cond_3
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method private a(II)V
    .locals 2

    .prologue
    const/high16 v1, 0x10000

    .line 274
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    .line 275
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 283
    :goto_0
    return-void

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->i()I

    move-result v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->j()I

    move-result v0

    if-eq v0, p2, :cond_2

    .line 278
    :cond_1
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 280
    :cond_2
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method private a(Lcom/samsung/samm/b/a/e;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const v1, -0x20001

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/b/a/j;->j:Lcom/samsung/samm/b/a/e;

    .line 288
    if-eqz p1, :cond_1

    .line 289
    invoke-virtual {p1}, Lcom/samsung/samm/b/a/e;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 290
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 291
    iput-object p1, p0, Lcom/samsung/samm/b/a/j;->j:Lcom/samsung/samm/b/a/e;

    .line 292
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->j:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/e;->b()I

    move-result v0

    iput v0, p0, Lcom/samsung/samm/b/a/j;->i:I

    .line 301
    :goto_0
    return-void

    .line 294
    :cond_0
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 295
    iput v2, p0, Lcom/samsung/samm/b/a/j;->i:I

    goto :goto_0

    .line 298
    :cond_1
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 299
    iput v2, p0, Lcom/samsung/samm/b/a/j;->i:I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    .line 231
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 248
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->g()Ljava/lang/String;

    move-result-object v0

    .line 234
    if-eqz v0, :cond_2

    .line 235
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 236
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 238
    :cond_1
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 241
    :cond_2
    if-eqz p1, :cond_3

    .line 242
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 244
    :cond_3
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method private b(F)V
    .locals 5

    .prologue
    const v4, 0xffff

    const v3, 0x461c4000    # 10000.0f

    .line 210
    float-to-int v0, p1

    .line 211
    invoke-direct {p0, v0}, Lcom/samsung/samm/b/a/j;->d(I)V

    .line 213
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v1, :cond_0

    .line 214
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 226
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->c()F

    move-result v1

    .line 217
    float-to-int v2, v1

    .line 218
    int-to-float v0, v0

    sub-float v0, p1, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    and-int/2addr v0, v4

    .line 219
    int-to-float v2, v2

    sub-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    and-int/2addr v1, v4

    .line 220
    if-eq v1, v0, :cond_1

    .line 221
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 223
    :cond_1
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    const v2, -0x8001

    const v1, 0x8000

    .line 252
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    .line 253
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 270
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->f()Ljava/lang/String;

    move-result-object v0

    .line 256
    if-eqz v0, :cond_2

    .line 257
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 258
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 260
    :cond_1
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/2addr v0, v2

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 263
    :cond_2
    if-eqz p1, :cond_3

    .line 264
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 266
    :cond_3
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/2addr v0, v2

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 109
    and-int/lit16 v0, p1, 0xff

    .line 111
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v1, :cond_0

    .line 112
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 121
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->c()F

    move-result v1

    float-to-int v1, v1

    and-int/lit16 v1, v1, 0xff

    .line 115
    if-eq v1, v0, :cond_1

    .line 116
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 118
    :cond_1
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    .line 126
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 134
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->b()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 129
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 131
    :cond_1
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 152
    and-int/lit16 v0, p1, 0xff

    .line 154
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v1, :cond_0

    .line 155
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 163
    :goto_0
    iput v0, p0, Lcom/samsung/samm/b/a/j;->c:I

    .line 164
    return-void

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->e:Lcom/samsung/samm/b/a/j;

    iget v1, v1, Lcom/samsung/samm/b/a/j;->c:I

    and-int/lit16 v1, v1, 0xff

    if-eq v1, v0, :cond_1

    .line 158
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 160
    :cond_1
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, -0x401

    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method private h()I
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->f()Ljava/lang/String;

    move-result-object v0

    .line 357
    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 360
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()I
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->g()Ljava/lang/String;

    move-result-object v0

    .line 366
    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 369
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a([BI)I
.end method

.method public a([BIII)I
    .locals 8

    .prologue
    const/4 v2, -0x1

    const/high16 v7, 0x43b40000    # 360.0f

    const/4 v3, 0x0

    .line 614
    .line 615
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/j;->a()V

    .line 616
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/j;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    .line 619
    invoke-static {p1, p2, v0}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    .line 621
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v1

    .line 623
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_18

    .line 624
    add-int/lit8 v0, v1, 0x1

    iget v4, p0, Lcom/samsung/samm/b/a/j;->a:I

    int-to-byte v4, v4

    aput-byte v4, p1, v1

    .line 627
    :goto_0
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 628
    add-int/lit8 v1, v0, 0x1

    iget-object v4, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v4}, Lcom/samsung/samm/a/d;->a()I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, p1, v0

    move v0, v1

    .line 631
    :cond_0
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 632
    add-int/lit8 v1, v0, 0x1

    iget-object v4, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v4}, Lcom/samsung/samm/a/d;->c()F

    move-result v4

    float-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, p1, v0

    move v0, v1

    .line 635
    :cond_1
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 637
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->b()I

    move-result v5

    .line 638
    const/16 v1, 0x18

    :goto_1
    if-gez v1, :cond_11

    .line 642
    :cond_2
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    .line 643
    add-int/lit8 v1, v0, 0x1

    aput-byte v3, p1, v0

    move v0, v1

    .line 646
    :cond_3
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 647
    add-int/lit8 v1, v0, 0x1

    aput-byte v3, p1, v0

    move v0, v1

    .line 650
    :cond_4
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 651
    add-int/lit8 v1, v0, 0x1

    aput-byte v3, p1, v0

    move v0, v1

    .line 654
    :cond_5
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_6

    .line 655
    invoke-static {p1, v0, v3}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 658
    :cond_6
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_7

    .line 659
    add-int/lit8 v1, v0, 0x1

    aput-byte v3, p1, v0

    move v0, v1

    .line 662
    :cond_7
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_8

    .line 663
    add-int/lit8 v1, v0, 0x1

    iget v4, p0, Lcom/samsung/samm/b/a/j;->b:I

    int-to-byte v4, v4

    aput-byte v4, p1, v0

    move v0, v1

    .line 666
    :cond_8
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_9

    .line 667
    add-int/lit8 v1, v0, 0x1

    iget v4, p0, Lcom/samsung/samm/b/a/j;->c:I

    int-to-byte v4, v4

    aput-byte v4, p1, v0

    move v0, v1

    .line 670
    :cond_9
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_a

    .line 671
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    add-int/2addr v1, p3

    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 672
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    add-int/2addr v1, p4

    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 673
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    add-int/2addr v1, p3

    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 674
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    add-int/2addr v1, p4

    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 677
    :cond_a
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_c

    .line 678
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->e()F

    move-result v1

    .line 679
    div-float v4, v1, v7

    float-to-int v4, v4

    mul-int/lit16 v4, v4, 0x168

    int-to-float v4, v4

    sub-float/2addr v1, v4

    .line 680
    const/4 v4, 0x0

    cmpg-float v4, v1, v4

    if-gez v4, :cond_b

    add-float/2addr v1, v7

    .line 681
    :cond_b
    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 682
    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 685
    :cond_c
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_d

    .line 686
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->c()F

    move-result v1

    .line 687
    float-to-int v4, v1

    .line 688
    int-to-float v4, v4

    sub-float/2addr v1, v4

    const v4, 0x461c4000    # 10000.0f

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 689
    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 692
    :cond_d
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_e

    .line 694
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->g()Ljava/lang/String;

    move-result-object v1

    .line 695
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;->i()I

    move-result v5

    .line 696
    invoke-static {p1, v0, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 697
    if-lez v5, :cond_e

    .line 699
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    move v1, v3

    .line 700
    :goto_2
    if-lt v1, v5, :cond_12

    .line 706
    :cond_e
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    const v4, 0x8000

    and-int/2addr v1, v4

    if-eqz v1, :cond_f

    .line 708
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->f()Ljava/lang/String;

    move-result-object v1

    .line 709
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;->h()I

    move-result v5

    .line 710
    invoke-static {p1, v0, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 711
    if-lez v5, :cond_f

    .line 713
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    move v1, v3

    .line 714
    :goto_3
    if-lt v1, v5, :cond_13

    .line 720
    :cond_f
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    const/high16 v4, 0x10000

    and-int/2addr v1, v4

    if-eqz v1, :cond_17

    .line 721
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->i()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    .line 722
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->j()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    move v1, v0

    .line 725
    :goto_4
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    const/high16 v4, 0x20000

    and-int/2addr v0, v4

    if-eqz v0, :cond_15

    .line 726
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e;

    .line 728
    if-eqz v0, :cond_16

    .line 729
    invoke-virtual {v0, p1, v1}, Lcom/samsung/samm/b/a/e;->b([BI)I

    move-result v0

    .line 731
    :goto_5
    if-gez v0, :cond_14

    move v0, v2

    .line 743
    :cond_10
    :goto_6
    return v0

    .line 639
    :cond_11
    add-int/lit8 v4, v0, 0x1

    shr-int v6, v5, v1

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    aput-byte v6, p1, v0

    .line 638
    add-int/lit8 v0, v1, -0x8

    move v1, v0

    move v0, v4

    goto/16 :goto_1

    .line 701
    :cond_12
    aget-char v4, v6, v1

    invoke-static {p1, v0, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 700
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_2

    .line 715
    :cond_13
    aget-char v4, v6, v1

    invoke-static {p1, v0, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 714
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_3

    .line 734
    :cond_14
    add-int/2addr v1, v0

    .line 737
    :cond_15
    sub-int v0, v1, p2

    .line 738
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/j;->f()I

    move-result v1

    if-eq v0, v1, :cond_10

    .line 739
    const-string v1, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Write Basic Data fail : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/samm/b/a/j;->f()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 740
    goto :goto_6

    :cond_16
    move v0, v3

    goto :goto_5

    :cond_17
    move v1, v0

    goto :goto_4

    :cond_18
    move v0, v1

    goto/16 :goto_0
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/samm/b/a/j;->a:I

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/j;->a(I)V

    .line 66
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/j;->b(I)V

    .line 67
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->c()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/samm/b/a/j;->b(F)V

    .line 68
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/samm/b/a/j;->e(I)V

    .line 69
    iget v0, p0, Lcom/samsung/samm/b/a/j;->b:I

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/j;->c(I)V

    .line 70
    iget v0, p0, Lcom/samsung/samm/b/a/j;->c:I

    invoke-direct {p0, v0}, Lcom/samsung/samm/b/a/j;->f(I)V

    .line 71
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/j;->a(Landroid/graphics/RectF;)Z

    .line 72
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->e()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/samm/b/a/j;->a(F)V

    .line 73
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/samm/b/a/j;->a(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/samm/b/a/j;->b(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->i()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->j()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/samm/b/a/j;->a(II)V

    .line 76
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e;

    invoke-direct {p0, v0}, Lcom/samsung/samm/b/a/j;->a(Lcom/samsung/samm/b/a/e;)V

    .line 77
    return-void
.end method

.method protected a(I)V
    .locals 2

    .prologue
    .line 80
    and-int/lit16 v0, p1, 0xff

    .line 82
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v1, :cond_0

    .line 83
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 91
    :goto_0
    iput v0, p0, Lcom/samsung/samm/b/a/j;->a:I

    .line 92
    return-void

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->e:Lcom/samsung/samm/b/a/j;

    iget v1, v1, Lcom/samsung/samm/b/a/j;->a:I

    and-int/lit16 v1, v1, 0xff

    if-eq v1, v0, :cond_1

    .line 86
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 88
    :cond_1
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method protected a(Lcom/samsung/samm/b/a/j;)V
    .locals 1

    .prologue
    .line 309
    iput-object p1, p0, Lcom/samsung/samm/b/a/j;->e:Lcom/samsung/samm/b/a/j;

    .line 310
    if-eqz p1, :cond_0

    .line 311
    iget-object v0, p1, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iput-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    .line 315
    :goto_0
    return-void

    .line 313
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    goto :goto_0
.end method

.method protected a(Landroid/graphics/RectF;)Z
    .locals 6

    .prologue
    const v5, 0xffff

    .line 168
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 186
    :goto_0
    return v0

    .line 169
    :cond_0
    iget v0, p1, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    and-int/2addr v0, v5

    .line 170
    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    and-int/2addr v1, v5

    .line 171
    iget v2, p1, Landroid/graphics/RectF;->right:F

    float-to-int v2, v2

    and-int/2addr v2, v5

    .line 172
    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v3

    and-int/2addr v3, v5

    .line 174
    iget-object v4, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v4, :cond_1

    .line 175
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 186
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 177
    :cond_1
    iget-object v4, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v4}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->left:F

    float-to-int v4, v4

    and-int/2addr v4, v5

    if-ne v4, v0, :cond_2

    .line 178
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    and-int/2addr v0, v5

    if-ne v0, v2, :cond_2

    .line 179
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    and-int/2addr v0, v5

    if-ne v0, v1, :cond_2

    .line 180
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    and-int/2addr v0, v5

    if-eq v0, v3, :cond_3

    .line 181
    :cond_2
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_1

    .line 183
    :cond_3
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_1
.end method

.method public abstract a([III)[B
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 304
    iget v0, p0, Lcom/samsung/samm/b/a/j;->a:I

    return v0
.end method

.method public b([BI)I
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 383
    .line 384
    const/4 v0, 0x1

    new-array v7, v0, [I

    .line 386
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v1

    .line 388
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/h;

    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/h;

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/h;->d(I)Z

    .line 410
    :goto_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->c()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/d;->a(F)V

    .line 411
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/d;->a(I)V

    .line 412
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/d;->a(Landroid/graphics/RectF;)V

    .line 413
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->e()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/d;->b(F)V

    .line 414
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/d;->a(Ljava/lang/String;)V

    .line 415
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/d;->b(Ljava/lang/String;)V

    .line 416
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/d;->b(I)V

    .line 417
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/d;->c(I)V

    .line 418
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->e:Lcom/samsung/samm/b/a/j;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/j;->c()I

    move-result v0

    iput v0, p0, Lcom/samsung/samm/b/a/j;->b:I

    .line 419
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->e:Lcom/samsung/samm/b/a/j;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/j;->d()I

    move-result v0

    iput v0, p0, Lcom/samsung/samm/b/a/j;->c:I

    .line 423
    :cond_0
    invoke-static {p1, p2, v7}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v8

    .line 424
    aget v9, v7, v4

    .line 425
    if-nez v9, :cond_7

    .line 426
    sub-int v0, v8, p2

    .line 610
    :goto_1
    return v0

    .line 391
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/g;

    if-eqz v0, :cond_2

    .line 392
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/g;

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/g;->d(I)Z

    goto :goto_0

    .line 394
    :cond_2
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/i;

    if-eqz v0, :cond_3

    .line 395
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/i;

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/i;->d(I)Z

    goto/16 :goto_0

    .line 397
    :cond_3
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/e;

    if-eqz v0, :cond_4

    .line 398
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/e;

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/e;->d(I)Z

    goto/16 :goto_0

    .line 400
    :cond_4
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/j;

    if-eqz v0, :cond_5

    .line 401
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/j;

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/j;->d(I)Z

    goto/16 :goto_0

    .line 403
    :cond_5
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/f;

    if-eqz v0, :cond_6

    .line 404
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/f;

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/f;->d(I)Z

    goto/16 :goto_0

    .line 407
    :cond_6
    const-string v0, "SAMMLibraryCore"

    const-string v1, "Unknown SAMM Object"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 408
    goto :goto_1

    .line 427
    :cond_7
    if-gez v9, :cond_8

    .line 428
    const-string v0, "SAMMLibraryCore"

    const-string v1, "SAMM Object Data is Invalid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 429
    goto :goto_1

    .line 432
    :cond_8
    add-int v10, v8, v9

    .line 435
    invoke-static {p1, v8, v7}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v1

    .line 436
    aget v0, v7, v4

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 438
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_29

    .line 439
    add-int/lit8 v0, v1, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lcom/samsung/samm/b/a/j;->a:I

    .line 442
    :goto_2
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_9

    .line 443
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v5, v0, 0xff

    .line 444
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/h;

    if-eqz v0, :cond_1a

    .line 445
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/h;

    invoke-virtual {v0, v5}, Lcom/samsung/samm/a/h;->d(I)Z

    move v0, v1

    .line 468
    :cond_9
    :goto_3
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_a

    .line 469
    iget-object v5, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Lcom/samsung/samm/a/d;->a(F)V

    move v0, v1

    .line 473
    :cond_a
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_b

    move v1, v4

    move v5, v4

    .line 475
    :goto_4
    const/4 v6, 0x4

    if-lt v1, v6, :cond_20

    .line 479
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1, v5}, Lcom/samsung/samm/a/d;->a(I)V

    .line 485
    :cond_b
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_c

    .line 486
    add-int/lit8 v0, v0, 0x1

    .line 489
    :cond_c
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_d

    .line 490
    add-int/lit8 v0, v0, 0x1

    .line 493
    :cond_d
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_e

    .line 494
    add-int/lit8 v0, v0, 0x1

    .line 497
    :cond_e
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_f

    .line 498
    add-int/lit8 v0, v0, 0x2

    .line 501
    :cond_f
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_10

    .line 502
    add-int/lit8 v0, v0, 0x1

    .line 507
    :cond_10
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_11

    .line 508
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/samm/b/a/j;->b:I

    move v0, v1

    .line 512
    :cond_11
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_12

    .line 513
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/samm/b/a/j;->c:I

    move v0, v1

    .line 517
    :cond_12
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_13

    .line 518
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 519
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 520
    aget v5, v7, v4

    int-to-short v5, v5

    int-to-float v5, v5

    iput v5, v1, Landroid/graphics/RectF;->left:F

    .line 521
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 522
    aget v5, v7, v4

    int-to-short v5, v5

    int-to-float v5, v5

    iput v5, v1, Landroid/graphics/RectF;->top:F

    .line 523
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 524
    aget v5, v7, v4

    int-to-short v5, v5

    int-to-float v5, v5

    iput v5, v1, Landroid/graphics/RectF;->right:F

    .line 525
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 526
    aget v5, v7, v4

    int-to-short v5, v5

    int-to-float v5, v5

    iput v5, v1, Landroid/graphics/RectF;->bottom:F

    .line 527
    iget-object v5, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v5, v1}, Lcom/samsung/samm/a/d;->a(Landroid/graphics/RectF;)V

    .line 531
    :cond_13
    if-ge v0, v10, :cond_24

    .line 534
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_14

    .line 535
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 536
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    aget v5, v7, v4

    int-to-float v5, v5

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    invoke-virtual {v1, v5}, Lcom/samsung/samm/a/d;->b(F)V

    .line 540
    :cond_14
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_15

    .line 541
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->c()F

    move-result v1

    .line 542
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 543
    aget v5, v7, v4

    int-to-short v5, v5

    int-to-float v5, v5

    const v6, 0x461c4000    # 10000.0f

    div-float/2addr v5, v6

    add-float/2addr v1, v5

    .line 544
    iget-object v5, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v5, v1}, Lcom/samsung/samm/a/d;->a(F)V

    .line 548
    :cond_15
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_28

    .line 550
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 551
    aget v1, v7, v4

    int-to-char v5, v1

    .line 554
    if-lez v5, :cond_27

    .line 556
    new-array v6, v5, [C

    move v1, v0

    move v0, v4

    .line 557
    :goto_5
    if-ge v0, v5, :cond_16

    if-lt v1, v10, :cond_21

    .line 561
    :cond_16
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>([C)V

    .line 563
    :goto_6
    iget-object v5, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v5, v0}, Lcom/samsung/samm/a/d;->b(Ljava/lang/String;)V

    .line 567
    :goto_7
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    const v5, 0x8000

    and-int/2addr v0, v5

    if-eqz v0, :cond_26

    .line 569
    invoke-static {p1, v1, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 570
    aget v1, v7, v4

    int-to-char v5, v1

    .line 573
    if-lez v5, :cond_18

    .line 575
    new-array v6, v5, [C

    move v1, v4

    .line 576
    :goto_8
    if-ge v1, v5, :cond_17

    if-lt v0, v10, :cond_22

    .line 580
    :cond_17
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>([C)V

    .line 582
    :cond_18
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v1, v2}, Lcom/samsung/samm/a/d;->a(Ljava/lang/String;)V

    .line 586
    :goto_9
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    const/high16 v2, 0x10000

    and-int/2addr v1, v2

    if-eqz v1, :cond_19

    .line 587
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v0

    .line 588
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    aget v2, v7, v4

    invoke-virtual {v1, v2}, Lcom/samsung/samm/a/d;->b(I)V

    .line 589
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v0

    .line 590
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    aget v2, v7, v4

    invoke-virtual {v1, v2}, Lcom/samsung/samm/a/d;->c(I)V

    .line 594
    :cond_19
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    const/high16 v2, 0x20000

    and-int/2addr v1, v2

    if-eqz v1, :cond_24

    .line 595
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->j:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/samm/b/a/e;->a([BI)I

    move-result v1

    iput v1, p0, Lcom/samsung/samm/b/a/j;->i:I

    .line 596
    iget v1, p0, Lcom/samsung/samm/b/a/j;->i:I

    if-gez v1, :cond_23

    move v0, v3

    .line 597
    goto/16 :goto_1

    .line 447
    :cond_1a
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/g;

    if-eqz v0, :cond_1b

    .line 448
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/g;

    invoke-virtual {v0, v5}, Lcom/samsung/samm/a/g;->d(I)Z

    move v0, v1

    .line 449
    goto/16 :goto_3

    .line 450
    :cond_1b
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/i;

    if-eqz v0, :cond_1c

    .line 451
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/i;

    invoke-virtual {v0, v5}, Lcom/samsung/samm/a/i;->d(I)Z

    move v0, v1

    .line 452
    goto/16 :goto_3

    .line 453
    :cond_1c
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/e;

    if-eqz v0, :cond_1d

    .line 454
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/e;

    invoke-virtual {v0, v5}, Lcom/samsung/samm/a/e;->d(I)Z

    move v0, v1

    .line 455
    goto/16 :goto_3

    .line 456
    :cond_1d
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/j;

    if-eqz v0, :cond_1e

    .line 457
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/j;

    invoke-virtual {v0, v5}, Lcom/samsung/samm/a/j;->d(I)Z

    move v0, v1

    .line 458
    goto/16 :goto_3

    .line 459
    :cond_1e
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    instance-of v0, v0, Lcom/samsung/samm/a/f;

    if-eqz v0, :cond_1f

    .line 460
    iget-object v0, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/f;

    invoke-virtual {v0, v5}, Lcom/samsung/samm/a/f;->d(I)Z

    move v0, v1

    .line 461
    goto/16 :goto_3

    .line 463
    :cond_1f
    const-string v0, "SAMMLibraryCore"

    const-string v1, "Unknown SAMM Object"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 464
    goto/16 :goto_1

    .line 476
    :cond_20
    shl-int/lit8 v5, v5, 0x8

    .line 477
    add-int/lit8 v6, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    add-int/2addr v5, v0

    .line 475
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v6

    goto/16 :goto_4

    .line 558
    :cond_21
    invoke-static {p1, v1, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 559
    aget v11, v7, v4

    int-to-char v11, v11

    aput-char v11, v6, v0

    .line 557
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_5

    .line 577
    :cond_22
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v2

    .line 578
    aget v0, v7, v4

    int-to-char v0, v0

    aput-char v0, v6, v1

    .line 576
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto/16 :goto_8

    .line 599
    :cond_23
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->d:Lcom/samsung/samm/a/d;

    iget-object v2, p0, Lcom/samsung/samm/b/a/j;->j:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v1, v2}, Lcom/samsung/samm/a/d;->a(Ljava/lang/Object;)Z

    .line 600
    iget v1, p0, Lcom/samsung/samm/b/a/j;->i:I

    add-int/2addr v0, v1

    .line 604
    :cond_24
    sub-int/2addr v0, v8

    .line 605
    if-eq v0, v9, :cond_25

    .line 606
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Read Basic Data fail : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 607
    goto/16 :goto_1

    .line 610
    :cond_25
    sub-int v0, v10, p2

    goto/16 :goto_1

    :cond_26
    move v0, v1

    goto/16 :goto_9

    :cond_27
    move v1, v0

    move-object v0, v2

    goto/16 :goto_6

    :cond_28
    move v1, v0

    goto/16 :goto_7

    :cond_29
    move v0, v1

    goto/16 :goto_2
.end method

.method protected b(I)V
    .locals 2

    .prologue
    .line 95
    and-int/lit16 v0, p1, 0xff

    .line 97
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v1, :cond_0

    .line 98
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 106
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/a/d;->a()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-eq v1, v0, :cond_1

    .line 101
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 103
    :cond_1
    iget v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/samsung/samm/b/a/j;->b:I

    return v0
.end method

.method protected c(I)V
    .locals 2

    .prologue
    .line 137
    and-int/lit16 v0, p1, 0xff

    .line 139
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->g:Lcom/samsung/samm/a/d;

    if-nez v1, :cond_0

    .line 140
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    .line 148
    :goto_0
    iput v0, p0, Lcom/samsung/samm/b/a/j;->b:I

    .line 149
    return-void

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/j;->e:Lcom/samsung/samm/b/a/j;

    iget v1, v1, Lcom/samsung/samm/b/a/j;->b:I

    and-int/lit16 v1, v1, 0xff

    if-eq v1, v0, :cond_1

    .line 143
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0

    .line 145
    :cond_1
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    goto :goto_0
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 306
    iget v0, p0, Lcom/samsung/samm/b/a/j;->c:I

    return v0
.end method

.method public e()I
    .locals 2

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/j;->f()I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/samm/b/a/j;->g()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public f()I
    .locals 3

    .prologue
    .line 326
    const/4 v0, 0x0

    .line 327
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 328
    :cond_0
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    .line 329
    :cond_1
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    .line 330
    :cond_2
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    add-int/lit8 v0, v0, 0x4

    .line 331
    :cond_3
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    add-int/lit8 v0, v0, 0x1

    .line 332
    :cond_4
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    add-int/lit8 v0, v0, 0x1

    .line 333
    :cond_5
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    add-int/lit8 v0, v0, 0x1

    .line 334
    :cond_6
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    add-int/lit8 v0, v0, 0x2

    .line 335
    :cond_7
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    add-int/lit8 v0, v0, 0x1

    .line 336
    :cond_8
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    add-int/lit8 v0, v0, 0x1

    .line 337
    :cond_9
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_a

    add-int/lit8 v0, v0, 0x1

    .line 338
    :cond_a
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_b

    add-int/lit8 v0, v0, 0x8

    .line 339
    :cond_b
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_c

    add-int/lit8 v0, v0, 0x2

    .line 340
    :cond_c
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_d

    add-int/lit8 v0, v0, 0x2

    .line 341
    :cond_d
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_e

    .line 342
    add-int/lit8 v0, v0, 0x2

    .line 343
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;->i()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 345
    :cond_e
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    const v2, 0x8000

    and-int/2addr v1, v2

    if-eqz v1, :cond_f

    .line 346
    add-int/lit8 v0, v0, 0x2

    .line 347
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;->h()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 349
    :cond_f
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    const/high16 v2, 0x10000

    and-int/2addr v1, v2

    if-eqz v1, :cond_10

    add-int/lit8 v0, v0, 0x8

    .line 350
    :cond_10
    iget v1, p0, Lcom/samsung/samm/b/a/j;->h:I

    const/high16 v2, 0x20000

    and-int/2addr v1, v2

    if-eqz v1, :cond_11

    iget v1, p0, Lcom/samsung/samm/b/a/j;->i:I

    add-int/2addr v0, v1

    .line 351
    :cond_11
    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/samsung/samm/b/a/j;->f:I

    .line 352
    iget v0, p0, Lcom/samsung/samm/b/a/j;->f:I

    return v0
.end method

.method public abstract g()I
.end method
