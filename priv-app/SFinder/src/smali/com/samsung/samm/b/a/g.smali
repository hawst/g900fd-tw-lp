.class public Lcom/samsung/samm/b/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B


# instance fields
.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/16 v0, 0xc

    new-array v0, v0, [B

    sput-object v0, Lcom/samsung/samm/b/a/g;->a:[B

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v4, p0, Lcom/samsung/samm/b/a/g;->b:I

    .line 23
    const-string v0, "AMS"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sget-object v1, Lcom/samsung/samm/b/a/g;->a:[B

    const-string v2, "AMS"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 24
    sget-object v0, Lcom/samsung/samm/b/a/g;->a:[B

    const-string v1, "AMS"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    aput-byte v3, v0, v1

    .line 25
    sget-object v0, Lcom/samsung/samm/b/a/g;->a:[B

    const-string v1, "AMS"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    aput-byte v3, v0, v1

    .line 26
    const-string v0, "Samsung"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sget-object v1, Lcom/samsung/samm/b/a/g;->a:[B

    const-string v2, "AMS"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    const-string v3, "Samsung"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 27
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/samm/b/a/g;->b:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 36
    iput p1, p0, Lcom/samsung/samm/b/a/g;->b:I

    .line 37
    return-void
.end method

.method public a(Ljava/io/RandomAccessFile;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 42
    :try_start_0
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    .line 43
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    .line 44
    const-wide/16 v6, 0x10

    sub-long/2addr v2, v6

    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    .line 66
    :goto_0
    return v0

    .line 47
    :cond_0
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v1

    iput v1, p0, Lcom/samsung/samm/b/a/g;->b:I

    .line 51
    const/16 v1, 0xc

    new-array v1, v1, [B

    .line 52
    invoke-virtual {p1, v1}, Ljava/io/RandomAccessFile;->read([B)I

    .line 53
    new-instance v2, Ljava/lang/String;

    sget-object v3, Lcom/samsung/samm/b/a/g;->a:[B

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 57
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Not AMS File(Invalid AMS End Marker)"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v1

    .line 62
    const-string v2, "SAMMLibraryCore"

    const-string v3, "Read AMS Tag Error : IOException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 66
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Ljava/io/RandomAccessFile;)Z
    .locals 3

    .prologue
    .line 72
    :try_start_0
    iget v0, p0, Lcom/samsung/samm/b/a/g;->b:I

    invoke-static {p1, v0}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 76
    sget-object v0, Lcom/samsung/samm/b/a/g;->a:[B

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Write AMS Tag Error : IOException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 81
    const/4 v0, 0x0

    goto :goto_0
.end method
