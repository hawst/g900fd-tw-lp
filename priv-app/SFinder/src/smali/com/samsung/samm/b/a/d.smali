.class public Lcom/samsung/samm/b/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/samm/b/a/d$a;
    }
.end annotation


# static fields
.field private static a:Ljava/lang/String;

.field private static b:I

.field private static c:I

.field private static d:Ljava/lang/String;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:Z

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:I

.field private S:I

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:Z

.field private Y:J

.field private Z:Ljava/lang/String;

.field private aa:I

.field private ab:I

.field private ac:Z

.field private ad:Lcom/samsung/samm/a/a;

.field private ae:I

.field private af:Ljava/lang/String;

.field private ag:Ljava/lang/String;

.field private ah:Ljava/lang/String;

.field private ai:Ljava/lang/String;

.field private aj:Ljava/lang/String;

.field private ak:Ljava/lang/String;

.field private al:Ljava/lang/String;

.field private am:Ljava/lang/String;

.field private an:Ljava/lang/String;

.field private ao:Ljava/lang/String;

.field private ap:Ljava/lang/String;

.field private aq:Ljava/lang/String;

.field private ar:Ljava/lang/String;

.field private as:Z

.field private at:Z

.field private au:Landroid/content/Context;

.field private av:Z

.field private e:I

.field private f:[B

.field private g:Lcom/samsung/samm/b/a/f;

.field private h:Lcom/samsung/samm/b/a/h;

.field private i:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private k:[Lcom/samsung/samm/b/a/d$a;

.field private l:[Lcom/samsung/samm/b/a/d$a;

.field private m:Lcom/samsung/samm/b/a/c;

.field private n:Lcom/samsung/samm/b/a/c;

.field private o:Lcom/samsung/samm/b/a/c;

.field private p:Lcom/samsung/samm/b/a/c;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:Lcom/samsung/samm/b/a/c;

.field private v:Lcom/samsung/samm/b/a/b;

.field private w:Lcom/samsung/samm/b/a/a;

.field private x:Lcom/samsung/samm/b/a/q;

.field private y:Lcom/samsung/samm/b/a/e;

.field private z:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "SAMM Sample Application"

    sput-object v0, Lcom/samsung/samm/b/a/d;->a:Ljava/lang/String;

    .line 53
    const/4 v0, 0x1

    sput v0, Lcom/samsung/samm/b/a/d;->b:I

    .line 54
    const/4 v0, 0x0

    sput v0, Lcom/samsung/samm/b/a/d;->c:I

    .line 55
    const-string v0, "SDK"

    sput-object v0, Lcom/samsung/samm/b/a/d;->d:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput v3, p0, Lcom/samsung/samm/b/a/d;->e:I

    .line 62
    const/16 v0, 0x1e

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->f:[B

    .line 67
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->i:Ljava/util/LinkedList;

    .line 68
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    .line 69
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    .line 70
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    .line 75
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->q:Z

    .line 76
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->r:Z

    .line 77
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->s:Z

    .line 78
    iput v3, p0, Lcom/samsung/samm/b/a/d;->t:I

    .line 83
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    .line 84
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->z:Ljava/util/LinkedList;

    .line 86
    iput v4, p0, Lcom/samsung/samm/b/a/d;->A:I

    .line 87
    iput v3, p0, Lcom/samsung/samm/b/a/d;->B:I

    .line 88
    iput v3, p0, Lcom/samsung/samm/b/a/d;->C:I

    .line 93
    iput v3, p0, Lcom/samsung/samm/b/a/d;->H:I

    .line 94
    iput v3, p0, Lcom/samsung/samm/b/a/d;->I:I

    .line 95
    iput v3, p0, Lcom/samsung/samm/b/a/d;->J:I

    .line 96
    iput v3, p0, Lcom/samsung/samm/b/a/d;->K:I

    .line 97
    iput v3, p0, Lcom/samsung/samm/b/a/d;->L:I

    .line 98
    iput v3, p0, Lcom/samsung/samm/b/a/d;->M:I

    .line 99
    iput v3, p0, Lcom/samsung/samm/b/a/d;->N:I

    .line 100
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->O:Z

    .line 103
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->Q:Ljava/lang/String;

    .line 104
    iput v3, p0, Lcom/samsung/samm/b/a/d;->R:I

    .line 105
    iput v3, p0, Lcom/samsung/samm/b/a/d;->S:I

    .line 106
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->T:Ljava/lang/String;

    .line 108
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->U:Ljava/lang/String;

    .line 109
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->V:Ljava/lang/String;

    .line 110
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->W:Ljava/lang/String;

    .line 111
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->X:Z

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/samm/b/a/d;->Y:J

    .line 113
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->Z:Ljava/lang/String;

    .line 114
    iput v3, p0, Lcom/samsung/samm/b/a/d;->aa:I

    .line 115
    iput v3, p0, Lcom/samsung/samm/b/a/d;->ab:I

    .line 116
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->ac:Z

    .line 117
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    .line 119
    iput v3, p0, Lcom/samsung/samm/b/a/d;->ae:I

    .line 120
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->af:Ljava/lang/String;

    .line 126
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    .line 127
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    .line 128
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->ai:Ljava/lang/String;

    .line 129
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->aj:Ljava/lang/String;

    .line 130
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->ak:Ljava/lang/String;

    .line 131
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->al:Ljava/lang/String;

    .line 132
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->am:Ljava/lang/String;

    .line 133
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    .line 134
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->ao:Ljava/lang/String;

    .line 135
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->ap:Ljava/lang/String;

    .line 136
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->aq:Ljava/lang/String;

    .line 137
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->ar:Ljava/lang/String;

    .line 140
    iput-boolean v4, p0, Lcom/samsung/samm/b/a/d;->as:Z

    .line 141
    iput-boolean v4, p0, Lcom/samsung/samm/b/a/d;->at:Z

    .line 143
    iput-object v2, p0, Lcom/samsung/samm/b/a/d;->au:Landroid/content/Context;

    .line 144
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->av:Z

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/samsung/samm/b/a/s;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "SPenSDKTemp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    .line 148
    if-eqz p2, :cond_1

    .line 149
    iput-object p2, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    .line 151
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 152
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    :cond_0
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->av:Z

    .line 162
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UpdatedAMSBGImage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->ai:Ljava/lang/String;

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UpdatedAMSFGImage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->aj:Ljava/lang/String;

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UpdatedAMSClearImage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->ak:Ljava/lang/String;

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UpdatedAMSTextureImage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->al:Ljava/lang/String;

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UpdatedAMSThumbnailImage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->am:Ljava/lang/String;

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Embed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "EmbedVoiceObject"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->ao:Ljava/lang/String;

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "TempAMS.ams"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->ap:Ljava/lang/String;

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "TempAuthorImage.png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->aq:Ljava/lang/String;

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "TempUnrecordedObjectImage.png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->ar:Ljava/lang/String;

    .line 175
    iput-boolean p3, p0, Lcom/samsung/samm/b/a/d;->as:Z

    .line 176
    iput-boolean p4, p0, Lcom/samsung/samm/b/a/d;->at:Z

    .line 178
    iput-object p1, p0, Lcom/samsung/samm/b/a/d;->au:Landroid/content/Context;

    .line 180
    invoke-direct {p0}, Lcom/samsung/samm/b/a/d;->N()V

    .line 181
    return-void

    .line 158
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    .line 159
    iput-boolean v4, p0, Lcom/samsung/samm/b/a/d;->av:Z

    goto/16 :goto_0
.end method

.method private N()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 183
    new-instance v0, Lcom/samsung/samm/b/a/f;

    invoke-direct {v0}, Lcom/samsung/samm/b/a/f;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->g:Lcom/samsung/samm/b/a/f;

    .line 184
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->g:Lcom/samsung/samm/b/a/f;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/f;->c()Lcom/samsung/samm/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    .line 187
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->i:Ljava/util/LinkedList;

    .line 190
    invoke-direct {p0}, Lcom/samsung/samm/b/a/d;->ab()V

    .line 193
    new-instance v0, Lcom/samsung/samm/b/a/c;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ai:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/samsung/samm/b/a/d;->as:Z

    invoke-direct {v0, v1, v2}, Lcom/samsung/samm/b/a/c;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    .line 196
    new-instance v0, Lcom/samsung/samm/b/a/c;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->aj:Ljava/lang/String;

    invoke-direct {v0, v1, v5}, Lcom/samsung/samm/b/a/c;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->n:Lcom/samsung/samm/b/a/c;

    .line 199
    new-instance v0, Lcom/samsung/samm/b/a/c;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ak:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/samsung/samm/b/a/d;->at:Z

    invoke-direct {v0, v1, v2}, Lcom/samsung/samm/b/a/c;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->o:Lcom/samsung/samm/b/a/c;

    .line 202
    new-instance v0, Lcom/samsung/samm/b/a/c;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->al:Ljava/lang/String;

    invoke-direct {v0, v1, v5}, Lcom/samsung/samm/b/a/c;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->p:Lcom/samsung/samm/b/a/c;

    .line 203
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->q:Z

    .line 204
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->r:Z

    .line 205
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->s:Z

    .line 206
    iput v3, p0, Lcom/samsung/samm/b/a/d;->t:I

    .line 209
    new-instance v0, Lcom/samsung/samm/b/a/c;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->am:Ljava/lang/String;

    invoke-direct {v0, v1, v5}, Lcom/samsung/samm/b/a/c;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->u:Lcom/samsung/samm/b/a/c;

    .line 212
    new-instance v0, Lcom/samsung/samm/b/a/b;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/samsung/samm/b/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->v:Lcom/samsung/samm/b/a/b;

    .line 215
    new-instance v0, Lcom/samsung/samm/b/a/a;

    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/samsung/samm/b/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->w:Lcom/samsung/samm/b/a/a;

    .line 218
    new-instance v0, Lcom/samsung/samm/b/a/q;

    invoke-direct {v0}, Lcom/samsung/samm/b/a/q;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->x:Lcom/samsung/samm/b/a/q;

    .line 221
    new-instance v0, Lcom/samsung/samm/b/a/e;

    invoke-direct {v0}, Lcom/samsung/samm/b/a/e;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    .line 224
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->z:Ljava/util/LinkedList;

    .line 227
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/d;->f()V

    .line 229
    iput v3, p0, Lcom/samsung/samm/b/a/d;->C:I

    .line 230
    iput v3, p0, Lcom/samsung/samm/b/a/d;->D:I

    .line 231
    iput v3, p0, Lcom/samsung/samm/b/a/d;->E:I

    .line 232
    iput v3, p0, Lcom/samsung/samm/b/a/d;->H:I

    .line 233
    iput v3, p0, Lcom/samsung/samm/b/a/d;->I:I

    .line 234
    iput v3, p0, Lcom/samsung/samm/b/a/d;->J:I

    .line 235
    iput v3, p0, Lcom/samsung/samm/b/a/d;->K:I

    .line 237
    iput-object v4, p0, Lcom/samsung/samm/b/a/d;->P:Ljava/lang/String;

    .line 238
    iput-object v4, p0, Lcom/samsung/samm/b/a/d;->Q:Ljava/lang/String;

    .line 239
    iput v3, p0, Lcom/samsung/samm/b/a/d;->R:I

    .line 240
    iput v3, p0, Lcom/samsung/samm/b/a/d;->S:I

    .line 241
    iput-object v4, p0, Lcom/samsung/samm/b/a/d;->T:Ljava/lang/String;

    .line 243
    iput-object v4, p0, Lcom/samsung/samm/b/a/d;->U:Ljava/lang/String;

    .line 244
    iput-object v4, p0, Lcom/samsung/samm/b/a/d;->V:Ljava/lang/String;

    .line 245
    iput-object v4, p0, Lcom/samsung/samm/b/a/d;->W:Ljava/lang/String;

    .line 246
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->X:Z

    .line 248
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/samm/b/a/d;->Y:J

    .line 249
    iput-object v4, p0, Lcom/samsung/samm/b/a/d;->Z:Ljava/lang/String;

    .line 250
    iput v3, p0, Lcom/samsung/samm/b/a/d;->aa:I

    .line 251
    iput v3, p0, Lcom/samsung/samm/b/a/d;->ab:I

    .line 252
    iput-boolean v3, p0, Lcom/samsung/samm/b/a/d;->ac:Z

    .line 253
    iput v3, p0, Lcom/samsung/samm/b/a/d;->ae:I

    .line 255
    const-string v0, "1.7c"

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->af:Ljava/lang/String;

    .line 259
    return-void
.end method

.method private O()I
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->af:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->af:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 338
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private P()I
    .locals 1

    .prologue
    .line 842
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->P:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->P:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 845
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Q()I
    .locals 1

    .prologue
    .line 903
    sget-object v0, Lcom/samsung/samm/b/a/d;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 904
    sget-object v0, Lcom/samsung/samm/b/a/d;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 906
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private R()I
    .locals 1

    .prologue
    .line 910
    sget-object v0, Lcom/samsung/samm/b/a/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 911
    sget-object v0, Lcom/samsung/samm/b/a/d;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 913
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private S()I
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->U:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1024
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private T()I
    .locals 1

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->V:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1029
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->V:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1031
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private U()I
    .locals 1

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1036
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->W:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1038
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private V()I
    .locals 1

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->Z:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1090
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->Z:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1092
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private W()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3457
    iput v0, p0, Lcom/samsung/samm/b/a/d;->L:I

    .line 3459
    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    if-nez v2, :cond_1

    .line 3480
    :cond_0
    :goto_0
    return v0

    .line 3461
    :cond_1
    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 3462
    goto :goto_0

    :cond_2
    move v2, v0

    .line 3464
    :goto_1
    const/16 v3, 0xff

    if-lt v2, v3, :cond_3

    .line 3470
    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-direct {p0, v2, v0}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;I)I

    move-result v2

    .line 3471
    if-ltz v2, :cond_0

    .line 3475
    iget-object v3, p0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-direct {p0, v3, v2}, Lcom/samsung/samm/b/a/d;->b(Ljava/util/LinkedList;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3478
    iput v2, p0, Lcom/samsung/samm/b/a/d;->L:I

    move v0, v1

    .line 3480
    goto :goto_0

    .line 3466
    :cond_3
    iget-object v3, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    aget-object v3, v3, v2

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/samsung/samm/b/a/d$a;->b(I)V

    .line 3464
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private X()Z
    .locals 2

    .prologue
    .line 3540
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/h;->k()I

    move-result v0

    .line 3541
    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-direct {p0, v1, v0}, Lcom/samsung/samm/b/a/d;->c(Ljava/util/LinkedList;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3542
    const/4 v0, 0x0

    .line 3544
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Y()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 3571
    iput v0, p0, Lcom/samsung/samm/b/a/d;->M:I

    .line 3574
    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3585
    iput v1, p0, Lcom/samsung/samm/b/a/d;->M:I

    .line 3587
    const/4 v0, 0x1

    return v0

    .line 3574
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 3575
    instance-of v2, v0, Lcom/samsung/samm/a/e;

    if-eqz v2, :cond_0

    .line 3576
    const-string v2, "SAMM___LIBRARY___CONTENT___ID___KEY"

    invoke-virtual {v0, v2, v5}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v2

    .line 3577
    if-ltz v2, :cond_2

    .line 3578
    const-string v4, "SAMM___LIBRARY___ENCODE___IMAGE___ID___KEY"

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0, v4, v1}, Lcom/samsung/samm/a/d;->c(Ljava/lang/String;I)Z

    move v1, v2

    .line 3579
    goto :goto_0

    .line 3580
    :cond_2
    const-string v2, "SAMM___LIBRARY___ENCODE___IMAGE___ID___KEY"

    invoke-virtual {v0, v2, v5}, Lcom/samsung/samm/a/d;->c(Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method private Z()Z
    .locals 10

    .prologue
    const/16 v9, 0xff

    const/4 v8, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3593
    iput v2, p0, Lcom/samsung/samm/b/a/d;->N:I

    .line 3594
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    if-nez v0, :cond_1

    .line 3652
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v0, v2

    .line 3597
    :goto_1
    if-lt v0, v9, :cond_4

    .line 3603
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_2
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 3633
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_a

    .line 3650
    iput v1, p0, Lcom/samsung/samm/b/a/d;->N:I

    move v2, v3

    .line 3652
    goto :goto_0

    .line 3599
    :cond_4
    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v1, v1, v0

    invoke-virtual {v1, v8}, Lcom/samsung/samm/b/a/d$a;->b(I)V

    .line 3597
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3603
    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 3604
    instance-of v4, v0, Lcom/samsung/samm/a/j;

    if-eqz v4, :cond_2

    .line 3605
    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v4

    if-ne v4, v3, :cond_2

    .line 3606
    :cond_6
    const-string v4, "SAMM___LIBRARY___CONTENT___ID___KEY"

    invoke-virtual {v0, v4, v8}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v6

    .line 3607
    if-ltz v6, :cond_0

    move v4, v2

    .line 3610
    :goto_3
    if-lt v4, v1, :cond_7

    move v4, v2

    .line 3616
    :goto_4
    if-nez v4, :cond_2

    .line 3619
    if-ge v1, v9, :cond_9

    .line 3620
    check-cast v0, Lcom/samsung/samm/a/j;

    .line 3621
    iget-object v4, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v4, v4, v1

    invoke-virtual {v4, v6}, Lcom/samsung/samm/b/a/d$a;->b(I)V

    .line 3622
    iget-object v4, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v4, v4, v1

    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->k()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/samm/b/a/p;->a(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/samsung/samm/b/a/d$a;->c(I)V

    .line 3623
    iget-object v4, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v4, v4, v1

    const-string v6, "SAMM___LIBRARY___CONTENT___SIZE___KEY"

    invoke-virtual {v0, v6, v2}, Lcom/samsung/samm/a/j;->d(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/samsung/samm/b/a/d$a;->a(I)V

    .line 3624
    iget-object v4, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v4, v4, v1

    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/samsung/samm/b/a/d$a;->a(Ljava/lang/String;)V

    .line 3625
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 3626
    goto :goto_2

    .line 3611
    :cond_7
    iget-object v7, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v7, v7, v4

    invoke-virtual {v7}, Lcom/samsung/samm/b/a/d$a;->b()I

    move-result v7

    if-ne v7, v6, :cond_8

    move v4, v3

    .line 3613
    goto :goto_4

    .line 3610
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 3627
    :cond_9
    const-string v0, "SAMMLibraryCore"

    const-string v1, "The number of video is out of bound (255)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3633
    :cond_a
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 3634
    instance-of v4, v0, Lcom/samsung/samm/a/j;

    if-eqz v4, :cond_3

    .line 3635
    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v4

    if-ne v4, v3, :cond_3

    .line 3636
    :cond_b
    const-string v4, "SAMM___LIBRARY___CONTENT___ID___KEY"

    invoke-virtual {v0, v4, v8}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v6

    .line 3637
    if-ltz v6, :cond_0

    move v4, v2

    .line 3639
    :goto_5
    if-ge v4, v1, :cond_3

    .line 3640
    iget-object v7, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v7, v7, v4

    invoke-virtual {v7}, Lcom/samsung/samm/b/a/d$a;->b()I

    move-result v7

    .line 3641
    if-ne v7, v6, :cond_c

    .line 3642
    if-eq v4, v7, :cond_c

    .line 3643
    const-string v7, "SAMM___LIBRARY___CONTENT___ID___KEY"

    invoke-virtual {v0, v7, v4}, Lcom/samsung/samm/a/d;->c(Ljava/lang/String;I)Z

    .line 3639
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_5
.end method

.method private a(Ljava/util/LinkedList;I)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 3485
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3510
    :goto_1
    return p2

    .line 3485
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 3486
    instance-of v4, v0, Lcom/samsung/samm/a/g;

    if-eqz v4, :cond_0

    .line 3487
    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 3488
    const-string v4, "SAMM___LIBRARY___CONTENT___ID___KEY"

    invoke-virtual {v0, v4, v1}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v4

    .line 3489
    if-gez v4, :cond_2

    move p2, v1

    .line 3490
    goto :goto_1

    :cond_2
    move v0, v2

    .line 3492
    :goto_2
    if-lt v0, p2, :cond_3

    move v0, v2

    .line 3498
    :goto_3
    if-nez v0, :cond_0

    .line 3501
    const/16 v0, 0xff

    if-ge p2, v0, :cond_5

    .line 3502
    iget-object v5, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    add-int/lit8 v0, p2, 0x1

    aget-object v5, v5, p2

    invoke-virtual {v5, v4}, Lcom/samsung/samm/b/a/d$a;->b(I)V

    move p2, v0

    .line 3503
    goto :goto_0

    .line 3493
    :cond_3
    iget-object v5, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/d$a;->b()I

    move-result v5

    if-ne v5, v4, :cond_4

    .line 3494
    const/4 v0, 0x1

    .line 3495
    goto :goto_3

    .line 3492
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3504
    :cond_5
    const-string v0, "SAMMLibraryCore"

    const-string v2, "The number of images is out of bound (255)"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move p2, v1

    .line 3505
    goto :goto_1
.end method

.method public static a(Ljava/util/LinkedList;[I[BILcom/samsung/samm/b/a/j;I)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;[I[BI",
            "Lcom/samsung/samm/b/a/j;",
            "I)I"
        }
    .end annotation

    .prologue
    .line 4083
    if-nez p2, :cond_0

    .line 4084
    const/4 v0, -0x1

    .line 4196
    :goto_0
    return v0

    .line 4088
    :cond_0
    const/4 v0, 0x0

    aget v5, p1, v0

    .line 4090
    const/4 v3, 0x0

    move v0, p3

    :goto_1
    if-lt v3, v5, :cond_1

    .line 4194
    const/4 v1, 0x0

    aput v3, p1, v1

    goto :goto_0

    .line 4093
    :cond_1
    const/4 v1, 0x1

    new-array v4, v1, [I

    .line 4094
    add-int/lit8 v1, v0, 0x4

    .line 4095
    invoke-static {p2, v1, v4}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v1

    .line 4096
    const/4 v2, 0x0

    aget v2, v4, v2

    .line 4098
    and-int/lit8 v2, v2, 0x1

    if-gtz v2, :cond_2

    if-nez p4, :cond_3

    .line 4099
    :cond_2
    aget-byte v1, p2, v1

    and-int/lit16 v1, v1, 0xff

    move v2, v1

    .line 4107
    :goto_2
    const/4 v1, 0x1

    if-ne v2, v1, :cond_4

    .line 4108
    new-instance v2, Lcom/samsung/samm/a/h;

    invoke-direct {v2}, Lcom/samsung/samm/a/h;-><init>()V

    .line 4109
    new-instance v1, Lcom/samsung/samm/b/a/n;

    invoke-direct {v1, p4, v2}, Lcom/samsung/samm/b/a/n;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    .line 4161
    :goto_3
    invoke-virtual {v1, p2, v0}, Lcom/samsung/samm/b/a/j;->a([BI)I

    move-result v4

    .line 4162
    if-gez v4, :cond_b

    .line 4163
    const-string v0, "SAMMLibraryCore"

    const-string v1, "Read Object Data Error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4164
    const/4 v0, -0x1

    goto :goto_0

    .line 4101
    :cond_3
    invoke-virtual {p4}, Lcom/samsung/samm/b/a/j;->b()I

    move-result v1

    move v2, v1

    goto :goto_2

    .line 4111
    :cond_4
    const/4 v1, 0x2

    if-ne v2, v1, :cond_5

    .line 4112
    new-instance v2, Lcom/samsung/samm/a/i;

    invoke-direct {v2}, Lcom/samsung/samm/a/i;-><init>()V

    .line 4113
    new-instance v1, Lcom/samsung/samm/b/a/o;

    invoke-direct {v1, p4, v2}, Lcom/samsung/samm/b/a/o;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    goto :goto_3

    .line 4115
    :cond_5
    const/4 v1, 0x3

    if-ne v2, v1, :cond_6

    .line 4116
    new-instance v2, Lcom/samsung/samm/a/g;

    invoke-direct {v2, p5}, Lcom/samsung/samm/a/g;-><init>(I)V

    .line 4117
    new-instance v1, Lcom/samsung/samm/b/a/m;

    invoke-direct {v1, p4, v2}, Lcom/samsung/samm/b/a/m;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    goto :goto_3

    .line 4119
    :cond_6
    const/4 v1, 0x4

    if-ne v2, v1, :cond_7

    .line 4120
    new-instance v2, Lcom/samsung/samm/a/e;

    invoke-direct {v2}, Lcom/samsung/samm/a/e;-><init>()V

    .line 4121
    new-instance v1, Lcom/samsung/samm/b/a/k;

    invoke-direct {v1, p4, v2}, Lcom/samsung/samm/b/a/k;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    goto :goto_3

    .line 4123
    :cond_7
    const/4 v1, 0x5

    if-ne v2, v1, :cond_8

    .line 4124
    new-instance v2, Lcom/samsung/samm/a/f;

    invoke-direct {v2, p5}, Lcom/samsung/samm/a/f;-><init>(I)V

    .line 4125
    new-instance v1, Lcom/samsung/samm/b/a/l;

    invoke-direct {v1, p4, v2}, Lcom/samsung/samm/b/a/l;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    goto :goto_3

    .line 4127
    :cond_8
    const/4 v1, 0x6

    if-ne v2, v1, :cond_9

    .line 4128
    new-instance v2, Lcom/samsung/samm/a/j;

    invoke-direct {v2}, Lcom/samsung/samm/a/j;-><init>()V

    .line 4129
    new-instance v1, Lcom/samsung/samm/b/a/p;

    invoke-direct {v1, p4, v2}, Lcom/samsung/samm/b/a/p;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    goto :goto_3

    .line 4136
    :cond_9
    new-instance v6, Lcom/samsung/samm/a/h;

    invoke-direct {v6}, Lcom/samsung/samm/a/h;-><init>()V

    .line 4137
    new-instance v1, Lcom/samsung/samm/b/a/n;

    invoke-direct {v1, p4, v6}, Lcom/samsung/samm/b/a/n;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    .line 4138
    invoke-virtual {v1, v2}, Lcom/samsung/samm/b/a/j;->a(I)V

    .line 4141
    invoke-virtual {v1, p2, v0}, Lcom/samsung/samm/b/a/j;->b([BI)I

    move-result v6

    .line 4142
    if-gez v6, :cond_a

    .line 4143
    const/4 v0, -0x1

    goto/16 :goto_0

    .line 4147
    :cond_a
    add-int/2addr v0, v6

    .line 4150
    invoke-static {p2, v0, v4}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v0

    .line 4151
    const/4 v6, 0x0

    aget v4, v4, v6

    .line 4152
    add-int/2addr v0, v4

    .line 4156
    const-string v4, "SAMMLibraryCore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Skip Unsupported Object Type : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4090
    :goto_4
    add-int/lit8 v3, v3, 0x1

    move-object p4, v1

    goto/16 :goto_1

    .line 4169
    :cond_b
    add-int/2addr v4, v0

    .line 4173
    invoke-virtual {v1}, Lcom/samsung/samm/b/a/j;->b()I

    move-result v0

    const/4 v6, 0x5

    if-ne v0, v6, :cond_c

    move-object v0, v1

    .line 4174
    check-cast v0, Lcom/samsung/samm/b/a/l;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/l;->h()I

    move-result v0

    .line 4175
    add-int/2addr v3, v0

    move-object v0, v2

    .line 4177
    check-cast v0, Lcom/samsung/samm/a/f;

    invoke-virtual {v0}, Lcom/samsung/samm/a/f;->k()Ljava/util/LinkedList;

    move-result-object v0

    .line 4178
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_e

    move v0, v4

    .line 4179
    goto :goto_4

    .line 4182
    :cond_c
    invoke-virtual {v1}, Lcom/samsung/samm/b/a/j;->b()I

    move-result v0

    const/4 v6, 0x3

    if-ne v0, v6, :cond_d

    .line 4183
    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->a()I

    move-result v0

    const/4 v6, 0x2

    if-eq v0, v6, :cond_d

    move v0, v4

    .line 4184
    goto :goto_4

    .line 4185
    :cond_d
    invoke-virtual {v1}, Lcom/samsung/samm/b/a/j;->b()I

    move-result v0

    const/4 v6, 0x6

    if-ne v0, v6, :cond_e

    .line 4186
    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->a()I

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->a()I

    move-result v0

    const/4 v6, 0x1

    if-eq v0, v6, :cond_e

    move v0, v4

    .line 4187
    goto :goto_4

    :cond_e
    move v0, v3

    .line 4191
    invoke-virtual {p0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move v3, v0

    move v0, v4

    goto :goto_4
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 756
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_Image_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 778
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_Video_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 779
    packed-switch p2, :pswitch_data_0

    .line 796
    const/4 v0, 0x0

    .line 799
    :goto_0
    return-object v0

    .line 781
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ".3gp"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 784
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ".mp4"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 787
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ".ts"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 790
    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ".webm"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 793
    :pswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ".mkv"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 779
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;IILjava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const v2, 0xffff

    .line 855
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 856
    :cond_0
    const-string v1, "SAMMLibraryCore"

    const-string v2, "setAMSAppID: App ID Name string is invalid!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    :goto_0
    return v0

    .line 859
    :cond_1
    if-ltz p1, :cond_2

    if-gt p1, v2, :cond_2

    if-ltz p2, :cond_2

    if-le p2, v2, :cond_3

    .line 860
    :cond_2
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setAMSAppID Parameter is out of range!!! (VerMajor:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", VerMinor:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 863
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_4

    .line 864
    const-string v1, "SAMMLibraryCore"

    const-string v2, "setAMSAppID: App ID Name string length is out of bound!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 867
    :cond_4
    if-eqz p3, :cond_5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_5

    .line 868
    const-string v1, "SAMMLibraryCore"

    const-string v2, "setAMSAppID: Patch Version string length is out of bound!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 871
    :cond_5
    sput-object p0, Lcom/samsung/samm/b/a/d;->a:Ljava/lang/String;

    .line 872
    and-int v0, p1, v2

    sput v0, Lcom/samsung/samm/b/a/d;->b:I

    .line 873
    and-int v0, p2, v2

    sput v0, Lcom/samsung/samm/b/a/d;->c:I

    .line 874
    sput-object p3, Lcom/samsung/samm/b/a/d;->d:Ljava/lang/String;

    .line 875
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Ljava/util/LinkedList;II)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;II)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2458
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2476
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2458
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 2459
    instance-of v3, v0, Lcom/samsung/samm/a/g;

    if-eqz v3, :cond_3

    .line 2460
    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 2461
    const-string v3, "SAMM___LIBRARY___CONTENT___ID___KEY"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v3

    .line 2462
    if-gez v3, :cond_2

    move v0, v1

    .line 2463
    goto :goto_1

    .line 2464
    :cond_2
    if-ne p2, v3, :cond_0

    .line 2465
    const-string v4, "SAMM___LIBRARY___CONTENT___SIZE___KEY"

    invoke-virtual {v0, v4, p3}, Lcom/samsung/samm/a/d;->c(Ljava/lang/String;I)Z

    .line 2466
    const-string v4, "SAMM___LIBRARY___CONTENT___PATH___KEY"

    iget-object v5, p0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/samsung/samm/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2467
    check-cast v0, Lcom/samsung/samm/a/g;

    iget-object v4, p0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/samm/a/g;->d(Ljava/lang/String;)Z

    goto :goto_0

    .line 2469
    :cond_3
    instance-of v3, v0, Lcom/samsung/samm/a/f;

    if-eqz v3, :cond_0

    .line 2470
    check-cast v0, Lcom/samsung/samm/a/f;

    .line 2471
    invoke-virtual {v0}, Lcom/samsung/samm/a/f;->k()Ljava/util/LinkedList;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;II)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2472
    goto :goto_1
.end method

.method public static a(Ljava/util/LinkedList;Ljava/util/ArrayList;Lcom/samsung/samm/b/a/j;[ILandroid/graphics/Rect;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;",
            "Ljava/util/ArrayList",
            "<[B>;",
            "Lcom/samsung/samm/b/a/j;",
            "[I",
            "Landroid/graphics/Rect;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3996
    if-nez p0, :cond_0

    .line 4077
    :goto_0
    return v1

    .line 4000
    :cond_0
    invoke-virtual {p0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4077
    const/4 v1, 0x1

    goto :goto_0

    .line 4000
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 4003
    instance-of v2, v0, Lcom/samsung/samm/a/h;

    if-eqz v2, :cond_3

    .line 4004
    new-instance v2, Lcom/samsung/samm/b/a/n;

    invoke-direct {v2, p2, v0}, Lcom/samsung/samm/b/a/n;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    move-object v3, v2

    .line 4039
    :goto_2
    if-eqz p4, :cond_9

    .line 4040
    iget v0, p4, Landroid/graphics/Rect;->left:I

    neg-int v2, v0

    .line 4041
    iget v0, p4, Landroid/graphics/Rect;->top:I

    neg-int v0, v0

    .line 4045
    :goto_3
    invoke-virtual {v3, p3, v2, v0}, Lcom/samsung/samm/b/a/j;->a([III)[B

    move-result-object v0

    .line 4046
    if-eqz v0, :cond_8

    .line 4047
    array-length v2, v0

    .line 4049
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4050
    aget v0, p3, v1

    add-int/lit8 v0, v0, 0x1

    aput v0, p3, v1

    goto :goto_1

    .line 4007
    :cond_3
    instance-of v2, v0, Lcom/samsung/samm/a/i;

    if-eqz v2, :cond_4

    .line 4008
    new-instance v2, Lcom/samsung/samm/b/a/o;

    invoke-direct {v2, p2, v0}, Lcom/samsung/samm/b/a/o;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    move-object v3, v2

    .line 4010
    goto :goto_2

    .line 4011
    :cond_4
    instance-of v2, v0, Lcom/samsung/samm/a/g;

    if-eqz v2, :cond_5

    .line 4012
    new-instance v2, Lcom/samsung/samm/b/a/m;

    invoke-direct {v2, p2, v0}, Lcom/samsung/samm/b/a/m;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    move-object v3, v2

    .line 4014
    goto :goto_2

    .line 4015
    :cond_5
    instance-of v2, v0, Lcom/samsung/samm/a/e;

    if-eqz v2, :cond_6

    .line 4016
    new-instance v2, Lcom/samsung/samm/b/a/k;

    invoke-direct {v2, p2, v0}, Lcom/samsung/samm/b/a/k;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    move-object v3, v2

    .line 4018
    goto :goto_2

    .line 4019
    :cond_6
    instance-of v2, v0, Lcom/samsung/samm/a/j;

    if-eqz v2, :cond_7

    .line 4020
    new-instance v2, Lcom/samsung/samm/b/a/p;

    invoke-direct {v2, p2, v0}, Lcom/samsung/samm/b/a/p;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    move-object v3, v2

    .line 4022
    goto :goto_2

    .line 4023
    :cond_7
    instance-of v2, v0, Lcom/samsung/samm/a/f;

    if-eqz v2, :cond_1

    .line 4024
    new-instance v2, Lcom/samsung/samm/b/a/l;

    invoke-direct {v2, p2, v0}, Lcom/samsung/samm/b/a/l;-><init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V

    move-object v3, v2

    goto :goto_2

    .line 4053
    :cond_8
    const-string v0, "SAMMLibraryCore"

    const-string v2, "Write Object Data Error"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    goto :goto_3
.end method

.method private aa()Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3658
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    if-nez v0, :cond_0

    move v0, v1

    .line 3676
    :goto_0
    return v0

    .line 3661
    :cond_0
    iget v4, p0, Lcom/samsung/samm/b/a/d;->N:I

    .line 3662
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 3676
    goto :goto_0

    .line 3662
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 3663
    instance-of v3, v0, Lcom/samsung/samm/a/j;

    if-eqz v3, :cond_1

    .line 3664
    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 3665
    :cond_3
    const-string v3, "SAMM___LIBRARY___CONTENT___ID___KEY"

    const/4 v6, -0x1

    invoke-virtual {v0, v3, v6}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v6

    .line 3666
    if-gez v6, :cond_4

    move v0, v1

    .line 3667
    goto :goto_0

    :cond_4
    move v3, v1

    .line 3668
    :goto_1
    if-ge v3, v4, :cond_1

    .line 3669
    iget-object v7, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v7, v7, v3

    invoke-virtual {v7}, Lcom/samsung/samm/b/a/d$a;->b()I

    move-result v7

    .line 3670
    if-ne v3, v6, :cond_5

    if-eq v3, v7, :cond_5

    .line 3671
    const-string v8, "SAMM___LIBRARY___CONTENT___ID___KEY"

    invoke-virtual {v0, v8, v7}, Lcom/samsung/samm/a/d;->c(Ljava/lang/String;I)Z

    .line 3668
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private ab()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0xff

    const/4 v1, 0x0

    .line 3699
    iput-object v5, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    .line 3700
    new-array v0, v4, [Lcom/samsung/samm/b/a/d$a;

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    move v0, v1

    .line 3701
    :goto_0
    if-lt v0, v4, :cond_0

    .line 3705
    iput v1, p0, Lcom/samsung/samm/b/a/d;->L:I

    .line 3706
    iput v1, p0, Lcom/samsung/samm/b/a/d;->I:I

    .line 3709
    iput v1, p0, Lcom/samsung/samm/b/a/d;->J:I

    .line 3712
    iput-object v5, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    .line 3713
    new-array v0, v4, [Lcom/samsung/samm/b/a/d$a;

    iput-object v0, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    move v0, v1

    .line 3714
    :goto_1
    if-lt v0, v4, :cond_1

    .line 3718
    iput v1, p0, Lcom/samsung/samm/b/a/d;->N:I

    .line 3719
    iput v1, p0, Lcom/samsung/samm/b/a/d;->K:I

    .line 3720
    return-void

    .line 3703
    :cond_0
    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    new-instance v3, Lcom/samsung/samm/b/a/d$a;

    invoke-direct {v3, p0}, Lcom/samsung/samm/b/a/d$a;-><init>(Lcom/samsung/samm/b/a/d;)V

    aput-object v3, v2, v0

    .line 3701
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3716
    :cond_1
    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    new-instance v3, Lcom/samsung/samm/b/a/d$a;

    invoke-direct {v3, p0}, Lcom/samsung/samm/b/a/d$a;-><init>(Lcom/samsung/samm/b/a/d;)V

    aput-object v3, v2, v0

    .line 3714
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 769
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_Fill_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1612
    new-instance v0, Lcom/samsung/samm/b/a/f;

    invoke-direct {v0}, Lcom/samsung/samm/b/a/f;-><init>()V

    .line 1613
    invoke-virtual {v0, p0}, Lcom/samsung/samm/b/a/f;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1614
    const/4 v0, 0x0

    .line 1615
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/samm/b/a/f;->d()Z

    move-result v0

    goto :goto_0
.end method

.method private b(Ljava/util/LinkedList;I)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3515
    if-lez p2, :cond_1

    .line 3516
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3534
    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 3516
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 3517
    instance-of v2, v0, Lcom/samsung/samm/a/g;

    if-eqz v2, :cond_0

    .line 3518
    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    .line 3519
    const-string v2, "SAMM___LIBRARY___CONTENT___ID___KEY"

    const/4 v4, -0x1

    invoke-virtual {v0, v2, v4}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v4

    .line 3520
    if-gez v4, :cond_3

    move v0, v1

    .line 3521
    goto :goto_0

    :cond_3
    move v2, v1

    .line 3522
    :goto_1
    if-ge v2, p2, :cond_0

    .line 3523
    iget-object v5, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/d$a;->b()I

    move-result v5

    .line 3524
    if-ne v5, v4, :cond_4

    .line 3525
    if-eq v2, v5, :cond_4

    .line 3526
    const-string v5, "SAMM___LIBRARY___CONTENT___ID___KEY"

    invoke-virtual {v0, v5, v2}, Lcom/samsung/samm/a/d;->c(Ljava/lang/String;I)Z

    .line 3522
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static c(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 803
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_Video_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/util/LinkedList;I)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3549
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3564
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 3549
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 3550
    instance-of v2, v0, Lcom/samsung/samm/a/g;

    if-eqz v2, :cond_0

    .line 3551
    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    .line 3552
    const-string v2, "SAMM___LIBRARY___CONTENT___ID___KEY"

    const/4 v4, -0x1

    invoke-virtual {v0, v2, v4}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v4

    .line 3553
    if-gez v4, :cond_2

    move v0, v1

    .line 3554
    goto :goto_0

    :cond_2
    move v2, v1

    .line 3555
    :goto_1
    if-ge v2, p2, :cond_0

    .line 3556
    iget-object v5, p0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/d$a;->b()I

    move-result v5

    .line 3557
    if-ne v2, v4, :cond_3

    if-eq v2, v5, :cond_3

    .line 3558
    const-string v6, "SAMM___LIBRARY___CONTENT___ID___KEY"

    invoke-virtual {v0, v6, v5}, Lcom/samsung/samm/a/d;->c(Ljava/lang/String;I)Z

    .line 3555
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 888
    sget-object v0, Lcom/samsung/samm/b/a/d;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 945
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->T:Ljava/lang/String;

    return-object v0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->U:Ljava/lang/String;

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->V:Ljava/lang/String;

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->W:Ljava/lang/String;

    return-object v0
.end method

.method public E()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1012
    iget-boolean v1, p0, Lcom/samsung/samm/b/a/d;->X:Z

    if-eqz v1, :cond_0

    .line 1014
    iget-object v1, p0, Lcom/samsung/samm/b/a/d;->aq:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1017
    :cond_0
    return-object v0
.end method

.method public F()J
    .locals 2

    .prologue
    .line 1048
    iget-wide v0, p0, Lcom/samsung/samm/b/a/d;->Y:J

    return-wide v0
.end method

.method public G()I
    .locals 1

    .prologue
    .line 1068
    iget v0, p0, Lcom/samsung/samm/b/a/d;->ae:I

    return v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->Z:Ljava/lang/String;

    return-object v0
.end method

.method public I()I
    .locals 1

    .prologue
    .line 1113
    iget v0, p0, Lcom/samsung/samm/b/a/d;->aa:I

    return v0
.end method

.method public J()I
    .locals 1

    .prologue
    .line 1117
    iget v0, p0, Lcom/samsung/samm/b/a/d;->ab:I

    return v0
.end method

.method public K()I
    .locals 1

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/h;->M()I

    move-result v0

    return v0
.end method

.method public L()Lcom/samsung/samm/a/a;
    .locals 1

    .prologue
    .line 1153
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    return-object v0
.end method

.method public M()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    const-string v1, "SAMM___LIBRARY___TAG___KEY"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/util/LinkedList;)Ljava/util/LinkedList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3680
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 3681
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 3694
    :goto_0
    return-object v0

    .line 3684
    :cond_1
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 3694
    goto :goto_0

    .line 3684
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 3685
    instance-of v3, v0, Lcom/samsung/samm/a/g;

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_5

    .line 3686
    :cond_4
    instance-of v3, v0, Lcom/samsung/samm/a/e;

    if-nez v3, :cond_5

    .line 3687
    instance-of v3, v0, Lcom/samsung/samm/a/j;

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 3688
    :cond_5
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3689
    :cond_6
    instance-of v3, v0, Lcom/samsung/samm/a/f;

    if-eqz v3, :cond_2

    .line 3690
    check-cast v0, Lcom/samsung/samm/a/f;

    invoke-virtual {v0}, Lcom/samsung/samm/a/f;->k()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 360
    iput p1, p0, Lcom/samsung/samm/b/a/d;->A:I

    .line 361
    return-void
.end method

.method public a(Lcom/samsung/samm/a/a;)V
    .locals 0

    .prologue
    .line 1157
    iput-object p1, p0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    .line 1158
    return-void
.end method

.method public a()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 263
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 264
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 265
    if-nez v1, :cond_0

    .line 285
    :goto_0
    return v0

    .line 267
    :cond_0
    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    .line 269
    :goto_1
    array-length v3, v1

    if-lt v0, v3, :cond_1

    .line 285
    const/4 v0, 0x1

    goto :goto_0

    .line 271
    :cond_1
    aget-object v3, v1, v0

    .line 272
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 273
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 276
    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_2

    .line 277
    iget-boolean v5, p0, Lcom/samsung/samm/b/a/d;->av:Z

    if-eqz v5, :cond_3

    .line 278
    iget-object v4, p0, Lcom/samsung/samm/b/a/d;->au:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 269
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 280
    :cond_3
    invoke-static {v4}, Lcom/samsung/samm/b/a/s;->a(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public a(Landroid/graphics/Bitmap;)Z
    .locals 3

    .prologue
    .line 607
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->n:Lcom/samsung/samm/b/a/c;

    iget v1, p0, Lcom/samsung/samm/b/a/d;->F:I

    iget v2, p0, Lcom/samsung/samm/b/a/d;->G:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/samsung/samm/b/a/c;->a(Landroid/graphics/Bitmap;II)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1077
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const v1, 0xffff

    if-le v0, v1, :cond_0

    .line 1078
    const-string v0, "SAMMLibraryCore"

    const-string v1, "setAMSHypertext: Hypertext string length is out of bound!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    const/4 v0, 0x0

    .line 1082
    :goto_0
    return v0

    .line 1081
    :cond_0
    iput-object p1, p0, Lcom/samsung/samm/b/a/d;->Z:Ljava/lang/String;

    .line 1082
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/samsung/samm/a/c;IILjava/lang/String;Z)Z
    .locals 18

    .prologue
    .line 1621
    if-nez p1, :cond_0

    .line 1622
    const/4 v2, 0x0

    .line 2454
    :goto_0
    return v2

    .line 1624
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->N()V

    .line 1626
    const/4 v3, 0x0

    .line 1628
    :try_start_0
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1629
    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_23
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_22
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-nez v4, :cond_2

    .line 2444
    if-eqz v3, :cond_1

    .line 2445
    :try_start_1
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1630
    :cond_1
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 2446
    :catch_0
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1632
    :cond_2
    :try_start_2
    new-instance v8, Ljava/io/RandomAccessFile;

    const-string v4, "r"

    invoke-direct {v8, v2, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_23
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_22
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1637
    :try_start_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->ab()V

    .line 1642
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->g:Lcom/samsung/samm/b/a/f;

    invoke-virtual {v2, v8}, Lcom/samsung/samm/b/a/f;->b(Ljava/io/RandomAccessFile;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_d
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    if-nez v2, :cond_4

    .line 2444
    if-eqz v8, :cond_3

    .line 2445
    :try_start_4
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1643
    :cond_3
    :goto_2
    const/4 v2, 0x0

    goto :goto_0

    .line 2446
    :catch_1
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1648
    :cond_4
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2, v8}, Lcom/samsung/samm/b/a/h;->a(Ljava/io/RandomAccessFile;)Z
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_d
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v2

    if-nez v2, :cond_6

    .line 2444
    if-eqz v8, :cond_5

    .line 2445
    :try_start_6
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 1649
    :cond_5
    :goto_3
    const/4 v2, 0x0

    goto :goto_0

    .line 2446
    :catch_2
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1654
    :cond_6
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->M()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->e:I

    .line 1655
    if-eqz p2, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->e:I
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_d
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    .line 2444
    if-eqz v8, :cond_7

    .line 2445
    :try_start_8
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 1656
    :cond_7
    :goto_4
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2446
    :catch_3
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1659
    :cond_8
    const/4 v2, 0x0

    .line 1660
    const/4 v3, 0x0

    :try_start_9
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/samm/b/a/d;->f:[B

    .line 1662
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->e:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_9

    .line 1663
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->e:I
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_d
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_b

    .line 2444
    :cond_9
    if-eqz v8, :cond_a

    .line 2445
    :try_start_a
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 1664
    :cond_a
    :goto_5
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_4
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1669
    :cond_b
    :try_start_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->o()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/samm/b/a/d;->c(I)V

    .line 1670
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->p()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/samm/b/a/d;->d(I)V

    .line 1673
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->x()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/samm/b/a/d;->ae:I

    .line 1684
    if-nez p2, :cond_c

    .line 1685
    const/4 v3, 0x1

    move/from16 v0, p4

    if-ne v0, v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->I()I

    move-result v3

    if-lez v3, :cond_f

    .line 1686
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->m()I

    move-result v2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 1712
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->i:Ljava/util/LinkedList;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;)Ljava/util/LinkedList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    .line 1719
    if-nez p2, :cond_15

    .line 1720
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/c;->b(I)V

    .line 1721
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/c;->c(I)V

    .line 1722
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->g()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/c;->d(I)V

    .line 1723
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/c;->e(I)V

    .line 1725
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->d()I

    move-result v3

    .line 1726
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/h;->i()I

    move-result v4

    .line 1727
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/h;->j()I

    move-result v5

    .line 1725
    invoke-virtual {v2, v8, v3, v4, v5}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;III)Z
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_d
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result v2

    .line 1727
    if-nez v2, :cond_16

    .line 2444
    if-eqz v8, :cond_e

    .line 2445
    :try_start_c
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 1728
    :cond_e
    :goto_6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1688
    :cond_f
    :try_start_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->m()I

    move-result v3

    new-array v4, v3, [B
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 1690
    :try_start_e
    invoke-virtual {v8, v4}, Ljava/io/RandomAccessFile;->read([B)I
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1696
    :goto_7
    :try_start_f
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->e:I

    if-nez v3, :cond_12

    .line 1697
    const/4 v2, 0x1

    new-array v3, v2, [I

    .line 1698
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/h;->n()I

    move-result v5

    aput v5, v3, v2

    .line 1700
    const/4 v6, 0x0

    .line 1701
    const/4 v5, 0x0

    .line 1702
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->i:Ljava/util/LinkedList;

    move/from16 v7, p3

    invoke-static/range {v2 .. v7}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;[I[BILcom/samsung/samm/b/a/j;I)I
    :try_end_f
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_6
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_d
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    move-result v2

    if-gez v2, :cond_d

    .line 2444
    if-eqz v8, :cond_10

    .line 2445
    :try_start_10
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_7

    .line 1703
    :cond_10
    :goto_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1691
    :catch_5
    move-exception v3

    .line 1693
    :try_start_11
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_11
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_6
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_d
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto :goto_7

    .line 2434
    :catch_6
    move-exception v2

    move-object v3, v8

    .line 2435
    :goto_9
    :try_start_12
    const-string v4, "SAMMLibraryCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FileNotFoundException : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2436
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    .line 2444
    if-eqz v3, :cond_11

    .line 2445
    :try_start_13
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_1e

    .line 2437
    :cond_11
    :goto_a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_7
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 1706
    :cond_12
    :try_start_14
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->e:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_13

    .line 1707
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->e:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_d

    .line 1708
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->f:[B

    aget-byte v2, v3, v2
    :try_end_14
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_14} :catch_6
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_d
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    if-lez v2, :cond_d

    .line 2444
    if-eqz v8, :cond_14

    .line 2445
    :try_start_15
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_8

    .line 1709
    :cond_14
    :goto_b
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_8
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 2446
    :catch_9
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_6

    .line 1731
    :cond_15
    :try_start_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->j()I

    move-result v2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 1739
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->k()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->L:I

    .line 1740
    const/4 v2, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->L:I

    if-lt v2, v3, :cond_18

    .line 1764
    if-nez p2, :cond_1e

    .line 1765
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->v:Lcom/samsung/samm/b/a/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->q()I

    move-result v3

    .line 1766
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/h;->r()I

    move-result v4

    .line 1767
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/h;->s()I

    move-result v5

    .line 1765
    invoke-virtual {v2, v8, v3, v4, v5}, Lcom/samsung/samm/b/a/b;->a(Ljava/io/RandomAccessFile;III)Z
    :try_end_16
    .catch Ljava/io/FileNotFoundException; {:try_start_16 .. :try_end_16} :catch_6
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_d
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    move-result v2

    .line 1767
    if-nez v2, :cond_1f

    .line 2444
    if-eqz v8, :cond_17

    .line 2445
    :try_start_17
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_c

    .line 1768
    :cond_17
    :goto_d
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1742
    :cond_18
    :try_start_18
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v3

    .line 1744
    if-nez p2, :cond_1c

    .line 1746
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 1748
    invoke-static {v8, v4, v3}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_18
    .catch Ljava/io/FileNotFoundException; {:try_start_18 .. :try_end_18} :catch_6
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_d
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    move-result v4

    if-nez v4, :cond_1a

    .line 2444
    if-eqz v8, :cond_19

    .line 2445
    :try_start_19
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_a

    .line 1749
    :cond_19
    :goto_e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_a
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 1753
    :cond_1a
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->i:Ljava/util/LinkedList;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2, v3}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;II)Z
    :try_end_1a
    .catch Ljava/io/FileNotFoundException; {:try_start_1a .. :try_end_1a} :catch_6
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_d
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    move-result v3

    if-nez v3, :cond_1d

    .line 2444
    if-eqz v8, :cond_1b

    .line 2445
    :try_start_1b
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_b

    .line 1754
    :cond_1b
    :goto_f
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_b
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 1756
    :cond_1c
    :try_start_1c
    invoke-virtual {v8, v3}, Ljava/io/RandomAccessFile;->skipBytes(I)I
    :try_end_1c
    .catch Ljava/io/FileNotFoundException; {:try_start_1c .. :try_end_1c} :catch_6
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_d
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    .line 1740
    :cond_1d
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_c

    .line 2446
    :catch_c
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_d

    .line 1771
    :cond_1e
    :try_start_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->s()I

    move-result v2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 1778
    :cond_1f
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->P:Ljava/lang/String;

    .line 1779
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->t()I

    move-result v4

    .line 1780
    if-lez v4, :cond_21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->u()I

    move-result v2

    if-lez v2, :cond_21

    .line 1781
    if-eqz p2, :cond_20

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/samsung/samm/a/c;->i:Z

    if-eqz v2, :cond_2c

    .line 1782
    :cond_20
    new-array v5, v4, [C

    .line 1783
    mul-int/lit8 v2, v4, 0x2

    new-array v6, v2, [B

    .line 1784
    const/4 v3, 0x0

    .line 1785
    const/4 v2, 0x1

    new-array v7, v2, [I

    .line 1786
    invoke-virtual {v8, v6}, Ljava/io/RandomAccessFile;->read([B)I

    .line 1787
    const/4 v2, 0x0

    :goto_10
    if-lt v2, v4, :cond_2b

    .line 1791
    invoke-static {v5}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->P:Ljava/lang/String;

    .line 1824
    :cond_21
    :goto_11
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->Q:Ljava/lang/String;

    .line 1825
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->R:I

    .line 1826
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->S:I

    .line 1827
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->T:Ljava/lang/String;

    .line 1828
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->u()I

    move-result v2

    if-lez v2, :cond_33

    .line 1829
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v4

    .line 1830
    if-lez v4, :cond_23

    .line 1831
    if-eqz p2, :cond_22

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/samsung/samm/a/c;->j:Z

    if-eqz v2, :cond_2f

    .line 1832
    :cond_22
    new-array v5, v4, [C

    .line 1833
    mul-int/lit8 v2, v4, 0x2

    new-array v6, v2, [B

    .line 1834
    const/4 v3, 0x0

    .line 1835
    const/4 v2, 0x1

    new-array v7, v2, [I

    .line 1836
    invoke-virtual {v8, v6}, Ljava/io/RandomAccessFile;->read([B)I

    .line 1837
    const/4 v2, 0x0

    :goto_12
    if-lt v2, v4, :cond_2e

    .line 1841
    invoke-static {v5}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->Q:Ljava/lang/String;

    .line 1847
    :cond_23
    :goto_13
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->R:I

    .line 1848
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->S:I

    .line 1850
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v4

    .line 1851
    if-lez v4, :cond_25

    .line 1852
    if-eqz p2, :cond_24

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/samsung/samm/a/c;->j:Z

    if-eqz v2, :cond_32

    .line 1853
    :cond_24
    new-array v5, v4, [C

    .line 1854
    mul-int/lit8 v2, v4, 0x2

    new-array v6, v2, [B

    .line 1855
    const/4 v3, 0x0

    .line 1856
    const/4 v2, 0x1

    new-array v7, v2, [I

    .line 1857
    invoke-virtual {v8, v6}, Ljava/io/RandomAccessFile;->read([B)I

    .line 1858
    const/4 v2, 0x0

    :goto_14
    if-lt v2, v4, :cond_31

    .line 1862
    invoke-static {v5}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->T:Ljava/lang/String;

    .line 1878
    :cond_25
    :goto_15
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->U:Ljava/lang/String;

    .line 1879
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->V:Ljava/lang/String;

    .line 1880
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->W:Ljava/lang/String;

    .line 1881
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/samm/b/a/d;->X:Z

    .line 1882
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->v()I

    move-result v2

    if-lez v2, :cond_39

    .line 1883
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v5

    .line 1884
    if-eqz p2, :cond_26

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/samsung/samm/a/c;->k:Z

    if-eqz v2, :cond_3c

    .line 1887
    :cond_26
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v6

    .line 1888
    const/4 v2, 0x2

    .line 1889
    if-lez v6, :cond_27

    .line 1890
    new-array v7, v6, [C

    .line 1891
    mul-int/lit8 v2, v6, 0x2

    new-array v9, v2, [B

    .line 1892
    const/4 v4, 0x0

    .line 1893
    const/4 v2, 0x1

    new-array v10, v2, [I

    .line 1894
    invoke-virtual {v8, v9}, Ljava/io/RandomAccessFile;->read([B)I

    .line 1895
    array-length v2, v9

    add-int/lit8 v2, v2, 0x2

    .line 1896
    const/4 v3, 0x0

    :goto_16
    if-lt v3, v6, :cond_34

    .line 1900
    invoke-static {v7}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/samm/b/a/d;->U:Ljava/lang/String;

    .line 1903
    :cond_27
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v6

    .line 1904
    add-int/lit8 v2, v2, 0x2

    .line 1905
    if-lez v6, :cond_28

    .line 1906
    new-array v7, v6, [C

    .line 1907
    mul-int/lit8 v3, v6, 0x2

    new-array v9, v3, [B

    .line 1908
    const/4 v4, 0x0

    .line 1909
    const/4 v3, 0x1

    new-array v10, v3, [I

    .line 1910
    invoke-virtual {v8, v9}, Ljava/io/RandomAccessFile;->read([B)I

    .line 1911
    array-length v3, v9

    add-int/2addr v2, v3

    .line 1912
    const/4 v3, 0x0

    :goto_17
    if-lt v3, v6, :cond_35

    .line 1916
    invoke-static {v7}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/samm/b/a/d;->V:Ljava/lang/String;

    .line 1919
    :cond_28
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v6

    .line 1920
    add-int/lit8 v2, v2, 0x2

    .line 1921
    if-lez v6, :cond_29

    .line 1922
    new-array v7, v6, [C

    .line 1923
    mul-int/lit8 v3, v6, 0x2

    new-array v9, v3, [B

    .line 1924
    const/4 v4, 0x0

    .line 1925
    const/4 v3, 0x1

    new-array v10, v3, [I

    .line 1926
    invoke-virtual {v8, v9}, Ljava/io/RandomAccessFile;->read([B)I

    .line 1927
    array-length v3, v9

    add-int/2addr v2, v3

    .line 1928
    const/4 v3, 0x0

    :goto_18
    if-lt v3, v6, :cond_36

    .line 1932
    invoke-static {v7}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/samm/b/a/d;->W:Ljava/lang/String;

    .line 1935
    :cond_29
    if-le v5, v2, :cond_39

    .line 1937
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v3

    .line 1938
    add-int/lit8 v2, v2, 0x4

    .line 1939
    if-lez v3, :cond_38

    .line 1940
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->aq:Ljava/lang/String;

    .line 1941
    invoke-static {v8, v4, v3}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_1d
    .catch Ljava/io/FileNotFoundException; {:try_start_1d .. :try_end_1d} :catch_6
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    move-result v4

    if-nez v4, :cond_37

    .line 2444
    if-eqz v8, :cond_2a

    .line 2445
    :try_start_1e
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_e

    .line 1942
    :cond_2a
    :goto_19
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1788
    :cond_2b
    :try_start_1f
    invoke-static {v6, v3, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 1789
    const/4 v9, 0x0

    aget v9, v7, v9

    int-to-char v9, v9

    aput-char v9, v5, v2

    .line 1787
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_10

    .line 1793
    :cond_2c
    mul-int/lit8 v2, v4, 0x2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I
    :try_end_1f
    .catch Ljava/io/FileNotFoundException; {:try_start_1f .. :try_end_1f} :catch_6
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_d
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    goto/16 :goto_11

    .line 2438
    :catch_d
    move-exception v2

    .line 2439
    :goto_1a
    :try_start_20
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2440
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    .line 2444
    if-eqz v8, :cond_2d

    .line 2445
    :try_start_21
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_1f

    .line 2441
    :cond_2d
    :goto_1b
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1838
    :cond_2e
    :try_start_22
    invoke-static {v6, v3, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 1839
    const/4 v9, 0x0

    aget v9, v7, v9

    int-to-char v9, v9

    aput-char v9, v5, v2

    .line 1837
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_12

    .line 1843
    :cond_2f
    mul-int/lit8 v2, v4, 0x2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I
    :try_end_22
    .catch Ljava/io/FileNotFoundException; {:try_start_22 .. :try_end_22} :catch_6
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_d
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    goto/16 :goto_13

    .line 2442
    :catchall_0
    move-exception v2

    .line 2444
    :goto_1c
    if-eqz v8, :cond_30

    .line 2445
    :try_start_23
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_23} :catch_20

    .line 2450
    :cond_30
    :goto_1d
    throw v2

    .line 1859
    :cond_31
    :try_start_24
    invoke-static {v6, v3, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 1860
    const/4 v9, 0x0

    aget v9, v7, v9

    int-to-char v9, v9

    aput-char v9, v5, v2

    .line 1858
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_14

    .line 1864
    :cond_32
    mul-int/lit8 v2, v4, 0x2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto/16 :goto_15

    .line 1869
    :cond_33
    const-string v2, "ChatON"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->Q:Ljava/lang/String;

    .line 1870
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->R:I

    .line 1871
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->S:I

    goto/16 :goto_15

    .line 1897
    :cond_34
    invoke-static {v9, v4, v10}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v4

    .line 1898
    const/4 v11, 0x0

    aget v11, v10, v11

    int-to-char v11, v11

    aput-char v11, v7, v3

    .line 1896
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_16

    .line 1913
    :cond_35
    invoke-static {v9, v4, v10}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v4

    .line 1914
    const/4 v11, 0x0

    aget v11, v10, v11

    int-to-char v11, v11

    aput-char v11, v7, v3

    .line 1912
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_17

    .line 1929
    :cond_36
    invoke-static {v9, v4, v10}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v4

    .line 1930
    const/4 v11, 0x0

    aget v11, v10, v11

    int-to-char v11, v11

    aput-char v11, v7, v3
    :try_end_24
    .catch Ljava/io/FileNotFoundException; {:try_start_24 .. :try_end_24} :catch_6
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_d
    .catchall {:try_start_24 .. :try_end_24} :catchall_0

    .line 1928
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_18

    .line 2446
    :catch_e
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_19

    .line 1943
    :cond_37
    add-int/2addr v2, v3

    .line 1944
    const/4 v3, 0x1

    :try_start_25
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/samm/b/a/d;->X:Z

    .line 1947
    :cond_38
    sub-int v2, v5, v2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 1958
    :cond_39
    :goto_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->w()I

    move-result v2

    if-lez v2, :cond_3e

    .line 1959
    if-eqz p2, :cond_3a

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/samsung/samm/a/c;->l:Z

    if-eqz v2, :cond_3d

    .line 1960
    :cond_3a
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/samm/b/a/d;->Y:J

    .line 1972
    :goto_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->y()I

    move-result v2

    if-lez v2, :cond_40

    .line 1973
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->x:Lcom/samsung/samm/b/a/q;

    if-nez p2, :cond_3f

    const/4 v2, 0x1

    :goto_20
    invoke-virtual {v3, v8, v2}, Lcom/samsung/samm/b/a/q;->a(Ljava/io/RandomAccessFile;Z)Z
    :try_end_25
    .catch Ljava/io/FileNotFoundException; {:try_start_25 .. :try_end_25} :catch_6
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_25} :catch_d
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    move-result v2

    if-nez v2, :cond_40

    .line 2444
    if-eqz v8, :cond_3b

    .line 2445
    :try_start_26
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_f

    .line 1974
    :cond_3b
    :goto_21
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1950
    :cond_3c
    :try_start_27
    invoke-virtual {v8, v5}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto :goto_1e

    .line 1962
    :cond_3d
    const/16 v2, 0x8

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto :goto_1f

    .line 1965
    :cond_3e
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/samm/b/a/d;->Y:J
    :try_end_27
    .catch Ljava/io/FileNotFoundException; {:try_start_27 .. :try_end_27} :catch_6
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_27} :catch_d
    .catchall {:try_start_27 .. :try_end_27} :catchall_0

    goto :goto_1f

    .line 1973
    :cond_3f
    const/4 v2, 0x0

    goto :goto_20

    .line 2446
    :catch_f
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_21

    .line 1980
    :cond_40
    :try_start_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->z()I

    move-result v2

    if-lez v2, :cond_43

    .line 1981
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->w:Lcom/samsung/samm/b/a/a;

    if-nez p2, :cond_42

    const/4 v2, 0x1

    :goto_22
    invoke-virtual {v3, v8, v2}, Lcom/samsung/samm/b/a/a;->a(Ljava/io/RandomAccessFile;Z)Z
    :try_end_28
    .catch Ljava/io/FileNotFoundException; {:try_start_28 .. :try_end_28} :catch_6
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_d
    .catchall {:try_start_28 .. :try_end_28} :catchall_0

    move-result v2

    if-nez v2, :cond_43

    .line 2444
    if-eqz v8, :cond_41

    .line 2445
    :try_start_29
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_29} :catch_10

    .line 1982
    :cond_41
    :goto_23
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1981
    :cond_42
    const/4 v2, 0x0

    goto :goto_22

    .line 2446
    :catch_10
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_23

    .line 1989
    :cond_43
    const/4 v2, 0x0

    :try_start_2a
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->Z:Ljava/lang/String;

    .line 1990
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->A()I

    move-result v2

    if-lez v2, :cond_45

    .line 1992
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v4

    .line 1993
    if-lez v4, :cond_45

    .line 1994
    if-eqz p2, :cond_44

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/samsung/samm/a/c;->m:Z

    if-eqz v2, :cond_4a

    .line 1996
    :cond_44
    new-array v5, v4, [C

    .line 1997
    mul-int/lit8 v2, v4, 0x2

    new-array v6, v2, [B

    .line 1998
    const/4 v3, 0x0

    .line 1999
    const/4 v2, 0x1

    new-array v7, v2, [I

    .line 2000
    invoke-virtual {v8, v6}, Ljava/io/RandomAccessFile;->read([B)I

    .line 2001
    const/4 v2, 0x0

    :goto_24
    if-lt v2, v4, :cond_49

    .line 2005
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>([C)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->Z:Ljava/lang/String;

    .line 2016
    :cond_45
    :goto_25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->B()I

    move-result v2

    if-lez v2, :cond_4c

    .line 2017
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2018
    if-eqz p2, :cond_46

    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/samsung/samm/a/c;->n:Z

    if-eqz v3, :cond_4b

    .line 2020
    :cond_46
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/samm/b/a/d;->aa:I

    .line 2021
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/samm/b/a/d;->ab:I

    .line 2022
    const/16 v3, 0x8

    .line 2023
    if-le v2, v3, :cond_47

    .line 2024
    add-int/lit8 v2, v2, -0x8

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 2038
    :cond_47
    :goto_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->C()I

    move-result v2

    if-lez v2, :cond_50

    .line 2039
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    if-nez p2, :cond_4d

    const/4 v2, 0x1

    move v3, v2

    :goto_27
    if-eqz p2, :cond_4e

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/samsung/samm/a/c;->o:Z

    if-eqz v2, :cond_4e

    const/4 v2, 0x1

    :goto_28
    invoke-virtual {v4, v8, v3, v2}, Lcom/samsung/samm/b/a/e;->a(Ljava/io/RandomAccessFile;ZZ)I
    :try_end_2a
    .catch Ljava/io/FileNotFoundException; {:try_start_2a .. :try_end_2a} :catch_6
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_2a} :catch_d
    .catchall {:try_start_2a .. :try_end_2a} :catchall_0

    move-result v2

    if-gez v2, :cond_4f

    .line 2444
    if-eqz v8, :cond_48

    .line 2445
    :try_start_2b
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_2b} :catch_11

    .line 2040
    :cond_48
    :goto_29
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2002
    :cond_49
    :try_start_2c
    invoke-static {v6, v3, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 2003
    const/4 v9, 0x0

    aget v9, v7, v9

    int-to-char v9, v9

    aput-char v9, v5, v2

    .line 2001
    add-int/lit8 v2, v2, 0x1

    goto :goto_24

    .line 2007
    :cond_4a
    mul-int/lit8 v2, v4, 0x2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto :goto_25

    .line 2027
    :cond_4b
    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto :goto_26

    .line 2030
    :cond_4c
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->aa:I

    .line 2031
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->ab:I
    :try_end_2c
    .catch Ljava/io/FileNotFoundException; {:try_start_2c .. :try_end_2c} :catch_6
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_2c} :catch_d
    .catchall {:try_start_2c .. :try_end_2c} :catchall_0

    goto :goto_26

    .line 2039
    :cond_4d
    const/4 v2, 0x0

    move v3, v2

    goto :goto_27

    :cond_4e
    const/4 v2, 0x0

    goto :goto_28

    .line 2446
    :catch_11
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_29

    .line 2043
    :cond_4f
    :try_start_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    const-string v3, "SAMM___LIBRARY___TAG___KEY"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2044
    if-eqz v3, :cond_50

    .line 2045
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->z:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 2046
    const/4 v2, 0x0

    :goto_2a
    array-length v4, v3

    if-lt v2, v4, :cond_52

    .line 2056
    :cond_50
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/samm/b/a/d;->ac:Z

    .line 2057
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->D()I

    move-result v2

    if-lez v2, :cond_54

    .line 2058
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2059
    if-lez v2, :cond_54

    .line 2060
    if-nez p2, :cond_58

    .line 2061
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->ar:Ljava/lang/String;

    .line 2062
    invoke-static {v8, v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_2d
    .catch Ljava/io/FileNotFoundException; {:try_start_2d .. :try_end_2d} :catch_6
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_2d} :catch_d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_0

    move-result v2

    if-nez v2, :cond_53

    .line 2444
    if-eqz v8, :cond_51

    .line 2445
    :try_start_2e
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_2e} :catch_12

    .line 2063
    :cond_51
    :goto_2b
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2047
    :cond_52
    :try_start_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->z:Ljava/util/LinkedList;

    aget-object v5, v3, v2

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2f
    .catch Ljava/io/FileNotFoundException; {:try_start_2f .. :try_end_2f} :catch_6
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_2f} :catch_d
    .catchall {:try_start_2f .. :try_end_2f} :catchall_0

    .line 2046
    add-int/lit8 v2, v2, 0x1

    goto :goto_2a

    .line 2446
    :catch_12
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2b

    .line 2064
    :cond_53
    const/4 v2, 0x1

    :try_start_30
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/samm/b/a/d;->ac:Z

    .line 2075
    :cond_54
    :goto_2c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->E()I

    move-result v2

    if-lez v2, :cond_5b

    .line 2077
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v4

    .line 2078
    if-lez v4, :cond_56

    .line 2079
    if-eqz p2, :cond_55

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/samsung/samm/a/c;->p:Z

    if-eqz v2, :cond_5a

    .line 2081
    :cond_55
    new-array v5, v4, [C

    .line 2082
    mul-int/lit8 v2, v4, 0x2

    new-array v6, v2, [B

    .line 2083
    const/4 v3, 0x0

    .line 2084
    const/4 v2, 0x1

    new-array v7, v2, [I

    .line 2085
    invoke-virtual {v8, v6}, Ljava/io/RandomAccessFile;->read([B)I

    .line 2086
    const/4 v2, 0x0

    :goto_2d
    if-lt v2, v4, :cond_59

    .line 2090
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>([C)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->af:Ljava/lang/String;

    .line 2103
    :cond_56
    :goto_2e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->F()I

    move-result v2

    if-lez v2, :cond_5d

    .line 2104
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2105
    if-lez v2, :cond_5d

    .line 2106
    if-nez p2, :cond_5c

    .line 2107
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->o:Lcom/samsung/samm/b/a/c;

    const/4 v4, 0x4

    .line 2108
    const/4 v5, 0x0

    .line 2107
    invoke-virtual {v3, v8, v4, v5, v2}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;III)Z
    :try_end_30
    .catch Ljava/io/FileNotFoundException; {:try_start_30 .. :try_end_30} :catch_6
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_30} :catch_d
    .catchall {:try_start_30 .. :try_end_30} :catchall_0

    move-result v2

    .line 2109
    if-nez v2, :cond_5d

    .line 2444
    if-eqz v8, :cond_57

    .line 2445
    :try_start_31
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_31} :catch_13

    .line 2110
    :cond_57
    :goto_2f
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2066
    :cond_58
    :try_start_32
    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto :goto_2c

    .line 2087
    :cond_59
    invoke-static {v6, v3, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 2088
    const/4 v9, 0x0

    aget v9, v7, v9

    int-to-char v9, v9

    aput-char v9, v5, v2

    .line 2086
    add-int/lit8 v2, v2, 0x1

    goto :goto_2d

    .line 2092
    :cond_5a
    mul-int/lit8 v2, v4, 0x2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto :goto_2e

    .line 2096
    :cond_5b
    const-string v2, "1.2Before"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->af:Ljava/lang/String;
    :try_end_32
    .catch Ljava/io/FileNotFoundException; {:try_start_32 .. :try_end_32} :catch_6
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_32} :catch_d
    .catchall {:try_start_32 .. :try_end_32} :catchall_0

    goto :goto_2e

    .line 2446
    :catch_13
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2f

    .line 2113
    :cond_5c
    :try_start_33
    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 2122
    :cond_5d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->G()I

    move-result v2

    if-lez v2, :cond_5e

    .line 2123
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2124
    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 2131
    :cond_5e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->H()I

    move-result v2

    if-lez v2, :cond_5f

    .line 2132
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v4

    .line 2133
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    .line 2134
    if-lez v4, :cond_5f

    .line 2135
    if-nez p2, :cond_6c

    .line 2137
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v5

    .line 2138
    const/4 v2, 0x0

    move v3, v2

    :goto_30
    if-lt v3, v5, :cond_65

    .line 2180
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v2

    sub-long/2addr v2, v6

    long-to-int v2, v2

    .line 2182
    if-le v4, v2, :cond_5f

    .line 2186
    sub-int v2, v4, v2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 2198
    :cond_5f
    :goto_31
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->I()I

    move-result v2

    if-lez v2, :cond_62

    .line 2199
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2200
    if-lez v2, :cond_62

    .line 2201
    if-eqz p2, :cond_60

    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/samsung/samm/a/c;->q:Z

    if-eqz v3, :cond_61

    .line 2202
    :cond_60
    if-nez p4, :cond_6d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/h;->n()I

    move-result v3

    if-lez v3, :cond_6d

    .line 2203
    :cond_61
    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 2218
    :cond_62
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->J()I

    move-result v2

    if-lez v2, :cond_70

    .line 2219
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2220
    if-lez v2, :cond_70

    .line 2221
    if-eqz p2, :cond_63

    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/samsung/samm/a/c;->c:Z

    if-eqz v3, :cond_6f

    .line 2222
    :cond_63
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->u:Lcom/samsung/samm/b/a/c;

    const/4 v4, 0x4

    .line 2223
    const/4 v5, 0x0

    .line 2222
    invoke-virtual {v3, v8, v4, v5, v2}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;III)Z
    :try_end_33
    .catch Ljava/io/FileNotFoundException; {:try_start_33 .. :try_end_33} :catch_6
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_33} :catch_d
    .catchall {:try_start_33 .. :try_end_33} :catchall_0

    move-result v2

    .line 2224
    if-nez v2, :cond_70

    .line 2444
    if-eqz v8, :cond_64

    .line 2445
    :try_start_34
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_34} :catch_16

    .line 2225
    :cond_64
    :goto_32
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2140
    :cond_65
    :try_start_35
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v9

    .line 2142
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v10

    .line 2145
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2148
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    invoke-static {v12, v3}, Lcom/samsung/samm/b/a/d;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v12

    .line 2150
    invoke-static {v8, v12, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_35
    .catch Ljava/io/FileNotFoundException; {:try_start_35 .. :try_end_35} :catch_6
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_35} :catch_d
    .catchall {:try_start_35 .. :try_end_35} :catchall_0

    move-result v2

    if-nez v2, :cond_67

    .line 2444
    if-eqz v8, :cond_66

    .line 2445
    :try_start_36
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_36} :catch_14

    .line 2151
    :cond_66
    :goto_33
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_14
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_33

    .line 2154
    :cond_67
    :try_start_37
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_68
    :goto_34
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_6a

    .line 2169
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v12

    sub-long v10, v12, v10

    long-to-int v2, v10

    .line 2172
    if-le v9, v2, :cond_69

    .line 2176
    sub-int v2, v9, v2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 2138
    :cond_69
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_30

    .line 2154
    :cond_6a
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/samm/a/d;

    .line 2155
    instance-of v14, v2, Lcom/samsung/samm/a/e;

    if-eqz v14, :cond_68

    .line 2156
    const-string v14, "SAMM___LIBRARY___CONTENT___ID___KEY"

    const/4 v15, -0x1

    invoke-virtual {v2, v14, v15}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v14

    .line 2157
    if-ltz v14, :cond_68

    .line 2159
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/samm/b/a/d;->J:I

    if-gt v15, v14, :cond_6b

    .line 2160
    add-int/lit8 v15, v14, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/samm/b/a/d;->J:I

    .line 2161
    :cond_6b
    if-ne v3, v14, :cond_68

    .line 2163
    const-string v14, "SAMM___LIBRARY___CONTENT___PATH___KEY"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    invoke-virtual {v2, v14, v15}, Lcom/samsung/samm/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2164
    const-string v14, "SAMM___LIBRARY___FILL___IMAGE___PATH___KEY"

    invoke-virtual {v2, v14, v12}, Lcom/samsung/samm/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_34

    .line 2189
    :cond_6c
    invoke-virtual {v8, v4}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto/16 :goto_31

    .line 2205
    :cond_6d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->n:Lcom/samsung/samm/b/a/c;

    const/4 v4, 0x4

    .line 2206
    const/4 v5, 0x0

    .line 2205
    invoke-virtual {v3, v8, v4, v5, v2}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;III)Z
    :try_end_37
    .catch Ljava/io/FileNotFoundException; {:try_start_37 .. :try_end_37} :catch_6
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_37} :catch_d
    .catchall {:try_start_37 .. :try_end_37} :catchall_0

    move-result v2

    .line 2207
    if-nez v2, :cond_62

    .line 2444
    if-eqz v8, :cond_6e

    .line 2445
    :try_start_38
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_38} :catch_15

    .line 2208
    :cond_6e
    :goto_35
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_15
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_35

    .line 2446
    :catch_16
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_32

    .line 2228
    :cond_6f
    :try_start_39
    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 2237
    :cond_70
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->K()I

    move-result v2

    if-lez v2, :cond_71

    .line 2238
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v7

    .line 2239
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v10

    .line 2240
    if-lez v7, :cond_71

    .line 2241
    if-nez p2, :cond_88

    .line 2243
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v9

    .line 2244
    const/4 v2, 0x0

    move v6, v2

    :goto_36
    if-lt v6, v9, :cond_74

    .line 2319
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v2

    sub-long/2addr v2, v10

    long-to-int v2, v2

    .line 2321
    if-le v7, v2, :cond_71

    .line 2325
    sub-int v2, v7, v2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 2337
    :cond_71
    :goto_37
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->L()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_72

    .line 2338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->L()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_8b

    .line 2340
    :cond_72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->L()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_89

    .line 2341
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/samm/b/a/d;->q:Z

    .line 2347
    :goto_38
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2348
    if-lez v2, :cond_8b

    .line 2349
    if-nez p2, :cond_92

    .line 2351
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2352
    if-lez v2, :cond_8a

    .line 2354
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->p:Lcom/samsung/samm/b/a/c;

    const/4 v4, 0x4

    .line 2355
    const/4 v5, 0x0

    .line 2354
    invoke-virtual {v3, v8, v4, v5, v2}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;III)Z
    :try_end_39
    .catch Ljava/io/FileNotFoundException; {:try_start_39 .. :try_end_39} :catch_6
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_39} :catch_d
    .catchall {:try_start_39 .. :try_end_39} :catchall_0

    move-result v2

    .line 2356
    if-nez v2, :cond_8a

    .line 2444
    if-eqz v8, :cond_73

    .line 2445
    :try_start_3a
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3a
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3a} :catch_1d

    .line 2357
    :cond_73
    :goto_39
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2246
    :cond_74
    :try_start_3b
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v12

    .line 2248
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v14

    .line 2251
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v3

    .line 2254
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v13

    .line 2257
    const/4 v2, 0x0

    .line 2258
    if-eqz v3, :cond_95

    .line 2259
    if-lez v13, :cond_95

    .line 2260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    invoke-static {v2, v6, v3}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;II)Ljava/lang/String;
    :try_end_3b
    .catch Ljava/io/FileNotFoundException; {:try_start_3b .. :try_end_3b} :catch_6
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_3b} :catch_d
    .catchall {:try_start_3b .. :try_end_3b} :catchall_0

    move-result-object v2

    .line 2261
    if-nez v2, :cond_76

    .line 2444
    if-eqz v8, :cond_75

    .line 2445
    :try_start_3c
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_3c} :catch_17

    .line 2262
    :cond_75
    :goto_3a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_17
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3a

    .line 2263
    :cond_76
    :try_start_3d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->au:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/samm/b/a/d;->av:Z

    invoke-static {v8, v2, v13, v3, v4}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;ILandroid/content/Context;Z)Z
    :try_end_3d
    .catch Ljava/io/FileNotFoundException; {:try_start_3d .. :try_end_3d} :catch_6
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_3d} :catch_d
    .catchall {:try_start_3d .. :try_end_3d} :catchall_0

    move-result v3

    if-nez v3, :cond_78

    .line 2444
    if-eqz v8, :cond_77

    .line 2445
    :try_start_3e
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_3e} :catch_18

    .line 2264
    :cond_77
    :goto_3b
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_18
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3b

    :cond_78
    move-object v5, v2

    .line 2269
    :goto_3c
    :try_start_3f
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v3

    .line 2271
    const/4 v2, 0x0

    .line 2272
    if-lez v3, :cond_94

    .line 2273
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/samsung/samm/b/a/d;->c(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_3f
    .catch Ljava/io/FileNotFoundException; {:try_start_3f .. :try_end_3f} :catch_6
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_3f} :catch_d
    .catchall {:try_start_3f .. :try_end_3f} :catchall_0

    move-result-object v2

    .line 2274
    if-nez v2, :cond_7a

    .line 2444
    if-eqz v8, :cond_79

    .line 2445
    :try_start_40
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_40} :catch_19

    .line 2275
    :cond_79
    :goto_3d
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_19
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3d

    .line 2276
    :cond_7a
    :try_start_41
    invoke-static {v8, v2, v3}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_41
    .catch Ljava/io/FileNotFoundException; {:try_start_41 .. :try_end_41} :catch_6
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_41} :catch_d
    .catchall {:try_start_41 .. :try_end_41} :catchall_0

    move-result v3

    if-nez v3, :cond_7c

    .line 2444
    if-eqz v8, :cond_7b

    .line 2445
    :try_start_42
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_42} :catch_1a

    .line 2277
    :cond_7b
    :goto_3e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_1a
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3e

    :cond_7c
    move-object v4, v2

    .line 2281
    :goto_3f
    :try_start_43
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_7d
    :goto_40
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_7f

    .line 2308
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v2

    sub-long/2addr v2, v14

    long-to-int v2, v2

    .line 2311
    if-le v12, v2, :cond_7e

    .line 2315
    sub-int v2, v12, v2

    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 2244
    :cond_7e
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_36

    .line 2281
    :cond_7f
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/samm/a/d;

    .line 2282
    instance-of v3, v2, Lcom/samsung/samm/a/j;

    if-eqz v3, :cond_7d

    .line 2283
    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->a()I

    move-result v3

    if-eqz v3, :cond_80

    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->a()I

    move-result v3

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v3, v0, :cond_7d

    .line 2284
    :cond_80
    const-string v3, "SAMM___LIBRARY___CONTENT___ID___KEY"

    const/16 v17, -0x1

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I
    :try_end_43
    .catch Ljava/io/FileNotFoundException; {:try_start_43 .. :try_end_43} :catch_6
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_43} :catch_d
    .catchall {:try_start_43 .. :try_end_43} :catchall_0

    move-result v3

    .line 2285
    if-gez v3, :cond_82

    .line 2444
    if-eqz v8, :cond_81

    .line 2445
    :try_start_44
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_44} :catch_1b

    .line 2286
    :cond_81
    :goto_41
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_1b
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_41

    .line 2287
    :cond_82
    :try_start_45
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/samm/b/a/d;->K:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-gt v0, v3, :cond_83

    .line 2288
    add-int/lit8 v17, v3, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/samm/b/a/d;->K:I

    .line 2289
    :cond_83
    if-ne v6, v3, :cond_7d

    .line 2290
    const-string v3, "SAMM___LIBRARY___CONTENT___PATH___KEY"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v2, v3, v0}, Lcom/samsung/samm/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2291
    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->a()I

    move-result v3

    if-nez v3, :cond_84

    .line 2292
    move-object v0, v2

    check-cast v0, Lcom/samsung/samm/a/j;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/samsung/samm/a/j;->k()Ljava/lang/String;

    move-result-object v17

    .line 2293
    if-eqz v5, :cond_85

    .line 2294
    move-object v0, v2

    check-cast v0, Lcom/samsung/samm/a/j;

    move-object v3, v0

    invoke-virtual {v3, v5}, Lcom/samsung/samm/a/j;->e(Ljava/lang/String;)Z

    .line 2295
    const-string v3, "SAMM___LIBRARY___CONTENT___SIZE___KEY"

    invoke-virtual {v2, v3, v13}, Lcom/samsung/samm/a/d;->c(Ljava/lang/String;I)Z

    .line 2296
    const-string v3, "SAMM___LIBRARY___EMBEDDED___CONTENT___FLAG___KEY"

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, Lcom/samsung/samm/a/d;->c(Ljava/lang/String;I)Z

    .line 2303
    :cond_84
    :goto_42
    check-cast v2, Lcom/samsung/samm/a/j;

    invoke-virtual {v2, v4}, Lcom/samsung/samm/a/j;->d(Ljava/lang/String;)Z

    goto/16 :goto_40

    .line 2297
    :cond_85
    if-eqz v17, :cond_86

    .line 2298
    move-object v0, v2

    check-cast v0, Lcom/samsung/samm/a/j;

    move-object v3, v0

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/samsung/samm/a/j;->e(Ljava/lang/String;)Z
    :try_end_45
    .catch Ljava/io/FileNotFoundException; {:try_start_45 .. :try_end_45} :catch_6
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_45} :catch_d
    .catchall {:try_start_45 .. :try_end_45} :catchall_0

    goto :goto_42

    .line 2444
    :cond_86
    if-eqz v8, :cond_87

    .line 2445
    :try_start_46
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_46
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_46} :catch_1c

    .line 2300
    :cond_87
    :goto_43
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2446
    :catch_1c
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_43

    .line 2328
    :cond_88
    :try_start_47
    invoke-virtual {v8, v7}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto/16 :goto_37

    .line 2343
    :cond_89
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/samm/b/a/d;->q:Z
    :try_end_47
    .catch Ljava/io/FileNotFoundException; {:try_start_47 .. :try_end_47} :catch_6
    .catch Ljava/io/IOException; {:try_start_47 .. :try_end_47} :catch_d
    .catchall {:try_start_47 .. :try_end_47} :catchall_0

    goto/16 :goto_38

    .line 2446
    :catch_1d
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_39

    .line 2362
    :cond_8a
    :try_start_48
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v2

    if-lez v2, :cond_90

    const/4 v2, 0x1

    :goto_44
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/samm/b/a/d;->r:Z

    .line 2364
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v2

    if-lez v2, :cond_91

    const/4 v2, 0x1

    :goto_45
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/samm/b/a/d;->s:Z

    .line 2366
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/samm/b/a/d;->t:I

    .line 2377
    :cond_8b
    :goto_46
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    .line 2378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->N()I

    move-result v2

    if-lez v2, :cond_8e

    .line 2379
    invoke-static {v8}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 2380
    if-lez v2, :cond_8e

    .line 2381
    if-eqz p2, :cond_8c

    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/samsung/samm/a/c;->r:Z

    if-eqz v3, :cond_93

    .line 2382
    :cond_8c
    new-array v2, v2, [B

    .line 2383
    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->read([B)I

    .line 2385
    const/4 v3, 0x1

    new-array v3, v3, [I

    .line 2386
    const/4 v4, 0x1

    new-array v4, v4, [J

    .line 2387
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    .line 2388
    const/4 v6, 0x0

    .line 2390
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    if-nez v7, :cond_8d

    .line 2391
    new-instance v7, Lcom/samsung/samm/a/a;

    invoke-direct {v7}, Lcom/samsung/samm/a/a;-><init>()V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    .line 2395
    :cond_8d
    invoke-static {v2, v6, v3}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v6

    .line 2396
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    const/4 v9, 0x0

    aget v3, v3, v9

    invoke-virtual {v7, v3}, Lcom/samsung/samm/a/a;->setAppType(I)V

    .line 2401
    invoke-static {v2, v6, v5}, Lcom/samsung/samm/b/a/s;->a([BI[Ljava/lang/String;)I

    move-result v3

    .line 2402
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    const/4 v7, 0x0

    aget-object v7, v5, v7

    invoke-virtual {v6, v7}, Lcom/samsung/samm/a/a;->setAppPackageName(Ljava/lang/String;)V

    .line 2404
    invoke-static {v2, v3, v5}, Lcom/samsung/samm/b/a/s;->a([BI[Ljava/lang/String;)I

    move-result v3

    .line 2405
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    const/4 v7, 0x0

    aget-object v7, v5, v7

    invoke-virtual {v6, v7}, Lcom/samsung/samm/a/a;->setAppClassName(Ljava/lang/String;)V

    .line 2407
    invoke-static {v2, v3, v5}, Lcom/samsung/samm/b/a/s;->a([BI[Ljava/lang/String;)I

    move-result v3

    .line 2408
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    const/4 v7, 0x0

    aget-object v7, v5, v7

    invoke-virtual {v6, v7}, Lcom/samsung/samm/a/a;->setAppURL(Ljava/lang/String;)V

    .line 2410
    invoke-static {v2, v3, v5}, Lcom/samsung/samm/b/a/s;->a([BI[Ljava/lang/String;)I

    move-result v3

    .line 2411
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    const/4 v7, 0x0

    aget-object v5, v5, v7

    invoke-virtual {v6, v5}, Lcom/samsung/samm/a/a;->setAppSrcPath(Ljava/lang/String;)V

    .line 2413
    invoke-static {v2, v3, v4}, Lcom/samsung/samm/b/a/s;->a([BI[J)I

    .line 2414
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    const/4 v3, 0x0

    aget-wide v4, v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/samsung/samm/a/a;->setAppTime(J)V
    :try_end_48
    .catch Ljava/io/FileNotFoundException; {:try_start_48 .. :try_end_48} :catch_6
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_48} :catch_d
    .catchall {:try_start_48 .. :try_end_48} :catchall_0

    .line 2444
    :cond_8e
    :goto_47
    if-eqz v8, :cond_8f

    .line 2445
    :try_start_49
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_49 .. :try_end_49} :catch_21

    .line 2454
    :cond_8f
    :goto_48
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2362
    :cond_90
    const/4 v2, 0x0

    goto/16 :goto_44

    .line 2364
    :cond_91
    const/4 v2, 0x0

    goto/16 :goto_45

    .line 2368
    :cond_92
    :try_start_4a
    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    goto/16 :goto_46

    .line 2429
    :cond_93
    invoke-virtual {v8, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I
    :try_end_4a
    .catch Ljava/io/FileNotFoundException; {:try_start_4a .. :try_end_4a} :catch_6
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4a} :catch_d
    .catchall {:try_start_4a .. :try_end_4a} :catchall_0

    goto :goto_47

    .line 2446
    :catch_1e
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_a

    .line 2446
    :catch_1f
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1b

    .line 2446
    :catch_20
    move-exception v3

    .line 2447
    const-string v4, "SAMMLibraryCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Read AMS Header Error : IOException : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1d

    .line 2446
    :catch_21
    move-exception v2

    .line 2447
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_48

    .line 2442
    :catchall_1
    move-exception v2

    move-object v8, v3

    goto/16 :goto_1c

    :catchall_2
    move-exception v2

    move-object v8, v3

    goto/16 :goto_1c

    .line 2438
    :catch_22
    move-exception v2

    move-object v8, v3

    goto/16 :goto_1a

    .line 2434
    :catch_23
    move-exception v2

    goto/16 :goto_9

    :cond_94
    move-object v4, v2

    goto/16 :goto_3f

    :cond_95
    move-object v5, v2

    goto/16 :goto_3c
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1242
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;ZLandroid/graphics/Rect;ZILjava/lang/String;)Z
    .locals 18

    .prologue
    .line 2491
    if-nez p1, :cond_0

    .line 2492
    const/4 v2, 0x0

    .line 3403
    :goto_0
    return v2

    .line 2516
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->D:I

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->m(I)V

    .line 2517
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->E:I

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->n(I)V

    .line 2522
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->ab()V

    .line 2524
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->i:Ljava/util/LinkedList;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;)Ljava/util/LinkedList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    .line 2526
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/samm/b/a/d;->e:I

    .line 2530
    if-eqz p2, :cond_4

    .line 2533
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->a(I)V

    .line 2536
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->b(I)V

    .line 2537
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->c(I)V

    .line 2538
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->d(I)V

    .line 2539
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->e(I)V

    .line 2540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->f(I)V

    .line 2541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->g(I)V

    .line 2544
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->h(I)V

    .line 2546
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->i(I)V

    .line 2548
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->j(I)V

    .line 2550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->o(I)V

    .line 2551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->p(I)V

    .line 2553
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->q(I)V

    .line 2555
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->r(I)V

    .line 2557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->s(I)V

    .line 2559
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->t(I)V

    .line 2561
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->u(I)V

    .line 2563
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->v(I)V

    .line 2565
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->w(I)V

    .line 2567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->x(I)V

    .line 2569
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->y(I)V

    .line 2571
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->z(I)V

    .line 2573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->A(I)V

    .line 2575
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->B(I)V

    .line 2577
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->C(I)V

    .line 2579
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->D(I)V

    .line 2581
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->E(I)V

    .line 2583
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->F(I)V

    .line 2585
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->Y()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2586
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2587
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->M:I

    if-lez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->G(I)V

    .line 2589
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->H(I)V

    .line 2591
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->I(I)V

    .line 2593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->J(I)V

    .line 2595
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->K(I)V

    .line 2597
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->L(I)V

    .line 2599
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->t(I)V

    .line 2726
    :goto_2
    invoke-static/range {p1 .. p1}, Lcom/samsung/samm/b/a/f;->a(Ljava/lang/String;)J

    move-result-wide v6

    .line 2727
    const/4 v4, 0x0

    .line 2730
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v2, "rw"

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_29
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2733
    const-wide/16 v4, 0x0

    cmp-long v2, v6, v4

    if-lez v2, :cond_1b

    .line 2734
    :try_start_1
    invoke-virtual {v3, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2742
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->g:Lcom/samsung/samm/b/a/f;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/f;->a()Lcom/samsung/samm/b/a/i;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/i;->b(Ljava/io/RandomAccessFile;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_1d

    .line 3393
    if-eqz v3, :cond_2

    .line 3394
    :try_start_2
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2743
    :cond_2
    :goto_4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2587
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 2603
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->A:I

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->a(I)V

    .line 2606
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->b(I)V

    .line 2608
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->d()I

    move-result v2

    if-ltz v2, :cond_5

    .line 2609
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->d()I

    move-result v2

    const/4 v3, 0x4

    if-le v2, v3, :cond_6

    .line 2610
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->b(I)V

    .line 2612
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/samm/b/a/d;->as:Z

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->d()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->d()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    .line 2613
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->b(I)V

    .line 2615
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/c;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->c(I)V

    .line 2616
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/c;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->d(I)V

    .line 2617
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/c;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->e(I)V

    .line 2618
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/c;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->f(I)V

    .line 2619
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/c;->g()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->g(I)V

    .line 2622
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/c;->i()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->h(I)V

    .line 2625
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->i(I)V

    .line 2628
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->j(I)V

    .line 2631
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->v:Lcom/samsung/samm/b/a/b;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/b;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->o(I)V

    .line 2632
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->v:Lcom/samsung/samm/b/a/b;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/b;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->p(I)V

    .line 2634
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->v:Lcom/samsung/samm/b/a/b;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/b;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->q(I)V

    .line 2635
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->s()I

    move-result v2

    if-gez v2, :cond_9

    .line 2636
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2639
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->P()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->r(I)V

    .line 2649
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->s(I)V

    .line 2652
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->Q()I

    move-result v2

    if-lez v2, :cond_b

    const/4 v2, 0x1

    :goto_5
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->t(I)V

    .line 2655
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->S()I

    move-result v2

    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->T()I

    move-result v4

    add-int/2addr v2, v4

    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->U()I

    move-result v4

    add-int/2addr v2, v4

    if-gtz v2, :cond_a

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/samm/b/a/d;->X:Z

    if-eqz v2, :cond_c

    :cond_a
    const/4 v2, 0x1

    :goto_6
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->u(I)V

    .line 2658
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->v(I)V

    .line 2661
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->ae:I

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->w(I)V

    .line 2664
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->x:Lcom/samsung/samm/b/a/q;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/q;->b()I

    move-result v2

    if-lez v2, :cond_d

    const/4 v2, 0x1

    :goto_7
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->x(I)V

    .line 2667
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->w:Lcom/samsung/samm/b/a/a;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/a;->b()I

    move-result v2

    if-lez v2, :cond_e

    const/4 v2, 0x1

    :goto_8
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->y(I)V

    .line 2670
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->V()I

    move-result v2

    if-lez v2, :cond_f

    const/4 v2, 0x1

    :goto_9
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->z(I)V

    .line 2673
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->A(I)V

    .line 2676
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/e;->a()I

    move-result v2

    if-lez v2, :cond_10

    const/4 v2, 0x1

    :goto_a
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->B(I)V

    .line 2679
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/samm/b/a/d;->ac:Z

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    :goto_b
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->C(I)V

    .line 2682
    const-string v2, "1.7c"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->af:Ljava/lang/String;

    .line 2683
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->D(I)V

    .line 2686
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/samm/b/a/d;->at:Z

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->o:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v2

    const/4 v4, 0x4

    if-ne v2, v4, :cond_12

    const/4 v2, 0x1

    :goto_c
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->E(I)V

    .line 2689
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->F(I)V

    .line 2692
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->Y()Z

    move-result v2

    if-nez v2, :cond_13

    .line 2693
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2652
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 2655
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 2664
    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 2667
    :cond_e
    const/4 v2, 0x0

    goto :goto_8

    .line 2670
    :cond_f
    const/4 v2, 0x0

    goto :goto_9

    .line 2676
    :cond_10
    const/4 v2, 0x0

    goto :goto_a

    .line 2679
    :cond_11
    const/4 v2, 0x0

    goto :goto_b

    .line 2686
    :cond_12
    const/4 v2, 0x0

    goto :goto_c

    .line 2694
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->M:I

    if-lez v2, :cond_14

    const/4 v2, 0x1

    :goto_d
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->G(I)V

    .line 2697
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->n:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v2

    const/4 v4, 0x4

    if-ne v2, v4, :cond_15

    const/4 v2, 0x1

    :goto_e
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->H(I)V

    .line 2700
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->u:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v2

    const/4 v4, 0x4

    if-ne v2, v4, :cond_16

    const/4 v2, 0x1

    :goto_f
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->I(I)V

    .line 2703
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->Z()Z

    move-result v2

    if-nez v2, :cond_17

    .line 2704
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2694
    :cond_14
    const/4 v2, 0x0

    goto :goto_d

    .line 2697
    :cond_15
    const/4 v2, 0x0

    goto :goto_e

    .line 2700
    :cond_16
    const/4 v2, 0x0

    goto :goto_f

    .line 2706
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->N:I

    if-lez v2, :cond_18

    const/4 v2, 0x1

    :goto_10
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->J(I)V

    .line 2709
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/samm/b/a/d;->q:Z

    if-eqz v2, :cond_19

    .line 2710
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->K(I)V

    .line 2716
    :goto_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/samm/b/a/d;->e:I

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->L(I)V

    .line 2719
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    if-eqz v2, :cond_1a

    const/4 v2, 0x1

    :goto_12
    invoke-virtual {v3, v2}, Lcom/samsung/samm/b/a/h;->M(I)V

    goto/16 :goto_2

    .line 2706
    :cond_18
    const/4 v2, 0x0

    goto :goto_10

    .line 2712
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/samm/b/a/d;->p:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->K(I)V

    goto :goto_11

    .line 2719
    :cond_1a
    const/4 v2, 0x0

    goto :goto_12

    .line 2737
    :cond_1b
    :try_start_3
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    long-to-int v2, v4

    invoke-virtual {v3, v2}, Ljava/io/RandomAccessFile;->skipBytes(I)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_3

    .line 3383
    :catch_0
    move-exception v2

    .line 3384
    :goto_13
    :try_start_4
    const-string v4, "SAMMLibraryCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FileNotFoundException : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3385
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3393
    if-eqz v3, :cond_1c

    .line 3394
    :try_start_5
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_25

    .line 3386
    :cond_1c
    :goto_14
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_1
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 2748
    :cond_1d
    :try_start_6
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v8

    .line 2749
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/h;->b(Ljava/io/RandomAccessFile;)Z
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v2

    if-nez v2, :cond_1f

    .line 3393
    if-eqz v3, :cond_1e

    .line 3394
    :try_start_7
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 2750
    :cond_1e
    :goto_15
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_2
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_15

    .line 2753
    :cond_1f
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->f:[B

    if-eqz v2, :cond_20

    .line 2754
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->f:[B

    .line 2756
    :cond_20
    const/16 v2, 0x1e

    new-array v2, v2, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/samm/b/a/d;->f:[B

    .line 2762
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->W()Z
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v2

    if-nez v2, :cond_22

    .line 3393
    if-eqz v3, :cond_21

    .line 3394
    :try_start_9
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 2763
    :cond_21
    :goto_16
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_3
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_16

    .line 2771
    :cond_22
    const/4 v2, 0x0

    .line 2772
    const/4 v4, 0x0

    :try_start_a
    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/samm/b/a/d;->H:I

    .line 2773
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2774
    const/4 v5, 0x1

    new-array v5, v5, [I

    .line 2775
    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    .line 2778
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->i:Ljava/util/LinkedList;

    move-object/from16 v0, p3

    invoke-static {v6, v4, v2, v5, v0}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;Ljava/util/ArrayList;Lcom/samsung/samm/b/a/j;[ILandroid/graphics/Rect;)Z
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v2

    if-nez v2, :cond_24

    .line 3393
    if-eqz v3, :cond_23

    .line 3394
    :try_start_b
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    .line 2779
    :cond_23
    :goto_17
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_4
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_17

    .line 2782
    :cond_24
    :try_start_c
    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v7

    .line 2783
    const/4 v4, 0x0

    .line 2785
    const/4 v2, 0x0

    aget v10, v5, v2

    .line 2786
    const/4 v2, 0x0

    move v6, v4

    move v4, v2

    :goto_18
    array-length v2, v7

    if-lt v4, v2, :cond_27

    .line 2789
    if-lez v6, :cond_25

    .line 2790
    new-array v11, v6, [B

    .line 2791
    const/4 v4, 0x0

    .line 2792
    const/4 v2, 0x0

    move v5, v4

    move v4, v2

    :goto_19
    array-length v2, v7

    if-lt v4, v2, :cond_28

    .line 2797
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->e:I
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    if-nez v2, :cond_2a

    .line 2800
    :try_start_d
    invoke-virtual {v3, v11}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 2813
    :cond_25
    :goto_1a
    :try_start_e
    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/samm/b/a/d;->H:I

    .line 2816
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    .line 2818
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->b()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2819
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/samm/b/a/d;->L:I

    invoke-virtual {v2, v6}, Lcom/samsung/samm/b/a/h;->i(I)V

    .line 2820
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->k()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 2822
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->c()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2823
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/samm/b/a/d;->H:I

    invoke-virtual {v2, v6}, Lcom/samsung/samm/b/a/h;->k(I)V

    .line 2824
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v6}, Lcom/samsung/samm/b/a/h;->m()I

    move-result v6

    shl-int/lit8 v6, v6, 0x3

    invoke-virtual {v2, v6}, Lcom/samsung/samm/b/a/h;->j(I)V

    .line 2825
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->l()I

    move-result v2

    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 2826
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->l()I

    move-result v2

    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 2828
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->a()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2829
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2, v10}, Lcom/samsung/samm/b/a/h;->l(I)V

    .line 2830
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->n()I

    move-result v2

    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;I)V

    .line 2832
    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2837
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/h;->d()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/h;->j()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;II)Z
    :try_end_e
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    move-result v2

    if-nez v2, :cond_2d

    .line 3393
    if-eqz v3, :cond_26

    .line 3394
    :try_start_f
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    .line 2838
    :cond_26
    :goto_1b
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2787
    :cond_27
    :try_start_10
    aget-object v2, v7, v4

    check-cast v2, [B

    array-length v2, v2

    add-int v5, v6, v2

    .line 2786
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v6, v5

    goto/16 :goto_18

    .line 2793
    :cond_28
    aget-object v2, v7, v4

    check-cast v2, [B

    array-length v2, v2

    .line 2794
    aget-object v12, v7, v4

    const/4 v13, 0x0

    invoke-static {v12, v13, v11, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2795
    add-int/2addr v5, v2

    .line 2792
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_19

    .line 2801
    :catch_5
    move-exception v2

    .line 2803
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_1a

    .line 3387
    :catch_6
    move-exception v2

    .line 3388
    :goto_1c
    :try_start_11
    const-string v4, "SAMMLibraryCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "IOException : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3389
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 3393
    if-eqz v3, :cond_29

    .line 3394
    :try_start_12
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_26

    .line 3390
    :cond_29
    :goto_1d
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2806
    :cond_2a
    :try_start_13
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->e:I

    const/4 v4, 0x1

    if-eq v2, v4, :cond_2b

    .line 2807
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->e:I
    :try_end_13
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_13} :catch_0
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_6
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    const/4 v4, 0x2

    if-ne v2, v4, :cond_25

    .line 3393
    :cond_2b
    if-eqz v3, :cond_2c

    .line 3394
    :try_start_14
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_7

    .line 2809
    :cond_2c
    :goto_1e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_7
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1e

    .line 3395
    :catch_8
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1b

    .line 2844
    :cond_2d
    const/4 v2, 0x0

    :goto_1f
    :try_start_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/h;->k()I

    move-result v4

    if-lt v2, v4, :cond_2f

    .line 2860
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->v:Lcom/samsung/samm/b/a/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/h;->q()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/h;->s()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/samm/b/a/b;->a(Ljava/io/RandomAccessFile;II)Z
    :try_end_15
    .catch Ljava/io/FileNotFoundException; {:try_start_15 .. :try_end_15} :catch_0
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_6
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    move-result v2

    if-nez v2, :cond_34

    .line 3393
    if-eqz v3, :cond_2e

    .line 3394
    :try_start_16
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_b

    .line 2861
    :cond_2e
    :goto_20
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2846
    :cond_2f
    :try_start_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/samm/b/a/d;->k:[Lcom/samsung/samm/b/a/d$a;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/d$a;->b()I

    move-result v5

    invoke-static {v4, v5}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 2847
    invoke-static {v4}, Lcom/samsung/samm/b/a/s;->b(Ljava/lang/String;)I
    :try_end_17
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_17} :catch_0
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_6
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    move-result v5

    .line 2848
    if-gtz v5, :cond_31

    .line 3393
    if-eqz v3, :cond_30

    .line 3394
    :try_start_18
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_9

    .line 2849
    :cond_30
    :goto_21
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_9
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_21

    .line 2851
    :cond_31
    :try_start_19
    invoke-static {v3, v5}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 2852
    invoke-static {v3, v4, v5}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_19
    .catch Ljava/io/FileNotFoundException; {:try_start_19 .. :try_end_19} :catch_0
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_6
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    move-result v4

    if-nez v4, :cond_33

    .line 3393
    if-eqz v3, :cond_32

    .line 3394
    :try_start_1a
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_a

    .line 2853
    :cond_32
    :goto_22
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_a
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_22

    .line 2844
    :cond_33
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1f

    .line 3395
    :catch_b
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_20

    .line 2867
    :cond_34
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->t()I

    move-result v5

    .line 2868
    if-lez v5, :cond_35

    .line 2869
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->P:Ljava/lang/String;

    if-eqz v2, :cond_38

    .line 2870
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->P:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 2871
    mul-int/lit8 v2, v5, 0x2

    new-array v7, v2, [B

    .line 2872
    const/4 v4, 0x0

    .line 2873
    const/4 v2, 0x0

    :goto_23
    if-lt v2, v5, :cond_37

    .line 2875
    invoke-virtual {v3, v7}, Ljava/io/RandomAccessFile;->write([B)V

    .line 2905
    :cond_35
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->X()Z
    :try_end_1b
    .catch Ljava/io/FileNotFoundException; {:try_start_1b .. :try_end_1b} :catch_0
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_6
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    move-result v2

    if-nez v2, :cond_3a

    .line 3393
    if-eqz v3, :cond_36

    .line 3394
    :try_start_1c
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_d

    .line 2906
    :cond_36
    :goto_24
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2874
    :cond_37
    :try_start_1d
    aget-char v10, v6, v2

    invoke-static {v7, v4, v10}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 2873
    add-int/lit8 v2, v2, 0x1

    goto :goto_23

    .line 2877
    :cond_38
    const-string v2, "AmsLib"

    const-string v4, "Title Text is invalid!!!"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d
    .catch Ljava/io/FileNotFoundException; {:try_start_1d .. :try_end_1d} :catch_0
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_6
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    .line 3393
    if-eqz v3, :cond_39

    .line 3394
    :try_start_1e
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_c

    .line 2878
    :cond_39
    :goto_25
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_c
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_25

    .line 3395
    :catch_d
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_24

    .line 2912
    :cond_3a
    :try_start_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->u()I

    move-result v2

    if-lez v2, :cond_3d

    .line 2913
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->Q()I

    move-result v6

    .line 2914
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->R()I

    move-result v7

    .line 2915
    mul-int/lit8 v2, v6, 0x2

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x2

    mul-int/lit8 v4, v7, 0x2

    add-int/2addr v2, v4

    new-array v10, v2, [B

    .line 2916
    const/4 v2, 0x0

    .line 2917
    invoke-static {v10, v2, v6}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v2

    .line 2918
    if-lez v6, :cond_3b

    .line 2919
    sget-object v4, Lcom/samsung/samm/b/a/d;->a:Ljava/lang/String;

    if-eqz v4, :cond_40

    .line 2921
    sget-object v4, Lcom/samsung/samm/b/a/d;->a:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 2922
    const/4 v4, 0x0

    :goto_26
    if-lt v4, v6, :cond_3f

    .line 2930
    :cond_3b
    sget v4, Lcom/samsung/samm/b/a/d;->b:I

    invoke-static {v10, v2, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v2

    .line 2931
    sget v4, Lcom/samsung/samm/b/a/d;->c:I

    invoke-static {v10, v2, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v2

    .line 2933
    invoke-static {v10, v2, v7}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 2934
    if-lez v7, :cond_3c

    .line 2935
    sget-object v2, Lcom/samsung/samm/b/a/d;->d:Ljava/lang/String;

    if-eqz v2, :cond_43

    .line 2937
    sget-object v2, Lcom/samsung/samm/b/a/d;->d:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    .line 2938
    const/4 v2, 0x0

    :goto_27
    if-lt v2, v7, :cond_42

    .line 2946
    :cond_3c
    invoke-virtual {v3, v10}, Ljava/io/RandomAccessFile;->write([B)V

    .line 2953
    :cond_3d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->v()I

    move-result v2

    if-lez v2, :cond_4d

    .line 2955
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->S()I

    move-result v7

    .line 2956
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->T()I

    move-result v10

    .line 2957
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->U()I

    move-result v11

    .line 2958
    const/4 v2, 0x0

    .line 2959
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/samm/b/a/d;->X:Z

    if-eqz v4, :cond_45

    .line 2960
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->aq:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/samm/b/a/s;->b(Ljava/lang/String;)I
    :try_end_1f
    .catch Ljava/io/FileNotFoundException; {:try_start_1f .. :try_end_1f} :catch_0
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_6
    .catchall {:try_start_1f .. :try_end_1f} :catchall_1

    move-result v2

    .line 2961
    if-gez v2, :cond_45

    .line 3393
    if-eqz v3, :cond_3e

    .line 3394
    :try_start_20
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_10

    .line 2962
    :cond_3e
    :goto_28
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2923
    :cond_3f
    :try_start_21
    aget-char v5, v11, v4

    invoke-static {v10, v2, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v5

    .line 2922
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v5

    goto :goto_26

    .line 2925
    :cond_40
    const-string v2, "AmsLib"

    const-string v4, "Application Identifier is invalid!!!"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_21
    .catch Ljava/io/FileNotFoundException; {:try_start_21 .. :try_end_21} :catch_0
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_6
    .catchall {:try_start_21 .. :try_end_21} :catchall_1

    .line 3393
    if-eqz v3, :cond_41

    .line 3394
    :try_start_22
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_e

    .line 2926
    :cond_41
    :goto_29
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_e
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_29

    .line 2939
    :cond_42
    :try_start_23
    aget-char v6, v5, v2

    invoke-static {v10, v4, v6}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 2938
    add-int/lit8 v2, v2, 0x1

    goto :goto_27

    .line 2941
    :cond_43
    const-string v2, "AmsLib"

    const-string v4, "Application Patch Version String is invalid!!!"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_23
    .catch Ljava/io/FileNotFoundException; {:try_start_23 .. :try_end_23} :catch_0
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_23} :catch_6
    .catchall {:try_start_23 .. :try_end_23} :catchall_1

    .line 3393
    if-eqz v3, :cond_44

    .line 3394
    :try_start_24
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_f

    .line 2942
    :cond_44
    :goto_2a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_f
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2a

    .line 3395
    :catch_10
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_28

    :cond_45
    move v6, v2

    .line 2964
    mul-int/lit8 v2, v7, 0x2

    add-int/lit8 v2, v2, 0x2

    mul-int/lit8 v4, v10, 0x2

    add-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    mul-int/lit8 v4, v11, 0x2

    add-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    add-int/lit8 v4, v6, 0x4

    add-int/2addr v2, v4

    .line 2965
    add-int/lit8 v4, v2, 0x4

    add-int/lit8 v5, v6, 0x4

    sub-int/2addr v4, v5

    :try_start_25
    new-array v12, v4, [B

    .line 2966
    const/4 v4, 0x0

    .line 2967
    invoke-static {v12, v4, v2}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v2

    .line 2969
    invoke-static {v12, v2, v7}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v2

    .line 2970
    if-lez v7, :cond_46

    .line 2971
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->U:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    .line 2972
    const/4 v4, 0x0

    :goto_2b
    if-lt v4, v7, :cond_4a

    .line 2976
    :cond_46
    invoke-static {v12, v2, v10}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v2

    .line 2977
    if-lez v10, :cond_47

    .line 2978
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->V:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    .line 2979
    const/4 v4, 0x0

    :goto_2c
    if-lt v4, v10, :cond_4b

    .line 2983
    :cond_47
    invoke-static {v12, v2, v11}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 2984
    if-lez v11, :cond_48

    .line 2985
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->W:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    .line 2986
    const/4 v2, 0x0

    :goto_2d
    if-lt v2, v11, :cond_4c

    .line 2990
    :cond_48
    invoke-virtual {v3, v12}, Ljava/io/RandomAccessFile;->write([B)V

    .line 2993
    invoke-static {v3, v6}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 2994
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/samm/b/a/d;->X:Z

    if-eqz v2, :cond_4d

    .line 2995
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->aq:Ljava/lang/String;

    invoke-static {v3, v2, v6}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_25
    .catch Ljava/io/FileNotFoundException; {:try_start_25 .. :try_end_25} :catch_0
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_25} :catch_6
    .catchall {:try_start_25 .. :try_end_25} :catchall_1

    move-result v2

    if-nez v2, :cond_4d

    .line 3393
    if-eqz v3, :cond_49

    .line 3394
    :try_start_26
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_11

    .line 2996
    :cond_49
    :goto_2e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2973
    :cond_4a
    :try_start_27
    aget-char v5, v13, v4

    invoke-static {v12, v2, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v5

    .line 2972
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v5

    goto :goto_2b

    .line 2980
    :cond_4b
    aget-char v5, v7, v4

    invoke-static {v12, v2, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v5

    .line 2979
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v5

    goto :goto_2c

    .line 2987
    :cond_4c
    aget-char v7, v5, v2

    invoke-static {v12, v4, v7}, Lcom/samsung/samm/b/a/s;->b([BII)I
    :try_end_27
    .catch Ljava/io/FileNotFoundException; {:try_start_27 .. :try_end_27} :catch_0
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_27} :catch_6
    .catchall {:try_start_27 .. :try_end_27} :catchall_1

    move-result v4

    .line 2986
    add-int/lit8 v2, v2, 0x1

    goto :goto_2d

    .line 3395
    :catch_11
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2e

    .line 3004
    :cond_4d
    :try_start_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->w()I

    move-result v2

    if-lez v2, :cond_4e

    .line 3005
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/samm/b/a/d;->Y:J

    .line 3006
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/samm/b/a/d;->Y:J

    invoke-static {v3, v4, v5}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;J)V

    .line 3013
    :cond_4e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->y()I

    move-result v2

    if-lez v2, :cond_50

    .line 3014
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->x:Lcom/samsung/samm/b/a/q;

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/q;->a(Ljava/io/RandomAccessFile;)I
    :try_end_28
    .catch Ljava/io/FileNotFoundException; {:try_start_28 .. :try_end_28} :catch_0
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_6
    .catchall {:try_start_28 .. :try_end_28} :catchall_1

    move-result v2

    .line 3015
    if-gez v2, :cond_50

    .line 3393
    if-eqz v3, :cond_4f

    .line 3394
    :try_start_29
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_29} :catch_12

    .line 3016
    :cond_4f
    :goto_2f
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_12
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2f

    .line 3023
    :cond_50
    :try_start_2a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->z()I

    move-result v2

    if-lez v2, :cond_52

    .line 3024
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->w:Lcom/samsung/samm/b/a/a;

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/a;->a(Ljava/io/RandomAccessFile;)I
    :try_end_2a
    .catch Ljava/io/FileNotFoundException; {:try_start_2a .. :try_end_2a} :catch_0
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_2a} :catch_6
    .catchall {:try_start_2a .. :try_end_2a} :catchall_1

    move-result v2

    .line 3025
    if-gez v2, :cond_52

    .line 3393
    if-eqz v3, :cond_51

    .line 3394
    :try_start_2b
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_2b} :catch_13

    .line 3026
    :cond_51
    :goto_30
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_13
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_30

    .line 3033
    :cond_52
    :try_start_2c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->A()I

    move-result v2

    if-lez v2, :cond_54

    .line 3035
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->V()I

    move-result v5

    .line 3036
    mul-int/lit8 v2, v5, 0x2

    add-int/lit8 v2, v2, 0x2

    new-array v6, v2, [B

    .line 3037
    const/4 v2, 0x0

    .line 3038
    invoke-static {v6, v2, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 3039
    if-lez v5, :cond_53

    .line 3041
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->Z:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    .line 3042
    const/4 v2, 0x0

    :goto_31
    if-lt v2, v5, :cond_57

    .line 3045
    :cond_53
    invoke-virtual {v3, v6}, Ljava/io/RandomAccessFile;->write([B)V

    .line 3052
    :cond_54
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->B()I

    move-result v2

    if-lez v2, :cond_55

    .line 3053
    const/16 v2, 0x8

    .line 3054
    const/16 v4, 0xc

    new-array v4, v4, [B

    .line 3055
    const/4 v5, 0x0

    .line 3056
    invoke-static {v4, v5, v2}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v2

    .line 3057
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/samm/b/a/d;->aa:I

    invoke-static {v4, v2, v5}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v2

    .line 3058
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/samm/b/a/d;->ab:I

    invoke-static {v4, v2, v5}, Lcom/samsung/samm/b/a/s;->a([BII)I

    .line 3059
    invoke-virtual {v3, v4}, Ljava/io/RandomAccessFile;->write([B)V

    .line 3066
    :cond_55
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->C()I

    move-result v2

    if-lez v2, :cond_59

    .line 3067
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/e;->b()I

    move-result v2

    new-array v2, v2, [B

    .line 3068
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Lcom/samsung/samm/b/a/e;->b([BI)I
    :try_end_2c
    .catch Ljava/io/FileNotFoundException; {:try_start_2c .. :try_end_2c} :catch_0
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_2c} :catch_6
    .catchall {:try_start_2c .. :try_end_2c} :catchall_1

    move-result v4

    .line 3069
    if-gez v4, :cond_58

    .line 3393
    if-eqz v3, :cond_56

    .line 3394
    :try_start_2d
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_2d} :catch_14

    .line 3070
    :cond_56
    :goto_32
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3043
    :cond_57
    :try_start_2e
    aget-char v10, v7, v2

    invoke-static {v6, v4, v10}, Lcom/samsung/samm/b/a/s;->b([BII)I
    :try_end_2e
    .catch Ljava/io/FileNotFoundException; {:try_start_2e .. :try_end_2e} :catch_0
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_2e} :catch_6
    .catchall {:try_start_2e .. :try_end_2e} :catchall_1

    move-result v4

    .line 3042
    add-int/lit8 v2, v2, 0x1

    goto :goto_31

    .line 3395
    :catch_14
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_32

    .line 3071
    :cond_58
    :try_start_2f
    invoke-virtual {v3, v2}, Ljava/io/RandomAccessFile;->write([B)V

    .line 3078
    :cond_59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->D()I

    move-result v2

    if-lez v2, :cond_5d

    .line 3079
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->ar:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/samm/b/a/s;->b(Ljava/lang/String;)I
    :try_end_2f
    .catch Ljava/io/FileNotFoundException; {:try_start_2f .. :try_end_2f} :catch_0
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_2f} :catch_6
    .catchall {:try_start_2f .. :try_end_2f} :catchall_1

    move-result v2

    .line 3080
    if-gez v2, :cond_5b

    .line 3393
    if-eqz v3, :cond_5a

    .line 3394
    :try_start_30
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_30
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_30} :catch_15

    .line 3081
    :cond_5a
    :goto_33
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_15
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_33

    .line 3082
    :cond_5b
    :try_start_31
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3083
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->ar:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_31
    .catch Ljava/io/FileNotFoundException; {:try_start_31 .. :try_end_31} :catch_0
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_31} :catch_6
    .catchall {:try_start_31 .. :try_end_31} :catchall_1

    move-result v2

    if-nez v2, :cond_5d

    .line 3393
    if-eqz v3, :cond_5c

    .line 3394
    :try_start_32
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_32} :catch_16

    .line 3084
    :cond_5c
    :goto_34
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_16
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_34

    .line 3091
    :cond_5d
    :try_start_33
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->E()I

    move-result v2

    if-lez v2, :cond_5f

    .line 3093
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->O()I

    move-result v5

    .line 3094
    mul-int/lit8 v2, v5, 0x2

    add-int/lit8 v2, v2, 0x2

    new-array v6, v2, [B

    .line 3095
    const/4 v2, 0x0

    .line 3096
    invoke-static {v6, v2, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 3097
    if-lez v5, :cond_5e

    .line 3099
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->af:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    .line 3100
    const/4 v2, 0x0

    :goto_35
    if-lt v2, v5, :cond_61

    .line 3103
    :cond_5e
    invoke-virtual {v3, v6}, Ljava/io/RandomAccessFile;->write([B)V

    .line 3110
    :cond_5f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->F()I

    move-result v2

    if-lez v2, :cond_62

    .line 3111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->o:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/c;->i()I

    move-result v2

    .line 3112
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3113
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->o:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/samm/b/a/d;->o:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v5

    invoke-virtual {v4, v3, v5, v2}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;II)Z
    :try_end_33
    .catch Ljava/io/FileNotFoundException; {:try_start_33 .. :try_end_33} :catch_0
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_33} :catch_6
    .catchall {:try_start_33 .. :try_end_33} :catchall_1

    move-result v2

    if-nez v2, :cond_62

    .line 3393
    if-eqz v3, :cond_60

    .line 3394
    :try_start_34
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_34} :catch_17

    .line 3114
    :cond_60
    :goto_36
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3101
    :cond_61
    :try_start_35
    aget-char v10, v7, v2

    invoke-static {v6, v4, v10}, Lcom/samsung/samm/b/a/s;->b([BII)I
    :try_end_35
    .catch Ljava/io/FileNotFoundException; {:try_start_35 .. :try_end_35} :catch_0
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_35} :catch_6
    .catchall {:try_start_35 .. :try_end_35} :catchall_1

    move-result v4

    .line 3100
    add-int/lit8 v2, v2, 0x1

    goto :goto_35

    .line 3395
    :catch_17
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_36

    .line 3128
    :cond_62
    :try_start_36
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->H()I

    move-result v2

    if-lez v2, :cond_64

    .line 3130
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    .line 3131
    const/4 v2, 0x0

    .line 3132
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3133
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v10

    .line 3136
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->M:I

    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3138
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->M:I

    new-array v5, v2, [I

    .line 3139
    const/4 v2, 0x0

    .line 3140
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->j:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v4, v2

    :cond_63
    :goto_37
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_66

    .line 3150
    const/4 v2, 0x0

    :goto_38
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/samm/b/a/d;->M:I

    if-lt v2, v4, :cond_67

    .line 3176
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    .line 3177
    sub-long v10, v4, v10

    long-to-int v2, v10

    .line 3178
    invoke-virtual {v3, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3179
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3180
    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3187
    :cond_64
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->I()I

    move-result v2

    if-lez v2, :cond_6c

    .line 3188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->n:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/c;->i()I

    move-result v2

    .line 3189
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->n:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/samm/b/a/d;->n:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v5

    invoke-virtual {v4, v3, v5, v2}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;II)Z
    :try_end_36
    .catch Ljava/io/FileNotFoundException; {:try_start_36 .. :try_end_36} :catch_0
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_36} :catch_6
    .catchall {:try_start_36 .. :try_end_36} :catchall_1

    move-result v2

    if-nez v2, :cond_6c

    .line 3393
    if-eqz v3, :cond_65

    .line 3394
    :try_start_37
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_37
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_37} :catch_1a

    .line 3191
    :cond_65
    :goto_39
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3140
    :cond_66
    :try_start_38
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/samm/a/d;

    .line 3141
    instance-of v13, v2, Lcom/samsung/samm/a/e;

    if-eqz v13, :cond_63

    .line 3142
    const-string v13, "SAMM___LIBRARY___CONTENT___ID___KEY"

    const/4 v14, -0x1

    invoke-virtual {v2, v13, v14}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v13

    .line 3143
    if-ltz v13, :cond_63

    .line 3144
    add-int/lit8 v2, v4, 0x1

    aput v13, v5, v4

    move v4, v2

    goto :goto_37

    .line 3152
    :cond_67
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v12

    .line 3153
    const/4 v4, 0x0

    .line 3154
    invoke-static {v3, v4}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3155
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v14

    .line 3158
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    aget v16, v5, v2

    move/from16 v0, v16

    invoke-static {v4, v0}, Lcom/samsung/samm/b/a/d;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 3159
    invoke-static {v4}, Lcom/samsung/samm/b/a/s;->b(Ljava/lang/String;)I
    :try_end_38
    .catch Ljava/io/FileNotFoundException; {:try_start_38 .. :try_end_38} :catch_0
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_38} :catch_6
    .catchall {:try_start_38 .. :try_end_38} :catchall_1

    move-result v16

    .line 3160
    if-gtz v16, :cond_69

    .line 3393
    if-eqz v3, :cond_68

    .line 3394
    :try_start_39
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_39} :catch_18

    .line 3161
    :cond_68
    :goto_3a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_18
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3a

    .line 3162
    :cond_69
    :try_start_3a
    move/from16 v0, v16

    invoke-static {v3, v0}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3164
    move/from16 v0, v16

    invoke-static {v3, v4, v0}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_3a
    .catch Ljava/io/FileNotFoundException; {:try_start_3a .. :try_end_3a} :catch_0
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3a} :catch_6
    .catchall {:try_start_3a .. :try_end_3a} :catchall_1

    move-result v4

    if-nez v4, :cond_6b

    .line 3393
    if-eqz v3, :cond_6a

    .line 3394
    :try_start_3b
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3b
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_3b} :catch_19

    .line 3165
    :cond_6a
    :goto_3b
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_19
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3b

    .line 3168
    :cond_6b
    :try_start_3c
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v16

    .line 3169
    sub-long v14, v16, v14

    long-to-int v4, v14

    .line 3170
    invoke-virtual {v3, v12, v13}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3171
    invoke-static {v3, v4}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3172
    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_3c
    .catch Ljava/io/FileNotFoundException; {:try_start_3c .. :try_end_3c} :catch_0
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_3c} :catch_6
    .catchall {:try_start_3c .. :try_end_3c} :catchall_1

    .line 3150
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_38

    .line 3395
    :catch_1a
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_39

    .line 3198
    :cond_6c
    :try_start_3d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->J()I

    move-result v2

    if-lez v2, :cond_6e

    .line 3199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->u:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/c;->i()I

    move-result v2

    .line 3200
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3201
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/samm/b/a/d;->u:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/samm/b/a/d;->u:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v5

    invoke-virtual {v4, v3, v5, v2}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;II)Z
    :try_end_3d
    .catch Ljava/io/FileNotFoundException; {:try_start_3d .. :try_end_3d} :catch_0
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_3d} :catch_6
    .catchall {:try_start_3d .. :try_end_3d} :catchall_1

    move-result v2

    if-nez v2, :cond_6e

    .line 3393
    if-eqz v3, :cond_6d

    .line 3394
    :try_start_3e
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_3e} :catch_1b

    .line 3202
    :cond_6d
    :goto_3c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_1b
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3c

    .line 3209
    :cond_6e
    :try_start_3f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->K()I

    move-result v2

    if-lez v2, :cond_6f

    .line 3211
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    .line 3212
    const/4 v2, 0x0

    .line 3213
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3214
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    .line 3217
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->N:I

    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3220
    const/4 v2, 0x0

    :goto_3d
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/samm/b/a/d;->N:I

    if-lt v2, v10, :cond_71

    .line 3267
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v10

    .line 3268
    sub-long v6, v10, v6

    long-to-int v2, v6

    .line 3269
    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3270
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3271
    invoke-virtual {v3, v10, v11}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3277
    :cond_6f
    invoke-direct/range {p0 .. p0}, Lcom/samsung/samm/b/a/d;->aa()Z
    :try_end_3f
    .catch Ljava/io/FileNotFoundException; {:try_start_3f .. :try_end_3f} :catch_0
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_3f} :catch_6
    .catchall {:try_start_3f .. :try_end_3f} :catchall_1

    move-result v2

    if-nez v2, :cond_7e

    .line 3393
    if-eqz v3, :cond_70

    .line 3394
    :try_start_40
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_40} :catch_21

    .line 3278
    :cond_70
    :goto_3e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3222
    :cond_71
    :try_start_41
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v10

    .line 3223
    const/4 v12, 0x0

    .line 3224
    invoke-static {v3, v12}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3225
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v12

    .line 3228
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v14, v14, v2

    invoke-virtual {v14}, Lcom/samsung/samm/b/a/d$a;->c()I

    move-result v14

    invoke-static {v3, v14}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3230
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v14, v14, v2

    invoke-virtual {v14}, Lcom/samsung/samm/b/a/d$a;->c()I

    move-result v14

    if-eqz v14, :cond_78

    .line 3231
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/samm/b/a/d;->O:Z

    if-nez v14, :cond_78

    if-nez p4, :cond_72

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v14, v14, v2

    invoke-virtual {v14}, Lcom/samsung/samm/b/a/d$a;->a()I

    move-result v14

    if-lez v14, :cond_78

    .line 3232
    :cond_72
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v14, v14, v2

    invoke-virtual {v14}, Lcom/samsung/samm/b/a/d$a;->d()Ljava/lang/String;
    :try_end_41
    .catch Ljava/io/FileNotFoundException; {:try_start_41 .. :try_end_41} :catch_0
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_41} :catch_6
    .catchall {:try_start_41 .. :try_end_41} :catchall_1

    move-result-object v14

    .line 3233
    if-nez v14, :cond_74

    .line 3393
    if-eqz v3, :cond_73

    .line 3394
    :try_start_42
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_42} :catch_1c

    .line 3234
    :cond_73
    :goto_3f
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_1c
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3f

    .line 3235
    :cond_74
    :try_start_43
    invoke-static {v14}, Lcom/samsung/samm/b/a/s;->b(Ljava/lang/String;)I
    :try_end_43
    .catch Ljava/io/FileNotFoundException; {:try_start_43 .. :try_end_43} :catch_0
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_43} :catch_6
    .catchall {:try_start_43 .. :try_end_43} :catchall_1

    move-result v15

    .line 3236
    if-gtz v15, :cond_76

    .line 3393
    if-eqz v3, :cond_75

    .line 3394
    :try_start_44
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_44} :catch_1d

    .line 3237
    :cond_75
    :goto_40
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_1d
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_40

    .line 3239
    :cond_76
    :try_start_45
    invoke-static {v3, v15}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3241
    invoke-static {v3, v14, v15}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_45
    .catch Ljava/io/FileNotFoundException; {:try_start_45 .. :try_end_45} :catch_0
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_45} :catch_6
    .catchall {:try_start_45 .. :try_end_45} :catchall_1

    move-result v14

    if-nez v14, :cond_79

    .line 3393
    if-eqz v3, :cond_77

    .line 3394
    :try_start_46
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_46
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_46} :catch_1e

    .line 3242
    :cond_77
    :goto_41
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_1e
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_41

    .line 3245
    :cond_78
    const/4 v14, 0x0

    :try_start_47
    invoke-static {v3, v14}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3249
    :cond_79
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/samm/b/a/d;->an:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/samm/b/a/d;->l:[Lcom/samsung/samm/b/a/d$a;

    aget-object v15, v15, v2

    invoke-virtual {v15}, Lcom/samsung/samm/b/a/d$a;->b()I

    move-result v15

    invoke-static {v14, v15}, Lcom/samsung/samm/b/a/d;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v14

    .line 3250
    invoke-static {v14}, Lcom/samsung/samm/b/a/s;->b(Ljava/lang/String;)I
    :try_end_47
    .catch Ljava/io/FileNotFoundException; {:try_start_47 .. :try_end_47} :catch_0
    .catch Ljava/io/IOException; {:try_start_47 .. :try_end_47} :catch_6
    .catchall {:try_start_47 .. :try_end_47} :catchall_1

    move-result v15

    .line 3251
    if-gez v15, :cond_7b

    .line 3393
    if-eqz v3, :cond_7a

    .line 3394
    :try_start_48
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_48
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_48} :catch_1f

    .line 3252
    :cond_7a
    :goto_42
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_1f
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_42

    .line 3253
    :cond_7b
    :try_start_49
    invoke-static {v3, v15}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3255
    invoke-static {v3, v14, v15}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    :try_end_49
    .catch Ljava/io/FileNotFoundException; {:try_start_49 .. :try_end_49} :catch_0
    .catch Ljava/io/IOException; {:try_start_49 .. :try_end_49} :catch_6
    .catchall {:try_start_49 .. :try_end_49} :catchall_1

    move-result v14

    if-nez v14, :cond_7d

    .line 3393
    if-eqz v3, :cond_7c

    .line 3394
    :try_start_4a
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4a
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4a} :catch_20

    .line 3256
    :cond_7c
    :goto_43
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_20
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_43

    .line 3259
    :cond_7d
    :try_start_4b
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v14

    .line 3260
    sub-long v12, v14, v12

    long-to-int v12, v12

    .line 3261
    invoke-virtual {v3, v10, v11}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3262
    invoke-static {v3, v12}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3263
    invoke-virtual {v3, v14, v15}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_4b
    .catch Ljava/io/FileNotFoundException; {:try_start_4b .. :try_end_4b} :catch_0
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4b} :catch_6
    .catchall {:try_start_4b .. :try_end_4b} :catchall_1

    .line 3220
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3d

    .line 3395
    :catch_21
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3e

    .line 3284
    :cond_7e
    :try_start_4c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->L()I

    move-result v2

    const/4 v4, 0x4

    if-eq v2, v4, :cond_7f

    .line 3285
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->L()I

    move-result v2

    const/4 v4, 0x5

    if-ne v2, v4, :cond_82

    .line 3287
    :cond_7f
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    .line 3288
    const/4 v2, 0x0

    .line 3289
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3290
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    .line 3293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->p:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/c;->i()I

    move-result v2

    .line 3294
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3295
    if-lez v2, :cond_81

    .line 3296
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/samm/b/a/d;->p:Lcom/samsung/samm/b/a/c;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/samm/b/a/d;->p:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v11}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v11

    invoke-virtual {v10, v3, v11, v2}, Lcom/samsung/samm/b/a/c;->a(Ljava/io/RandomAccessFile;II)Z
    :try_end_4c
    .catch Ljava/io/FileNotFoundException; {:try_start_4c .. :try_end_4c} :catch_0
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_4c} :catch_6
    .catchall {:try_start_4c .. :try_end_4c} :catchall_1

    move-result v2

    if-nez v2, :cond_81

    .line 3393
    if-eqz v3, :cond_80

    .line 3394
    :try_start_4d
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4d .. :try_end_4d} :catch_22

    .line 3297
    :cond_80
    :goto_44
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_22
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_44

    .line 3301
    :cond_81
    :try_start_4e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/samm/b/a/d;->r:Z

    if-eqz v2, :cond_86

    const/4 v2, 0x1

    :goto_45
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;I)V

    .line 3303
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/samm/b/a/d;->s:Z

    if-eqz v2, :cond_87

    const/4 v2, 0x1

    :goto_46
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;I)V

    .line 3305
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/samm/b/a/d;->t:I

    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3308
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v10

    .line 3309
    sub-long v6, v10, v6

    long-to-int v2, v6

    .line 3310
    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3311
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3312
    invoke-virtual {v3, v10, v11}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3319
    :cond_82
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->N()I

    move-result v2

    if-lez v2, :cond_83

    .line 3320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    invoke-virtual {v2}, Lcom/samsung/samm/a/a;->getAppInfoSize()I

    move-result v2

    .line 3321
    new-array v4, v2, [B

    .line 3322
    const/4 v5, 0x0

    .line 3325
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    invoke-virtual {v6}, Lcom/samsung/samm/a/a;->getAppType()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v5

    .line 3329
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    invoke-virtual {v6}, Lcom/samsung/samm/a/a;->getAppPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/samm/b/a/s;->a([BILjava/lang/String;)I

    move-result v5

    .line 3331
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    invoke-virtual {v6}, Lcom/samsung/samm/a/a;->getAppClassName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/samm/b/a/s;->a([BILjava/lang/String;)I

    move-result v5

    .line 3333
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    invoke-virtual {v6}, Lcom/samsung/samm/a/a;->getAppURL()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/samm/b/a/s;->a([BILjava/lang/String;)I

    move-result v5

    .line 3335
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    invoke-virtual {v6}, Lcom/samsung/samm/a/a;->getAppSrcPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/samm/b/a/s;->a([BILjava/lang/String;)I

    move-result v5

    .line 3337
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/samm/b/a/d;->ad:Lcom/samsung/samm/a/a;

    invoke-virtual {v6}, Lcom/samsung/samm/a/a;->getAppTime()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/samm/b/a/s;->a([BIJ)I

    .line 3340
    invoke-static {v3, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 3342
    invoke-virtual {v3, v4}, Ljava/io/RandomAccessFile;->write([B)V

    .line 3365
    :cond_83
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->M()I

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_84

    .line 3366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/h;->M()I
    :try_end_4e
    .catch Ljava/io/FileNotFoundException; {:try_start_4e .. :try_end_4e} :catch_0
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_4e} :catch_6
    .catchall {:try_start_4e .. :try_end_4e} :catchall_1

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_88

    .line 3393
    :cond_84
    if-eqz v3, :cond_85

    .line 3394
    :try_start_4f
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4f
    .catch Ljava/io/IOException; {:try_start_4f .. :try_end_4f} :catch_23

    .line 3368
    :cond_85
    :goto_47
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3301
    :cond_86
    const/4 v2, 0x0

    goto/16 :goto_45

    .line 3303
    :cond_87
    const/4 v2, 0x0

    goto/16 :goto_46

    .line 3395
    :catch_23
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_47

    .line 3375
    :cond_88
    :try_start_50
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->g:Lcom/samsung/samm/b/a/f;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/f;->b()Lcom/samsung/samm/b/a/g;

    move-result-object v2

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    sub-long/2addr v4, v8

    long-to-int v4, v4

    invoke-virtual {v2, v4}, Lcom/samsung/samm/b/a/g;->a(I)V

    .line 3376
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/d;->g:Lcom/samsung/samm/b/a/f;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/f;->b()Lcom/samsung/samm/b/a/g;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/samsung/samm/b/a/g;->b(Ljava/io/RandomAccessFile;)Z
    :try_end_50
    .catch Ljava/io/FileNotFoundException; {:try_start_50 .. :try_end_50} :catch_0
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_50} :catch_6
    .catchall {:try_start_50 .. :try_end_50} :catchall_1

    move-result v2

    if-nez v2, :cond_8a

    .line 3393
    if-eqz v3, :cond_89

    .line 3394
    :try_start_51
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_51} :catch_24

    .line 3377
    :cond_89
    :goto_48
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3395
    :catch_24
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_48

    .line 3381
    :cond_8a
    :try_start_52
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->setLength(J)V
    :try_end_52
    .catch Ljava/io/FileNotFoundException; {:try_start_52 .. :try_end_52} :catch_0
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_52} :catch_6
    .catchall {:try_start_52 .. :try_end_52} :catchall_1

    .line 3393
    if-eqz v3, :cond_8b

    .line 3394
    :try_start_53
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_53
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_53} :catch_28

    .line 3403
    :cond_8b
    :goto_49
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 3395
    :catch_25
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_14

    .line 3395
    :catch_26
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1d

    .line 3391
    :catchall_0
    move-exception v2

    move-object v3, v4

    .line 3393
    :goto_4a
    if-eqz v3, :cond_8c

    .line 3394
    :try_start_54
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_54
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_54} :catch_27

    .line 3399
    :cond_8c
    :goto_4b
    throw v2

    .line 3395
    :catch_27
    move-exception v3

    .line 3396
    const-string v4, "SAMMLibraryCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Read AMS Header Error : IOException : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4b

    .line 3395
    :catch_28
    move-exception v2

    .line 3396
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read AMS Header Error : IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3397
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_49

    .line 3391
    :catchall_1
    move-exception v2

    goto :goto_4a

    .line 3387
    :catch_29
    move-exception v2

    move-object v3, v4

    goto/16 :goto_1c

    .line 3383
    :catch_2a
    move-exception v2

    move-object v3, v4

    goto/16 :goto_13
.end method

.method public a(Ljava/lang/String;[B)Z
    .locals 1

    .prologue
    .line 1343
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;[B)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 368
    iput p1, p0, Lcom/samsung/samm/b/a/d;->B:I

    .line 369
    return-void
.end method

.method public b()Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 291
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 292
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 293
    if-nez v3, :cond_0

    .line 328
    :goto_0
    return v1

    .line 297
    :cond_0
    iget-object v4, p0, Lcom/samsung/samm/b/a/d;->ag:Ljava/lang/String;

    move v0, v1

    .line 299
    :goto_1
    array-length v2, v3

    if-lt v0, v2, :cond_1

    .line 328
    const/4 v1, 0x1

    goto :goto_0

    .line 301
    :cond_1
    aget-object v2, v3, v0

    .line 302
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/samsung/samm/b/a/d;->ah:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 303
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 305
    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 306
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-le v6, v7, :cond_2

    .line 307
    const-string v6, "SPenSDKTemp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 308
    if-gez v6, :cond_3

    .line 299
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 310
    :cond_3
    invoke-virtual {v2, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 313
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    sub-long v6, v8, v6

    .line 318
    const-wide/32 v8, 0x5265c00

    cmp-long v6, v6, v8

    if-ltz v6, :cond_2

    .line 319
    iget-boolean v6, p0, Lcom/samsung/samm/b/a/d;->av:Z

    if-eqz v6, :cond_4

    .line 320
    iget-object v5, p0, Lcom/samsung/samm/b/a/d;->au:Landroid/content/Context;

    invoke-virtual {v5, v2}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_2

    .line 314
    :catch_0
    move-exception v2

    .line 315
    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_2

    .line 322
    :cond_4
    invoke-static {v5}, Lcom/samsung/samm/b/a/s;->a(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public b(Landroid/graphics/Bitmap;)Z
    .locals 3

    .prologue
    .line 636
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->u:Lcom/samsung/samm/b/a/c;

    iget v1, p0, Lcom/samsung/samm/b/a/d;->F:I

    iget v2, p0, Lcom/samsung/samm/b/a/d;->G:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/samsung/samm/b/a/c;->a(Landroid/graphics/Bitmap;II)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;[B)[B
    .locals 1

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1282
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->af:Ljava/lang/String;

    return-object v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 392
    iput p1, p0, Lcom/samsung/samm/b/a/d;->D:I

    .line 393
    return-void
.end method

.method public d()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->i:Ljava/util/LinkedList;

    return-object v0
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 399
    iput p1, p0, Lcom/samsung/samm/b/a/d;->E:I

    .line 400
    return-void
.end method

.method public d(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public e(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 351
    return-void
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 406
    iput p1, p0, Lcom/samsung/samm/b/a/d;->F:I

    .line 407
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 355
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/d;->a(I)V

    .line 356
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/d;->b(I)V

    .line 357
    return-void
.end method

.method public f(I)V
    .locals 0

    .prologue
    .line 413
    iput p1, p0, Lcom/samsung/samm/b/a/d;->G:I

    .line 414
    return-void
.end method

.method public g()I
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/h;->n()I

    move-result v0

    return v0
.end method

.method public g(I)V
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->y:Lcom/samsung/samm/b/a/e;

    const-string v1, "SAMM___LIBRARY___FG___IMAGE___ANGLE___KEY"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;I)Z

    .line 628
    return-void
.end method

.method public h()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Lcom/samsung/samm/b/a/d;->D:I

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 402
    iget v0, p0, Lcom/samsung/samm/b/a/d;->E:I

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 409
    iget v0, p0, Lcom/samsung/samm/b/a/d;->F:I

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 416
    iget v0, p0, Lcom/samsung/samm/b/a/d;->G:I

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/c;->a()I

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/h;->d()I

    move-result v0

    .line 474
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 475
    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 476
    :cond_0
    const/4 v0, 0x1

    .line 478
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/c;->b()I

    move-result v0

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->m:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/c;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->n:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/c;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public q()V
    .locals 2

    .prologue
    .line 617
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->n:Lcom/samsung/samm/b/a/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/samm/b/a/c;->a(I)V

    .line 618
    return-void
.end method

.method public r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->u:Lcom/samsung/samm/b/a/c;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/c;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public s()V
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->u:Lcom/samsung/samm/b/a/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/samm/b/a/c;->a(I)V

    .line 647
    return-void
.end method

.method public t()Z
    .locals 2

    .prologue
    .line 663
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->h:Lcom/samsung/samm/b/a/h;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/h;->q()I

    move-result v0

    .line 664
    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 665
    const/16 v1, 0xb

    if-gt v0, v1, :cond_0

    .line 666
    const/4 v0, 0x1

    .line 668
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 726
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->w:Lcom/samsung/samm/b/a/a;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/a;->b()I

    move-result v0

    return v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->P:Ljava/lang/String;

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 933
    iget-object v0, p0, Lcom/samsung/samm/b/a/d;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public y()I
    .locals 1

    .prologue
    .line 937
    iget v0, p0, Lcom/samsung/samm/b/a/d;->R:I

    return v0
.end method

.method public z()I
    .locals 1

    .prologue
    .line 941
    iget v0, p0, Lcom/samsung/samm/b/a/d;->S:I

    return v0
.end method
