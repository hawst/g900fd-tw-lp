.class public Lcom/samsung/samm/b/a/q;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/samm/b/a/q$a;
    }
.end annotation


# instance fields
.field private a:[Lcom/samsung/samm/b/a/q$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/q;->a()V

    .line 22
    return-void
.end method


# virtual methods
.method public a(Ljava/io/RandomAccessFile;)I
    .locals 14

    .prologue
    .line 183
    const/4 v1, 0x0

    .line 185
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/q;->b()I

    move-result v0

    invoke-static {p1, v0}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 186
    const/4 v1, 0x2

    .line 188
    const/4 v3, -0x1

    .line 189
    const/4 v0, 0x0

    move v7, v0

    move v0, v1

    :goto_0
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/q;->b()I

    move-result v1

    if-lt v7, v1, :cond_0

    .line 274
    :goto_1
    return v0

    .line 191
    :cond_0
    const/4 v1, 0x0

    .line 192
    add-int/lit8 v2, v3, 0x1

    :goto_2
    iget-object v4, p0, Lcom/samsung/samm/b/a/q;->a:[Lcom/samsung/samm/b/a/q$a;

    array-length v4, v4

    if-lt v2, v4, :cond_1

    move-object v5, v1

    move v6, v3

    .line 199
    :goto_3
    if-nez v5, :cond_3

    .line 200
    const/4 v0, -0x1

    goto :goto_1

    .line 193
    :cond_1
    iget-object v4, p0, Lcom/samsung/samm/b/a/q;->a:[Lcom/samsung/samm/b/a/q$a;

    aget-object v4, v4, v2

    invoke-static {v4}, Lcom/samsung/samm/b/a/q$a;->a(Lcom/samsung/samm/b/a/q$a;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 195
    iget-object v1, p0, Lcom/samsung/samm/b/a/q;->a:[Lcom/samsung/samm/b/a/q$a;

    aget-object v1, v1, v2

    move-object v5, v1

    move v6, v2

    .line 196
    goto :goto_3

    .line 192
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 203
    :cond_3
    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->a(Lcom/samsung/samm/b/a/q$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    .line 204
    const/4 v1, 0x0

    .line 205
    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->d(Lcom/samsung/samm/b/a/q$a;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->d(Lcom/samsung/samm/b/a/q$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    move v4, v1

    .line 206
    :goto_4
    mul-int/lit8 v1, v8, 0x2

    add-int/lit8 v1, v1, 0x6

    add-int/lit8 v1, v1, 0x2

    mul-int/lit8 v2, v4, 0x2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x1

    .line 207
    new-array v9, v1, [B

    .line 208
    const/4 v2, 0x0

    .line 211
    invoke-static {p1, v1}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 212
    add-int/lit8 v1, v0, 0x4

    .line 214
    :try_start_2
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v10

    .line 217
    invoke-static {v9, v2, v6}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 220
    invoke-static {v9, v0, v8}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    .line 222
    if-lez v8, :cond_4

    .line 223
    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->a(Lcom/samsung/samm/b/a/q$a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    .line 224
    const/4 v2, 0x0

    :goto_5
    if-lt v2, v8, :cond_6

    .line 230
    :cond_4
    invoke-static {v9, v0, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 232
    if-lez v4, :cond_5

    .line 233
    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->d(Lcom/samsung/samm/b/a/q$a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    .line 234
    const/4 v2, 0x0

    :goto_6
    if-lt v2, v4, :cond_7

    .line 240
    :cond_5
    add-int/lit8 v2, v0, 0x1

    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->e(Lcom/samsung/samm/b/a/q$a;)I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v9, v0

    .line 243
    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->f(Lcom/samsung/samm/b/a/q$a;)I

    move-result v0

    invoke-static {v9, v2, v0}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v2

    .line 247
    const/4 v0, 0x3

    :goto_7
    if-gez v0, :cond_8

    .line 253
    const/4 v0, 0x3

    :goto_8
    if-gez v0, :cond_9

    .line 258
    add-int/lit8 v0, v2, 0x1

    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->g(Lcom/samsung/samm/b/a/q$a;)I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v9, v2

    .line 259
    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->h(Lcom/samsung/samm/b/a/q$a;)I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v9, v0

    .line 261
    invoke-virtual {p1, v9}, Ljava/io/RandomAccessFile;->write([B)V

    .line 263
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v2

    sub-long/2addr v2, v10

    long-to-int v0, v2

    .line 264
    add-int/2addr v1, v0

    .line 189
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v3, v6

    move v0, v1

    goto/16 :goto_0

    .line 225
    :cond_6
    aget-char v3, v12, v2

    invoke-static {v9, v0, v3}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v3

    .line 224
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5

    .line 235
    :cond_7
    aget-char v3, v8, v2

    invoke-static {v9, v0, v3}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v3

    .line 234
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_6

    .line 248
    :cond_8
    add-int/lit8 v3, v2, 0x1

    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->b(Lcom/samsung/samm/b/a/q$a;)I

    move-result v4

    mul-int/lit8 v8, v0, 0x8

    shr-int/2addr v4, v8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v9, v2

    .line 247
    add-int/lit8 v0, v0, -0x1

    move v2, v3

    goto :goto_7

    .line 254
    :cond_9
    add-int/lit8 v3, v2, 0x1

    invoke-static {v5}, Lcom/samsung/samm/b/a/q$a;->c(Lcom/samsung/samm/b/a/q$a;)I

    move-result v4

    mul-int/lit8 v8, v0, 0x8

    shr-int/2addr v4, v8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v9, v2
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 253
    add-int/lit8 v0, v0, -0x1

    move v2, v3

    goto :goto_8

    .line 266
    :catch_0
    move-exception v0

    move-object v13, v0

    move v0, v1

    move-object v1, v13

    .line 267
    :goto_9
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FileNotFoundException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 269
    :catch_1
    move-exception v0

    move-object v13, v0

    move v0, v1

    move-object v1, v13

    .line 270
    :goto_a
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 269
    :catch_2
    move-exception v1

    goto :goto_a

    .line 266
    :catch_3
    move-exception v1

    goto :goto_9

    :cond_a
    move v4, v1

    goto/16 :goto_4
.end method

.method public a()V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/b/a/q;->a:[Lcom/samsung/samm/b/a/q$a;

    .line 26
    new-array v0, v3, [Lcom/samsung/samm/b/a/q$a;

    iput-object v0, p0, Lcom/samsung/samm/b/a/q;->a:[Lcom/samsung/samm/b/a/q$a;

    .line 27
    const/4 v0, 0x0

    :goto_0
    if-lt v0, v3, :cond_0

    .line 31
    return-void

    .line 29
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/q;->a:[Lcom/samsung/samm/b/a/q$a;

    new-instance v2, Lcom/samsung/samm/b/a/q$a;

    invoke-direct {v2, p0}, Lcom/samsung/samm/b/a/q$a;-><init>(Lcom/samsung/samm/b/a/q;)V

    aput-object v2, v1, v0

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/io/RandomAccessFile;Z)Z
    .locals 13

    .prologue
    .line 83
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v4

    .line 84
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-lt v3, v4, :cond_0

    .line 178
    const/4 v0, 0x1

    return v0

    .line 87
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v5

    .line 89
    if-eqz p2, :cond_8

    .line 90
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    .line 93
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v0

    .line 94
    iget-object v1, p0, Lcom/samsung/samm/b/a/q;->a:[Lcom/samsung/samm/b/a/q$a;

    aget-object v8, v1, v0

    .line 97
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 99
    const/4 v0, 0x0

    invoke-static {v8, v0}, Lcom/samsung/samm/b/a/q$a;->a(Lcom/samsung/samm/b/a/q$a;Ljava/lang/String;)V

    .line 100
    if-lez v2, :cond_1

    .line 101
    new-array v9, v2, [C

    .line 102
    mul-int/lit8 v0, v2, 0x2

    new-array v10, v0, [B

    .line 103
    const/4 v1, 0x0

    .line 104
    const/4 v0, 0x1

    new-array v11, v0, [I

    .line 105
    invoke-virtual {p1, v10}, Ljava/io/RandomAccessFile;->read([B)I

    .line 106
    const/4 v0, 0x0

    :goto_1
    if-lt v0, v2, :cond_4

    .line 110
    invoke-static {v9}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/samsung/samm/b/a/q$a;->a(Lcom/samsung/samm/b/a/q$a;Ljava/lang/String;)V

    .line 114
    :cond_1
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 116
    const/4 v0, 0x0

    invoke-static {v8, v0}, Lcom/samsung/samm/b/a/q$a;->b(Lcom/samsung/samm/b/a/q$a;Ljava/lang/String;)V

    .line 117
    if-lez v2, :cond_2

    .line 118
    new-array v9, v2, [C

    .line 119
    mul-int/lit8 v0, v2, 0x2

    new-array v10, v0, [B

    .line 120
    const/4 v1, 0x0

    .line 121
    const/4 v0, 0x1

    new-array v11, v0, [I

    .line 122
    invoke-virtual {p1, v10}, Ljava/io/RandomAccessFile;->read([B)I

    .line 123
    const/4 v0, 0x0

    :goto_2
    if-lt v0, v2, :cond_5

    .line 127
    invoke-static {v9}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/samsung/samm/b/a/q$a;->b(Lcom/samsung/samm/b/a/q$a;Ljava/lang/String;)V

    .line 130
    :cond_2
    const/16 v0, 0xb

    new-array v9, v0, [B

    .line 131
    const/4 v0, 0x0

    .line 132
    const/4 v1, 0x1

    new-array v2, v1, [I

    .line 133
    invoke-virtual {p1, v9}, Ljava/io/RandomAccessFile;->read([B)I

    .line 136
    const/4 v1, 0x1

    aget-byte v0, v9, v0

    and-int/lit16 v0, v0, 0xff

    invoke-static {v8, v0}, Lcom/samsung/samm/b/a/q$a;->a(Lcom/samsung/samm/b/a/q$a;I)V

    .line 139
    invoke-static {v9, v1, v2}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 140
    const/4 v0, 0x0

    aget v0, v2, v0

    invoke-static {v8, v0}, Lcom/samsung/samm/b/a/q$a;->b(Lcom/samsung/samm/b/a/q$a;I)V

    .line 144
    const/4 v0, 0x3

    :goto_3
    if-gez v0, :cond_6

    .line 150
    const/4 v0, 0x3

    :goto_4
    if-gez v0, :cond_7

    .line 154
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    sub-long/2addr v0, v6

    long-to-int v0, v0

    .line 157
    if-le v5, v0, :cond_3

    .line 159
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedByte()I

    move-result v0

    invoke-static {v8, v0}, Lcom/samsung/samm/b/a/q$a;->e(Lcom/samsung/samm/b/a/q$a;I)V

    .line 160
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedByte()I

    move-result v0

    invoke-static {v8, v0}, Lcom/samsung/samm/b/a/q$a;->f(Lcom/samsung/samm/b/a/q$a;I)V

    .line 163
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    sub-long/2addr v0, v6

    long-to-int v0, v0

    .line 164
    sub-int v0, v5, v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 84
    :cond_3
    :goto_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 107
    :cond_4
    invoke-static {v10, v1, v11}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 108
    const/4 v12, 0x0

    aget v12, v11, v12

    int-to-char v12, v12

    aput-char v12, v9, v0

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 124
    :cond_5
    invoke-static {v10, v1, v11}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 125
    const/4 v12, 0x0

    aget v12, v11, v12

    int-to-char v12, v12

    aput-char v12, v9, v0

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 145
    :cond_6
    invoke-static {v8}, Lcom/samsung/samm/b/a/q$a;->b(Lcom/samsung/samm/b/a/q$a;)I

    move-result v10

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v9, v1

    and-int/lit16 v1, v1, 0xff

    mul-int/lit8 v11, v0, 0x8

    shl-int/2addr v1, v11

    add-int/2addr v1, v10

    invoke-static {v8, v1}, Lcom/samsung/samm/b/a/q$a;->c(Lcom/samsung/samm/b/a/q$a;I)V

    .line 144
    add-int/lit8 v0, v0, -0x1

    move v1, v2

    goto :goto_3

    .line 151
    :cond_7
    invoke-static {v8}, Lcom/samsung/samm/b/a/q$a;->c(Lcom/samsung/samm/b/a/q$a;)I

    move-result v10

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v9, v1

    and-int/lit16 v1, v1, 0xff

    mul-int/lit8 v11, v0, 0x8

    shl-int/2addr v1, v11

    add-int/2addr v1, v10

    invoke-static {v8, v1}, Lcom/samsung/samm/b/a/q$a;->d(Lcom/samsung/samm/b/a/q$a;I)V

    .line 150
    add-int/lit8 v0, v0, -0x1

    move v1, v2

    goto :goto_4

    .line 167
    :cond_8
    invoke-virtual {p1, v5}, Ljava/io/RandomAccessFile;->skipBytes(I)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_5

    .line 169
    :catch_0
    move-exception v0

    .line 170
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "FileNotFoundException : "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_5

    .line 172
    :catch_1
    move-exception v0

    .line 173
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "IOException : "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5
.end method

.method public b()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 280
    move v1, v0

    .line 281
    :goto_0
    iget-object v2, p0, Lcom/samsung/samm/b/a/q;->a:[Lcom/samsung/samm/b/a/q$a;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 285
    return v1

    .line 282
    :cond_0
    iget-object v2, p0, Lcom/samsung/samm/b/a/q;->a:[Lcom/samsung/samm/b/a/q$a;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/samsung/samm/b/a/q$a;->a(Lcom/samsung/samm/b/a/q$a;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 283
    add-int/lit8 v1, v1, 0x1

    .line 281
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
