.class public Lcom/samsung/samm/b/a/o;
.super Lcom/samsung/samm/b/a/j;
.source "SourceFile"


# instance fields
.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/b/a/o;->f:I

    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/o;->a(Lcom/samsung/samm/b/a/j;)V

    .line 28
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/samm/b/a/o;->a:I

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/b/a/o;->f:I

    .line 33
    invoke-virtual {p0, p1}, Lcom/samsung/samm/b/a/o;->a(Lcom/samsung/samm/b/a/j;)V

    .line 34
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/samm/b/a/o;->a:I

    .line 35
    iput-object p2, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    .line 36
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58
    if-nez p1, :cond_0

    .line 59
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Text is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :goto_0
    return v0

    .line 62
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 63
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Text length is 0"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 68
    const v1, 0xffff

    if-le v0, v1, :cond_2

    .line 69
    const-string v0, "SAMMLibraryCore"

    const-string v1, "String length exceeds the limit.(65535) The excess will be lost!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/RectF;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1}, Lcom/samsung/samm/b/a/o;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    if-eqz p2, :cond_2

    .line 50
    invoke-direct {p0, p2}, Lcom/samsung/samm/b/a/o;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    :cond_2
    invoke-virtual {p0, p3}, Lcom/samsung/samm/b/a/o;->a(Landroid/graphics/RectF;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 76
    if-nez p1, :cond_0

    .line 77
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Fontface name is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :goto_0
    return v0

    .line 80
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 81
    const-string v1, "SAMMLibraryCore"

    const-string v2, "The length of the fontface name is 0"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 86
    const/16 v2, 0x80

    if-le v1, v2, :cond_2

    .line 87
    const-string v1, "SAMMLibraryCore"

    const-string v2, "The length of the fontface name exceeds the limit(128)"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 91
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private h()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    move v0, v1

    .line 101
    :goto_0
    return v0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/i;

    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->k()Ljava/lang/String;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_1

    .line 99
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 101
    goto :goto_0
.end method

.method private i()I
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->h()I

    move-result v0

    .line 107
    const/16 v1, 0x80

    if-le v0, v1, :cond_0

    .line 108
    add-int/lit8 v0, v0, -0x80

    .line 110
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 115
    iget-object v1, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    if-nez v1, :cond_1

    .line 121
    :cond_0
    :goto_0
    return v0

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    const-string v2, "SAMM___LIBRARY___RICH___TEXT___STRING___KEY"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/samm/a/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 118
    if-eqz v1, :cond_0

    .line 119
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method private k()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v0, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    move v0, v1

    .line 132
    :goto_0
    return v0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/i;

    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->l()Ljava/lang/String;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_1

    .line 130
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 132
    goto :goto_0
.end method


# virtual methods
.method public a([BI)I
    .locals 14

    .prologue
    .line 166
    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    const/4 v0, -0x1

    .line 325
    :goto_0
    return v0

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    .line 170
    :cond_2
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/samm/b/a/o;->b([BI)I

    move-result v0

    .line 171
    if-gez v0, :cond_3

    .line 172
    const/4 v0, -0x1

    goto :goto_0

    .line 173
    :cond_3
    add-int v1, p2, v0

    .line 174
    const/4 v0, 0x1

    new-array v6, v0, [I

    .line 176
    iget-object v0, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/i;

    .line 179
    invoke-static {p1, v1, v6}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v5

    .line 180
    const/4 v1, 0x0

    aget v7, v6, v1

    .line 181
    if-nez v7, :cond_4

    .line 182
    sub-int v0, v5, p2

    goto :goto_0

    .line 183
    :cond_4
    if-gez v7, :cond_5

    .line 184
    const-string v0, "SAMMLibraryCore"

    const-string v1, "SAMM Object Data is Invalid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v0, -0x1

    goto :goto_0

    .line 188
    :cond_5
    add-int v8, v5, v7

    .line 191
    invoke-static {p1, v5, v6}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v2

    .line 192
    const/4 v1, 0x0

    aget v3, v6, v1

    .line 194
    if-lez v3, :cond_b

    .line 196
    new-array v4, v3, [C

    .line 197
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_6

    if-lt v2, v8, :cond_a

    .line 201
    :cond_6
    invoke-static {v4}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/i;->d(Ljava/lang/String;)V

    .line 207
    const/4 v1, 0x0

    .line 208
    if-ge v2, v8, :cond_19

    .line 209
    invoke-static {p1, v2, v6}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v2

    .line 210
    const/4 v1, 0x0

    aget v1, v6, v1

    move v4, v1

    move v1, v2

    .line 213
    :goto_2
    if-lez v4, :cond_8

    .line 215
    new-array v9, v4, [C

    .line 216
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v4, :cond_7

    if-lt v1, v8, :cond_c

    .line 220
    :cond_7
    invoke-static {v9}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/samm/a/i;->e(Ljava/lang/String;)V

    .line 226
    :cond_8
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/samm/b/a/o;->f:I

    .line 227
    if-ge v1, v8, :cond_9

    .line 230
    const/4 v3, 0x0

    .line 231
    const/4 v2, 0x0

    :goto_4
    const/4 v4, 0x4

    if-lt v2, v4, :cond_d

    .line 235
    invoke-virtual {v0, v3}, Lcom/samsung/samm/a/i;->e(I)V

    .line 236
    iget v2, p0, Lcom/samsung/samm/b/a/o;->f:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/samm/b/a/o;->f:I

    .line 239
    if-ge v1, v8, :cond_9

    .line 240
    add-int/lit8 v2, v1, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v3, v1, 0xff

    .line 241
    add-int/lit8 v1, v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    .line 242
    invoke-virtual {v0, v3, v2}, Lcom/samsung/samm/a/i;->a(II)Z

    .line 243
    iget v2, p0, Lcom/samsung/samm/b/a/o;->f:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/samsung/samm/b/a/o;->f:I

    .line 247
    :cond_9
    sub-int/2addr v1, v5

    .line 248
    if-eq v1, v7, :cond_e

    .line 249
    const/4 v0, -0x1

    goto/16 :goto_0

    .line 198
    :cond_a
    invoke-static {p1, v2, v6}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v2

    .line 199
    const/4 v9, 0x0

    aget v9, v6, v9

    int-to-char v9, v9

    aput-char v9, v4, v1

    .line 197
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 203
    :cond_b
    const/4 v0, -0x1

    goto/16 :goto_0

    .line 217
    :cond_c
    invoke-static {p1, v1, v6}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 218
    const/4 v1, 0x0

    aget v1, v6, v1

    int-to-char v1, v1

    aput-char v1, v9, v2

    .line 216
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_3

    .line 232
    :cond_d
    shl-int/lit8 v3, v3, 0x8

    .line 233
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v3, v1

    .line 231
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v4

    goto :goto_4

    .line 254
    :cond_e
    const-string v1, "SAMM___LIBRARY___TEXT___EXTRA___KEY"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/samm/a/i;->b(Ljava/lang/String;[B)[B

    move-result-object v7

    .line 255
    if-eqz v7, :cond_10

    .line 256
    array-length v9, v7

    .line 257
    const/4 v1, 0x0

    .line 260
    invoke-static {v7, v1, v6}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v1

    .line 261
    const/4 v2, 0x0

    aget v10, v6, v2

    .line 262
    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->k()Ljava/lang/String;

    move-result-object v11

    .line 263
    if-lez v10, :cond_15

    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->h()I

    move-result v2

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15

    .line 264
    invoke-static {v7, v1, v6}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v4

    .line 265
    const/4 v1, 0x0

    aget v1, v6, v1

    .line 266
    const/16 v2, 0x80

    if-le v1, v2, :cond_14

    .line 268
    new-array v12, v1, [C

    .line 269
    const/4 v2, 0x0

    move v3, v2

    move v2, v4

    :goto_5
    if-lt v3, v1, :cond_11

    .line 278
    const/4 v3, 0x0

    const/16 v4, 0x80

    invoke-static {v12, v3, v4}, Ljava/lang/String;->copyValueOf([CII)Ljava/lang/String;

    move-result-object v3

    .line 279
    invoke-virtual {v11, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_18

    .line 280
    const v3, 0xffff

    if-ge v1, v3, :cond_13

    .line 281
    :goto_6
    const/4 v3, 0x0

    invoke-static {v12, v3, v1}, Ljava/lang/String;->copyValueOf([CII)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/i;->d(Ljava/lang/String;)V

    move v1, v2

    .line 291
    :goto_7
    if-ge v1, v9, :cond_f

    .line 292
    invoke-static {v7, v1, v6}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v1

    .line 293
    const/4 v2, 0x0

    aget v4, v6, v2

    .line 294
    if-lez v4, :cond_f

    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->h()I

    move-result v2

    if-lez v2, :cond_f

    .line 295
    invoke-static {v7, v1, v6}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v3

    .line 296
    const/4 v1, 0x0

    aget v5, v6, v1

    .line 297
    if-lez v5, :cond_f

    .line 299
    new-array v9, v5, [C

    .line 300
    const/4 v1, 0x0

    move v2, v3

    :goto_8
    if-lt v1, v5, :cond_16

    .line 308
    const-string v1, "SAMM___LIBRARY___RICH___TEXT___STRING___KEY"

    invoke-static {v9}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/samm/a/i;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 322
    :cond_f
    const-string v1, "SAMM___LIBRARY___TEXT___EXTRA___KEY"

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/i;->c(Ljava/lang/String;)Z

    .line 325
    :cond_10
    sub-int v0, v8, p2

    goto/16 :goto_0

    .line 270
    :cond_11
    sub-int v5, v2, v4

    if-ge v5, v10, :cond_12

    add-int/lit8 v5, v2, 0x2

    array-length v13, v7

    if-gt v5, v13, :cond_12

    .line 271
    invoke-static {v7, v2, v6}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v5

    .line 272
    const/4 v2, 0x0

    aget v2, v6, v2

    int-to-char v2, v2

    aput-char v2, v12, v3

    .line 269
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v5

    goto :goto_5

    .line 274
    :cond_12
    const/4 v0, -0x1

    goto/16 :goto_0

    .line 280
    :cond_13
    const v1, 0xffff

    goto :goto_6

    .line 284
    :cond_14
    add-int/lit8 v1, v10, -0x4

    add-int/2addr v1, v4

    .line 286
    goto :goto_7

    .line 287
    :cond_15
    add-int/2addr v1, v10

    goto :goto_7

    .line 301
    :cond_16
    sub-int v10, v2, v3

    if-ge v10, v4, :cond_17

    add-int/lit8 v10, v2, 0x2

    array-length v11, v7

    if-gt v10, v11, :cond_17

    .line 302
    invoke-static {v7, v2, v6}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v2

    .line 303
    const/4 v10, 0x0

    aget v10, v6, v10

    int-to-char v10, v10

    aput-char v10, v9, v1

    .line 300
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 305
    :cond_17
    const/4 v0, -0x1

    goto/16 :goto_0

    :cond_18
    move v1, v2

    goto :goto_7

    :cond_19
    move v4, v1

    move v1, v2

    goto/16 :goto_2
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Lcom/samsung/samm/b/a/j;->a()V

    .line 138
    iget v0, p0, Lcom/samsung/samm/b/a/o;->f:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/samm/b/a/o;->f:I

    .line 139
    iget v0, p0, Lcom/samsung/samm/b/a/o;->f:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/samm/b/a/o;->f:I

    .line 140
    return-void
.end method

.method public a([III)[B
    .locals 11

    .prologue
    .line 329
    iget-object v0, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 469
    :goto_0
    return-object v0

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/o;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/i;

    .line 333
    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->d()Landroid/graphics/RectF;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/samm/b/a/o;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/RectF;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 334
    const-string v0, "SAMMLibraryCore"

    const-string v1, "setTextObject fail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const/4 v0, 0x0

    goto :goto_0

    .line 337
    :cond_1
    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->h()I

    move-result v2

    .line 342
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 345
    const/4 v3, 0x0

    .line 346
    const/4 v1, 0x0

    .line 347
    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->i()I

    move-result v7

    .line 348
    if-lez v7, :cond_13

    .line 349
    const v1, 0xffff

    if-ge v2, v1, :cond_3

    move v1, v2

    .line 350
    :goto_1
    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v4, v3, 0x4

    .line 351
    add-int/lit8 v3, v4, 0x4

    new-array v3, v3, [B

    .line 352
    const/4 v5, 0x0

    .line 354
    invoke-static {v3, v5, v4}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v4

    .line 356
    invoke-static {v3, v4, v1}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v5

    .line 357
    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    .line 358
    const/4 v4, 0x0

    :goto_2
    if-lt v4, v1, :cond_4

    .line 361
    const/4 v1, 0x1

    .line 362
    sub-int/2addr v2, v7

    move v5, v2

    move v2, v1

    move-object v1, v3

    .line 364
    :goto_3
    if-nez v2, :cond_2

    .line 365
    const/4 v1, 0x4

    new-array v1, v1, [B

    .line 366
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/samm/b/a/s;->a([BII)I

    .line 368
    :cond_2
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    const/4 v2, 0x0

    .line 372
    const/4 v1, 0x0

    .line 373
    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->j()I

    move-result v4

    .line 374
    if-lez v4, :cond_12

    .line 375
    mul-int/lit8 v1, v4, 0x2

    add-int/lit8 v1, v1, 0x4

    .line 376
    add-int/lit8 v2, v1, 0x4

    new-array v2, v2, [B

    .line 377
    const/4 v3, 0x0

    .line 379
    invoke-static {v2, v3, v1}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v1

    .line 381
    invoke-static {v2, v1, v4}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v3

    .line 382
    const-string v1, "SAMM___LIBRARY___RICH___TEXT___STRING___KEY"

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v7}, Lcom/samsung/samm/a/i;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 383
    if-nez v1, :cond_5

    .line 384
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 349
    :cond_3
    const v1, 0xffff

    goto :goto_1

    .line 359
    :cond_4
    aget-char v9, v8, v4

    invoke-static {v3, v5, v9}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v5

    .line 358
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 386
    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    .line 388
    const/4 v1, 0x0

    :goto_4
    if-lt v1, v4, :cond_8

    .line 391
    const/4 v1, 0x1

    move v10, v1

    move-object v1, v2

    move v2, v10

    .line 393
    :goto_5
    if-nez v2, :cond_6

    .line 394
    const/4 v1, 0x4

    new-array v1, v1, [B

    .line 395
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/samm/b/a/s;->a([BII)I

    .line 397
    :cond_6
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 400
    invoke-virtual {v6}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v4

    .line 401
    const/4 v2, 0x0

    .line 402
    const/4 v1, 0x0

    move v3, v2

    move v2, v1

    :goto_6
    array-length v1, v4

    if-lt v2, v1, :cond_9

    .line 405
    if-lez v3, :cond_7

    .line 406
    new-array v6, v3, [B

    .line 407
    const/4 v2, 0x0

    .line 408
    const/4 v1, 0x0

    move v3, v2

    move v2, v1

    :goto_7
    array-length v1, v4

    if-lt v2, v1, :cond_a

    .line 414
    const-string v1, "SAMM___LIBRARY___TEXT___EXTRA___KEY"

    invoke-virtual {v0, v1, v6}, Lcom/samsung/samm/a/i;->a(Ljava/lang/String;[B)Z

    .line 417
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/o;->a()V

    .line 419
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/o;->e()I

    move-result v1

    new-array v2, v1, [B

    .line 422
    const/4 v1, 0x0

    invoke-virtual {p0, v2, v1, p2, p3}, Lcom/samsung/samm/b/a/o;->a([BIII)I

    move-result v1

    .line 423
    if-gez v1, :cond_b

    .line 424
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 389
    :cond_8
    aget-char v8, v7, v1

    invoke-static {v2, v3, v8}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v3

    .line 388
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 403
    :cond_9
    aget-object v1, v4, v2

    check-cast v1, [B

    array-length v1, v1

    add-int/2addr v3, v1

    .line 402
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_6

    .line 409
    :cond_a
    aget-object v1, v4, v2

    check-cast v1, [B

    array-length v1, v1

    .line 410
    aget-object v7, v4, v2

    const/4 v8, 0x0

    invoke-static {v7, v8, v6, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411
    const/4 v7, 0x0

    aput-object v7, v4, v2

    .line 412
    add-int/2addr v3, v1

    .line 408
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_7

    .line 427
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/o;->g()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    .line 428
    invoke-static {v2, v1, v3}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v1

    .line 431
    invoke-static {v2, v1, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v1

    .line 433
    if-lez v5, :cond_c

    .line 435
    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 436
    const/4 v3, 0x0

    :goto_8
    if-lt v3, v5, :cond_e

    .line 442
    :cond_c
    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->k()I

    move-result v5

    .line 443
    invoke-static {v2, v1, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v1

    .line 445
    if-lez v5, :cond_d

    .line 447
    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 448
    const/4 v3, 0x0

    :goto_9
    if-lt v3, v5, :cond_f

    .line 458
    :cond_d
    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->m()I

    move-result v5

    .line 459
    const/16 v3, 0x18

    move v10, v3

    move v3, v1

    move v1, v10

    :goto_a
    if-gez v1, :cond_10

    .line 463
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->n()I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 464
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->o()I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v2, v1

    .line 466
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/o;->e()I

    move-result v0

    if-eq v3, v0, :cond_11

    .line 467
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 437
    :cond_e
    aget-char v4, v6, v3

    invoke-static {v2, v1, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 436
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v4

    goto :goto_8

    .line 449
    :cond_f
    aget-char v4, v6, v3

    invoke-static {v2, v1, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 448
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v4

    goto :goto_9

    .line 460
    :cond_10
    add-int/lit8 v4, v3, 0x1

    shr-int v6, v5, v1

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    aput-byte v6, v2, v3

    .line 459
    add-int/lit8 v1, v1, -0x8

    move v3, v4

    goto :goto_a

    :cond_11
    move-object v0, v2

    .line 469
    goto/16 :goto_0

    :cond_12
    move v10, v1

    move-object v1, v2

    move v2, v10

    goto/16 :goto_5

    :cond_13
    move v5, v2

    move v2, v1

    move-object v1, v3

    goto/16 :goto_3
.end method

.method public g()I
    .locals 3

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    iget v1, p0, Lcom/samsung/samm/b/a/o;->f:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    .line 153
    :cond_0
    iget v1, p0, Lcom/samsung/samm/b/a/o;->f:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x2

    .line 156
    :cond_1
    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->h()I

    move-result v1

    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->i()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {p0}, Lcom/samsung/samm/b/a/o;->k()I

    move-result v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    return v0
.end method
