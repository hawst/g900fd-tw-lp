.class public Lcom/samsung/samm/b/a/p;
.super Lcom/samsung/samm/b/a/j;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/p;->a(Lcom/samsung/samm/b/a/j;)V

    .line 34
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/samm/b/a/p;->a:I

    .line 35
    return-void
.end method

.method public constructor <init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 38
    invoke-virtual {p0, p1}, Lcom/samsung/samm/b/a/p;->a(Lcom/samsung/samm/b/a/j;)V

    .line 39
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/samm/b/a/p;->a:I

    .line 40
    iput-object p2, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    .line 41
    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 255
    invoke-static {p0}, Lcom/samsung/samm/b/a/p;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 256
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Invalid Video File"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    :goto_0
    return v0

    .line 261
    :cond_0
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 263
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 264
    const-string v2, "3gp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 265
    const/4 v0, 0x3

    .line 266
    goto :goto_0

    .line 267
    :cond_1
    const-string v2, "mp4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 268
    const/4 v0, 0x4

    .line 269
    goto :goto_0

    .line 270
    :cond_2
    const-string v2, "ts"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 271
    const/4 v0, 0x5

    .line 272
    goto :goto_0

    .line 273
    :cond_3
    const-string v2, "webm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    .line 274
    const/4 v0, 0x6

    .line 275
    goto :goto_0

    .line 276
    :cond_4
    const-string v2, "mkv"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 277
    const/4 v0, 0x7

    .line 278
    goto :goto_0

    .line 281
    :cond_5
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Unsupported video file format"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 288
    if-nez p0, :cond_0

    .line 311
    :goto_0
    return v0

    .line 291
    :cond_0
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 293
    :try_start_0
    invoke-virtual {v2, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 294
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xa

    if-le v3, v4, :cond_2

    .line 295
    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    .line 296
    const-string v4, "video/"

    .line 297
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 298
    if-eqz v3, :cond_1

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-le v3, v5, :cond_1

    .line 313
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    move v0, v1

    .line 299
    goto :goto_0

    .line 313
    :cond_1
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    move v0, v1

    .line 304
    goto :goto_0

    .line 306
    :catch_0
    move-exception v1

    .line 307
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 309
    :catch_1
    move-exception v1

    .line 310
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 313
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 312
    :catchall_0
    move-exception v0

    .line 313
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 314
    throw v0
.end method

.method private h()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    move v0, v1

    .line 98
    :goto_0
    return v0

    .line 85
    :cond_0
    const/4 v0, 0x0

    .line 86
    iget-object v2, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->a()I

    move-result v2

    if-nez v2, :cond_2

    .line 87
    iget-object v2, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    const-string v3, "SAMM___LIBRARY___CONTENT___SIZE___KEY"

    invoke-virtual {v2, v3, v1}, Lcom/samsung/samm/a/d;->d(Ljava/lang/String;I)I

    move-result v2

    .line 88
    if-gtz v2, :cond_1

    .line 89
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/j;

    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->k()Ljava/lang/String;

    move-result-object v0

    .line 95
    :cond_1
    :goto_1
    if-eqz v0, :cond_3

    .line 96
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 91
    :cond_2
    iget-object v2, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->a()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 92
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/j;

    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 98
    goto :goto_0
.end method

.method private i()[C
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 103
    .line 104
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/j;

    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->k()Ljava/lang/String;

    move-result-object v0

    .line 110
    :goto_0
    if-eqz v0, :cond_1

    .line 111
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 113
    :goto_1
    return-object v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->a()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 107
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/j;

    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 113
    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a([BI)I
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 147
    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    move v0, v2

    .line 200
    :goto_0
    return v0

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    .line 151
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/samsung/samm/b/a/p;->b([BI)I

    move-result v6

    .line 152
    if-gez v6, :cond_3

    move v0, v2

    .line 153
    goto :goto_0

    .line 154
    :cond_3
    add-int v1, p2, v6

    .line 155
    new-array v7, v11, [I

    .line 157
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/j;

    .line 158
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->a()I

    move-result v3

    if-eqz v3, :cond_4

    .line 159
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->a()I

    move-result v3

    if-ne v3, v11, :cond_5

    .line 160
    :cond_4
    const-string v3, "SAMM___LIBRARY___CONTENT___ID___KEY"

    iget v5, p0, Lcom/samsung/samm/b/a/p;->b:I

    invoke-virtual {v0, v3, v5}, Lcom/samsung/samm/a/j;->c(Ljava/lang/String;I)Z

    .line 164
    :cond_5
    invoke-static {p1, v1, v7}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v1

    .line 165
    aget v3, v7, v4

    .line 166
    if-nez v3, :cond_6

    .line 167
    sub-int v0, v1, p2

    goto :goto_0

    .line 168
    :cond_6
    if-gez v3, :cond_7

    .line 169
    const-string v0, "SAMMLibraryCore"

    const-string v1, "SAMM Object Data is Invalid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 170
    goto :goto_0

    .line 172
    :cond_7
    add-int v8, v1, v3

    .line 175
    invoke-static {p1, v1, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 176
    aget v9, v7, v4

    .line 179
    if-lez v9, :cond_c

    .line 180
    new-array v10, v9, [C

    move v3, v4

    .line 182
    :goto_1
    if-ge v3, v9, :cond_8

    if-lt v1, v8, :cond_9

    .line 186
    :cond_8
    if-ge v3, v9, :cond_a

    move v0, v2

    .line 187
    goto :goto_0

    .line 183
    :cond_9
    invoke-static {p1, v1, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v5

    .line 184
    aget v1, v7, v4

    int-to-char v1, v1

    aput-char v1, v10, v3

    .line 182
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v5

    goto :goto_1

    .line 189
    :cond_a
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->a()I

    move-result v3

    if-nez v3, :cond_b

    .line 190
    invoke-static {v10}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/samm/a/j;->e(Ljava/lang/String;)Z

    move v0, v1

    .line 196
    :goto_2
    add-int v1, p2, v6

    sub-int/2addr v0, v1

    .line 197
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/p;->g()I

    move-result v1

    if-eq v0, v1, :cond_d

    move v0, v2

    .line 198
    goto :goto_0

    .line 191
    :cond_b
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->a()I

    move-result v3

    if-ne v3, v11, :cond_c

    .line 192
    invoke-static {v10}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/samm/a/j;->f(Ljava/lang/String;)Z

    :cond_c
    move v0, v1

    goto :goto_2

    .line 200
    :cond_d
    sub-int v0, v8, p2

    goto/16 :goto_0
.end method

.method protected a()V
    .locals 0

    .prologue
    .line 118
    invoke-super {p0}, Lcom/samsung/samm/b/a/j;->a()V

    .line 119
    return-void
.end method

.method public a([III)[B
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 204
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 249
    :goto_0
    return-object v0

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/j;

    .line 207
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->a()I

    move-result v2

    if-eqz v2, :cond_1

    .line 208
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->a()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_3

    .line 209
    :cond_1
    const-string v2, "SAMM___LIBRARY___CONTENT___ID___KEY"

    const/4 v4, -0x1

    invoke-virtual {v0, v2, v4}, Lcom/samsung/samm/a/j;->d(Ljava/lang/String;I)I

    move-result v0

    .line 210
    if-gez v0, :cond_2

    .line 211
    const-string v0, "SAMMLibraryCore"

    const-string v2, "Image ID < 0"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 212
    goto :goto_0

    .line 214
    :cond_2
    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/p;->d(I)Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 215
    goto :goto_0

    .line 219
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/p;->a()V

    .line 221
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/p;->e()I

    move-result v0

    new-array v2, v0, [B

    .line 224
    invoke-virtual {p0, v2, v3, p2, p3}, Lcom/samsung/samm/b/a/p;->a([BIII)I

    move-result v0

    .line 225
    if-gez v0, :cond_4

    move-object v0, v1

    .line 226
    goto :goto_0

    .line 229
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/p;->g()I

    move-result v4

    add-int/lit8 v4, v4, -0x4

    .line 230
    invoke-static {v2, v0, v4}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    .line 233
    invoke-direct {p0}, Lcom/samsung/samm/b/a/p;->h()I

    move-result v5

    .line 234
    invoke-static {v2, v0, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 237
    if-lez v5, :cond_7

    .line 238
    invoke-direct {p0}, Lcom/samsung/samm/b/a/p;->i()[C

    move-result-object v6

    .line 239
    if-nez v6, :cond_6

    move-object v0, v1

    .line 240
    goto :goto_0

    .line 242
    :cond_5
    aget-char v4, v6, v3

    invoke-static {v2, v0, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 241
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v4

    :cond_6
    if-lt v3, v5, :cond_5

    .line 246
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/p;->e()I

    move-result v3

    if-eq v0, v3, :cond_8

    move-object v0, v1

    .line 247
    goto :goto_0

    :cond_8
    move-object v0, v2

    .line 249
    goto :goto_0
.end method

.method public d(I)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 52
    iget-object v0, p0, Lcom/samsung/samm/b/a/p;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/j;

    .line 58
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->d()Landroid/graphics/RectF;

    move-result-object v3

    .line 59
    if-nez v3, :cond_0

    .line 60
    const-string v0, "SAMMLibraryCore"

    const-string v2, "Video rect is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 78
    :goto_0
    return v0

    .line 63
    :cond_0
    invoke-virtual {p0, v3}, Lcom/samsung/samm/b/a/p;->a(Landroid/graphics/RectF;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 64
    goto :goto_0

    .line 66
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->a()I

    move-result v3

    if-nez v3, :cond_2

    .line 67
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/samm/b/a/p;->a(Ljava/lang/String;)I

    move-result v0

    .line 68
    if-nez v0, :cond_3

    move v0, v1

    .line 69
    goto :goto_0

    .line 70
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->a()I

    move-result v3

    if-ne v3, v2, :cond_3

    .line 71
    invoke-virtual {v0}, Lcom/samsung/samm/a/j;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    .line 72
    goto :goto_0

    .line 75
    :cond_3
    invoke-virtual {p0, p1}, Lcom/samsung/samm/b/a/p;->c(I)V

    move v0, v2

    .line 78
    goto :goto_0
.end method

.method public g()I
    .locals 2

    .prologue
    .line 130
    const/4 v0, 0x0

    .line 134
    invoke-direct {p0}, Lcom/samsung/samm/b/a/p;->h()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x6

    add-int/2addr v0, v1

    return v0
.end method
