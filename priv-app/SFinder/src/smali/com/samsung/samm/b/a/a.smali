.class public Lcom/samsung/samm/b/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/samm/b/a/a$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/b/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/samsung/samm/b/a/a;->a:Ljava/util/LinkedList;

    .line 20
    iput-object v0, p0, Lcom/samsung/samm/b/a/a;->b:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/samsung/samm/b/a/a;->c:Ljava/lang/String;

    .line 24
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/a;->a()V

    .line 25
    iput-object p1, p0, Lcom/samsung/samm/b/a/a;->b:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/samsung/samm/b/a/a;->c:Ljava/lang/String;

    .line 27
    return-void
.end method

.method private a(ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 350
    if-nez p2, :cond_0

    .line 351
    const/4 v0, 0x0

    .line 358
    :goto_0
    return-object v0

    .line 352
    :cond_0
    const-string v0, "."

    invoke-virtual {p2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 353
    if-lez v0, :cond_1

    .line 354
    const/4 v1, 0x0

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 355
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 356
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsung/samm/b/a/a;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/samm/b/a/a;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 358
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/a;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(ILjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 364
    if-eqz p2, :cond_0

    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/a;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_.png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 367
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/a;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/samm/b/a/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_.png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/io/RandomAccessFile;)I
    .locals 19

    .prologue
    .line 249
    const/4 v3, 0x0

    .line 251
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/a;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;I)V

    .line 252
    const/4 v3, 0x2

    .line 254
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/b/a/a;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    .line 339
    :goto_1
    return v2

    .line 254
    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/samm/b/a/a$a;

    .line 255
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->b(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->c(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 256
    :cond_1
    const/4 v2, -0x1

    goto :goto_1

    .line 259
    :cond_2
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->c(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    .line 260
    const/4 v4, 0x0

    .line 261
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->d(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 262
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->d(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    move v8, v4

    .line 263
    :goto_2
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->b(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/samm/b/a/s;->b(Ljava/lang/String;)I

    move-result v11

    .line 264
    if-gez v11, :cond_3

    .line 265
    const/4 v2, -0x1

    goto :goto_1

    .line 266
    :cond_3
    const/4 v4, 0x0

    .line 267
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->a(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 268
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->a(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/samm/b/a/s;->b(Ljava/lang/String;)I

    move-result v4

    .line 269
    if-gez v4, :cond_4

    .line 270
    const/4 v2, -0x1

    goto :goto_1

    :cond_4
    move v7, v4

    .line 272
    mul-int/lit8 v4, v10, 0x2

    add-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x2

    mul-int/lit8 v5, v8, 0x2

    add-int/2addr v4, v5

    new-array v12, v4, [B

    .line 273
    const/4 v4, 0x0

    .line 276
    invoke-virtual/range {p1 .. p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v14

    .line 277
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 278
    add-int/lit8 v3, v3, 0x4

    .line 280
    invoke-virtual/range {p1 .. p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v16

    .line 283
    invoke-static {v12, v4, v10}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v4

    .line 285
    if-lez v10, :cond_5

    .line 286
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->c(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    .line 287
    const/4 v5, 0x0

    :goto_3
    if-lt v5, v10, :cond_7

    .line 292
    :cond_5
    invoke-static {v12, v4, v8}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v5

    .line 294
    if-lez v8, :cond_6

    .line 295
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->d(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 296
    const/4 v4, 0x0

    :goto_4
    if-lt v4, v8, :cond_8

    .line 300
    :cond_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/io/RandomAccessFile;->write([B)V

    .line 303
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 305
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->b(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4, v11}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_9

    .line 306
    const/4 v2, -0x1

    goto/16 :goto_1

    .line 288
    :cond_7
    aget-char v6, v13, v5

    invoke-static {v12, v4, v6}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v6

    .line 287
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v6

    goto :goto_3

    .line 297
    :cond_8
    aget-char v10, v6, v4

    invoke-static {v12, v5, v10}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v5

    .line 296
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 310
    :cond_9
    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 311
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->a(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 313
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->a(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4, v7}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_a

    .line 314
    const/4 v2, -0x1

    goto/16 :goto_1

    .line 318
    :cond_a
    invoke-static {v2}, Lcom/samsung/samm/b/a/a$a;->e(Lcom/samsung/samm/b/a/a$a;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/io/RandomAccessFile;->write(I)V

    .line 320
    invoke-virtual/range {p1 .. p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    .line 321
    sub-long v6, v4, v16

    long-to-int v2, v6

    .line 324
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 325
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;I)V

    .line 326
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 329
    add-int/2addr v3, v2

    goto/16 :goto_0

    .line 331
    :catch_0
    move-exception v2

    move-object/from16 v18, v2

    move v2, v3

    move-object/from16 v3, v18

    .line 332
    const-string v4, "SAMMLibraryCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FileNotFoundException : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 334
    :catch_1
    move-exception v2

    move-object/from16 v18, v2

    move v2, v3

    move-object/from16 v3, v18

    .line 335
    const-string v4, "SAMMLibraryCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "IOException : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    :cond_b
    move v8, v4

    goto/16 :goto_2
.end method

.method public a()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/samm/b/a/a;->a:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    .line 31
    iget-object v0, p0, Lcom/samsung/samm/b/a/a;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/samsung/samm/b/a/a;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 38
    :goto_1
    return-void

    .line 31
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/a$a;

    .line 32
    invoke-static {v0}, Lcom/samsung/samm/b/a/a$a;->a(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/samm/b/a/s;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_1
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/b/a/a;->a:Ljava/util/LinkedList;

    goto :goto_1
.end method

.method public a(Ljava/io/RandomAccessFile;Z)Z
    .locals 13

    .prologue
    .line 148
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v3

    .line 149
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 244
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 151
    :cond_0
    :try_start_0
    new-instance v4, Lcom/samsung/samm/b/a/a$a;

    invoke-direct {v4, p0}, Lcom/samsung/samm/b/a/a$a;-><init>(Lcom/samsung/samm/b/a/a;)V

    .line 154
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v5

    .line 156
    if-eqz p2, :cond_8

    .line 157
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    .line 160
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v8

    .line 162
    if-lez v8, :cond_1

    .line 163
    new-array v9, v8, [C

    .line 164
    mul-int/lit8 v0, v8, 0x2

    new-array v10, v0, [B

    .line 165
    const/4 v1, 0x0

    .line 166
    const/4 v0, 0x1

    new-array v11, v0, [I

    .line 167
    invoke-virtual {p1, v10}, Ljava/io/RandomAccessFile;->read([B)I

    .line 168
    const/4 v0, 0x0

    :goto_2
    if-lt v0, v8, :cond_3

    .line 172
    invoke-static {v9}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/samsung/samm/b/a/a$a;->c(Lcom/samsung/samm/b/a/a$a;Ljava/lang/String;)V

    .line 176
    :cond_1
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v8

    .line 178
    if-lez v8, :cond_2

    .line 179
    new-array v9, v8, [C

    .line 180
    mul-int/lit8 v0, v8, 0x2

    new-array v10, v0, [B

    .line 181
    const/4 v1, 0x0

    .line 182
    const/4 v0, 0x1

    new-array v11, v0, [I

    .line 183
    invoke-virtual {p1, v10}, Ljava/io/RandomAccessFile;->read([B)I

    .line 184
    const/4 v0, 0x0

    :goto_3
    if-lt v0, v8, :cond_4

    .line 188
    invoke-static {v9}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/samsung/samm/b/a/a$a;->b(Lcom/samsung/samm/b/a/a$a;Ljava/lang/String;)V

    .line 192
    :cond_2
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v0

    .line 195
    invoke-static {v4}, Lcom/samsung/samm/b/a/a$a;->c(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v2, v1}, Lcom/samsung/samm/b/a/a;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-static {v4, v1}, Lcom/samsung/samm/b/a/a$a;->a(Lcom/samsung/samm/b/a/a$a;Ljava/lang/String;)V

    .line 199
    invoke-static {p1, v1, v0}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 200
    const/4 v0, 0x0

    goto :goto_1

    .line 169
    :cond_3
    invoke-static {v10, v1, v11}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 170
    const/4 v12, 0x0

    aget v12, v11, v12

    int-to-char v12, v12

    aput-char v12, v9, v0

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 185
    :cond_4
    invoke-static {v10, v1, v11}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 186
    const/4 v12, 0x0

    aget v12, v11, v12

    int-to-char v12, v12

    aput-char v12, v9, v0

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 203
    :cond_5
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v0

    .line 206
    if-lez v0, :cond_6

    .line 209
    invoke-static {v4}, Lcom/samsung/samm/b/a/a$a;->c(Lcom/samsung/samm/b/a/a$a;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v2, v1}, Lcom/samsung/samm/b/a/a;->b(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 210
    invoke-static {v4, v1}, Lcom/samsung/samm/b/a/a$a;->d(Lcom/samsung/samm/b/a/a$a;Ljava/lang/String;)V

    .line 213
    invoke-static {p1, v1, v0}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 214
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 218
    :cond_6
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedByte()I

    move-result v0

    invoke-static {v4, v0}, Lcom/samsung/samm/b/a/a$a;->a(Lcom/samsung/samm/b/a/a$a;I)V

    .line 219
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    sub-long/2addr v0, v6

    long-to-int v0, v0

    .line 222
    if-le v5, v0, :cond_7

    .line 226
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    sub-long/2addr v0, v6

    long-to-int v0, v0

    .line 227
    sub-int v0, v5, v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 231
    :cond_7
    iget-object v0, p0, Lcom/samsung/samm/b/a/a;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 149
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 233
    :cond_8
    invoke-virtual {p1, v5}, Ljava/io/RandomAccessFile;->skipBytes(I)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_4

    .line 235
    :catch_0
    move-exception v0

    .line 236
    const-string v1, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FileNotFoundException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_4

    .line 238
    :catch_1
    move-exception v0

    .line 239
    const-string v1, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4
.end method

.method public b()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/samm/b/a/a;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method
