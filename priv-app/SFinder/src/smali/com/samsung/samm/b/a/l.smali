.class public Lcom/samsung/samm/b/a/l;
.super Lcom/samsung/samm/b/a/j;
.source "SourceFile"


# instance fields
.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 24
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/l;->a(Lcom/samsung/samm/b/a/j;)V

    .line 25
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/samm/b/a/l;->a:I

    .line 26
    iput v1, p0, Lcom/samsung/samm/b/a/l;->f:I

    .line 27
    iput v1, p0, Lcom/samsung/samm/b/a/l;->g:I

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 31
    invoke-virtual {p0, p1}, Lcom/samsung/samm/b/a/l;->a(Lcom/samsung/samm/b/a/j;)V

    .line 32
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/samm/b/a/l;->a:I

    .line 33
    iput-object p2, p0, Lcom/samsung/samm/b/a/l;->d:Lcom/samsung/samm/a/d;

    .line 34
    iput v1, p0, Lcom/samsung/samm/b/a/l;->f:I

    .line 35
    iput v1, p0, Lcom/samsung/samm/b/a/l;->g:I

    .line 36
    return-void
.end method

.method private a(II)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/l;->a()V

    .line 115
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/l;->e()I

    move-result v1

    new-array v1, v1, [B

    .line 118
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, p1, p2}, Lcom/samsung/samm/b/a/l;->a([BIII)I

    move-result v2

    .line 119
    if-gez v2, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-object v0

    .line 123
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/l;->g()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    .line 124
    invoke-static {v1, v2, v3}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v2

    .line 127
    iput v2, p0, Lcom/samsung/samm/b/a/l;->g:I

    .line 128
    add-int/lit8 v2, v2, 0x4

    .line 130
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/l;->e()I

    move-result v3

    if-ne v2, v3, :cond_0

    move-object v0, v1

    .line 133
    goto :goto_0
.end method

.method private c([BI)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, -0x1

    .line 80
    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/samsung/samm/b/a/l;->b([BI)I

    move-result v1

    .line 84
    if-ltz v1, :cond_0

    .line 86
    add-int/2addr v1, p2

    .line 87
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 90
    invoke-static {p1, v1, v2}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v1

    .line 91
    aget v3, v2, v6

    .line 92
    if-nez v3, :cond_2

    .line 93
    sub-int v0, v1, p2

    goto :goto_0

    .line 94
    :cond_2
    if-gez v3, :cond_3

    .line 95
    const-string v1, "SAMMLibraryCore"

    const-string v2, "SAMM Object Data is Invalid"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 99
    :cond_3
    add-int v4, v1, v3

    .line 102
    invoke-static {p1, v1, v2}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v5

    .line 103
    aget v2, v2, v6

    iput v2, p0, Lcom/samsung/samm/b/a/l;->f:I

    .line 105
    sub-int v1, v5, v1

    .line 106
    if-ne v1, v3, :cond_0

    .line 109
    sub-int v0, v4, p2

    goto :goto_0
.end method


# virtual methods
.method public a([BI)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 137
    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    move v0, v6

    .line 158
    :goto_0
    return v0

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/l;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_2

    move v0, v6

    goto :goto_0

    .line 141
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/samm/b/a/l;->c([BI)I

    move-result v0

    .line 142
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/l;->e()I

    move-result v1

    if-eq v0, v1, :cond_3

    move v0, v6

    .line 143
    goto :goto_0

    .line 144
    :cond_3
    add-int v3, p2, v0

    .line 147
    iget-object v0, p0, Lcom/samsung/samm/b/a/l;->d:Lcom/samsung/samm/a/d;

    move-object v2, v0

    check-cast v2, Lcom/samsung/samm/a/f;

    .line 148
    invoke-virtual {v2}, Lcom/samsung/samm/a/f;->k()Ljava/util/LinkedList;

    move-result-object v0

    .line 149
    const/4 v1, 0x1

    new-array v1, v1, [I

    .line 150
    iget v4, p0, Lcom/samsung/samm/b/a/l;->f:I

    aput v4, v1, v7

    .line 151
    const/4 v4, 0x0

    .line 152
    invoke-virtual {v2}, Lcom/samsung/samm/a/f;->l()I

    move-result v5

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;[I[BILcom/samsung/samm/b/a/j;I)I

    move-result v0

    .line 153
    if-gez v0, :cond_4

    move v0, v6

    .line 154
    goto :goto_0

    .line 156
    :cond_4
    aget v1, v1, v7

    iput v1, p0, Lcom/samsung/samm/b/a/l;->f:I

    .line 158
    sub-int/2addr v0, p2

    goto :goto_0
.end method

.method protected a()V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0}, Lcom/samsung/samm/b/a/j;->a()V

    .line 53
    return-void
.end method

.method public a([III)[B
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 162
    iget-object v0, p0, Lcom/samsung/samm/b/a/l;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    move-object v0, v4

    .line 203
    :goto_0
    return-object v0

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/l;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/f;

    .line 165
    invoke-virtual {v0}, Lcom/samsung/samm/a/f;->k()Ljava/util/LinkedList;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-gtz v1, :cond_2

    :cond_1
    move-object v0, v4

    .line 167
    goto :goto_0

    .line 169
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 172
    invoke-direct {p0, p2, p3}, Lcom/samsung/samm/b/a/l;->a(II)[B

    move-result-object v3

    .line 173
    if-nez v3, :cond_3

    move-object v0, v4

    .line 174
    goto :goto_0

    .line 175
    :cond_3
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    aget v5, p1, v2

    .line 179
    new-instance v6, Landroid/graphics/Rect;

    neg-int v7, p2

    neg-int v8, p3

    invoke-direct {v6, v7, v8, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 180
    invoke-static {v0, v1, v4, p1, v6}, Lcom/samsung/samm/b/a/d;->a(Ljava/util/LinkedList;Ljava/util/ArrayList;Lcom/samsung/samm/b/a/j;[ILandroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v4

    .line 181
    goto :goto_0

    .line 184
    :cond_4
    iget v0, p0, Lcom/samsung/samm/b/a/l;->g:I

    aget v6, p1, v2

    sub-int v5, v6, v5

    invoke-static {v3, v0, v5}, Lcom/samsung/samm/b/a/s;->a([BII)I

    .line 187
    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v5

    move v1, v2

    move v3, v2

    .line 189
    :goto_1
    array-length v0, v5

    if-lt v1, v0, :cond_5

    .line 192
    if-lez v3, :cond_7

    .line 193
    new-array v4, v3, [B

    move v1, v2

    move v3, v2

    .line 195
    :goto_2
    array-length v0, v5

    if-lt v1, v0, :cond_6

    move-object v0, v4

    .line 200
    goto :goto_0

    .line 190
    :cond_5
    aget-object v0, v5, v1

    check-cast v0, [B

    array-length v0, v0

    add-int/2addr v3, v0

    .line 189
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 196
    :cond_6
    aget-object v0, v5, v1

    check-cast v0, [B

    array-length v0, v0

    .line 197
    aget-object v6, v5, v1

    invoke-static {v6, v2, v4, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    add-int/2addr v3, v0

    .line 195
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_7
    move-object v0, v4

    .line 203
    goto :goto_0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 64
    .line 67
    const/16 v0, 0x8

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/samm/b/a/l;->f:I

    return v0
.end method
