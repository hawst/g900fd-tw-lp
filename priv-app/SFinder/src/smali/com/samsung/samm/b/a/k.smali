.class public Lcom/samsung/samm/b/a/k;
.super Lcom/samsung/samm/b/a/j;
.source "SourceFile"


# instance fields
.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/b/a/k;->f:I

    .line 21
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/k;->a(Lcom/samsung/samm/b/a/j;)V

    .line 22
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/samm/b/a/k;->a:I

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/b/a/k;->f:I

    .line 27
    invoke-virtual {p0, p1}, Lcom/samsung/samm/b/a/k;->a(Lcom/samsung/samm/b/a/j;)V

    .line 28
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/samm/b/a/k;->a:I

    .line 29
    iput-object p2, p0, Lcom/samsung/samm/b/a/k;->d:Lcom/samsung/samm/a/d;

    .line 30
    return-void
.end method


# virtual methods
.method public a([BI)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, -0x1

    .line 73
    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    move v0, v1

    .line 122
    :goto_0
    return v0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/k;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    .line 77
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/samsung/samm/b/a/k;->b([BI)I

    move-result v0

    .line 78
    if-gez v0, :cond_3

    move v0, v1

    .line 79
    goto :goto_0

    .line 80
    :cond_3
    add-int v2, p2, v0

    .line 81
    const/4 v0, 0x1

    new-array v3, v0, [I

    .line 83
    iget-object v0, p0, Lcom/samsung/samm/b/a/k;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/e;

    .line 86
    invoke-static {p1, v2, v3}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v4

    .line 87
    aget v5, v3, v9

    .line 88
    if-nez v5, :cond_4

    .line 89
    sub-int v0, v4, p2

    goto :goto_0

    .line 90
    :cond_4
    if-gez v5, :cond_5

    .line 91
    const-string v0, "SAMMLibraryCore"

    const-string v2, "SAMM Object Data is Invalid"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 92
    goto :goto_0

    .line 95
    :cond_5
    add-int v6, v4, v5

    .line 98
    new-instance v7, Landroid/graphics/PointF;

    invoke-direct {v7}, Landroid/graphics/PointF;-><init>()V

    .line 99
    invoke-static {p1, v4, v3}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v2

    .line 100
    aget v8, v3, v9

    int-to-short v8, v8

    int-to-float v8, v8

    iput v8, v7, Landroid/graphics/PointF;->x:F

    .line 101
    invoke-static {p1, v2, v3}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v2

    .line 102
    aget v8, v3, v9

    int-to-short v8, v8

    int-to-float v8, v8

    iput v8, v7, Landroid/graphics/PointF;->y:F

    .line 103
    invoke-virtual {v0, v7}, Lcom/samsung/samm/a/e;->a(Landroid/graphics/PointF;)V

    .line 108
    iput v9, p0, Lcom/samsung/samm/b/a/k;->f:I

    .line 109
    if-ge v2, v6, :cond_6

    .line 111
    invoke-static {p1, v2, v3}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v2

    .line 112
    aget v3, v3, v9

    .line 113
    const-string v7, "SAMM___LIBRARY___CONTENT___ID___KEY"

    invoke-virtual {v0, v7, v3}, Lcom/samsung/samm/a/e;->c(Ljava/lang/String;I)Z

    .line 114
    iget v0, p0, Lcom/samsung/samm/b/a/k;->f:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/samm/b/a/k;->f:I

    :cond_6
    move v0, v2

    .line 117
    sub-int/2addr v0, v4

    .line 118
    if-eq v0, v5, :cond_7

    move v0, v1

    .line 120
    goto :goto_0

    .line 122
    :cond_7
    sub-int v0, v6, p2

    goto :goto_0
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/samsung/samm/b/a/j;->a()V

    .line 44
    iget v0, p0, Lcom/samsung/samm/b/a/k;->f:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/samm/b/a/k;->f:I

    .line 45
    return-void
.end method

.method public a([III)[B
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v0, p0, Lcom/samsung/samm/b/a/k;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 162
    :goto_0
    return-object v0

    .line 128
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/k;->a()V

    .line 130
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/k;->e()I

    move-result v0

    new-array v2, v0, [B

    .line 133
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0, p2, p3}, Lcom/samsung/samm/b/a/k;->a([BIII)I

    move-result v3

    .line 134
    if-gez v3, :cond_1

    move-object v0, v1

    .line 135
    goto :goto_0

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/k;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/e;

    .line 140
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/k;->g()I

    move-result v4

    add-int/lit8 v4, v4, -0x4

    .line 141
    invoke-static {v2, v3, v4}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v3

    .line 144
    invoke-virtual {v0}, Lcom/samsung/samm/a/e;->k()Landroid/graphics/PointF;

    move-result-object v4

    .line 145
    if-nez v4, :cond_2

    .line 146
    const-string v0, "SAMMLibraryCore"

    const-string v2, "Fill points data is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 147
    goto :goto_0

    .line 149
    :cond_2
    iget v5, v4, Landroid/graphics/PointF;->x:F

    float-to-int v5, v5

    add-int/2addr v5, p2

    invoke-static {v2, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v3

    .line 150
    iget v4, v4, Landroid/graphics/PointF;->y:F

    float-to-int v4, v4

    add-int/2addr v4, p3

    invoke-static {v2, v3, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v3

    .line 156
    const-string v4, "SAMM___LIBRARY___ENCODE___IMAGE___ID___KEY"

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Lcom/samsung/samm/a/e;->d(Ljava/lang/String;I)I

    move-result v0

    .line 157
    invoke-static {v2, v3, v0}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    .line 159
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/k;->e()I

    move-result v3

    if-eq v0, v3, :cond_3

    move-object v0, v1

    .line 160
    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 162
    goto :goto_0
.end method

.method public g()I
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x0

    .line 57
    iget v1, p0, Lcom/samsung/samm/b/a/k;->f:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    .line 60
    :cond_0
    add-int/lit8 v0, v0, 0x8

    return v0
.end method
