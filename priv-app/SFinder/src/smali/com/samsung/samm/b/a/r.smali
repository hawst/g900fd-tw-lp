.class public Lcom/samsung/samm/b/a/r;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(IIII)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 357
    const/4 v0, 0x1

    .line 362
    if-gt p0, p2, :cond_0

    if-le p1, p3, :cond_2

    .line 364
    :cond_0
    if-le p0, p2, :cond_4

    .line 365
    int-to-float v0, p0

    int-to-float v2, p2

    div-float/2addr v0, v2

    .line 366
    :goto_0
    if-le p1, p3, :cond_1

    .line 367
    int-to-float v1, p1

    int-to-float v2, p3

    div-float/2addr v1, v2

    .line 369
    :cond_1
    cmpl-float v2, v0, v1

    if-ltz v2, :cond_3

    .line 372
    :goto_1
    float-to-int v0, v0

    .line 375
    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 370
    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 455
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 456
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 457
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 459
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 460
    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 461
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-object p0, v0

    .line 470
    :cond_0
    :goto_0
    return-object p0

    .line 465
    :catch_0
    move-exception v0

    .line 467
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 390
    if-nez p0, :cond_1

    move-object p0, v0

    .line 417
    :cond_0
    :goto_0
    return-object p0

    .line 393
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 394
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 395
    if-ltz v1, :cond_2

    if-gez v2, :cond_3

    :cond_2
    move-object p0, v0

    .line 396
    goto :goto_0

    .line 399
    :cond_3
    if-gt v1, p1, :cond_4

    if-le v2, p2, :cond_0

    .line 400
    :cond_4
    int-to-float v0, p1

    int-to-float v3, v1

    div-float/2addr v0, v3

    .line 401
    int-to-float v3, p2

    int-to-float v4, v2

    div-float/2addr v3, v4

    .line 405
    cmpg-float v4, v0, v3

    if-gtz v4, :cond_5

    .line 407
    int-to-float v1, v2

    mul-float/2addr v0, v1

    float-to-int p2, v0

    .line 414
    :goto_1
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0

    .line 410
    :cond_5
    int-to-float v0, v1

    mul-float/2addr v0, v3

    float-to-int p1, v0

    .line 411
    goto :goto_1
.end method

.method public static a(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 254
    invoke-static {p0}, Lcom/samsung/samm/b/a/r;->b(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    .line 255
    if-nez v3, :cond_1

    .line 256
    const/4 v0, 0x0

    .line 288
    :cond_0
    :goto_0
    return-object v0

    .line 263
    :cond_1
    if-eqz p3, :cond_4

    .line 264
    invoke-static {p0}, Lcom/samsung/samm/b/a/r;->c(Ljava/lang/String;)I

    move-result v0

    move v1, v0

    .line 267
    :goto_1
    const/16 v0, 0x5a

    if-eq v1, v0, :cond_2

    const/16 v0, 0x10e

    if-ne v1, v0, :cond_3

    .line 268
    :cond_2
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v0, v4, p1, p2}, Lcom/samsung/samm/b/a/r;->a(IIII)I

    move-result v0

    .line 278
    :goto_2
    iput-boolean v2, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 279
    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 280
    iput-boolean v2, v3, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 281
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 282
    const/4 v0, 0x1

    iput-boolean v0, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 284
    invoke-static {p0, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 285
    if-eqz p3, :cond_0

    .line 286
    invoke-static {v0, v1}, Lcom/samsung/samm/b/a/r;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 271
    :cond_3
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0, v4, p1, p2}, Lcom/samsung/samm/b/a/r;->a(IIII)I

    move-result v0

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 294
    if-nez p2, :cond_0

    .line 295
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 300
    :goto_0
    return-object v0

    .line 298
    :cond_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 299
    invoke-static {p0}, Lcom/samsung/samm/b/a/r;->c(Ljava/lang/String;)I

    move-result v1

    .line 300
    invoke-static {v0, v1}, Lcom/samsung/samm/b/a/r;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Z)Landroid/graphics/BitmapFactory$Options;
    .locals 3

    .prologue
    .line 322
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 323
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 325
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 327
    if-eqz p1, :cond_1

    .line 328
    invoke-static {p0}, Lcom/samsung/samm/b/a/r;->c(Ljava/lang/String;)I

    move-result v1

    .line 329
    const/16 v2, 0x5a

    if-eq v1, v2, :cond_0

    const/16 v2, 0x10e

    if-ne v1, v2, :cond_1

    .line 330
    :cond_0
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 331
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 332
    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 336
    :cond_1
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 155
    if-nez p0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 159
    iput-boolean v1, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 160
    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 162
    iget-object v2, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 163
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 24
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v1

    .line 30
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 32
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 33
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    :cond_2
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 44
    :goto_1
    const/4 v5, 0x0

    .line 46
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 47
    :try_start_2
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p1, v3, v5, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    move-result v3

    move v5, v3

    .line 53
    :goto_2
    if-eqz v4, :cond_4

    .line 55
    :try_start_3
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 56
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v3, v2

    .line 67
    :goto_3
    if-eqz v4, :cond_3

    .line 70
    :try_start_4
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 78
    :cond_3
    :goto_4
    if-eqz v0, :cond_0

    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    move v1, v2

    .line 79
    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    .line 41
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v0, v1

    goto :goto_1

    .line 48
    :catch_1
    move-exception v3

    move-object v4, v5

    .line 49
    :goto_5
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move v5, v1

    .line 50
    goto :goto_2

    :cond_4
    move v3, v1

    .line 62
    goto :goto_3

    :catch_2
    move-exception v3

    .line 63
    :try_start_5
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 67
    if-eqz v4, :cond_6

    .line 70
    :try_start_6
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    move v3, v1

    .line 71
    goto :goto_4

    :catch_3
    move-exception v3

    .line 73
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move v3, v1

    goto :goto_4

    .line 66
    :catchall_0
    move-exception v0

    .line 67
    if-eqz v4, :cond_5

    .line 70
    :try_start_7
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 76
    :cond_5
    :goto_6
    throw v0

    .line 71
    :catch_4
    move-exception v1

    .line 73
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 71
    :catch_5
    move-exception v4

    .line 73
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 48
    :catch_6
    move-exception v3

    goto :goto_5

    :cond_6
    move v3, v1

    goto :goto_4
.end method

.method public static a(Ljava/lang/String;Landroid/graphics/Bitmap;I)Z
    .locals 7

    .prologue
    const/16 v0, 0xa

    const/4 v2, 0x1

    const/16 v3, 0x64

    const/4 v1, 0x0

    .line 86
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    if-ltz p2, :cond_0

    if-gt p2, v3, :cond_0

    .line 95
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 98
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 103
    :cond_2
    if-ge p2, v0, :cond_5

    move p2, v0

    .line 107
    :cond_3
    :goto_1
    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 113
    :goto_2
    const/4 v5, 0x0

    .line 115
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 116
    :try_start_2
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {p1, v3, p2, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    move-result v3

    move v5, v3

    .line 122
    :goto_3
    if-eqz v4, :cond_6

    .line 124
    :try_start_3
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 125
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v3, v2

    .line 136
    :goto_4
    if-eqz v4, :cond_4

    .line 139
    :try_start_4
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 147
    :cond_4
    :goto_5
    if-eqz v0, :cond_0

    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    move v1, v2

    .line 148
    goto :goto_0

    .line 104
    :cond_5
    if-le p2, v3, :cond_3

    move p2, v3

    goto :goto_1

    .line 108
    :catch_0
    move-exception v0

    .line 110
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v0, v1

    goto :goto_2

    .line 117
    :catch_1
    move-exception v3

    move-object v4, v5

    .line 118
    :goto_6
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move v5, v1

    .line 119
    goto :goto_3

    :cond_6
    move v3, v1

    .line 131
    goto :goto_4

    :catch_2
    move-exception v3

    .line 132
    :try_start_5
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 136
    if-eqz v4, :cond_8

    .line 139
    :try_start_6
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    move v3, v1

    .line 140
    goto :goto_5

    :catch_3
    move-exception v3

    .line 142
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move v3, v1

    goto :goto_5

    .line 135
    :catchall_0
    move-exception v0

    .line 136
    if-eqz v4, :cond_7

    .line 139
    :try_start_7
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 145
    :cond_7
    :goto_7
    throw v0

    .line 140
    :catch_4
    move-exception v1

    .line 142
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 140
    :catch_5
    move-exception v4

    .line 142
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 117
    :catch_6
    move-exception v3

    goto :goto_6

    :cond_8
    move v3, v1

    goto :goto_5
.end method

.method public static b(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;
    .locals 2

    .prologue
    .line 309
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 310
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 312
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 314
    return-object v0
.end method

.method public static c(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 424
    .line 427
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    if-eqz v1, :cond_0

    .line 435
    const-string v2, "Orientation"

    invoke-virtual {v1, v2, v3}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v1

    .line 437
    if-eq v1, v3, :cond_0

    .line 438
    packed-switch v1, :pswitch_data_0

    .line 451
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 428
    :catch_0
    move-exception v1

    .line 430
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 440
    :pswitch_1
    const/16 v0, 0x5a

    .line 441
    goto :goto_0

    .line 443
    :pswitch_2
    const/16 v0, 0xb4

    .line 444
    goto :goto_0

    .line 446
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 438
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
