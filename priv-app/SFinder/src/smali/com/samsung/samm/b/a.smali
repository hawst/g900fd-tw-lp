.class public Lcom/samsung/samm/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/samm/b/a$a;
    }
.end annotation


# instance fields
.field protected a:Lcom/samsung/samm/b/a/d;

.field protected b:Lcom/samsung/samm/b/a$a;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Lcom/samsung/samm/b/b/a/a;

.field private f:Lcom/samsung/samm/a/k;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IIZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/samm/b/a;->c:Z

    .line 103
    iput-object v1, p0, Lcom/samsung/samm/b/a;->d:Ljava/lang/String;

    .line 106
    iput-object v1, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    .line 109
    iput-object v1, p0, Lcom/samsung/samm/b/a;->e:Lcom/samsung/samm/b/b/a/a;

    .line 111
    iput-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    .line 113
    iput-boolean v2, p0, Lcom/samsung/samm/b/a;->g:Z

    .line 114
    iput-boolean v2, p0, Lcom/samsung/samm/b/a;->h:Z

    .line 4949
    new-instance v0, Lcom/samsung/samm/b/a$1;

    invoke-direct {v0, p0}, Lcom/samsung/samm/b/a$1;-><init>(Lcom/samsung/samm/b/a;)V

    iput-object v0, p0, Lcom/samsung/samm/b/a;->b:Lcom/samsung/samm/b/a$a;

    .line 138
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/samm/b/a;->a(Landroid/content/Context;IIZ)Z

    .line 139
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIZZLjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/samm/b/a;->c:Z

    .line 103
    iput-object v1, p0, Lcom/samsung/samm/b/a;->d:Ljava/lang/String;

    .line 106
    iput-object v1, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    .line 109
    iput-object v1, p0, Lcom/samsung/samm/b/a;->e:Lcom/samsung/samm/b/b/a/a;

    .line 111
    iput-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    .line 113
    iput-boolean v7, p0, Lcom/samsung/samm/b/a;->g:Z

    .line 114
    iput-boolean v7, p0, Lcom/samsung/samm/b/a;->h:Z

    .line 4949
    new-instance v0, Lcom/samsung/samm/b/a$1;

    invoke-direct {v0, p0}, Lcom/samsung/samm/b/a$1;-><init>(Lcom/samsung/samm/b/a;)V

    iput-object v0, p0, Lcom/samsung/samm/b/a;->b:Lcom/samsung/samm/b/a$a;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    .line 161
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/samm/b/a;->a(Landroid/content/Context;IIZZLjava/lang/String;Z)Z

    .line 162
    return-void
.end method

.method private a(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2334
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2380
    :cond_0
    :goto_0
    return-object v0

    .line 2337
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    if-nez v0, :cond_2

    move-object v0, v1

    .line 2338
    goto :goto_0

    .line 2342
    :cond_2
    iget-object v0, p0, Lcom/samsung/samm/b/a;->b:Lcom/samsung/samm/b/a$a;

    invoke-interface {v0, v2, v2}, Lcom/samsung/samm/b/a$a;->a(ZI)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2343
    if-nez v0, :cond_3

    move-object v0, v1

    .line 2344
    goto :goto_0

    .line 2346
    :cond_3
    if-eqz p1, :cond_4

    .line 2347
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    iget v5, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2350
    :cond_4
    iget-object v1, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/b/a/d;->j()I

    move-result v2

    .line 2351
    iget-object v1, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/b/a/d;->k()I

    move-result v1

    .line 2354
    iget-object v3, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v3}, Lcom/samsung/samm/a/k;->k()I

    move-result v3

    .line 2357
    if-nez v3, :cond_5

    .line 2380
    :goto_1
    invoke-static {v0, v2, v1}, Lcom/samsung/samm/b/a/r;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 2363
    :cond_5
    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 2365
    div-int/lit8 v2, v2, 0x2

    .line 2366
    div-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 2369
    :cond_6
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 2371
    div-int/lit8 v2, v2, 0x4

    .line 2372
    div-int/lit8 v1, v1, 0x4

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/samm/a/c;)Lcom/samsung/samm/a/b;
    .locals 10

    .prologue
    const/16 v6, 0x64

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 837
    if-nez p2, :cond_0

    .line 1041
    :goto_0
    return-object v5

    .line 840
    :cond_0
    new-instance v8, Lcom/samsung/samm/a/b;

    invoke-direct {v8}, Lcom/samsung/samm/a/b;-><init>()V

    .line 841
    iput-boolean v4, v8, Lcom/samsung/samm/a/b;->a:Z

    .line 848
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->b:Z

    if-eqz v0, :cond_1

    .line 849
    invoke-static {p1, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Z)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    .line 850
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v1, v8, Lcom/samsung/samm/a/b;->c:I

    .line 851
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v0, v8, Lcom/samsung/samm/a/b;->d:I

    .line 854
    :cond_1
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->a:Z

    if-eqz v0, :cond_c

    .line 858
    iget v0, p2, Lcom/samsung/samm/a/c;->d:I

    if-gtz v0, :cond_7

    .line 860
    iget v0, p2, Lcom/samsung/samm/a/c;->e:I

    if-lez v0, :cond_2

    iget v0, p2, Lcom/samsung/samm/a/c;->f:I

    if-gtz v0, :cond_4

    .line 861
    :cond_2
    invoke-static {p1, v5, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->b:Landroid/graphics/Bitmap;

    .line 902
    :cond_3
    :goto_1
    new-instance v9, Lcom/samsung/samm/b/a;

    invoke-direct {v9, p0, v6, v6, v4}, Lcom/samsung/samm/b/a;-><init>(Landroid/content/Context;IIZ)V

    .line 905
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    move-object v1, p1

    move-object v2, p2

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;Lcom/samsung/samm/a/c;IILjava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_d

    .line 906
    invoke-virtual {v9}, Lcom/samsung/samm/b/a;->a()Z

    .line 907
    iput-boolean v4, v8, Lcom/samsung/samm/a/b;->a:Z

    .line 908
    const-string v0, "SAMMLibrary"

    const-string v1, "Error on load SAMM File Info"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v8

    .line 909
    goto :goto_0

    .line 865
    :cond_4
    invoke-static {p1, v5, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 866
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p2, Lcom/samsung/samm/a/c;->e:I

    if-ne v1, v2, :cond_5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p2, Lcom/samsung/samm/a/c;->f:I

    if-eq v1, v2, :cond_6

    .line 867
    :cond_5
    iget v1, p2, Lcom/samsung/samm/a/c;->e:I

    iget v2, p2, Lcom/samsung/samm/a/c;->f:I

    invoke-static {v0, v1, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->b:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 869
    :cond_6
    iput-object v0, v8, Lcom/samsung/samm/a/b;->b:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 877
    :cond_7
    iget v0, p2, Lcom/samsung/samm/a/c;->e:I

    if-lez v0, :cond_8

    iget v0, p2, Lcom/samsung/samm/a/c;->f:I

    if-gtz v0, :cond_9

    .line 878
    :cond_8
    iget v0, p2, Lcom/samsung/samm/a/c;->d:I

    iget v1, p2, Lcom/samsung/samm/a/c;->d:I

    invoke-static {p1, v0, v1, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->b:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 882
    :cond_9
    iget v0, p2, Lcom/samsung/samm/a/c;->d:I

    iget v1, p2, Lcom/samsung/samm/a/c;->d:I

    invoke-static {p1, v0, v1, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 883
    if-eqz v0, :cond_b

    .line 884
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p2, Lcom/samsung/samm/a/c;->e:I

    if-ne v1, v2, :cond_a

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p2, Lcom/samsung/samm/a/c;->f:I

    if-eq v1, v2, :cond_3

    .line 885
    :cond_a
    iget v1, p2, Lcom/samsung/samm/a/c;->e:I

    iget v2, p2, Lcom/samsung/samm/a/c;->f:I

    invoke-static {v0, v1, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->b:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 888
    :cond_b
    iput-object v0, v8, Lcom/samsung/samm/a/b;->b:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 894
    :cond_c
    iput-object v5, v8, Lcom/samsung/samm/a/b;->b:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 912
    :cond_d
    iput-boolean v7, v8, Lcom/samsung/samm/a/b;->a:Z

    .line 913
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->K()I

    move-result v0

    if-ne v0, v3, :cond_1d

    .line 914
    iput-boolean v7, v8, Lcom/samsung/samm/a/b;->C:Z

    .line 921
    :goto_2
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->c:Z

    if-eqz v0, :cond_27

    .line 922
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->r()Ljava/lang/String;

    move-result-object v0

    .line 923
    if-eqz v0, :cond_26

    .line 926
    iget v1, p2, Lcom/samsung/samm/a/c;->d:I

    if-gtz v1, :cond_21

    .line 928
    iget v1, p2, Lcom/samsung/samm/a/c;->e:I

    if-lez v1, :cond_e

    iget v1, p2, Lcom/samsung/samm/a/c;->f:I

    if-gtz v1, :cond_1e

    .line 929
    :cond_e
    invoke-static {v0, v5, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->e:Landroid/graphics/Bitmap;

    .line 976
    :cond_f
    :goto_3
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->g:Z

    if-eqz v0, :cond_10

    .line 977
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->h()I

    move-result v0

    iput v0, v8, Lcom/samsung/samm/a/b;->f:I

    .line 978
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->i()I

    move-result v0

    iput v0, v8, Lcom/samsung/samm/a/b;->g:I

    .line 981
    :cond_10
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->h:Z

    if-eqz v0, :cond_12

    .line 982
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->g()I

    move-result v0

    iput v0, v8, Lcom/samsung/samm/a/b;->h:I

    .line 983
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->m()Z

    move-result v0

    iput-boolean v0, v8, Lcom/samsung/samm/a/b;->i:Z

    .line 984
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->t()Z

    move-result v0

    iput-boolean v0, v8, Lcom/samsung/samm/a/b;->j:Z

    .line 985
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->G()I

    move-result v0

    iput v0, v8, Lcom/samsung/samm/a/b;->k:I

    .line 986
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->u()I

    move-result v0

    if-lez v0, :cond_11

    move v4, v7

    :cond_11
    iput-boolean v4, v8, Lcom/samsung/samm/a/b;->l:Z

    .line 988
    :cond_12
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->i:Z

    if-eqz v0, :cond_13

    .line 989
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->m:Ljava/lang/String;

    .line 991
    :cond_13
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->j:Z

    if-eqz v0, :cond_14

    .line 992
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->x()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->n:Ljava/lang/String;

    .line 993
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->y()I

    move-result v0

    iput v0, v8, Lcom/samsung/samm/a/b;->o:I

    .line 994
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->z()I

    move-result v0

    iput v0, v8, Lcom/samsung/samm/a/b;->p:I

    .line 995
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->q:Ljava/lang/String;

    .line 997
    :cond_14
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->k:Z

    if-eqz v0, :cond_15

    .line 998
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->r:Ljava/lang/String;

    .line 999
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->s:Ljava/lang/String;

    .line 1000
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->t:Ljava/lang/String;

    .line 1001
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->E()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->u:Landroid/graphics/Bitmap;

    .line 1003
    :cond_15
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->l:Z

    if-eqz v0, :cond_16

    .line 1004
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->F()J

    move-result-wide v0

    iput-wide v0, v8, Lcom/samsung/samm/a/b;->v:J

    .line 1006
    :cond_16
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->m:Z

    if-eqz v0, :cond_17

    .line 1007
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->H()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->w:Ljava/lang/String;

    .line 1009
    :cond_17
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->n:Z

    if-eqz v0, :cond_18

    .line 1010
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->I()I

    move-result v0

    iput v0, v8, Lcom/samsung/samm/a/b;->x:I

    .line 1011
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->J()I

    move-result v0

    iput v0, v8, Lcom/samsung/samm/a/b;->y:I

    .line 1013
    :cond_18
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->o:Z

    if-eqz v0, :cond_19

    .line 1014
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->M()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->z:[Ljava/lang/String;

    .line 1016
    :cond_19
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->p:Z

    if-eqz v0, :cond_1a

    .line 1017
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->A:Ljava/lang/String;

    .line 1019
    :cond_1a
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->q:Z

    if-eqz v0, :cond_29

    .line 1020
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->p()Ljava/lang/String;

    move-result-object v0

    .line 1021
    if-eqz v0, :cond_28

    .line 1023
    invoke-static {v0, v5, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->B:Landroid/graphics/Bitmap;

    .line 1031
    :goto_4
    iget-boolean v0, p2, Lcom/samsung/samm/a/c;->r:Z

    if-eqz v0, :cond_1b

    .line 1032
    iget-object v0, v9, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->L()Lcom/samsung/samm/a/a;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->D:Lcom/samsung/samm/a/a;

    .line 1033
    iget-object v0, v8, Lcom/samsung/samm/a/b;->D:Lcom/samsung/samm/a/a;

    if-eqz v0, :cond_1b

    .line 1034
    iput-boolean v7, v8, Lcom/samsung/samm/a/b;->E:Z

    .line 1038
    :cond_1b
    if-eqz v9, :cond_1c

    .line 1039
    invoke-virtual {v9}, Lcom/samsung/samm/b/a;->a()Z

    :cond_1c
    move-object v5, v8

    .line 1041
    goto/16 :goto_0

    .line 916
    :cond_1d
    iput-boolean v4, v8, Lcom/samsung/samm/a/b;->C:Z

    goto/16 :goto_2

    .line 933
    :cond_1e
    invoke-static {v0, v5, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 934
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p2, Lcom/samsung/samm/a/c;->e:I

    if-ne v1, v2, :cond_1f

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p2, Lcom/samsung/samm/a/c;->f:I

    if-eq v1, v2, :cond_20

    .line 935
    :cond_1f
    iget v1, p2, Lcom/samsung/samm/a/c;->e:I

    iget v2, p2, Lcom/samsung/samm/a/c;->f:I

    invoke-static {v0, v1, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->e:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 938
    :cond_20
    iput-object v0, v8, Lcom/samsung/samm/a/b;->e:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 945
    :cond_21
    iget v1, p2, Lcom/samsung/samm/a/c;->e:I

    if-lez v1, :cond_22

    iget v1, p2, Lcom/samsung/samm/a/c;->f:I

    if-gtz v1, :cond_23

    .line 946
    :cond_22
    iget v1, p2, Lcom/samsung/samm/a/c;->d:I

    iget v2, p2, Lcom/samsung/samm/a/c;->d:I

    invoke-static {v0, v1, v2, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->e:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 950
    :cond_23
    iget v1, p2, Lcom/samsung/samm/a/c;->d:I

    iget v2, p2, Lcom/samsung/samm/a/c;->d:I

    invoke-static {v0, v1, v2, v7}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 951
    if-eqz v0, :cond_25

    .line 952
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p2, Lcom/samsung/samm/a/c;->e:I

    if-ne v1, v2, :cond_24

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p2, Lcom/samsung/samm/a/c;->f:I

    if-eq v1, v2, :cond_f

    .line 953
    :cond_24
    iget v1, p2, Lcom/samsung/samm/a/c;->e:I

    iget v2, p2, Lcom/samsung/samm/a/c;->f:I

    invoke-static {v0, v1, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/samm/a/b;->e:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 957
    :cond_25
    iput-object v0, v8, Lcom/samsung/samm/a/b;->e:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 963
    :cond_26
    iput-object v5, v8, Lcom/samsung/samm/a/b;->e:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 967
    :cond_27
    iput-object v5, v8, Lcom/samsung/samm/a/b;->e:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 1025
    :cond_28
    iput-object v5, v8, Lcom/samsung/samm/a/b;->B:Landroid/graphics/Bitmap;

    goto/16 :goto_4

    .line 1029
    :cond_29
    iput-object v5, v8, Lcom/samsung/samm/a/b;->B:Landroid/graphics/Bitmap;

    goto/16 :goto_4
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Rect;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2156
    if-nez p1, :cond_1

    .line 2157
    const-string v0, "SAMMLibrary"

    const-string v2, "saveImageFile : Invalid Save Image File Path"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object p1, v1

    .line 2203
    :cond_0
    :goto_0
    return-object p1

    .line 2161
    :cond_1
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_2

    move-object p1, v1

    .line 2162
    goto :goto_0

    .line 2172
    :cond_2
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 2174
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2175
    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 2178
    const/4 v2, -0x1

    if-ne v0, v2, :cond_3

    .line 2179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".png"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2187
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2191
    if-nez v0, :cond_4

    .line 2193
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->g()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2194
    if-nez v0, :cond_4

    move-object p1, v1

    .line 2195
    goto :goto_0

    .line 2198
    :cond_4
    invoke-direct {p0, p1, v0}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    move-result v0

    .line 2200
    if-nez v0, :cond_0

    move-object p1, v1

    .line 2201
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 667
    invoke-static {p0}, Lcom/samsung/samm/b/a/d;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;IILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 2530
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2443
    if-nez p1, :cond_1

    .line 2444
    const-string v1, "SAMMLibrary"

    const-string v2, "saveAMSImageFile sFilePath is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2468
    :cond_0
    :goto_0
    return v0

    .line 2448
    :cond_1
    if-nez p2, :cond_2

    .line 2449
    const-string v1, "SAMMLibrary"

    const-string v2, "Unable to get Bitmap"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2453
    :cond_2
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 2454
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 2457
    const-string v2, "PNG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    .line 2458
    invoke-static {p1, p2}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2468
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 2460
    :cond_4
    const-string v2, "JPG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 2461
    iget-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v1}, Lcom/samsung/samm/a/k;->e()I

    move-result v1

    invoke-static {p1, p2, v1}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/Bitmap;I)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2464
    :cond_5
    const-string v1, "SAMMLibrary"

    const-string v2, "Unsupported File Format"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v8, 0x3

    const/4 v10, 0x1

    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 2218
    if-nez p1, :cond_0

    move-object v0, v3

    .line 2328
    :goto_0
    return-object v0

    .line 2221
    :cond_0
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v3

    .line 2222
    goto :goto_0

    .line 2224
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    if-nez v0, :cond_2

    move-object v0, v3

    .line 2225
    goto :goto_0

    .line 2228
    :cond_2
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->f()Z

    move-result v4

    .line 2231
    iget-object v0, p0, Lcom/samsung/samm/b/a;->b:Lcom/samsung/samm/b/a$a;

    invoke-interface {v0, v4, v9}, Lcom/samsung/samm/b/a$a;->a(ZI)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2232
    if-nez v0, :cond_3

    move-object v0, v3

    .line 2233
    goto :goto_0

    .line 2235
    :cond_3
    if-eqz p2, :cond_4

    .line 2236
    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    iget v5, p2, Landroid/graphics/Rect;->right:I

    iget v6, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    iget v6, p2, Landroid/graphics/Rect;->bottom:I

    iget v7, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v7

    invoke-static {v0, v1, v2, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2241
    :cond_4
    iget-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v1}, Lcom/samsung/samm/a/k;->d()I

    move-result v1

    if-ne v1, v10, :cond_5

    .line 2243
    const/16 v2, 0x1e0

    .line 2244
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/lit16 v1, v1, 0x1e0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/2addr v1, v5

    .line 2263
    :goto_1
    const-string v5, "."

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 2264
    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2266
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 2267
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 2270
    if-eqz v4, :cond_8

    .line 2271
    invoke-static {v0, v2, v1, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 2246
    :cond_5
    iget-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v1}, Lcom/samsung/samm/a/k;->d()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 2248
    const/16 v2, 0xf0

    .line 2249
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/lit16 v1, v1, 0xf0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/2addr v1, v5

    goto :goto_1

    .line 2251
    :cond_6
    iget-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v1}, Lcom/samsung/samm/a/k;->d()I

    move-result v1

    if-ne v1, v8, :cond_7

    .line 2253
    const/16 v2, 0x40

    .line 2254
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/lit8 v1, v1, 0x40

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/2addr v1, v5

    goto :goto_1

    .line 2259
    :cond_7
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 2260
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    goto :goto_1

    .line 2275
    :cond_8
    iget-object v4, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/d;->l()I

    move-result v4

    if-eq v4, v8, :cond_9

    .line 2276
    iget-object v4, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/d;->l()I

    move-result v4

    const/4 v8, 0x4

    if-ne v4, v8, :cond_d

    .line 2278
    :cond_9
    iget-object v4, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/d;->o()Ljava/lang/String;

    move-result-object v4

    .line 2280
    invoke-static {v4, v3, v10}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2281
    if-nez v4, :cond_a

    move-object v0, v3

    .line 2282
    goto/16 :goto_0

    .line 2284
    :cond_a
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ne v2, v5, :cond_b

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-ne v1, v5, :cond_b

    .line 2286
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v4, v1, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2293
    :goto_2
    if-nez v1, :cond_c

    move-object v0, v3

    .line 2294
    goto/16 :goto_0

    .line 2290
    :cond_b
    invoke-static {v4, v2, v1, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_2

    .line 2297
    :cond_c
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2298
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v6, v9, v9, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2299
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v7, v9, v9, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2300
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v2, v0, v6, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move-object v0, v1

    .line 2301
    goto/16 :goto_0

    .line 2308
    :cond_d
    iget-object v3, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/d;->n()I

    move-result v3

    .line 2309
    shr-int/lit8 v4, v3, 0x18

    and-int/lit16 v4, v4, 0xff

    .line 2311
    if-nez v4, :cond_e

    const-string v8, "PNG"

    invoke-virtual {v5, v8}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_e

    .line 2312
    invoke-static {v0, v2, v1, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 2316
    :cond_e
    if-nez v4, :cond_f

    const-string v4, "JPG"

    invoke-virtual {v5, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_f

    .line 2317
    const/4 v3, -0x1

    .line 2319
    :cond_f
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2320
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2321
    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 2322
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v6, v9, v9, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2323
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v7, v9, v9, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2324
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v2, v0, v6, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move-object v0, v1

    goto/16 :goto_0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    if-nez v0, :cond_0

    .line 307
    const-string v0, "SAMMLibrary"

    const-string v1, "SAMM Library is null. Library was not created successfully."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    const/4 v0, 0x0

    .line 311
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2411
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2425
    :goto_0
    return-object v0

    .line 2413
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v2, "assets/samm_embeded_icon/temp_icon.png"

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 2414
    if-eqz v2, :cond_2

    .line 2415
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2417
    :try_start_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2418
    :catch_0
    move-exception v0

    .line 2419
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 2420
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2425
    goto :goto_0
.end method


# virtual methods
.method a(ZZZZ)Landroid/graphics/Rect;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/high16 v9, 0x42480000    # 50.0f

    const/4 v8, 0x0

    .line 2059
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->d()Ljava/util/LinkedList;

    move-result-object v0

    .line 2060
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 2098
    :goto_0
    return-object v0

    .line 2063
    :cond_1
    iget-object v2, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/d;->j()I

    move-result v2

    .line 2064
    iget-object v3, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/d;->k()I

    move-result v3

    .line 2065
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 2066
    if-eqz p1, :cond_8

    int-to-float v5, v2

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 2068
    :goto_1
    if-eqz p3, :cond_9

    iput v8, v4, Landroid/graphics/RectF;->right:F

    .line 2070
    :goto_2
    if-eqz p2, :cond_a

    int-to-float v5, v3

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 2072
    :goto_3
    if-eqz p4, :cond_b

    iput v8, v4, Landroid/graphics/RectF;->bottom:F

    .line 2075
    :goto_4
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 2084
    iget v0, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v9

    iput v0, v4, Landroid/graphics/RectF;->left:F

    .line 2085
    iget v0, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v9

    iput v0, v4, Landroid/graphics/RectF;->top:F

    .line 2086
    iget v0, v4, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, v9

    iput v0, v4, Landroid/graphics/RectF;->right:F

    .line 2087
    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v9

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    .line 2090
    iget v0, v4, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v8

    if-gez v0, :cond_3

    iput v8, v4, Landroid/graphics/RectF;->left:F

    .line 2091
    :cond_3
    iget v0, v4, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v8

    if-gez v0, :cond_4

    iput v8, v4, Landroid/graphics/RectF;->top:F

    .line 2092
    :cond_4
    iget v0, v4, Landroid/graphics/RectF;->right:F

    int-to-float v5, v2

    cmpl-float v0, v0, v5

    if-lez v0, :cond_5

    int-to-float v0, v2

    iput v0, v4, Landroid/graphics/RectF;->right:F

    .line 2093
    :cond_5
    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_6

    int-to-float v0, v3

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    .line 2095
    :cond_6
    iget v0, v4, Landroid/graphics/RectF;->left:F

    iget v2, v4, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v2

    if-gez v0, :cond_7

    iget v0, v4, Landroid/graphics/RectF;->top:F

    iget v2, v4, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_10

    :cond_7
    move-object v0, v1

    .line 2096
    goto :goto_0

    .line 2067
    :cond_8
    iput v8, v4, Landroid/graphics/RectF;->left:F

    goto :goto_1

    .line 2069
    :cond_9
    int-to-float v5, v2

    iput v5, v4, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 2071
    :cond_a
    iput v8, v4, Landroid/graphics/RectF;->top:F

    goto :goto_3

    .line 2073
    :cond_b
    int-to-float v5, v3

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto :goto_4

    .line 2075
    :cond_c
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 2076
    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v0

    .line 2077
    if-eqz p1, :cond_d

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget v7, v4, Landroid/graphics/RectF;->left:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_d

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iput v6, v4, Landroid/graphics/RectF;->left:F

    .line 2078
    :cond_d
    if-eqz p3, :cond_e

    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget v7, v4, Landroid/graphics/RectF;->right:F

    cmpl-float v6, v6, v7

    if-lez v6, :cond_e

    iget v6, v0, Landroid/graphics/RectF;->right:F

    iput v6, v4, Landroid/graphics/RectF;->right:F

    .line 2079
    :cond_e
    if-eqz p2, :cond_f

    iget v6, v0, Landroid/graphics/RectF;->top:F

    iget v7, v4, Landroid/graphics/RectF;->top:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_f

    iget v6, v0, Landroid/graphics/RectF;->top:F

    iput v6, v4, Landroid/graphics/RectF;->top:F

    .line 2080
    :cond_f
    if-eqz p4, :cond_2

    iget v6, v0, Landroid/graphics/RectF;->bottom:F

    iget v7, v4, Landroid/graphics/RectF;->bottom:F

    cmpl-float v6, v6, v7

    if-lez v6, :cond_2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_5

    .line 2098
    :cond_10
    new-instance v0, Landroid/graphics/Rect;

    iget v1, v4, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, v4, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v3, v4, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    goto/16 :goto_0
.end method

.method a(Landroid/graphics/RectF;FFF)Landroid/graphics/RectF;
    .locals 16

    .prologue
    .line 1600
    move/from16 v0, p2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    .line 1602
    const/4 v2, 0x4

    new-array v3, v2, [Landroid/graphics/PointF;

    .line 1604
    const/4 v2, 0x0

    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    sub-float/2addr v5, v8

    move-object/from16 v0, p1

    iget v8, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-direct {v4, v5, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v2

    .line 1605
    const/4 v2, 0x1

    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    sub-float/2addr v5, v8

    move-object/from16 v0, p1

    iget v8, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-direct {v4, v5, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v2

    .line 1606
    const/4 v2, 0x2

    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    sub-float/2addr v5, v8

    move-object/from16 v0, p1

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-direct {v4, v5, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v2

    .line 1607
    const/4 v2, 0x3

    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    sub-float/2addr v5, v8

    move-object/from16 v0, p1

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-direct {v4, v5, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v2

    .line 1611
    const/4 v2, 0x4

    new-array v13, v2, [Landroid/graphics/PointF;

    .line 1613
    const/4 v2, 0x0

    :goto_0
    const/4 v4, 0x4

    if-lt v2, v4, :cond_0

    .line 1625
    const/4 v3, 0x0

    .line 1626
    const/4 v2, 0x0

    .line 1628
    const/4 v4, 0x0

    :goto_1
    const/4 v5, 0x4

    if-lt v4, v5, :cond_1

    .line 1632
    new-instance v4, Landroid/graphics/PointF;

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v3, v5

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    invoke-direct {v4, v3, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1635
    const/4 v2, 0x0

    :goto_2
    const/4 v3, 0x4

    if-lt v2, v3, :cond_2

    .line 1640
    const/4 v2, 0x0

    :goto_3
    const/4 v3, 0x4

    if-lt v2, v3, :cond_3

    .line 1650
    const/4 v3, 0x0

    .line 1651
    const/4 v5, 0x0

    .line 1652
    const/4 v7, 0x0

    .line 1653
    const/4 v9, 0x0

    .line 1655
    const/4 v2, 0x0

    .line 1656
    const/4 v4, 0x0

    .line 1657
    const/4 v6, 0x0

    .line 1658
    const/4 v8, 0x0

    .line 1660
    const/4 v11, 0x0

    .line 1661
    const/4 v10, 0x0

    .line 1664
    const/4 v12, 0x0

    :goto_4
    const/4 v14, 0x4

    if-lt v12, v14, :cond_4

    .line 1668
    new-instance v12, Landroid/graphics/PointF;

    const/high16 v14, 0x40800000    # 4.0f

    div-float/2addr v11, v14

    const/high16 v14, 0x40800000    # 4.0f

    div-float/2addr v10, v14

    invoke-direct {v12, v11, v10}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1671
    const/4 v10, 0x0

    :goto_5
    const/4 v11, 0x4

    if-lt v10, v11, :cond_5

    .line 1690
    div-float/2addr v2, v3

    .line 1691
    div-float v3, v4, v5

    .line 1692
    div-float v4, v6, v7

    .line 1693
    div-float v5, v8, v9

    .line 1695
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v2, v4, v3, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v6

    .line 1615
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    float-to-double v4, v4

    aget-object v8, v3, v2

    iget v8, v8, Landroid/graphics/PointF;->x:F

    float-to-double v8, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v4, v8

    aget-object v8, v3, v2

    iget v8, v8, Landroid/graphics/PointF;->y:F

    float-to-double v8, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    sub-double/2addr v4, v8

    double-to-float v4, v4

    .line 1616
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    float-to-double v8, v5

    aget-object v5, v3, v2

    iget v5, v5, Landroid/graphics/PointF;->x:F

    float-to-double v10, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v10, v14

    add-double/2addr v8, v10

    aget-object v5, v3, v2

    iget v5, v5, Landroid/graphics/PointF;->y:F

    float-to-double v10, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v10, v14

    add-double/2addr v8, v10

    double-to-float v5, v8

    .line 1619
    mul-float v4, v4, p3

    .line 1620
    mul-float v5, v5, p4

    .line 1622
    new-instance v8, Landroid/graphics/PointF;

    invoke-direct {v8, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v8, v13, v2

    .line 1613
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1629
    :cond_1
    aget-object v5, v13, v4

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v5

    .line 1630
    aget-object v5, v13, v4

    iget v5, v5, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v5

    .line 1628
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 1636
    :cond_2
    aget-object v3, v13, v2

    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v8

    iput v5, v3, Landroid/graphics/PointF;->x:F

    .line 1637
    aget-object v3, v13, v2

    iget v5, v3, Landroid/graphics/PointF;->y:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v8

    iput v5, v3, Landroid/graphics/PointF;->y:F

    .line 1635
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 1642
    :cond_3
    iget v3, v4, Landroid/graphics/PointF;->x:F

    float-to-double v8, v3

    aget-object v3, v13, v2

    iget v3, v3, Landroid/graphics/PointF;->x:F

    float-to-double v10, v3

    neg-double v14, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v10, v14

    add-double/2addr v8, v10

    aget-object v3, v13, v2

    iget v3, v3, Landroid/graphics/PointF;->y:F

    float-to-double v10, v3

    neg-double v14, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v10, v14

    sub-double/2addr v8, v10

    double-to-float v3, v8

    .line 1643
    iget v5, v4, Landroid/graphics/PointF;->y:F

    float-to-double v8, v5

    aget-object v5, v13, v2

    iget v5, v5, Landroid/graphics/PointF;->x:F

    float-to-double v10, v5

    neg-double v14, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v10, v14

    add-double/2addr v8, v10

    aget-object v5, v13, v2

    iget v5, v5, Landroid/graphics/PointF;->y:F

    float-to-double v10, v5

    neg-double v14, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v10, v14

    add-double/2addr v8, v10

    double-to-float v5, v8

    .line 1645
    aget-object v8, v13, v2

    iput v3, v8, Landroid/graphics/PointF;->x:F

    .line 1646
    aget-object v3, v13, v2

    iput v5, v3, Landroid/graphics/PointF;->y:F

    .line 1640
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    .line 1665
    :cond_4
    aget-object v14, v13, v12

    iget v14, v14, Landroid/graphics/PointF;->x:F

    add-float/2addr v11, v14

    .line 1666
    aget-object v14, v13, v12

    iget v14, v14, Landroid/graphics/PointF;->y:F

    add-float/2addr v10, v14

    .line 1664
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_4

    .line 1672
    :cond_5
    aget-object v11, v13, v10

    iget v11, v11, Landroid/graphics/PointF;->x:F

    iget v14, v12, Landroid/graphics/PointF;->x:F

    cmpg-float v11, v11, v14

    if-gtz v11, :cond_6

    .line 1673
    aget-object v11, v13, v10

    iget v11, v11, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, v11

    .line 1674
    const/high16 v11, 0x3f800000    # 1.0f

    add-float/2addr v3, v11

    .line 1676
    :cond_6
    aget-object v11, v13, v10

    iget v11, v11, Landroid/graphics/PointF;->x:F

    iget v14, v12, Landroid/graphics/PointF;->x:F

    cmpl-float v11, v11, v14

    if-ltz v11, :cond_7

    .line 1677
    aget-object v11, v13, v10

    iget v11, v11, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v11

    .line 1678
    const/high16 v11, 0x3f800000    # 1.0f

    add-float/2addr v5, v11

    .line 1680
    :cond_7
    aget-object v11, v13, v10

    iget v11, v11, Landroid/graphics/PointF;->y:F

    iget v14, v12, Landroid/graphics/PointF;->y:F

    cmpg-float v11, v11, v14

    if-gtz v11, :cond_8

    .line 1681
    aget-object v11, v13, v10

    iget v11, v11, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, v11

    .line 1682
    const/high16 v11, 0x3f800000    # 1.0f

    add-float/2addr v7, v11

    .line 1684
    :cond_8
    aget-object v11, v13, v10

    iget v11, v11, Landroid/graphics/PointF;->y:F

    iget v14, v12, Landroid/graphics/PointF;->y:F

    cmpl-float v11, v11, v14

    if-ltz v11, :cond_9

    .line 1685
    aget-object v11, v13, v10

    iget v11, v11, Landroid/graphics/PointF;->y:F

    add-float/2addr v8, v11

    .line 1686
    const/high16 v11, 0x3f800000    # 1.0f

    add-float/2addr v9, v11

    .line 1671
    :cond_9
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_5
.end method

.method public a(Lcom/samsung/samm/a/a;)V
    .locals 1

    .prologue
    .line 4195
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1}, Lcom/samsung/samm/b/a/d;->a(Lcom/samsung/samm/a/a;)V

    .line 4196
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    const/4 v0, 0x0

    .line 249
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->a()Z

    move-result v0

    goto :goto_0
.end method

.method a(II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 327
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    :goto_0
    return v0

    .line 330
    :cond_0
    if-lez p1, :cond_1

    if-gtz p2, :cond_2

    .line 331
    :cond_1
    const-string v1, "SAMMLibrary"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Canvas Resolution : width ="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 332
    const-string v3, ", height="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 331
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 335
    :cond_2
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1}, Lcom/samsung/samm/b/a/d;->e(I)V

    .line 336
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p2}, Lcom/samsung/samm/b/a/d;->f(I)V

    .line 338
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Landroid/content/Context;IIZ)Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 179
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    move v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/samm/b/a;->a(Landroid/content/Context;IIZZLjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method a(Landroid/content/Context;IIZZLjava/lang/String;Z)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 184
    iput-boolean v0, p0, Lcom/samsung/samm/b/a;->c:Z

    .line 186
    invoke-virtual {p0}, Lcom/samsung/samm/b/a;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 232
    :cond_0
    :goto_0
    return v0

    .line 193
    :cond_1
    if-eqz p1, :cond_2

    if-lez p2, :cond_2

    if-gtz p3, :cond_3

    .line 195
    :cond_2
    const-string v1, "SAMMLibrary"

    const-string v2, "Error on create SAMMLibrary"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 200
    :cond_3
    if-nez p6, :cond_4

    .line 201
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/samm/b/a;->d:Ljava/lang/String;

    .line 206
    :goto_1
    iput-boolean p4, p0, Lcom/samsung/samm/b/a;->g:Z

    .line 207
    iput-boolean p5, p0, Lcom/samsung/samm/b/a;->h:Z

    .line 210
    new-instance v2, Lcom/samsung/samm/b/a/d;

    iget-object v3, p0, Lcom/samsung/samm/b/a;->d:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/samsung/samm/b/a;->g:Z

    iget-boolean v5, p0, Lcom/samsung/samm/b/a;->h:Z

    invoke-direct {v2, p1, v3, v4, v5}, Lcom/samsung/samm/b/a/d;-><init>(Landroid/content/Context;Ljava/lang/String;ZZ)V

    iput-object v2, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    .line 211
    iget-object v2, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    if-nez v2, :cond_5

    .line 212
    const-string v1, "SAMMLibrary"

    const-string v2, "Error on create Animation Data"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 203
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/SPenSDKTemp/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/samm/b/a;->d:Ljava/lang/String;

    goto :goto_1

    .line 216
    :cond_5
    invoke-virtual {p0, p2, p3}, Lcom/samsung/samm/b/a;->a(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 219
    new-instance v2, Lcom/samsung/samm/a/k;

    invoke-direct {v2}, Lcom/samsung/samm/a/k;-><init>()V

    iput-object v2, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    .line 222
    if-eqz p7, :cond_6

    .line 223
    iget-object v2, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v2}, Lcom/samsung/samm/b/a/d;->b()Z

    move-result v2

    if-nez v2, :cond_6

    .line 225
    const-string v1, "SAMMLibrary"

    const-string v2, "Error on cleanTempSaveFolderWithTimeLimit"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 231
    :cond_6
    iput-boolean v1, p0, Lcom/samsung/samm/b/a;->c:Z

    move v0, v1

    .line 232
    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 3108
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3109
    const/4 v0, 0x0

    .line 3110
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/d;->d(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 3053
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3054
    const/4 v0, 0x0

    .line 3055
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 10

    .prologue
    .line 702
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 703
    const/4 v0, 0x0

    .line 818
    :goto_0
    return v0

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v1}, Lcom/samsung/samm/a/k;->k()I

    move-result v3

    iget-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v1}, Lcom/samsung/samm/a/k;->i()I

    move-result v4

    move-object v1, p1

    move-object v5, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;Lcom/samsung/samm/a/c;IILjava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 707
    const-string v0, "SAMMLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error on load AMS File : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    const/4 v0, 0x0

    goto :goto_0

    .line 715
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->j()I

    move-result v4

    .line 716
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->k()I

    move-result v5

    .line 717
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->h()I

    move-result v2

    .line 718
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->i()I

    move-result v3

    .line 719
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->a()I

    move-result v0

    .line 720
    if-nez v0, :cond_2

    .line 722
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->d()Ljava/util/LinkedList;

    move-result-object v1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/samm/b/a;->a(Ljava/util/LinkedList;IIII)Z

    move-result v0

    if-nez v0, :cond_12

    .line 723
    const-string v0, "SAMMLibrary"

    const-string v1, "Error on convertResolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    const/4 v0, 0x0

    goto :goto_0

    .line 727
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 728
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->b()I

    move-result v6

    .line 729
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->c()I

    move-result v7

    .line 730
    int-to-float v0, v4

    int-to-float v1, v2

    div-float v1, v0, v1

    .line 731
    int-to-float v0, v5

    int-to-float v8, v3

    div-float/2addr v0, v8

    .line 735
    cmpl-float v8, v1, v0

    if-lez v8, :cond_6

    .line 737
    const/4 v1, 0x0

    .line 739
    if-nez v6, :cond_3

    .line 740
    const/4 v2, 0x0

    .line 764
    :goto_1
    iget-object v3, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/d;->d()Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {p0, v3, v0, v2, v1}, Lcom/samsung/samm/b/a;->a(Ljava/util/LinkedList;FII)Z

    move-result v0

    if-nez v0, :cond_12

    .line 765
    const-string v0, "SAMMLibrary"

    const-string v1, "Error on convertResolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 741
    :cond_3
    const/4 v3, 0x1

    if-ne v6, v3, :cond_4

    .line 742
    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    sub-int v2, v4, v2

    div-int/lit8 v2, v2, 0x2

    goto :goto_1

    .line 743
    :cond_4
    const/4 v3, 0x2

    if-ne v6, v3, :cond_5

    .line 744
    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    sub-int v2, v4, v2

    goto :goto_1

    .line 746
    :cond_5
    const/4 v2, 0x0

    .line 747
    goto :goto_1

    .line 751
    :cond_6
    const/4 v2, 0x0

    .line 753
    if-nez v7, :cond_7

    .line 754
    const/4 v0, 0x0

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    .line 755
    :cond_7
    const/4 v0, 0x1

    if-ne v7, v0, :cond_8

    .line 756
    int-to-float v0, v3

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sub-int v0, v5, v0

    div-int/lit8 v0, v0, 0x2

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    .line 757
    :cond_8
    const/4 v0, 0x2

    if-ne v7, v0, :cond_9

    .line 758
    int-to-float v0, v3

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sub-int v0, v5, v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    .line 760
    :cond_9
    const/4 v0, 0x0

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    .line 769
    :cond_a
    const/4 v1, 0x2

    if-ne v0, v1, :cond_11

    .line 770
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->j()I

    move-result v0

    .line 771
    iget-object v1, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v1}, Lcom/samsung/samm/b/a/d;->k()I

    move-result v4

    .line 778
    iget-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v1}, Lcom/samsung/samm/a/k;->b()I

    move-result v1

    .line 779
    iget-object v5, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v5}, Lcom/samsung/samm/a/k;->c()I

    move-result v5

    .line 781
    if-nez v1, :cond_b

    .line 782
    const/4 v0, 0x0

    move v1, v0

    .line 791
    :goto_2
    if-nez v5, :cond_e

    .line 792
    const/4 v0, 0x0

    .line 801
    :goto_3
    const/high16 v2, 0x3f800000    # 1.0f

    .line 803
    iget-object v3, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v3}, Lcom/samsung/samm/b/a/d;->d()Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {p0, v3, v2, v1, v0}, Lcom/samsung/samm/b/a;->a(Ljava/util/LinkedList;FII)Z

    move-result v0

    if-nez v0, :cond_12

    .line 804
    const-string v0, "SAMMLibrary"

    const-string v1, "Error on convertResolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 783
    :cond_b
    const/4 v6, 0x1

    if-ne v1, v6, :cond_c

    .line 784
    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    move v1, v0

    goto :goto_2

    .line 785
    :cond_c
    const/4 v6, 0x2

    if-ne v1, v6, :cond_d

    .line 786
    sub-int/2addr v0, v2

    move v1, v0

    goto :goto_2

    .line 788
    :cond_d
    const/4 v0, 0x0

    move v1, v0

    goto :goto_2

    .line 793
    :cond_e
    const/4 v0, 0x1

    if-ne v5, v0, :cond_f

    .line 794
    sub-int v0, v4, v3

    div-int/lit8 v0, v0, 0x2

    goto :goto_3

    .line 795
    :cond_f
    const/4 v0, 0x2

    if-ne v5, v0, :cond_10

    .line 796
    sub-int v0, v4, v3

    goto :goto_3

    .line 798
    :cond_10
    const/4 v0, 0x0

    goto :goto_3

    .line 812
    :cond_11
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->d()Ljava/util/LinkedList;

    move-result-object v1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/samm/b/a;->a(Ljava/util/LinkedList;IIII)Z

    move-result v0

    if-nez v0, :cond_12

    .line 813
    const-string v0, "SAMMLibrary"

    const-string v1, "Error on convertResolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 818
    :cond_12
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method a(Ljava/lang/String;ZZZILjava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1937
    if-eqz p2, :cond_2

    .line 1938
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1939
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1940
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2053
    :cond_0
    :goto_0
    return v2

    .line 1945
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v0

    .line 1946
    if-nez v0, :cond_3

    .line 1947
    const-string v0, "SAMMLibrary"

    const-string v1, "Fail to create SAMMDataFile"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1950
    :catch_0
    move-exception v0

    .line 1952
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1953
    const-string v0, "SAMMLibrary"

    const-string v1, "Fail to create SAMMDataFile"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1957
    :cond_2
    if-nez p3, :cond_3

    .line 1958
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1959
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1960
    const-string v0, "SAMMLibrary"

    const-string v1, "File does not exist"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1970
    :cond_3
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->l()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1971
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->m()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1972
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->n()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1973
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->o()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1975
    :cond_4
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->l()Z

    move-result v0

    .line 1976
    iget-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v1}, Lcom/samsung/samm/a/k;->n()Z

    move-result v1

    .line 1977
    iget-object v4, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v4}, Lcom/samsung/samm/a/k;->m()Z

    move-result v4

    .line 1978
    iget-object v5, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v5}, Lcom/samsung/samm/a/k;->o()Z

    move-result v5

    .line 1974
    invoke-virtual {p0, v0, v1, v4, v5}, Lcom/samsung/samm/b/a;->a(ZZZZ)Landroid/graphics/Rect;

    move-result-object v0

    .line 1983
    :goto_1
    iget-object v1, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v1}, Lcom/samsung/samm/a/k;->j()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1984
    invoke-virtual {p0}, Lcom/samsung/samm/b/a;->e()Z

    .line 1990
    :cond_5
    if-eqz p3, :cond_6

    .line 1992
    invoke-direct {p0, p1, v0}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v1

    .line 1993
    if-nez v1, :cond_7

    .line 1994
    const-string v0, "SAMMLibrary"

    const-string v1, "Error make SAMM File"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    move-object v1, p1

    .line 2002
    :cond_7
    if-nez p4, :cond_9

    iget-object v4, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v4}, Lcom/samsung/samm/a/k;->g()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2004
    iget-object v4, p0, Lcom/samsung/samm/b/a;->b:Lcom/samsung/samm/b/a$a;

    invoke-interface {v4, v7, v2}, Lcom/samsung/samm/b/a$a;->a(ZI)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2005
    if-eqz v4, :cond_0

    .line 2007
    iget-object v5, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v5, v4}, Lcom/samsung/samm/b/a/d;->a(Landroid/graphics/Bitmap;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 2008
    const-string v0, "SAMMLibrary"

    const-string v1, "Fail to encode foreground image"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2011
    :cond_8
    iget-object v4, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v4, v2}, Lcom/samsung/samm/b/a/d;->g(I)V

    .line 2019
    :goto_2
    if-nez p4, :cond_a

    iget-object v4, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v4}, Lcom/samsung/samm/a/k;->h()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2020
    iget-object v4, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-direct {p0, v0}, Lcom/samsung/samm/b/a;->a(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/samm/b/a/d;->b(Landroid/graphics/Bitmap;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 2021
    const-string v0, "SAMMLibrary"

    const-string v1, "Fail to encode SAMM thumbnail image"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2013
    :cond_9
    iget-object v4, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/d;->q()V

    goto :goto_2

    .line 2025
    :cond_a
    iget-object v4, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/d;->s()V

    .line 2029
    :cond_b
    iget-object v4, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v4}, Lcom/samsung/samm/b/a/d;->j()I

    move-result v4

    .line 2030
    iget-object v5, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v5}, Lcom/samsung/samm/b/a/d;->k()I

    move-result v5

    .line 2031
    iget-object v6, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v6}, Lcom/samsung/samm/a/k;->p()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2032
    if-eqz v0, :cond_c

    .line 2033
    iget-object v3, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/samm/b/a/d;->c(I)V

    .line 2034
    iget-object v3, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/samm/b/a/d;->d(I)V

    move-object v3, v0

    .line 2048
    :goto_3
    iget-object v0, p0, Lcom/samsung/samm/b/a;->f:Lcom/samsung/samm/a/k;

    invoke-virtual {v0}, Lcom/samsung/samm/a/k;->q()Z

    move-result v4

    .line 2049
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;ZLandroid/graphics/Rect;ZILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 2050
    const-string v0, "SAMMLibrary"

    const-string v1, "Error save SAMM File"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2037
    :cond_c
    iget-object v3, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v3, v4}, Lcom/samsung/samm/b/a/d;->c(I)V

    .line 2038
    iget-object v3, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v3, v5}, Lcom/samsung/samm/b/a/d;->d(I)V

    move-object v3, v0

    .line 2040
    goto :goto_3

    .line 2043
    :cond_d
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, v4}, Lcom/samsung/samm/b/a/d;->c(I)V

    .line 2044
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, v5}, Lcom/samsung/samm/b/a/d;->d(I)V

    goto :goto_3

    :cond_e
    move v2, v7

    .line 2053
    goto/16 :goto_0

    :cond_f
    move-object v0, v3

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;[B)Z
    .locals 1

    .prologue
    .line 3218
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3219
    const/4 v0, 0x0

    .line 3220
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;[B)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[I)Z
    .locals 2

    .prologue
    .line 3273
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3274
    const/4 v0, 0x0

    .line 3278
    :goto_0
    return v0

    .line 3275
    :cond_0
    array-length v0, p2

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 3276
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v1

    .line 3277
    invoke-virtual {v1, p2}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    .line 3278
    iget-object v1, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;[B)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 3163
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3164
    const/4 v0, 0x0

    .line 3165
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method a(Ljava/util/LinkedList;FII)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;FII)Z"
        }
    .end annotation

    .prologue
    .line 1438
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1439
    const/4 v0, 0x0

    .line 1594
    :goto_0
    return v0

    .line 1444
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    const/4 v0, 0x1

    move v4, v0

    .line 1449
    :goto_1
    if-lez p3, :cond_3

    const/4 v0, 0x1

    move v3, v0

    .line 1453
    :goto_2
    if-lez p4, :cond_4

    const/4 v0, 0x1

    move v2, v0

    .line 1457
    :goto_3
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1594
    const/4 v0, 0x1

    goto :goto_0

    .line 1445
    :cond_2
    const/4 v0, 0x0

    move v4, v0

    goto :goto_1

    .line 1450
    :cond_3
    const/4 v0, 0x0

    move v3, v0

    goto :goto_2

    .line 1454
    :cond_4
    const/4 v0, 0x0

    move v2, v0

    goto :goto_3

    .line 1457
    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/a/d;

    .line 1458
    if-nez v0, :cond_6

    .line 1459
    const-string v0, "SAMMLibrary"

    const-string v1, "Invalid SAMM Object Instance"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1460
    const/4 v0, 0x0

    goto :goto_0

    .line 1466
    :cond_6
    invoke-virtual {v0}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v6

    .line 1467
    if-nez v6, :cond_7

    .line 1468
    const-string v0, "SAMMLibrary"

    const-string v1, "Invalid SAMM Object Rect Data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1469
    const/4 v0, 0x0

    goto :goto_0

    .line 1472
    :cond_7
    if-eqz v4, :cond_8

    .line 1474
    instance-of v1, v0, Lcom/samsung/samm/a/g;

    if-eqz v1, :cond_c

    move-object v1, v0

    .line 1475
    check-cast v1, Lcom/samsung/samm/a/g;

    .line 1476
    invoke-virtual {v1}, Lcom/samsung/samm/a/g;->e()F

    move-result v1

    .line 1477
    const/4 v7, 0x0

    cmpl-float v7, v1, v7

    if-nez v7, :cond_b

    .line 1478
    iget v1, v6, Landroid/graphics/RectF;->left:F

    mul-float/2addr v1, p2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    .line 1479
    iget v1, v6, Landroid/graphics/RectF;->right:F

    mul-float/2addr v1, p2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    .line 1480
    iget v1, v6, Landroid/graphics/RectF;->top:F

    mul-float/2addr v1, p2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    .line 1481
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v1, p2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    .line 1498
    :cond_8
    :goto_5
    if-eqz v3, :cond_9

    .line 1499
    iget v1, v6, Landroid/graphics/RectF;->left:F

    int-to-float v7, p3

    add-float/2addr v1, v7

    iput v1, v6, Landroid/graphics/RectF;->left:F

    .line 1500
    iget v1, v6, Landroid/graphics/RectF;->right:F

    int-to-float v7, p3

    add-float/2addr v1, v7

    iput v1, v6, Landroid/graphics/RectF;->right:F

    .line 1502
    :cond_9
    if-eqz v2, :cond_a

    .line 1503
    iget v1, v6, Landroid/graphics/RectF;->top:F

    int-to-float v7, p4

    add-float/2addr v1, v7

    iput v1, v6, Landroid/graphics/RectF;->top:F

    .line 1504
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    int-to-float v7, p4

    add-float/2addr v1, v7

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    .line 1506
    :cond_a
    invoke-virtual {v0, v6}, Lcom/samsung/samm/a/d;->a(Landroid/graphics/RectF;)V

    .line 1509
    instance-of v1, v0, Lcom/samsung/samm/a/h;

    if-eqz v1, :cond_12

    .line 1510
    check-cast v0, Lcom/samsung/samm/a/h;

    .line 1511
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->c()F

    move-result v1

    .line 1512
    mul-float/2addr v1, p2

    .line 1513
    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/h;->a(F)V

    .line 1518
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->k()I

    move-result v6

    .line 1519
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->l()[Landroid/graphics/PointF;

    move-result-object v7

    .line 1520
    if-nez v7, :cond_d

    .line 1521
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1483
    :cond_b
    invoke-virtual {p0, v6, v1, p2, p2}, Lcom/samsung/samm/b/a;->a(Landroid/graphics/RectF;FFF)Landroid/graphics/RectF;

    move-result-object v1

    .line 1484
    iget v7, v1, Landroid/graphics/RectF;->left:F

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 1485
    iget v7, v1, Landroid/graphics/RectF;->right:F

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 1486
    iget v7, v1, Landroid/graphics/RectF;->top:F

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 1487
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto :goto_5

    .line 1491
    :cond_c
    iget v1, v6, Landroid/graphics/RectF;->left:F

    mul-float/2addr v1, p2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    .line 1492
    iget v1, v6, Landroid/graphics/RectF;->right:F

    mul-float/2addr v1, p2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    .line 1493
    iget v1, v6, Landroid/graphics/RectF;->top:F

    mul-float/2addr v1, p2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    .line 1494
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v1, p2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto :goto_5

    .line 1522
    :cond_d
    const/4 v1, 0x0

    :goto_6
    if-lt v1, v6, :cond_e

    .line 1542
    invoke-virtual {v0, v7}, Lcom/samsung/samm/a/h;->a([Landroid/graphics/PointF;)V

    goto/16 :goto_4

    .line 1523
    :cond_e
    aget-object v8, v7, v1

    .line 1524
    new-instance v9, Landroid/graphics/PointF;

    iget v10, v8, Landroid/graphics/PointF;->x:F

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-direct {v9, v10, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1525
    if-eqz v4, :cond_f

    .line 1526
    iget v8, v9, Landroid/graphics/PointF;->x:F

    mul-float/2addr v8, p2

    iput v8, v9, Landroid/graphics/PointF;->x:F

    .line 1527
    iget v8, v9, Landroid/graphics/PointF;->y:F

    mul-float/2addr v8, p2

    iput v8, v9, Landroid/graphics/PointF;->y:F

    .line 1530
    :cond_f
    if-eqz v3, :cond_10

    .line 1531
    iget v8, v9, Landroid/graphics/PointF;->x:F

    int-to-float v10, p3

    add-float/2addr v8, v10

    iput v8, v9, Landroid/graphics/PointF;->x:F

    .line 1533
    :cond_10
    if-eqz v2, :cond_11

    .line 1534
    iget v8, v9, Landroid/graphics/PointF;->y:F

    int-to-float v10, p4

    add-float/2addr v8, v10

    iput v8, v9, Landroid/graphics/PointF;->y:F

    .line 1540
    :cond_11
    aput-object v9, v7, v1

    .line 1522
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1544
    :cond_12
    instance-of v1, v0, Lcom/samsung/samm/a/e;

    if-eqz v1, :cond_14

    .line 1545
    check-cast v0, Lcom/samsung/samm/a/e;

    .line 1546
    invoke-virtual {v0}, Lcom/samsung/samm/a/e;->k()Landroid/graphics/PointF;

    move-result-object v1

    .line 1547
    if-eqz v4, :cond_13

    .line 1548
    iget v6, v1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v6, p2

    iput v6, v1, Landroid/graphics/PointF;->x:F

    .line 1549
    iget v6, v1, Landroid/graphics/PointF;->y:F

    mul-float/2addr v6, p2

    iput v6, v1, Landroid/graphics/PointF;->y:F

    .line 1551
    :cond_13
    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/e;->a(Landroid/graphics/PointF;)V

    goto/16 :goto_4

    .line 1553
    :cond_14
    instance-of v1, v0, Lcom/samsung/samm/a/i;

    if-eqz v1, :cond_16

    .line 1554
    check-cast v0, Lcom/samsung/samm/a/i;

    .line 1555
    invoke-virtual {v0}, Lcom/samsung/samm/a/i;->c()F

    move-result v1

    .line 1560
    if-eqz v4, :cond_15

    .line 1561
    mul-float/2addr v1, p2

    .line 1582
    :cond_15
    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/i;->a(F)V

    goto/16 :goto_4

    .line 1584
    :cond_16
    instance-of v1, v0, Lcom/samsung/samm/a/f;

    if-eqz v1, :cond_1

    .line 1585
    check-cast v0, Lcom/samsung/samm/a/f;

    .line 1586
    invoke-virtual {v0}, Lcom/samsung/samm/a/f;->k()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/samsung/samm/b/a;->a(Ljava/util/LinkedList;FII)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1587
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method a(Ljava/util/LinkedList;IIII)Z
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;IIII)Z"
        }
    .end annotation

    .prologue
    .line 1256
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1257
    const/4 v2, 0x0

    .line 1431
    :goto_0
    return v2

    .line 1259
    :cond_0
    if-nez p1, :cond_1

    .line 1260
    const/4 v2, 0x0

    goto :goto_0

    .line 1262
    :cond_1
    if-lez p2, :cond_2

    if-lez p3, :cond_2

    if-lez p4, :cond_2

    if-gtz p5, :cond_3

    .line 1263
    :cond_2
    const-string v2, "SAMMLibrary"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid Resolution : srcWidth ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1264
    const-string v4, ", srcHeight="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1265
    const-string v4, ", desWidth ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1266
    const-string v4, ", desHeight="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1263
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1267
    const/4 v2, 0x0

    goto :goto_0

    .line 1271
    :cond_3
    move/from16 v0, p2

    move/from16 v1, p4

    if-eq v0, v1, :cond_5

    const/4 v2, 0x1

    move v9, v2

    .line 1274
    :goto_1
    move/from16 v0, p3

    move/from16 v1, p5

    if-eq v0, v1, :cond_6

    const/4 v2, 0x1

    move v8, v2

    .line 1278
    :goto_2
    move/from16 v0, p4

    int-to-float v2, v0

    move/from16 v0, p2

    int-to-float v3, v0

    div-float v10, v2, v3

    .line 1279
    move/from16 v0, p5

    int-to-float v2, v0

    move/from16 v0, p3

    int-to-float v3, v0

    div-float v11, v2, v3

    .line 1281
    invoke-virtual/range {p1 .. p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_4
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1431
    const/4 v2, 0x1

    goto :goto_0

    .line 1272
    :cond_5
    const/4 v2, 0x0

    move v9, v2

    goto :goto_1

    .line 1275
    :cond_6
    const/4 v2, 0x0

    move v8, v2

    goto :goto_2

    .line 1281
    :cond_7
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/samm/a/d;

    .line 1282
    if-nez v2, :cond_8

    .line 1283
    const-string v2, "SAMMLibrary"

    const-string v3, "Invalid SAMM Object Instance"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1284
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1290
    :cond_8
    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->d()Landroid/graphics/RectF;

    move-result-object v4

    .line 1291
    if-nez v4, :cond_9

    .line 1292
    const-string v2, "SAMMLibrary"

    const-string v3, "Invalid SAMM Object Rect Data"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1293
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1296
    :cond_9
    instance-of v3, v2, Lcom/samsung/samm/a/g;

    if-nez v3, :cond_a

    .line 1297
    instance-of v3, v2, Lcom/samsung/samm/a/e;

    if-nez v3, :cond_a

    .line 1298
    instance-of v3, v2, Lcom/samsung/samm/a/i;

    if-nez v3, :cond_a

    .line 1299
    instance-of v3, v2, Lcom/samsung/samm/a/h;

    if-eqz v3, :cond_d

    .line 1300
    :cond_a
    invoke-virtual {v2}, Lcom/samsung/samm/a/d;->e()F

    move-result v3

    .line 1301
    const/4 v5, 0x0

    cmpl-float v5, v3, v5

    if-nez v5, :cond_c

    .line 1302
    iget v3, v4, Landroid/graphics/RectF;->left:F

    mul-float/2addr v3, v10

    iput v3, v4, Landroid/graphics/RectF;->left:F

    .line 1303
    iget v3, v4, Landroid/graphics/RectF;->right:F

    mul-float/2addr v3, v10

    iput v3, v4, Landroid/graphics/RectF;->right:F

    .line 1304
    iget v3, v4, Landroid/graphics/RectF;->top:F

    mul-float/2addr v3, v11

    iput v3, v4, Landroid/graphics/RectF;->top:F

    .line 1305
    iget v3, v4, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v3, v11

    iput v3, v4, Landroid/graphics/RectF;->bottom:F

    .line 1326
    :cond_b
    :goto_4
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 1327
    iget v5, v4, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    int-to-float v5, v5

    iput v5, v3, Landroid/graphics/RectF;->left:F

    .line 1328
    iget v5, v4, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    int-to-float v5, v5

    iput v5, v3, Landroid/graphics/RectF;->top:F

    .line 1329
    iget v5, v4, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    int-to-float v5, v5

    iput v5, v3, Landroid/graphics/RectF;->right:F

    .line 1330
    iget v5, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    int-to-float v5, v5

    iput v5, v3, Landroid/graphics/RectF;->bottom:F

    .line 1331
    invoke-virtual {v2, v3}, Lcom/samsung/samm/a/d;->a(Landroid/graphics/RectF;)V

    .line 1334
    instance-of v3, v2, Lcom/samsung/samm/a/h;

    if-eqz v3, :cond_16

    .line 1335
    check-cast v2, Lcom/samsung/samm/a/h;

    .line 1336
    invoke-virtual {v2}, Lcom/samsung/samm/a/h;->c()F

    move-result v3

    .line 1337
    mul-float/2addr v3, v10

    .line 1338
    invoke-virtual {v2, v3}, Lcom/samsung/samm/a/h;->a(F)V

    .line 1343
    invoke-virtual {v2}, Lcom/samsung/samm/a/h;->k()I

    move-result v5

    .line 1344
    invoke-virtual {v2}, Lcom/samsung/samm/a/h;->l()[Landroid/graphics/PointF;

    move-result-object v6

    .line 1345
    if-nez v6, :cond_f

    .line 1346
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1307
    :cond_c
    invoke-virtual {p0, v4, v3, v10, v11}, Lcom/samsung/samm/b/a;->a(Landroid/graphics/RectF;FFF)Landroid/graphics/RectF;

    move-result-object v3

    .line 1308
    iget v5, v3, Landroid/graphics/RectF;->left:F

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 1309
    iget v5, v3, Landroid/graphics/RectF;->right:F

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 1310
    iget v5, v3, Landroid/graphics/RectF;->top:F

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 1311
    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iput v3, v4, Landroid/graphics/RectF;->bottom:F

    goto :goto_4

    .line 1315
    :cond_d
    if-eqz v9, :cond_e

    .line 1316
    iget v3, v4, Landroid/graphics/RectF;->left:F

    mul-float/2addr v3, v10

    iput v3, v4, Landroid/graphics/RectF;->left:F

    .line 1317
    iget v3, v4, Landroid/graphics/RectF;->right:F

    mul-float/2addr v3, v10

    iput v3, v4, Landroid/graphics/RectF;->right:F

    .line 1319
    :cond_e
    if-eqz v8, :cond_b

    .line 1320
    iget v3, v4, Landroid/graphics/RectF;->top:F

    mul-float/2addr v3, v11

    iput v3, v4, Landroid/graphics/RectF;->top:F

    .line 1321
    iget v3, v4, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v3, v11

    iput v3, v4, Landroid/graphics/RectF;->bottom:F

    goto :goto_4

    .line 1347
    :cond_f
    const/4 v3, 0x0

    :goto_5
    if-lt v3, v5, :cond_12

    .line 1363
    invoke-virtual {v2, v6}, Lcom/samsung/samm/a/h;->a([Landroid/graphics/PointF;)V

    .line 1367
    const-string v3, "SAMM___LIBRARY___ORIGINAL___RECT___KEY"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lcom/samsung/samm/a/h;->b(Ljava/lang/String;I)I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_15

    .line 1368
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 1369
    const-string v4, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_left"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/samsung/samm/a/h;->b(Ljava/lang/String;I)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 1370
    const-string v4, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_top"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/samsung/samm/a/h;->b(Ljava/lang/String;I)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 1371
    const-string v4, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_right"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/samsung/samm/a/h;->b(Ljava/lang/String;I)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 1372
    const-string v4, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_bottom"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/samsung/samm/a/h;->b(Ljava/lang/String;I)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 1374
    if-eqz v9, :cond_10

    .line 1375
    iget v4, v3, Landroid/graphics/RectF;->left:F

    mul-float/2addr v4, v10

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 1376
    iget v4, v3, Landroid/graphics/RectF;->right:F

    mul-float/2addr v4, v10

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 1378
    :cond_10
    if-eqz v8, :cond_11

    .line 1379
    iget v4, v3, Landroid/graphics/RectF;->top:F

    mul-float/2addr v4, v11

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 1380
    iget v4, v3, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v4, v11

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 1384
    :cond_11
    const-string v4, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_left"

    iget v5, v3, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    invoke-virtual {v2, v4, v5}, Lcom/samsung/samm/a/h;->a(Ljava/lang/String;I)Z

    .line 1385
    const-string v4, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_top"

    iget v5, v3, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    invoke-virtual {v2, v4, v5}, Lcom/samsung/samm/a/h;->a(Ljava/lang/String;I)Z

    .line 1386
    const-string v4, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_right"

    iget v5, v3, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    invoke-virtual {v2, v4, v5}, Lcom/samsung/samm/a/h;->a(Ljava/lang/String;I)Z

    .line 1387
    const-string v4, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_bottom"

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v3

    invoke-virtual {v2, v4, v3}, Lcom/samsung/samm/a/h;->a(Ljava/lang/String;I)Z

    goto/16 :goto_3

    .line 1348
    :cond_12
    aget-object v7, v6, v3

    .line 1349
    new-instance v13, Landroid/graphics/PointF;

    iget v14, v7, Landroid/graphics/PointF;->x:F

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-direct {v13, v14, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1350
    if-eqz v9, :cond_13

    .line 1351
    iget v7, v13, Landroid/graphics/PointF;->x:F

    mul-float/2addr v7, v10

    iput v7, v13, Landroid/graphics/PointF;->x:F

    .line 1353
    :cond_13
    if-eqz v8, :cond_14

    .line 1354
    iget v7, v13, Landroid/graphics/PointF;->y:F

    mul-float/2addr v7, v11

    iput v7, v13, Landroid/graphics/PointF;->y:F

    .line 1361
    :cond_14
    aput-object v13, v6, v3

    .line 1347
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    .line 1389
    :cond_15
    const-string v3, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_left"

    iget v5, v4, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    invoke-virtual {v2, v3, v5}, Lcom/samsung/samm/a/h;->a(Ljava/lang/String;I)Z

    .line 1390
    const-string v3, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_top"

    iget v5, v4, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    invoke-virtual {v2, v3, v5}, Lcom/samsung/samm/a/h;->a(Ljava/lang/String;I)Z

    .line 1391
    const-string v3, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_right"

    iget v5, v4, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    invoke-virtual {v2, v3, v5}, Lcom/samsung/samm/a/h;->a(Ljava/lang/String;I)Z

    .line 1392
    const-string v3, "SAMM___LIBRARY___ORIGINAL___RECT___KEY_bottom"

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/samm/a/h;->a(Ljava/lang/String;I)Z

    goto/16 :goto_3

    .line 1395
    :cond_16
    instance-of v3, v2, Lcom/samsung/samm/a/e;

    if-eqz v3, :cond_19

    .line 1396
    check-cast v2, Lcom/samsung/samm/a/e;

    .line 1397
    invoke-virtual {v2}, Lcom/samsung/samm/a/e;->k()Landroid/graphics/PointF;

    move-result-object v3

    .line 1398
    if-eqz v9, :cond_17

    .line 1399
    iget v4, v3, Landroid/graphics/PointF;->x:F

    mul-float/2addr v4, v10

    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 1401
    :cond_17
    if-eqz v8, :cond_18

    .line 1402
    iget v4, v3, Landroid/graphics/PointF;->y:F

    mul-float/2addr v4, v11

    iput v4, v3, Landroid/graphics/PointF;->y:F

    .line 1404
    :cond_18
    invoke-virtual {v2, v3}, Lcom/samsung/samm/a/e;->a(Landroid/graphics/PointF;)V

    goto/16 :goto_3

    .line 1406
    :cond_19
    instance-of v3, v2, Lcom/samsung/samm/a/i;

    if-eqz v3, :cond_1b

    .line 1407
    check-cast v2, Lcom/samsung/samm/a/i;

    .line 1408
    invoke-virtual {v2}, Lcom/samsung/samm/a/i;->c()F

    move-result v3

    .line 1413
    if-eqz v9, :cond_1a

    .line 1414
    mul-float/2addr v3, v10

    .line 1419
    :cond_1a
    invoke-virtual {v2, v3}, Lcom/samsung/samm/a/i;->a(F)V

    goto/16 :goto_3

    .line 1421
    :cond_1b
    instance-of v3, v2, Lcom/samsung/samm/a/f;

    if-eqz v3, :cond_4

    .line 1422
    check-cast v2, Lcom/samsung/samm/a/f;

    .line 1423
    invoke-virtual {v2}, Lcom/samsung/samm/a/f;->k()Ljava/util/LinkedList;

    move-result-object v3

    move-object v2, p0

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/samm/b/a;->a(Ljava/util/LinkedList;IIII)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1424
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 3125
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3127
    :goto_0
    return p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/d;->e(Ljava/lang/String;I)I

    move-result p2

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 3070
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3072
    :goto_0
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method b()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 350
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 351
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "google_sdk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "sdk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v0, v1

    .line 384
    :goto_0
    return v0

    .line 357
    :cond_1
    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 358
    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 359
    if-eqz v3, :cond_2

    if-nez v4, :cond_3

    .line 360
    :cond_2
    const-string v1, "SAMMLibrary"

    const-string v2, "Unknown Brand/Manufacturer Device"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 363
    :cond_3
    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 364
    const-string v6, "Samsung"

    invoke-virtual {v3, v6}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "Samsung"

    invoke-virtual {v4, v6}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_4

    .line 365
    const-string v1, "SAMMLibrary"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Device("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "), Model("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "), Brand("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), Manufacturer("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is not a Saumsung device."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 384
    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1889
    if-nez p1, :cond_1

    .line 1890
    const-string v0, "SAMMLibrary"

    const-string v1, "Invalid SAMM File Path"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1903
    :cond_0
    :goto_0
    return v2

    .line 1894
    :cond_1
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1898
    invoke-virtual {p0}, Lcom/samsung/samm/b/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1903
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;ZZZILjava/lang/String;)Z

    move-result v2

    goto :goto_0
.end method

.method public b(Ljava/lang/String;[B)[B
    .locals 1

    .prologue
    .line 3235
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3237
    :goto_0
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/d;->b(Ljava/lang/String;[B)[B

    move-result-object p2

    goto :goto_0
.end method

.method public b(Ljava/lang/String;[I)[I
    .locals 2

    .prologue
    .line 3293
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3301
    :cond_0
    :goto_0
    return-object p2

    .line 3295
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/samsung/samm/b/a/d;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    .line 3296
    if-eqz v0, :cond_0

    .line 3298
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    .line 3299
    invoke-virtual {v0}, Ljava/nio/IntBuffer;->remaining()I

    move-result v1

    new-array p2, v1, [I

    .line 3300
    invoke-virtual {v0, p2}, Ljava/nio/IntBuffer;->get([I)Ljava/nio/IntBuffer;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 3180
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3182
    :goto_0
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/d;->b(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method c()Z
    .locals 2

    .prologue
    .line 2104
    invoke-static {}, Lcom/samsung/samm/b/a/d;->w()Ljava/lang/String;

    move-result-object v0

    .line 2105
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 2106
    :cond_0
    const-string v0, "SAMMLibrary"

    const-string v1, "Application ID Name must be specified for data compatibility. Call setAppID() before save file."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2107
    const/4 v0, 0x0

    .line 2109
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2847
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2848
    const/4 v0, 0x0

    .line 2849
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0, p1}, Lcom/samsung/samm/b/a/d;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public d()Lcom/samsung/samm/a/a;
    .locals 1

    .prologue
    .line 4207
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->L()Lcom/samsung/samm/a/a;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 4227
    invoke-direct {p0}, Lcom/samsung/samm/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4228
    const/4 v0, 0x0

    .line 4230
    :goto_0
    return v0

    .line 4229
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a;->a:Lcom/samsung/samm/b/a/d;

    invoke-virtual {v0}, Lcom/samsung/samm/b/a/d;->e()V

    .line 4230
    const/4 v0, 0x1

    goto :goto_0
.end method
