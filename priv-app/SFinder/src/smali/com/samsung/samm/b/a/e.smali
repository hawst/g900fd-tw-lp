.class public Lcom/samsung/samm/b/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/samm/b/a/e$a;,
        Lcom/samsung/samm/b/a/e$b;,
        Lcom/samsung/samm/b/a/e$c;,
        Lcom/samsung/samm/b/a/e$d;,
        Lcom/samsung/samm/b/a/e$e;,
        Lcom/samsung/samm/b/a/e$f;,
        Lcom/samsung/samm/b/a/e$g;
    }
.end annotation


# instance fields
.field private a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/b/a/e$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    .line 28
    return-void
.end method

.method private a([BILcom/samsung/samm/b/a/e$a;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 971
    .line 974
    iget-object v2, p3, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    .line 975
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    .line 976
    invoke-static {p1, p2, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 977
    if-lez v4, :cond_0

    .line 978
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    move v2, v1

    .line 979
    :goto_0
    if-lt v2, v4, :cond_2

    .line 984
    :cond_0
    instance-of v2, p3, Lcom/samsung/samm/b/a/e$d;

    if-eqz v2, :cond_3

    .line 986
    check-cast p3, Lcom/samsung/samm/b/a/e$d;

    invoke-static {p3}, Lcom/samsung/samm/b/a/e$d;->a(Lcom/samsung/samm/b/a/e$d;)I

    move-result v1

    .line 987
    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    .line 1034
    :cond_1
    :goto_1
    sub-int/2addr v0, p2

    :goto_2
    return v0

    .line 980
    :cond_2
    aget-char v3, v5, v2

    invoke-static {p1, v0, v3}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v3

    .line 979
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    .line 988
    :cond_3
    instance-of v2, p3, Lcom/samsung/samm/b/a/e$f;

    if-eqz v2, :cond_4

    .line 990
    check-cast p3, Lcom/samsung/samm/b/a/e$f;

    invoke-static {p3}, Lcom/samsung/samm/b/a/e$f;->a(Lcom/samsung/samm/b/a/e$f;)Ljava/lang/String;

    move-result-object v2

    .line 991
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    .line 992
    invoke-static {p1, v0, v3}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 993
    if-lez v3, :cond_1

    .line 994
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 995
    :goto_3
    if-ge v1, v3, :cond_1

    .line 996
    aget-char v4, v2, v1

    invoke-static {p1, v0, v4}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 995
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 999
    :cond_4
    instance-of v2, p3, Lcom/samsung/samm/b/a/e$b;

    if-eqz v2, :cond_6

    .line 1001
    check-cast p3, Lcom/samsung/samm/b/a/e$b;

    invoke-static {p3}, Lcom/samsung/samm/b/a/e$b;->a(Lcom/samsung/samm/b/a/e$b;)Z

    move-result v3

    .line 1002
    add-int/lit8 v2, v0, 0x1

    if-eqz v3, :cond_5

    const/4 v1, 0x1

    :cond_5
    int-to-byte v1, v1

    aput-byte v1, p1, v0

    move v0, v2

    .line 1003
    goto :goto_1

    :cond_6
    instance-of v2, p3, Lcom/samsung/samm/b/a/e$g;

    if-eqz v2, :cond_9

    .line 1005
    check-cast p3, Lcom/samsung/samm/b/a/e$g;

    invoke-static {p3}, Lcom/samsung/samm/b/a/e$g;->a(Lcom/samsung/samm/b/a/e$g;)[Ljava/lang/String;

    move-result-object v5

    .line 1006
    array-length v6, v5

    .line 1007
    invoke-static {p1, v0, v6}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    move v4, v1

    .line 1008
    :goto_4
    if-ge v4, v6, :cond_1

    .line 1010
    aget-object v2, v5, v4

    .line 1011
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    .line 1012
    invoke-static {p1, v0, v7}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 1013
    if-lez v7, :cond_7

    .line 1014
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    move v2, v1

    .line 1015
    :goto_5
    if-lt v2, v7, :cond_8

    .line 1008
    :cond_7
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_4

    .line 1016
    :cond_8
    aget-char v3, v8, v2

    invoke-static {p1, v0, v3}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v3

    .line 1015
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5

    .line 1020
    :cond_9
    instance-of v1, p3, Lcom/samsung/samm/b/a/e$c;

    if-eqz v1, :cond_a

    .line 1022
    check-cast p3, Lcom/samsung/samm/b/a/e$c;

    invoke-static {p3}, Lcom/samsung/samm/b/a/e$c;->a(Lcom/samsung/samm/b/a/e$c;)[B

    move-result-object v1

    .line 1023
    array-length v2, v1

    .line 1024
    invoke-static {p1, v0, v2}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    .line 1025
    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->a([BI[B)I

    move-result v0

    goto/16 :goto_1

    .line 1026
    :cond_a
    instance-of v1, p3, Lcom/samsung/samm/b/a/e$e;

    if-eqz v1, :cond_b

    .line 1028
    check-cast p3, Lcom/samsung/samm/b/a/e$e;

    invoke-static {p3}, Lcom/samsung/samm/b/a/e$e;->a(Lcom/samsung/samm/b/a/e$e;)S

    move-result v1

    .line 1029
    invoke-static {p1, v0, v1}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    goto/16 :goto_1

    .line 1031
    :cond_b
    const/4 v0, -0x1

    goto/16 :goto_2
.end method

.method private a(Ljava/io/RandomAccessFile;IIZZ)Z
    .locals 11

    .prologue
    .line 702
    if-eqz p2, :cond_0

    .line 703
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    .line 704
    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    .line 705
    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    .line 706
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    .line 707
    const/4 v0, 0x5

    if-eq p2, v0, :cond_0

    .line 708
    :try_start_0
    invoke-virtual {p1, p3}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 709
    const/4 v0, 0x1

    .line 794
    :goto_0
    return v0

    .line 713
    :cond_0
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v2

    .line 714
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v4

    .line 715
    new-array v5, v4, [C

    .line 716
    mul-int/lit8 v0, v4, 0x2

    new-array v6, v0, [B

    .line 717
    const/4 v1, 0x0

    .line 718
    const/4 v0, 0x1

    new-array v7, v0, [I

    .line 719
    invoke-virtual {p1, v6}, Ljava/io/RandomAccessFile;->read([B)I

    .line 720
    const/4 v0, 0x0

    :goto_1
    if-lt v0, v4, :cond_2

    .line 724
    invoke-static {v5}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v4

    .line 728
    if-nez p4, :cond_3

    if-eqz p5, :cond_1

    const-string v0, "SAMM___LIBRARY___TAG___KEY"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 729
    :cond_1
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    sub-int v0, p3, v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 730
    const/4 v0, 0x1

    goto :goto_0

    .line 721
    :cond_2
    invoke-static {v6, v1, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 722
    const/4 v8, 0x0

    aget v8, v7, v8

    int-to-char v8, v8

    aput-char v8, v5, v0

    .line 720
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 733
    :cond_3
    if-nez p2, :cond_5

    .line 735
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v0

    .line 736
    invoke-virtual {p0, v4, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;I)Z

    .line 794
    :cond_4
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 737
    :cond_5
    const/4 v0, 0x1

    if-ne p2, v0, :cond_7

    .line 739
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v2

    .line 740
    new-array v3, v2, [C

    .line 741
    mul-int/lit8 v0, v2, 0x2

    new-array v5, v0, [B

    .line 742
    const/4 v1, 0x0

    .line 743
    invoke-virtual {p1, v5}, Ljava/io/RandomAccessFile;->read([B)I

    .line 744
    const/4 v0, 0x0

    :goto_3
    if-lt v0, v2, :cond_6

    .line 748
    invoke-static {v3}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    .line 749
    invoke-virtual {p0, v4, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 786
    :catch_0
    move-exception v0

    .line 787
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FileNotFoundException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 745
    :cond_6
    :try_start_1
    invoke-static {v5, v1, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 746
    const/4 v6, 0x0

    aget v6, v7, v6

    int-to-char v6, v6

    aput-char v6, v3, v0

    .line 744
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 750
    :cond_7
    const/4 v0, 0x2

    if-ne p2, v0, :cond_8

    .line 752
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readBoolean()Z

    move-result v0

    .line 753
    invoke-virtual {p0, v4, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;Z)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 789
    :catch_1
    move-exception v0

    .line 790
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IOException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 754
    :cond_8
    const/4 v0, 0x3

    if-ne p2, v0, :cond_b

    .line 756
    :try_start_2
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v3

    .line 757
    new-array v5, v3, [Ljava/lang/String;

    .line 758
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    if-lt v2, v3, :cond_9

    .line 772
    invoke-virtual {p0, v4, v5}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    goto :goto_2

    .line 760
    :cond_9
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v6

    .line 761
    new-array v8, v6, [C

    .line 762
    mul-int/lit8 v0, v6, 0x2

    new-array v9, v0, [B

    .line 763
    const/4 v1, 0x0

    .line 764
    invoke-virtual {p1, v9}, Ljava/io/RandomAccessFile;->read([B)I

    .line 765
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v6, :cond_a

    .line 769
    invoke-static {v8}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    .line 770
    aput-object v0, v5, v2

    .line 758
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 766
    :cond_a
    invoke-static {v9, v1, v7}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v1

    .line 767
    const/4 v10, 0x0

    aget v10, v7, v10

    int-to-char v10, v10

    aput-char v10, v8, v0

    .line 765
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 773
    :cond_b
    const/4 v0, 0x4

    if-ne p2, v0, :cond_c

    .line 775
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v0

    .line 776
    new-array v0, v0, [B

    .line 777
    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->read([B)I

    .line 778
    invoke-virtual {p0, v4, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;[B)Z

    goto/16 :goto_2

    .line 779
    :cond_c
    const/4 v0, 0x5

    if-ne p2, v0, :cond_4

    .line 781
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->c(Ljava/io/RandomAccessFile;)I

    move-result v0

    int-to-short v0, v0

    .line 782
    invoke-virtual {p0, v4, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;S)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2
.end method

.method private a([BII)Z
    .locals 12

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 799
    .line 800
    new-array v5, v1, [I

    .line 802
    if-eqz p2, :cond_1

    .line 803
    if-eq p2, v1, :cond_1

    .line 804
    if-eq p2, v8, :cond_1

    .line 805
    if-eq p2, v9, :cond_1

    .line 806
    if-eq p2, v10, :cond_1

    .line 807
    const/4 v0, 0x5

    if-eq p2, v0, :cond_1

    .line 878
    :cond_0
    :goto_0
    return v1

    .line 813
    :cond_1
    invoke-static {p1, p3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 814
    aget v4, v5, v2

    .line 815
    new-array v6, v4, [C

    move v3, v0

    move v0, v2

    .line 816
    :goto_1
    if-lt v0, v4, :cond_2

    .line 820
    invoke-static {v6}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v6

    .line 824
    if-nez p2, :cond_3

    .line 826
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    .line 827
    aget v0, v5, v2

    .line 828
    invoke-virtual {p0, v6, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;I)Z

    goto :goto_0

    .line 817
    :cond_2
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 818
    aget v7, v5, v2

    int-to-char v7, v7

    aput-char v7, v6, v0

    .line 816
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 829
    :cond_3
    if-ne p2, v1, :cond_5

    .line 831
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 832
    aget v4, v5, v2

    .line 833
    new-array v7, v4, [C

    move v3, v0

    move v0, v2

    .line 834
    :goto_2
    if-lt v0, v4, :cond_4

    .line 838
    invoke-static {v7}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    .line 839
    invoke-virtual {p0, v6, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 835
    :cond_4
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 836
    aget v8, v5, v2

    int-to-char v8, v8

    aput-char v8, v7, v0

    .line 834
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 840
    :cond_5
    if-ne p2, v8, :cond_7

    .line 842
    aget-byte v0, p1, v3

    if-lez v0, :cond_6

    move v0, v1

    .line 843
    :goto_3
    invoke-virtual {p0, v6, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;Z)Z

    goto :goto_0

    :cond_6
    move v0, v2

    .line 842
    goto :goto_3

    .line 844
    :cond_7
    if-ne p2, v9, :cond_a

    .line 846
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v3

    .line 847
    aget v7, v5, v2

    .line 848
    new-array v8, v7, [Ljava/lang/String;

    move v4, v2

    .line 849
    :goto_4
    if-lt v4, v7, :cond_8

    .line 861
    invoke-virtual {p0, v6, v8}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    goto :goto_0

    .line 851
    :cond_8
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 852
    aget v9, v5, v2

    .line 853
    new-array v10, v9, [C

    move v3, v0

    move v0, v2

    .line 854
    :goto_5
    if-lt v0, v9, :cond_9

    .line 858
    invoke-static {v10}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    .line 859
    aput-object v0, v8, v4

    .line 849
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    .line 855
    :cond_9
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 856
    aget v11, v5, v2

    int-to-char v11, v11

    aput-char v11, v10, v0

    .line 854
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 862
    :cond_a
    if-ne p2, v10, :cond_b

    .line 864
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v3

    .line 865
    aget v0, v5, v2

    .line 866
    filled-new-array {v1, v0}, [I

    move-result-object v0

    sget-object v4, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-static {v4, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[B

    .line 867
    invoke-static {p1, v3, v0}, Lcom/samsung/samm/b/a/s;->a([BI[[B)I

    .line 868
    aget-object v0, v0, v2

    invoke-virtual {p0, v6, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 869
    :cond_b
    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    .line 871
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    .line 872
    aget v0, v5, v2

    int-to-short v0, v0

    .line 873
    invoke-virtual {p0, v6, v0}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;S)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 1212
    const/4 v0, 0x0

    .line 1213
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-eqz v1, :cond_0

    .line 1214
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    .line 1216
    :cond_0
    return v0
.end method

.method public a(Ljava/io/RandomAccessFile;ZZ)I
    .locals 10

    .prologue
    .line 1040
    .line 1042
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I

    move-result v8

    .line 1043
    const/4 v1, 0x4

    .line 1047
    const/4 v0, 0x0

    move v7, v0

    move v0, v1

    :goto_0
    if-lt v7, v8, :cond_0

    .line 1072
    :goto_1
    return v0

    .line 1049
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedByte()I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 1050
    add-int/lit8 v1, v0, 0x1

    .line 1053
    :try_start_1
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v3

    .line 1054
    add-int/lit8 v6, v1, 0x4

    .line 1055
    if-nez p2, :cond_1

    if-eqz p3, :cond_2

    const/4 v0, 0x3

    if-ne v2, v0, :cond_2

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    .line 1056
    :try_start_2
    invoke-direct/range {v0 .. v5}, Lcom/samsung/samm/b/a/e;->a(Ljava/io/RandomAccessFile;IIZZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1057
    const/4 v0, -0x1

    goto :goto_1

    .line 1060
    :cond_2
    invoke-virtual {p1, v3}, Ljava/io/RandomAccessFile;->skipBytes(I)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1062
    :cond_3
    add-int v1, v6, v3

    .line 1047
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v0, v1

    goto :goto_0

    .line 1064
    :catch_0
    move-exception v1

    .line 1065
    :goto_2
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FileNotFoundException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 1067
    :catch_1
    move-exception v1

    .line 1068
    :goto_3
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1067
    :catch_2
    move-exception v0

    move-object v9, v0

    move v0, v1

    move-object v1, v9

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v1, v0

    move v0, v6

    goto :goto_3

    .line 1064
    :catch_4
    move-exception v0

    move-object v9, v0

    move v0, v1

    move-object v1, v9

    goto :goto_2

    :catch_5
    move-exception v0

    move-object v1, v0

    move v0, v6

    goto :goto_2
.end method

.method public a([BI)I
    .locals 8

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 1077
    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    .line 1100
    :cond_0
    :goto_0
    return v0

    .line 1080
    :cond_1
    const/4 v1, 0x1

    new-array v4, v1, [I

    .line 1082
    invoke-static {p1, p2, v4}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v1

    .line 1083
    aget v5, v4, v2

    move v3, v1

    move v1, v2

    .line 1086
    :goto_1
    if-lt v1, v5, :cond_2

    .line 1100
    sub-int v0, v3, p2

    goto :goto_0

    .line 1088
    :cond_2
    add-int/lit8 v6, v3, 0x1

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    .line 1091
    invoke-static {p1, v6, v4}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v6

    .line 1092
    aget v7, v4, v2

    .line 1094
    invoke-direct {p0, p1, v3, v6}, Lcom/samsung/samm/b/a/e;->a([BII)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1097
    add-int v3, v6, v7

    .line 1086
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 542
    if-nez p1, :cond_0

    .line 543
    const-string v0, "SAMMLibraryCore"

    const-string v1, "keyName is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    :goto_0
    return v2

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-nez v0, :cond_1

    .line 549
    const-string v0, "SAMMLibraryCore"

    const-string v1, "There is no item"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 555
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 566
    const-string v0, "SAMMLibraryCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "There is no item of key "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 555
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 556
    instance-of v4, v0, Lcom/samsung/samm/b/a/e$c;

    if-eqz v4, :cond_4

    iget-object v0, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 557
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 558
    const/4 v2, 0x1

    goto :goto_0

    .line 560
    :cond_3
    const-string v0, "SAMMLibraryCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "remove item of key \'"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\' error"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 564
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 228
    if-nez p1, :cond_0

    .line 245
    :goto_0
    return v0

    .line 232
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-nez v1, :cond_1

    .line 233
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    .line 237
    :cond_1
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 245
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v1, Lcom/samsung/samm/b/a/e$d;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/samm/b/a/e$d;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 237
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 238
    instance-of v3, v0, Lcom/samsung/samm/b/a/e$d;

    if-eqz v3, :cond_3

    iget-object v0, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v2, Lcom/samsung/samm/b/a/e$d;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/samm/b/a/e$d;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 240
    const/4 v0, 0x1

    goto :goto_0

    .line 242
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 142
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 160
    :cond_0
    :goto_0
    return v0

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-nez v1, :cond_2

    .line 147
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    .line 151
    :cond_2
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 160
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v1, Lcom/samsung/samm/b/a/e$f;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/samm/b/a/e$f;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 151
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 152
    instance-of v3, v0, Lcom/samsung/samm/b/a/e$f;

    if-eqz v3, :cond_4

    iget-object v0, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 154
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v2, Lcom/samsung/samm/b/a/e$f;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/samm/b/a/e$f;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 155
    const/4 v0, 0x1

    goto :goto_0

    .line 157
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;S)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 582
    if-nez p1, :cond_0

    .line 599
    :goto_0
    return v0

    .line 586
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-nez v1, :cond_1

    .line 587
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    .line 591
    :cond_1
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 599
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v1, Lcom/samsung/samm/b/a/e$e;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-direct {v1, p0, p1, v2}, Lcom/samsung/samm/b/a/e$e;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;Ljava/lang/Short;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 591
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 592
    instance-of v3, v0, Lcom/samsung/samm/b/a/e$e;

    if-eqz v3, :cond_3

    iget-object v0, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 593
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v2, Lcom/samsung/samm/b/a/e$e;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    invoke-direct {v2, p0, p1, v3}, Lcom/samsung/samm/b/a/e$e;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;Ljava/lang/Short;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 594
    const/4 v0, 0x1

    goto :goto_0

    .line 596
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 314
    if-nez p1, :cond_0

    .line 331
    :goto_0
    return v0

    .line 318
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-nez v1, :cond_1

    .line 319
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    .line 323
    :cond_1
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 331
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v1, Lcom/samsung/samm/b/a/e$b;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/samm/b/a/e$b;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 323
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 324
    instance-of v3, v0, Lcom/samsung/samm/b/a/e$b;

    if-eqz v3, :cond_3

    iget-object v0, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 325
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v2, Lcom/samsung/samm/b/a/e$b;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/samm/b/a/e$b;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 326
    const/4 v0, 0x1

    goto :goto_0

    .line 328
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;[B)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 492
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v1, p2

    if-gtz v1, :cond_1

    .line 512
    :cond_0
    :goto_0
    return v0

    .line 496
    :cond_1
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-nez v1, :cond_2

    .line 497
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    .line 501
    :cond_2
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 510
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v1, Lcom/samsung/samm/b/a/e$c;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/samm/b/a/e$c;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;[B)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 512
    goto :goto_0

    .line 501
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 502
    instance-of v4, v0, Lcom/samsung/samm/b/a/e$c;

    if-eqz v4, :cond_4

    iget-object v0, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 504
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v3, Lcom/samsung/samm/b/a/e$c;

    invoke-direct {v3, p0, p1, p2}, Lcom/samsung/samm/b/a/e$c;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;[B)V

    invoke-virtual {v0, v1, v3}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    .line 505
    goto :goto_0

    .line 507
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 401
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v1, p2

    if-gtz v1, :cond_1

    .line 421
    :cond_0
    :goto_0
    return v0

    .line 405
    :cond_1
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-nez v1, :cond_2

    .line 406
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    .line 410
    :cond_2
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 419
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v1, Lcom/samsung/samm/b/a/e$g;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/samm/b/a/e$g;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 421
    goto :goto_0

    .line 410
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 411
    instance-of v4, v0, Lcom/samsung/samm/b/a/e$g;

    if-eqz v4, :cond_4

    iget-object v0, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 413
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    new-instance v3, Lcom/samsung/samm/b/a/e$g;

    invoke-direct {v3, p0, p1, p2}, Lcom/samsung/samm/b/a/e$g;-><init>(Lcom/samsung/samm/b/a/e;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    .line 414
    goto :goto_0

    .line 416
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public b()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1221
    .line 1222
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    .line 1223
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1224
    const/4 v0, 0x4

    .line 1225
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v2, v1

    .line 1255
    :cond_1
    return v2

    .line 1225
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 1226
    add-int/lit8 v1, v1, 0x1

    .line 1227
    add-int/lit8 v1, v1, 0x4

    .line 1228
    add-int/lit8 v1, v1, 0x2

    .line 1229
    iget-object v3, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    .line 1230
    instance-of v3, v0, Lcom/samsung/samm/b/a/e$d;

    if-eqz v3, :cond_3

    .line 1231
    add-int/lit8 v0, v1, 0x4

    move v1, v0

    .line 1232
    goto :goto_0

    :cond_3
    instance-of v3, v0, Lcom/samsung/samm/b/a/e$f;

    if-eqz v3, :cond_4

    .line 1233
    add-int/lit8 v1, v1, 0x2

    .line 1234
    check-cast v0, Lcom/samsung/samm/b/a/e$f;

    invoke-static {v0}, Lcom/samsung/samm/b/a/e$f;->a(Lcom/samsung/samm/b/a/e$f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    move v1, v0

    .line 1235
    goto :goto_0

    :cond_4
    instance-of v3, v0, Lcom/samsung/samm/b/a/e$b;

    if-eqz v3, :cond_5

    .line 1236
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1237
    goto :goto_0

    :cond_5
    instance-of v3, v0, Lcom/samsung/samm/b/a/e$g;

    if-eqz v3, :cond_7

    .line 1238
    add-int/lit8 v1, v1, 0x4

    .line 1239
    check-cast v0, Lcom/samsung/samm/b/a/e$g;

    invoke-static {v0}, Lcom/samsung/samm/b/a/e$g;->a(Lcom/samsung/samm/b/a/e$g;)[Ljava/lang/String;

    move-result-object v5

    move v0, v1

    move v1, v2

    .line 1240
    :goto_1
    array-length v3, v5

    if-lt v1, v3, :cond_6

    move v1, v0

    .line 1244
    goto :goto_0

    .line 1241
    :cond_6
    add-int/lit8 v0, v0, 0x2

    .line 1242
    aget-object v3, v5, v1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v0

    .line 1240
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_1

    .line 1244
    :cond_7
    instance-of v3, v0, Lcom/samsung/samm/b/a/e$c;

    if-eqz v3, :cond_8

    .line 1245
    add-int/lit8 v1, v1, 0x4

    .line 1246
    check-cast v0, Lcom/samsung/samm/b/a/e$c;

    invoke-static {v0}, Lcom/samsung/samm/b/a/e$c;->a(Lcom/samsung/samm/b/a/e$c;)[B

    move-result-object v0

    array-length v0, v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1247
    goto :goto_0

    :cond_8
    instance-of v0, v0, Lcom/samsung/samm/b/a/e$e;

    if-eqz v0, :cond_0

    .line 1248
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;I)I
    .locals 3

    .prologue
    .line 255
    if-nez p1, :cond_1

    .line 268
    :cond_0
    :goto_0
    return p2

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 264
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$d;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 265
    check-cast v0, Lcom/samsung/samm/b/a/e$d;

    invoke-static {v0}, Lcom/samsung/samm/b/a/e$d;->a(Lcom/samsung/samm/b/a/e$d;)I

    move-result p2

    goto :goto_0
.end method

.method public b([BI)I
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 1166
    .line 1168
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/e;->a()I

    move-result v0

    .line 1169
    invoke-static {p1, p2, v0}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v0

    .line 1172
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-eqz v1, :cond_8

    .line 1173
    iget-object v1, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1207
    :goto_1
    sub-int v0, v1, p2

    :goto_2
    return v0

    .line 1173
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 1175
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$d;

    if-eqz v2, :cond_1

    .line 1176
    add-int/lit8 v2, v1, 0x1

    const/4 v5, 0x0

    aput-byte v5, p1, v1

    move v1, v2

    .line 1194
    :goto_3
    add-int/lit8 v2, v1, 0x4

    .line 1196
    invoke-direct {p0, p1, v2, v0}, Lcom/samsung/samm/b/a/e;->a([BILcom/samsung/samm/b/a/e$a;)I

    move-result v5

    .line 1197
    if-gez v5, :cond_7

    move v0, v3

    .line 1198
    goto :goto_2

    .line 1177
    :cond_1
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$f;

    if-eqz v2, :cond_2

    .line 1178
    add-int/lit8 v2, v1, 0x1

    const/4 v5, 0x1

    aput-byte v5, p1, v1

    move v1, v2

    .line 1179
    goto :goto_3

    :cond_2
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$b;

    if-eqz v2, :cond_3

    .line 1180
    add-int/lit8 v2, v1, 0x1

    const/4 v5, 0x2

    aput-byte v5, p1, v1

    move v1, v2

    .line 1181
    goto :goto_3

    :cond_3
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$g;

    if-eqz v2, :cond_4

    .line 1182
    add-int/lit8 v2, v1, 0x1

    const/4 v5, 0x3

    aput-byte v5, p1, v1

    move v1, v2

    .line 1183
    goto :goto_3

    :cond_4
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$c;

    if-eqz v2, :cond_5

    .line 1184
    add-int/lit8 v2, v1, 0x1

    const/4 v5, 0x4

    aput-byte v5, p1, v1

    move v1, v2

    .line 1185
    goto :goto_3

    :cond_5
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$e;

    if-eqz v2, :cond_6

    .line 1186
    add-int/lit8 v2, v1, 0x1

    const/4 v5, 0x5

    aput-byte v5, p1, v1

    move v1, v2

    .line 1187
    goto :goto_3

    :cond_6
    move v0, v3

    .line 1188
    goto :goto_2

    .line 1200
    :cond_7
    add-int v0, v2, v5

    .line 1203
    invoke-static {p1, v1, v5}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move v1, v0

    goto :goto_0

    :cond_8
    move v1, v0

    goto :goto_1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 169
    if-nez p1, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-object p2

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 178
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$f;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 180
    check-cast v0, Lcom/samsung/samm/b/a/e$f;

    invoke-static {v0}, Lcom/samsung/samm/b/a/e$f;->a(Lcom/samsung/samm/b/a/e$f;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public b(Ljava/lang/String;[B)[B
    .locals 3

    .prologue
    .line 521
    if-nez p1, :cond_1

    .line 534
    :cond_0
    :goto_0
    return-object p2

    .line 525
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 530
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$c;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 531
    check-cast v0, Lcom/samsung/samm/b/a/e$c;

    invoke-static {v0}, Lcom/samsung/samm/b/a/e$c;->a(Lcom/samsung/samm/b/a/e$c;)[B

    move-result-object v0

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    move-object p2, v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 430
    if-nez p1, :cond_1

    .line 443
    :cond_0
    :goto_0
    return-object p2

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/samsung/samm/b/a/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/b/a/e$a;

    .line 439
    instance-of v2, v0, Lcom/samsung/samm/b/a/e$g;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/samsung/samm/b/a/e$a;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 440
    check-cast v0, Lcom/samsung/samm/b/a/e$g;

    invoke-static {v0}, Lcom/samsung/samm/b/a/e$g;->a(Lcom/samsung/samm/b/a/e$g;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object p2, v0

    goto :goto_0
.end method
