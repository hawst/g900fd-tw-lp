.class public Lcom/samsung/samm/b/a/n;
.super Lcom/samsung/samm/b/a/j;
.source "SourceFile"


# instance fields
.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/b/a/n;->f:I

    .line 26
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/n;->a(Lcom/samsung/samm/b/a/j;)V

    .line 27
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/n;->a(I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/b/a/n;->f:I

    .line 32
    invoke-virtual {p0, p1}, Lcom/samsung/samm/b/a/n;->a(Lcom/samsung/samm/b/a/j;)V

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/samm/b/a/n;->a:I

    .line 34
    iput-object p2, p0, Lcom/samsung/samm/b/a/n;->d:Lcom/samsung/samm/a/d;

    .line 35
    return-void
.end method


# virtual methods
.method public a([BI)I
    .locals 14

    .prologue
    .line 79
    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    const/4 v0, -0x1

    .line 236
    :goto_0
    return v0

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/n;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/samm/b/a/n;->b([BI)I

    move-result v0

    .line 84
    if-gez v0, :cond_3

    .line 85
    const/4 v0, -0x1

    goto :goto_0

    .line 86
    :cond_3
    add-int v1, p2, v0

    .line 87
    const/4 v0, 0x1

    new-array v5, v0, [I

    .line 89
    iget-object v0, p0, Lcom/samsung/samm/b/a/n;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/h;

    .line 92
    invoke-static {p1, v1, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v6

    .line 93
    const/4 v1, 0x0

    aget v7, v5, v1

    .line 94
    if-nez v7, :cond_4

    .line 95
    sub-int v0, v6, p2

    goto :goto_0

    .line 96
    :cond_4
    if-gez v7, :cond_5

    .line 97
    const-string v0, "SAMMLibraryCore"

    const-string v1, "SAMM Object Data is Invalid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const/4 v0, -0x1

    goto :goto_0

    .line 101
    :cond_5
    add-int v8, v6, v7

    .line 104
    invoke-static {p1, v6, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 105
    const/4 v1, 0x0

    aget v9, v5, v1

    .line 107
    if-lez v9, :cond_8

    .line 109
    new-array v2, v9, [Landroid/graphics/PointF;

    .line 110
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v9, :cond_6

    if-lt v3, v8, :cond_a

    .line 117
    :cond_6
    invoke-virtual {v0, v2}, Lcom/samsung/samm/a/h;->a([Landroid/graphics/PointF;)V

    .line 120
    const/4 v2, 0x0

    .line 121
    new-array v10, v9, [F

    .line 122
    const/4 v1, 0x0

    move v4, v1

    :goto_2
    if-lt v4, v9, :cond_b

    .line 135
    if-nez v2, :cond_7

    .line 136
    const/4 v1, 0x0

    :goto_3
    array-length v2, v10

    if-lt v1, v2, :cond_e

    .line 140
    :cond_7
    invoke-virtual {v0, v10}, Lcom/samsung/samm/a/h;->a([F)V

    .line 146
    :cond_8
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/samm/b/a/n;->f:I

    .line 147
    if-ge v3, v8, :cond_9

    .line 149
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 150
    const/4 v1, 0x0

    aget v1, v5, v1

    .line 151
    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/h;->e(I)V

    .line 152
    iget v1, p0, Lcom/samsung/samm/b/a/n;->f:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/samm/b/a/n;->f:I

    .line 155
    :cond_9
    sub-int v1, v3, v6

    .line 156
    if-eq v1, v7, :cond_f

    .line 157
    const/4 v0, -0x1

    goto :goto_0

    .line 111
    :cond_a
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    aput-object v4, v2, v1

    .line 112
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 113
    aget-object v4, v2, v1

    const/4 v10, 0x0

    aget v10, v5, v10

    int-to-short v10, v10

    int-to-float v10, v10

    iput v10, v4, Landroid/graphics/PointF;->x:F

    .line 114
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 115
    aget-object v4, v2, v1

    const/4 v10, 0x0

    aget v10, v5, v10

    int-to-short v10, v10

    int-to-float v10, v10

    iput v10, v4, Landroid/graphics/PointF;->y:F

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 124
    :cond_b
    if-ge v3, v8, :cond_d

    .line 125
    invoke-static {p1, v3, v5}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v3

    .line 126
    const/4 v1, 0x0

    aget v1, v5, v1

    int-to-float v1, v1

    const/high16 v11, 0x45800000    # 4096.0f

    div-float/2addr v1, v11

    .line 127
    const/4 v11, 0x0

    cmpl-float v11, v1, v11

    if-lez v11, :cond_c

    .line 128
    const/4 v2, 0x1

    .line 133
    :cond_c
    :goto_4
    aput v1, v10, v4

    .line 122
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 131
    :cond_d
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_4

    .line 137
    :cond_e
    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v10, v1

    .line 136
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 162
    :cond_f
    const-string v1, "SAMM___LIBRARY___PEN___EXTRA___KEY"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/samm/a/h;->b(Ljava/lang/String;[B)[B

    move-result-object v6

    .line 163
    if-eqz v6, :cond_11

    .line 164
    const/4 v1, 0x0

    .line 167
    invoke-static {v6, v1, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v3

    .line 168
    const/4 v1, 0x0

    aget v7, v5, v1

    .line 169
    if-lez v7, :cond_14

    if-lez v9, :cond_14

    .line 171
    new-array v10, v9, [J

    .line 172
    const/4 v1, 0x1

    new-array v11, v1, [J

    .line 173
    const/4 v1, 0x0

    move v2, v1

    move v1, v3

    :goto_5
    if-lt v2, v9, :cond_12

    .line 190
    :goto_6
    invoke-static {v6, v1, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v3

    .line 191
    const/4 v1, 0x0

    aget v4, v5, v1

    .line 192
    if-lez v4, :cond_10

    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->a()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_10

    .line 195
    invoke-static {v6, v3, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v1

    .line 196
    const/4 v2, 0x0

    aget v7, v5, v2

    .line 198
    invoke-static {v6, v1, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v2

    .line 199
    const/4 v1, 0x0

    aget v9, v5, v1

    .line 200
    new-array v10, v9, [I

    .line 201
    const/4 v1, 0x0

    :goto_7
    array-length v11, v10

    if-lt v1, v11, :cond_15

    .line 210
    invoke-static {v6, v2, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v1

    .line 211
    const/4 v2, 0x0

    aget v2, v5, v2

    .line 213
    invoke-static {v6, v1, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    .line 214
    const/4 v1, 0x0

    aget v1, v5, v1

    .line 224
    invoke-virtual {v0, v2}, Lcom/samsung/samm/a/h;->f(I)Z

    .line 225
    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/h;->g(I)Z

    .line 226
    invoke-virtual {v0, v7}, Lcom/samsung/samm/a/h;->i(I)Z

    .line 227
    const/4 v1, 0x0

    :goto_8
    if-ge v1, v9, :cond_10

    const/16 v2, 0x8

    if-lt v1, v2, :cond_17

    .line 233
    :cond_10
    const-string v1, "SAMM___LIBRARY___PEN___EXTRA___KEY"

    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/h;->c(Ljava/lang/String;)Z

    .line 236
    :cond_11
    sub-int v0, v8, p2

    goto/16 :goto_0

    .line 174
    :cond_12
    sub-int v4, v1, v3

    if-ge v4, v7, :cond_13

    add-int/lit8 v4, v1, 0x8

    array-length v12, v6

    if-gt v4, v12, :cond_13

    .line 175
    invoke-static {v6, v1, v11}, Lcom/samsung/samm/b/a/s;->a([BI[J)I

    move-result v4

    .line 176
    const/4 v1, 0x0

    aget-wide v12, v11, v1

    aput-wide v12, v10, v2

    .line 173
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v4

    goto :goto_5

    .line 178
    :cond_13
    const/4 v0, -0x1

    goto/16 :goto_0

    .line 186
    :cond_14
    add-int v1, v3, v7

    goto :goto_6

    .line 202
    :cond_15
    sub-int v11, v2, v3

    if-ge v11, v4, :cond_16

    add-int/lit8 v11, v2, 0x4

    array-length v12, v6

    if-gt v11, v12, :cond_16

    .line 203
    invoke-static {v6, v2, v5}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v2

    .line 204
    const/4 v11, 0x0

    aget v11, v5, v11

    aput v11, v10, v1

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 206
    :cond_16
    const/4 v0, -0x1

    goto/16 :goto_0

    .line 228
    :cond_17
    aget v2, v10, v1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/samm/a/h;->a(II)Z

    .line 227
    add-int/lit8 v1, v1, 0x1

    goto :goto_8
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Lcom/samsung/samm/b/a/j;->a()V

    .line 48
    iget v0, p0, Lcom/samsung/samm/b/a/n;->f:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/samm/b/a/n;->f:I

    .line 49
    return-void
.end method

.method public a([III)[B
    .locals 12

    .prologue
    const/4 v10, 0x4

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 241
    iget-object v0, p0, Lcom/samsung/samm/b/a/n;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-object v2

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/n;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/h;

    .line 243
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->k()I

    move-result v7

    .line 248
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 269
    new-array v1, v10, [B

    .line 270
    invoke-static {v1, v4, v4}, Lcom/samsung/samm/b/a/s;->a([BII)I

    .line 272
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    if-lez v7, :cond_f

    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->a()I

    move-result v1

    const/4 v3, 0x5

    if-ne v1, v3, :cond_f

    .line 302
    const/16 v8, 0x8

    .line 304
    const/16 v1, 0x30

    .line 305
    const/16 v3, 0x34

    new-array v3, v3, [B

    .line 308
    invoke-static {v3, v4, v1}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v1

    .line 310
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->r()I

    move-result v5

    .line 311
    invoke-static {v3, v1, v5}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v1

    .line 313
    invoke-static {v3, v1, v8}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v1

    move v5, v1

    move v1, v4

    .line 314
    :goto_1
    if-lt v1, v8, :cond_4

    .line 319
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->p()I

    move-result v1

    .line 320
    invoke-static {v3, v5, v1}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v1

    .line 322
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->q()I

    move-result v5

    .line 323
    invoke-static {v3, v1, v5}, Lcom/samsung/samm/b/a/s;->a([BII)I

    .line 324
    const/4 v1, 0x1

    move v11, v1

    move-object v1, v3

    move v3, v11

    .line 327
    :goto_2
    if-nez v3, :cond_2

    .line 328
    new-array v1, v10, [B

    .line 329
    invoke-static {v1, v4, v4}, Lcom/samsung/samm/b/a/s;->a([BII)I

    .line 331
    :cond_2
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-virtual {v6}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v6

    move v3, v4

    move v5, v4

    .line 336
    :goto_3
    array-length v1, v6

    if-lt v3, v1, :cond_5

    .line 339
    if-lez v5, :cond_3

    .line 340
    new-array v8, v5, [B

    move v3, v4

    move v5, v4

    .line 342
    :goto_4
    array-length v1, v6

    if-lt v3, v1, :cond_6

    .line 348
    const-string v1, "SAMM___LIBRARY___PEN___EXTRA___KEY"

    invoke-virtual {v0, v1, v8}, Lcom/samsung/samm/a/h;->a(Ljava/lang/String;[B)Z

    .line 351
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/n;->a()V

    .line 353
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/n;->e()I

    move-result v1

    new-array v5, v1, [B

    .line 356
    invoke-virtual {p0, v5, v4, p2, p3}, Lcom/samsung/samm/b/a/n;->a([BIII)I

    move-result v1

    .line 357
    if-ltz v1, :cond_0

    .line 361
    const-string v3, "SAMM___LIBRARY___PEN___EXTRA___KEY"

    invoke-virtual {v0, v3}, Lcom/samsung/samm/a/h;->c(Ljava/lang/String;)Z

    .line 364
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/n;->g()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    .line 365
    invoke-static {v5, v1, v3}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v1

    .line 368
    invoke-static {v5, v1, v7}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v1

    .line 370
    if-lez v7, :cond_a

    .line 372
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->l()[Landroid/graphics/PointF;

    move-result-object v8

    .line 373
    if-nez v8, :cond_7

    .line 374
    const-string v0, "SAMMLibraryCore"

    const-string v1, "Pen points data is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 315
    :cond_4
    invoke-virtual {v0, v1}, Lcom/samsung/samm/a/h;->h(I)I

    move-result v9

    .line 316
    invoke-static {v3, v5, v9}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v5

    .line 314
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 337
    :cond_5
    aget-object v1, v6, v3

    check-cast v1, [B

    array-length v1, v1

    add-int/2addr v5, v1

    .line 336
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 343
    :cond_6
    aget-object v1, v6, v3

    check-cast v1, [B

    array-length v1, v1

    .line 344
    aget-object v9, v6, v3

    invoke-static {v9, v4, v8, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 345
    aput-object v2, v6, v3

    .line 346
    add-int/2addr v5, v1

    .line 342
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_4

    .line 377
    :cond_7
    if-nez p2, :cond_c

    if-nez p3, :cond_c

    move v3, v4

    .line 378
    :goto_5
    if-lt v3, v7, :cond_b

    .line 390
    :cond_8
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->m()I

    move-result v6

    .line 391
    if-eq v6, v7, :cond_9

    .line 392
    const-string v3, "SAMMLibraryCore"

    const-string v8, "Number of points and pressures are diffent!"

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    :cond_9
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->n()[F

    move-result-object v8

    move v3, v1

    .line 397
    :goto_6
    if-lt v4, v7, :cond_d

    move v1, v3

    .line 412
    :cond_a
    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->o()I

    move-result v0

    invoke-static {v5, v1, v0}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 414
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/n;->e()I

    move-result v1

    if-ne v0, v1, :cond_0

    move-object v2, v5

    .line 417
    goto/16 :goto_0

    .line 379
    :cond_b
    aget-object v6, v8, v3

    iget v6, v6, Landroid/graphics/PointF;->x:F

    float-to-int v6, v6

    invoke-static {v5, v1, v6}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v1

    .line 380
    aget-object v6, v8, v3

    iget v6, v6, Landroid/graphics/PointF;->y:F

    float-to-int v6, v6

    invoke-static {v5, v1, v6}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v6

    .line 378
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v6

    goto :goto_5

    :cond_c
    move v3, v4

    .line 384
    :goto_7
    if-ge v3, v7, :cond_8

    .line 385
    aget-object v6, v8, v3

    iget v6, v6, Landroid/graphics/PointF;->x:F

    float-to-int v6, v6

    add-int/2addr v6, p2

    invoke-static {v5, v1, v6}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v1

    .line 386
    aget-object v6, v8, v3

    iget v6, v6, Landroid/graphics/PointF;->y:F

    float-to-int v6, v6

    add-int/2addr v6, p3

    invoke-static {v5, v1, v6}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v6

    .line 384
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v6

    goto :goto_7

    .line 399
    :cond_d
    if-eqz v8, :cond_e

    if-lez v6, :cond_e

    if-ge v4, v6, :cond_e

    .line 400
    aget v1, v8, v4

    const/high16 v9, 0x45800000    # 4096.0f

    mul-float/2addr v1, v9

    float-to-int v1, v1

    .line 404
    :goto_8
    invoke-static {v5, v3, v1}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v3

    .line 397
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 402
    :cond_e
    const/16 v1, 0x1000

    goto :goto_8

    :cond_f
    move v3, v4

    move-object v1, v2

    goto/16 :goto_2
.end method

.method public g()I
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    iget v1, p0, Lcom/samsung/samm/b/a/n;->f:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    move v1, v0

    .line 64
    :goto_0
    iget-object v0, p0, Lcom/samsung/samm/b/a/n;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/h;

    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->k()I

    move-result v2

    .line 65
    iget-object v0, p0, Lcom/samsung/samm/b/a/n;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/h;

    invoke-virtual {v0}, Lcom/samsung/samm/a/h;->m()I

    move-result v0

    .line 66
    mul-int/lit8 v2, v2, 0x2

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x6

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    return v0

    :cond_0
    move v1, v0

    goto :goto_0
.end method
