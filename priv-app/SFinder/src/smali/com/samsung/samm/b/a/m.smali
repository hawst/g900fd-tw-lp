.class public Lcom/samsung/samm/b/a/m;
.super Lcom/samsung/samm/b/a/j;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 21
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/m;->a(Lcom/samsung/samm/b/a/j;)V

    .line 22
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/samm/b/a/m;->a:I

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/samsung/samm/b/a/j;Lcom/samsung/samm/a/d;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/samm/b/a/j;-><init>()V

    .line 27
    invoke-virtual {p0, p1}, Lcom/samsung/samm/b/a/m;->a(Lcom/samsung/samm/b/a/j;)V

    .line 28
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/samm/b/a/m;->a:I

    .line 29
    iput-object p2, p0, Lcom/samsung/samm/b/a/m;->d:Lcom/samsung/samm/a/d;

    .line 30
    return-void
.end method


# virtual methods
.method public a([BI)I
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 84
    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    move v0, v1

    .line 116
    :goto_0
    return v0

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/m;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/samsung/samm/b/a/m;->b([BI)I

    move-result v0

    .line 89
    if-gez v0, :cond_3

    move v0, v1

    .line 90
    goto :goto_0

    .line 91
    :cond_3
    add-int v2, p2, v0

    .line 92
    const/4 v0, 0x1

    new-array v3, v0, [I

    .line 94
    iget-object v0, p0, Lcom/samsung/samm/b/a/m;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/g;

    .line 95
    invoke-virtual {v0}, Lcom/samsung/samm/a/g;->a()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 97
    const-string v4, "SAMM___LIBRARY___CONTENT___ID___KEY"

    iget v5, p0, Lcom/samsung/samm/b/a/m;->b:I

    invoke-virtual {v0, v4, v5}, Lcom/samsung/samm/a/g;->c(Ljava/lang/String;I)Z

    .line 101
    :cond_4
    invoke-static {p1, v2, v3}, Lcom/samsung/samm/b/a/s;->a([BI[I)I

    move-result v0

    .line 102
    const/4 v2, 0x0

    aget v2, v3, v2

    .line 103
    if-nez v2, :cond_5

    .line 104
    sub-int/2addr v0, p2

    goto :goto_0

    .line 105
    :cond_5
    if-gez v2, :cond_6

    .line 106
    const-string v0, "SAMMLibraryCore"

    const-string v2, "SAMM Object Data is Invalid"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 107
    goto :goto_0

    .line 110
    :cond_6
    add-int v3, v0, v2

    .line 112
    sub-int/2addr v0, v0

    .line 113
    if-eq v0, v2, :cond_7

    move v0, v1

    .line 114
    goto :goto_0

    .line 116
    :cond_7
    sub-int v0, v3, p2

    goto :goto_0
.end method

.method protected a()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/samsung/samm/b/a/j;->a()V

    .line 56
    return-void
.end method

.method public a(Landroid/graphics/RectF;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 41
    if-nez p1, :cond_1

    .line 42
    const-string v1, "SAMMLibraryCore"

    const-string v2, "Image rect is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 46
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/samm/b/a/m;->a(Landroid/graphics/RectF;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/samm/b/a/m;->b(I)V

    .line 49
    invoke-virtual {p0, p2}, Lcom/samsung/samm/b/a/m;->c(I)V

    .line 51
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a([III)[B
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 120
    iget-object v0, p0, Lcom/samsung/samm/b/a/m;->d:Lcom/samsung/samm/a/d;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 150
    :cond_0
    :goto_0
    return-object v0

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/b/a/m;->d:Lcom/samsung/samm/a/d;

    check-cast v0, Lcom/samsung/samm/a/g;

    .line 123
    invoke-virtual {v0}, Lcom/samsung/samm/a/g;->a()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 124
    const-string v2, "SAMM___LIBRARY___CONTENT___ID___KEY"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/samsung/samm/a/g;->d(Ljava/lang/String;I)I

    move-result v2

    .line 125
    if-gez v2, :cond_2

    .line 126
    const-string v0, "SAMMLibraryCore"

    const-string v2, "Image ID < 0"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 127
    goto :goto_0

    .line 129
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/samm/a/g;->d()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/samsung/samm/b/a/m;->a(Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 130
    goto :goto_0

    .line 134
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/m;->a()V

    .line 136
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/m;->e()I

    move-result v0

    new-array v0, v0, [B

    .line 139
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, p2, p3}, Lcom/samsung/samm/b/a/m;->a([BIII)I

    move-result v2

    .line 140
    if-gez v2, :cond_4

    move-object v0, v1

    .line 141
    goto :goto_0

    .line 144
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/m;->g()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    .line 145
    invoke-static {v0, v2, v3}, Lcom/samsung/samm/b/a/s;->a([BII)I

    move-result v2

    .line 147
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/m;->e()I

    move-result v3

    if-eq v2, v3, :cond_0

    move-object v0, v1

    .line 148
    goto :goto_0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 67
    .line 71
    const/4 v0, 0x4

    return v0
.end method
