.class public Lcom/samsung/samm/b/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v0, p0, Lcom/samsung/samm/b/a/c;->a:I

    .line 23
    iput v0, p0, Lcom/samsung/samm/b/a/c;->b:I

    .line 24
    iput v0, p0, Lcom/samsung/samm/b/a/c;->c:I

    .line 25
    iput v0, p0, Lcom/samsung/samm/b/a/c;->d:I

    .line 26
    iput v0, p0, Lcom/samsung/samm/b/a/c;->e:I

    .line 28
    iput v0, p0, Lcom/samsung/samm/b/a/c;->f:I

    .line 30
    iput-object v1, p0, Lcom/samsung/samm/b/a/c;->g:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/samsung/samm/b/a/c;->h:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lcom/samsung/samm/b/a/c;->i:Ljava/lang/String;

    .line 37
    iput-boolean v2, p0, Lcom/samsung/samm/b/a/c;->k:Z

    .line 42
    iput-boolean v2, p0, Lcom/samsung/samm/b/a/c;->l:Z

    .line 47
    iput-object p1, p0, Lcom/samsung/samm/b/a/c;->j:Ljava/lang/String;

    .line 48
    iput-boolean p2, p0, Lcom/samsung/samm/b/a/c;->k:Z

    .line 49
    invoke-direct {p0}, Lcom/samsung/samm/b/a/c;->j()V

    .line 50
    return-void
.end method

.method private f(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/samsung/samm/b/a/c;->h:Ljava/lang/String;

    .line 249
    :goto_0
    return-object v0

    .line 246
    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/samsung/samm/b/a/c;->i:Ljava/lang/String;

    goto :goto_0

    .line 249
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 53
    iput v0, p0, Lcom/samsung/samm/b/a/c;->a:I

    .line 54
    iput v0, p0, Lcom/samsung/samm/b/a/c;->f:I

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/c;->j:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/c;->h:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/samm/b/a/c;->j:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/b/a/c;->i:Ljava/lang/String;

    .line 59
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/samsung/samm/b/a/c;->a:I

    return v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    iput v1, p0, Lcom/samsung/samm/b/a/c;->a:I

    .line 80
    shr-int/lit8 v0, p1, 0x18

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/samm/b/a/c;->b:I

    .line 81
    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/samm/b/a/c;->c:I

    .line 82
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/samm/b/a/c;->d:I

    .line 83
    and-int/lit16 v0, p1, 0xff

    iput v0, p0, Lcom/samsung/samm/b/a/c;->e:I

    .line 85
    iput v1, p0, Lcom/samsung/samm/b/a/c;->f:I

    .line 86
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;II)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x4

    const/4 v0, 0x0

    .line 257
    if-nez p1, :cond_0

    .line 258
    const-string v1, "SAMMLibraryCore"

    const-string v2, "bmBGBitmap is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :goto_0
    return v0

    .line 262
    :cond_0
    if-lez p2, :cond_1

    if-gtz p3, :cond_2

    .line 263
    :cond_1
    const-string v1, "SAMMLibraryCore"

    const-string v2, "setBGImageBitmap : Invalid nCanvasWidth or nCanvasHeight"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-gt v2, p2, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v2, p3, :cond_4

    .line 275
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    int-to-float v3, p2

    div-float/2addr v2, v3

    .line 276
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, p3

    div-float/2addr v3, v4

    .line 280
    cmpl-float v4, v2, v3

    if-ltz v4, :cond_6

    .line 281
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v2, v3, v2

    float-to-int p3, v2

    .line 286
    :goto_1
    invoke-static {p1, p2, p3, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 295
    :cond_4
    iget-boolean v2, p0, Lcom/samsung/samm/b/a/c;->k:Z

    if-eqz v2, :cond_7

    .line 296
    invoke-direct {p0, v7}, Lcom/samsung/samm/b/a/c;->f(I)Ljava/lang/String;

    move-result-object v1

    .line 297
    invoke-static {v1, p1}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    move-result v1

    .line 300
    if-eqz v1, :cond_5

    .line 301
    iput v7, p0, Lcom/samsung/samm/b/a/c;->a:I

    .line 303
    iput v0, p0, Lcom/samsung/samm/b/a/c;->b:I

    .line 304
    iput v0, p0, Lcom/samsung/samm/b/a/c;->c:I

    .line 305
    iput v0, p0, Lcom/samsung/samm/b/a/c;->d:I

    .line 306
    iput v0, p0, Lcom/samsung/samm/b/a/c;->e:I

    .line 307
    iput v0, p0, Lcom/samsung/samm/b/a/c;->f:I

    :cond_5
    move v0, v1

    .line 309
    goto :goto_0

    .line 283
    :cond_6
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    float-to-int p2, v2

    goto :goto_1

    .line 311
    :cond_7
    invoke-direct {p0, v7}, Lcom/samsung/samm/b/a/c;->f(I)Ljava/lang/String;

    move-result-object v2

    .line 312
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x4

    .line 313
    new-array v5, v3, [B

    .line 315
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 316
    invoke-virtual {p1, v3}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 319
    const/4 v4, 0x0

    .line 322
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v6, "rw"

    invoke-direct {v3, v2, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    :try_start_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 324
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 325
    invoke-virtual {v3, v5}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 336
    if-eqz v3, :cond_8

    .line 337
    :try_start_2
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 344
    :cond_8
    :goto_2
    iput v7, p0, Lcom/samsung/samm/b/a/c;->a:I

    .line 346
    iput v0, p0, Lcom/samsung/samm/b/a/c;->b:I

    .line 347
    iput v0, p0, Lcom/samsung/samm/b/a/c;->c:I

    .line 348
    iput v0, p0, Lcom/samsung/samm/b/a/c;->d:I

    .line 349
    iput v0, p0, Lcom/samsung/samm/b/a/c;->e:I

    .line 350
    iput v0, p0, Lcom/samsung/samm/b/a/c;->f:I

    move v0, v1

    .line 352
    goto/16 :goto_0

    .line 327
    :catch_0
    move-exception v2

    move-object v3, v4

    .line 329
    :goto_3
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 336
    if-eqz v3, :cond_8

    .line 337
    :try_start_4
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 338
    :catch_1
    move-exception v2

    .line 339
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AnimationBGImage IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 330
    :catch_2
    move-exception v2

    move-object v3, v4

    .line 332
    :goto_4
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 336
    if-eqz v3, :cond_8

    .line 337
    :try_start_6
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    .line 338
    :catch_3
    move-exception v2

    .line 339
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AnimationBGImage IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 334
    :catchall_0
    move-exception v0

    move-object v3, v4

    .line 336
    :goto_5
    if-eqz v3, :cond_9

    .line 337
    :try_start_7
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 342
    :cond_9
    :goto_6
    throw v0

    .line 338
    :catch_4
    move-exception v1

    .line 339
    const-string v2, "SAMMLibraryCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AnimationBGImage IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 338
    :catch_5
    move-exception v2

    .line 339
    const-string v3, "SAMMLibraryCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AnimationBGImage IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 334
    :catchall_1
    move-exception v0

    goto :goto_5

    .line 330
    :catch_6
    move-exception v2

    goto :goto_4

    .line 327
    :catch_7
    move-exception v2

    goto/16 :goto_3
.end method

.method public a(Ljava/io/RandomAccessFile;II)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 517
    iget-boolean v1, p0, Lcom/samsung/samm/b/a/c;->k:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    if-eq p2, v1, :cond_0

    .line 518
    const/4 v1, 0x4

    if-ne p2, v1, :cond_2

    .line 521
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, p3}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 528
    :cond_1
    :goto_0
    return v0

    .line 524
    :cond_2
    if-gtz p3, :cond_1

    .line 528
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/io/RandomAccessFile;III)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 493
    iput p2, p0, Lcom/samsung/samm/b/a/c;->a:I

    .line 494
    iput p3, p0, Lcom/samsung/samm/b/a/c;->f:I

    .line 496
    iget v1, p0, Lcom/samsung/samm/b/a/c;->a:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 497
    iget v1, p0, Lcom/samsung/samm/b/a/c;->a:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 500
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, p4}, Lcom/samsung/samm/b/a/s;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 512
    :goto_0
    return v0

    .line 504
    :cond_1
    :try_start_0
    invoke-virtual {p1, p4}, Ljava/io/RandomAccessFile;->skipBytes(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 505
    :catch_0
    move-exception v1

    .line 507
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 107
    iget v0, p0, Lcom/samsung/samm/b/a/c;->b:I

    shl-int/lit8 v0, v0, 0x18

    iget v1, p0, Lcom/samsung/samm/b/a/c;->c:I

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/samm/b/a/c;->d:I

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/samm/b/a/c;->e:I

    or-int/2addr v0, v1

    return v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 89
    iput p1, p0, Lcom/samsung/samm/b/a/c;->b:I

    .line 90
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/samsung/samm/b/a/c;->b:I

    return v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 93
    iput p1, p0, Lcom/samsung/samm/b/a/c;->c:I

    .line 94
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/samsung/samm/b/a/c;->c:I

    return v0
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 97
    iput p1, p0, Lcom/samsung/samm/b/a/c;->d:I

    .line 98
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/samm/b/a/c;->d:I

    return v0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 101
    iput p1, p0, Lcom/samsung/samm/b/a/c;->e:I

    .line 102
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/samsung/samm/b/a/c;->e:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/samsung/samm/b/a/c;->f:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 235
    iget v0, p0, Lcom/samsung/samm/b/a/c;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 236
    iget-object v0, p0, Lcom/samsung/samm/b/a/c;->h:Ljava/lang/String;

    .line 240
    :goto_0
    return-object v0

    .line 237
    :cond_0
    iget v0, p0, Lcom/samsung/samm/b/a/c;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 238
    iget-object v0, p0, Lcom/samsung/samm/b/a/c;->i:Ljava/lang/String;

    goto :goto_0

    .line 240
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 475
    iget-boolean v1, p0, Lcom/samsung/samm/b/a/c;->k:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/samsung/samm/b/a/c;->a:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 476
    iget v1, p0, Lcom/samsung/samm/b/a/c;->a:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 477
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/samm/b/a/c;->h()Ljava/lang/String;

    move-result-object v1

    .line 478
    if-nez v1, :cond_2

    .line 488
    :cond_1
    :goto_0
    return v0

    .line 480
    :cond_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 482
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 483
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method
