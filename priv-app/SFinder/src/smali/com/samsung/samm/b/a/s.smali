.class public Lcom/samsung/samm/b/a/s;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([BII)I
    .locals 3

    .prologue
    .line 764
    .line 765
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 767
    return p1

    .line 766
    :cond_0
    add-int/lit8 v1, p1, 0x1

    mul-int/lit8 v2, v0, 0x8

    shr-int v2, p2, v2

    int-to-byte v2, v2

    aput-byte v2, p0, p1

    .line 765
    add-int/lit8 v0, v0, 0x1

    move p1, v1

    goto :goto_0
.end method

.method public static a([BIJ)I
    .locals 4

    .prologue
    .line 745
    .line 746
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 748
    return p1

    .line 747
    :cond_0
    add-int/lit8 v1, p1, 0x1

    mul-int/lit8 v2, v0, 0x8

    shr-long v2, p2, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, p0, p1

    .line 746
    add-int/lit8 v0, v0, 0x1

    move p1, v1

    goto :goto_0
.end method

.method public static a([BILjava/lang/String;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 932
    array-length v4, p0

    .line 936
    if-eqz p2, :cond_4

    .line 937
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    move v3, v0

    .line 939
    :goto_0
    if-ge p1, v4, :cond_1

    .line 940
    invoke-static {p0, p1, v3}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v0

    .line 946
    if-lez v3, :cond_0

    .line 947
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    .line 948
    :goto_1
    if-lt v1, v3, :cond_2

    .line 958
    :cond_0
    :goto_2
    return v0

    .line 942
    :cond_1
    const-string v0, "SAMMLibraryCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Buffer index is out of bound!!! : length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", index="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, p1

    .line 943
    goto :goto_2

    .line 949
    :cond_2
    if-ge v0, v4, :cond_3

    .line 950
    aget-char v2, v5, v1

    invoke-static {p0, v0, v2}, Lcom/samsung/samm/b/a/s;->b([BII)I

    move-result v2

    .line 948
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    .line 952
    :cond_3
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Buffer index is out of bound!!! : length="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    move v3, v1

    goto :goto_0
.end method

.method public static a([BI[B)I
    .locals 4

    .prologue
    .line 726
    .line 727
    const/4 v0, 0x0

    move v1, p1

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    array-length v2, p0

    if-lt p1, v2, :cond_1

    .line 729
    :cond_0
    return v1

    .line 728
    :cond_1
    add-int/lit8 v2, v1, 0x1

    aget-byte v3, p2, v0

    aput-byte v3, p0, v1

    .line 727
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0
.end method

.method public static a([BI[I)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 847
    array-length v3, p0

    .line 849
    aput v2, p2, v2

    move v1, v2

    move v0, p1

    .line 850
    :goto_0
    const/4 v4, 0x4

    if-lt v1, v4, :cond_0

    .line 858
    :goto_1
    return v0

    .line 851
    :cond_0
    if-ge v0, v3, :cond_1

    .line 852
    aget v4, p2, v2

    add-int/lit8 p1, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    mul-int/lit8 v5, v1, 0x8

    shl-int/2addr v0, v5

    add-int/2addr v0, v4

    aput v0, p2, v2

    .line 850
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, p1

    goto :goto_0

    .line 854
    :cond_1
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Buffer index is out of bound!!! : length="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static a([BI[J)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 820
    array-length v3, p0

    .line 822
    const-wide/16 v0, 0x0

    aput-wide v0, p2, v2

    move v1, v2

    move v0, p1

    .line 823
    :goto_0
    const/16 v4, 0x8

    if-lt v1, v4, :cond_0

    .line 831
    :goto_1
    return v0

    .line 824
    :cond_0
    if-ge v0, v3, :cond_1

    .line 825
    aget-wide v4, p2, v2

    add-int/lit8 p1, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    int-to-long v6, v0

    mul-int/lit8 v0, v1, 0x8

    shl-long/2addr v6, v0

    add-long/2addr v4, v6

    aput-wide v4, p2, v2

    .line 823
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, p1

    goto :goto_0

    .line 827
    :cond_1
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Buffer index is out of bound!!! : length="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static a([BI[Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 900
    array-length v3, p0

    .line 902
    const/4 v0, 0x0

    aput-object v0, p2, v1

    .line 904
    const/4 v0, 0x1

    new-array v4, v0, [I

    .line 905
    if-ge p1, v3, :cond_0

    .line 906
    invoke-static {p0, p1, v4}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v0

    .line 912
    aget v2, v4, v1

    .line 913
    if-lez v2, :cond_3

    .line 915
    new-array v5, v2, [C

    move v2, v0

    move v0, v1

    .line 916
    :goto_0
    array-length v6, v5

    if-lt v0, v6, :cond_1

    .line 925
    invoke-static {v5}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p2, v1

    .line 928
    :goto_1
    return v2

    .line 908
    :cond_0
    const-string v0, "SAMMLibraryCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Buffer index is out of bound!!! : length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", index="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, p1

    .line 909
    goto :goto_1

    .line 917
    :cond_1
    if-ge v2, v3, :cond_2

    .line 918
    invoke-static {p0, v2, v4}, Lcom/samsung/samm/b/a/s;->b([BI[I)I

    move-result v2

    .line 919
    aget v6, v4, v1

    int-to-char v6, v6

    aput-char v6, v5, v0

    .line 916
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 921
    :cond_2
    const-string v0, "SAMMLibraryCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Buffer index is out of bound!!! : length="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", index="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public static a([BI[[B)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 801
    move v0, v1

    .line 802
    :goto_0
    aget-object v2, p2, v1

    array-length v2, v2

    if-ge v0, v2, :cond_0

    array-length v2, p0

    if-lt p1, v2, :cond_1

    .line 804
    :cond_0
    return p1

    .line 803
    :cond_1
    aget-object v3, p2, v1

    add-int/lit8 v2, p1, 0x1

    aget-byte v4, p0, p1

    aput-byte v4, v3, v0

    .line 802
    add-int/lit8 v0, v0, 0x1

    move p1, v2

    goto :goto_0
.end method

.method public static a(Ljava/io/RandomAccessFile;)J
    .locals 7

    .prologue
    .line 511
    const-wide/16 v2, 0x0

    .line 513
    const/4 v0, 0x0

    move v6, v0

    move-wide v0, v2

    move v2, v6

    :goto_0
    const/16 v3, 0x8

    if-lt v2, v3, :cond_0

    .line 524
    :goto_1
    return-wide v0

    .line 514
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->readUnsignedByte()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-long v4, v3

    .line 515
    mul-int/lit8 v3, v2, 0x8

    shl-long/2addr v4, v3

    add-long/2addr v4, v0

    .line 513
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-wide v0, v4

    goto :goto_0

    .line 517
    :catch_0
    move-exception v0

    .line 519
    const-string v1, "Tool_FileManage_Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "readIntData IOException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 521
    const-wide/16 v0, -0x1

    goto :goto_1
.end method

.method public static a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 709
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 710
    return-object v0
.end method

.method public static a(Ljava/io/RandomAccessFile;I)V
    .locals 4

    .prologue
    .line 612
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 621
    :goto_1
    return-void

    .line 613
    :cond_0
    mul-int/lit8 v1, v0, 0x8

    shr-int v1, p1, v1

    int-to-byte v1, v1

    :try_start_0
    invoke-virtual {p0, v1}, Ljava/io/RandomAccessFile;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 612
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 615
    :catch_0
    move-exception v0

    .line 617
    const-string v1, "Tool_FileManage_Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "writeIntData IOException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static a(Ljava/io/RandomAccessFile;J)V
    .locals 5

    .prologue
    .line 590
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 599
    :goto_1
    return-void

    .line 591
    :cond_0
    mul-int/lit8 v1, v0, 0x8

    shr-long v2, p1, v1

    long-to-int v1, v2

    int-to-byte v1, v1

    :try_start_0
    invoke-virtual {p0, v1}, Ljava/io/RandomAccessFile;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 590
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 593
    :catch_0
    move-exception v0

    .line 595
    const-string v1, "Tool_FileManage_Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "writeIntData IOException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 89
    if-nez p0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 93
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    const-string v0, "Tool_FileManage_Utils"

    const-string v1, "Delete File Error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    .locals 2

    .prologue
    .line 281
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;ILandroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/io/RandomAccessFile;Ljava/lang/String;ILandroid/content/Context;Z)Z
    .locals 1

    .prologue
    .line 298
    invoke-static {p0, p1, p2, p3, p4}, Lcom/samsung/samm/b/a/s;->b(Ljava/io/RandomAccessFile;Ljava/lang/String;ILandroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/io/RandomAccessFile;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 538
    move v1, v0

    .line 540
    :goto_0
    const/4 v2, 0x4

    if-lt v1, v2, :cond_0

    .line 551
    :goto_1
    return v0

    .line 541
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->readUnsignedByte()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 542
    mul-int/lit8 v3, v1, 0x8

    shl-int/2addr v2, v3

    add-int/2addr v2, v0

    .line 540
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 544
    :catch_0
    move-exception v0

    .line 546
    const-string v1, "Tool_FileManage_Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "readIntData IOException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 548
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 656
    if-nez p0, :cond_1

    .line 690
    :cond_0
    :goto_0
    return v0

    .line 658
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 659
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 661
    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 665
    const/4 v3, 0x0

    .line 668
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v4, "r"

    invoke-direct {v2, v1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    long-to-int v0, v0

    .line 682
    if-eqz v2, :cond_0

    .line 683
    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 684
    :catch_0
    move-exception v1

    .line 685
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getFileSize IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 670
    :catch_1
    move-exception v1

    move-object v2, v3

    .line 672
    :goto_1
    :try_start_3
    const-string v3, "Tool_FileManage_Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getFileSize FileNotFoundException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 682
    if-eqz v2, :cond_0

    .line 683
    :try_start_4
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 684
    :catch_2
    move-exception v1

    .line 685
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getFileSize IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 675
    :catch_3
    move-exception v1

    .line 677
    :goto_2
    :try_start_5
    const-string v2, "Tool_FileManage_Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getFileSize IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 682
    if-eqz v3, :cond_0

    .line 683
    :try_start_6
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_0

    .line 684
    :catch_4
    move-exception v1

    .line 685
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getFileSize IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 680
    :catchall_0
    move-exception v0

    .line 682
    :goto_3
    if-eqz v3, :cond_2

    .line 683
    :try_start_7
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 688
    :cond_2
    :goto_4
    throw v0

    .line 684
    :catch_5
    move-exception v1

    .line 685
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getFileSize IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 680
    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_3

    .line 675
    :catch_6
    move-exception v1

    move-object v3, v2

    goto :goto_2

    .line 670
    :catch_7
    move-exception v1

    goto/16 :goto_1
.end method

.method public static b([BII)I
    .locals 3

    .prologue
    .line 782
    .line 783
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 785
    return p1

    .line 784
    :cond_0
    add-int/lit8 v1, p1, 0x1

    mul-int/lit8 v2, v0, 0x8

    shr-int v2, p2, v2

    int-to-byte v2, v2

    aput-byte v2, p0, p1

    .line 783
    add-int/lit8 v0, v0, 0x1

    move p1, v1

    goto :goto_0
.end method

.method public static b([BI[I)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 874
    array-length v3, p0

    .line 876
    aput v2, p2, v2

    move v1, v2

    move v0, p1

    .line 877
    :goto_0
    const/4 v4, 0x2

    if-lt v1, v4, :cond_0

    .line 885
    :goto_1
    return v0

    .line 878
    :cond_0
    if-ge v0, v3, :cond_1

    .line 879
    aget v4, p2, v2

    add-int/lit8 p1, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    mul-int/lit8 v5, v1, 0x8

    shl-int/2addr v0, v5

    add-int/2addr v0, v4

    aput v0, p2, v2

    .line 877
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, p1

    goto :goto_0

    .line 881
    :cond_1
    const-string v1, "SAMMLibraryCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Buffer index is out of bound!!! : length="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static b(Ljava/io/RandomAccessFile;I)V
    .locals 4

    .prologue
    .line 635
    and-int/lit16 v0, p1, 0xff

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 636
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Ljava/io/RandomAccessFile;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 643
    :goto_0
    return-void

    .line 637
    :catch_0
    move-exception v0

    .line 639
    const-string v1, "Tool_FileManage_Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "writeShortData IOException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static b(Ljava/io/RandomAccessFile;Ljava/lang/String;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 425
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 497
    :cond_0
    :goto_0
    return v0

    .line 429
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 430
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 432
    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 435
    const/4 v3, 0x0

    .line 437
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v2, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    const/high16 v1, 0x100000

    :try_start_1
    new-array v3, v1, [B

    move v1, v0

    .line 447
    :goto_1
    invoke-virtual {v2, v3}, Ljava/io/RandomAccessFile;->read([B)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    if-gtz v4, :cond_2

    .line 473
    if-eq v1, p2, :cond_4

    .line 489
    if-eqz v2, :cond_0

    .line 490
    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 491
    :catch_0
    move-exception v1

    .line 492
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "writeToDataFromFile IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 448
    :cond_2
    add-int/2addr v1, v4

    .line 449
    const/4 v5, 0x0

    :try_start_3
    invoke-virtual {p0, v3, v5, v4}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 479
    :catch_1
    move-exception v1

    .line 480
    :goto_2
    :try_start_4
    const-string v3, "Tool_FileManage_Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "writeToDataFromFile FileNotFoundException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 489
    if-eqz v2, :cond_0

    .line 490
    :try_start_5
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 491
    :catch_2
    move-exception v1

    .line 492
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "writeToDataFromFile IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 483
    :catch_3
    move-exception v1

    move-object v2, v3

    .line 484
    :goto_3
    :try_start_6
    const-string v3, "Tool_FileManage_Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "writeToDataFromFile IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 489
    if-eqz v2, :cond_0

    .line 490
    :try_start_7
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_0

    .line 491
    :catch_4
    move-exception v1

    .line 492
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "writeToDataFromFile IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 487
    :catchall_0
    move-exception v0

    move-object v2, v3

    .line 489
    :goto_4
    if-eqz v2, :cond_3

    .line 490
    :try_start_8
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 495
    :cond_3
    :goto_5
    throw v0

    .line 491
    :catch_5
    move-exception v1

    .line 492
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "writeToDataFromFile IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 489
    :cond_4
    if-eqz v2, :cond_5

    .line 490
    :try_start_9
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 497
    :cond_5
    :goto_6
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 491
    :catch_6
    move-exception v0

    .line 492
    const-string v1, "Tool_FileManage_Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "writeToDataFromFile IOException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 487
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 483
    :catch_7
    move-exception v1

    goto :goto_3

    .line 479
    :catch_8
    move-exception v1

    move-object v2, v3

    goto/16 :goto_2
.end method

.method private static b(Ljava/io/RandomAccessFile;Ljava/lang/String;ILandroid/content/Context;Z)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 303
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 411
    :cond_0
    :goto_0
    return v0

    .line 306
    :cond_1
    if-eqz p4, :cond_7

    .line 307
    if-eqz p3, :cond_0

    .line 310
    const-string v3, "/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 311
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 314
    invoke-virtual {p3, v3}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 318
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {p3, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    .line 321
    const/high16 v3, 0x100000

    .line 323
    div-int v4, p2, v3

    .line 324
    rem-int v5, p2, v3

    .line 328
    if-lez v4, :cond_2

    .line 330
    new-array v6, v3, [B

    move v3, v0

    .line 331
    :goto_1
    if-lt v3, v4, :cond_5

    .line 337
    :cond_2
    if-lez v5, :cond_3

    .line 340
    new-array v3, v5, [B

    .line 341
    invoke-virtual {p0, v3}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v4

    .line 342
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    :cond_3
    if-eqz v2, :cond_4

    .line 355
    :try_start_1
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    :cond_4
    :goto_2
    move v0, v1

    .line 411
    goto :goto_0

    .line 333
    :cond_5
    :try_start_2
    invoke-virtual {p0, v6}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v7

    .line 334
    const/4 v8, 0x0

    invoke-virtual {v2, v6, v8, v7}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 331
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 344
    :catch_0
    move-exception v1

    .line 345
    :try_start_3
    const-string v3, "Tool_FileManage_Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "readToFileFromData FileNotFoundException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 354
    if-eqz v2, :cond_0

    .line 355
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 356
    :catch_1
    move-exception v1

    .line 357
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "readToFileFromData IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 348
    :catch_2
    move-exception v1

    .line 349
    :try_start_5
    const-string v3, "Tool_FileManage_Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "readToFileFromData IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 354
    if-eqz v2, :cond_0

    .line 355
    :try_start_6
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 356
    :catch_3
    move-exception v1

    .line 357
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "readToFileFromData IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 352
    :catchall_0
    move-exception v0

    .line 354
    if-eqz v2, :cond_6

    .line 355
    :try_start_7
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 360
    :cond_6
    :goto_3
    throw v0

    .line 356
    :catch_4
    move-exception v1

    .line 357
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "readToFileFromData IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 356
    :catch_5
    move-exception v0

    .line 357
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "readToFileFromData IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 362
    :cond_7
    invoke-static {p1}, Lcom/samsung/samm/b/a/s;->a(Ljava/lang/String;)V

    .line 366
    :try_start_8
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "rw"

    invoke-direct {v3, p1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 369
    const/high16 v2, 0x100000

    .line 371
    :try_start_9
    div-int v4, p2, v2

    .line 372
    rem-int v5, p2, v2

    .line 376
    if-lez v4, :cond_8

    .line 378
    new-array v6, v2, [B

    move v2, v0

    .line 379
    :goto_4
    if-lt v2, v4, :cond_a

    .line 385
    :cond_8
    if-lez v5, :cond_9

    .line 388
    new-array v2, v5, [B

    .line 389
    invoke-virtual {p0, v2}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v4

    .line 390
    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5, v4}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_d
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_c
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 402
    :cond_9
    if-eqz v3, :cond_4

    .line 403
    :try_start_a
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_2

    .line 404
    :catch_6
    move-exception v0

    .line 405
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "readToFileFromData IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 381
    :cond_a
    :try_start_b
    invoke-virtual {p0, v6}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v7

    .line 382
    const/4 v8, 0x0

    invoke-virtual {v3, v6, v8, v7}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_d
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_c
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 379
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 392
    :catch_7
    move-exception v1

    .line 393
    :goto_5
    :try_start_c
    const-string v3, "Tool_FileManage_Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "readToFileFromData FileNotFoundException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 402
    if-eqz v2, :cond_0

    .line 403
    :try_start_d
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    goto/16 :goto_0

    .line 404
    :catch_8
    move-exception v1

    .line 405
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "readToFileFromData IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 396
    :catch_9
    move-exception v1

    move-object v3, v2

    .line 397
    :goto_6
    :try_start_e
    const-string v2, "Tool_FileManage_Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "readToFileFromData IOException : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 402
    if-eqz v3, :cond_0

    .line 403
    :try_start_f
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_a

    goto/16 :goto_0

    .line 404
    :catch_a
    move-exception v1

    .line 405
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "readToFileFromData IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 400
    :catchall_1
    move-exception v0

    move-object v3, v2

    .line 402
    :goto_7
    if-eqz v3, :cond_b

    .line 403
    :try_start_10
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    .line 408
    :cond_b
    :goto_8
    throw v0

    .line 404
    :catch_b
    move-exception v1

    .line 405
    const-string v2, "Tool_FileManage_Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "readToFileFromData IOException : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 400
    :catchall_2
    move-exception v0

    goto :goto_7

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_7

    .line 396
    :catch_c
    move-exception v1

    goto :goto_6

    .line 392
    :catch_d
    move-exception v1

    move-object v2, v3

    goto/16 :goto_5
.end method

.method public static c(Ljava/io/RandomAccessFile;)I
    .locals 4

    .prologue
    .line 567
    :try_start_0
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->readUnsignedByte()I

    move-result v0

    .line 568
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->readUnsignedByte()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 576
    :goto_0
    return v0

    .line 569
    :catch_0
    move-exception v0

    .line 571
    const-string v1, "Tool_FileManage_Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "readIntData IOException : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 573
    const/4 v0, -0x1

    goto :goto_0
.end method
