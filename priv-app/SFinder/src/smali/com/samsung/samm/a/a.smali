.class public Lcom/samsung/samm/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final APP_TYPE_LAUNCHER:I = 0x1

.field public static final APP_TYPE_NORMAL:I


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v1, p0, Lcom/samsung/samm/a/a;->a:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lcom/samsung/samm/a/a;->b:Ljava/lang/String;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/a/a;->c:I

    .line 22
    iput-object v1, p0, Lcom/samsung/samm/a/a;->d:Ljava/lang/String;

    .line 23
    iput-object v1, p0, Lcom/samsung/samm/a/a;->e:Ljava/lang/String;

    .line 24
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/samm/a/a;->f:J

    .line 25
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 82
    const/4 v0, 0x2

    .line 83
    if-eqz p1, :cond_0

    .line 84
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 87
    :cond_0
    return v0
.end method


# virtual methods
.method public getAppClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/samm/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getAppInfoSize()I
    .locals 6

    .prologue
    .line 70
    const/4 v0, 0x4

    .line 72
    iget-object v1, p0, Lcom/samsung/samm/a/a;->b:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/samm/a/a;->a(Ljava/lang/String;)I

    move-result v1

    .line 73
    iget-object v2, p0, Lcom/samsung/samm/a/a;->a:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/samm/a/a;->a(Ljava/lang/String;)I

    move-result v2

    .line 74
    iget-object v3, p0, Lcom/samsung/samm/a/a;->e:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/samsung/samm/a/a;->a(Ljava/lang/String;)I

    move-result v3

    .line 75
    iget-object v4, p0, Lcom/samsung/samm/a/a;->d:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/samsung/samm/a/a;->a(Ljava/lang/String;)I

    move-result v4

    .line 76
    const/16 v5, 0x8

    .line 78
    add-int/2addr v0, v1

    add-int/2addr v0, v2

    add-int/2addr v0, v3

    add-int/2addr v0, v4

    add-int/2addr v0, v5

    return v0
.end method

.method public getAppPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/samm/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getAppSrcPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/samm/a/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getAppTime()J
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/samm/a/a;->f:J

    return-wide v0
.end method

.method public getAppType()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/samsung/samm/a/a;->c:I

    return v0
.end method

.method public getAppURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/samm/a/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public setAppClassName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/samm/a/a;->a:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setAppPackageName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/samm/a/a;->b:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setAppSrcPath(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/samm/a/a;->d:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setAppTime(J)V
    .locals 1

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/samsung/samm/a/a;->f:J

    .line 67
    return-void
.end method

.method public setAppType(I)V
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/samsung/samm/a/a;->c:I

    .line 32
    return-void
.end method

.method public setAppURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/samm/a/a;->e:Ljava/lang/String;

    .line 53
    return-void
.end method
