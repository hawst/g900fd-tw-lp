.class public Lcom/samsung/samm/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:I

.field private s:I

.field private t:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    iput v1, p0, Lcom/samsung/samm/a/k;->a:I

    .line 272
    iput v2, p0, Lcom/samsung/samm/a/k;->b:I

    .line 273
    iput v1, p0, Lcom/samsung/samm/a/k;->c:I

    .line 276
    iput-boolean v1, p0, Lcom/samsung/samm/a/k;->d:Z

    .line 277
    iput-boolean v1, p0, Lcom/samsung/samm/a/k;->e:Z

    .line 278
    iput-boolean v1, p0, Lcom/samsung/samm/a/k;->f:Z

    .line 279
    iput-boolean v1, p0, Lcom/samsung/samm/a/k;->g:Z

    .line 280
    iput-boolean v1, p0, Lcom/samsung/samm/a/k;->h:Z

    .line 283
    iput-boolean v1, p0, Lcom/samsung/samm/a/k;->i:Z

    .line 291
    iput v2, p0, Lcom/samsung/samm/a/k;->j:I

    .line 299
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/samm/a/k;->k:I

    .line 317
    iput-boolean v1, p0, Lcom/samsung/samm/a/k;->l:Z

    .line 325
    iput-boolean v2, p0, Lcom/samsung/samm/a/k;->m:Z

    .line 333
    iput v2, p0, Lcom/samsung/samm/a/k;->n:I

    .line 341
    iput-boolean v1, p0, Lcom/samsung/samm/a/k;->o:Z

    .line 349
    iput-boolean v1, p0, Lcom/samsung/samm/a/k;->p:Z

    .line 357
    iput-boolean v2, p0, Lcom/samsung/samm/a/k;->q:Z

    .line 366
    iput v1, p0, Lcom/samsung/samm/a/k;->r:I

    .line 368
    iput v1, p0, Lcom/samsung/samm/a/k;->s:I

    .line 369
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/k;->t:Ljava/lang/String;

    .line 11
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 409
    iget v0, p0, Lcom/samsung/samm/a/k;->a:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 452
    iget v0, p0, Lcom/samsung/samm/a/k;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 495
    iget v0, p0, Lcom/samsung/samm/a/k;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 541
    iget v0, p0, Lcom/samsung/samm/a/k;->j:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 584
    iget v0, p0, Lcom/samsung/samm/a/k;->k:I

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 616
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->l:Z

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 676
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->o:Z

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 706
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->p:Z

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 751
    iget v0, p0, Lcom/samsung/samm/a/k;->r:I

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 781
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->q:Z

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 833
    iget v0, p0, Lcom/samsung/samm/a/k;->n:I

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 861
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->d:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 889
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->e:Z

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 917
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->f:Z

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 945
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->g:Z

    return v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 973
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->h:Z

    return v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 1001
    iget-boolean v0, p0, Lcom/samsung/samm/a/k;->i:Z

    return v0
.end method
