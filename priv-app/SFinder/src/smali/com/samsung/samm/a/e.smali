.class public Lcom/samsung/samm/a/e;
.super Lcom/samsung/samm/a/d;
.source "SourceFile"


# instance fields
.field private t:Landroid/graphics/PointF;

.field private u:Landroid/graphics/Bitmap;

.field private v:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/samsung/samm/a/d;-><init>()V

    .line 51
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/samm/a/e;->b:I

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/a/e;->a:I

    .line 55
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/a/e;->t:Landroid/graphics/PointF;

    .line 57
    iput-object v1, p0, Lcom/samsung/samm/a/e;->u:Landroid/graphics/Bitmap;

    .line 58
    iput-object v1, p0, Lcom/samsung/samm/a/e;->v:Ljava/lang/String;

    .line 59
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/PointF;)V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/samm/a/e;->t:Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 114
    iget-object v0, p0, Lcom/samsung/samm/a/e;->t:Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 115
    return-void
.end method

.method public d(I)Z
    .locals 3

    .prologue
    .line 76
    if-nez p1, :cond_0

    .line 77
    iput p1, p0, Lcom/samsung/samm/a/e;->a:I

    .line 78
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    .line 81
    :cond_0
    const-string v0, "SAMMLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Undefined filling Style : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 97
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/samsung/samm/a/e;->t:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/samm/a/e;->t:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method
