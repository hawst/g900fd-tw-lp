.class public Lcom/samsung/samm/a/f;
.super Lcom/samsung/samm/a/d;
.source "SourceFile"


# instance fields
.field private t:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private u:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/samm/a/d;-><init>()V

    .line 391
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/f;->t:Ljava/util/LinkedList;

    .line 57
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/a/f;->t:Ljava/util/LinkedList;

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/samm/a/f;->u:I

    .line 59
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/samm/a/d;-><init>()V

    .line 391
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/f;->t:Ljava/util/LinkedList;

    .line 73
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/a/f;->t:Ljava/util/LinkedList;

    .line 74
    iput p1, p0, Lcom/samsung/samm/a/f;->u:I

    .line 75
    return-void
.end method


# virtual methods
.method public d(I)Z
    .locals 3

    .prologue
    .line 443
    if-eqz p1, :cond_0

    .line 444
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 445
    :cond_0
    iput p1, p0, Lcom/samsung/samm/a/f;->a:I

    .line 446
    const/4 v0, 0x1

    .line 450
    :goto_0
    return v0

    .line 449
    :cond_1
    const-string v0, "SAMMLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Undefined Group Style : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/samm/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    iget-object v0, p0, Lcom/samsung/samm/a/f;->t:Ljava/util/LinkedList;

    return-object v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 309
    iget v0, p0, Lcom/samsung/samm/a/f;->u:I

    return v0
.end method
