.class public Lcom/samsung/samm/a/g;
.super Lcom/samsung/samm/a/d;
.source "SourceFile"


# instance fields
.field private t:Landroid/graphics/Bitmap;

.field private u:Ljava/lang/String;

.field private v:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/samsung/samm/a/d;-><init>()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/g;->t:Landroid/graphics/Bitmap;

    .line 106
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/samm/a/g;->a:I

    .line 107
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/samm/a/g;->v:I

    .line 108
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/samsung/samm/a/d;-><init>()V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/g;->t:Landroid/graphics/Bitmap;

    .line 127
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/samm/a/g;->a:I

    .line 128
    iput p1, p0, Lcom/samsung/samm/a/g;->v:I

    .line 129
    return-void
.end method


# virtual methods
.method public d(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 147
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    .line 148
    if-eqz p1, :cond_0

    .line 149
    if-eq p1, v0, :cond_0

    .line 150
    const/16 v1, 0x64

    if-ne p1, v1, :cond_1

    .line 151
    :cond_0
    iput p1, p0, Lcom/samsung/samm/a/g;->a:I

    .line 156
    :goto_0
    return v0

    .line 155
    :cond_1
    const-string v0, "SAMMLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Undefined Image Style : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 334
    iget v1, p0, Lcom/samsung/samm/a/g;->a:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 335
    const-string v1, "SAMMLibrary"

    const-string v2, "Image style is not applicable. Set style as SAMM_IMAGESTYLE_NORMAL."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_0
    :goto_0
    return v0

    .line 339
    :cond_1
    invoke-static {p1}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341
    iput-object p1, p0, Lcom/samsung/samm/a/g;->u:Ljava/lang/String;

    .line 342
    iget-object v0, p0, Lcom/samsung/samm/a/g;->t:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 343
    iget-object v0, p0, Lcom/samsung/samm/a/g;->t:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 345
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/g;->t:Landroid/graphics/Bitmap;

    .line 346
    const/4 v0, 0x1

    goto :goto_0
.end method
