.class public Lcom/samsung/samm/a/j;
.super Lcom/samsung/samm/a/d;
.source "SourceFile"


# instance fields
.field private t:Landroid/graphics/Bitmap;

.field private u:Ljava/lang/String;

.field private v:I

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Lcom/samsung/samm/a/d;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/a/j;->a:I

    .line 76
    iput-object v1, p0, Lcom/samsung/samm/a/j;->w:Ljava/lang/String;

    .line 77
    iput-object v1, p0, Lcom/samsung/samm/a/j;->x:Ljava/lang/String;

    .line 78
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/samm/a/j;->v:I

    .line 79
    return-void
.end method


# virtual methods
.method public d(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 114
    if-eqz p1, :cond_0

    .line 115
    if-eq p1, v0, :cond_0

    .line 116
    const/16 v1, 0x64

    if-ne p1, v1, :cond_1

    .line 117
    :cond_0
    iput p1, p0, Lcom/samsung/samm/a/j;->a:I

    .line 122
    :goto_0
    return v0

    .line 121
    :cond_1
    const-string v0, "SAMMLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Undefined Video Style : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 261
    iget v2, p0, Lcom/samsung/samm/a/j;->a:I

    if-eqz v2, :cond_1

    .line 262
    iget v2, p0, Lcom/samsung/samm/a/j;->a:I

    if-eq v2, v1, :cond_1

    .line 263
    const-string v1, "SAMMLibrary"

    const-string v2, "Video style is not applicable. Set style as SAMM_VIDEOSTYLE_NORMAL or SAMM_VIDEOSTYLE_URL."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_0
    :goto_0
    return v0

    .line 267
    :cond_1
    invoke-static {p1}, Lcom/samsung/samm/b/a/r;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 269
    iput-object p1, p0, Lcom/samsung/samm/a/j;->u:Ljava/lang/String;

    .line 270
    iget-object v0, p0, Lcom/samsung/samm/a/j;->t:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/samsung/samm/a/j;->t:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 273
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/j;->t:Landroid/graphics/Bitmap;

    move v0, v1

    .line 274
    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 401
    iget v0, p0, Lcom/samsung/samm/a/j;->a:I

    if-eqz v0, :cond_0

    .line 402
    const-string v0, "SAMMLibrary"

    const-string v1, "Video Style is not Normal. setStyle as SAMM_VIDEOSTYLE_NORMAL."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const/4 v0, 0x0

    .line 413
    :goto_0
    return v0

    .line 410
    :cond_0
    iput-object p1, p0, Lcom/samsung/samm/a/j;->w:Ljava/lang/String;

    .line 411
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/j;->x:Ljava/lang/String;

    .line 413
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 489
    iget v1, p0, Lcom/samsung/samm/a/j;->a:I

    if-eq v1, v0, :cond_0

    .line 490
    const-string v0, "SAMMLibrary"

    const-string v1, "Video Style is not Normal. setStyle as SAMM_VIDEOSTYLE_URL."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    const/4 v0, 0x0

    .line 497
    :goto_0
    return v0

    .line 494
    :cond_0
    iput-object p1, p0, Lcom/samsung/samm/a/j;->x:Ljava/lang/String;

    .line 495
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/samm/a/j;->w:Ljava/lang/String;

    goto :goto_0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/samm/a/j;->w:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/samsung/samm/a/j;->x:Ljava/lang/String;

    return-object v0
.end method
