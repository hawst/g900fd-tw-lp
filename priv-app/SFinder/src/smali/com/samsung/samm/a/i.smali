.class public Lcom/samsung/samm/a/i;
.super Lcom/samsung/samm/a/d;
.source "SourceFile"


# instance fields
.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:I

.field private w:I

.field private x:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Lcom/samsung/samm/a/d;-><init>()V

    .line 92
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/samm/a/i;->b:I

    .line 93
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/samsung/samm/a/i;->c:F

    .line 95
    iput-object v2, p0, Lcom/samsung/samm/a/i;->t:Ljava/lang/String;

    .line 96
    iput-object v2, p0, Lcom/samsung/samm/a/i;->u:Ljava/lang/String;

    .line 97
    iput v1, p0, Lcom/samsung/samm/a/i;->v:I

    .line 98
    iput v1, p0, Lcom/samsung/samm/a/i;->w:I

    .line 99
    iput v1, p0, Lcom/samsung/samm/a/i;->x:I

    .line 100
    return-void
.end method


# virtual methods
.method public a(II)Z
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 285
    if-eqz p1, :cond_0

    .line 286
    if-eq p1, v0, :cond_0

    .line 287
    if-ne p1, v1, :cond_2

    .line 288
    :cond_0
    if-eqz p2, :cond_1

    .line 289
    if-eq p2, v0, :cond_1

    .line 290
    if-ne p2, v1, :cond_2

    .line 291
    :cond_1
    iput p1, p0, Lcom/samsung/samm/a/i;->w:I

    iput p2, p0, Lcom/samsung/samm/a/i;->x:I

    .line 295
    :goto_0
    return v0

    .line 294
    :cond_2
    const-string v0, "SAMMLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Undefined Text Align Option : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/samsung/samm/a/i;->t:Ljava/lang/String;

    return-void
.end method

.method public d(I)Z
    .locals 3

    .prologue
    .line 126
    if-eqz p1, :cond_0

    .line 127
    and-int/lit8 v0, p1, -0x8

    if-nez v0, :cond_1

    .line 128
    :cond_0
    iput p1, p0, Lcom/samsung/samm/a/i;->a:I

    .line 129
    const/4 v0, 0x1

    .line 132
    :goto_0
    return v0

    .line 131
    :cond_1
    const-string v0, "SAMMLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Undefined Text Style : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 228
    iput p1, p0, Lcom/samsung/samm/a/i;->v:I

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/samsung/samm/a/i;->u:Ljava/lang/String;

    return-void
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/samm/a/i;->t:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/samm/a/i;->u:Ljava/lang/String;

    return-object v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/samsung/samm/a/i;->v:I

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/samsung/samm/a/i;->w:I

    return v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/samsung/samm/a/i;->x:I

    return v0
.end method
