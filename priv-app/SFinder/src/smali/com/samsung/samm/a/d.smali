.class public abstract Lcom/samsung/samm/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:I

.field protected b:I

.field protected c:F

.field protected d:Landroid/graphics/RectF;

.field protected e:I

.field protected f:I

.field protected g:I

.field protected h:I

.field protected i:I

.field protected j:I

.field protected k:I

.field protected l:F

.field protected m:Ljava/lang/String;

.field protected n:Ljava/lang/String;

.field protected o:I

.field protected p:I

.field protected q:Z

.field protected r:Lcom/samsung/samm/b/a/e;

.field protected s:Lcom/samsung/samm/b/a/e;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 507
    iput-object v0, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    .line 508
    iput-object v0, p0, Lcom/samsung/samm/a/d;->s:Lcom/samsung/samm/b/a/e;

    .line 110
    iput v1, p0, Lcom/samsung/samm/a/d;->a:I

    .line 111
    iput v1, p0, Lcom/samsung/samm/a/d;->b:I

    .line 112
    iput v2, p0, Lcom/samsung/samm/a/d;->c:F

    .line 113
    iput-object v0, p0, Lcom/samsung/samm/a/d;->d:Landroid/graphics/RectF;

    .line 114
    iput v1, p0, Lcom/samsung/samm/a/d;->e:I

    .line 115
    iput v2, p0, Lcom/samsung/samm/a/d;->l:F

    .line 116
    iput-object v0, p0, Lcom/samsung/samm/a/d;->m:Ljava/lang/String;

    .line 117
    iput-object v0, p0, Lcom/samsung/samm/a/d;->n:Ljava/lang/String;

    .line 118
    new-instance v0, Lcom/samsung/samm/b/a/e;

    invoke-direct {v0}, Lcom/samsung/samm/b/a/e;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    .line 119
    new-instance v0, Lcom/samsung/samm/b/a/e;

    invoke-direct {v0}, Lcom/samsung/samm/b/a/e;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/a/d;->s:Lcom/samsung/samm/b/a/e;

    .line 120
    iput v1, p0, Lcom/samsung/samm/a/d;->o:I

    .line 121
    iput v1, p0, Lcom/samsung/samm/a/d;->p:I

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/samm/a/d;->q:Z

    .line 125
    iput v1, p0, Lcom/samsung/samm/a/d;->f:I

    .line 126
    iput v1, p0, Lcom/samsung/samm/a/d;->g:I

    .line 127
    iput v1, p0, Lcom/samsung/samm/a/d;->h:I

    .line 128
    iput v1, p0, Lcom/samsung/samm/a/d;->i:I

    .line 129
    iput v1, p0, Lcom/samsung/samm/a/d;->j:I

    .line 130
    iput v1, p0, Lcom/samsung/samm/a/d;->k:I

    .line 131
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/samsung/samm/a/d;->a:I

    return v0
.end method

.method public a(F)V
    .locals 3

    .prologue
    .line 324
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x437f0000    # 255.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 325
    :cond_0
    const-string v0, "SAMMLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid range of size 0.000~255.000 : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :goto_0
    return-void

    .line 328
    :cond_1
    iput p1, p0, Lcom/samsung/samm/a/d;->c:F

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 310
    iput p1, p0, Lcom/samsung/samm/a/d;->b:I

    return-void
.end method

.method public a(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    const v2, 0x46fffe00    # 32767.0f

    const/high16 v1, -0x39000000    # -32768.0f

    .line 344
    if-nez p1, :cond_0

    .line 345
    const-string v0, "SAMMLibrary"

    const-string v1, "Parameter rect is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    :goto_0
    return-void

    .line 348
    :cond_0
    iget v0, p1, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 349
    iget v0, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 350
    iget v0, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 351
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    .line 352
    :cond_1
    const-string v0, "SAMMLibrary"

    const-string v1, "Invalid range of rect -32768.000~32767.000"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 355
    :cond_2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lcom/samsung/samm/a/d;->d:Landroid/graphics/RectF;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/samsung/samm/a/d;->m:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 427
    if-nez p1, :cond_0

    .line 428
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    .line 437
    :goto_0
    return v0

    .line 432
    :cond_0
    instance-of v1, p1, Lcom/samsung/samm/b/a/e;

    if-eqz v1, :cond_1

    .line 433
    check-cast p1, Lcom/samsung/samm/b/a/e;

    iput-object p1, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    goto :goto_0

    .line 437
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/samsung/samm/a/d;->s:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;[B)Z
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;[B)Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/samsung/samm/a/d;->b:I

    return v0
.end method

.method public b(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 955
    iget-object v0, p0, Lcom/samsung/samm/a/d;->s:Lcom/samsung/samm/b/a/e;

    if-nez v0, :cond_0

    .line 956
    const/4 v0, 0x0

    .line 957
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/a/d;->s:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x43b40000    # 360.0f

    .line 383
    move v0, p1

    .line 384
    :goto_0
    const/high16 v1, -0x3c4c0000    # -360.0f

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 385
    :goto_1
    cmpl-float v1, v0, v2

    if-gtz v1, :cond_1

    .line 386
    iput v0, p0, Lcom/samsung/samm/a/d;->l:F

    .line 387
    return-void

    .line 384
    :cond_0
    add-float/2addr v0, v2

    goto :goto_0

    .line 385
    :cond_1
    sub-float/2addr v0, v2

    goto :goto_1
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 451
    iput p1, p0, Lcom/samsung/samm/a/d;->o:I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/samsung/samm/a/d;->n:Ljava/lang/String;

    return-void
.end method

.method public b(Ljava/lang/String;[B)[B
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    if-nez v0, :cond_0

    .line 778
    const/4 v0, 0x0

    .line 779
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    goto :goto_0
.end method

.method public c()F
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/samsung/samm/a/d;->c:F

    return v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 463
    iput p1, p0, Lcom/samsung/samm/a/d;->p:I

    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lcom/samsung/samm/a/d;->s:Lcom/samsung/samm/b/a/e;

    if-nez v0, :cond_0

    .line 877
    const/4 v0, 0x0

    .line 878
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/a/d;->s:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->a(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 896
    iget-object v0, p0, Lcom/samsung/samm/a/d;->s:Lcom/samsung/samm/b/a/e;

    if-nez v0, :cond_0

    .line 898
    :goto_0
    return p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/a/d;->s:Lcom/samsung/samm/b/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a/e;->b(Ljava/lang/String;I)I

    move-result p2

    goto :goto_0
.end method

.method public d()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/samm/a/d;->d:Landroid/graphics/RectF;

    return-object v0
.end method

.method public e()F
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/samsung/samm/a/d;->l:F

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/samm/a/d;->m:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/samm/a/d;->n:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/samm/a/d;->r:Lcom/samsung/samm/b/a/e;

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 272
    iget v0, p0, Lcom/samsung/samm/a/d;->o:I

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/samsung/samm/a/d;->p:I

    return v0
.end method
