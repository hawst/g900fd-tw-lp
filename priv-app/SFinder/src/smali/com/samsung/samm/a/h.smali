.class public Lcom/samsung/samm/a/h;
.super Lcom/samsung/samm/a/d;
.source "SourceFile"


# instance fields
.field private A:I

.field private t:I

.field private u:[Landroid/graphics/PointF;

.field private v:[F

.field private w:F

.field private x:I

.field private y:I

.field private z:[I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x5

    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 770
    invoke-direct {p0}, Lcom/samsung/samm/a/d;-><init>()V

    .line 772
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/samm/a/h;->b:I

    .line 773
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/samsung/samm/a/h;->c:F

    .line 774
    const/high16 v0, 0x42900000    # 72.0f

    iput v0, p0, Lcom/samsung/samm/a/h;->w:F

    .line 775
    iput v1, p0, Lcom/samsung/samm/a/h;->a:I

    .line 777
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    .line 778
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/h;->v:[F

    .line 781
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/samm/a/h;->x:I

    .line 782
    iput v1, p0, Lcom/samsung/samm/a/h;->y:I

    .line 783
    new-array v0, v2, [I

    aput v4, v0, v1

    aput v2, v0, v3

    const/4 v1, 0x4

    aput v2, v0, v1

    const/16 v1, 0x46

    aput v1, v0, v4

    const/4 v1, 0x6

    aput v5, v0, v1

    const/4 v1, 0x7

    aput v3, v0, v1

    iput-object v0, p0, Lcom/samsung/samm/a/h;->z:[I

    .line 784
    iput v5, p0, Lcom/samsung/samm/a/h;->A:I

    .line 785
    return-void
.end method

.method private s()V
    .locals 1

    .prologue
    .line 1217
    invoke-direct {p0}, Lcom/samsung/samm/a/h;->t()Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/a/h;->d:Landroid/graphics/RectF;

    return-void
.end method

.method private t()Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1232
    iget-object v0, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    array-length v0, v0

    if-gtz v0, :cond_1

    .line 1233
    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1257
    :goto_0
    return-object v0

    .line 1239
    :cond_1
    const/4 v0, 0x0

    move v2, v1

    move v3, v1

    move v4, v1

    :goto_1
    iget-object v5, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    array-length v5, v5

    if-lt v0, v5, :cond_2

    .line 1256
    iget v0, p0, Lcom/samsung/samm/a/h;->c:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v0, v5

    .line 1257
    new-instance v0, Landroid/graphics/RectF;

    sub-float/2addr v4, v5

    sub-float/2addr v3, v5

    add-float/2addr v2, v5

    add-float/2addr v1, v5

    invoke-direct {v0, v4, v3, v2, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    .line 1240
    :cond_2
    iget-object v5, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v4, v5

    if-gtz v5, :cond_3

    if-nez v0, :cond_4

    .line 1241
    :cond_3
    iget-object v4, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    aget-object v4, v4, v0

    iget v4, v4, Landroid/graphics/PointF;->x:F

    .line 1242
    :cond_4
    iget-object v5, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v3, v5

    if-gtz v5, :cond_5

    if-nez v0, :cond_6

    .line 1243
    :cond_5
    iget-object v3, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 1244
    :cond_6
    iget-object v5, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v2, v5

    if-ltz v5, :cond_7

    if-nez v0, :cond_8

    .line 1245
    :cond_7
    iget-object v2, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/PointF;->x:F

    .line 1246
    :cond_8
    iget-object v5, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v1, v5

    if-ltz v5, :cond_9

    if-nez v0, :cond_a

    .line 1247
    :cond_9
    iget-object v1, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    aget-object v1, v1, v0

    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 1239
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a([F)V
    .locals 3

    .prologue
    .line 942
    if-eqz p1, :cond_0

    array-length v0, p1

    if-gtz v0, :cond_2

    .line 943
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/h;->v:[F

    .line 950
    :cond_1
    return-void

    .line 945
    :cond_2
    array-length v0, p1

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/samm/a/h;->v:[F

    .line 946
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 947
    iget-object v1, p0, Lcom/samsung/samm/a/h;->v:[F

    aget v2, p1, v0

    aput v2, v1, v0

    .line 946
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a([Landroid/graphics/PointF;)V
    .locals 5

    .prologue
    .line 873
    if-eqz p1, :cond_0

    array-length v0, p1

    if-gtz v0, :cond_3

    .line 874
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    .line 883
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/a/h;->d:Landroid/graphics/RectF;

    if-nez v0, :cond_2

    .line 884
    invoke-direct {p0}, Lcom/samsung/samm/a/h;->s()V

    .line 885
    :cond_2
    return-void

    .line 876
    :cond_3
    array-length v0, p1

    new-array v0, v0, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    .line 877
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 878
    iget-object v1, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aget-object v4, p1, v0

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v1, v0

    .line 877
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(II)Z
    .locals 4

    .prologue
    const/16 v3, 0x64

    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 1119
    if-nez p1, :cond_0

    if-lt p2, v0, :cond_0

    const/16 v1, 0xc

    if-le p2, v1, :cond_6

    .line 1120
    :cond_0
    if-ne p1, v0, :cond_1

    if-lt p2, v0, :cond_1

    const/16 v1, 0x10

    if-le p2, v1, :cond_6

    .line 1121
    :cond_1
    if-ne p1, v2, :cond_2

    if-lt p2, v0, :cond_2

    const/16 v1, 0x14

    if-le p2, v1, :cond_6

    .line 1122
    :cond_2
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    if-lt p2, v0, :cond_3

    const/16 v1, 0x28

    if-le p2, v1, :cond_6

    .line 1123
    :cond_3
    const/4 v1, 0x5

    if-ne p1, v1, :cond_4

    if-lt p2, v0, :cond_4

    if-le p2, v3, :cond_6

    .line 1124
    :cond_4
    const/4 v1, 0x6

    if-ne p1, v1, :cond_5

    if-lt p2, v0, :cond_5

    if-le p2, v3, :cond_6

    .line 1125
    :cond_5
    const/4 v1, 0x7

    if-ne p1, v1, :cond_7

    if-ltz p2, :cond_7

    if-gt p2, v2, :cond_7

    .line 1127
    :cond_6
    iget-object v1, p0, Lcom/samsung/samm/a/h;->z:[I

    aput p2, v1, p1

    .line 1131
    :goto_0
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 803
    if-eqz p1, :cond_0

    .line 804
    if-eq p1, v0, :cond_0

    .line 805
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    .line 806
    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    .line 807
    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    .line 808
    const/4 v1, 0x5

    if-eq p1, v1, :cond_0

    .line 809
    const/4 v1, 0x6

    if-eq p1, v1, :cond_0

    .line 810
    const/4 v1, 0x7

    if-eq p1, v1, :cond_0

    .line 811
    const/16 v1, 0x8

    if-eq p1, v1, :cond_0

    .line 812
    const/16 v1, 0x64

    if-ne p1, v1, :cond_1

    .line 813
    :cond_0
    iput p1, p0, Lcom/samsung/samm/a/h;->a:I

    .line 818
    :goto_0
    return v0

    .line 817
    :cond_1
    const-string v0, "SAMMLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Undefined Stroke Style : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 978
    iput p1, p0, Lcom/samsung/samm/a/h;->t:I

    return-void
.end method

.method public f(I)Z
    .locals 1

    .prologue
    .line 1010
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    .line 1011
    :cond_0
    const/4 v0, 0x0

    .line 1014
    :goto_0
    return v0

    .line 1013
    :cond_1
    iput p1, p0, Lcom/samsung/samm/a/h;->x:I

    .line 1014
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public g(I)Z
    .locals 1

    .prologue
    .line 1046
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-le p1, v0, :cond_1

    .line 1047
    :cond_0
    const/4 v0, 0x0

    .line 1050
    :goto_0
    return v0

    .line 1049
    :cond_1
    iput p1, p0, Lcom/samsung/samm/a/h;->y:I

    .line 1050
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h(I)I
    .locals 1

    .prologue
    .line 1077
    if-eqz p1, :cond_0

    .line 1078
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 1079
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 1081
    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    .line 1082
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 1083
    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    .line 1084
    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    .line 1085
    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    .line 1086
    const/4 v0, -0x1

    .line 1088
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/a/h;->z:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public i(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1170
    if-eq p1, v0, :cond_0

    .line 1171
    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    .line 1172
    const/4 v1, 0x5

    if-eq p1, v1, :cond_0

    .line 1173
    const/4 v1, 0x6

    if-eq p1, v1, :cond_0

    .line 1174
    const/16 v1, 0xb

    if-eq p1, v1, :cond_0

    .line 1175
    const/16 v1, 0xc

    if-eq p1, v1, :cond_0

    .line 1176
    const/4 v0, 0x0

    .line 1179
    :goto_0
    return v0

    .line 1178
    :cond_0
    iput p1, p0, Lcom/samsung/samm/a/h;->A:I

    goto :goto_0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    array-length v0, v0

    .line 836
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()[Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 850
    iget-object v0, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    if-nez v0, :cond_0

    .line 851
    const/4 v0, 0x0

    .line 857
    :goto_0
    return-object v0

    .line 853
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    array-length v0, v0

    new-array v1, v0, [Landroid/graphics/PointF;

    .line 854
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    move-object v0, v1

    .line 857
    goto :goto_0

    .line 855
    :cond_1
    iget-object v2, p0, Lcom/samsung/samm/a/h;->u:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 854
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public m()I
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/samsung/samm/a/h;->v:[F

    if-eqz v0, :cond_0

    .line 905
    iget-object v0, p0, Lcom/samsung/samm/a/h;->v:[F

    array-length v0, v0

    .line 907
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()[F
    .locals 3

    .prologue
    .line 921
    iget-object v0, p0, Lcom/samsung/samm/a/h;->v:[F

    if-nez v0, :cond_0

    .line 922
    const/4 v0, 0x0

    .line 928
    :goto_0
    return-object v0

    .line 924
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/a/h;->v:[F

    array-length v0, v0

    new-array v1, v0, [F

    .line 925
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/samsung/samm/a/h;->v:[F

    array-length v2, v2

    if-lt v0, v2, :cond_1

    move-object v0, v1

    .line 928
    goto :goto_0

    .line 926
    :cond_1
    iget-object v2, p0, Lcom/samsung/samm/a/h;->v:[F

    aget v2, v2, v0

    aput v2, v1, v0

    .line 925
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public o()I
    .locals 1

    .prologue
    .line 964
    iget v0, p0, Lcom/samsung/samm/a/h;->t:I

    return v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 992
    iget v0, p0, Lcom/samsung/samm/a/h;->x:I

    return v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 1029
    iget v0, p0, Lcom/samsung/samm/a/h;->y:I

    return v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 1148
    iget v0, p0, Lcom/samsung/samm/a/h;->A:I

    return v0
.end method
