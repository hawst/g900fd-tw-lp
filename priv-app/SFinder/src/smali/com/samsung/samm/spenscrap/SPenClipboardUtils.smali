.class public Lcom/samsung/samm/spenscrap/SPenClipboardUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final CLIPBOARD_EXTRA_URL:Ljava/lang/String; = "KeyURL"

.field public static final KEY_IMAGE_FILE_PATH_FOR_PASTE_INTENT:Ljava/lang/String; = "copyPath"

.field protected static final TAG:Ljava/lang/String; = "SPenClipboardUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyImageFileToClipbboard(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 86
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.clipboardsaveservice.CLIPBOARD_COPY_RECEIVER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 87
    const-string v1, "copyPath"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 89
    return-void
.end method

.method public static copyIntentToClipbboard(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 157
    if-nez p0, :cond_0

    .line 158
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Context is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 177
    :goto_0
    return v0

    .line 163
    :cond_0
    const-string v0, "clipboard"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 164
    if-nez v0, :cond_1

    .line 165
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Fail to get Clipboard"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 166
    goto :goto_0

    .line 170
    :cond_1
    const-string v2, "Intent"

    invoke-static {v2, p1}, Landroid/content/ClipData;->newIntent(Ljava/lang/CharSequence;Landroid/content/Intent;)Landroid/content/ClipData;

    move-result-object v2

    .line 171
    if-nez v2, :cond_2

    .line 172
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Fail to make ClipData"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 173
    goto :goto_0

    .line 176
    :cond_2
    invoke-virtual {v0, v2}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 177
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static copyTextToClipboard(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;)Z
    .locals 3

    .prologue
    .line 100
    if-nez p0, :cond_0

    .line 101
    const-string v0, "SPenClipboardUtils"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/4 v0, 0x0

    .line 112
    :goto_0
    return v0

    .line 104
    :cond_0
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-virtual {p2}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 106
    invoke-virtual {p2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    .line 107
    if-ne v1, v2, :cond_1

    .line 108
    invoke-static {p0, p1, v0}, Lcom/samsung/samm/spenscrap/SPenClipboardUtils;->copyTextToClipboard(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 111
    :cond_1
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-static {p0, p1, v0}, Lcom/samsung/samm/spenscrap/SPenClipboardUtils;->copyTextToClipboard(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static copyTextToClipboard(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 125
    if-nez p0, :cond_0

    .line 126
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Context is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 144
    :goto_0
    return v0

    .line 131
    :cond_0
    const-string v0, "clipboard"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 132
    if-nez v0, :cond_1

    .line 133
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Fail to get Clipboard"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 134
    goto :goto_0

    .line 137
    :cond_1
    invoke-static {p1, p2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v2

    .line 138
    if-nez v2, :cond_2

    .line 139
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Fail to make ClipData"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 140
    goto :goto_0

    .line 143
    :cond_2
    invoke-virtual {v0, v2}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 144
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static getClipboardIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 578
    if-nez p0, :cond_1

    .line 579
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Context is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 599
    :cond_0
    :goto_0
    return-object v0

    .line 582
    :cond_1
    const-string v0, "clipboard"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 583
    if-nez v0, :cond_2

    .line 584
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Fail to get Clipboard"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 585
    goto :goto_0

    .line 587
    :cond_2
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v2

    if-nez v2, :cond_3

    .line 588
    const-string v0, "SPenClipboardUtils"

    const-string v2, "There is no primary clip"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 589
    goto :goto_0

    .line 593
    :cond_3
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 594
    if-nez v0, :cond_0

    move-object v0, v1

    .line 599
    goto :goto_0
.end method

.method public static getClipboardText(Landroid/content/Context;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 424
    if-nez p0, :cond_1

    .line 425
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Context is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 486
    :cond_0
    :goto_0
    return-object v0

    .line 428
    :cond_1
    const-string v0, "clipboard"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 429
    if-nez v0, :cond_2

    .line 430
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Fail to get Clipboard"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 431
    goto :goto_0

    .line 433
    :cond_2
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v2

    if-nez v2, :cond_3

    .line 434
    const-string v0, "SPenClipboardUtils"

    const-string v2, "There is no primary clip"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 435
    goto :goto_0

    .line 438
    :cond_3
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClipDescription()Landroid/content/ClipDescription;

    move-result-object v4

    .line 439
    const-string v2, "text/plain"

    invoke-virtual {v4, v2}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 440
    const-string v0, "SPenClipboardUtils"

    const-string v2, "There is no text type(MIMETYPE_TEXT_PLAIN) clip"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 441
    goto :goto_0

    .line 445
    :cond_4
    invoke-virtual {v4}, Landroid/content/ClipDescription;->getMimeTypeCount()I

    move-result v5

    .line 447
    const/4 v2, 0x0

    :goto_1
    if-lt v2, v5, :cond_5

    move v2, v3

    .line 456
    :goto_2
    if-ne v2, v3, :cond_8

    .line 457
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Fail to find item of MIMETYPE_TEXT_PLAIN"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 458
    goto :goto_0

    .line 448
    :cond_5
    invoke-virtual {v4, v2}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v6

    .line 449
    if-nez v6, :cond_7

    .line 447
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 451
    :cond_7
    const-string v7, "text/plain"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    goto :goto_2

    .line 464
    :cond_8
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    .line 466
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 468
    if-nez v0, :cond_0

    .line 474
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 476
    if-nez v2, :cond_0

    .line 485
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Clipboard contains an invalid data type"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 486
    goto :goto_0
.end method

.method public static isClipboardClipExist(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 61
    if-nez p0, :cond_0

    .line 62
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Context is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 74
    :goto_0
    return v0

    .line 65
    :cond_0
    const-string v0, "clipboard"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 66
    if-nez v0, :cond_1

    .line 67
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Fail to get Clipboard"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_1
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v0

    if-nez v0, :cond_2

    .line 71
    const-string v0, "SPenClipboardUtils"

    const-string v2, "There is no primary clip"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 72
    goto :goto_0

    .line 74
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isClipboardTextExist(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 44
    const-string v0, "clipboard"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 45
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 51
    :goto_0
    return v0

    .line 48
    :cond_0
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClipDescription()Landroid/content/ClipDescription;

    move-result-object v0

    const-string v2, "text/plain"

    invoke-virtual {v0, v2}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 49
    goto :goto_0

    .line 51
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static pasteClipboardImageOfIntent(Landroid/content/Context;Landroid/widget/EditText;II)Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 504
    if-nez p0, :cond_1

    .line 505
    const-string v0, "SPenClipboardUtils"

    const-string v2, "Context is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    :cond_0
    :goto_0
    return v1

    .line 508
    :cond_1
    if-nez p1, :cond_2

    .line 509
    const-string v0, "SPenClipboardUtils"

    const-string v2, "EditText is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 513
    :cond_2
    invoke-static {p0}, Lcom/samsung/samm/spenscrap/SPenClipboardUtils;->getClipboardIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 514
    if-eqz v2, :cond_0

    .line 522
    const-string v0, "copyPath"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 523
    if-nez v0, :cond_3

    .line 527
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 528
    if-eqz v2, :cond_3

    .line 529
    invoke-static {p0, v2}, Lcom/samsung/samm/spenscrap/a/a;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 534
    :cond_3
    if-lez p2, :cond_4

    if-gtz p3, :cond_6

    .line 536
    :cond_4
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 544
    :goto_1
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v2

    .line 545
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v5

    .line 547
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 548
    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 550
    if-gez v3, :cond_5

    move v3, v1

    .line 553
    :cond_5
    if-gez v2, :cond_8

    .line 557
    :goto_2
    const-string v2, "\u3000"

    .line 559
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 560
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5, v3, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 562
    new-instance v1, Landroid/text/style/ImageSpan;

    invoke-direct {v1, p0, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    .line 563
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 564
    if-lt v3, v0, :cond_7

    .line 565
    add-int/lit8 v0, v0, -0x1

    .line 566
    :goto_3
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    const/16 v5, 0x21

    invoke-interface {v2, v1, v0, v3, v5}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    move v1, v4

    .line 568
    goto :goto_0

    .line 540
    :cond_6
    invoke-static {v0, p2, p3, v4}, Lcom/samsung/samm/spenscrap/a/a;->a(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    :cond_7
    move v0, v3

    goto :goto_3

    :cond_8
    move v1, v2

    goto :goto_2
.end method

.method public static pasteClipboardText(Landroid/content/Context;Landroid/widget/EditText;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 363
    if-nez p0, :cond_1

    .line 364
    const-string v1, "SPenClipboardUtils"

    const-string v2, "Context is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_0
    :goto_0
    return v0

    .line 367
    :cond_1
    if-nez p1, :cond_2

    .line 368
    const-string v1, "SPenClipboardUtils"

    const-string v2, "EditText is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 372
    :cond_2
    invoke-static {p0}, Lcom/samsung/samm/spenscrap/SPenClipboardUtils;->getClipboardText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 373
    if-eqz v3, :cond_0

    .line 397
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 398
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v4

    .line 400
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 401
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 403
    if-gez v2, :cond_3

    move v2, v0

    .line 406
    :cond_3
    if-gez v1, :cond_4

    .line 410
    :goto_1
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 411
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1, v2, v0, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 413
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static pasteFromClipboard(Landroid/content/Context;Landroid/widget/EditText;)Z
    .locals 11

    .prologue
    const/16 v3, 0x12c

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 193
    if-nez p0, :cond_0

    .line 194
    const-string v0, "SPenClipboardUtils"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :goto_0
    return v2

    .line 197
    :cond_0
    if-nez p1, :cond_1

    .line 198
    const-string v0, "SPenClipboardUtils"

    const-string v1, "EditText is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 205
    :cond_1
    const-string v0, "clipboard"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 206
    if-nez v0, :cond_2

    .line 207
    const-string v0, "SPenClipboardUtils"

    const-string v1, "Fail to get Clipboard"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 210
    :cond_2
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v1

    if-nez v1, :cond_3

    .line 211
    const-string v0, "SPenClipboardUtils"

    const-string v1, "There is no primary clip"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 221
    :cond_3
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 222
    if-eqz v4, :cond_8

    .line 228
    const-string v0, "copyPath"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 229
    if-nez v0, :cond_4

    .line 233
    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 234
    if-eqz v1, :cond_4

    .line 235
    invoke-static {p0, v1}, Lcom/samsung/samm/spenscrap/a/a;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 248
    :cond_4
    invoke-static {v0, v3, v3, v5}, Lcom/samsung/samm/spenscrap/a/a;->a(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 254
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 255
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v6

    .line 256
    invoke-static {v1, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 257
    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 258
    if-gez v3, :cond_5

    move v3, v2

    .line 259
    :cond_5
    if-gez v1, :cond_6

    move v1, v2

    .line 261
    :cond_6
    const-string v2, "\u3000"

    .line 263
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-static {v6, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 264
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6, v3, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 266
    new-instance v1, Landroid/text/style/ImageSpan;

    invoke-direct {v1, p0, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    .line 267
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 268
    if-lt v3, v0, :cond_10

    .line 269
    add-int/lit8 v0, v0, -0x1

    .line 270
    :goto_1
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    const/16 v6, 0x21

    invoke-interface {v2, v1, v0, v3, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 275
    const-string v1, "KeyURL"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 276
    if-eqz v1, :cond_7

    .line 277
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "\n"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v0, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    :cond_7
    move v2, v5

    .line 280
    goto/16 :goto_0

    .line 288
    :cond_8
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClipDescription()Landroid/content/ClipDescription;

    move-result-object v8

    .line 292
    invoke-virtual {v8}, Landroid/content/ClipDescription;->getMimeTypeCount()I

    move-result v9

    move v7, v2

    move v4, v2

    .line 293
    :goto_2
    if-lt v7, v9, :cond_9

    .line 345
    if-nez v4, :cond_e

    .line 346
    const-string v0, "SPenClipboardUtils"

    const-string v1, "Fail to paste item"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 294
    :cond_9
    invoke-virtual {v8, v7}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v1

    .line 295
    if-nez v1, :cond_a

    move v1, v4

    .line 293
    :goto_3
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v4, v1

    goto :goto_2

    .line 300
    :cond_a
    const-string v3, "text/plain"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 304
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v3

    .line 306
    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 307
    if-nez v1, :cond_b

    .line 311
    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v3

    .line 313
    if-nez v3, :cond_b

    .line 316
    const-string v1, "SPenClipboardUtils"

    const-string v3, "Clipboard contains an invalid data type"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v4

    .line 317
    goto :goto_3

    .line 332
    :cond_b
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v3

    .line 333
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v10

    .line 334
    invoke-static {v3, v10}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 335
    invoke-static {v3, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 336
    if-gez v6, :cond_c

    move v6, v2

    .line 337
    :cond_c
    if-gez v3, :cond_d

    move v3, v2

    .line 339
    :cond_d
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-static {v10, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 340
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10, v6, v3, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 341
    add-int/lit8 v1, v4, 0x1

    goto :goto_3

    :cond_e
    move v2, v5

    .line 349
    goto/16 :goto_0

    :cond_f
    move v1, v4

    goto :goto_3

    :cond_10
    move v0, v3

    goto/16 :goto_1
.end method
