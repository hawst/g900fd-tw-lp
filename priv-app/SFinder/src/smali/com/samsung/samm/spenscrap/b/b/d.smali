.class public Lcom/samsung/samm/spenscrap/b/b/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/samsung/samm/spenscrap/b/b/c;Ljava/util/ArrayList;D)Lcom/samsung/samm/spenscrap/b/b/e;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/samm/spenscrap/b/b/c;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;D)",
            "Lcom/samsung/samm/spenscrap/b/b/e;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 121
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 123
    new-instance v5, Lcom/samsung/samm/spenscrap/b/b/e;

    invoke-direct {v5}, Lcom/samsung/samm/spenscrap/b/b/e;-><init>()V

    .line 124
    iput-object p1, v5, Lcom/samsung/samm/spenscrap/b/b/e;->e:Lcom/samsung/samm/spenscrap/b/b/c;

    move v3, v2

    .line 125
    :goto_0
    if-lt v3, v4, :cond_0

    .line 133
    return-object v5

    .line 127
    :cond_0
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 128
    invoke-interface {p1, v0}, Lcom/samsung/samm/spenscrap/b/b/c;->a(Landroid/graphics/PointF;)D

    move-result-wide v6

    .line 129
    cmpg-double v1, v6, p3

    if-gez v1, :cond_1

    const/4 v1, 0x1

    .line 131
    :goto_1
    double-to-float v6, v6

    invoke-virtual {v5, v0, v6, v1}, Lcom/samsung/samm/spenscrap/b/b/e;->a(Landroid/graphics/PointF;FZ)V

    .line 125
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 129
    goto :goto_1
.end method

.method protected a(Ljava/util/ArrayList;IZ)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;IZ)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 38
    if-nez p1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-object v0

    .line 40
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 41
    if-lt v3, p2, :cond_0

    .line 43
    if-eqz p3, :cond_2

    add-int/lit8 p2, p2, -0x2

    .line 45
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 46
    if-eqz p3, :cond_6

    .line 47
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_3
    :goto_1
    if-lt v2, p2, :cond_4

    .line 58
    add-int/lit8 v0, v3, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object v0, v1

    .line 75
    goto :goto_0

    .line 50
    :cond_4
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    add-int/lit8 v0, v3, -0x2

    int-to-double v6, v0

    mul-double/2addr v4, v6

    double-to-int v0, v4

    add-int/lit8 v0, v0, 0x1

    .line 51
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 52
    invoke-virtual {p0, v1, v0}, Lcom/samsung/samm/spenscrap/b/b/d;->a(Ljava/util/ArrayList;Landroid/graphics/PointF;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 53
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 65
    :cond_5
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    .line 66
    int-to-double v6, v3

    mul-double/2addr v4, v6

    double-to-int v0, v4

    .line 67
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 68
    invoke-virtual {p0, v1, v0}, Lcom/samsung/samm/spenscrap/b/b/d;->a(Ljava/util/ArrayList;Landroid/graphics/PointF;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 69
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 64
    :cond_6
    if-lt v2, p2, :cond_5

    goto :goto_2
.end method

.method protected a(Ljava/util/ArrayList;Landroid/graphics/PointF;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Landroid/graphics/PointF;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 19
    if-nez p1, :cond_0

    move v0, v1

    .line 27
    :goto_0
    return v0

    .line 21
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 22
    :goto_1
    if-lt v2, v3, :cond_1

    move v0, v1

    .line 27
    goto :goto_0

    .line 23
    :cond_1
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 24
    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v5, p2, Landroid/graphics/PointF;->x:F

    cmpl-float v4, v4, v5

    if-nez v4, :cond_2

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v4, p2, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_2

    .line 25
    const/4 v0, 0x1

    goto :goto_0

    .line 22
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method

.method protected b(Ljava/util/ArrayList;IZ)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;IZ)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 86
    if-nez p1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return-object v0

    .line 88
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 89
    if-lt v3, p2, :cond_0

    .line 92
    if-eqz p3, :cond_2

    add-int/lit8 p2, p2, -0x1

    .line 93
    :cond_2
    int-to-float v0, v3

    int-to-float v1, p2

    div-float v4, v0, v1

    .line 95
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 96
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-lt v2, p2, :cond_4

    .line 102
    if-eqz p3, :cond_3

    .line 104
    add-int/lit8 v0, v3, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v0, v1

    .line 107
    goto :goto_0

    .line 97
    :cond_4
    int-to-float v0, v2

    mul-float/2addr v0, v4

    float-to-int v0, v0

    .line 99
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 100
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method
