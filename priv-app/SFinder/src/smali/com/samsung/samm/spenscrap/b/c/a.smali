.class Lcom/samsung/samm/spenscrap/b/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:[[D

.field private final b:I

.field private final c:I

.field private d:I

.field private final e:[I


# direct methods
.method public constructor <init>(Lcom/samsung/samm/spenscrap/b/c/c;)V
    .locals 12

    .prologue
    .line 1174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178
    invoke-virtual {p1}, Lcom/samsung/samm/spenscrap/b/c/c;->c()[[D

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    .line 1179
    invoke-virtual {p1}, Lcom/samsung/samm/spenscrap/b/c/c;->d()I

    move-result v0

    iput v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    .line 1180
    invoke-virtual {p1}, Lcom/samsung/samm/spenscrap/b/c/c;->e()I

    move-result v0

    iput v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->c:I

    .line 1181
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->e:[I

    .line 1182
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    if-lt v0, v1, :cond_0

    .line 1185
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->d:I

    .line 1187
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    new-array v3, v0, [D

    .line 1191
    const/4 v2, 0x0

    :goto_1
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->c:I

    if-lt v2, v0, :cond_1

    .line 1245
    return-void

    .line 1183
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/c/a;->e:[I

    aput v0, v1, v0

    .line 1182
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1195
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget v1, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    if-lt v0, v1, :cond_4

    .line 1201
    const/4 v0, 0x0

    :goto_3
    iget v1, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    if-lt v0, v1, :cond_5

    .line 1218
    add-int/lit8 v0, v2, 0x1

    move v1, v2

    :goto_4
    iget v4, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    if-lt v0, v4, :cond_7

    .line 1223
    if-eq v1, v2, :cond_2

    .line 1224
    const/4 v0, 0x0

    :goto_5
    iget v4, p0, Lcom/samsung/samm/spenscrap/b/c/a;->c:I

    if-lt v0, v4, :cond_9

    .line 1229
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->e:[I

    aget v0, v0, v1

    .line 1230
    iget-object v4, p0, Lcom/samsung/samm/spenscrap/b/c/a;->e:[I

    iget-object v5, p0, Lcom/samsung/samm/spenscrap/b/c/a;->e:[I

    aget v5, v5, v2

    aput v5, v4, v1

    .line 1231
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/c/a;->e:[I

    aput v0, v1, v2

    .line 1232
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->d:I

    neg-int v0, v0

    iput v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->d:I

    .line 1239
    :cond_2
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    if-ge v2, v0, :cond_3

    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v2

    const-wide/16 v4, 0x0

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_3

    .line 1240
    add-int/lit8 v0, v2, 0x1

    :goto_6
    iget v1, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    if-lt v0, v1, :cond_a

    .line 1191
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1196
    :cond_4
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v1, v1, v0

    aget-wide v4, v1, v2

    aput-wide v4, v3, v0

    .line 1195
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1202
    :cond_5
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v6, v1, v0

    .line 1206
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 1207
    const-wide/16 v4, 0x0

    .line 1208
    const/4 v1, 0x0

    :goto_7
    if-lt v1, v7, :cond_6

    .line 1212
    aget-wide v8, v3, v0

    sub-double v4, v8, v4

    aput-wide v4, v3, v0

    aput-wide v4, v6, v2

    .line 1201
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1209
    :cond_6
    aget-wide v8, v6, v1

    aget-wide v10, v3, v1

    mul-double/2addr v8, v10

    add-double/2addr v4, v8

    .line 1208
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 1219
    :cond_7
    aget-wide v4, v3, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    aget-wide v6, v3, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    cmpl-double v4, v4, v6

    if-lez v4, :cond_8

    move v1, v0

    .line 1218
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1225
    :cond_9
    iget-object v4, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v4, v4, v1

    aget-wide v4, v4, v0

    .line 1226
    iget-object v6, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v7, v7, v2

    aget-wide v8, v7, v0

    aput-wide v8, v6, v0

    .line 1227
    iget-object v6, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v6, v6, v2

    aput-wide v4, v6, v0

    .line 1224
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_5

    .line 1241
    :cond_a
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v1, v1, v0

    aget-wide v4, v1, v2

    iget-object v6, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v6, v6, v2

    aget-wide v6, v6, v2

    div-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 1240
    add-int/lit8 v0, v0, 0x1

    goto :goto_6
.end method


# virtual methods
.method public a(Lcom/samsung/samm/spenscrap/b/c/c;)Lcom/samsung/samm/spenscrap/b/c/c;
    .locals 14

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1389
    invoke-virtual {p1}, Lcom/samsung/samm/spenscrap/b/c/c;->d()I

    move-result v2

    iget v3, p0, Lcom/samsung/samm/spenscrap/b/c/a;->b:I

    if-eq v2, v3, :cond_0

    .line 1392
    const-string v1, "Matrix:LUDecomposition"

    const-string v2, "Matrix row dimensions must agree."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1425
    :goto_0
    return-object v0

    .line 1395
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/samm/spenscrap/b/c/a;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1397
    const-string v1, "Matrix:LUDecomposition"

    const-string v2, "Matrix is singular."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1402
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/samm/spenscrap/b/c/c;->e()I

    move-result v5

    .line 1403
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->e:[I

    add-int/lit8 v2, v5, -0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/samsung/samm/spenscrap/b/c/c;->a([III)Lcom/samsung/samm/spenscrap/b/c/c;

    move-result-object v3

    .line 1404
    invoke-virtual {v3}, Lcom/samsung/samm/spenscrap/b/c/c;->b()[[D

    move-result-object v6

    move v0, v1

    .line 1407
    :goto_1
    iget v2, p0, Lcom/samsung/samm/spenscrap/b/c/a;->c:I

    if-lt v0, v2, :cond_2

    .line 1415
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/a;->c:I

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_2
    if-gez v4, :cond_5

    move-object v0, v3

    .line 1425
    goto :goto_0

    .line 1408
    :cond_2
    add-int/lit8 v2, v0, 0x1

    :goto_3
    iget v4, p0, Lcom/samsung/samm/spenscrap/b/c/a;->c:I

    if-lt v2, v4, :cond_3

    .line 1407
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v4, v1

    .line 1409
    :goto_4
    if-lt v4, v5, :cond_4

    .line 1408
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1410
    :cond_4
    aget-object v7, v6, v2

    aget-wide v8, v7, v4

    aget-object v10, v6, v0

    aget-wide v10, v10, v4

    iget-object v12, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v12, v12, v2

    aget-wide v12, v12, v0

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    aput-wide v8, v7, v4

    .line 1409
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_5
    move v0, v1

    .line 1416
    :goto_5
    if-lt v0, v5, :cond_6

    move v2, v1

    .line 1419
    :goto_6
    if-lt v2, v4, :cond_7

    .line 1415
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_2

    .line 1417
    :cond_6
    aget-object v2, v6, v4

    aget-wide v8, v2, v0

    iget-object v7, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v7, v7, v4

    aget-wide v10, v7, v4

    div-double/2addr v8, v10

    aput-wide v8, v2, v0

    .line 1416
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    move v0, v1

    .line 1420
    :goto_7
    if-lt v0, v5, :cond_8

    .line 1419
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 1421
    :cond_8
    aget-object v7, v6, v2

    aget-wide v8, v7, v0

    aget-object v10, v6, v4

    aget-wide v10, v10, v0

    iget-object v12, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v12, v12, v2

    aget-wide v12, v12, v4

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    aput-wide v8, v7, v0

    .line 1420
    add-int/lit8 v0, v0, 0x1

    goto :goto_7
.end method

.method public a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1284
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/samsung/samm/spenscrap/b/c/a;->c:I

    if-lt v0, v2, :cond_1

    .line 1288
    const/4 v1, 0x1

    :cond_0
    return v1

    .line 1285
    :cond_1
    iget-object v2, p0, Lcom/samsung/samm/spenscrap/b/c/a;->a:[[D

    aget-object v2, v2, v0

    aget-wide v2, v2, v0

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 1284
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
