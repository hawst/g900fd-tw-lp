.class public Lcom/samsung/samm/spenscrap/b/b/a;
.super Lcom/samsung/samm/spenscrap/b/b/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/samm/spenscrap/b/b/d;-><init>()V

    .line 17
    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)Lcom/samsung/samm/spenscrap/b/b/c;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;)",
            "Lcom/samsung/samm/spenscrap/b/b/c;"
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 26
    const/4 v0, 0x5

    if-eq v4, v0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const/4 v0, 0x5

    const/4 v1, 0x5

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[D

    .line 30
    const/4 v1, 0x5

    const/4 v2, 0x1

    filled-new-array {v1, v2}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[D

    .line 34
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-lt v3, v4, :cond_1

    .line 50
    new-instance v2, Lcom/samsung/samm/spenscrap/b/c/c;

    invoke-direct {v2, v0}, Lcom/samsung/samm/spenscrap/b/c/c;-><init>([[D)V

    .line 51
    new-instance v0, Lcom/samsung/samm/spenscrap/b/c/c;

    invoke-direct {v0, v1}, Lcom/samsung/samm/spenscrap/b/c/c;-><init>([[D)V

    .line 52
    invoke-virtual {v2, v0}, Lcom/samsung/samm/spenscrap/b/c/c;->a(Lcom/samsung/samm/spenscrap/b/c/c;)Lcom/samsung/samm/spenscrap/b/c/c;

    move-result-object v3

    .line 53
    if-nez v3, :cond_2

    .line 54
    const-string v0, "EllipseRANSAC"

    const-string v1, "Error to compute Ellipse Matrix"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const/4 v0, 0x0

    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 36
    iget v5, v2, Landroid/graphics/PointF;->x:F

    float-to-double v6, v5

    .line 37
    iget v2, v2, Landroid/graphics/PointF;->y:F

    float-to-double v8, v2

    .line 39
    aget-object v2, v0, v3

    const/4 v5, 0x0

    mul-double v10, v6, v8

    aput-wide v10, v2, v5

    .line 40
    aget-object v2, v0, v3

    const/4 v5, 0x1

    mul-double v10, v8, v8

    aput-wide v10, v2, v5

    .line 41
    aget-object v2, v0, v3

    const/4 v5, 0x2

    aput-wide v6, v2, v5

    .line 42
    aget-object v2, v0, v3

    const/4 v5, 0x3

    aput-wide v8, v2, v5

    .line 43
    aget-object v2, v0, v3

    const/4 v5, 0x4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v2, v5

    .line 45
    aget-object v2, v1, v3

    const/4 v5, 0x0

    neg-double v8, v6

    mul-double/2addr v6, v8

    aput-wide v6, v2, v5

    .line 34
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 58
    :cond_2
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Lcom/samsung/samm/spenscrap/b/c/c;->a(II)D

    move-result-wide v0

    .line 61
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v4

    if-lez v2, :cond_3

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    div-double v0, v4, v0

    .line 63
    :goto_2
    new-instance v2, Lcom/samsung/samm/spenscrap/b/b/b;

    invoke-direct {v2}, Lcom/samsung/samm/spenscrap/b/b/b;-><init>()V

    .line 64
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v4, v0

    iput-wide v4, v2, Lcom/samsung/samm/spenscrap/b/b/b;->a:D

    .line 65
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/samm/spenscrap/b/c/c;->a(II)D

    move-result-wide v4

    mul-double/2addr v4, v0

    iput-wide v4, v2, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    .line 66
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/samm/spenscrap/b/c/c;->a(II)D

    move-result-wide v4

    mul-double/2addr v4, v0

    iput-wide v4, v2, Lcom/samsung/samm/spenscrap/b/b/b;->c:D

    .line 67
    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/samm/spenscrap/b/c/c;->a(II)D

    move-result-wide v4

    mul-double/2addr v4, v0

    iput-wide v4, v2, Lcom/samsung/samm/spenscrap/b/b/b;->d:D

    .line 68
    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/samm/spenscrap/b/c/c;->a(II)D

    move-result-wide v4

    mul-double/2addr v4, v0

    iput-wide v4, v2, Lcom/samsung/samm/spenscrap/b/b/b;->e:D

    .line 69
    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/samm/spenscrap/b/c/c;->a(II)D

    move-result-wide v4

    mul-double/2addr v0, v4

    iput-wide v0, v2, Lcom/samsung/samm/spenscrap/b/b/b;->f:D

    .line 71
    invoke-virtual {v2}, Lcom/samsung/samm/spenscrap/b/b/b;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 73
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 61
    :cond_3
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_2

    :cond_4
    move-object v0, v2

    .line 75
    goto/16 :goto_0
.end method

.method public a(Ljava/util/ArrayList;Lcom/samsung/samm/spenscrap/b/b/f;)Lcom/samsung/samm/spenscrap/b/b/e;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/samm/spenscrap/b/b/f;",
            ")",
            "Lcom/samsung/samm/spenscrap/b/b/e;"
        }
    .end annotation

    .prologue
    .line 89
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 90
    :cond_0
    const/4 v6, 0x0

    .line 173
    :cond_1
    :goto_0
    return-object v6

    .line 91
    :cond_2
    const/4 v10, 0x5

    .line 92
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v2, v10, :cond_3

    .line 93
    const/4 v6, 0x0

    goto :goto_0

    .line 100
    :cond_3
    const/high16 v2, 0x41a00000    # 20.0f

    .line 101
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/samsung/samm/spenscrap/b/c/d;->a(Ljava/util/ArrayList;F)Ljava/util/ArrayList;

    move-result-object v2

    .line 102
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v3, v10, :cond_5

    :cond_4
    move-object/from16 v2, p1

    .line 104
    :cond_5
    const-string v3, "EllipseRANSAC"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Point Space Normalization : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " => "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    move-object/from16 v0, p2

    iget v3, v0, Lcom/samsung/samm/spenscrap/b/b/f;->d:F

    float-to-double v12, v3

    .line 108
    move-object/from16 v0, p2

    iget-boolean v11, v0, Lcom/samsung/samm/spenscrap/b/b/f;->a:Z

    .line 109
    move-object/from16 v0, p2

    iget-boolean v14, v0, Lcom/samsung/samm/spenscrap/b/b/f;->b:Z

    .line 110
    move-object/from16 v0, p2

    iget-boolean v4, v0, Lcom/samsung/samm/spenscrap/b/b/f;->c:Z

    .line 112
    const/4 v7, 0x0

    .line 114
    const/4 v6, 0x0

    .line 115
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    .line 117
    const/4 v3, 0x1

    .line 118
    if-eqz v4, :cond_6

    .line 119
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide v16, 0x3f847ae147ae1480L    # 0.010000000000000009

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->log(D)D

    move-result-wide v16

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    int-to-double v0, v10

    move-wide/from16 v22, v0

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v20

    sub-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->log(D)D

    move-result-wide v18

    div-double v16, v16, v18

    add-double v8, v8, v16

    double-to-int v3, v8

    .line 122
    :cond_6
    const/4 v4, 0x0

    move v9, v4

    move v4, v5

    move v5, v6

    move-object v6, v7

    :goto_1
    if-ge v9, v3, :cond_1

    .line 127
    if-eqz v11, :cond_7

    if-nez v9, :cond_7

    .line 128
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10, v14}, Lcom/samsung/samm/spenscrap/b/b/a;->b(Ljava/util/ArrayList;IZ)Ljava/util/ArrayList;

    move-result-object v7

    .line 132
    :goto_2
    if-nez v7, :cond_8

    .line 133
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 130
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10, v14}, Lcom/samsung/samm/spenscrap/b/b/a;->a(Ljava/util/ArrayList;IZ)Ljava/util/ArrayList;

    move-result-object v7

    goto :goto_2

    .line 136
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/samm/spenscrap/b/b/a;->a(Ljava/util/ArrayList;)Lcom/samsung/samm/spenscrap/b/b/c;

    move-result-object v15

    .line 137
    if-nez v15, :cond_a

    .line 122
    :cond_9
    :goto_3
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    goto :goto_1

    .line 143
    :cond_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v2, v12, v13}, Lcom/samsung/samm/spenscrap/b/b/a;->a(Lcom/samsung/samm/spenscrap/b/b/c;Ljava/util/ArrayList;D)Lcom/samsung/samm/spenscrap/b/b/e;

    move-result-object v7

    .line 144
    if-eqz v7, :cond_9

    .line 148
    const/4 v8, 0x0

    .line 149
    move-object/from16 v0, p2

    iget v0, v0, Lcom/samsung/samm/spenscrap/b/b/f;->e:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_c

    .line 150
    iget-object v0, v7, Lcom/samsung/samm/spenscrap/b/b/e;->b:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 152
    move/from16 v0, v16

    if-ge v5, v0, :cond_b

    .line 153
    const/4 v8, 0x1

    .line 163
    :cond_b
    :goto_4
    if-eqz v8, :cond_9

    .line 164
    iget-object v4, v7, Lcom/samsung/samm/spenscrap/b/b/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 165
    invoke-virtual {v7}, Lcom/samsung/samm/spenscrap/b/b/e;->a()F

    move-result v4

    .line 169
    iput-object v15, v7, Lcom/samsung/samm/spenscrap/b/b/e;->e:Lcom/samsung/samm/spenscrap/b/b/c;

    move-object v6, v7

    goto :goto_3

    .line 156
    :cond_c
    move-object/from16 v0, p2

    iget v0, v0, Lcom/samsung/samm/spenscrap/b/b/f;->e:I

    move/from16 v16, v0

    if-nez v16, :cond_b

    .line 157
    invoke-virtual {v7}, Lcom/samsung/samm/spenscrap/b/b/e;->a()F

    move-result v16

    .line 158
    cmpl-float v16, v4, v16

    if-lez v16, :cond_b

    .line 159
    const/4 v8, 0x1

    goto :goto_4
.end method
