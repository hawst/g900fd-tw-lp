.class public Lcom/samsung/samm/spenscrap/SmartClipParameter;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public mAlignDistance:I

.field public mCropStyle:I

.field public mCurveDetectDistance:I

.field public mCurveDetectRatio:I

.field public mCurveEstimationMethod:I

.field public mDegreeMaxNeighborPointNum:I

.field public mDegreeMinNeighborDistance:I

.field public mDetectSmartShape:I

.field public mEllipseDetectAverageErrorRatio:I

.field public mEllipseDetectCompleteness:I

.field public mRoundingDistance:I

.field public mSegmentPointDegreeThreshold:F

.field public mSmoothPointNum:I

.field public mTrimDistance:I

.field public mbAlignedShape:Z

.field public mbCircular:Z

.field public mbEllipseDetectorAXES:Z

.field public mbEllipseDetectorRANSAC:Z

.field public mbEllipseRANSACIncludeEndPoints:Z

.field public mbEllipseRANSACIteratvie:Z

.field public mbEllipseRANSACMinimizeErrorStrategy:Z

.field public mbLinearShape:Z

.field public mbRounding:Z

.field public mbTrim:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    const/16 v3, 0x1e

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    iput v2, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 141
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mSmoothPointNum:I

    .line 142
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDegreeMaxNeighborPointNum:I

    .line 143
    iput v3, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDegreeMinNeighborDistance:I

    .line 144
    const/high16 v0, 0x41c80000    # 25.0f

    iput v0, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mSegmentPointDegreeThreshold:F

    .line 145
    iput-boolean v1, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbCircular:Z

    .line 146
    iput-boolean v1, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbTrim:Z

    .line 147
    iput v3, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mTrimDistance:I

    .line 148
    iput-boolean v1, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    .line 149
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mRoundingDistance:I

    .line 150
    iput-boolean v2, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbLinearShape:Z

    .line 151
    iput v1, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCurveEstimationMethod:I

    .line 152
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCurveDetectRatio:I

    .line 153
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCurveDetectDistance:I

    .line 154
    iput-boolean v2, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbAlignedShape:Z

    .line 155
    iput v3, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mAlignDistance:I

    .line 156
    iput-boolean v2, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorAXES:Z

    .line 157
    iput-boolean v1, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorRANSAC:Z

    .line 158
    iput-boolean v1, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACIteratvie:Z

    .line 159
    iput-boolean v2, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACIncludeEndPoints:Z

    .line 160
    iput-boolean v1, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACMinimizeErrorStrategy:Z

    .line 161
    sget v0, Lcom/samsung/samm/spenscrap/b/c;->a:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mEllipseDetectAverageErrorRatio:I

    .line 162
    sget v0, Lcom/samsung/samm/spenscrap/b/c;->b:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mEllipseDetectCompleteness:I

    .line 165
    iput v2, p0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    .line 166
    return-void
.end method
