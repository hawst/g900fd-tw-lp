.class public Lcom/samsung/samm/spenscrap/b/c/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# instance fields
.field private final a:[[D

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput p1, p0, Lcom/samsung/samm/spenscrap/b/c/c;->b:I

    .line 102
    iput p2, p0, Lcom/samsung/samm/spenscrap/b/c/c;->c:I

    .line 103
    filled-new-array {p1, p2}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[D

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/c/c;->a:[[D

    .line 104
    return-void
.end method

.method public constructor <init>([[D)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    array-length v1, p1

    iput v1, p0, Lcom/samsung/samm/spenscrap/b/c/c;->b:I

    .line 140
    aget-object v1, p1, v0

    array-length v1, v1

    iput v1, p0, Lcom/samsung/samm/spenscrap/b/c/c;->c:I

    .line 141
    :goto_0
    iget v1, p0, Lcom/samsung/samm/spenscrap/b/c/c;->b:I

    if-lt v0, v1, :cond_0

    .line 147
    iput-object p1, p0, Lcom/samsung/samm/spenscrap/b/c/c;->a:[[D

    .line 148
    return-void

    .line 142
    :cond_0
    aget-object v1, p1, v0

    array-length v1, v1

    iget v2, p0, Lcom/samsung/samm/spenscrap/b/c/c;->c:I

    if-eq v1, v2, :cond_1

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 144
    const-string v1, "All rows must have the same length."

    .line 143
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(II)D
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/c/c;->a:[[D

    aget-object v0, v0, p1

    aget-wide v0, v0, p2

    return-wide v0
.end method

.method public a()Lcom/samsung/samm/spenscrap/b/c/c;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 229
    new-instance v3, Lcom/samsung/samm/spenscrap/b/c/c;

    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/c;->b:I

    iget v2, p0, Lcom/samsung/samm/spenscrap/b/c/c;->c:I

    invoke-direct {v3, v0, v2}, Lcom/samsung/samm/spenscrap/b/c/c;-><init>(II)V

    .line 230
    invoke-virtual {v3}, Lcom/samsung/samm/spenscrap/b/c/c;->b()[[D

    move-result-object v4

    move v0, v1

    .line 231
    :goto_0
    iget v2, p0, Lcom/samsung/samm/spenscrap/b/c/c;->b:I

    if-lt v0, v2, :cond_0

    .line 236
    return-object v3

    :cond_0
    move v2, v1

    .line 232
    :goto_1
    iget v5, p0, Lcom/samsung/samm/spenscrap/b/c/c;->c:I

    if-lt v2, v5, :cond_1

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233
    :cond_1
    aget-object v5, v4, v0

    iget-object v6, p0, Lcom/samsung/samm/spenscrap/b/c/c;->a:[[D

    aget-object v6, v6, v0

    aget-wide v6, v6, v2

    aput-wide v6, v5, v2

    .line 232
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public a(Lcom/samsung/samm/spenscrap/b/c/c;)Lcom/samsung/samm/spenscrap/b/c/c;
    .locals 1

    .prologue
    .line 968
    new-instance v0, Lcom/samsung/samm/spenscrap/b/c/a;

    invoke-direct {v0, p0}, Lcom/samsung/samm/spenscrap/b/c/a;-><init>(Lcom/samsung/samm/spenscrap/b/c/c;)V

    invoke-virtual {v0, p1}, Lcom/samsung/samm/spenscrap/b/c/a;->a(Lcom/samsung/samm/spenscrap/b/c/c;)Lcom/samsung/samm/spenscrap/b/c/c;

    move-result-object v0

    return-object v0
.end method

.method public a([III)Lcom/samsung/samm/spenscrap/b/c/c;
    .locals 8

    .prologue
    .line 443
    new-instance v2, Lcom/samsung/samm/spenscrap/b/c/c;

    array-length v0, p1

    sub-int v1, p3, p2

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v2, v0, v1}, Lcom/samsung/samm/spenscrap/b/c/c;-><init>(II)V

    .line 444
    invoke-virtual {v2}, Lcom/samsung/samm/spenscrap/b/c/c;->b()[[D

    move-result-object v3

    .line 446
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    array-length v0, p1

    if-lt v1, v0, :cond_0

    .line 454
    return-object v2

    :cond_0
    move v0, p2

    .line 447
    :goto_1
    if-le v0, p3, :cond_1

    .line 446
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 448
    :cond_1
    aget-object v4, v3, v1

    sub-int v5, v0, p2

    iget-object v6, p0, Lcom/samsung/samm/spenscrap/b/c/c;->a:[[D

    aget v7, p1, v1

    aget-object v6, v6, v7

    aget-wide v6, v6, v0

    aput-wide v6, v4, v5
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 451
    :catch_0
    move-exception v0

    .line 452
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Submatrix indices"

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()[[D
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/c/c;->a:[[D

    return-object v0
.end method

.method public c()[[D
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 265
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/c;->b:I

    iget v1, p0, Lcom/samsung/samm/spenscrap/b/c/c;->c:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[D

    move v1, v2

    .line 266
    :goto_0
    iget v3, p0, Lcom/samsung/samm/spenscrap/b/c/c;->b:I

    if-lt v1, v3, :cond_0

    .line 271
    return-object v0

    :cond_0
    move v3, v2

    .line 267
    :goto_1
    iget v4, p0, Lcom/samsung/samm/spenscrap/b/c/c;->c:I

    if-lt v3, v4, :cond_1

    .line 266
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 268
    :cond_1
    aget-object v4, v0, v1

    iget-object v5, p0, Lcom/samsung/samm/spenscrap/b/c/c;->a:[[D

    aget-object v5, v5, v1

    aget-wide v6, v5, v3

    aput-wide v6, v4, v3

    .line 267
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/samsung/samm/spenscrap/b/c/c;->a()Lcom/samsung/samm/spenscrap/b/c/c;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 313
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/c;->b:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/c/c;->c:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1090
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    move v0, v1

    .line 1091
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/samm/spenscrap/b/c/c;->d()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 1100
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v2, v1

    .line 1093
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/samm/spenscrap/b/c/c;->e()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 1097
    const-string v2, "\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1091
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1094
    :cond_1
    invoke-virtual {p0, v0, v2}, Lcom/samsung/samm/spenscrap/b/c/c;->a(II)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 1095
    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1093
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
