.class public Lcom/samsung/samm/spenscrap/ClipImageLibrary;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static final TAG:Ljava/lang/String; = "ClipImageLibrary"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/graphics/Path;)Landroid/graphics/Path;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 460
    new-instance v2, Landroid/graphics/PathMeasure;

    invoke-direct {v2, p0, v9}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    .line 462
    invoke-virtual {v2}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v0

    float-to-int v3, v0

    .line 464
    new-instance v4, Lcom/samsung/samm/spenscrap/SmartClipLibrary;

    invoke-direct {v4}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;-><init>()V

    .line 466
    const/4 v0, 0x2

    new-array v5, v0, [F

    fill-array-data v5, :array_0

    move v0, v1

    .line 468
    :goto_0
    if-le v0, v3, :cond_0

    .line 473
    invoke-virtual {v4, v1, v9}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getCustomSmartClipParameter(IZ)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    .line 474
    iput-boolean v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbTrim:Z

    .line 475
    const/high16 v1, 0x42340000    # 45.0f

    iput v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mSegmentPointDegreeThreshold:F

    .line 476
    const/4 v1, 0x5

    iput v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mSmoothPointNum:I

    .line 478
    invoke-virtual {v4, v0}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getEnhancedPath(Lcom/samsung/samm/spenscrap/SmartClipParameter;)Landroid/graphics/Path;

    move-result-object v0

    return-object v0

    .line 469
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v6

    int-to-float v7, v0

    int-to-float v8, v3

    div-float/2addr v7, v8

    mul-float/2addr v6, v7

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v5, v7}, Landroid/graphics/PathMeasure;->getPosTan(F[F[F)Z

    .line 470
    aget v6, v5, v1

    aget v7, v5, v9

    invoke-virtual {v4, v6, v7}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->addPoint(FF)V

    .line 468
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 466
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static cropImagePath(Landroid/graphics/Bitmap;Landroid/graphics/Path;Landroid/graphics/Paint;Z)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    .line 53
    if-nez p0, :cond_0

    .line 54
    const/4 v0, 0x0

    .line 153
    :goto_0
    return-object v0

    .line 56
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 60
    :cond_2
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p1, v4, v0}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 64
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 65
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 66
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 67
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 72
    if-eqz p3, :cond_7

    .line 73
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 74
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 76
    invoke-virtual {v1, p1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 77
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x0

    invoke-virtual {v1, p0, v2, v3, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 79
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->getOutlinePaths(Landroid/graphics/Bitmap;I)Ljava/util/ArrayList;

    move-result-object v2

    .line 81
    if-nez v2, :cond_3

    .line 82
    const-string v0, "ClipImageLibrary"

    const-string v1, "getOutlinePaths Error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    const/4 v0, 0x0

    goto :goto_0

    .line 86
    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 88
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_6

    .line 92
    const/4 v0, 0x4

    move v1, v0

    .line 101
    :goto_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 102
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    invoke-virtual {v8, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 104
    const/4 v0, 0x1

    move v3, v0

    :goto_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_8

    .line 108
    :cond_4
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x0

    invoke-virtual {v8, p0, v0, v3, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 112
    if-eqz p2, :cond_e

    .line 113
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    float-to-int v1, v0

    .line 115
    iget v0, v4, Landroid/graphics/RectF;->left:F

    int-to-float v3, v1

    sub-float/2addr v0, v3

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_9

    iget v0, v4, Landroid/graphics/RectF;->left:F

    int-to-float v3, v1

    sub-float/2addr v0, v3

    :goto_4
    iput v0, v4, Landroid/graphics/RectF;->left:F

    .line 116
    iget v0, v4, Landroid/graphics/RectF;->top:F

    int-to-float v3, v1

    sub-float/2addr v0, v3

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_a

    iget v0, v4, Landroid/graphics/RectF;->top:F

    int-to-float v3, v1

    sub-float/2addr v0, v3

    :goto_5
    iput v0, v4, Landroid/graphics/RectF;->top:F

    .line 117
    iget v0, v4, Landroid/graphics/RectF;->right:F

    int-to-float v3, v1

    add-float/2addr v0, v3

    int-to-float v3, v5

    cmpg-float v0, v0, v3

    if-gez v0, :cond_b

    iget v0, v4, Landroid/graphics/RectF;->right:F

    int-to-float v3, v1

    add-float/2addr v0, v3

    :goto_6
    iput v0, v4, Landroid/graphics/RectF;->right:F

    .line 118
    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v3, v1

    add-float/2addr v0, v3

    int-to-float v3, v6

    cmpg-float v0, v0, v3

    if-gez v0, :cond_c

    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_7
    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    .line 119
    sget-object v0, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {v8, v4, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z

    .line 121
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_d

    .line 147
    :cond_5
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v1, v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 148
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 149
    new-instance v2, Landroid/graphics/Rect;

    iget v3, v4, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget v5, v4, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    iget v6, v4, Landroid/graphics/RectF;->right:F

    float-to-int v6, v6

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v8

    invoke-direct {v2, v3, v5, v6, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 150
    new-instance v3, Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v4, v4

    invoke-direct {v3, v5, v6, v8, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 151
    const/4 v4, 0x0

    invoke-virtual {v1, v7, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 89
    :cond_6
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    invoke-static {v0}, Lcom/samsung/samm/spenscrap/ClipImageLibrary;->a(Landroid/graphics/Path;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 88
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 95
    :cond_7
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 96
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0, p1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    const/4 v0, 0x3

    move-object v2, v1

    move v1, v0

    goto/16 :goto_2

    .line 105
    :cond_8
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    sget-object v9, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    invoke-virtual {v8, v0, v9}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 104
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_3

    .line 115
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 116
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 117
    :cond_b
    int-to-float v0, v5

    goto/16 :goto_6

    .line 118
    :cond_c
    int-to-float v0, v6

    goto/16 :goto_7

    .line 122
    :cond_d
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    invoke-virtual {v8, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 121
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_8

    .line 129
    :cond_e
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 130
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 131
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 132
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 133
    const v0, 0x106000b

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 135
    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 136
    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 137
    int-to-float v0, v1

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 138
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 140
    const/4 v0, 0x0

    move v1, v0

    :goto_9
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 141
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    invoke-virtual {v8, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 140
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9
.end method

.method public static cropImageRect(Landroid/graphics/Bitmap;Landroid/graphics/Rect;ILandroid/graphics/Paint;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 169
    if-nez p0, :cond_0

    move-object v0, v2

    .line 229
    :goto_0
    return-object v0

    .line 172
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    .line 173
    goto :goto_0

    .line 176
    :cond_1
    if-gtz p2, :cond_2

    .line 177
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 178
    iget v1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 177
    invoke-static {p0, v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 181
    :cond_2
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 184
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 185
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 187
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 186
    invoke-static {v4, v5, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 188
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 189
    invoke-virtual {v7, p0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 192
    sget-object v0, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {v7, p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 193
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v7, v10, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 196
    if-lez p2, :cond_4

    .line 198
    iget v0, v3, Landroid/graphics/RectF;->left:F

    int-to-float v8, p2

    sub-float/2addr v0, v8

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    iget v0, v3, Landroid/graphics/RectF;->left:F

    .line 199
    int-to-float v8, p2

    sub-float/2addr v0, v8

    .line 198
    :goto_1
    iput v0, v3, Landroid/graphics/RectF;->left:F

    .line 201
    iget v0, v3, Landroid/graphics/RectF;->top:F

    int-to-float v8, p2

    sub-float/2addr v0, v8

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget v0, v3, Landroid/graphics/RectF;->top:F

    .line 202
    int-to-float v1, p2

    sub-float v1, v0, v1

    .line 201
    :cond_3
    iput v1, v3, Landroid/graphics/RectF;->top:F

    .line 204
    iget v0, v3, Landroid/graphics/RectF;->right:F

    int-to-float v1, p2

    add-float/2addr v0, v1

    int-to-float v1, v4

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    iget v0, v3, Landroid/graphics/RectF;->right:F

    .line 205
    int-to-float v1, p2

    add-float/2addr v0, v1

    .line 204
    :goto_2
    iput v0, v3, Landroid/graphics/RectF;->right:F

    .line 207
    iget v0, v3, Landroid/graphics/RectF;->bottom:F

    int-to-float v1, p2

    add-float/2addr v0, v1

    int-to-float v1, v5

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    iget v0, v3, Landroid/graphics/RectF;->bottom:F

    .line 208
    int-to-float v1, p2

    add-float/2addr v0, v1

    .line 207
    :goto_3
    iput v0, v3, Landroid/graphics/RectF;->bottom:F

    .line 212
    invoke-virtual {v7, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 214
    sget-object v0, Landroid/graphics/Region$Op;->REVERSE_DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {v7, p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 215
    invoke-virtual {v7, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 219
    :cond_4
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    .line 220
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v1, v1

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 219
    invoke-static {v0, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 221
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 222
    new-instance v4, Landroid/graphics/Rect;

    iget v5, v3, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    .line 223
    iget v7, v3, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    iget v8, v3, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    .line 224
    iget v9, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v9, v9

    .line 222
    invoke-direct {v4, v5, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 225
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v7

    float-to-int v7, v7

    .line 226
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    .line 225
    invoke-direct {v5, v10, v10, v7, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 227
    invoke-virtual {v1, v6, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 200
    goto :goto_1

    .line 206
    :cond_6
    int-to-float v0, v4

    goto :goto_2

    .line 209
    :cond_7
    int-to-float v0, v5

    goto :goto_3
.end method

.method public static makeImageOutline(Landroid/graphics/Bitmap;Landroid/graphics/Paint;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/samsung/samm/spenscrap/ClipImageLibrary;->makeImageOutline(Landroid/graphics/Bitmap;Landroid/graphics/Paint;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static makeImageOutline(Landroid/graphics/Bitmap;Landroid/graphics/Paint;Z)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 258
    if-nez p0, :cond_1

    move-object p0, v1

    .line 291
    :cond_0
    :goto_0
    return-object p0

    .line 260
    :cond_1
    if-eqz p1, :cond_0

    .line 263
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->getOutlinePath(Landroid/graphics/Bitmap;I)Landroid/graphics/Path;

    move-result-object v0

    .line 265
    if-nez v0, :cond_2

    move-object p0, v1

    goto :goto_0

    .line 267
    :cond_2
    if-eqz p2, :cond_3

    .line 268
    invoke-static {v0}, Lcom/samsung/samm/spenscrap/ClipImageLibrary;->a(Landroid/graphics/Path;)Landroid/graphics/Path;

    move-result-object v0

    .line 270
    if-nez v0, :cond_3

    move-object p0, v1

    goto :goto_0

    .line 273
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    float-to-int v3, v2

    .line 275
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 276
    mul-int/lit8 v4, v3, 0x2

    .line 275
    add-int/2addr v2, v4

    .line 276
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/lit8 v5, v3, 0x2

    add-int/2addr v4, v5

    .line 277
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 275
    invoke-static {v2, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 278
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 282
    if-eqz p1, :cond_4

    .line 284
    int-to-float v5, v3

    int-to-float v6, v3

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->offset(FF)V

    .line 285
    int-to-float v5, v3

    int-to-float v3, v3

    invoke-virtual {v4, p0, v5, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 288
    invoke-virtual {v4, v0, p1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_4
    move-object p0, v2

    .line 291
    goto :goto_0
.end method

.method public static makeImageReflect(Landroid/graphics/Bitmap;IF)Landroid/graphics/Bitmap;
    .locals 17

    .prologue
    .line 419
    if-nez p0, :cond_0

    const/4 v2, 0x0

    .line 456
    :goto_0
    return-object v2

    .line 420
    :cond_0
    if-gez p1, :cond_1

    .line 421
    const-string v2, "ClipImageLibrary"

    const-string v3, "nOffset must be > 0"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    const/4 v2, 0x0

    goto :goto_0

    .line 426
    :cond_1
    move/from16 v0, p2

    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_2

    move/from16 v0, p2

    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_3

    .line 427
    :cond_2
    const-string v2, "ClipImageLibrary"

    const-string v3, "fReflectionHeightRatio must be between 0.0 and 1.0"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    const/4 v2, 0x0

    goto :goto_0

    .line 432
    :cond_3
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 433
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    .line 435
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 436
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v7, v2, v3}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 438
    int-to-float v2, v15

    mul-float v2, v2, p2

    float-to-int v6, v2

    .line 440
    if-gtz v6, :cond_4

    const/4 v6, 0x1

    .line 442
    :cond_4
    const/4 v3, 0x0

    sub-int v4, v15, v6

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 444
    add-int v2, v15, v6

    add-int v2, v2, p1

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 446
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 447
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v4, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 448
    const/4 v4, 0x0

    add-int v6, v15, p1

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v4, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 450
    new-instance v16, Landroid/graphics/Paint;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Paint;-><init>()V

    .line 451
    new-instance v6, Landroid/graphics/LinearGradient;

    const/4 v7, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v8, v3

    const/4 v9, 0x0

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v10, v3

    const v11, 0x70ffffff

    const v12, 0xffffff

    sget-object v13, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v6 .. v13}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 452
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 453
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 454
    const/4 v3, 0x0

    int-to-float v4, v15

    int-to-float v5, v5

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    move-object/from16 v7, v16

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move-object v2, v14

    .line 456
    goto/16 :goto_0
.end method

.method public static makeImageShadow(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/16 v5, 0x64

    .line 355
    if-nez p0, :cond_0

    .line 356
    const/4 v1, 0x0

    .line 377
    :goto_0
    return-object v1

    .line 358
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 359
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 361
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v3, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 362
    new-instance v3, Landroid/graphics/Rect;

    add-int/2addr v0, p2

    add-int/2addr v1, p3

    invoke-direct {v3, p2, p3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 365
    iget v0, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->top:I

    .line 366
    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    .line 367
    iget v0, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->left:I

    .line 368
    iget v0, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->right:I

    .line 370
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 373
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v6, v5

    move v7, v5

    .line 375
    invoke-static/range {v0 .. v7}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->makeImageShadow(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIII)Z

    goto :goto_0
.end method

.method public static makeImageShadow(Landroid/graphics/Bitmap;IIIIII)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 315
    if-nez p0, :cond_0

    .line 316
    const/4 v1, 0x0

    .line 337
    :goto_0
    return-object v1

    .line 318
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 319
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 321
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v3, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 322
    new-instance v3, Landroid/graphics/Rect;

    add-int/2addr v0, p2

    add-int/2addr v1, p3

    invoke-direct {v3, p2, p3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 325
    iget v0, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->top:I

    .line 326
    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    .line 327
    iget v0, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->left:I

    .line 328
    iget v0, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, p1

    iput v0, v3, Landroid/graphics/Rect;->right:I

    .line 330
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 333
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    .line 335
    invoke-static/range {v0 .. v7}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->makeImageShadow(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIII)Z

    goto :goto_0
.end method
