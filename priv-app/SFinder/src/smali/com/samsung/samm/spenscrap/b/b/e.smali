.class public Lcom/samsung/samm/spenscrap/b/b/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public d:F

.field public e:Lcom/samsung/samm/spenscrap/b/b/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->b:Ljava/util/ArrayList;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->a:Ljava/util/ArrayList;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->c:Ljava/util/ArrayList;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->d:F

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->e:Lcom/samsung/samm/spenscrap/b/b/c;

    .line 25
    return-void
.end method


# virtual methods
.method public a()F
    .locals 2

    .prologue
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 36
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/b/e;->c:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v0

    .line 38
    :cond_1
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/b/e;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 39
    if-lez v1, :cond_0

    .line 41
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->d:F

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public a(Landroid/graphics/PointF;FZ)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->c:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->d:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->d:F

    .line 31
    if-eqz p3, :cond_0

    .line 32
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/b/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    :cond_0
    return-void
.end method
