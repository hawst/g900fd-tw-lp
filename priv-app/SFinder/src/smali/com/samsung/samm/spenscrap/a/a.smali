.class public Lcom/samsung/samm/spenscrap/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(IIII)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 549
    const/4 v0, 0x1

    .line 554
    if-gt p0, p2, :cond_0

    if-le p1, p3, :cond_2

    .line 556
    :cond_0
    if-le p0, p2, :cond_4

    .line 557
    int-to-float v0, p0

    int-to-float v2, p2

    div-float/2addr v0, v2

    .line 558
    :goto_0
    if-le p1, p3, :cond_1

    .line 559
    int-to-float v1, p1

    int-to-float v2, p3

    div-float/2addr v1, v2

    .line 561
    :cond_1
    cmpl-float v2, v0, v1

    if-ltz v2, :cond_3

    .line 564
    :goto_1
    float-to-int v0, v0

    .line 567
    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 562
    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 603
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 604
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 605
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 607
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 608
    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-object p0, v0

    .line 618
    :cond_0
    :goto_0
    return-object p0

    .line 613
    :catch_0
    move-exception v0

    .line 615
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/16 v6, 0x10e

    const/16 v5, 0x5a

    const/4 v2, 0x0

    .line 446
    invoke-static {p0}, Lcom/samsung/samm/spenscrap/a/a;->a(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    .line 447
    if-nez v3, :cond_1

    .line 448
    const/4 v0, 0x0

    .line 481
    :cond_0
    :goto_0
    return-object v0

    .line 455
    :cond_1
    if-eqz p3, :cond_5

    .line 456
    invoke-static {p0}, Lcom/samsung/samm/spenscrap/a/a;->b(Ljava/lang/String;)I

    move-result v0

    move v1, v0

    .line 459
    :goto_1
    if-eq v1, v5, :cond_2

    if-ne v1, v6, :cond_4

    .line 460
    :cond_2
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v0, v4, p1, p2}, Lcom/samsung/samm/spenscrap/a/a;->a(IIII)I

    move-result v0

    .line 470
    :goto_2
    iput-boolean v2, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 471
    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 472
    iput-boolean v2, v3, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 473
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 474
    const/4 v0, 0x1

    iput-boolean v0, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 476
    invoke-static {p0, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 477
    if-eqz p3, :cond_0

    .line 478
    if-eq v1, v5, :cond_3

    if-ne v1, v6, :cond_0

    .line 479
    :cond_3
    invoke-static {v0, v1}, Lcom/samsung/samm/spenscrap/a/a;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 463
    :cond_4
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0, v4, p1, p2}, Lcom/samsung/samm/spenscrap/a/a;->a(IIII)I

    move-result v0

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;
    .locals 2

    .prologue
    .line 502
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 503
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 505
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 507
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 41
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 43
    if-eqz v0, :cond_2

    .line 45
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x34

    if-ne v0, v1, :cond_2

    .line 47
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v3, v2

    .line 48
    const-string v7, ""

    .line 49
    new-instance v0, Landroid/content/CursorLoader;

    move-object v1, p0

    move-object v2, p1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {v0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v1

    .line 51
    if-eqz v1, :cond_4

    .line 53
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 54
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 55
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 56
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 58
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 61
    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 63
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 68
    :cond_1
    :goto_2
    return-object v0

    :cond_2
    move-object v0, v4

    goto :goto_2

    :cond_3
    move-object v0, v7

    goto :goto_0

    :cond_4
    move-object v0, v7

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 571
    .line 574
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 582
    if-eqz v1, :cond_0

    .line 583
    const-string v2, "Orientation"

    invoke-virtual {v1, v2, v3}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v1

    .line 585
    if-eq v1, v3, :cond_0

    .line 586
    packed-switch v1, :pswitch_data_0

    .line 599
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 575
    :catch_0
    move-exception v1

    .line 577
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 588
    :pswitch_1
    const/16 v0, 0x5a

    .line 589
    goto :goto_0

    .line 591
    :pswitch_2
    const/16 v0, 0xb4

    .line 592
    goto :goto_0

    .line 594
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 586
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
