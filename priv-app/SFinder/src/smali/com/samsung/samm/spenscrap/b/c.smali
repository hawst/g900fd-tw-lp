.class public Lcom/samsung/samm/spenscrap/b/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:F

.field public static b:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const v0, 0x3dcccccd    # 0.1f

    sput v0, Lcom/samsung/samm/spenscrap/b/c;->a:F

    .line 19
    const v0, 0x3f4ccccd    # 0.8f

    sput v0, Lcom/samsung/samm/spenscrap/b/c;->b:F

    .line 20
    return-void
.end method

.method public static a(Ljava/util/ArrayList;)Lcom/samsung/samm/spenscrap/b/b;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;)",
            "Lcom/samsung/samm/spenscrap/b/b;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 146
    if-nez p0, :cond_0

    move-object v0, v1

    .line 169
    :goto_0
    return-object v0

    .line 148
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 149
    if-nez v0, :cond_1

    move-object v0, v1

    .line 150
    goto :goto_0

    .line 152
    :cond_1
    new-instance v0, Lcom/samsung/samm/spenscrap/b/b;

    invoke-direct {v0}, Lcom/samsung/samm/spenscrap/b/b;-><init>()V

    .line 155
    invoke-static {p0}, Lcom/samsung/samm/spenscrap/b/c/d;->a(Ljava/util/ArrayList;)Landroid/graphics/RectF;

    move-result-object v2

    .line 156
    if-nez v2, :cond_2

    move-object v0, v1

    .line 157
    goto :goto_0

    .line 160
    :cond_2
    const/4 v4, 0x0

    .line 162
    new-instance v3, Landroid/graphics/PointF;

    iget v1, v2, Landroid/graphics/RectF;->right:F

    iget v5, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v5

    div-float/2addr v1, v7

    iget v5, v2, Landroid/graphics/RectF;->top:F

    iget v6, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v5, v6

    div-float/2addr v5, v7

    invoke-direct {v3, v1, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 165
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v1, v7

    .line 166
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v7

    .line 167
    const/4 v6, 0x1

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/samm/spenscrap/b/b;->a(FFLandroid/graphics/PointF;FLjava/util/ArrayList;Z)Z

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;Lcom/samsung/samm/spenscrap/b/b/f;Z)Lcom/samsung/samm/spenscrap/b/b;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/samm/spenscrap/b/b/f;",
            "Z)",
            "Lcom/samsung/samm/spenscrap/b/b;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 91
    if-nez p0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-object v0

    .line 93
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 94
    if-eqz v1, :cond_0

    .line 97
    new-instance v1, Lcom/samsung/samm/spenscrap/b/b/a;

    invoke-direct {v1}, Lcom/samsung/samm/spenscrap/b/b/a;-><init>()V

    .line 98
    invoke-virtual {v1, p0, p1}, Lcom/samsung/samm/spenscrap/b/b/a;->a(Ljava/util/ArrayList;Lcom/samsung/samm/spenscrap/b/b/f;)Lcom/samsung/samm/spenscrap/b/b/e;

    move-result-object v1

    .line 99
    if-eqz v1, :cond_0

    .line 101
    iget-object v0, v1, Lcom/samsung/samm/spenscrap/b/b/e;->e:Lcom/samsung/samm/spenscrap/b/b/c;

    move-object v2, v0

    check-cast v2, Lcom/samsung/samm/spenscrap/b/b/b;

    .line 106
    iget-object v0, v1, Lcom/samsung/samm/spenscrap/b/b/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 107
    const-string v1, "EllipseLibrary"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ellipse(RANSAC) Inlier num = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    new-instance v0, Lcom/samsung/samm/spenscrap/b/b;

    invoke-direct {v0}, Lcom/samsung/samm/spenscrap/b/b;-><init>()V

    .line 117
    iget-wide v4, v2, Lcom/samsung/samm/spenscrap/b/b/b;->k:D

    const-wide v6, 0x4066800000000000L    # 180.0

    mul-double/2addr v4, v6

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v4, v6

    double-to-float v1, v4

    .line 119
    const/4 v3, 0x0

    cmpg-float v3, v1, v3

    if-gez v3, :cond_2

    .line 120
    neg-float v4, v1

    .line 126
    :goto_1
    new-instance v3, Landroid/graphics/PointF;

    iget-wide v6, v2, Lcom/samsung/samm/spenscrap/b/b/b;->g:D

    double-to-float v1, v6

    iget-wide v6, v2, Lcom/samsung/samm/spenscrap/b/b/b;->h:D

    double-to-float v5, v6

    invoke-direct {v3, v1, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 129
    iget-wide v6, v2, Lcom/samsung/samm/spenscrap/b/b/b;->i:D

    double-to-float v1, v6

    .line 130
    iget-wide v6, v2, Lcom/samsung/samm/spenscrap/b/b/b;->j:D

    double-to-float v2, v6

    move-object v5, p0

    move v6, p2

    .line 131
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/samm/spenscrap/b/b;->a(FFLandroid/graphics/PointF;FLjava/util/ArrayList;Z)Z

    goto :goto_0

    .line 122
    :cond_2
    const/high16 v3, 0x43b40000    # 360.0f

    sub-float v4, v3, v1

    goto :goto_1
.end method

.method public static a(Ljava/util/ArrayList;Z)Lcom/samsung/samm/spenscrap/b/b;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;Z)",
            "Lcom/samsung/samm/spenscrap/b/b;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 30
    if-nez p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-object v0

    .line 32
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 33
    if-eqz v10, :cond_0

    move v3, v6

    move v5, v6

    move v8, v6

    move v9, v7

    .line 40
    :goto_1
    if-lt v3, v10, :cond_2

    move v4, v6

    move v2, v7

    .line 53
    :goto_2
    if-lt v4, v10, :cond_4

    .line 60
    new-instance v0, Lcom/samsung/samm/spenscrap/b/b;

    invoke-direct {v0}, Lcom/samsung/samm/spenscrap/b/b;-><init>()V

    .line 63
    const/high16 v4, 0x43b40000    # 360.0f

    invoke-virtual {p0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    const/4 v6, 0x1

    invoke-static {v1, v3, v6}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v1

    sub-float/2addr v4, v1

    .line 65
    invoke-virtual {p0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 66
    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Landroid/graphics/PointF;

    .line 67
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 68
    iget v6, v1, Landroid/graphics/PointF;->x:F

    iget v7, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v7

    div-float/2addr v6, v11

    iput v6, v3, Landroid/graphics/PointF;->x:F

    .line 69
    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v5, v5, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v5

    div-float/2addr v1, v11

    iput v1, v3, Landroid/graphics/PointF;->y:F

    .line 71
    div-float v1, v9, v11

    move-object v5, p0

    move v6, p1

    .line 73
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/samm/spenscrap/b/b;->a(FFLandroid/graphics/PointF;FLjava/util/ArrayList;Z)Z

    goto :goto_0

    :cond_2
    move v2, v5

    move v4, v8

    move v8, v3

    move v5, v9

    .line 41
    :goto_3
    add-int/lit8 v0, v10, -0x1

    if-lt v8, v0, :cond_3

    .line 40
    add-int/lit8 v3, v3, 0x1

    move v8, v4

    move v9, v5

    move v5, v2

    goto :goto_1

    .line 42
    :cond_3
    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    add-int/lit8 v1, v8, 0x1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    invoke-static {v0, v1, v6}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v1

    .line 43
    cmpl-float v0, v1, v5

    if-lez v0, :cond_6

    .line 46
    add-int/lit8 v0, v8, 0x1

    move v2, v1

    move v1, v3

    .line 41
    :goto_4
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v5, v2

    move v4, v1

    move v2, v0

    goto :goto_3

    .line 54
    :cond_4
    invoke-virtual {p0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    invoke-static {v0, v1, v3}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v0

    .line 55
    cmpl-float v1, v0, v2

    if-lez v1, :cond_5

    move v2, v0

    .line 53
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_2

    :cond_6
    move v0, v2

    move v1, v4

    move v2, v5

    goto :goto_4
.end method
