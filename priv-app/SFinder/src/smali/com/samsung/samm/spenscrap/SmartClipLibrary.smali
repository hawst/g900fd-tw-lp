.class public Lcom/samsung/samm/spenscrap/SmartClipLibrary;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final CURVE_ESTIMATION_BY_BEZIER:I = 0x1

.field public static final CURVE_ESTIMATION_BY_DIVIDE:I = 0x0

.field public static final CURVE_ESTIMATION_BY_ELLIPSE:I = 0x2

.field protected static final ENHANCE_SPEED:Z = false

.field public static final SIMPLE_CLIP_ORIGINAL:I = 0x0

.field public static final SIMPLE_CLIP_SHAPE_ELLIPSE:I = 0x1

.field public static final SIMPLE_CLIP_SHAPE_RECT:I = 0x2

.field public static final SMART_CLIP_NONE:I = -0x1

.field public static final SMART_CLIP_OUTLINE_ENHANCE:I = 0x0

.field public static final SMART_CLIP_SHAPE_AUTO:I = 0x1

.field public static final SMART_CLIP_SHAPE_COMBIND:I = 0x5

.field public static final SMART_CLIP_SHAPE_ELLIPSE:I = 0x2

.field public static final SMART_CLIP_SHAPE_POLYGON:I = 0x4

.field public static final SMART_CLIP_SHAPE_RECT:I = 0x3

.field protected static final TAG:Ljava/lang/String; = "SmartClipLibrary"

.field protected static mbForceGenerateShape:Z


# instance fields
.field private a:Lcom/samsung/samm/spenscrap/SmartClipParameter;

.field private b:Lcom/samsung/samm/spenscrap/SmartClipParameter;

.field private c:Lcom/samsung/samm/spenscrap/SmartClipParameter;

.field private d:Lcom/samsung/samm/spenscrap/SmartClipParameter;

.field private e:Lcom/samsung/samm/spenscrap/SmartClipParameter;

.field private f:Lcom/samsung/samm/spenscrap/SmartClipParameter;

.field protected mPoints:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mbForceGenerateShape:Z

    .line 115
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    .line 128
    iput-object v1, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->a:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 129
    iput-object v1, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->b:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 130
    iput-object v1, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->c:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 131
    iput-object v1, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->d:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 132
    iput-object v1, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->e:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 133
    iput-object v1, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->f:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 139
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;III)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;III)I"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 1650
    invoke-virtual {p2, p5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    invoke-virtual {p2, p6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v0

    .line 1652
    int-to-float v1, p4

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1653
    invoke-virtual {p3, p5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1654
    invoke-virtual {p3, p6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1656
    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    .line 1657
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, p5, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1668
    :cond_0
    :goto_0
    return p6

    .line 1662
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, p6, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move p6, p5

    .line 1664
    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;IIFI)Lcom/samsung/samm/spenscrap/b/a;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;IIFI)",
            "Lcom/samsung/samm/spenscrap/b/a;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 532
    if-nez p1, :cond_0

    move-object v0, v3

    .line 582
    :goto_0
    return-object v0

    .line 534
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 536
    if-le p3, p2, :cond_1

    .line 537
    sub-int v0, p3, p2

    add-int/lit8 v0, v0, 0x1

    .line 540
    :goto_1
    const/4 v1, 0x2

    if-gt v0, v1, :cond_2

    move-object v0, v3

    .line 541
    goto :goto_0

    .line 539
    :cond_1
    sub-int v0, v7, p2

    add-int/lit8 v0, v0, -0x1

    add-int/2addr v0, p3

    goto :goto_1

    .line 543
    :cond_2
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 544
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 546
    new-instance v4, Lcom/samsung/samm/spenscrap/b/a;

    invoke-direct {v4}, Lcom/samsung/samm/spenscrap/b/a;-><init>()V

    .line 548
    if-le p3, p2, :cond_7

    .line 549
    add-int/lit8 v2, p2, 0x1

    move v5, v2

    :goto_2
    if-lt v5, p3, :cond_5

    .line 576
    :cond_3
    invoke-static {v0, v1, v6}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v0

    .line 577
    invoke-virtual {v4}, Lcom/samsung/samm/spenscrap/b/a;->a()F

    move-result v1

    .line 578
    mul-float/2addr v0, p4

    .line 579
    cmpl-float v0, v1, v0

    if-gtz v0, :cond_4

    int-to-float v0, p5

    cmpl-float v0, v1, v0

    if-lez v0, :cond_b

    :cond_4
    move-object v0, v4

    .line 580
    goto :goto_0

    .line 550
    :cond_5
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-static {v0, v1, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    .line 551
    invoke-virtual {v4}, Lcom/samsung/samm/spenscrap/b/a;->a()F

    move-result v7

    cmpl-float v7, v2, v7

    if-lez v7, :cond_6

    .line 552
    invoke-virtual {v4, v2}, Lcom/samsung/samm/spenscrap/b/a;->a(F)V

    .line 553
    invoke-virtual {v4, v5}, Lcom/samsung/samm/spenscrap/b/a;->a(I)V

    .line 549
    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 558
    :cond_7
    add-int/lit8 v2, p2, 0x1

    move v5, v2

    :goto_3
    if-lt v5, v7, :cond_9

    move v5, v6

    .line 565
    :goto_4
    if-ge v5, p3, :cond_3

    .line 566
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-static {v0, v1, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    .line 567
    invoke-virtual {v4}, Lcom/samsung/samm/spenscrap/b/a;->a()F

    move-result v7

    cmpl-float v7, v2, v7

    if-lez v7, :cond_8

    .line 568
    invoke-virtual {v4, v2}, Lcom/samsung/samm/spenscrap/b/a;->a(F)V

    .line 569
    invoke-virtual {v4, v5}, Lcom/samsung/samm/spenscrap/b/a;->a(I)V

    .line 565
    :cond_8
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_4

    .line 559
    :cond_9
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-static {v0, v1, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    .line 560
    invoke-virtual {v4}, Lcom/samsung/samm/spenscrap/b/a;->a()F

    move-result v8

    cmpl-float v8, v2, v8

    if-lez v8, :cond_a

    .line 561
    invoke-virtual {v4, v2}, Lcom/samsung/samm/spenscrap/b/a;->a(F)V

    .line 562
    invoke-virtual {v4, v5}, Lcom/samsung/samm/spenscrap/b/a;->a(I)V

    .line 558
    :cond_a
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    :cond_b
    move-object v0, v3

    .line 582
    goto/16 :goto_0
.end method

.method public static isForceGenerateShape()Z
    .locals 1

    .prologue
    .line 302
    sget-boolean v0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mbForceGenerateShape:Z

    return v0
.end method

.method public static setForceGenerateShape(Z)V
    .locals 0

    .prologue
    .line 311
    sput-boolean p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mbForceGenerateShape:Z

    .line 312
    return-void
.end method


# virtual methods
.method protected AlignSegmentPoint(Ljava/util/ArrayList;ZF)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/samm/spenscrap/b/d;",
            ">;ZF)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1024
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1033
    new-array v6, v5, [Lcom/samsung/a/a/c;

    move v4, v1

    .line 1034
    :goto_0
    if-lt v4, v5, :cond_3

    .line 1051
    new-instance v7, Lcom/samsung/a/a/e;

    invoke-direct {v7}, Lcom/samsung/a/a/e;-><init>()V

    .line 1052
    new-instance v8, Lcom/samsung/a/a/d;

    invoke-direct {v8}, Lcom/samsung/a/a/d;-><init>()V

    .line 1059
    iput v3, v8, Lcom/samsung/a/a/d;->a:I

    .line 1060
    iput v5, v8, Lcom/samsung/a/a/d;->b:I

    .line 1061
    iput v5, v8, Lcom/samsung/a/a/d;->d:I

    .line 1062
    const/16 v0, 0xa

    iput v0, v8, Lcom/samsung/a/a/d;->c:I

    .line 1063
    iput v1, v8, Lcom/samsung/a/a/d;->e:I

    .line 1064
    iput p3, v8, Lcom/samsung/a/a/d;->g:F

    .line 1065
    const v0, 0x3c23d70a    # 0.01f

    iput v0, v8, Lcom/samsung/a/a/d;->f:F

    .line 1066
    iput-boolean v1, v8, Lcom/samsung/a/a/d;->h:Z

    .line 1067
    iput-boolean v1, v8, Lcom/samsung/a/a/d;->i:Z

    .line 1068
    iput-boolean v1, v8, Lcom/samsung/a/a/d;->j:Z

    .line 1070
    iput v1, v8, Lcom/samsung/a/a/d;->k:I

    .line 1071
    const/4 v0, 0x0

    iput v0, v8, Lcom/samsung/a/a/d;->l:F

    .line 1072
    iput-boolean v1, v8, Lcom/samsung/a/a/d;->m:Z

    .line 1075
    invoke-virtual {v7, v8}, Lcom/samsung/a/a/e;->a(Lcom/samsung/a/a/d;)Z

    move v0, v1

    move v2, v1

    .line 1081
    :goto_1
    iget v4, v8, Lcom/samsung/a/a/d;->c:I

    if-lt v2, v4, :cond_6

    :cond_0
    move v2, v1

    .line 1123
    :goto_2
    if-lt v2, v5, :cond_b

    move v2, v3

    .line 1140
    :goto_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_d

    .line 1150
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1151
    if-le v2, v3, :cond_1

    .line 1152
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    iget-object v1, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1153
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    iget-object v0, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1154
    iget v4, v1, Landroid/graphics/PointF;->x:F

    iget v5, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v1, v0

    if-nez v0, :cond_1

    .line 1155
    const-string v0, "SmartClipLibrary"

    const-string v1, "Remove Redundant Point(Last)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1163
    :cond_1
    invoke-virtual {v7}, Lcom/samsung/a/a/e;->a()Z

    move v1, v3

    .line 1165
    :cond_2
    return v1

    .line 1035
    :cond_3
    new-instance v0, Lcom/samsung/a/a/c;

    invoke-direct {v0}, Lcom/samsung/a/a/c;-><init>()V

    aput-object v0, v6, v4

    .line 1036
    aget-object v0, v6, v4

    invoke-virtual {v0, v3}, Lcom/samsung/a/a/c;->a(I)Z

    move v2, v1

    .line 1037
    :goto_4
    if-lt v2, v3, :cond_4

    .line 1034
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 1038
    :cond_4
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    .line 1039
    if-eqz p2, :cond_5

    .line 1040
    aget-object v7, v6, v4

    iget-object v7, v7, Lcom/samsung/a/a/c;->b:[F

    iget-object v0, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    aput v0, v7, v2

    .line 1037
    :goto_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1042
    :cond_5
    aget-object v7, v6, v4

    iget-object v7, v7, Lcom/samsung/a/a/c;->b:[F

    iget-object v0, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aput v0, v7, v2

    goto :goto_5

    .line 1084
    :cond_6
    invoke-virtual {v7}, Lcom/samsung/a/a/e;->c()V

    move v4, v1

    .line 1087
    :goto_6
    if-lt v4, v5, :cond_9

    .line 1096
    :goto_7
    if-nez v0, :cond_0

    .line 1098
    iget-boolean v4, v8, Lcom/samsung/a/a/d;->h:Z

    if-eqz v4, :cond_7

    .line 1100
    invoke-virtual {v7}, Lcom/samsung/a/a/e;->d()I

    .line 1104
    :cond_7
    iget-boolean v4, v8, Lcom/samsung/a/a/d;->i:Z

    if-eqz v4, :cond_8

    .line 1106
    invoke-virtual {v7}, Lcom/samsung/a/a/e;->b()F

    move-result v4

    .line 1107
    iget-object v9, v7, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v9, v9, Lcom/samsung/a/a/d;->f:F

    cmpg-float v4, v4, v9

    if-lez v4, :cond_0

    .line 1081
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 1090
    :cond_9
    aget-object v9, v6, v4

    invoke-virtual {v7, v9, v4}, Lcom/samsung/a/a/e;->a(Lcom/samsung/a/a/c;I)Z

    move-result v9

    if-nez v9, :cond_a

    move v0, v3

    .line 1093
    goto :goto_7

    .line 1087
    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 1125
    :cond_b
    aget-object v0, v6, v2

    invoke-virtual {v7, v0}, Lcom/samsung/a/a/e;->a(Lcom/samsung/a/a/c;)Lcom/samsung/a/a/b;

    move-result-object v4

    .line 1126
    if-eqz v4, :cond_2

    .line 1128
    iget v0, v4, Lcom/samsung/a/a/b;->a:I

    const/4 v8, -0x1

    if-eq v0, v8, :cond_2

    .line 1131
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    .line 1132
    if-eqz p2, :cond_c

    .line 1133
    iget-object v0, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    iget-object v8, v7, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    iget v4, v4, Lcom/samsung/a/a/b;->a:I

    aget-object v4, v8, v4

    iget-object v4, v4, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    iget-object v4, v4, Lcom/samsung/a/a/c;->b:[F

    aget v4, v4, v1

    iput v4, v0, Landroid/graphics/PointF;->x:F

    .line 1123
    :goto_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_2

    .line 1135
    :cond_c
    iget-object v0, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    iget-object v8, v7, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    iget v4, v4, Lcom/samsung/a/a/b;->a:I

    aget-object v4, v8, v4

    iget-object v4, v4, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    iget-object v4, v4, Lcom/samsung/a/a/c;->b:[F

    aget v4, v4, v1

    iput v4, v0, Landroid/graphics/PointF;->y:F

    goto :goto_8

    .line 1141
    :cond_d
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    iget-object v4, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1142
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    iget-object v0, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1143
    iget v5, v4, Landroid/graphics/PointF;->x:F

    iget v6, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_e

    iget v4, v4, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v4, v0

    if-nez v0, :cond_e

    .line 1144
    const-string v0, "SmartClipLibrary"

    const-string v4, "Remove Redundant Point"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1146
    add-int/lit8 v2, v2, -0x1

    .line 1140
    :cond_e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_3
.end method

.method public addPoint(FF)V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    return-void
.end method

.method protected findNextIndex(ILjava/util/ArrayList;IFZ)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;IFZ)I"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 2494
    if-nez p2, :cond_1

    .line 2527
    :cond_0
    :goto_0
    return v0

    .line 2496
    :cond_1
    if-ltz p1, :cond_0

    .line 2498
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 2499
    if-lez v4, :cond_0

    .line 2501
    add-int/lit8 v1, v4, -0x1

    if-le p3, v1, :cond_2

    .line 2502
    add-int/lit8 p3, v4, -0x1

    .line 2504
    :cond_2
    add-int v5, p1, p3

    .line 2506
    const/4 v1, 0x0

    .line 2507
    add-int/lit8 v2, p1, 0x1

    move v3, v1

    :goto_1
    if-gt v2, v5, :cond_0

    .line 2508
    if-lt v2, v4, :cond_4

    .line 2509
    if-eqz p5, :cond_3

    .line 2510
    sub-int v1, v2, v4

    .line 2511
    int-to-float v3, v3

    sub-int v0, v2, v4

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v3

    float-to-int v0, v0

    move v6, v0

    move v0, v1

    move v1, v6

    .line 2523
    :goto_2
    int-to-float v3, v1

    cmpl-float v3, v3, p4

    if-gtz v3, :cond_0

    .line 2507
    add-int/lit8 v2, v2, 0x1

    move v3, v1

    goto :goto_1

    .line 2514
    :cond_3
    add-int/lit8 v0, v4, -0x1

    .line 2515
    goto :goto_0

    .line 2520
    :cond_4
    int-to-float v1, v3

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    move v1, v0

    move v0, v2

    goto :goto_2
.end method

.method protected findPreIndex(ILjava/util/ArrayList;IFZ)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;IFZ)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 2397
    if-nez p2, :cond_1

    .line 2433
    :cond_0
    :goto_0
    return v0

    .line 2399
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 2400
    if-lez v4, :cond_0

    .line 2402
    if-ltz p1, :cond_0

    .line 2404
    add-int/lit8 v2, v4, -0x1

    if-le p3, v2, :cond_2

    .line 2405
    add-int/lit8 p3, v4, -0x1

    .line 2407
    :cond_2
    sub-int v5, p1, p3

    move v3, v1

    .line 2410
    :goto_1
    if-lt p1, v5, :cond_0

    .line 2411
    add-int/lit8 v0, p1, -0x1

    if-gez v0, :cond_4

    .line 2412
    if-eqz p5, :cond_3

    .line 2413
    add-int v0, p1, v4

    add-int/lit8 v2, v0, -0x1

    .line 2424
    :goto_2
    add-int/lit8 v0, v2, 0x1

    if-lt v0, v4, :cond_5

    .line 2425
    int-to-float v3, v3

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v3

    float-to-int v0, v0

    .line 2429
    :goto_3
    int-to-float v3, v0

    cmpl-float v3, v3, p4

    if-lez v3, :cond_6

    move v0, v2

    .line 2430
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2417
    goto :goto_0

    .line 2421
    :cond_4
    add-int/lit8 v2, p1, -0x1

    goto :goto_2

    .line 2427
    :cond_5
    int-to-float v3, v3

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v3

    float-to-int v0, v0

    goto :goto_3

    .line 2410
    :cond_6
    add-int/lit8 p1, p1, -0x1

    move v3, v0

    move v0, v2

    goto :goto_1
.end method

.method protected findSafeNextIndex(ILjava/util/ArrayList;Ljava/util/ArrayList;IFZ)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;IFZ)I"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, -0x1

    .line 2534
    if-nez p2, :cond_1

    move p1, v1

    .line 2576
    :cond_0
    :goto_0
    return p1

    .line 2536
    :cond_1
    if-gez p1, :cond_2

    move p1, v1

    .line 2537
    goto :goto_0

    .line 2538
    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 2539
    if-gtz v5, :cond_3

    move p1, v1

    .line 2540
    goto :goto_0

    .line 2541
    :cond_3
    add-int/lit8 v0, v5, -0x1

    if-le p4, v0, :cond_4

    .line 2542
    add-int/lit8 p4, v5, -0x1

    .line 2544
    :cond_4
    add-int v6, p1, p4

    .line 2546
    const/4 v2, 0x0

    .line 2547
    add-int/lit8 v3, p1, 0x1

    move v4, v2

    move v2, v1

    :goto_1
    if-le v3, v6, :cond_6

    move v0, v2

    .line 2574
    :cond_5
    :goto_2
    if-eq v0, v1, :cond_0

    move p1, v0

    goto :goto_0

    .line 2549
    :cond_6
    if-lt v3, v5, :cond_a

    .line 2550
    if-eqz p6, :cond_8

    .line 2551
    sub-int v0, v3, v5

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v7, :cond_7

    move v0, v2

    .line 2552
    goto :goto_2

    .line 2553
    :cond_7
    sub-int v2, v3, v5

    .line 2554
    int-to-float v4, v4

    sub-int v0, v3, v5

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v4

    float-to-int v0, v0

    move v8, v0

    move v0, v2

    move v2, v8

    .line 2570
    :goto_3
    int-to-float v4, v2

    cmpl-float v4, v4, p5

    if-gtz v4, :cond_5

    .line 2547
    add-int/lit8 v3, v3, 0x1

    move v4, v2

    move v2, v0

    goto :goto_1

    .line 2557
    :cond_8
    add-int/lit8 v0, v5, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v7, :cond_9

    move v0, v2

    .line 2558
    goto :goto_2

    .line 2559
    :cond_9
    add-int/lit8 v0, v5, -0x1

    .line 2560
    goto :goto_2

    .line 2564
    :cond_a
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v7, :cond_b

    move v0, v2

    .line 2565
    goto :goto_2

    .line 2567
    :cond_b
    int-to-float v2, v4

    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v2

    float-to-int v0, v0

    move v2, v0

    move v0, v3

    goto :goto_3
.end method

.method protected findSafePreIndex(ILjava/util/ArrayList;Ljava/util/ArrayList;IFZ)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;IFZ)I"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 2440
    if-nez p2, :cond_1

    move p1, v1

    .line 2488
    :cond_0
    :goto_0
    return p1

    .line 2442
    :cond_1
    if-gez p1, :cond_2

    move p1, v1

    .line 2443
    goto :goto_0

    .line 2444
    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_3

    move p1, v1

    .line 2445
    goto :goto_0

    .line 2447
    :cond_3
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 2448
    if-gtz v6, :cond_4

    move p1, v1

    .line 2449
    goto :goto_0

    .line 2450
    :cond_4
    add-int/lit8 v0, v6, -0x1

    if-le p4, v0, :cond_5

    .line 2451
    add-int/lit8 p4, v6, -0x1

    .line 2453
    :cond_5
    sub-int v7, p1, p4

    move v4, p1

    move v5, v3

    move v2, v1

    .line 2456
    :goto_1
    if-ge v4, v7, :cond_6

    move v0, v2

    .line 2486
    :goto_2
    if-eq v0, v1, :cond_0

    move p1, v0

    goto :goto_0

    .line 2458
    :cond_6
    add-int/lit8 v0, v4, -0x1

    if-gez v0, :cond_a

    .line 2459
    if-eqz p6, :cond_8

    .line 2460
    add-int v0, v4, v6

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v8, :cond_7

    move v0, v2

    .line 2461
    goto :goto_2

    .line 2462
    :cond_7
    add-int v0, v4, v6

    add-int/lit8 v2, v0, -0x1

    .line 2477
    :goto_3
    add-int/lit8 v0, v2, 0x1

    if-lt v0, v6, :cond_c

    .line 2478
    int-to-float v5, v5

    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v5

    float-to-int v0, v0

    .line 2482
    :goto_4
    int-to-float v5, v0

    cmpl-float v5, v5, p5

    if-lez v5, :cond_d

    move v0, v2

    .line 2483
    goto :goto_2

    .line 2465
    :cond_8
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v8, :cond_9

    move v0, v2

    .line 2466
    goto :goto_2

    :cond_9
    move v0, v3

    .line 2468
    goto :goto_2

    .line 2472
    :cond_a
    add-int/lit8 v0, v4, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v8, :cond_b

    move v0, v2

    .line 2473
    goto :goto_2

    .line 2474
    :cond_b
    add-int/lit8 v2, v4, -0x1

    goto :goto_3

    .line 2480
    :cond_c
    int-to-float v5, v5

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v5

    float-to-int v0, v0

    goto :goto_4

    .line 2456
    :cond_d
    add-int/lit8 v4, v4, -0x1

    move v5, v0

    goto :goto_1
.end method

.method protected getAlignPoint(Ljava/util/ArrayList;ZF)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;ZF)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1170
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1179
    new-array v6, v5, [Lcom/samsung/a/a/c;

    move v4, v1

    .line 1180
    :goto_0
    if-lt v4, v5, :cond_2

    .line 1196
    new-instance v7, Lcom/samsung/a/a/e;

    invoke-direct {v7}, Lcom/samsung/a/a/e;-><init>()V

    .line 1197
    new-instance v8, Lcom/samsung/a/a/d;

    invoke-direct {v8}, Lcom/samsung/a/a/d;-><init>()V

    .line 1204
    iput v3, v8, Lcom/samsung/a/a/d;->a:I

    .line 1205
    iput v5, v8, Lcom/samsung/a/a/d;->b:I

    .line 1206
    iput v5, v8, Lcom/samsung/a/a/d;->d:I

    .line 1207
    const/16 v0, 0xa

    iput v0, v8, Lcom/samsung/a/a/d;->c:I

    .line 1208
    iput v1, v8, Lcom/samsung/a/a/d;->e:I

    .line 1209
    iput p3, v8, Lcom/samsung/a/a/d;->g:F

    .line 1210
    const v0, 0x3c23d70a    # 0.01f

    iput v0, v8, Lcom/samsung/a/a/d;->f:F

    .line 1211
    iput-boolean v1, v8, Lcom/samsung/a/a/d;->h:Z

    .line 1212
    iput-boolean v1, v8, Lcom/samsung/a/a/d;->i:Z

    .line 1213
    iput-boolean v1, v8, Lcom/samsung/a/a/d;->j:Z

    .line 1215
    iput v1, v8, Lcom/samsung/a/a/d;->k:I

    .line 1216
    const/4 v0, 0x0

    iput v0, v8, Lcom/samsung/a/a/d;->l:F

    .line 1217
    iput-boolean v1, v8, Lcom/samsung/a/a/d;->m:Z

    .line 1220
    invoke-virtual {v7, v8}, Lcom/samsung/a/a/e;->a(Lcom/samsung/a/a/d;)Z

    move v0, v1

    move v2, v1

    .line 1226
    :goto_1
    iget v4, v8, Lcom/samsung/a/a/d;->c:I

    if-lt v2, v4, :cond_5

    :cond_0
    move v2, v1

    .line 1268
    :goto_2
    if-lt v2, v5, :cond_a

    .line 1286
    invoke-virtual {v7}, Lcom/samsung/a/a/e;->a()Z

    move v1, v3

    .line 1288
    :cond_1
    return v1

    .line 1181
    :cond_2
    new-instance v0, Lcom/samsung/a/a/c;

    invoke-direct {v0}, Lcom/samsung/a/a/c;-><init>()V

    aput-object v0, v6, v4

    .line 1182
    aget-object v0, v6, v4

    invoke-virtual {v0, v3}, Lcom/samsung/a/a/c;->a(I)Z

    move v2, v1

    .line 1183
    :goto_3
    if-lt v2, v3, :cond_3

    .line 1180
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 1184
    :cond_3
    if-eqz p2, :cond_4

    .line 1185
    aget-object v0, v6, v4

    iget-object v7, v0, Lcom/samsung/a/a/c;->b:[F

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    aput v0, v7, v2

    .line 1183
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1187
    :cond_4
    aget-object v0, v6, v4

    iget-object v7, v0, Lcom/samsung/a/a/c;->b:[F

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aput v0, v7, v2

    goto :goto_4

    .line 1229
    :cond_5
    invoke-virtual {v7}, Lcom/samsung/a/a/e;->c()V

    move v4, v1

    .line 1232
    :goto_5
    if-lt v4, v5, :cond_8

    .line 1241
    :goto_6
    if-nez v0, :cond_0

    .line 1243
    iget-boolean v4, v8, Lcom/samsung/a/a/d;->h:Z

    if-eqz v4, :cond_6

    .line 1245
    invoke-virtual {v7}, Lcom/samsung/a/a/e;->d()I

    .line 1249
    :cond_6
    iget-boolean v4, v8, Lcom/samsung/a/a/d;->i:Z

    if-eqz v4, :cond_7

    .line 1251
    invoke-virtual {v7}, Lcom/samsung/a/a/e;->b()F

    move-result v4

    .line 1252
    iget-object v9, v7, Lcom/samsung/a/a/e;->a:Lcom/samsung/a/a/d;

    iget v9, v9, Lcom/samsung/a/a/d;->f:F

    cmpg-float v4, v4, v9

    if-lez v4, :cond_0

    .line 1226
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1235
    :cond_8
    aget-object v9, v6, v4

    invoke-virtual {v7, v9, v4}, Lcom/samsung/a/a/e;->a(Lcom/samsung/a/a/c;I)Z

    move-result v9

    if-nez v9, :cond_9

    move v0, v3

    .line 1238
    goto :goto_6

    .line 1232
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 1270
    :cond_a
    aget-object v0, v6, v2

    invoke-virtual {v7, v0}, Lcom/samsung/a/a/e;->a(Lcom/samsung/a/a/c;)Lcom/samsung/a/a/b;

    move-result-object v4

    .line 1271
    if-eqz v4, :cond_1

    .line 1273
    iget v0, v4, Lcom/samsung/a/a/b;->a:I

    const/4 v8, -0x1

    if-eq v0, v8, :cond_1

    .line 1276
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 1277
    if-eqz p2, :cond_b

    .line 1278
    iget-object v8, v7, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    iget v4, v4, Lcom/samsung/a/a/b;->a:I

    aget-object v4, v8, v4

    iget-object v4, v4, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    iget-object v4, v4, Lcom/samsung/a/a/c;->b:[F

    aget v4, v4, v1

    iput v4, v0, Landroid/graphics/PointF;->x:F

    .line 1268
    :goto_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_2

    .line 1280
    :cond_b
    iget-object v8, v7, Lcom/samsung/a/a/e;->b:[Lcom/samsung/a/a/a;

    iget v4, v4, Lcom/samsung/a/a/b;->a:I

    aget-object v4, v8, v4

    iget-object v4, v4, Lcom/samsung/a/a/a;->a:Lcom/samsung/a/a/c;

    iget-object v4, v4, Lcom/samsung/a/a/c;->b:[F

    aget v4, v4, v1

    iput v4, v0, Landroid/graphics/PointF;->y:F

    goto :goto_7
.end method

.method protected getAveragePoints(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IFZ)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;IFZ)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2309
    const/4 v0, 0x1

    if-ge p4, v0, :cond_0

    .line 2310
    const/4 v0, 0x0

    .line 2388
    :goto_0
    return-object v0

    .line 2312
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 2314
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2320
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v9, :cond_1

    move-object v0, v8

    .line 2388
    goto :goto_0

    .line 2322
    :cond_1
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 2323
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2324
    new-instance v2, Landroid/graphics/PointF;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2320
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    .line 2329
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->findSafePreIndex(ILjava/util/ArrayList;Ljava/util/ArrayList;IFZ)I

    move-result v7

    .line 2330
    const/4 v0, -0x1

    if-ne v7, v0, :cond_3

    .line 2331
    const-string v0, "SmartClipLibrary"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "average["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] : 0 (preIndex==-1)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2332
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2333
    new-instance v2, Landroid/graphics/PointF;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    .line 2337
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->findSafeNextIndex(ILjava/util/ArrayList;Ljava/util/ArrayList;IFZ)I

    move-result v5

    .line 2338
    const/4 v0, -0x1

    if-ne v5, v0, :cond_4

    .line 2339
    const-string v0, "SmartClipLibrary"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "average["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] : 0 (nextIndex==-1)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2340
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2341
    new-instance v2, Landroid/graphics/PointF;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2346
    :cond_4
    const/4 v3, 0x0

    .line 2347
    const/4 v2, 0x0

    .line 2348
    const/4 v0, 0x0

    .line 2350
    if-le v7, v1, :cond_b

    move v4, v3

    move v3, v2

    move v2, v0

    .line 2351
    :goto_3
    if-lt v7, v9, :cond_5

    .line 2357
    const/4 v0, 0x0

    move v10, v2

    move v2, v3

    move v3, v4

    move v4, v0

    move v0, v10

    :goto_4
    move v6, v4

    move v4, v3

    move v3, v2

    move v2, v0

    .line 2359
    :goto_5
    if-lt v6, v1, :cond_6

    .line 2366
    if-ge v5, v1, :cond_a

    .line 2367
    const/4 v0, 0x0

    move v6, v0

    :goto_6
    if-lt v6, v5, :cond_7

    .line 2373
    add-int/lit8 v0, v9, -0x1

    move v10, v2

    move v2, v3

    move v3, v4

    move v4, v0

    move v0, v10

    :goto_7
    move v6, v1

    move v5, v3

    move v3, v2

    move v2, v0

    .line 2376
    :goto_8
    if-le v6, v4, :cond_8

    .line 2384
    int-to-float v0, v2

    div-float v0, v5, v0

    .line 2385
    int-to-float v2, v2

    div-float v2, v3, v2

    .line 2386
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2352
    :cond_5
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2353
    iget v6, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v6

    .line 2354
    iget v0, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v0

    .line 2355
    add-int/lit8 v2, v2, 0x1

    .line 2351
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 2360
    :cond_6
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2361
    iget v7, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v7, v4

    .line 2362
    iget v0, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v0

    .line 2363
    add-int/lit8 v2, v2, 0x1

    .line 2359
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v7

    goto :goto_5

    .line 2368
    :cond_7
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2369
    iget v7, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v7

    .line 2370
    iget v0, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v0

    .line 2371
    add-int/lit8 v2, v2, 0x1

    .line 2367
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_6

    .line 2377
    :cond_8
    if-ge v6, v9, :cond_9

    .line 2378
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2379
    iget v7, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v7

    .line 2380
    iget v0, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v0

    .line 2381
    add-int/lit8 v0, v2, 0x1

    move v2, v3

    move v3, v5

    .line 2376
    :goto_9
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v3

    move v3, v2

    move v2, v0

    goto :goto_8

    :cond_9
    move v0, v2

    move v2, v3

    move v3, v5

    goto :goto_9

    :cond_a
    move v0, v2

    move v2, v3

    move v3, v4

    move v4, v5

    goto :goto_7

    :cond_b
    move v4, v7

    goto/16 :goto_4
.end method

.method public getCustomSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;
    .locals 1

    .prologue
    .line 433
    if-nez p1, :cond_1

    .line 434
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->a:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    if-nez v0, :cond_0

    .line 435
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getDefaultSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->a:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->a:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 438
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->a:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 476
    :goto_0
    return-object v0

    .line 440
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    .line 441
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->b:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    if-nez v0, :cond_2

    .line 442
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getDefaultSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->b:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 444
    :cond_2
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->b:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 445
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->b:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 447
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5

    .line 448
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->c:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    if-nez v0, :cond_4

    .line 449
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getDefaultSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->c:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 451
    :cond_4
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->c:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 452
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->c:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 454
    :cond_5
    const/4 v0, 0x3

    if-ne p1, v0, :cond_7

    .line 455
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->d:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    if-nez v0, :cond_6

    .line 456
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getDefaultSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->d:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 458
    :cond_6
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->d:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 459
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->d:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 461
    :cond_7
    const/4 v0, 0x4

    if-ne p1, v0, :cond_9

    .line 462
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->e:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    if-nez v0, :cond_8

    .line 463
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getDefaultSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->e:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 465
    :cond_8
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->e:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 466
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->e:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 468
    :cond_9
    const/4 v0, 0x5

    if-ne p1, v0, :cond_b

    .line 469
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->f:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    if-nez v0, :cond_a

    .line 470
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getDefaultSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->f:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 472
    :cond_a
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->f:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 473
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->f:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 476
    :cond_b
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCustomSmartClipParameter(IZ)Lcom/samsung/samm/spenscrap/SmartClipParameter;
    .locals 1

    .prologue
    .line 488
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getCustomSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    .line 489
    if-eqz v0, :cond_0

    .line 490
    iput-boolean p2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    .line 492
    :cond_0
    return-object v0
.end method

.method public getDefaultSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;
    .locals 6

    .prologue
    const/16 v5, 0xf

    const/4 v4, 0x5

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 357
    new-instance v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;

    invoke-direct {v0}, Lcom/samsung/samm/spenscrap/SmartClipParameter;-><init>()V

    .line 358
    if-nez p1, :cond_0

    .line 359
    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 360
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    .line 408
    :goto_0
    return-object v0

    .line 362
    :cond_0
    if-ne p1, v2, :cond_1

    .line 363
    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 364
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    .line 365
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorAXES:Z

    .line 366
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorRANSAC:Z

    goto :goto_0

    .line 368
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 369
    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 370
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    .line 371
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorAXES:Z

    .line 372
    iput-boolean v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorRANSAC:Z

    .line 373
    iput-boolean v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACIncludeEndPoints:Z

    .line 374
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACIteratvie:Z

    .line 375
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACMinimizeErrorStrategy:Z

    .line 376
    const/16 v1, 0xa

    iput v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mEllipseDetectAverageErrorRatio:I

    .line 377
    const/16 v1, 0x50

    iput v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mEllipseDetectCompleteness:I

    goto :goto_0

    .line 379
    :cond_2
    const/4 v1, 0x3

    if-ne p1, v1, :cond_3

    .line 380
    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 381
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbLinearShape:Z

    .line 382
    iput v5, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCurveDetectRatio:I

    .line 383
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbAlignedShape:Z

    .line 384
    iput-boolean v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    goto :goto_0

    .line 386
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_4

    .line 387
    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 388
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbLinearShape:Z

    .line 389
    iput v5, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCurveDetectRatio:I

    .line 390
    iput-boolean v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    goto :goto_0

    .line 392
    :cond_4
    if-ne p1, v4, :cond_5

    .line 393
    iput p1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 394
    iput-boolean v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbLinearShape:Z

    .line 395
    iput v4, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCurveDetectRatio:I

    .line 396
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    .line 397
    iput-boolean v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorAXES:Z

    .line 398
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorRANSAC:Z

    .line 399
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACIncludeEndPoints:Z

    .line 400
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACIteratvie:Z

    .line 401
    iput-boolean v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACMinimizeErrorStrategy:Z

    .line 402
    const/16 v1, 0x32

    iput v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mEllipseDetectAverageErrorRatio:I

    .line 403
    const/16 v1, 0x1e

    iput v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mEllipseDetectCompleteness:I

    goto :goto_0

    .line 406
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultSmartClipParameter(IZ)Lcom/samsung/samm/spenscrap/SmartClipParameter;
    .locals 1

    .prologue
    .line 419
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getDefaultSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    .line 420
    if-eqz v0, :cond_0

    .line 421
    iput-boolean p2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    .line 423
    :cond_0
    return-object v0
.end method

.method protected getDegreeNextIndex(ILjava/util/ArrayList;IFZZ)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;IFZZ)I"
        }
    .end annotation

    .prologue
    .line 1683
    invoke-virtual/range {p0 .. p5}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->findNextIndex(ILjava/util/ArrayList;IFZ)I

    move-result v0

    .line 1684
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1685
    const-string v1, "SmartClipLibrary"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "degree["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] : 0 (nextIndex==-1)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1687
    :cond_0
    return v0
.end method

.method protected getDegreePreIndex(ILjava/util/ArrayList;IFZZ)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;IFZZ)I"
        }
    .end annotation

    .prologue
    .line 1675
    invoke-virtual/range {p0 .. p5}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->findPreIndex(ILjava/util/ArrayList;IFZ)I

    move-result v0

    .line 1676
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1677
    const-string v1, "SmartClipLibrary"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "degree["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] : 0 (preIndex==-1)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1679
    :cond_0
    return v0
.end method

.method protected getEnhancedPath(Lcom/samsung/samm/spenscrap/SmartClipParameter;)Landroid/graphics/Path;
    .locals 25

    .prologue
    .line 747
    move-object/from16 v0, p1

    iget v4, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDegreeMaxNeighborPointNum:I

    .line 748
    move-object/from16 v0, p1

    iget v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDegreeMinNeighborDistance:I

    int-to-float v15, v1

    .line 749
    move-object/from16 v0, p1

    iget v11, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mSegmentPointDegreeThreshold:F

    .line 750
    move-object/from16 v0, p1

    iget v0, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mSmoothPointNum:I

    move/from16 v16, v0

    .line 752
    move-object/from16 v0, p1

    iget-boolean v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbTrim:Z

    .line 753
    move-object/from16 v0, p1

    iget v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mTrimDistance:I

    .line 754
    move-object/from16 v0, p1

    iget-boolean v6, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbCircular:Z

    .line 756
    move-object/from16 v0, p1

    iget v0, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    move/from16 v17, v0

    .line 758
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbRounding:Z

    if-eqz v3, :cond_1

    move-object/from16 v0, p1

    iget v14, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mRoundingDistance:I

    .line 760
    :goto_0
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbAlignedShape:Z

    move/from16 v18, v0

    .line 761
    move-object/from16 v0, p1

    iget v0, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mAlignDistance:I

    move/from16 v19, v0

    .line 763
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbLinearShape:Z

    move/from16 v20, v0

    .line 764
    move-object/from16 v0, p1

    iget v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCurveDetectRatio:I

    int-to-float v3, v3

    const/high16 v5, 0x42c80000    # 100.0f

    div-float v21, v3, v5

    .line 765
    move-object/from16 v0, p1

    iget v0, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCurveDetectDistance:I

    move/from16 v22, v0

    .line 767
    move-object/from16 v0, p1

    iget v13, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCurveEstimationMethod:I

    .line 769
    move-object/from16 v0, p1

    iget-boolean v8, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorAXES:Z

    .line 770
    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseDetectorRANSAC:Z

    .line 771
    move-object/from16 v0, p1

    iget-boolean v9, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACIteratvie:Z

    .line 772
    move-object/from16 v0, p1

    iget-boolean v10, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACIncludeEndPoints:Z

    .line 773
    move-object/from16 v0, p1

    iget-boolean v12, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mbEllipseRANSACMinimizeErrorStrategy:Z

    .line 775
    move-object/from16 v0, p1

    iget v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mEllipseDetectAverageErrorRatio:I

    int-to-float v3, v3

    const/high16 v7, 0x42c80000    # 100.0f

    div-float/2addr v3, v7

    .line 776
    move-object/from16 v0, p1

    iget v7, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mEllipseDetectCompleteness:I

    int-to-float v7, v7

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v23, v7, v23

    .line 783
    if-eqz v1, :cond_2

    .line 784
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getTrimmedPoints(Ljava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v2

    .line 794
    :goto_1
    const/4 v1, 0x1

    move/from16 v0, v17

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    move/from16 v0, v17

    if-ne v0, v1, :cond_6

    .line 796
    :cond_0
    const/4 v7, 0x0

    .line 799
    new-instance v24, Lcom/samsung/samm/spenscrap/b/b/f;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/samm/spenscrap/b/b/f;-><init>()V

    .line 800
    const/4 v1, 0x1

    move-object/from16 v0, v24

    iput-boolean v1, v0, Lcom/samsung/samm/spenscrap/b/b/f;->a:Z

    .line 801
    move-object/from16 v0, v24

    iput-boolean v10, v0, Lcom/samsung/samm/spenscrap/b/b/f;->b:Z

    .line 802
    move-object/from16 v0, v24

    iput-boolean v9, v0, Lcom/samsung/samm/spenscrap/b/b/f;->c:Z

    .line 803
    const/high16 v1, 0x41200000    # 10.0f

    move-object/from16 v0, v24

    iput v1, v0, Lcom/samsung/samm/spenscrap/b/b/f;->d:F

    .line 804
    if-eqz v12, :cond_3

    const/4 v1, 0x0

    :goto_2
    move-object/from16 v0, v24

    iput v1, v0, Lcom/samsung/samm/spenscrap/b/b/f;->e:I

    .line 805
    const/4 v1, 0x1

    .line 812
    if-eqz v5, :cond_16

    .line 813
    move-object/from16 v0, v24

    invoke-static {v2, v0, v1}, Lcom/samsung/samm/spenscrap/b/c;->a(Ljava/util/ArrayList;Lcom/samsung/samm/spenscrap/b/b/f;Z)Lcom/samsung/samm/spenscrap/b/b;

    move-result-object v5

    .line 814
    if-eqz v5, :cond_16

    .line 815
    invoke-virtual {v5}, Lcom/samsung/samm/spenscrap/b/b;->f()F

    move-result v1

    .line 816
    invoke-virtual {v5}, Lcom/samsung/samm/spenscrap/b/b;->g()F

    move-result v9

    .line 818
    cmpg-float v10, v1, v3

    if-gez v10, :cond_16

    cmpl-float v9, v9, v23

    if-lez v9, :cond_16

    move-object v3, v5

    move v5, v1

    .line 828
    :goto_3
    if-eqz v8, :cond_15

    .line 829
    const/4 v1, 0x1

    invoke-static {v2, v1}, Lcom/samsung/samm/spenscrap/b/c;->a(Ljava/util/ArrayList;Z)Lcom/samsung/samm/spenscrap/b/b;

    move-result-object v1

    .line 830
    if-eqz v1, :cond_15

    .line 831
    invoke-virtual {v1}, Lcom/samsung/samm/spenscrap/b/b;->f()F

    move-result v7

    .line 832
    invoke-virtual {v1}, Lcom/samsung/samm/spenscrap/b/b;->g()F

    move-result v8

    .line 834
    cmpg-float v5, v7, v5

    if-gez v5, :cond_15

    cmpl-float v5, v8, v23

    if-lez v5, :cond_15

    .line 841
    :goto_4
    if-eqz v1, :cond_4

    .line 843
    const/4 v2, 0x2

    move-object/from16 v0, p1

    iput v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    .line 844
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makeEllipseSegmentInfoArray(Lcom/samsung/samm/spenscrap/b/b;)Ljava/util/ArrayList;

    move-result-object v1

    .line 845
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v14, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makePath_Third(Ljava/util/ArrayList;IZ)Landroid/graphics/Path;

    move-result-object v1

    .line 970
    :goto_5
    return-object v1

    .line 758
    :cond_1
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 787
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    goto :goto_1

    .line 804
    :cond_3
    const/4 v1, 0x1

    goto :goto_2

    .line 853
    :cond_4
    const/4 v1, 0x2

    move/from16 v0, v17

    if-ne v0, v1, :cond_6

    .line 855
    sget-boolean v1, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mbForceGenerateShape:Z

    if-eqz v1, :cond_5

    .line 856
    invoke-static {v2}, Lcom/samsung/samm/spenscrap/b/c;->a(Ljava/util/ArrayList;)Lcom/samsung/samm/spenscrap/b/b;

    move-result-object v1

    .line 859
    const/4 v2, 0x2

    move-object/from16 v0, p1

    iput v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    .line 861
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makeEllipseSegmentInfoArray(Lcom/samsung/samm/spenscrap/b/b;)Ljava/util/ArrayList;

    move-result-object v1

    .line 862
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v14, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makePath_Third(Ljava/util/ArrayList;IZ)Landroid/graphics/Path;

    move-result-object v1

    goto :goto_5

    .line 865
    :cond_5
    const/4 v1, 0x0

    goto :goto_5

    .line 873
    :cond_6
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6, v1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getNeighborDistances(Ljava/util/ArrayList;ZZ)Ljava/util/ArrayList;

    move-result-object v3

    .line 874
    const/4 v1, 0x0

    invoke-static {v15, v1}, Lcom/samsung/samm/spenscrap/b/c/b;->a(FZ)F

    move-result v1

    float-to-int v5, v1

    .line 881
    const/4 v7, 0x1

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getPointsDegree(Ljava/util/ArrayList;Ljava/util/ArrayList;IIZZ)Ljava/util/ArrayList;

    move-result-object v8

    .line 882
    if-nez v8, :cond_7

    .line 883
    const/4 v1, 0x0

    goto :goto_5

    :cond_7
    move-object/from16 v7, p0

    move-object v9, v3

    move v10, v5

    move v12, v6

    .line 886
    invoke-virtual/range {v7 .. v12}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getKneePoints(Ljava/util/ArrayList;Ljava/util/ArrayList;IFZ)Ljava/util/ArrayList;

    move-result-object v9

    .line 887
    if-nez v9, :cond_8

    .line 888
    const/4 v1, 0x0

    goto :goto_5

    .line 894
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v2, v8, v5}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->removeNearKneePoints(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)Z

    .line 899
    if-nez v17, :cond_9

    move-object/from16 v7, p0

    move-object v8, v2

    move-object v10, v3

    move/from16 v11, v16

    move v12, v15

    move v13, v6

    .line 901
    invoke-virtual/range {v7 .. v13}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getAveragePoints(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IFZ)Ljava/util/ArrayList;

    move-result-object v1

    .line 902
    if-nez v1, :cond_14

    .line 906
    :goto_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makePath(Ljava/util/ArrayList;Z)Landroid/graphics/Path;

    move-result-object v1

    goto :goto_5

    :cond_9
    move-object/from16 v7, p0

    move-object v8, v2

    move/from16 v10, v21

    move/from16 v11, v22

    move/from16 v12, v20

    .line 909
    invoke-virtual/range {v7 .. v14}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getSegmentPointsArray(Ljava/util/ArrayList;Ljava/util/ArrayList;FIZII)Ljava/util/ArrayList;

    move-result-object v1

    .line 910
    if-nez v1, :cond_a

    .line 911
    const/4 v1, 0x0

    goto :goto_5

    .line 913
    :cond_a
    if-eqz v18, :cond_b

    .line 914
    const/4 v3, 0x1

    move/from16 v0, v19

    int-to-float v4, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v3, v4}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->AlignSegmentPoint(Ljava/util/ArrayList;ZF)Z

    .line 915
    const/4 v3, 0x0

    move/from16 v0, v19

    int-to-float v4, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v3, v4}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->AlignSegmentPoint(Ljava/util/ArrayList;ZF)Z

    .line 922
    :cond_b
    const/4 v3, 0x1

    move/from16 v0, v17

    if-ne v0, v3, :cond_e

    .line 923
    move-object/from16 v0, p1

    iget v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mAlignDistance:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->isRect(Ljava/util/ArrayList;F)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 924
    const/4 v2, 0x3

    move-object/from16 v0, p1

    iput v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    .line 934
    :goto_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v14, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makePath_Third(Ljava/util/ArrayList;IZ)Landroid/graphics/Path;

    move-result-object v1

    goto/16 :goto_5

    .line 927
    :cond_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->isPolygon(Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 928
    const/4 v2, 0x4

    move-object/from16 v0, p1

    iput v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    goto :goto_7

    .line 930
    :cond_d
    const/4 v2, 0x5

    move-object/from16 v0, p1

    iput v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    goto :goto_7

    .line 936
    :cond_e
    const/4 v3, 0x3

    move/from16 v0, v17

    if-ne v0, v3, :cond_11

    .line 937
    move-object/from16 v0, p1

    iget v3, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mAlignDistance:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v3}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->isRect(Ljava/util/ArrayList;F)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 938
    const/4 v2, 0x3

    move-object/from16 v0, p1

    iput v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    .line 940
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v14, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makePath_Third(Ljava/util/ArrayList;IZ)Landroid/graphics/Path;

    move-result-object v1

    goto/16 :goto_5

    .line 944
    :cond_f
    sget-boolean v1, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mbForceGenerateShape:Z

    if-eqz v1, :cond_10

    .line 945
    const/4 v1, 0x3

    move-object/from16 v0, p1

    iput v1, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    .line 947
    invoke-static {v2}, Lcom/samsung/samm/spenscrap/b/c/d;->a(Ljava/util/ArrayList;)Landroid/graphics/RectF;

    move-result-object v1

    .line 948
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makeRectSegmentInfoArray(Landroid/graphics/RectF;)Ljava/util/ArrayList;

    move-result-object v1

    .line 951
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v14, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makePath_Third(Ljava/util/ArrayList;IZ)Landroid/graphics/Path;

    move-result-object v1

    goto/16 :goto_5

    .line 954
    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 958
    :cond_11
    const/4 v2, 0x4

    move/from16 v0, v17

    if-ne v0, v2, :cond_12

    .line 959
    const/4 v2, 0x4

    move-object/from16 v0, p1

    iput v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    .line 961
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v14, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makePath_Third(Ljava/util/ArrayList;IZ)Landroid/graphics/Path;

    move-result-object v1

    goto/16 :goto_5

    .line 963
    :cond_12
    const/4 v2, 0x5

    move/from16 v0, v17

    if-ne v0, v2, :cond_13

    .line 964
    const/4 v2, 0x5

    move-object/from16 v0, p1

    iput v2, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    .line 966
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v14, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makePath_Third(Ljava/util/ArrayList;IZ)Landroid/graphics/Path;

    move-result-object v1

    goto/16 :goto_5

    .line 969
    :cond_13
    const-string v1, "SmartClipLibrary"

    const-string v2, "Undefined Style"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_14
    move-object v2, v1

    goto/16 :goto_6

    :cond_15
    move-object v1, v3

    goto/16 :goto_4

    :cond_16
    move v5, v3

    move-object v3, v7

    goto/16 :goto_3
.end method

.method protected getKneePoints(Ljava/util/ArrayList;Ljava/util/ArrayList;IFZ)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;IFZ)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1522
    .line 1524
    if-nez p1, :cond_0

    .line 1525
    const/4 v0, 0x0

    .line 1613
    :goto_0
    return-object v0

    .line 1526
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1528
    const/4 v4, 0x0

    .line 1529
    const/4 v3, 0x0

    .line 1532
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1533
    const/4 v1, 0x0

    .line 1536
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-lt v6, v7, :cond_1

    move-object v0, v5

    .line 1613
    goto :goto_0

    .line 1537
    :cond_1
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 1539
    cmpg-float v0, v2, p4

    if-gez v0, :cond_2

    .line 1540
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v2

    move v1, v3

    move v2, v4

    .line 1536
    :goto_2
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v4, v2

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 1545
    :cond_2
    if-nez v6, :cond_6

    .line 1546
    if-eqz p5, :cond_5

    .line 1548
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    int-to-float v1, p3

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 1549
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    move v1, v0

    .line 1554
    :goto_3
    add-int/lit8 v0, v6, 0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1582
    :goto_4
    sub-float v8, v2, v1

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_a

    .line 1583
    const/4 v1, 0x1

    .line 1590
    :goto_5
    sub-float v4, v0, v2

    const/4 v8, 0x0

    cmpg-float v4, v4, v8

    if-gez v4, :cond_b

    .line 1591
    const/4 v0, 0x1

    .line 1599
    :goto_6
    if-eqz v1, :cond_d

    .line 1600
    add-int/lit8 v3, v7, -0x1

    if-eq v6, v3, :cond_3

    if-eqz v0, :cond_c

    .line 1601
    :cond_3
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_7
    move v10, v2

    move v2, v1

    move v1, v0

    move v0, v10

    .line 1610
    goto :goto_2

    .line 1552
    :cond_4
    const/4 v0, 0x0

    move v1, v0

    goto :goto_3

    .line 1557
    :cond_5
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v2

    move v1, v3

    move v2, v4

    .line 1559
    goto :goto_2

    .line 1562
    :cond_6
    add-int/lit8 v0, v7, -0x1

    if-ne v6, v0, :cond_9

    .line 1563
    if-eqz p5, :cond_8

    .line 1565
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    int-to-float v8, p3

    cmpg-float v0, v0, v8

    if-gez v0, :cond_7

    .line 1566
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_4

    .line 1569
    :cond_7
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_4

    .line 1573
    :cond_8
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v2

    move v1, v3

    move v2, v4

    .line 1575
    goto/16 :goto_2

    .line 1579
    :cond_9
    add-int/lit8 v0, v6, 0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_4

    .line 1584
    :cond_a
    sub-float v1, v2, v1

    const/4 v8, 0x0

    cmpg-float v1, v1, v8

    if-gez v1, :cond_f

    .line 1585
    const/4 v1, 0x0

    goto :goto_5

    .line 1592
    :cond_b
    sub-float/2addr v0, v2

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_e

    .line 1593
    const/4 v0, 0x0

    goto :goto_6

    .line 1603
    :cond_c
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 1606
    :cond_d
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_e
    move v0, v3

    goto/16 :goto_6

    :cond_f
    move v1, v4

    goto/16 :goto_5
.end method

.method protected getNeighborDistances(Ljava/util/ArrayList;ZZ)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;ZZ)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 3080
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 3081
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 3087
    if-eqz p2, :cond_3

    move v3, v0

    move v1, v4

    move v2, v4

    .line 3088
    :goto_0
    if-lt v3, v6, :cond_1

    .line 3128
    :cond_0
    return-object v7

    .line 3089
    :cond_1
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v5, v0, Landroid/graphics/PointF;->x:F

    .line 3090
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v4, v0, Landroid/graphics/PointF;->y:F

    .line 3092
    if-nez v3, :cond_6

    .line 3093
    add-int/lit8 v0, v6, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    .line 3094
    add-int/lit8 v0, v6, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 3096
    :goto_1
    sub-float v1, v5, v1

    .line 3097
    sub-float v0, v4, v0

    .line 3098
    if-eqz p3, :cond_2

    .line 3099
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3088
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v1, v4

    move v2, v5

    goto :goto_0

    .line 3101
    :cond_2
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move v2, v0

    move v3, v4

    move v5, v4

    .line 3108
    :goto_3
    if-ge v2, v6, :cond_0

    .line 3109
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    .line 3110
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 3112
    if-nez v2, :cond_4

    .line 3113
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3108
    :goto_4
    add-int/lit8 v2, v2, 0x1

    move v3, v0

    move v5, v1

    goto :goto_3

    .line 3118
    :cond_4
    sub-float v5, v1, v5

    .line 3119
    sub-float v3, v0, v3

    .line 3120
    if-eqz p3, :cond_5

    .line 3121
    mul-float/2addr v5, v5

    mul-float/2addr v3, v3

    add-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 3123
    :cond_5
    mul-float/2addr v5, v5

    mul-float/2addr v3, v3

    add-float/2addr v3, v5

    float-to-double v8, v3

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v3, v8

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method protected getPointsDegree(Ljava/util/ArrayList;Ljava/util/ArrayList;IIZZ)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;IIZZ)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2259
    const/4 v0, 0x1

    if-ge p3, v0, :cond_0

    .line 2260
    const/4 v0, 0x0

    .line 2298
    :goto_0
    return-object v0

    .line 2262
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 2264
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2269
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v7, :cond_1

    move-object v0, v6

    .line 2298
    goto :goto_0

    .line 2271
    :cond_1
    int-to-float v4, p4

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->findPreIndex(ILjava/util/ArrayList;IFZ)I

    move-result v8

    .line 2272
    const/4 v0, -0x1

    if-ne v8, v0, :cond_2

    .line 2274
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2269
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2278
    :cond_2
    int-to-float v4, p4

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->findNextIndex(ILjava/util/ArrayList;IFZ)I

    move-result v2

    .line 2279
    const/4 v0, -0x1

    if-ne v2, v0, :cond_3

    .line 2281
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2286
    :cond_3
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v0

    .line 2287
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v0

    .line 2288
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v5, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v0

    .line 2289
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v2, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v0, v2, v0

    .line 2291
    invoke-static {v3, v4, v5, v0}, Lcom/samsung/samm/spenscrap/b/c/b;->a(FFFF)F

    move-result v0

    float-to-double v2, v0

    .line 2292
    if-eqz p6, :cond_4

    .line 2293
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 2295
    :cond_4
    double-to-float v0, v2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method protected getSegmentPointsArray(Ljava/util/ArrayList;Ljava/util/ArrayList;FIZII)Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;FIZII)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/samm/spenscrap/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1320
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1321
    :cond_0
    const/4 v1, 0x0

    .line 1517
    :goto_0
    return-object v1

    .line 1323
    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1324
    if-gtz v3, :cond_2

    .line 1325
    const/4 v1, 0x0

    goto :goto_0

    .line 1326
    :cond_2
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1328
    if-eq v3, v4, :cond_3

    .line 1329
    const-string v1, "SmartClipLibrary"

    const-string v2, "The size of result knee point is different"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1330
    const/4 v1, 0x0

    goto :goto_0

    .line 1333
    :cond_3
    const/4 v1, 0x2

    if-ge v4, v1, :cond_4

    .line 1334
    const/4 v1, 0x0

    goto :goto_0

    .line 1336
    :cond_4
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1338
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-lt v2, v3, :cond_6

    .line 1349
    if-nez p5, :cond_8

    const/4 v1, 0x1

    move/from16 v0, p4

    if-ne v0, v1, :cond_8

    const/4 v1, 0x1

    move v2, v1

    .line 1354
    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_a

    .line 1356
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 1357
    new-instance v3, Lcom/samsung/samm/spenscrap/b/d;

    invoke-direct {v3}, Lcom/samsung/samm/spenscrap/b/d;-><init>()V

    .line 1358
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v5, v6, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v5, v3, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1359
    move/from16 v0, p5

    iput-boolean v0, v3, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    .line 1360
    const/4 v1, 0x0

    iput v1, v3, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1361
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1363
    div-int/lit8 v3, v4, 0x2

    .line 1364
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 1365
    new-instance v4, Lcom/samsung/samm/spenscrap/b/d;

    invoke-direct {v4}, Lcom/samsung/samm/spenscrap/b/d;-><init>()V

    .line 1366
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v5, v6, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v5, v4, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1367
    if-eqz v2, :cond_9

    .line 1368
    const/4 v1, 0x1

    iput-boolean v1, v4, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    .line 1371
    :goto_3
    iput v3, v4, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1372
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1373
    const-string v1, "SmartClipLibrary"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Knee point is 0, add 2 points(0,"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1393
    :cond_5
    :goto_4
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1397
    const/4 v1, 0x0

    .line 1400
    mul-int v3, p7, p7

    int-to-float v10, v3

    move v8, v1

    move v9, v2

    .line 1402
    :goto_5
    if-lt v8, v9, :cond_c

    move-object v1, v7

    .line 1517
    goto/16 :goto_0

    .line 1339
    :cond_6
    const/4 v5, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v5, v1, :cond_7

    .line 1340
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 1341
    new-instance v5, Lcom/samsung/samm/spenscrap/b/d;

    invoke-direct {v5}, Lcom/samsung/samm/spenscrap/b/d;-><init>()V

    .line 1342
    new-instance v6, Landroid/graphics/PointF;

    iget v8, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v6, v8, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v6, v5, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1343
    const/4 v1, 0x1

    iput-boolean v1, v5, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    .line 1344
    iput v2, v5, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1345
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1338
    :cond_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_1

    .line 1349
    :cond_8
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_2

    .line 1370
    :cond_9
    move/from16 v0, p5

    iput-boolean v0, v4, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    goto :goto_3

    .line 1375
    :cond_a
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_5

    .line 1376
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/samm/spenscrap/b/d;

    iget v5, v1, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1377
    div-int/lit8 v1, v4, 0x2

    add-int/2addr v1, v5

    .line 1378
    if-lt v1, v4, :cond_18

    sub-int/2addr v1, v4

    move v3, v1

    .line 1379
    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 1380
    new-instance v4, Lcom/samsung/samm/spenscrap/b/d;

    invoke-direct {v4}, Lcom/samsung/samm/spenscrap/b/d;-><init>()V

    .line 1381
    new-instance v6, Landroid/graphics/PointF;

    iget v8, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v6, v8, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v6, v4, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1382
    if-eqz v2, :cond_b

    .line 1383
    const/4 v1, 0x1

    iput-boolean v1, v4, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    .line 1386
    :goto_7
    iput v3, v4, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1387
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1388
    const-string v1, "SmartClipLibrary"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Knee point is 1("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "), add another points("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1385
    :cond_b
    move/from16 v0, p5

    iput-boolean v0, v4, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    goto :goto_7

    .line 1404
    :cond_c
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/samm/spenscrap/b/d;

    .line 1405
    add-int/lit8 v2, v8, 0x1

    if-lt v2, v9, :cond_d

    .line 1406
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/samm/spenscrap/b/d;

    .line 1410
    :goto_8
    iget v3, v1, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1411
    iget v4, v2, Lcom/samsung/samm/spenscrap/b/d;->f:I

    move-object v1, p0

    move-object/from16 v2, p1

    move/from16 v5, p3

    move/from16 v6, p4

    .line 1413
    invoke-direct/range {v1 .. v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->a(Ljava/util/ArrayList;IIFI)Lcom/samsung/samm/spenscrap/b/a;

    move-result-object v11

    .line 1414
    if-eqz v11, :cond_16

    .line 1417
    invoke-virtual {v11}, Lcom/samsung/samm/spenscrap/b/a;->b()I

    move-result v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 1418
    if-lez p7, :cond_f

    .line 1420
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1421
    const/4 v5, 0x1

    invoke-static {v1, v2, v5}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    .line 1422
    cmpg-float v2, v2, v10

    if-gez v2, :cond_e

    .line 1423
    add-int/lit8 v1, v8, 0x1

    .line 1424
    const-string v2, "SmartClipLibrary"

    const-string v3, "Detected curve point is too close. Skip!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v1

    .line 1425
    goto/16 :goto_5

    .line 1408
    :cond_d
    add-int/lit8 v2, v8, 0x1

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/samm/spenscrap/b/d;

    goto :goto_8

    .line 1428
    :cond_e
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1429
    const/4 v5, 0x1

    invoke-static {v1, v2, v5}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    .line 1430
    cmpg-float v2, v2, v10

    if-gez v2, :cond_f

    .line 1431
    add-int/lit8 v1, v8, 0x1

    .line 1432
    const-string v2, "SmartClipLibrary"

    const-string v3, "Detected curve point is too close. Skip!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v1

    .line 1433
    goto/16 :goto_5

    .line 1437
    :cond_f
    new-instance v12, Lcom/samsung/samm/spenscrap/b/d;

    invoke-direct {v12}, Lcom/samsung/samm/spenscrap/b/d;-><init>()V

    .line 1438
    move/from16 v0, p5

    iput-boolean v0, v12, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    .line 1440
    if-nez p5, :cond_11

    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_11

    const/4 v6, 0x1

    .line 1441
    :goto_9
    if-eqz v6, :cond_17

    .line 1442
    invoke-virtual {v11}, Lcom/samsung/samm/spenscrap/b/a;->a()F

    move-result v13

    .line 1443
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1444
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    .line 1445
    const/4 v14, 0x0

    invoke-static {v2, v5, v14}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    .line 1448
    const v5, 0x3ea8f5c3    # 0.33f

    mul-float/2addr v2, v5

    .line 1451
    cmpl-float v2, v13, v2

    if-gtz v2, :cond_10

    mul-int/lit8 v2, p4, 0x2

    int-to-float v2, v2

    cmpl-float v2, v13, v2

    if-lez v2, :cond_17

    .line 1452
    :cond_10
    const/4 v2, 0x0

    .line 1453
    const/4 v5, 0x1

    iput-boolean v5, v12, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    .line 1458
    :goto_a
    if-eqz v2, :cond_15

    .line 1461
    const/high16 v2, 0x42480000    # 50.0f

    move-object/from16 v0, p1

    invoke-static {v0, v3, v4, v2}, Lcom/samsung/samm/spenscrap/b/a/a;->a(Ljava/util/ArrayList;IIF)Lcom/samsung/samm/spenscrap/b/a/a$a;

    move-result-object v3

    .line 1466
    if-eqz v3, :cond_14

    .line 1469
    invoke-virtual {v3}, Lcom/samsung/samm/spenscrap/b/a/a$a;->b()F

    move-result v2

    .line 1470
    const/high16 v4, 0x43960000    # 300.0f

    cmpg-float v2, v2, v4

    if-gez v2, :cond_12

    .line 1471
    const/4 v2, 0x1

    .line 1475
    :goto_b
    if-eqz v2, :cond_13

    .line 1476
    invoke-virtual {v3}, Lcom/samsung/samm/spenscrap/b/a/a$a;->a()Landroid/graphics/PointF;

    move-result-object v1

    .line 1477
    const/4 v2, 0x1

    iput-boolean v2, v12, Lcom/samsung/samm/spenscrap/b/d;->c:Z

    .line 1478
    new-instance v2, Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, v12, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1479
    invoke-virtual {v11}, Lcom/samsung/samm/spenscrap/b/a;->b()I

    move-result v1

    iput v1, v12, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1480
    add-int/lit8 v1, v8, 0x1

    invoke-virtual {v7, v1, v12}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1482
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1483
    add-int/lit8 v1, v8, 0x2

    move v8, v1

    move v9, v2

    .line 1484
    goto/16 :goto_5

    .line 1440
    :cond_11
    const/4 v6, 0x0

    goto :goto_9

    .line 1473
    :cond_12
    const/4 v2, 0x0

    goto :goto_b

    .line 1488
    :cond_13
    new-instance v2, Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, v12, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1489
    invoke-virtual {v11}, Lcom/samsung/samm/spenscrap/b/a;->b()I

    move-result v1

    iput v1, v12, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1490
    add-int/lit8 v1, v8, 0x1

    invoke-virtual {v7, v1, v12}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1492
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v9, v1

    .line 1493
    goto/16 :goto_5

    .line 1498
    :cond_14
    add-int/lit8 v1, v8, 0x1

    .line 1499
    const-string v2, "SmartClipLibrary"

    const-string v3, "Cannot get the control point of the curve"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v1

    .line 1501
    goto/16 :goto_5

    .line 1504
    :cond_15
    new-instance v2, Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, v12, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1505
    invoke-virtual {v11}, Lcom/samsung/samm/spenscrap/b/a;->b()I

    move-result v1

    iput v1, v12, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1506
    add-int/lit8 v1, v8, 0x1

    invoke-virtual {v7, v1, v12}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1508
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v9, v1

    .line 1510
    goto/16 :goto_5

    .line 1513
    :cond_16
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_5

    :cond_17
    move v2, v6

    goto/16 :goto_a

    :cond_18
    move v3, v1

    goto/16 :goto_6
.end method

.method public getSimpleClipPath(I)Landroid/graphics/Path;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 322
    if-nez p1, :cond_1

    .line 323
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->makePath(Ljava/util/ArrayList;Z)Landroid/graphics/Path;

    move-result-object v0

    .line 342
    :cond_0
    :goto_0
    return-object v0

    .line 325
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 326
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/samsung/samm/spenscrap/b/c/d;->a(Ljava/util/ArrayList;)Landroid/graphics/RectF;

    move-result-object v1

    .line 327
    if-eqz v1, :cond_0

    .line 329
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 330
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    goto :goto_0

    .line 333
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 334
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/samsung/samm/spenscrap/b/c/d;->a(Ljava/util/ArrayList;)Landroid/graphics/RectF;

    move-result-object v1

    .line 335
    if-eqz v1, :cond_0

    .line 337
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 338
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    goto :goto_0
.end method

.method public getSmartClipInfo(Z)Lcom/samsung/samm/spenscrap/SmartClipInfo;
    .locals 3

    .prologue
    .line 600
    invoke-virtual {p0}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->recognizeSmartClipShape()I

    move-result v1

    .line 601
    invoke-virtual {p0, v1, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getSmartClipPath(IZ)Landroid/graphics/Path;

    move-result-object v2

    .line 602
    if-nez v2, :cond_0

    .line 603
    const/4 v0, 0x0

    .line 605
    :goto_0
    return-object v0

    .line 604
    :cond_0
    new-instance v0, Lcom/samsung/samm/spenscrap/SmartClipInfo;

    invoke-direct {v0, v2, v1}, Lcom/samsung/samm/spenscrap/SmartClipInfo;-><init>(Landroid/graphics/Path;I)V

    goto :goto_0
.end method

.method public getSmartClipPath(I)Landroid/graphics/Path;
    .locals 3

    .prologue
    .line 633
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getCustomSmartClipParameter(I)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    .line 634
    if-nez v0, :cond_0

    .line 635
    const-string v0, "SmartClipLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Smart Clip Shape : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    const/4 v0, 0x0

    .line 638
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getEnhancedPath(Lcom/samsung/samm/spenscrap/SmartClipParameter;)Landroid/graphics/Path;

    move-result-object v0

    goto :goto_0
.end method

.method public getSmartClipPath(IZ)Landroid/graphics/Path;
    .locals 3

    .prologue
    .line 617
    invoke-virtual {p0, p1, p2}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getCustomSmartClipParameter(IZ)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    .line 618
    if-nez v0, :cond_0

    .line 619
    const-string v0, "SmartClipLibrary"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Smart Clip Shape : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    const/4 v0, 0x0

    .line 622
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getEnhancedPath(Lcom/samsung/samm/spenscrap/SmartClipParameter;)Landroid/graphics/Path;

    move-result-object v0

    goto :goto_0
.end method

.method protected getTrimmedPoints(Ljava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1292
    int-to-float v0, p2

    invoke-virtual {p0, p1, v0}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->trimPointsByCrossPoint(Ljava/util/ArrayList;F)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected isPolygon(Ljava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/samm/spenscrap/b/d;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 3136
    if-nez p1, :cond_1

    .line 3148
    :cond_0
    :goto_0
    return v3

    .line 3138
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    move v2, v3

    .line 3140
    :goto_1
    if-lt v4, v5, :cond_3

    .line 3148
    if-eqz v2, :cond_2

    move v1, v3

    :cond_2
    move v3, v1

    goto :goto_0

    .line 3141
    :cond_3
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    .line 3142
    iget-boolean v6, v0, Lcom/samsung/samm/spenscrap/b/d;->c:Z

    if-nez v6, :cond_0

    .line 3145
    iget-boolean v0, v0, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-nez v0, :cond_4

    move v0, v1

    .line 3140
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method protected isRect(Ljava/util/ArrayList;F)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/samm/spenscrap/b/d;",
            ">;F)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 3153
    if-nez p1, :cond_0

    move v0, v2

    .line 3172
    :goto_0
    return v0

    .line 3155
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->isPolygon(Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 3156
    goto :goto_0

    .line 3158
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 3159
    const/4 v0, 0x4

    if-eq v4, v0, :cond_2

    move v0, v2

    .line 3160
    goto :goto_0

    :cond_2
    move v3, v2

    .line 3162
    :goto_1
    if-lt v3, v4, :cond_3

    .line 3172
    const/4 v0, 0x1

    goto :goto_0

    .line 3163
    :cond_3
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    .line 3164
    add-int/lit8 v1, v4, -0x1

    if-ne v3, v1, :cond_4

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/samm/spenscrap/b/d;

    .line 3166
    :goto_2
    iget-object v5, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, v1, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    .line 3167
    iget-object v0, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, v1, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    .line 3169
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, p2

    if-lez v0, :cond_5

    move v0, v2

    .line 3170
    goto :goto_0

    .line 3164
    :cond_4
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/samm/spenscrap/b/d;

    goto :goto_2

    .line 3162
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method public loadPointDataFile(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 257
    if-nez p1, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v0

    .line 261
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 262
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 265
    invoke-virtual {p0}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->resetPoints()V

    .line 268
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "r"

    invoke-direct {v2, v1, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 270
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readInt()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    move v1, v0

    .line 271
    :goto_1
    if-lt v1, v3, :cond_2

    .line 281
    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 292
    const/4 v0, 0x1

    goto :goto_0

    .line 272
    :cond_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readFloat()F

    move-result v4

    .line 273
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readFloat()F

    move-result v5

    .line 274
    invoke-virtual {p0, v4, v5}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->addPoint(FF)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 271
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 276
    :catch_0
    move-exception v1

    .line 277
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 281
    :try_start_5
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 282
    :catch_1
    move-exception v1

    .line 283
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    .line 287
    :catch_2
    move-exception v1

    .line 288
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 279
    :catchall_0
    move-exception v1

    .line 281
    :try_start_7
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_2

    .line 286
    :try_start_8
    throw v1

    .line 282
    :catch_3
    move-exception v1

    .line 283
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 282
    :catch_4
    move-exception v1

    .line 283
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_0
.end method

.method protected makeEllipseSegmentInfoArray(Lcom/samsung/samm/spenscrap/b/b;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/samm/spenscrap/b/b;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/samm/spenscrap/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 978
    if-nez p1, :cond_0

    .line 979
    const/4 v0, 0x0

    .line 992
    :goto_0
    return-object v0

    .line 982
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 983
    new-instance v1, Lcom/samsung/samm/spenscrap/b/d;

    invoke-direct {v1}, Lcom/samsung/samm/spenscrap/b/d;-><init>()V

    .line 984
    invoke-virtual {p1}, Lcom/samsung/samm/spenscrap/b/b;->b()Landroid/graphics/PointF;

    move-result-object v2

    .line 985
    new-instance v3, Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v3, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v3, v1, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 986
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    .line 987
    iput-boolean v5, v1, Lcom/samsung/samm/spenscrap/b/d;->c:Z

    .line 988
    iput-boolean v5, v1, Lcom/samsung/samm/spenscrap/b/d;->d:Z

    .line 989
    iput-object p1, v1, Lcom/samsung/samm/spenscrap/b/d;->e:Lcom/samsung/samm/spenscrap/b/b;

    .line 990
    const/4 v2, -0x1

    iput v2, v1, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 991
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected makePath(Ljava/util/ArrayList;Z)Landroid/graphics/Path;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;Z)",
            "Landroid/graphics/Path;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 3026
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 3027
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 3029
    if-ge v5, v1, :cond_0

    const/4 v0, 0x0

    .line 3067
    :goto_0
    return-object v0

    .line 3031
    :cond_0
    if-eqz p2, :cond_2

    .line 3033
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 3034
    add-int/lit8 v1, v5, -0x1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 3035
    new-instance v4, Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v7, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v7

    div-float/2addr v6, v10

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    div-float/2addr v0, v10

    invoke-direct {v4, v6, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3036
    iget v0, v4, Landroid/graphics/PointF;->x:F

    iget v1, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    move v1, v3

    move-object v3, v4

    .line 3040
    :goto_1
    if-lt v1, v5, :cond_1

    .line 3047
    iget v0, v3, Landroid/graphics/PointF;->x:F

    iget v1, v3, Landroid/graphics/PointF;->y:F

    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget v6, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v6

    div-float/2addr v5, v10

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v4

    div-float/2addr v3, v10

    invoke-virtual {v2, v0, v1, v5, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 3048
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    :goto_2
    move-object v0, v2

    .line 3067
    goto :goto_0

    .line 3041
    :cond_1
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 3042
    iget v6, v3, Landroid/graphics/PointF;->x:F

    iget v7, v3, Landroid/graphics/PointF;->y:F

    iget v8, v3, Landroid/graphics/PointF;->x:F

    iget v9, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v8, v9

    div-float/2addr v8, v10

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget v9, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v9

    div-float/2addr v3, v10

    invoke-virtual {v2, v6, v7, v8, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 3040
    add-int/lit8 v1, v1, 0x1

    move-object v3, v0

    goto :goto_1

    .line 3052
    :cond_2
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 3053
    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    move-object v3, v0

    .line 3056
    :goto_3
    if-lt v1, v5, :cond_3

    .line 3063
    add-int/lit8 v0, v5, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 3064
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 3065
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    goto :goto_2

    .line 3057
    :cond_3
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 3058
    iget v4, v3, Landroid/graphics/PointF;->x:F

    iget v6, v3, Landroid/graphics/PointF;->y:F

    iget v7, v3, Landroid/graphics/PointF;->x:F

    iget v8, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v7, v8

    div-float/2addr v7, v10

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget v8, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v8

    div-float/2addr v3, v10

    invoke-virtual {v2, v4, v6, v7, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 3056
    add-int/lit8 v1, v1, 0x1

    move-object v3, v0

    goto :goto_3
.end method

.method protected makePath_Second(Ljava/util/ArrayList;I)Landroid/graphics/Path;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/samm/spenscrap/b/d;",
            ">;I)",
            "Landroid/graphics/Path;"
        }
    .end annotation

    .prologue
    .line 2664
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 2665
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 2666
    const/4 v0, 0x1

    if-ge v6, v0, :cond_0

    const/4 v0, 0x0

    .line 2738
    :goto_0
    return-object v0

    .line 2668
    :cond_0
    add-int/lit8 v0, v6, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    iget-object v3, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 2669
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    iget-object v0, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 2670
    new-instance v1, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    iget v5, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    invoke-direct {v1, v4, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2671
    new-instance v7, Landroid/graphics/PointF;

    invoke-direct {v7}, Landroid/graphics/PointF;-><init>()V

    .line 2673
    new-instance v8, Landroid/graphics/PointF;

    invoke-direct {v8}, Landroid/graphics/PointF;-><init>()V

    .line 2674
    new-instance v9, Landroid/graphics/PointF;

    invoke-direct {v9}, Landroid/graphics/PointF;-><init>()V

    .line 2676
    iget v0, v1, Landroid/graphics/PointF;->x:F

    iget v3, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2683
    const/4 v0, 0x0

    move v3, v0

    move-object v4, v1

    :goto_1
    if-lt v3, v6, :cond_1

    .line 2736
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    move-object v0, v2

    .line 2738
    goto :goto_0

    .line 2685
    :cond_1
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/samm/spenscrap/b/d;

    .line 2686
    iget-object v5, v0, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 2689
    add-int/lit8 v1, v6, -0x1

    if-ne v3, v1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/samm/spenscrap/b/d;

    iget-object v1, v1, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 2691
    :goto_2
    iget v10, v5, Landroid/graphics/PointF;->x:F

    iget v11, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    iput v10, v7, Landroid/graphics/PointF;->x:F

    .line 2692
    iget v10, v5, Landroid/graphics/PointF;->y:F

    iget v11, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    iput v10, v7, Landroid/graphics/PointF;->y:F

    .line 2697
    iget-boolean v0, v0, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-eqz v0, :cond_6

    .line 2699
    if-nez p2, :cond_3

    .line 2700
    iget v0, v5, Landroid/graphics/PointF;->x:F

    iget v1, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2701
    iget v0, v7, Landroid/graphics/PointF;->x:F

    iget v1, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2683
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move-object v4, v5

    goto :goto_1

    .line 2690
    :cond_2
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/samm/spenscrap/b/d;

    iget-object v1, v1, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    goto :goto_2

    .line 2705
    :cond_3
    const/4 v0, 0x0

    invoke-static {v5, v4, v0}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v0

    .line 2706
    int-to-float v10, p2

    const/high16 v11, 0x40000000    # 2.0f

    div-float v11, v0, v11

    cmpg-float v10, v10, v11

    if-gtz v10, :cond_4

    .line 2707
    int-to-float v10, p2

    div-float v0, v10, v0

    .line 2710
    :goto_4
    iget v10, v4, Landroid/graphics/PointF;->x:F

    mul-float/2addr v10, v0

    iget v11, v5, Landroid/graphics/PointF;->x:F

    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v0

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    iput v10, v8, Landroid/graphics/PointF;->x:F

    .line 2711
    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float/2addr v4, v0

    iget v10, v5, Landroid/graphics/PointF;->y:F

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float v0, v11, v0

    mul-float/2addr v0, v10

    add-float/2addr v0, v4

    iput v0, v8, Landroid/graphics/PointF;->y:F

    .line 2713
    const/4 v0, 0x0

    invoke-static {v5, v1, v0}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v0

    .line 2714
    int-to-float v4, p2

    const/high16 v10, 0x40000000    # 2.0f

    div-float v10, v0, v10

    cmpg-float v4, v4, v10

    if-gtz v4, :cond_5

    .line 2715
    int-to-float v4, p2

    div-float v0, v4, v0

    .line 2718
    :goto_5
    iget v4, v5, Landroid/graphics/PointF;->x:F

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v0

    mul-float/2addr v4, v10

    iget v10, v1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v10, v0

    add-float/2addr v4, v10

    iput v4, v9, Landroid/graphics/PointF;->x:F

    .line 2719
    iget v4, v5, Landroid/graphics/PointF;->y:F

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v0

    mul-float/2addr v4, v10

    iget v1, v1, Landroid/graphics/PointF;->y:F

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    iput v0, v9, Landroid/graphics/PointF;->y:F

    .line 2721
    iget v0, v8, Landroid/graphics/PointF;->x:F

    iget v1, v8, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2722
    iget v0, v5, Landroid/graphics/PointF;->x:F

    iget v1, v5, Landroid/graphics/PointF;->y:F

    iget v4, v9, Landroid/graphics/PointF;->x:F

    iget v10, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v1, v4, v10}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 2723
    iget v0, v7, Landroid/graphics/PointF;->x:F

    iget v1, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_3

    .line 2709
    :cond_4
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_4

    .line 2717
    :cond_5
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_5

    .line 2730
    :cond_6
    iget v0, v5, Landroid/graphics/PointF;->x:F

    iget v1, v5, Landroid/graphics/PointF;->y:F

    iget v4, v7, Landroid/graphics/PointF;->x:F

    iget v10, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v1, v4, v10}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto/16 :goto_3
.end method

.method protected makePath_Third(Ljava/util/ArrayList;IZ)Landroid/graphics/Path;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/samm/spenscrap/b/d;",
            ">;IZ)",
            "Landroid/graphics/Path;"
        }
    .end annotation

    .prologue
    .line 2749
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 2750
    if-nez p1, :cond_0

    .line 2751
    const/4 v2, 0x0

    .line 3015
    :goto_0
    return-object v2

    .line 2752
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 2753
    const/4 v2, 0x1

    if-ge v7, v2, :cond_1

    .line 2754
    const/4 v2, 0x0

    goto :goto_0

    .line 2758
    :cond_1
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    .line 2760
    new-instance v8, Landroid/graphics/PointF;

    invoke-direct {v8}, Landroid/graphics/PointF;-><init>()V

    .line 2761
    new-instance v9, Landroid/graphics/PointF;

    invoke-direct {v9}, Landroid/graphics/PointF;-><init>()V

    .line 2762
    new-instance v10, Landroid/graphics/PointF;

    invoke-direct {v10}, Landroid/graphics/PointF;-><init>()V

    .line 2763
    new-instance v11, Landroid/graphics/PointF;

    invoke-direct {v11}, Landroid/graphics/PointF;-><init>()V

    .line 2772
    const/4 v2, 0x0

    move v6, v2

    :goto_1
    if-lt v6, v7, :cond_3

    .line 3010
    if-eqz p3, :cond_2

    .line 3012
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    :cond_2
    move-object v2, v5

    .line 3015
    goto :goto_0

    .line 2774
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/samm/spenscrap/b/d;

    .line 2775
    iget-object v12, v2, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 2778
    if-nez v6, :cond_6

    add-int/lit8 v3, v7, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/samm/spenscrap/b/d;

    move-object v4, v3

    .line 2780
    :goto_2
    iget-object v13, v4, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 2783
    add-int/lit8 v3, v7, -0x1

    if-ne v6, v3, :cond_7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/samm/spenscrap/b/d;

    .line 2785
    :goto_3
    iget-object v14, v3, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 2791
    iget-boolean v15, v2, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-eqz v15, :cond_8

    if-nez p2, :cond_8

    .line 2793
    if-nez v6, :cond_4

    .line 2795
    iget v2, v12, Landroid/graphics/PointF;->x:F

    iget v4, v12, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2802
    :cond_4
    iget-boolean v2, v3, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-eqz v2, :cond_5

    .line 2803
    iget v2, v14, Landroid/graphics/PointF;->x:F

    iget v3, v14, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2772
    :cond_5
    :goto_4
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_1

    .line 2779
    :cond_6
    add-int/lit8 v3, v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/samm/spenscrap/b/d;

    move-object v4, v3

    goto :goto_2

    .line 2784
    :cond_7
    add-int/lit8 v3, v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/samm/spenscrap/b/d;

    goto :goto_3

    .line 2817
    :cond_8
    iget-boolean v15, v2, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-eqz v15, :cond_c

    if-lez p2, :cond_c

    .line 2819
    const/4 v2, 0x0

    invoke-static {v12, v13, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    .line 2820
    move/from16 v0, p2

    int-to-float v4, v0

    const/high16 v15, 0x40000000    # 2.0f

    div-float v15, v2, v15

    cmpg-float v4, v4, v15

    if-gtz v4, :cond_a

    .line 2821
    move/from16 v0, p2

    int-to-float v4, v0

    div-float v2, v4, v2

    .line 2824
    :goto_5
    iget v4, v13, Landroid/graphics/PointF;->x:F

    mul-float/2addr v4, v2

    iget v15, v12, Landroid/graphics/PointF;->x:F

    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v16, v16, v2

    mul-float v15, v15, v16

    add-float/2addr v4, v15

    iput v4, v8, Landroid/graphics/PointF;->x:F

    .line 2825
    iget v4, v13, Landroid/graphics/PointF;->y:F

    mul-float/2addr v4, v2

    iget v13, v12, Landroid/graphics/PointF;->y:F

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float v2, v15, v2

    mul-float/2addr v2, v13

    add-float/2addr v2, v4

    iput v2, v8, Landroid/graphics/PointF;->y:F

    .line 2827
    const/4 v2, 0x0

    invoke-static {v12, v14, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    .line 2828
    move/from16 v0, p2

    int-to-float v4, v0

    const/high16 v13, 0x40000000    # 2.0f

    div-float v13, v2, v13

    cmpg-float v4, v4, v13

    if-gtz v4, :cond_b

    .line 2829
    move/from16 v0, p2

    int-to-float v4, v0

    div-float v2, v4, v2

    .line 2832
    :goto_6
    iget v4, v12, Landroid/graphics/PointF;->x:F

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v2

    mul-float/2addr v4, v13

    iget v13, v14, Landroid/graphics/PointF;->x:F

    mul-float/2addr v13, v2

    add-float/2addr v4, v13

    iput v4, v9, Landroid/graphics/PointF;->x:F

    .line 2833
    iget v4, v12, Landroid/graphics/PointF;->y:F

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v2

    mul-float/2addr v4, v13

    iget v13, v14, Landroid/graphics/PointF;->y:F

    mul-float/2addr v13, v2

    add-float/2addr v4, v13

    iput v4, v9, Landroid/graphics/PointF;->y:F

    .line 2838
    if-nez v6, :cond_9

    .line 2840
    iget v4, v8, Landroid/graphics/PointF;->x:F

    iget v13, v8, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v4, v13}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2845
    :cond_9
    iget v4, v12, Landroid/graphics/PointF;->x:F

    iget v13, v12, Landroid/graphics/PointF;->y:F

    iget v15, v9, Landroid/graphics/PointF;->x:F

    iget v0, v9, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v5, v4, v13, v15, v0}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 2856
    iget-boolean v3, v3, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-eqz v3, :cond_5

    .line 2857
    iget v3, v12, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v2

    iget v4, v14, Landroid/graphics/PointF;->x:F

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v2

    mul-float/2addr v4, v13

    add-float/2addr v3, v4

    iput v3, v11, Landroid/graphics/PointF;->x:F

    .line 2858
    iget v3, v12, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v2

    iget v4, v14, Landroid/graphics/PointF;->y:F

    const/high16 v12, 0x3f800000    # 1.0f

    sub-float v2, v12, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    iput v2, v11, Landroid/graphics/PointF;->y:F

    .line 2859
    iget v2, v11, Landroid/graphics/PointF;->x:F

    iget v3, v11, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_4

    .line 2823
    :cond_a
    const/high16 v2, 0x3f000000    # 0.5f

    goto/16 :goto_5

    .line 2831
    :cond_b
    const/high16 v2, 0x3f000000    # 0.5f

    goto :goto_6

    .line 2870
    :cond_c
    iget-boolean v15, v2, Lcom/samsung/samm/spenscrap/b/d;->d:Z

    if-nez v15, :cond_14

    .line 2874
    if-nez v6, :cond_d

    .line 2876
    iget-boolean v2, v4, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-eqz v2, :cond_e

    if-nez p2, :cond_e

    .line 2878
    iget v2, v13, Landroid/graphics/PointF;->x:F

    iput v2, v10, Landroid/graphics/PointF;->x:F

    .line 2879
    iget v2, v13, Landroid/graphics/PointF;->y:F

    iput v2, v10, Landroid/graphics/PointF;->y:F

    .line 2896
    :goto_7
    iget v2, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2903
    :cond_d
    iget-boolean v2, v3, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-eqz v2, :cond_11

    if-nez p2, :cond_11

    .line 2904
    iget v2, v14, Landroid/graphics/PointF;->x:F

    iput v2, v11, Landroid/graphics/PointF;->x:F

    .line 2905
    iget v2, v14, Landroid/graphics/PointF;->y:F

    iput v2, v11, Landroid/graphics/PointF;->y:F

    .line 2922
    :goto_8
    iget v2, v12, Landroid/graphics/PointF;->x:F

    iget v3, v12, Landroid/graphics/PointF;->y:F

    iget v4, v11, Landroid/graphics/PointF;->x:F

    iget v12, v11, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v2, v3, v4, v12}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto/16 :goto_4

    .line 2882
    :cond_e
    iget-boolean v2, v3, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-eqz v2, :cond_10

    if-lez p2, :cond_10

    .line 2883
    const/4 v2, 0x0

    invoke-static {v12, v13, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    .line 2884
    move/from16 v0, p2

    int-to-float v4, v0

    const/high16 v15, 0x40000000    # 2.0f

    div-float v15, v2, v15

    cmpg-float v4, v4, v15

    if-gtz v4, :cond_f

    .line 2885
    move/from16 v0, p2

    int-to-float v4, v0

    div-float v2, v4, v2

    .line 2888
    :goto_9
    iget v4, v13, Landroid/graphics/PointF;->x:F

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v15, v2

    mul-float/2addr v4, v15

    iget v15, v12, Landroid/graphics/PointF;->x:F

    mul-float/2addr v15, v2

    add-float/2addr v4, v15

    iput v4, v10, Landroid/graphics/PointF;->x:F

    .line 2889
    iget v4, v13, Landroid/graphics/PointF;->y:F

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v2

    mul-float/2addr v4, v13

    iget v13, v12, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, v13

    add-float/2addr v2, v4

    iput v2, v10, Landroid/graphics/PointF;->y:F

    goto :goto_7

    .line 2887
    :cond_f
    const/high16 v2, 0x3f000000    # 0.5f

    goto :goto_9

    .line 2893
    :cond_10
    iget v2, v13, Landroid/graphics/PointF;->x:F

    iget v4, v12, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    iput v2, v10, Landroid/graphics/PointF;->x:F

    .line 2894
    iget v2, v13, Landroid/graphics/PointF;->y:F

    iget v4, v12, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    iput v2, v10, Landroid/graphics/PointF;->y:F

    goto :goto_7

    .line 2908
    :cond_11
    iget-boolean v2, v3, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    if-eqz v2, :cond_13

    if-lez p2, :cond_13

    .line 2909
    const/4 v2, 0x0

    invoke-static {v12, v14, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    .line 2910
    move/from16 v0, p2

    int-to-float v3, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v2, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_12

    .line 2911
    move/from16 v0, p2

    int-to-float v3, v0

    div-float v2, v3, v2

    .line 2914
    :goto_a
    iget v3, v12, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v2

    iget v4, v14, Landroid/graphics/PointF;->x:F

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v2

    mul-float/2addr v4, v13

    add-float/2addr v3, v4

    iput v3, v11, Landroid/graphics/PointF;->x:F

    .line 2915
    iget v3, v12, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v2

    iget v4, v14, Landroid/graphics/PointF;->y:F

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float v2, v13, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    iput v2, v11, Landroid/graphics/PointF;->y:F

    goto/16 :goto_8

    .line 2913
    :cond_12
    const/high16 v2, 0x3f000000    # 0.5f

    goto :goto_a

    .line 2919
    :cond_13
    iget v2, v12, Landroid/graphics/PointF;->x:F

    iget v3, v14, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iput v2, v11, Landroid/graphics/PointF;->x:F

    .line 2920
    iget v2, v12, Landroid/graphics/PointF;->y:F

    iget v3, v14, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iput v2, v11, Landroid/graphics/PointF;->y:F

    goto/16 :goto_8

    .line 2928
    :cond_14
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 2930
    iget-object v12, v2, Lcom/samsung/samm/spenscrap/b/d;->e:Lcom/samsung/samm/spenscrap/b/b;

    .line 2931
    invoke-virtual {v12}, Lcom/samsung/samm/spenscrap/b/b;->a()Landroid/graphics/RectF;

    move-result-object v13

    .line 2932
    invoke-virtual {v12}, Lcom/samsung/samm/spenscrap/b/b;->b()Landroid/graphics/PointF;

    move-result-object v14

    .line 2933
    invoke-virtual {v12}, Lcom/samsung/samm/spenscrap/b/b;->e()F

    move-result v15

    .line 2934
    iget-object v2, v2, Lcom/samsung/samm/spenscrap/b/d;->e:Lcom/samsung/samm/spenscrap/b/b;

    invoke-virtual {v2}, Lcom/samsung/samm/spenscrap/b/b;->i()Z

    move-result v2

    .line 2936
    if-eqz v2, :cond_15

    .line 2937
    const/4 v2, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    invoke-virtual {v4, v13, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 2938
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 2939
    neg-float v3, v15

    iget v12, v14, Landroid/graphics/PointF;->x:F

    iget v13, v14, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v12, v13}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 2940
    invoke-virtual {v4, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 3002
    :goto_b
    invoke-virtual {v5, v4}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    goto/16 :goto_4

    .line 2944
    :cond_15
    invoke-virtual {v12}, Lcom/samsung/samm/spenscrap/b/b;->c()Landroid/graphics/PointF;

    move-result-object v16

    .line 2945
    invoke-virtual {v12}, Lcom/samsung/samm/spenscrap/b/b;->d()Landroid/graphics/PointF;

    move-result-object v17

    .line 2947
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget v3, v14, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    iget v0, v14, Landroid/graphics/PointF;->x:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    const-wide v18, 0x4066800000000000L    # 180.0

    mul-double v2, v2, v18

    const-wide v18, 0x400921fb54442d18L    # Math.PI

    div-double v2, v2, v18

    double-to-float v2, v2

    .line 2948
    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget v3, v14, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    iget v0, v14, Landroid/graphics/PointF;->x:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    const-wide v18, 0x4066800000000000L    # 180.0

    mul-double v2, v2, v18

    const-wide v18, 0x400921fb54442d18L    # Math.PI

    div-double v2, v2, v18

    double-to-float v2, v2

    .line 2955
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 2956
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v18

    .line 2958
    cmpl-float v3, v2, v18

    if-lez v3, :cond_17

    .line 2959
    const/high16 v3, 0x3f800000    # 1.0f

    .line 2960
    div-float v2, v2, v18

    .line 2966
    :goto_c
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v18, v0

    iget v0, v14, Landroid/graphics/PointF;->y:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    mul-float v18, v18, v2

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    iget v0, v14, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v16, v16, v20

    mul-float v16, v16, v3

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v18

    const-wide v20, 0x4066800000000000L    # 180.0

    mul-double v18, v18, v20

    const-wide v20, 0x400921fb54442d18L    # Math.PI

    div-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v16, v0

    .line 2967
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v18, v0

    iget v0, v14, Landroid/graphics/PointF;->y:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    mul-float v2, v2, v18

    float-to-double v0, v2

    move-wide/from16 v18, v0

    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v0, v14, Landroid/graphics/PointF;->x:F

    move/from16 v17, v0

    sub-float v2, v2, v17

    mul-float/2addr v2, v3

    float-to-double v2, v2

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    const-wide v18, 0x4066800000000000L    # 180.0

    mul-double v2, v2, v18

    const-wide v18, 0x400921fb54442d18L    # Math.PI

    div-double v2, v2, v18

    double-to-float v2, v2

    .line 2971
    sub-float v2, v2, v16

    .line 2972
    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_18

    .line 2973
    invoke-virtual {v12}, Lcom/samsung/samm/spenscrap/b/b;->h()Z

    move-result v3

    if-nez v3, :cond_16

    .line 2977
    const/high16 v3, 0x43b40000    # 360.0f

    sub-float v2, v3, v2

    neg-float v2, v2

    .line 2996
    :cond_16
    :goto_d
    move/from16 v0, v16

    invoke-virtual {v4, v13, v0, v2}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 2997
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 2998
    neg-float v3, v15

    iget v12, v14, Landroid/graphics/PointF;->x:F

    iget v13, v14, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v12, v13}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 2999
    invoke-virtual {v4, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    goto/16 :goto_b

    .line 2963
    :cond_17
    div-float v3, v18, v2

    .line 2964
    const/high16 v2, 0x3f800000    # 1.0f

    goto/16 :goto_c

    .line 2981
    :cond_18
    invoke-virtual {v12}, Lcom/samsung/samm/spenscrap/b/b;->h()Z

    move-result v3

    if-eqz v3, :cond_16

    .line 2982
    const/high16 v3, 0x43b40000    # 360.0f

    add-float/2addr v2, v3

    goto :goto_d
.end method

.method protected makeRectSegmentInfoArray(Landroid/graphics/RectF;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/RectF;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/samm/spenscrap/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 997
    if-nez p1, :cond_1

    .line 998
    const/4 v0, 0x0

    .line 1018
    :cond_0
    return-object v0

    .line 1001
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1003
    new-array v3, v8, [Landroid/graphics/PointF;

    .line 1004
    new-instance v1, Landroid/graphics/PointF;

    iget v4, p1, Landroid/graphics/RectF;->left:F

    iget v5, p1, Landroid/graphics/RectF;->top:F

    invoke-direct {v1, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v3, v2

    .line 1005
    new-instance v1, Landroid/graphics/PointF;

    iget v4, p1, Landroid/graphics/RectF;->right:F

    iget v5, p1, Landroid/graphics/RectF;->top:F

    invoke-direct {v1, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v3, v7

    .line 1006
    const/4 v1, 0x2

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p1, Landroid/graphics/RectF;->right:F

    iget v6, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v1

    .line 1007
    const/4 v1, 0x3

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p1, Landroid/graphics/RectF;->left:F

    iget v6, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v1

    move v1, v2

    .line 1009
    :goto_0
    if-ge v1, v8, :cond_0

    .line 1010
    new-instance v4, Lcom/samsung/samm/spenscrap/b/d;

    invoke-direct {v4}, Lcom/samsung/samm/spenscrap/b/d;-><init>()V

    .line 1011
    aget-object v5, v3, v1

    iput-object v5, v4, Lcom/samsung/samm/spenscrap/b/d;->a:Landroid/graphics/PointF;

    .line 1012
    iput-boolean v7, v4, Lcom/samsung/samm/spenscrap/b/d;->b:Z

    .line 1013
    iput-boolean v2, v4, Lcom/samsung/samm/spenscrap/b/d;->c:Z

    .line 1014
    iput-boolean v2, v4, Lcom/samsung/samm/spenscrap/b/d;->d:Z

    .line 1015
    const/4 v5, -0x1

    iput v5, v4, Lcom/samsung/samm/spenscrap/b/d;->f:I

    .line 1016
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1009
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected makeRoundingPath(Ljava/util/ArrayList;I)Landroid/graphics/Path;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;I)",
            "Landroid/graphics/Path;"
        }
    .end annotation

    .prologue
    .line 2599
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 2600
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 2602
    const/4 v0, 0x1

    if-ge v4, v0, :cond_0

    const/4 v0, 0x0

    .line 2653
    :goto_0
    return-object v0

    .line 2605
    :cond_0
    const/4 v0, 0x1

    if-le p2, v0, :cond_5

    .line 2607
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, p2

    div-float v5, v0, v1

    .line 2611
    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    .line 2612
    new-instance v7, Landroid/graphics/PointF;

    invoke-direct {v7}, Landroid/graphics/PointF;-><init>()V

    .line 2615
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-lt v3, v4, :cond_2

    .line 2651
    :cond_1
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    move-object v0, v2

    .line 2653
    goto :goto_0

    .line 2616
    :cond_2
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/graphics/PointF;

    .line 2617
    if-nez v3, :cond_3

    .line 2618
    add-int/lit8 v0, v4, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2620
    iget v8, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v8, v5

    iget v9, v1, Landroid/graphics/PointF;->x:F

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v5

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iput v8, v7, Landroid/graphics/PointF;->x:F

    .line 2621
    iget v0, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v0, v5

    iget v8, v1, Landroid/graphics/PointF;->y:F

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v9, v5

    mul-float/2addr v8, v9

    add-float/2addr v0, v8

    iput v0, v7, Landroid/graphics/PointF;->y:F

    .line 2622
    iget v0, v7, Landroid/graphics/PointF;->x:F

    iget v8, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2624
    :cond_3
    add-int/lit8 v0, v3, 0x1

    if-lt v0, v4, :cond_4

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2628
    :goto_2
    iget v8, v1, Landroid/graphics/PointF;->x:F

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v9, v5

    mul-float/2addr v8, v9

    iget v9, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v9, v5

    add-float/2addr v8, v9

    iput v8, v6, Landroid/graphics/PointF;->x:F

    .line 2629
    iget v8, v1, Landroid/graphics/PointF;->y:F

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v9, v5

    mul-float/2addr v8, v9

    iget v9, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v9, v5

    add-float/2addr v8, v9

    iput v8, v6, Landroid/graphics/PointF;->y:F

    .line 2630
    iget v8, v1, Landroid/graphics/PointF;->x:F

    iget v9, v1, Landroid/graphics/PointF;->y:F

    iget v10, v6, Landroid/graphics/PointF;->x:F

    iget v11, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 2633
    iget v8, v1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v8, v5

    iget v9, v0, Landroid/graphics/PointF;->x:F

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v5

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iput v8, v7, Landroid/graphics/PointF;->x:F

    .line 2634
    iget v1, v1, Landroid/graphics/PointF;->y:F

    mul-float/2addr v1, v5

    iget v0, v0, Landroid/graphics/PointF;->y:F

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v8, v5

    mul-float/2addr v0, v8

    add-float/2addr v0, v1

    iput v0, v7, Landroid/graphics/PointF;->y:F

    .line 2635
    iget v0, v7, Landroid/graphics/PointF;->x:F

    iget v1, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2615
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    .line 2625
    :cond_4
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    goto :goto_2

    .line 2641
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_1

    .line 2642
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2643
    if-nez v1, :cond_6

    .line 2644
    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2641
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2646
    :cond_6
    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_4
.end method

.method public offsetPoints(FF)V
    .locals 4

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 175
    :cond_0
    return-void

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 169
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 170
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 171
    iget v3, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, p1

    iput v3, v0, Landroid/graphics/PointF;->x:F

    .line 172
    iget v3, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, p2

    iput v3, v0, Landroid/graphics/PointF;->y:F

    .line 173
    iget-object v3, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    invoke-virtual {v3, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 169
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public recognizeSmartClipShape()I
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    const/4 v6, 0x1

    .line 646
    const/4 v3, -0x1

    .line 650
    invoke-virtual {p0, v0, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getCustomSmartClipParameter(IZ)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v4

    .line 651
    iput v6, v4, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 652
    invoke-virtual {p0, v4}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getEnhancedPath(Lcom/samsung/samm/spenscrap/SmartClipParameter;)Landroid/graphics/Path;

    move-result-object v5

    .line 653
    if-eqz v5, :cond_0

    .line 654
    iget v4, v4, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    if-ne v4, v0, :cond_0

    .line 678
    :goto_0
    return v0

    .line 659
    :cond_0
    invoke-virtual {p0, v1, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getCustomSmartClipParameter(IZ)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    .line 660
    iput v6, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 661
    invoke-virtual {p0, v0}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getEnhancedPath(Lcom/samsung/samm/spenscrap/SmartClipParameter;)Landroid/graphics/Path;

    move-result-object v4

    .line 662
    if-eqz v4, :cond_1

    .line 663
    iget v0, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 664
    goto :goto_0

    .line 668
    :cond_1
    invoke-virtual {p0, v6, v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getCustomSmartClipParameter(IZ)Lcom/samsung/samm/spenscrap/SmartClipParameter;

    move-result-object v0

    .line 669
    iput v6, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mCropStyle:I

    .line 670
    invoke-virtual {p0, v0}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->getEnhancedPath(Lcom/samsung/samm/spenscrap/SmartClipParameter;)Landroid/graphics/Path;

    move-result-object v1

    .line 671
    if-eqz v1, :cond_3

    .line 672
    iget v0, v0, Lcom/samsung/samm/spenscrap/SmartClipParameter;->mDetectSmartShape:I

    if-ne v0, v2, :cond_2

    move v0, v2

    .line 673
    goto :goto_0

    .line 675
    :cond_2
    const/4 v0, 0x5

    goto :goto_0

    :cond_3
    move v0, v3

    .line 678
    goto :goto_0
.end method

.method protected removeNearKneePoints(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v8, -0x1

    .line 1620
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 1624
    const/4 v6, 0x0

    move v5, v8

    move v7, v8

    :goto_0
    if-lt v6, v9, :cond_1

    .line 1641
    if-eq v5, v8, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, v7

    .line 1642
    invoke-direct/range {v0 .. v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;III)I

    .line 1644
    :cond_0
    return v10

    .line 1626
    :cond_1
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v10, :cond_4

    .line 1627
    if-ne v5, v8, :cond_2

    .line 1630
    if-ne v7, v8, :cond_3

    move v5, v6

    move v0, v6

    .line 1624
    :goto_1
    add-int/lit8 v6, v6, 0x1

    move v7, v0

    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 1635
    invoke-direct/range {v0 .. v6}, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;III)I

    move-result v5

    move v0, v7

    goto :goto_1

    :cond_3
    move v5, v6

    move v0, v7

    goto :goto_1

    :cond_4
    move v0, v7

    goto :goto_1
.end method

.method public resetPoints()V
    .locals 1

    .prologue
    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    .line 201
    return-void
.end method

.method public savePointDataFile(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 218
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 221
    :try_start_0
    new-instance v4, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v4, p1, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 223
    :try_start_1
    invoke-virtual {v4, v3}, Ljava/io/RandomAccessFile;->writeInt(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v1

    .line 224
    :goto_0
    if-lt v2, v3, :cond_0

    .line 237
    :try_start_2
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3

    .line 247
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 226
    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {v4, v0}, Ljava/io/RandomAccessFile;->writeFloat(F)V

    .line 227
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->mPoints:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v0}, Ljava/io/RandomAccessFile;->writeFloat(F)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 224
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 232
    :catch_1
    move-exception v0

    .line 233
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 237
    :try_start_6
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_3

    move v0, v1

    .line 234
    goto :goto_1

    .line 238
    :catch_2
    move-exception v0

    .line 239
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_3

    move v0, v1

    .line 240
    goto :goto_1

    .line 235
    :catchall_0
    move-exception v0

    .line 237
    :try_start_8
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_3

    .line 242
    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_3

    .line 243
    :catch_3
    move-exception v0

    .line 244
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move v0, v1

    .line 245
    goto :goto_1

    .line 238
    :catch_4
    move-exception v0

    .line 239
    :try_start_a
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v0, v1

    .line 240
    goto :goto_1

    .line 238
    :catch_5
    move-exception v0

    .line 239
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_3

    move v0, v1

    .line 240
    goto :goto_1
.end method

.method public setCustomSmartClipParameter(ILcom/samsung/samm/spenscrap/SmartClipParameter;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 503
    if-nez p1, :cond_0

    .line 504
    iput-object p2, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->a:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    .line 524
    :goto_0
    return v0

    .line 506
    :cond_0
    if-ne p1, v0, :cond_1

    .line 507
    iput-object p2, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->b:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 509
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 510
    iput-object p2, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->c:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 512
    :cond_2
    const/4 v1, 0x3

    if-ne p1, v1, :cond_3

    .line 513
    iput-object p2, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->d:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 515
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_4

    .line 516
    iput-object p2, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->e:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 518
    :cond_4
    const/4 v1, 0x5

    if-ne p1, v1, :cond_5

    .line 519
    iput-object p2, p0, Lcom/samsung/samm/spenscrap/SmartClipLibrary;->f:Lcom/samsung/samm/spenscrap/SmartClipParameter;

    goto :goto_0

    .line 522
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected trimPoints(Ljava/util/ArrayList;F)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;F)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 1699
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v1, v3

    .line 1701
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    .line 1705
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    move-object p1, v6

    .line 1752
    :cond_0
    :goto_1
    return-object p1

    .line 1702
    :cond_1
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v4, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v4, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1701
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1707
    :cond_2
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 1708
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 1710
    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v4

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v5, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    mul-float/2addr v2, v4

    iget v4, v0, Landroid/graphics/PointF;->y:F

    iget v5, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/PointF;->y:F

    iget v7, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v7

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v2, v4

    cmpg-float v2, v2, p2

    if-gez v2, :cond_0

    move v4, v3

    .line 1716
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v4, v2, :cond_5

    move v4, v3

    :goto_3
    move v5, v3

    .line 1727
    :goto_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v5, v2, :cond_7

    move v2, v3

    .line 1737
    :cond_3
    :goto_5
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    if-ge v4, v5, :cond_4

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    div-int/lit8 v5, v5, 0x4

    if-ge v2, v5, :cond_4

    move v5, v3

    .line 1738
    :goto_6
    if-lt v5, v4, :cond_9

    .line 1742
    :goto_7
    if-lt v3, v2, :cond_a

    .line 1746
    new-instance v2, Landroid/graphics/PointF;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    div-float/2addr v3, v10

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    div-float/2addr v0, v10

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object p1, v6

    .line 1749
    goto/16 :goto_1

    .line 1717
    :cond_5
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1719
    iget v5, v0, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v7

    iget v7, v0, Landroid/graphics/PointF;->x:F

    iget v8, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v8

    mul-float/2addr v5, v7

    iget v7, v0, Landroid/graphics/PointF;->y:F

    iget v8, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v7, v8

    iget v8, v0, Landroid/graphics/PointF;->y:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float v2, v8, v2

    mul-float/2addr v2, v7

    add-float/2addr v2, v5

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v2, v8

    div-float v5, p2, v10

    cmpl-float v2, v2, v5

    if-lez v2, :cond_6

    .line 1720
    add-int/lit8 v2, v4, -0x1

    .line 1721
    if-gez v2, :cond_b

    move v4, v3

    .line 1722
    goto :goto_3

    .line 1716
    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 1728
    :cond_7
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v5

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1730
    iget v7, v1, Landroid/graphics/PointF;->x:F

    iget v8, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v8

    iget v8, v1, Landroid/graphics/PointF;->x:F

    iget v9, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    iget v8, v1, Landroid/graphics/PointF;->y:F

    iget v9, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v8, v9

    iget v9, v1, Landroid/graphics/PointF;->y:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float v2, v9, v2

    mul-float/2addr v2, v8

    add-float/2addr v2, v7

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v2, v8

    const/high16 v7, 0x40800000    # 4.0f

    div-float v7, p2, v7

    cmpl-float v2, v2, v7

    if-lez v2, :cond_8

    .line 1731
    add-int/lit8 v2, v5, -0x1

    .line 1732
    if-gez v2, :cond_3

    move v2, v3

    .line 1733
    goto/16 :goto_5

    .line 1727
    :cond_8
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_4

    .line 1739
    :cond_9
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1738
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_6

    .line 1743
    :cond_a
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1742
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_b
    move v4, v2

    goto/16 :goto_3
.end method

.method protected trimPointsByAngle(Ljava/util/ArrayList;Ljava/util/ArrayList;F)Ljava/util/ArrayList;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;F)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1758
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1760
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_1

    .line 1764
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_2

    move-object/from16 p1, v13

    .line 2089
    :cond_0
    :goto_1
    return-object p1

    .line 1761
    :cond_1
    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v5, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v4, v5, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1760
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1766
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1767
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 1769
    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    iget v5, v2, Landroid/graphics/PointF;->x:F

    iget v6, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    iget v5, v2, Landroid/graphics/PointF;->y:F

    iget v6, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    iget v6, v2, Landroid/graphics/PointF;->y:F

    iget v7, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    cmpg-float v4, v4, p3

    if-gez v4, :cond_0

    .line 1771
    const/high16 v4, 0x40000000    # 2.0f

    mul-float v8, p3, v4

    .line 1776
    const/4 v6, 0x0

    .line 1777
    const/4 v7, 0x0

    .line 1781
    const/4 v4, 0x0

    move v5, v4

    :goto_2
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v5, v4, :cond_7

    move v5, v6

    .line 1791
    :goto_3
    const/4 v2, 0x0

    move v4, v2

    :goto_4
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v4, v2, :cond_9

    move v6, v7

    .line 1802
    :goto_5
    const/4 v3, 0x0

    .line 1804
    const/4 v2, 0x0

    move v4, v2

    :goto_6
    if-le v4, v5, :cond_b

    move v2, v3

    .line 1811
    :goto_7
    if-eqz v2, :cond_3

    .line 1812
    const/4 v2, 0x0

    :goto_8
    if-lt v2, v4, :cond_d

    .line 1817
    :cond_3
    const/4 v3, 0x0

    .line 1819
    const/4 v2, 0x0

    move v4, v2

    :goto_9
    if-lt v4, v6, :cond_e

    move v2, v3

    .line 1826
    :goto_a
    if-eqz v2, :cond_4

    .line 1827
    const/4 v2, 0x0

    :goto_b
    if-lt v2, v4, :cond_10

    .line 1837
    :cond_4
    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1838
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 1840
    const/4 v4, 0x0

    move v7, v4

    :goto_c
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v7, v4, :cond_11

    .line 1850
    :cond_5
    :goto_d
    const/4 v2, 0x0

    move v4, v2

    :goto_e
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v4, v2, :cond_13

    .line 1870
    :cond_6
    :goto_f
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1872
    const/4 v2, 0x0

    move v4, v2

    :goto_10
    if-lt v4, v5, :cond_15

    .line 1881
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_17

    .line 1882
    const-string v2, "SmartClipLibrary"

    const-string v3, "Front TmpArray size error"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 p1, v13

    .line 1884
    goto/16 :goto_1

    .line 1782
    :cond_7
    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 1784
    iget v9, v2, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v10

    iget v10, v2, Landroid/graphics/PointF;->x:F

    iget v11, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v10, v11

    mul-float/2addr v9, v10

    iget v10, v2, Landroid/graphics/PointF;->y:F

    iget v11, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v11

    iget v11, v2, Landroid/graphics/PointF;->y:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float v4, v11, v4

    mul-float/2addr v4, v10

    add-float/2addr v4, v9

    float-to-double v10, v4

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v4, v10

    cmpl-float v4, v4, v8

    if-lez v4, :cond_8

    .line 1785
    add-int/lit8 v2, v5, -0x1

    .line 1786
    if-gez v2, :cond_2b

    const/4 v2, 0x0

    move v5, v2

    .line 1787
    goto/16 :goto_3

    .line 1781
    :cond_8
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_2

    .line 1792
    :cond_9
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v4

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1794
    iget v6, v3, Landroid/graphics/PointF;->x:F

    iget v9, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v6, v9

    iget v9, v3, Landroid/graphics/PointF;->x:F

    iget v10, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v10

    mul-float/2addr v6, v9

    iget v9, v3, Landroid/graphics/PointF;->y:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v9, v10

    iget v10, v3, Landroid/graphics/PointF;->y:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float v2, v10, v2

    mul-float/2addr v2, v9

    add-float/2addr v2, v6

    float-to-double v10, v2

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v2, v10

    cmpl-float v2, v2, v8

    if-lez v2, :cond_a

    .line 1795
    add-int/lit8 v2, v4, -0x1

    .line 1796
    if-gez v2, :cond_2a

    const/4 v2, 0x0

    move v6, v2

    .line 1797
    goto/16 :goto_5

    .line 1791
    :cond_a
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_4

    .line 1805
    :cond_b
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1806
    const/4 v2, 0x1

    .line 1807
    goto/16 :goto_7

    .line 1804
    :cond_c
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_6

    .line 1813
    :cond_d
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1812
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_8

    .line 1820
    :cond_e
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1821
    const/4 v2, 0x1

    .line 1822
    goto/16 :goto_a

    .line 1819
    :cond_f
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_9

    .line 1828
    :cond_10
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1827
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_b

    .line 1841
    :cond_11
    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 1843
    iget v9, v2, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v10

    iget v10, v2, Landroid/graphics/PointF;->x:F

    iget v11, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v10, v11

    mul-float/2addr v9, v10

    iget v10, v2, Landroid/graphics/PointF;->y:F

    iget v11, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v11

    iget v11, v2, Landroid/graphics/PointF;->y:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float v4, v11, v4

    mul-float/2addr v4, v10

    add-float/2addr v4, v9

    float-to-double v10, v4

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v4, v10

    cmpl-float v4, v4, v8

    if-lez v4, :cond_12

    .line 1844
    add-int/lit8 v5, v7, -0x1

    .line 1845
    if-gez v5, :cond_5

    const/4 v5, 0x0

    .line 1846
    goto/16 :goto_d

    .line 1840
    :cond_12
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto/16 :goto_c

    .line 1851
    :cond_13
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v4

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1853
    iget v7, v3, Landroid/graphics/PointF;->x:F

    iget v9, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v9

    iget v9, v3, Landroid/graphics/PointF;->x:F

    iget v10, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v10

    mul-float/2addr v7, v9

    iget v9, v3, Landroid/graphics/PointF;->y:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v9, v10

    iget v10, v3, Landroid/graphics/PointF;->y:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float v2, v10, v2

    mul-float/2addr v2, v9

    add-float/2addr v2, v7

    float-to-double v10, v2

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v2, v10

    cmpl-float v2, v2, v8

    if-lez v2, :cond_14

    .line 1854
    add-int/lit8 v6, v4, -0x1

    .line 1855
    if-gez v6, :cond_6

    const/4 v6, 0x0

    .line 1856
    goto/16 :goto_f

    .line 1850
    :cond_14
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_e

    .line 1873
    :cond_15
    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1874
    add-int/lit8 v3, v4, 0x1

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 1876
    iget v7, v2, Landroid/graphics/PointF;->x:F

    iget v9, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const/high16 v9, 0x800000

    cmpg-float v7, v7, v9

    if-gez v7, :cond_16

    iget v7, v2, Landroid/graphics/PointF;->y:F

    iget v9, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const/high16 v9, 0x800000

    cmpg-float v7, v7, v9

    if-gez v7, :cond_16

    .line 1872
    :goto_11
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_10

    .line 1878
    :cond_16
    const/4 v7, 0x1

    invoke-static {v2, v3, v7}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_11

    .line 1887
    :cond_17
    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1889
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v2, v2, 0x2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_18

    .line 1890
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    move v4, v2

    .line 1904
    :goto_12
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 1906
    const/4 v2, 0x0

    move v7, v2

    :goto_13
    if-lt v7, v6, :cond_1b

    .line 1918
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_1d

    .line 1919
    const-string v2, "SmartClipLibrary"

    const-string v3, "Back TmpArray size error"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 p1, v13

    .line 1921
    goto/16 :goto_1

    .line 1893
    :cond_18
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x1

    .line 1894
    if-gez v2, :cond_19

    const/4 v2, 0x0

    .line 1896
    :cond_19
    add-int/lit8 v3, v2, 0x1

    .line 1898
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_1a

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 1900
    :cond_1a
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    add-float/2addr v2, v4

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move v4, v2

    goto :goto_12

    .line 1908
    :cond_1b
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    sub-int/2addr v2, v7

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 1909
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v7

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 1911
    iget v9, v2, Landroid/graphics/PointF;->x:F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x800000

    cmpg-float v9, v9, v10

    if-gez v9, :cond_1c

    iget v9, v2, Landroid/graphics/PointF;->y:F

    iget v10, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x800000

    cmpg-float v9, v9, v10

    if-gez v9, :cond_1c

    .line 1906
    :goto_14
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto/16 :goto_13

    .line 1913
    :cond_1c
    const/4 v9, 0x1

    invoke-static {v2, v3, v9}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_14

    .line 1924
    :cond_1d
    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1926
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v2, v2, 0x2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1e

    .line 1927
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    move v14, v2

    .line 1944
    :goto_15
    sub-float v2, v4, v14

    .line 1947
    const/4 v3, 0x0

    cmpg-float v3, v2, v3

    if-gez v3, :cond_27

    const/high16 v3, 0x43b40000    # 360.0f

    add-float/2addr v2, v3

    move v7, v2

    .line 1950
    :goto_16
    const-string v2, "SmartClipLibrary"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "fTargetAngle : "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1952
    const/4 v11, 0x0

    .line 1953
    const/4 v9, 0x0

    .line 1954
    const/high16 v8, -0x40800000    # -1.0f

    .line 1956
    const/4 v10, 0x0

    :goto_17
    if-lt v10, v5, :cond_20

    .line 2022
    const-string v2, "SmartClipLibrary"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "nMinIndex(front) : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2023
    const-string v2, "SmartClipLibrary"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "nMinIndex(back) : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2025
    const/4 v2, 0x0

    :goto_18
    if-lt v2, v9, :cond_24

    .line 2029
    const/4 v2, 0x0

    :goto_19
    if-lt v2, v11, :cond_25

    move-object/from16 p1, v13

    .line 2086
    goto/16 :goto_1

    .line 1930
    :cond_1e
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v3, v2, -0x1

    .line 1931
    add-int/lit8 v2, v3, 0x1

    .line 1933
    if-gez v3, :cond_29

    const/4 v3, 0x0

    move v7, v3

    .line 1934
    :goto_1a
    if-gez v2, :cond_1f

    const/4 v2, 0x0

    .line 1935
    :cond_1f
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_28

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    .line 1937
    :goto_1b
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    add-float/2addr v2, v7

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move v14, v2

    goto/16 :goto_15

    .line 1957
    :cond_20
    const/4 v12, 0x0

    :goto_1c
    if-lt v12, v6, :cond_21

    .line 1956
    add-int/lit8 v10, v10, 0x1

    goto :goto_17

    .line 1959
    :cond_21
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v12

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    const/4 v15, 0x1

    invoke-static {v2, v3, v15}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v3

    .line 1961
    const-string v2, "SmartClipLibrary"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "fAngle : "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v2, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1983
    sub-float v2, v4, v3

    .line 1984
    sub-float v3, v14, v3

    .line 1986
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v15

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v16

    cmpl-float v15, v15, v16

    if-lez v15, :cond_23

    .line 1998
    :goto_1d
    const-string v3, "SmartClipLibrary"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "fAngle(max) : "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v3, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2000
    invoke-static {v7, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->a(FF)F

    move-result v2

    .line 2012
    const-string v3, "SmartClipLibrary"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "fDiff : "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v3, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2014
    const/4 v3, 0x0

    cmpg-float v3, v8, v3

    if-ltz v3, :cond_22

    cmpg-float v3, v2, v8

    if-gez v3, :cond_26

    :cond_22
    move v3, v10

    move v8, v12

    .line 1957
    :goto_1e
    add-int/lit8 v12, v12, 0x1

    move v9, v3

    move v11, v8

    move v8, v2

    goto :goto_1c

    :cond_23
    move v2, v3

    .line 1990
    goto :goto_1d

    .line 2026
    :cond_24
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2025
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_18

    .line 2030
    :cond_25
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2029
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_19

    :cond_26
    move v2, v8

    move v3, v9

    move v8, v11

    goto :goto_1e

    :cond_27
    move v7, v2

    goto/16 :goto_16

    :cond_28
    move v3, v2

    goto/16 :goto_1b

    :cond_29
    move v7, v3

    goto/16 :goto_1a

    :cond_2a
    move v6, v2

    goto/16 :goto_5

    :cond_2b
    move v5, v2

    goto/16 :goto_3
.end method

.method protected trimPointsByCrossPoint(Ljava/util/ArrayList;F)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;F)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2096
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2098
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    .line 2102
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    move-object v4, v7

    .line 2243
    :cond_0
    :goto_1
    return-object v4

    .line 2099
    :cond_1
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2098
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2103
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_3

    move-object v4, v7

    goto :goto_1

    .line 2105
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 2106
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 2108
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 2109
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 2111
    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    iget v5, v2, Landroid/graphics/PointF;->x:F

    iget v6, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    iget v5, v2, Landroid/graphics/PointF;->y:F

    iget v6, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    iget v6, v2, Landroid/graphics/PointF;->y:F

    iget v8, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v8

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    cmpg-float v4, v4, p2

    if-gez v4, :cond_b

    .line 2119
    const/4 v6, 0x0

    .line 2120
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v8, v4, -0x1

    .line 2124
    const/4 v4, 0x0

    move v5, v4

    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v5, v4, :cond_6

    move v4, v6

    .line 2134
    :goto_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v5, v2

    :goto_4
    if-gez v5, :cond_8

    move v5, v8

    .line 2144
    :goto_5
    if-ge v4, v5, :cond_c

    .line 2145
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2147
    :goto_6
    if-le v4, v5, :cond_a

    move-object v4, v3

    .line 2188
    :goto_7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_0

    .line 2193
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 2194
    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 2196
    iget v5, v2, Landroid/graphics/PointF;->x:F

    iget v6, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x800000

    cmpg-float v5, v5, v6

    if-gez v5, :cond_4

    iget v5, v2, Landroid/graphics/PointF;->y:F

    iget v6, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x800000

    cmpg-float v5, v5, v6

    if-gez v5, :cond_4

    .line 2198
    :cond_4
    const/4 v5, 0x1

    invoke-static {v2, v3, v5}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v5

    .line 2200
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 2201
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 2203
    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v7, 0x800000

    cmpg-float v6, v6, v7

    if-gez v6, :cond_5

    iget v6, v2, Landroid/graphics/PointF;->y:F

    iget v7, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v7, 0x800000

    cmpg-float v6, v6, v7

    if-gez v6, :cond_5

    const/4 v5, 0x0

    .line 2205
    :cond_5
    const/4 v6, 0x1

    invoke-static {v2, v3, v6}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    .line 2208
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 2214
    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    double-to-float v3, v6

    .line 2215
    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    double-to-float v5, v6

    .line 2217
    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    sub-float v2, v3, v5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v6, 0x800000

    cmpl-float v2, v2, v6

    if-lez v2, :cond_0

    .line 2221
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v6, v2, Landroid/graphics/PointF;->y:F

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v2, v3

    sub-float/2addr v6, v2

    .line 2222
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v2, v5

    sub-float v2, v7, v2

    .line 2224
    sub-float/2addr v2, v6

    sub-float v5, v3, v5

    div-float/2addr v2, v5

    .line 2225
    mul-float/2addr v3, v2

    add-float/2addr v3, v6

    .line 2227
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v7, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-direct {v5, v6, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2229
    const/high16 v0, 0x3fc00000    # 1.5f

    mul-float/2addr v0, p2

    .line 2232
    iget v1, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v6, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v6, v2

    mul-float/2addr v1, v6

    iget v6, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v3

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v3

    mul-float/2addr v5, v6

    add-float/2addr v1, v5

    float-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v1, v6

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    .line 2233
    const/4 v0, 0x0

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2234
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2125
    :cond_6
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 2127
    iget v9, v2, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v10

    iget v10, v2, Landroid/graphics/PointF;->x:F

    iget v11, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v10, v11

    mul-float/2addr v9, v10

    iget v10, v2, Landroid/graphics/PointF;->y:F

    iget v11, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v11

    iget v11, v2, Landroid/graphics/PointF;->y:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float v4, v11, v4

    mul-float/2addr v4, v10

    add-float/2addr v4, v9

    float-to-double v10, v4

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v4, v10

    cmpl-float v4, v4, p2

    if-lez v4, :cond_7

    .line 2128
    add-int/lit8 v2, v5, -0x1

    .line 2129
    if-gez v2, :cond_e

    const/4 v2, 0x0

    move v4, v2

    .line 2130
    goto/16 :goto_3

    .line 2124
    :cond_7
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_2

    .line 2135
    :cond_8
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 2137
    iget v6, v3, Landroid/graphics/PointF;->x:F

    iget v9, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v6, v9

    iget v9, v3, Landroid/graphics/PointF;->x:F

    iget v10, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v10

    mul-float/2addr v6, v9

    iget v9, v3, Landroid/graphics/PointF;->y:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v9, v10

    iget v10, v3, Landroid/graphics/PointF;->y:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float v2, v10, v2

    mul-float/2addr v2, v9

    add-float/2addr v2, v6

    float-to-double v10, v2

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v2, v10

    cmpl-float v2, v2, p2

    if-lez v2, :cond_9

    .line 2138
    add-int/lit8 v2, v5, 0x1

    .line 2139
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_d

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v5, v2

    .line 2140
    goto/16 :goto_5

    .line 2134
    :cond_9
    add-int/lit8 v2, v5, -0x1

    move v5, v2

    goto/16 :goto_4

    .line 2148
    :cond_a
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2147
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_6

    :cond_b
    move-object v4, p1

    .line 2243
    goto/16 :goto_1

    :cond_c
    move-object v4, v7

    goto/16 :goto_7

    :cond_d
    move v5, v2

    goto/16 :goto_5

    :cond_e
    move v4, v2

    goto/16 :goto_3
.end method
