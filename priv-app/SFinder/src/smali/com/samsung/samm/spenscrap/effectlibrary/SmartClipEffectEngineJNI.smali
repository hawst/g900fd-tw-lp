.class public Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngineJNI;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "SmartClipEffect"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native getOutlinePath(Landroid/graphics/Bitmap;I)[I
.end method

.method public static native makeImageShadow(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIII)V
.end method

.method public static native setCheckSupportingModel([CI)V
.end method
