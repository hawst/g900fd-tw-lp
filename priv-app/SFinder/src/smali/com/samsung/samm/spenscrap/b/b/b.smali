.class public Lcom/samsung/samm/spenscrap/b/b/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/samm/spenscrap/b/b/c;


# instance fields
.field public a:D

.field public b:D

.field public c:D

.field public d:D

.field public e:D

.field public f:D

.field public g:D

.field public h:D

.field public i:D

.field public j:D

.field public k:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/PointF;)D
    .locals 8

    .prologue
    .line 14
    iget v0, p1, Landroid/graphics/PointF;->x:F

    float-to-double v0, v0

    .line 15
    iget v2, p1, Landroid/graphics/PointF;->y:F

    float-to-double v2, v2

    .line 17
    iget-wide v4, p0, Lcom/samsung/samm/spenscrap/b/b/b;->a:D

    mul-double/2addr v4, v0

    mul-double/2addr v4, v0

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    mul-double/2addr v6, v0

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->c:D

    mul-double/2addr v6, v2

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->d:D

    mul-double/2addr v0, v6

    add-double/2addr v0, v4

    iget-wide v4, p0, Lcom/samsung/samm/spenscrap/b/b/b;->e:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/samsung/samm/spenscrap/b/b/b;->f:D

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 18
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public a()Z
    .locals 12

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    iget-wide v2, p0, Lcom/samsung/samm/spenscrap/b/b/b;->a:D

    iget-wide v4, p0, Lcom/samsung/samm/spenscrap/b/b/b;->c:D

    sub-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/samm/spenscrap/b/b/b;->k:D

    .line 28
    iget-wide v0, p0, Lcom/samsung/samm/spenscrap/b/b/b;->k:D

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 29
    iget-wide v2, p0, Lcom/samsung/samm/spenscrap/b/b/b;->k:D

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 30
    iget-wide v4, p0, Lcom/samsung/samm/spenscrap/b/b/b;->a:D

    mul-double/2addr v4, v0

    mul-double/2addr v4, v0

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    mul-double/2addr v6, v0

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->c:D

    mul-double/2addr v6, v2

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    .line 31
    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->a:D

    mul-double/2addr v6, v2

    mul-double/2addr v6, v2

    iget-wide v8, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    mul-double/2addr v8, v0

    mul-double/2addr v2, v8

    sub-double v2, v6, v2

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->c:D

    mul-double/2addr v6, v0

    mul-double/2addr v0, v6

    add-double/2addr v0, v2

    .line 34
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->c:D

    mul-double/2addr v2, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->d:D

    mul-double/2addr v2, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    iget-wide v8, p0, Lcom/samsung/samm/spenscrap/b/b/b;->e:D

    mul-double/2addr v6, v8

    sub-double/2addr v2, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    iget-wide v8, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    iget-wide v10, p0, Lcom/samsung/samm/spenscrap/b/b/b;->a:D

    mul-double/2addr v8, v10

    iget-wide v10, p0, Lcom/samsung/samm/spenscrap/b/b/b;->c:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    div-double/2addr v2, v6

    iput-wide v2, p0, Lcom/samsung/samm/spenscrap/b/b/b;->g:D

    .line 35
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->a:D

    mul-double/2addr v2, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->e:D

    mul-double/2addr v2, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    iget-wide v8, p0, Lcom/samsung/samm/spenscrap/b/b/b;->d:D

    mul-double/2addr v6, v8

    sub-double/2addr v2, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    iget-wide v8, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    iget-wide v10, p0, Lcom/samsung/samm/spenscrap/b/b/b;->a:D

    mul-double/2addr v8, v10

    iget-wide v10, p0, Lcom/samsung/samm/spenscrap/b/b/b;->c:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    div-double/2addr v2, v6

    iput-wide v2, p0, Lcom/samsung/samm/spenscrap/b/b/b;->h:D

    .line 38
    iget-wide v2, p0, Lcom/samsung/samm/spenscrap/b/b/b;->a:D

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->g:D

    mul-double/2addr v2, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->g:D

    mul-double/2addr v2, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->b:D

    iget-wide v8, p0, Lcom/samsung/samm/spenscrap/b/b/b;->g:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lcom/samsung/samm/spenscrap/b/b/b;->h:D

    mul-double/2addr v6, v8

    add-double/2addr v2, v6

    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->c:D

    iget-wide v8, p0, Lcom/samsung/samm/spenscrap/b/b/b;->h:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lcom/samsung/samm/spenscrap/b/b/b;->h:D

    mul-double/2addr v6, v8

    add-double/2addr v2, v6

    .line 39
    iget-wide v6, p0, Lcom/samsung/samm/spenscrap/b/b/b;->f:D

    sub-double/2addr v2, v6

    .line 41
    div-double v6, v2, v4

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-lez v6, :cond_0

    div-double v6, v2, v0

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_1

    .line 43
    :cond_0
    const/4 v0, 0x0

    .line 48
    :goto_0
    return v0

    .line 46
    :cond_1
    div-double v4, v2, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/samm/spenscrap/b/b/b;->i:D

    .line 47
    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/samm/spenscrap/b/b/b;->j:D

    .line 48
    const/4 v0, 0x1

    goto :goto_0
.end method
