.class public Lcom/samsung/samm/spenscrap/ClipAppInfo;
.super Lcom/samsung/samm/a/a;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Lcom/samsung/samm/a/a;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/samsung/samm/spenscrap/ClipAppInfo;->a:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/samsung/samm/spenscrap/ClipAppInfo;->b:Landroid/graphics/Bitmap;

    .line 25
    return-void
.end method


# virtual methods
.method public getAppBitmapIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipAppInfo;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getAppTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipAppInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public setAppBitmapIcon(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/samm/spenscrap/ClipAppInfo;->b:Landroid/graphics/Bitmap;

    .line 61
    return-void
.end method

.method public setAppTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/samm/spenscrap/ClipAppInfo;->a:Ljava/lang/String;

    .line 43
    return-void
.end method
