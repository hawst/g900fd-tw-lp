.class public Lcom/samsung/samm/spenscrap/ClipSAMMLib;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static final APPLICATION_ID_NAME:Ljava/lang/String; = "Scrap"

.field protected static final APPLICATION_ID_VERSION_PATCHNAME:Ljava/lang/String; = "Debug"

.field protected static final DEFAULT_CANCAS_SIZE:I = 0x64

.field public static final KEY_APPINFO_CLASS_NAME:Ljava/lang/String; = "AppInfoClassName"

.field public static final KEY_APPINFO_PACKAGE_NAME:Ljava/lang/String; = "AppInfoPackageName"

.field public static final KEY_APPINFO_TYPE:Ljava/lang/String; = "AppInfoType"

.field protected static mAppVersionMajor:I

.field protected static mAppVersionMinor:I


# instance fields
.field private a:Lcom/samsung/samm/b/a;

.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    sput v0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->mAppVersionMajor:I

    .line 41
    const/4 v0, 0x0

    sput v0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->mAppVersionMinor:I

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    iput-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    .line 211
    iput-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->b:Ljava/lang/String;

    .line 15
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 499
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    if-nez v1, :cond_0

    .line 500
    const-string v1, "ClipSAMMLib"

    const-string v2, "Clip SAMM library is not opened"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    :goto_0
    return v0

    .line 503
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 504
    :cond_1
    const-string v1, "ClipSAMMLib"

    const-string v2, "Key must be specified"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 507
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static createSAMMLibrary(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/b/a;
    .locals 7

    .prologue
    const/16 v2, 0x64

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 111
    if-eqz p1, :cond_0

    .line 112
    invoke-static {p1}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;)Z

    move-result v0

    .line 119
    :goto_0
    if-eqz v0, :cond_1

    .line 120
    invoke-static {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->getSAMMFileInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/a/b;

    move-result-object v0

    .line 122
    iget v2, v0, Lcom/samsung/samm/a/b;->f:I

    .line 123
    iget v3, v0, Lcom/samsung/samm/a/b;->g:I

    .line 126
    new-instance v0, Lcom/samsung/samm/b/a;

    move-object v1, p0

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/samm/b/a;-><init>(Landroid/content/Context;IIZZLjava/lang/String;)V

    .line 128
    invoke-virtual {v0, p1, v6, v4}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 129
    const-string v0, "ClipSAMMLib"

    const-string v1, "Fail to load SAMM data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :goto_1
    return-object v6

    :cond_0
    move v0, v4

    .line 115
    goto :goto_0

    .line 136
    :cond_1
    const-string v0, "Scrap"

    sget v1, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->mAppVersionMajor:I

    sget v3, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->mAppVersionMinor:I

    const-string v5, "Debug"

    invoke-static {v0, v1, v3, v5}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;IILjava/lang/String;)Z

    .line 138
    new-instance v0, Lcom/samsung/samm/b/a;

    move-object v1, p0

    move v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/samm/b/a;-><init>(Landroid/content/Context;IIZZLjava/lang/String;)V

    :cond_2
    move-object v6, v0

    .line 140
    goto :goto_1
.end method

.method protected static getSAMMFileInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/a/b;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 150
    new-instance v0, Lcom/samsung/samm/a/c;

    invoke-direct {v0}, Lcom/samsung/samm/a/c;-><init>()V

    .line 152
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->b:Z

    .line 153
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->a:Z

    .line 155
    iput-boolean v2, v0, Lcom/samsung/samm/a/c;->g:Z

    .line 156
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->c:Z

    .line 157
    iput-boolean v2, v0, Lcom/samsung/samm/a/c;->h:Z

    .line 158
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->i:Z

    .line 159
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->j:Z

    .line 160
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->k:Z

    .line 161
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->l:Z

    .line 162
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->m:Z

    .line 163
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->n:Z

    .line 164
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->o:Z

    .line 165
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->p:Z

    .line 166
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->q:Z

    .line 168
    invoke-static {p0, p1, v0}, Lcom/samsung/samm/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/samm/a/c;)Lcom/samsung/samm/a/b;

    move-result-object v0

    return-object v0
.end method

.method protected static getSAMMFileInfoForAppVersion(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/a/b;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 172
    new-instance v0, Lcom/samsung/samm/a/c;

    invoke-direct {v0}, Lcom/samsung/samm/a/c;-><init>()V

    .line 174
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->b:Z

    .line 175
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->a:Z

    .line 177
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->g:Z

    .line 178
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->c:Z

    .line 179
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->h:Z

    .line 180
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->i:Z

    .line 181
    iput-boolean v2, v0, Lcom/samsung/samm/a/c;->j:Z

    .line 182
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->k:Z

    .line 183
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->l:Z

    .line 184
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->m:Z

    .line 185
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->n:Z

    .line 186
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->o:Z

    .line 187
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->p:Z

    .line 188
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->q:Z

    .line 189
    iput-boolean v2, v0, Lcom/samsung/samm/a/c;->r:Z

    .line 190
    invoke-static {p0, p1, v0}, Lcom/samsung/samm/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/samm/a/c;)Lcom/samsung/samm/a/b;

    move-result-object v0

    return-object v0
.end method

.method public static isSAMMClipImage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-static {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->getSAMMFileInfoForAppVersion(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/a/b;

    move-result-object v1

    .line 65
    if-nez v1, :cond_1

    .line 66
    const-string v1, "ClipSAMMLib"

    const-string v2, "Fail to extract SAMM file information"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    iget-boolean v2, v1, Lcom/samsung/samm/a/b;->a:Z

    if-eqz v2, :cond_0

    .line 74
    iget-object v2, v1, Lcom/samsung/samm/a/b;->n:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 75
    const-string v2, "Scrap"

    iget-object v3, v1, Lcom/samsung/samm/a/b;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_3

    .line 76
    :cond_2
    const-string v1, "ClipSAMMLib"

    const-string v2, "This is SAMM file, but not SAMM Clip."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 81
    :cond_3
    iget-boolean v1, v1, Lcom/samsung/samm/a/b;->E:Z

    if-nez v1, :cond_4

    .line 82
    const-string v1, "ClipSAMMLib"

    const-string v2, "This is SAMM Clip file, but it does not contain ClipAppInfo."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setAppVersion(II)V
    .locals 0

    .prologue
    .line 96
    sput p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->mAppVersionMajor:I

    .line 97
    sput p1, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->mAppVersionMinor:I

    .line 98
    return-void
.end method


# virtual methods
.method public closeClipSAMMLibrary()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 275
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    if-nez v1, :cond_0

    .line 276
    const-string v1, "ClipSAMMLib"

    const-string v2, "Clip SAMM library is not opened"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :goto_0
    return v0

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v1}, Lcom/samsung/samm/b/a;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 282
    const-string v1, "ClipSAMMLib"

    const-string v2, "Fail to close Clip SAMM library"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 286
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->b:Ljava/lang/String;

    .line 287
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getApplicationInfo()Lcom/samsung/samm/spenscrap/ClipAppInfo;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 313
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    if-nez v1, :cond_1

    .line 314
    const-string v1, "ClipSAMMLib"

    const-string v2, "Clip SAMM library is not opened"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :cond_0
    :goto_0
    return-object v0

    .line 319
    :cond_1
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v1}, Lcom/samsung/samm/b/a;->d()Lcom/samsung/samm/a/a;

    move-result-object v1

    .line 320
    if-eqz v1, :cond_0

    .line 323
    new-instance v0, Lcom/samsung/samm/spenscrap/ClipAppInfo;

    invoke-direct {v0}, Lcom/samsung/samm/spenscrap/ClipAppInfo;-><init>()V

    .line 324
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppType()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppType(I)V

    .line 325
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppURL(Ljava/lang/String;)V

    .line 326
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppSrcPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppSrcPath(Ljava/lang/String;)V

    .line 327
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppTime(J)V

    .line 328
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppPackageName(Ljava/lang/String;)V

    .line 329
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppClassName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getByteArrayExtra(Ljava/lang/String;[B)[B
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 493
    :goto_0
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;[B)[B

    move-result-object p2

    goto :goto_0
.end method

.method public getIntegerArrayExtra(Ljava/lang/String;[I)[I
    .locals 1

    .prologue
    .line 424
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 428
    :goto_0
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;[I)[I

    move-result-object p2

    goto :goto_0
.end method

.method public getIntegerExtra(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 363
    :goto_0
    return p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;I)I

    move-result p2

    goto :goto_0
.end method

.method public getStringArrayExtra(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 457
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 461
    :goto_0
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public getStringExtra(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    :goto_0
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public openClipSAMMLibrary(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->closeClipSAMMLibrary()Z

    .line 226
    invoke-static {p1, p2}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->createSAMMLibrary(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    .line 227
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    if-nez v0, :cond_0

    .line 228
    const-string v0, "ClipSAMMLib"

    const-string v1, "Fail to create SAMM Library"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/4 v0, 0x0

    .line 234
    :goto_0
    return v0

    .line 232
    :cond_0
    iput-object p2, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->b:Ljava/lang/String;

    .line 234
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public putApplicationInfo(Lcom/samsung/samm/spenscrap/ClipAppInfo;)Z
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    if-nez v0, :cond_0

    .line 298
    const-string v0, "ClipSAMMLib"

    const-string v1, "Clip SAMM library is not opened"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    const/4 v0, 0x0

    .line 304
    :goto_0
    return v0

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1}, Lcom/samsung/samm/b/a;->a(Lcom/samsung/samm/a/a;)V

    .line 304
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public putByteArrayExtra(Ljava/lang/String;[B)Z
    .locals 1

    .prologue
    .line 475
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 476
    const/4 v0, 0x0

    .line 479
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;[B)Z

    move-result v0

    goto :goto_0
.end method

.method public putIntegerArrayExtra(Ljava/lang/String;[I)Z
    .locals 1

    .prologue
    .line 409
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 410
    const/4 v0, 0x0

    .line 413
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;[I)Z

    move-result v0

    goto :goto_0
.end method

.method public putIntegerExtra(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 345
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    const/4 v0, 0x0

    .line 349
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public putStringArrayExtra(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 442
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    const/4 v0, 0x0

    .line 446
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public putStringExtra(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 377
    invoke-direct {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    const/4 v0, 0x0

    .line 381
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public saveClipSAMMFile(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 244
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    if-nez v1, :cond_0

    .line 245
    const-string v1, "ClipSAMMLib"

    const-string v2, "Clip SAMM library is not opened"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :goto_0
    return v0

    .line 250
    :cond_0
    if-eqz p1, :cond_1

    .line 255
    :goto_1
    if-nez p1, :cond_2

    .line 256
    const-string v1, "ClipSAMMLib"

    const-string v2, "File name is not specified."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 253
    :cond_1
    iget-object p1, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->b:Ljava/lang/String;

    goto :goto_1

    .line 261
    :cond_2
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/ClipSAMMLib;->a:Lcom/samsung/samm/b/a;

    invoke-virtual {v1, p1}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 262
    const-string v1, "ClipSAMMLib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fail to save as Clip SAMM file : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 266
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
