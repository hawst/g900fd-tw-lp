.class public Lcom/samsung/samm/spenscrap/b/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/samm/spenscrap/b/a/a$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;F)Landroid/graphics/PointF;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 140
    invoke-static {p0, p2, v3}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v1

    .line 141
    cmpg-float v2, v1, p3

    if-gez v2, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-object v0

    .line 145
    :cond_1
    invoke-static {p1, p2, v3}, Lcom/samsung/samm/spenscrap/b/c/b;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Z)F

    move-result v2

    .line 146
    cmpg-float v3, v2, p3

    if-ltz v3, :cond_0

    .line 152
    add-float v0, v1, v2

    div-float/2addr v1, v0

    .line 155
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 156
    iget v2, p2, Landroid/graphics/PointF;->x:F

    sub-float v3, v5, v1

    sub-float v4, v5, v1

    mul-float/2addr v3, v4

    iget v4, p0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float v3, v1, v1

    iget v4, p1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    sub-float v3, v5, v1

    mul-float/2addr v3, v6

    mul-float/2addr v3, v1

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 157
    iget v2, p2, Landroid/graphics/PointF;->y:F

    sub-float v3, v5, v1

    sub-float v4, v5, v1

    mul-float/2addr v3, v4

    iget v4, p0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float v3, v1, v1

    iget v4, p1, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    sub-float v3, v5, v1

    mul-float/2addr v3, v6

    mul-float/2addr v1, v3

    div-float v1, v2, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;IIF)Lcom/samsung/samm/spenscrap/b/a/a$a;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;IIF)",
            "Lcom/samsung/samm/spenscrap/b/a/a$a;"
        }
    .end annotation

    .prologue
    .line 43
    if-nez p0, :cond_0

    .line 44
    const/4 v0, 0x0

    .line 135
    :goto_0
    return-object v0

    .line 45
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 47
    if-le p2, p1, :cond_1

    .line 48
    sub-int v0, p2, p1

    add-int/lit8 v0, v0, 0x1

    move v2, v0

    .line 51
    :goto_1
    const/4 v0, 0x2

    if-gt v2, v0, :cond_2

    .line 52
    const/4 v0, 0x0

    goto :goto_0

    .line 50
    :cond_1
    sub-int v0, v6, p1

    add-int/lit8 v1, p2, 0x1

    add-int/2addr v0, v1

    move v2, v0

    goto :goto_1

    .line 58
    :cond_2
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 59
    invoke-virtual {p0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 62
    add-int/lit8 v7, v2, -0x2

    .line 63
    new-array v8, v7, [Landroid/graphics/PointF;

    .line 64
    new-array v9, v7, [Landroid/graphics/PointF;

    .line 66
    const/4 v3, 0x0

    .line 67
    if-le p2, p1, :cond_5

    .line 68
    add-int/lit8 v2, p1, 0x1

    move v4, v3

    move v3, v2

    :goto_2
    if-lt v3, p2, :cond_4

    .line 88
    :cond_3
    const/4 v5, 0x0

    .line 89
    const/4 v4, 0x0

    .line 90
    const/4 v3, 0x0

    .line 91
    const/high16 v6, 0x40a00000    # 5.0f

    .line 92
    const/4 v2, 0x0

    move v12, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v12

    :goto_3
    if-lt v5, v7, :cond_7

    .line 101
    if-nez v2, :cond_9

    .line 102
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_4
    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 71
    add-int/lit8 v5, v4, 0x1

    new-instance v6, Landroid/graphics/PointF;

    iget v10, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v6, v10, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v6, v8, v4

    .line 68
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v4, v5

    goto :goto_2

    .line 75
    :cond_5
    add-int/lit8 v2, p1, 0x1

    move v4, v2

    :goto_4
    if-lt v4, v6, :cond_6

    .line 79
    const/4 v2, 0x0

    move v4, v3

    move v3, v2

    :goto_5
    if-ge v3, p2, :cond_3

    .line 80
    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 81
    add-int/lit8 v5, v4, 0x1

    new-instance v6, Landroid/graphics/PointF;

    iget v10, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v6, v10, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v6, v8, v4

    .line 79
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v4, v5

    goto :goto_5

    .line 76
    :cond_6
    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 77
    add-int/lit8 v5, v3, 0x1

    new-instance v10, Landroid/graphics/PointF;

    iget v11, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v10, v11, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v10, v8, v3

    .line 75
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v5

    goto :goto_4

    .line 93
    :cond_7
    aget-object v10, v8, v5

    invoke-static {v0, v1, v10, v6}, Lcom/samsung/samm/spenscrap/b/a/a;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;F)Landroid/graphics/PointF;

    move-result-object v10

    aput-object v10, v9, v5

    .line 94
    aget-object v10, v9, v5

    if-nez v10, :cond_8

    .line 92
    :goto_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 96
    :cond_8
    aget-object v10, v9, v5

    iget v10, v10, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v10

    .line 97
    aget-object v10, v9, v5

    iget v10, v10, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v10

    .line 98
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 109
    :cond_9
    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5}, Landroid/graphics/PointF;-><init>()V

    .line 110
    int-to-float v6, v2

    div-float/2addr v4, v6

    iput v4, v5, Landroid/graphics/PointF;->x:F

    .line 111
    int-to-float v2, v2

    div-float v2, v3, v2

    iput v2, v5, Landroid/graphics/PointF;->y:F

    .line 114
    const/4 v3, 0x0

    .line 116
    const/4 v4, 0x0

    .line 117
    const/4 v2, 0x0

    move v12, v2

    move v2, v3

    move v3, v4

    move v4, v12

    :goto_7
    if-lt v4, v7, :cond_a

    .line 126
    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v3, v2

    .line 128
    new-instance v2, Lcom/samsung/samm/spenscrap/b/a/a$a;

    invoke-direct {v2}, Lcom/samsung/samm/spenscrap/b/a/a$a;-><init>()V

    .line 129
    invoke-virtual {v2, v5}, Lcom/samsung/samm/spenscrap/b/a/a$a;->a(Landroid/graphics/PointF;)V

    .line 130
    invoke-virtual {v2, v3}, Lcom/samsung/samm/spenscrap/b/a/a$a;->a(F)V

    .line 132
    const-string v4, "SPenBezierUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Start point ("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v0, Landroid/graphics/PointF;->x:F

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v0, v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "), End point ("

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v6, v1, Landroid/graphics/PointF;->x:F

    float-to-int v6, v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const-string v0, "SPenBezierUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, ": Average point ("

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v5, Landroid/graphics/PointF;->x:F

    float-to-int v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, v5, Landroid/graphics/PointF;->y:F

    float-to-int v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "), STD="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    float-to-int v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 135
    goto/16 :goto_0

    .line 118
    :cond_a
    aget-object v6, v9, v4

    if-nez v6, :cond_b

    .line 117
    :goto_8
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_7

    .line 120
    :cond_b
    aget-object v6, v9, v4

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget v8, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v6, v8

    .line 121
    aget-object v8, v9, v4

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget v10, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v8, v10

    .line 122
    mul-float/2addr v6, v6

    mul-float/2addr v8, v8

    add-float/2addr v6, v8

    add-float/2addr v2, v6

    .line 123
    add-int/lit8 v3, v3, 0x1

    goto :goto_8
.end method
