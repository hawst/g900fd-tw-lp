.class public Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method private static a()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 216
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 217
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "google_sdk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "sdk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 218
    :cond_0
    const-string v0, "SAMSUNG_SAMMFORMAT"

    .line 235
    :goto_0
    return-object v0

    .line 223
    :cond_1
    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 224
    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 225
    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    .line 226
    :cond_2
    const-string v1, "SmartClipEffectEngine"

    const-string v2, "Unknown Brand/Manufacturer Device"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 229
    :cond_3
    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 230
    const-string v5, "Samsung"

    invoke-virtual {v2, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "Samsung"

    invoke-virtual {v3, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_4

    .line 231
    const-string v5, "SmartClipEffectEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Device("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "), Model("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "), Brand("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), Manufacturer("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is not a Saumsung device."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 235
    :cond_4
    const-string v0, "SAMSUNG_SAMMFORMAT"

    goto :goto_0
.end method

.method public static getOutlinePath(Landroid/graphics/Bitmap;I)Landroid/graphics/Path;
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 41
    invoke-static {}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->setCheckSupportingModel()Z

    move-result v1

    if-nez v1, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-object v0

    .line 44
    :cond_1
    if-eqz p0, :cond_0

    if-ltz p1, :cond_0

    .line 46
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 48
    invoke-static {p0, p1}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngineJNI;->getOutlinePath(Landroid/graphics/Bitmap;I)[I

    move-result-object v6

    .line 50
    if-eqz v6, :cond_0

    .line 52
    const-string v1, "SmartClipEffectEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "array length : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    array-length v1, v6

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 69
    const/4 v0, 0x1

    aget v2, v6, v0

    .line 70
    const/4 v0, 0x2

    aget v1, v6, v0

    .line 72
    const/4 v0, 0x0

    :goto_1
    array-length v3, v6

    div-int/lit8 v3, v3, 0x3

    if-lt v0, v3, :cond_2

    .line 91
    array-length v0, v6

    add-int/lit8 v0, v0, -0x2

    aget v0, v6, v0

    int-to-float v0, v0

    array-length v1, v6

    add-int/lit8 v1, v1, -0x1

    aget v1, v6, v1

    int-to-float v1, v1

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 92
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    move-object v0, v5

    .line 94
    goto :goto_0

    .line 73
    :cond_2
    mul-int/lit8 v3, v0, 0x3

    aget v3, v6, v3

    if-nez v3, :cond_3

    .line 75
    mul-int/lit8 v1, v0, 0x3

    add-int/lit8 v1, v1, 0x1

    aget v2, v6, v1

    .line 76
    mul-int/lit8 v1, v0, 0x3

    add-int/lit8 v1, v1, 0x2

    aget v1, v6, v1

    .line 78
    int-to-float v3, v2

    int-to-float v4, v1

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 72
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 81
    :cond_3
    mul-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x1

    aget v4, v6, v3

    .line 82
    mul-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x2

    aget v3, v6, v3

    .line 84
    int-to-float v7, v2

    int-to-float v8, v1

    add-int/2addr v2, v4

    int-to-float v2, v2

    div-float/2addr v2, v9

    add-int/2addr v1, v3

    int-to-float v1, v1

    div-float/2addr v1, v9

    invoke-virtual {v5, v7, v8, v2, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    move v1, v3

    move v2, v4

    .line 87
    goto :goto_2
.end method

.method public static getOutlinePaths(Landroid/graphics/Bitmap;I)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 98
    invoke-static {}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->setCheckSupportingModel()Z

    move-result v1

    if-nez v1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-object v0

    .line 101
    :cond_1
    if-eqz p0, :cond_0

    if-ltz p1, :cond_0

    .line 104
    invoke-static {p0, p1}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngineJNI;->getOutlinePath(Landroid/graphics/Bitmap;I)[I

    move-result-object v7

    .line 106
    if-eqz v7, :cond_0

    .line 108
    const-string v1, "SmartClipEffectEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "array length : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    array-length v1, v7

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 114
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 116
    const/4 v0, 0x1

    aget v2, v7, v0

    .line 117
    const/4 v0, 0x2

    aget v1, v7, v0

    .line 119
    const/4 v0, 0x0

    move v3, v2

    move v2, v1

    move v1, v0

    :goto_1
    array-length v0, v7

    div-int/lit8 v0, v0, 0x3

    if-lt v1, v0, :cond_2

    .line 139
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    array-length v1, v7

    add-int/lit8 v1, v1, -0x2

    aget v1, v7, v1

    int-to-float v1, v1

    array-length v2, v7

    add-int/lit8 v2, v2, -0x1

    aget v2, v7, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    move-object v0, v6

    .line 141
    goto :goto_0

    .line 120
    :cond_2
    mul-int/lit8 v0, v1, 0x3

    aget v0, v7, v0

    if-nez v0, :cond_3

    .line 121
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    mul-int/lit8 v0, v1, 0x3

    add-int/lit8 v0, v0, 0x1

    aget v3, v7, v0

    .line 124
    mul-int/lit8 v0, v1, 0x3

    add-int/lit8 v0, v0, 0x2

    aget v2, v7, v0

    .line 126
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    int-to-float v4, v3

    int-to-float v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 119
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 129
    :cond_3
    mul-int/lit8 v0, v1, 0x3

    add-int/lit8 v0, v0, 0x1

    aget v5, v7, v0

    .line 130
    mul-int/lit8 v0, v1, 0x3

    add-int/lit8 v0, v0, 0x2

    aget v4, v7, v0

    .line 132
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    int-to-float v8, v3

    int-to-float v9, v2

    add-int/2addr v3, v5

    int-to-float v3, v3

    div-float/2addr v3, v10

    add-int/2addr v2, v4

    int-to-float v2, v2

    div-float/2addr v2, v10

    invoke-virtual {v0, v8, v9, v3, v2}, Landroid/graphics/Path;->quadTo(FFFF)V

    move v2, v4

    move v3, v5

    .line 135
    goto :goto_2
.end method

.method public static makeImageShadow(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIII)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/16 v2, 0xff

    .line 28
    invoke-static {}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->setCheckSupportingModel()Z

    move-result v1

    if-nez v1, :cond_1

    .line 36
    :cond_0
    :goto_0
    return v0

    .line 31
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    if-ltz p5, :cond_0

    if-ltz p6, :cond_0

    if-ltz p7, :cond_0

    .line 32
    if-gt p5, v2, :cond_0

    if-gt p6, v2, :cond_0

    if-gt p7, v2, :cond_0

    .line 34
    invoke-static/range {p0 .. p7}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngineJNI;->makeImageShadow(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIII)V

    .line 36
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setCheckSupportingModel()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 199
    invoke-static {}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngine;->a()Ljava/lang/String;

    move-result-object v1

    .line 200
    if-nez v1, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v0

    .line 202
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 203
    if-eqz v1, :cond_0

    .line 206
    array-length v0, v1

    invoke-static {v1, v0}, Lcom/samsung/samm/spenscrap/effectlibrary/SmartClipEffectEngineJNI;->setCheckSupportingModel([CI)V

    .line 208
    const/4 v0, 0x1

    goto :goto_0
.end method
