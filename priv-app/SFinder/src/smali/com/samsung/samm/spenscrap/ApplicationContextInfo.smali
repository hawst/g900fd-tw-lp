.class public Lcom/samsung/samm/spenscrap/ApplicationContextInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 178
    if-nez p0, :cond_0

    .line 179
    const/4 v0, 0x0

    .line 186
    :goto_0
    return-object v0

    .line 180
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v1

    .line 181
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v2

    .line 182
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 183
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 184
    invoke-virtual {p0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 185
    invoke-virtual {p0, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/util/List;Lcom/samsung/samm/spenscrap/ClipAppInfo;Z)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/pm/PackageManager;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Lcom/samsung/samm/spenscrap/ClipAppInfo;",
            "Z)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 119
    if-nez p3, :cond_0

    move v0, v1

    .line 174
    :goto_0
    return v0

    .line 124
    :cond_0
    invoke-virtual {p3}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v4

    .line 125
    invoke-virtual {p3}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->getAppClassName()Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 130
    goto :goto_0

    .line 135
    :cond_1
    const-string v3, "com.android.launcher"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 136
    const-string v3, "com.android.launcher2.Launcher"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 137
    invoke-virtual {p3, v2}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppType(I)V

    move v0, v2

    .line 138
    goto :goto_0

    .line 140
    :cond_2
    const-string v3, "com.sec.android.app.launcher"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 141
    const-string v3, "com.android.launcher2.Launcher"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 142
    invoke-virtual {p3, v2}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppType(I)V

    move v0, v2

    .line 143
    goto :goto_0

    .line 150
    :cond_3
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    move v3, v1

    .line 152
    :goto_1
    if-lt v3, v5, :cond_4

    move v0, v1

    .line 170
    :goto_2
    if-nez v0, :cond_8

    move v0, v1

    .line 171
    goto :goto_0

    .line 157
    :cond_4
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 158
    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 159
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p3, v3}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppClassName(Ljava/lang/String;)V

    .line 160
    if-eqz p4, :cond_6

    .line 161
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 162
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppTitle(Ljava/lang/String;)V

    .line 163
    :cond_5
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v0, p1}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/samm/spenscrap/ApplicationContextInfo;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppBitmapIcon(Landroid/graphics/Bitmap;)V

    :cond_6
    move v0, v2

    .line 166
    goto :goto_2

    .line 152
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_8
    move v0, v2

    .line 174
    goto/16 :goto_0
.end method

.method public static getLastPageURL(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v3, 0x0

    .line 197
    .line 200
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    sget-object v2, Landroid/provider/Browser;->HISTORY_PROJECTION:[Ljava/lang/String;

    const-string v5, "date DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 201
    if-nez v0, :cond_0

    .line 228
    :goto_0
    return-object v3

    .line 204
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 206
    const-string v2, "title"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 207
    const-string v4, "url"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 209
    if-le v2, v6, :cond_1

    if-le v4, v6, :cond_1

    if-lez v1, :cond_1

    .line 211
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 212
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 227
    :cond_1
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 214
    :cond_2
    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    .line 216
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 218
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 219
    if-eqz v3, :cond_1

    .line 220
    const-string v1, "History"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Last page browsed "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 224
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1
.end method

.method public static getRecentApplicationInfo(Landroid/content/Context;Z)Lcom/samsung/samm/spenscrap/ClipAppInfo;
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 35
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 36
    if-nez v4, :cond_1

    move-object v0, v2

    .line 92
    :cond_0
    :goto_0
    return-object v0

    .line 40
    :cond_1
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 42
    const/4 v1, 0x5

    .line 43
    const/4 v5, 0x2

    .line 48
    invoke-virtual {v0, v1, v5}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v5

    .line 49
    if-eqz v5, :cond_4

    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v0, v6, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 53
    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    invoke-virtual {v4, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 55
    if-nez v6, :cond_2

    move-object v0, v2

    .line 56
    goto :goto_0

    .line 58
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 59
    if-ge v0, v1, :cond_3

    move v1, v0

    .line 61
    :cond_3
    :goto_1
    if-lt v3, v1, :cond_5

    :cond_4
    move-object v0, v2

    .line 92
    goto :goto_0

    .line 64
    :cond_5
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RecentTaskInfo;

    .line 65
    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 66
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 70
    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    .line 71
    new-instance v0, Lcom/samsung/samm/spenscrap/ClipAppInfo;

    invoke-direct {v0}, Lcom/samsung/samm/spenscrap/ClipAppInfo;-><init>()V

    .line 72
    invoke-virtual {v0, v7}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppPackageName(Ljava/lang/String;)V

    .line 73
    invoke-virtual {v0, v8}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppClassName(Ljava/lang/String;)V

    .line 74
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppTime(J)V

    .line 76
    invoke-static {p0, v4, v6, v0, p1}, Lcom/samsung/samm/spenscrap/ApplicationContextInfo;->a(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/util/List;Lcom/samsung/samm/spenscrap/ClipAppInfo;Z)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 81
    if-eqz p1, :cond_0

    .line 82
    const-string v1, "com.android.browser"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    const-string v1, "com.android.browser.BrowserActivity"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    invoke-static {p0}, Lcom/samsung/samm/spenscrap/ApplicationContextInfo;->getLastPageURL(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppURL(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method public static updateLaunchableApplicationInfo(Landroid/content/Context;Lcom/samsung/samm/spenscrap/ClipAppInfo;Z)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 104
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 105
    if-nez v1, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 109
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 110
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 112
    if-eqz v2, :cond_0

    .line 115
    invoke-static {p0, v1, v2, p1, p2}, Lcom/samsung/samm/spenscrap/ApplicationContextInfo;->a(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/util/List;Lcom/samsung/samm/spenscrap/ClipAppInfo;Z)Z

    move-result v0

    goto :goto_0
.end method
