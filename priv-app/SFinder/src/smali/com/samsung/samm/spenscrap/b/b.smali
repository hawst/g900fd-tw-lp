.class public Lcom/samsung/samm/spenscrap/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/graphics/PointF;

.field private b:Landroid/graphics/PointF;

.field private c:Landroid/graphics/PointF;

.field private d:F

.field private e:F

.field private f:F

.field private g:Z

.field private h:F

.field private i:F

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    .line 17
    iput-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->b:Landroid/graphics/PointF;

    .line 18
    iput-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->c:Landroid/graphics/PointF;

    .line 19
    iput v2, p0, Lcom/samsung/samm/spenscrap/b/b;->d:F

    .line 20
    iput v2, p0, Lcom/samsung/samm/spenscrap/b/b;->e:F

    .line 21
    iput v0, p0, Lcom/samsung/samm/spenscrap/b/b;->f:F

    .line 22
    iput-boolean v3, p0, Lcom/samsung/samm/spenscrap/b/b;->g:Z

    .line 25
    iput v0, p0, Lcom/samsung/samm/spenscrap/b/b;->h:F

    .line 26
    iput v0, p0, Lcom/samsung/samm/spenscrap/b/b;->i:F

    .line 27
    iput-boolean v3, p0, Lcom/samsung/samm/spenscrap/b/b;->j:Z

    .line 12
    return-void
.end method

.method private static a(Ljava/util/ArrayList;FFLandroid/graphics/PointF;)F
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;FF",
            "Landroid/graphics/PointF;",
            ")F"
        }
    .end annotation

    .prologue
    .line 232
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 233
    if-nez v3, :cond_0

    .line 234
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 246
    :goto_0
    return v0

    .line 236
    :cond_0
    const/4 v1, 0x0

    .line 237
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-lt v2, v3, :cond_1

    move v0, v1

    .line 246
    goto :goto_0

    .line 238
    :cond_1
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 239
    mul-float v4, p2, p2

    iget v5, v0, Landroid/graphics/PointF;->x:F

    iget v6, p3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/PointF;->x:F

    iget v6, p3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    .line 240
    mul-float v5, p1, p1

    iget v6, v0, Landroid/graphics/PointF;->y:F

    iget v7, p3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v6, p3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v6

    mul-float/2addr v0, v5

    .line 241
    mul-float v5, p1, p1

    mul-float/2addr v5, p2

    mul-float/2addr v5, p2

    .line 242
    add-float/2addr v0, v4

    sub-float/2addr v0, v5

    div-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 244
    add-float/2addr v1, v0

    .line 237
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method

.method private static a(Ljava/util/ArrayList;FLandroid/graphics/PointF;FF)F
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;F",
            "Landroid/graphics/PointF;",
            "FF)F"
        }
    .end annotation

    .prologue
    .line 185
    invoke-static {p0, p1, p2}, Lcom/samsung/samm/spenscrap/b/b;->a(Ljava/util/ArrayList;FLandroid/graphics/PointF;)Ljava/util/ArrayList;

    move-result-object v0

    .line 186
    if-nez v0, :cond_0

    .line 187
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 189
    :goto_0
    return v0

    :cond_0
    invoke-static {v0, p3, p4, p2}, Lcom/samsung/samm/spenscrap/b/b;->a(Ljava/util/ArrayList;FFLandroid/graphics/PointF;)F

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;FLandroid/graphics/PointF;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;F",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 209
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 210
    if-nez v3, :cond_0

    .line 211
    const/4 v0, 0x0

    .line 228
    :goto_0
    return-object v0

    .line 212
    :cond_0
    mul-int/lit8 v1, v3, 0x2

    new-array v4, v1, [F

    move v1, v0

    move v2, v0

    .line 214
    :goto_1
    if-lt v1, v3, :cond_1

    .line 221
    mul-int/lit8 v0, v3, 0x2

    new-array v0, v0, [F

    .line 222
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 223
    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, p1, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 224
    invoke-virtual {v1, v0, v4}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 227
    invoke-static {v0}, Lcom/samsung/samm/spenscrap/b/b;->a([F)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 215
    :cond_1
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    aput v0, v4, v2

    .line 216
    add-int/lit8 v2, v2, 0x1

    .line 217
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aput v0, v4, v2

    .line 218
    add-int/lit8 v2, v2, 0x1

    .line 214
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private static a([F)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([F)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 196
    array-length v2, p0

    .line 197
    if-nez v2, :cond_1

    .line 198
    const/4 v0, 0x0

    .line 203
    :cond_0
    return-object v0

    .line 199
    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/lit8 v3, v2, -0x1

    if-ge v1, v3, :cond_0

    .line 200
    new-instance v3, Landroid/graphics/PointF;

    aget v4, p0, v1

    add-int/lit8 v5, v1, 0x1

    aget v5, p0, v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v1, v1, 0x1

    .line 199
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)F
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;)F"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 133
    if-nez p1, :cond_0

    move v0, v5

    .line 154
    :goto_0
    return v0

    .line 135
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 136
    const/4 v0, 0x5

    if-ge v4, v0, :cond_1

    move v0, v5

    .line 137
    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 139
    div-int/lit8 v1, v4, 0x4

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 140
    div-int/lit8 v2, v4, 0x2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 141
    mul-int/lit8 v3, v4, 0x3

    div-int/lit8 v3, v3, 0x4

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 142
    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 145
    iget-object v6, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    invoke-static {v0, v6, v1}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v0

    .line 146
    add-float/2addr v0, v5

    .line 147
    iget-object v5, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    invoke-static {v1, v5, v2}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v1

    .line 148
    add-float/2addr v0, v1

    .line 149
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    invoke-static {v2, v1, v3}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v1

    .line 150
    add-float/2addr v0, v1

    .line 151
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    invoke-static {v3, v1, v4}, Lcom/samsung/samm/spenscrap/b/c/b;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v1

    .line 152
    add-float/2addr v0, v1

    .line 154
    goto :goto_0
.end method

.method public a()Landroid/graphics/RectF;
    .locals 3

    .prologue
    .line 92
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 93
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget v2, p0, Lcom/samsung/samm/spenscrap/b/b;->d:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 94
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget v2, p0, Lcom/samsung/samm/spenscrap/b/b;->d:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 95
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/samsung/samm/spenscrap/b/b;->e:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 96
    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/samsung/samm/spenscrap/b/b;->e:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 97
    return-object v0
.end method

.method public a(FFLandroid/graphics/PointF;FLjava/util/ArrayList;Z)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF",
            "Landroid/graphics/PointF;",
            "F",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 40
    if-nez p3, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v2

    .line 42
    :cond_1
    cmpg-float v0, p1, v7

    if-lez v0, :cond_0

    cmpg-float v0, p2, v7

    if-lez v0, :cond_0

    .line 44
    if-eqz p5, :cond_0

    .line 46
    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 47
    const/4 v0, 0x2

    if-lt v3, v0, :cond_0

    .line 52
    iput p1, p0, Lcom/samsung/samm/spenscrap/b/b;->d:F

    .line 53
    iput p2, p0, Lcom/samsung/samm/spenscrap/b/b;->e:F

    .line 54
    new-instance v0, Landroid/graphics/PointF;

    iget v4, p3, Landroid/graphics/PointF;->x:F

    iget v5, p3, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    .line 55
    iput p4, p0, Lcom/samsung/samm/spenscrap/b/b;->f:F

    .line 56
    iput-boolean p6, p0, Lcom/samsung/samm/spenscrap/b/b;->g:Z

    .line 59
    iget-boolean v0, p0, Lcom/samsung/samm/spenscrap/b/b;->g:Z

    if-nez v0, :cond_2

    .line 60
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 61
    invoke-virtual {p5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 62
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v5, v6, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    add-int/lit8 v0, v3, -0x1

    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 64
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v5, v6, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-static {v4, p4, p3}, Lcom/samsung/samm/spenscrap/b/b;->a(Ljava/util/ArrayList;FLandroid/graphics/PointF;)Ljava/util/ArrayList;

    move-result-object v4

    .line 66
    if-eqz v4, :cond_2

    .line 67
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/b;->b:Landroid/graphics/PointF;

    .line 68
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/samsung/samm/spenscrap/b/b;->c:Landroid/graphics/PointF;

    .line 73
    :cond_2
    invoke-virtual {p0, p5}, Lcom/samsung/samm/spenscrap/b/b;->a(Ljava/util/ArrayList;)F

    move-result v4

    .line 74
    cmpl-float v0, v4, v7

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/samsung/samm/spenscrap/b/b;->j:Z

    .line 75
    const/high16 v0, 0x43b40000    # 360.0f

    div-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/samsung/samm/spenscrap/b/b;->i:F

    .line 76
    invoke-virtual {p0, p5}, Lcom/samsung/samm/spenscrap/b/b;->b(Ljava/util/ArrayList;)F

    move-result v0

    int-to-float v2, v3

    div-float/2addr v0, v2

    iput v0, p0, Lcom/samsung/samm/spenscrap/b/b;->h:F

    .line 78
    cmpg-float v0, v4, v7

    if-gez v0, :cond_4

    .line 79
    const-string v0, "EllipseInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ellipse Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/samm/spenscrap/b/b;->h:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Completness: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/samm/spenscrap/b/b;->i:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Range(Anti-clockwise): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    neg-float v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    move v2, v1

    .line 84
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 74
    goto :goto_1

    .line 82
    :cond_4
    const-string v0, "EllipseInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ellipse[RANSAC] Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/samm/spenscrap/b/b;->h:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Completness: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/samm/spenscrap/b/b;->i:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Range(Clockwise): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public b(Ljava/util/ArrayList;)F
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;)F"
        }
    .end annotation

    .prologue
    .line 179
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/b;->f:F

    iget-object v1, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    iget v2, p0, Lcom/samsung/samm/spenscrap/b/b;->d:F

    iget v3, p0, Lcom/samsung/samm/spenscrap/b/b;->e:F

    invoke-static {p1, v0, v1, v2, v3}, Lcom/samsung/samm/spenscrap/b/b;->a(Ljava/util/ArrayList;FLandroid/graphics/PointF;FF)F

    move-result v0

    return v0
.end method

.method public b()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/b;->a:Landroid/graphics/PointF;

    return-object v0
.end method

.method public c()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/b;->b:Landroid/graphics/PointF;

    return-object v0
.end method

.method public d()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/samm/spenscrap/b/b;->c:Landroid/graphics/PointF;

    return-object v0
.end method

.method public e()F
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/b;->f:F

    return v0
.end method

.method public f()F
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/b;->h:F

    return v0
.end method

.method public g()F
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/samsung/samm/spenscrap/b/b;->i:F

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/samsung/samm/spenscrap/b/b;->j:Z

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/samsung/samm/spenscrap/b/b;->g:Z

    return v0
.end method
