.class public Lcom/samsung/samm/spenscrap/ClipSAMMLibStatic;
.super Lcom/samsung/samm/spenscrap/ClipSAMMLib;
.source "SourceFile"


# static fields
.field public static final KEY_APPINFO_SRCFILE_PATH:Ljava/lang/String; = "AppInfoSrcFilePath"

.field public static final KEY_APPINFO_TIME:Ljava/lang/String; = "AppInfoTime"

.field public static final KEY_APPINFO_URL:Ljava/lang/String; = "AppInfoURL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/samm/spenscrap/ClipSAMMLib;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Z)Lcom/samsung/samm/a/b;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 376
    new-instance v0, Lcom/samsung/samm/a/c;

    invoke-direct {v0}, Lcom/samsung/samm/a/c;-><init>()V

    .line 378
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->b:Z

    .line 379
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->a:Z

    .line 381
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->g:Z

    .line 382
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->c:Z

    .line 383
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->h:Z

    .line 384
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->i:Z

    .line 385
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->j:Z

    .line 386
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->k:Z

    .line 387
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->l:Z

    .line 388
    iput-boolean p2, v0, Lcom/samsung/samm/a/c;->m:Z

    .line 389
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->n:Z

    .line 390
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->o:Z

    .line 391
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->p:Z

    .line 392
    iput-boolean v1, v0, Lcom/samsung/samm/a/c;->q:Z

    .line 393
    invoke-static {p0, p1, v0}, Lcom/samsung/samm/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/samm/a/c;)Lcom/samsung/samm/a/b;

    move-result-object v0

    return-object v0
.end method

.method public static clearURL(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 314
    invoke-static {p1}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;)Z

    move-result v0

    .line 316
    if-nez v0, :cond_0

    .line 317
    const-string v0, "ClipSAMMLibExt"

    const-string v1, "The file is not Live Picture(SAMM format) file."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v7

    .line 350
    :goto_0
    return v4

    .line 321
    :cond_0
    invoke-static {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLibStatic;->getSAMMFileInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/a/b;

    move-result-object v0

    .line 323
    iget v2, v0, Lcom/samsung/samm/a/b;->f:I

    .line 324
    iget v3, v0, Lcom/samsung/samm/a/b;->g:I

    .line 327
    new-instance v0, Lcom/samsung/samm/b/a;

    move-object v1, p0

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/samm/b/a;-><init>(Landroid/content/Context;IIZZLjava/lang/String;)V

    .line 329
    invoke-virtual {v0, p1, v6, v4}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 330
    const-string v0, "ClipSAMMLibExt"

    const-string v1, "Fail to load SAMM data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 334
    :cond_1
    invoke-virtual {v0, v6}, Lcom/samsung/samm/b/a;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 335
    const-string v0, "ClipSAMMLibExt"

    const-string v1, "Fail to clear audiodata"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 340
    :cond_2
    invoke-virtual {v0, p1}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 341
    const-string v0, "ClipSAMMLibExt"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fail to save as SAMM file : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 346
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/samm/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 347
    const-string v0, "ClipSAMMLibExt"

    const-string v1, "Fail to close SAMM library"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move v4, v7

    .line 350
    goto :goto_0
.end method

.method public static decodeApplicationInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/spenscrap/ClipAppInfo;
    .locals 7

    .prologue
    const/16 v2, 0x64

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 169
    new-instance v0, Lcom/samsung/samm/b/a;

    move-object v1, p0

    move v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/samm/b/a;-><init>(Landroid/content/Context;IIZZLjava/lang/String;)V

    .line 172
    invoke-virtual {v0, p1, v6, v4}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 173
    const-string v0, "ClipSAMMLibExt"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fail to load SAMM file : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    :goto_0
    return-object v6

    .line 178
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/samm/b/a;->d()Lcom/samsung/samm/a/a;

    move-result-object v1

    .line 179
    if-eqz v1, :cond_0

    .line 183
    invoke-virtual {v0}, Lcom/samsung/samm/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 184
    const-string v0, "ClipSAMMLibExt"

    const-string v1, "Fail to close SAMM library"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 188
    :cond_2
    new-instance v6, Lcom/samsung/samm/spenscrap/ClipAppInfo;

    invoke-direct {v6}, Lcom/samsung/samm/spenscrap/ClipAppInfo;-><init>()V

    .line 189
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppType()I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppType(I)V

    .line 190
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppURL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppURL(Ljava/lang/String;)V

    .line 191
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppSrcPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppSrcPath(Ljava/lang/String;)V

    .line 192
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppTime()J

    move-result-wide v2

    invoke-virtual {v6, v2, v3}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppTime(J)V

    .line 193
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppPackageName(Ljava/lang/String;)V

    .line 194
    invoke-virtual {v1}, Lcom/samsung/samm/a/a;->getAppClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/samm/spenscrap/ClipAppInfo;->setAppClassName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static decodeIntMetaInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v2, 0x64

    const/4 v4, 0x0

    .line 243
    new-instance v0, Lcom/samsung/samm/b/a;

    move-object v1, p0

    move v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/samm/b/a;-><init>(Landroid/content/Context;IIZZLjava/lang/String;)V

    .line 246
    invoke-virtual {v0, p1, v6, v4}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 247
    const-string v0, "ClipSAMMLibExt"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fail to load SAMM file : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :goto_0
    return p4

    .line 252
    :cond_0
    invoke-virtual {v0, p2, p3}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;I)I

    move-result v1

    .line 255
    invoke-virtual {v0}, Lcom/samsung/samm/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 256
    const-string v0, "ClipSAMMLibExt"

    const-string v1, "Fail to close SAMM library"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    move p4, v1

    .line 259
    goto :goto_0
.end method

.method public static decodeStringMetaInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v2, 0x64

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 212
    new-instance v0, Lcom/samsung/samm/b/a;

    move-object v1, p0

    move v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/samm/b/a;-><init>(Landroid/content/Context;IIZZLjava/lang/String;)V

    .line 215
    invoke-virtual {v0, p1, v6, v4}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    const-string v0, "ClipSAMMLibExt"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fail to load SAMM file : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :goto_0
    return-object v6

    .line 221
    :cond_0
    invoke-virtual {v0, p2, p3}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 224
    invoke-virtual {v0}, Lcom/samsung/samm/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    const-string v0, "ClipSAMMLibExt"

    const-string v1, "Fail to close SAMM library"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    move-object v6, v1

    .line 228
    goto :goto_0
.end method

.method public static encodeApplicationInfo(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/samm/spenscrap/ClipAppInfo;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 52
    if-nez p2, :cond_0

    .line 53
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "ClipAppInfo must be specified"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :goto_0
    return v0

    .line 57
    :cond_0
    invoke-static {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLibStatic;->createSAMMLibrary(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/b/a;

    move-result-object v1

    .line 58
    if-nez v1, :cond_1

    .line 59
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Fail to create SAMM Library"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {v1, p2}, Lcom/samsung/samm/b/a;->a(Lcom/samsung/samm/a/a;)V

    .line 67
    invoke-virtual {v1, p1}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 68
    const-string v1, "ClipSAMMLibExt"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fail to save as SAMM file : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 72
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/samm/b/a;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 73
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Fail to close SAMM library"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 76
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static encodeMetaInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 125
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126
    :cond_0
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Key must be specified"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :goto_0
    return v0

    .line 130
    :cond_1
    invoke-static {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLibStatic;->createSAMMLibrary(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/b/a;

    move-result-object v1

    .line 131
    if-nez v1, :cond_2

    .line 132
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Fail to create SAMM Library"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 137
    :cond_2
    invoke-virtual {v1, p2, p3}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;I)Z

    .line 140
    invoke-virtual {v1, p1}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 141
    const-string v1, "ClipSAMMLibExt"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fail to save as SAMM file : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 145
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/samm/b/a;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 146
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Fail to close SAMM library"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 149
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static encodeStringMetaInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 88
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    :cond_0
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Key must be specified"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :goto_0
    return v0

    .line 93
    :cond_1
    invoke-static {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLibStatic;->createSAMMLibrary(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/b/a;

    move-result-object v1

    .line 94
    if-nez v1, :cond_2

    .line 95
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Fail to create SAMM Library"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 100
    :cond_2
    invoke-virtual {v1, p2, p3}, Lcom/samsung/samm/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 103
    invoke-virtual {v1, p1}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 104
    const-string v1, "ClipSAMMLibExt"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fail to save as SAMM file : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 108
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/samm/b/a;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 109
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Fail to close SAMM library"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 112
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static encodeURL(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 279
    if-nez p2, :cond_0

    .line 280
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "URL must be specified"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :goto_0
    return v0

    .line 284
    :cond_0
    invoke-static {p0, p1}, Lcom/samsung/samm/spenscrap/ClipSAMMLibStatic;->createSAMMLibrary(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/samm/b/a;

    move-result-object v1

    .line 285
    if-nez v1, :cond_1

    .line 286
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Fail to create SAMM Library"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 292
    :cond_1
    invoke-virtual {v1, p1}, Lcom/samsung/samm/b/a;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 293
    const-string v1, "ClipSAMMLibExt"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fail to save as SAMM file : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 297
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/samm/b/a;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 298
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Fail to close SAMM library"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static hasURL(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 361
    const/4 v1, 0x1

    invoke-static {p0, p1, v1}, Lcom/samsung/samm/spenscrap/ClipSAMMLibStatic;->a(Landroid/content/Context;Ljava/lang/String;Z)Lcom/samsung/samm/a/b;

    move-result-object v1

    .line 362
    if-nez v1, :cond_1

    .line 363
    const-string v1, "ClipSAMMLibExt"

    const-string v2, "Fail to extract SAMM file information"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    :cond_0
    :goto_0
    return-object v0

    .line 366
    :cond_1
    iget-boolean v2, v1, Lcom/samsung/samm/a/b;->a:Z

    if-eqz v2, :cond_0

    .line 368
    iget-object v0, v1, Lcom/samsung/samm/a/b;->w:Ljava/lang/String;

    goto :goto_0
.end method
