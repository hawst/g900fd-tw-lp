.class final Lcom/nuance/nmdp/speechkit/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/Recognizer;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/nuance/nmdp/speechkit/u;

.field private final f:Lcom/nuance/nmdp/speechkit/ae;

.field private final g:Lcom/nuance/nmdp/speechkit/ac;

.field private h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

.field private i:Z

.field private j:Lcom/nuance/nmdp/speechkit/Prompt;

.field private k:Lcom/nuance/nmdp/speechkit/Prompt;

.field private l:Lcom/nuance/nmdp/speechkit/Prompt;

.field private m:Lcom/nuance/nmdp/speechkit/Prompt;


# direct methods
.method constructor <init>(Lcom/nuance/nmdp/speechkit/u;Ljava/lang/String;ILjava/lang/String;Lcom/nuance/nmdp/speechkit/ae;Lcom/nuance/nmdp/speechkit/Recognizer$Listener;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p3, p0, Lcom/nuance/nmdp/speechkit/b;->a:I

    iput-object p4, p0, Lcom/nuance/nmdp/speechkit/b;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/b;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/nuance/nmdp/speechkit/b;->c:Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/b;->e:Lcom/nuance/nmdp/speechkit/u;

    new-instance v0, Lcom/nuance/nmdp/speechkit/b$1;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/b$1;-><init>(Lcom/nuance/nmdp/speechkit/b;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->g:Lcom/nuance/nmdp/speechkit/ac;

    iput-object p5, p0, Lcom/nuance/nmdp/speechkit/b;->f:Lcom/nuance/nmdp/speechkit/ae;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/b;->i:Z

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/b;->j:Lcom/nuance/nmdp/speechkit/Prompt;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/b;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/b;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/b;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    return-void
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    return-object v0
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    return-object v0
.end method

.method static synthetic c(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/Recognizer$Listener;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->c:Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    return-object v0
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/SpeechKit$a;->b()V

    goto :goto_0
.end method

.method public final getAudioLevel()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final setListener(Lcom/nuance/nmdp/speechkit/Recognizer$Listener;)V
    .locals 0

    return-void
.end method

.method public final setPrompt(ILcom/nuance/nmdp/speechkit/Prompt;)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/b;->j:Lcom/nuance/nmdp/speechkit/Prompt;

    goto :goto_0

    :pswitch_1
    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/b;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    goto :goto_0

    :pswitch_2
    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/b;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    goto :goto_0

    :pswitch_3
    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/b;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final start()V
    .locals 14

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->e:Lcom/nuance/nmdp/speechkit/u;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/u;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/b;->i:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/b;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v3, v11

    :goto_1
    iget v0, p0, Lcom/nuance/nmdp/speechkit/b;->a:I

    if-ne v0, v11, :cond_2

    move v2, v11

    :goto_2
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->j:Lcom/nuance/nmdp/speechkit/Prompt;

    if-nez v0, :cond_3

    move-object v5, v13

    :goto_3
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    if-nez v0, :cond_4

    move-object v6, v13

    :goto_4
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    if-nez v0, :cond_5

    move-object v7, v13

    :goto_5
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    if-nez v0, :cond_6

    move-object v8, v13

    :goto_6
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->e:Lcom/nuance/nmdp/speechkit/u;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/b;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/b;->b:Ljava/lang/String;

    iget-object v9, p0, Lcom/nuance/nmdp/speechkit/b;->f:Lcom/nuance/nmdp/speechkit/ae;

    iget-object v10, p0, Lcom/nuance/nmdp/speechkit/b;->g:Lcom/nuance/nmdp/speechkit/ac;

    invoke-virtual/range {v0 .. v10}, Lcom/nuance/nmdp/speechkit/u;->a(Ljava/lang/String;ZZLjava/lang/String;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ae;Lcom/nuance/nmdp/speechkit/ac;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-nez v0, :cond_7

    const-string v0, "Unable to create recognition transaction"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->c(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->c:Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    new-instance v1, Lcom/nuance/nmdp/speechkit/d;

    invoke-direct {v1, v12, v13, v13}, Lcom/nuance/nmdp/speechkit/d;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, p0, v1}, Lcom/nuance/nmdp/speechkit/Recognizer$Listener;->onError(Lcom/nuance/nmdp/speechkit/Recognizer;Lcom/nuance/nmdp/speechkit/SpeechError;)V

    goto :goto_0

    :cond_1
    move v3, v12

    goto :goto_1

    :cond_2
    move v2, v12

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->j:Lcom/nuance/nmdp/speechkit/Prompt;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/Prompt;->a()Lcom/nuance/nmdp/speechkit/ay;

    move-result-object v5

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/Prompt;->a()Lcom/nuance/nmdp/speechkit/ay;

    move-result-object v6

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/Prompt;->a()Lcom/nuance/nmdp/speechkit/ay;

    move-result-object v7

    goto :goto_5

    :cond_6
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/Prompt;->a()Lcom/nuance/nmdp/speechkit/ay;

    move-result-object v8

    goto :goto_6

    :cond_7
    iput-boolean v11, p0, Lcom/nuance/nmdp/speechkit/b;->i:Z

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/SpeechKit$a;->a()V

    goto :goto_0

    :cond_8
    const-string v0, "Unable to create recognition transaction. Transaction runner is invalid."

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->c(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->c:Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    new-instance v1, Lcom/nuance/nmdp/speechkit/d;

    invoke-direct {v1, v12, v13, v13}, Lcom/nuance/nmdp/speechkit/d;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, p0, v1}, Lcom/nuance/nmdp/speechkit/Recognizer$Listener;->onError(Lcom/nuance/nmdp/speechkit/Recognizer;Lcom/nuance/nmdp/speechkit/SpeechError;)V

    goto :goto_0
.end method

.method public final stopRecording()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b;->h:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/SpeechKit$a;->c()V

    goto :goto_0
.end method
