.class public final Lcom/nuance/nmdp/speechkit/bc;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/nuance/nmdp/speechkit/aw;


# instance fields
.field private final b:Lcom/nuance/nmdp/speechkit/aw$a;

.field private final c:Lcom/nuance/nmdp/speechkit/az;

.field private d:Lcom/nuance/nmdp/speechkit/bv;

.field private final e:Ljava/lang/Object;

.field private final f:Lcom/nuance/nmdp/speechkit/bu;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private final k:Ljava/lang/Object;

.field private final l:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/nuance/nmdp/speechkit/aw;

    invoke-direct {v0}, Lcom/nuance/nmdp/speechkit/aw;-><init>()V

    sput-object v0, Lcom/nuance/nmdp/speechkit/bc;->a:Lcom/nuance/nmdp/speechkit/aw;

    return-void
.end method

.method public constructor <init>(Lcom/nuance/nmdp/speechkit/cf;ZZLjava/lang/Object;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/az;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/nuance/nmdp/speechkit/bc$2;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/bc$2;-><init>(Lcom/nuance/nmdp/speechkit/bc;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->f:Lcom/nuance/nmdp/speechkit/bu;

    iput-object p6, p0, Lcom/nuance/nmdp/speechkit/bc;->c:Lcom/nuance/nmdp/speechkit/az;

    iput-object p4, p0, Lcom/nuance/nmdp/speechkit/bc;->e:Ljava/lang/Object;

    iput-boolean v2, p0, Lcom/nuance/nmdp/speechkit/bc;->g:Z

    iput-boolean v2, p0, Lcom/nuance/nmdp/speechkit/bc;->h:Z

    iput-boolean v2, p0, Lcom/nuance/nmdp/speechkit/bc;->j:Z

    iput-boolean v2, p0, Lcom/nuance/nmdp/speechkit/bc;->i:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->k:Ljava/lang/Object;

    iput-object p5, p0, Lcom/nuance/nmdp/speechkit/bc;->l:Ljava/lang/Object;

    sget-object v0, Lcom/nuance/nmdp/speechkit/bc;->a:Lcom/nuance/nmdp/speechkit/aw;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/aw;->c()I

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->b:Lcom/nuance/nmdp/speechkit/aw$a;

    new-instance v0, Lcom/nuance/nmdp/speechkit/aw;

    invoke-direct {v0}, Lcom/nuance/nmdp/speechkit/aw;-><init>()V

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/bc;->b:Lcom/nuance/nmdp/speechkit/aw$a;

    if-nez v2, :cond_0

    if-eqz p2, :cond_2

    new-instance v2, Lcom/nuance/nmdp/speechkit/bz;

    const-string v3, "ep.enable"

    const-string v4, "TRUE"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    sget-object v5, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    invoke-direct {v2, v3, v4, v5}, Lcom/nuance/nmdp/speechkit/bz;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bz$a;)V

    invoke-virtual {v0, v2}, Lcom/nuance/nmdp/speechkit/aw;->a(Ljava/lang/Object;)V

    :cond_0
    :goto_1
    new-instance v2, Lcom/nuance/nmdp/speechkit/bz;

    const-string v3, "USE_ENERGY_LEVEL"

    const-string v4, "TRUE"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    sget-object v5, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    invoke-direct {v2, v3, v4, v5}, Lcom/nuance/nmdp/speechkit/bz;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bz$a;)V

    invoke-virtual {v0, v2}, Lcom/nuance/nmdp/speechkit/aw;->a(Ljava/lang/Object;)V

    new-instance v2, Lcom/nuance/nmdp/speechkit/bz;

    const-string v3, "Android_Context"

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/bc;->l:Ljava/lang/Object;

    sget-object v5, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    invoke-direct {v2, v3, v4, v5}, Lcom/nuance/nmdp/speechkit/bz;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bz$a;)V

    invoke-virtual {v0, v2}, Lcom/nuance/nmdp/speechkit/aw;->a(Ljava/lang/Object;)V

    :try_start_0
    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/bc;->f:Lcom/nuance/nmdp/speechkit/bu;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/aw;->f()Ljava/util/Vector;

    move-result-object v0

    sget-object v3, Lcom/nuance/nmdp/speechkit/bd;->a:Lcom/nuance/nmdp/speechkit/bd;

    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "NMSPAudioRecordListener can not be null!"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v2, "Error creating recorder"

    invoke-static {p0, v2, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/bc;->d:Lcom/nuance/nmdp/speechkit/bv;

    :goto_2
    return-void

    :cond_1
    sget-object v0, Lcom/nuance/nmdp/speechkit/bc;->a:Lcom/nuance/nmdp/speechkit/aw;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/aw;->e()Lcom/nuance/nmdp/speechkit/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/aw;->d()Lcom/nuance/nmdp/speechkit/aw$a;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_0

    new-instance v2, Lcom/nuance/nmdp/speechkit/bz;

    const-string v3, "ep.enable"

    const-string v4, "TRUE"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    sget-object v5, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    invoke-direct {v2, v3, v4, v5}, Lcom/nuance/nmdp/speechkit/bz;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bz$a;)V

    invoke-virtual {v0, v2}, Lcom/nuance/nmdp/speechkit/aw;->a(Ljava/lang/Object;)V

    new-instance v2, Lcom/nuance/nmdp/speechkit/bz;

    const-string v3, "ep.VadLongUtterance"

    const-string v4, "TRUE"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    sget-object v5, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    invoke-direct {v2, v3, v4, v5}, Lcom/nuance/nmdp/speechkit/bz;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bz$a;)V

    invoke-virtual {v0, v2}, Lcom/nuance/nmdp/speechkit/aw;->a(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    if-nez p1, :cond_4

    :try_start_1
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "Manager can not be null!"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v4, Lcom/nuance/nmdp/speechkit/ce;

    invoke-direct {v4, v2, p1, v0, v3}, Lcom/nuance/nmdp/speechkit/ce;-><init>(Lcom/nuance/nmdp/speechkit/bu;Lcom/nuance/nmdp/speechkit/cf;Ljava/util/Vector;Lcom/nuance/nmdp/speechkit/bd;)V

    iput-object v4, p0, Lcom/nuance/nmdp/speechkit/bc;->d:Lcom/nuance/nmdp/speechkit/bv;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/bc;)Lcom/nuance/nmdp/speechkit/aw$a;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->b:Lcom/nuance/nmdp/speechkit/aw$a;

    return-object v0
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/bc;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->k:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c()Lcom/nuance/nmdp/speechkit/aw;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic c(Lcom/nuance/nmdp/speechkit/bc;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->i:Z

    return v0
.end method

.method static synthetic d(Lcom/nuance/nmdp/speechkit/bc;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->j:Z

    return v0
.end method

.method static synthetic e(Lcom/nuance/nmdp/speechkit/bc;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->j:Z

    return v0
.end method

.method static synthetic f(Lcom/nuance/nmdp/speechkit/bc;)Lcom/nuance/nmdp/speechkit/bv;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->d:Lcom/nuance/nmdp/speechkit/bv;

    return-object v0
.end method

.method static synthetic g(Lcom/nuance/nmdp/speechkit/bc;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic h(Lcom/nuance/nmdp/speechkit/bc;)Lcom/nuance/nmdp/speechkit/az;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->c:Lcom/nuance/nmdp/speechkit/az;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->g:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->g:Z

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->d:Lcom/nuance/nmdp/speechkit/bv;

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "Starting recorder"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->d:Lcom/nuance/nmdp/speechkit/bv;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/bv;->f()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error starting recorder"

    invoke-static {p0, v1, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->c:Lcom/nuance/nmdp/speechkit/az;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bc;->e:Ljava/lang/Object;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/az;->a(I)V

    goto :goto_0

    :cond_1
    const-string v0, "Recorder already started"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->c(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/bx;)V
    .locals 2

    const-string v0, "Capturing audio from recorder"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->b:Lcom/nuance/nmdp/speechkit/aw$a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->d:Lcom/nuance/nmdp/speechkit/bv;

    invoke-interface {v0, p1}, Lcom/nuance/nmdp/speechkit/bv;->a(Lcom/nuance/nmdp/speechkit/bx;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->d:Lcom/nuance/nmdp/speechkit/bv;

    new-instance v1, Lcom/nuance/nmdp/speechkit/bc$1;

    invoke-direct {v1, p0, p1}, Lcom/nuance/nmdp/speechkit/bc$1;-><init>(Lcom/nuance/nmdp/speechkit/bc;Lcom/nuance/nmdp/speechkit/bx;)V

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bv;->a(Lcom/nuance/nmdp/speechkit/bx;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->h:Z

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lcom/nuance/nmdp/speechkit/bc;->h:Z

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->d:Lcom/nuance/nmdp/speechkit/bv;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bc;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->i:Z

    if-nez v0, :cond_0

    const-string v0, "Stopping recorder"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->d:Lcom/nuance/nmdp/speechkit/bv;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/bv;->g()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->j:Z

    :goto_0
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->i:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->k:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    monitor-exit v1

    :cond_1
    :goto_1
    return-void

    :catch_1
    move-exception v0

    const-string v2, "Error stopping recorder"

    invoke-static {p0, v2, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bc;->i:Z

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    const-string v0, "Can\'t stop recorder because it wasn\'t started"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->c(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bc;->c:Lcom/nuance/nmdp/speechkit/az;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bc;->e:Ljava/lang/Object;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/az;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
