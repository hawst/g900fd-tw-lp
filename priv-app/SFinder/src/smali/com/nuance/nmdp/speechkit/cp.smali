.class public Lcom/nuance/nmdp/speechkit/cp;
.super Lcom/nuance/nmdp/speechkit/co;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/do;


# static fields
.field private static final g:Lcom/nuance/nmdp/speechkit/bh;

.field private static k:Ljava/lang/String;


# instance fields
.field private h:Lcom/nuance/nmdp/speechkit/bi;

.field private i:Lcom/nuance/nmdp/speechkit/ck;

.field private j:Lcom/nuance/nmdp/speechkit/dk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/nuance/nmdp/speechkit/cp;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Class;)Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    sput-object v0, Lcom/nuance/nmdp/speechkit/cp;->g:Lcom/nuance/nmdp/speechkit/bh;

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sput-object v0, Lcom/nuance/nmdp/speechkit/cp;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/nuance/nmdp/speechkit/cl;Lcom/nuance/nmdp/speechkit/ck;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Lcom/nuance/nmdp/speechkit/co;-><init>(Lcom/nuance/nmdp/speechkit/cl;Lcom/nuance/nmdp/speechkit/ck;)V

    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/cp;->i:Lcom/nuance/nmdp/speechkit/ck;

    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/cl;->a()Lcom/nuance/nmdp/speechkit/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->h:Lcom/nuance/nmdp/speechkit/bi;

    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/cl;->h()Lcom/nuance/nmdp/speechkit/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->a:Lcom/nuance/nmdp/speechkit/cm;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->a:Lcom/nuance/nmdp/speechkit/cm;

    invoke-virtual {v0, p0}, Lcom/nuance/nmdp/speechkit/cm;->a(Lcom/nuance/nmdp/speechkit/ci;)V

    iput-object v3, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->h:Lcom/nuance/nmdp/speechkit/bi;

    new-instance v1, Lcom/nuance/nmdp/speechkit/bi$a;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/cp;->h:Lcom/nuance/nmdp/speechkit/bi;

    invoke-interface {v3}, Lcom/nuance/nmdp/speechkit/bi;->a()[Ljava/lang/Object;

    invoke-interface {v0, v1, p0, v2}, Lcom/nuance/nmdp/speechkit/bi;->a(Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bi$b;Ljava/lang/Object;)V

    return-void
.end method

.method private a(B)V
    .locals 4

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    iget-byte v0, v0, Lcom/nuance/nmdp/speechkit/dk;->a:B

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/nuance/nmdp/speechkit/cp;->d()Lcom/nuance/nmdp/speechkit/cf;

    move-result-object v0

    check-cast v0, Lcom/nuance/nmdp/speechkit/cl;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/cl;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_2

    sget-object v1, Lcom/nuance/nmdp/speechkit/cp;->g:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clearResLogsToServer() before clean the log vector tranId["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] log list size ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/nmdp/speechkit/dk$a;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/dk$a;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/nuance/nmdp/speechkit/cp;->g:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clearResLogsToServer() after clean the log vector tranId["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] log list size ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/dn;)Lcom/nuance/nmdp/speechkit/bg$j;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "dict can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/nuance/nmdp/speechkit/cu;

    check-cast p2, Lcom/nuance/nmdp/speechkit/cv;

    invoke-direct {v0, p1, p2}, Lcom/nuance/nmdp/speechkit/cu;-><init>(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/cv;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/dn;Lcom/nuance/nmdp/speechkit/bx;)Lcom/nuance/nmdp/speechkit/bg$j;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "tts_dict can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "audioSink can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/nuance/nmdp/speechkit/dj;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/cp;->a:Lcom/nuance/nmdp/speechkit/cm;

    check-cast p2, Lcom/nuance/nmdp/speechkit/cv;

    invoke-direct {v0, p1, v1, p2, p3}, Lcom/nuance/nmdp/speechkit/dj;-><init>(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/cm;Lcom/nuance/nmdp/speechkit/cv;Lcom/nuance/nmdp/speechkit/bx;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/nuance/nmdp/speechkit/bx;
    .locals 3

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/nuance/nmdp/speechkit/cr;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/cp;->a:Lcom/nuance/nmdp/speechkit/cm;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/cp;->h:Lcom/nuance/nmdp/speechkit/bi;

    invoke-direct {v0, p1, v1, v2}, Lcom/nuance/nmdp/speechkit/cr;-><init>(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/cm;Lcom/nuance/nmdp/speechkit/bi;)V

    return-object v0
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/dq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/nmdp/speechkit/dn;)Lcom/nuance/nmdp/speechkit/dm;
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/nmdp/speechkit/cj;
        }
    .end annotation

    const-string v2, ""

    if-nez p1, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "commandListener is invalid; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    if-eqz p2, :cond_1

    const-string v3, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "cmd should be non-null; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_2
    if-eqz p3, :cond_3

    const-string v3, ""

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "scriptVersion should be non-null; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_4
    if-eqz p4, :cond_5

    const-string v3, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "dictationLanguage should be non-null; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_6
    if-eqz p6, :cond_7

    const-string v3, ""

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "phoneModel should be non-null; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_8
    const-wide/16 v4, 0x7530

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "commandTimeout is invalid; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_9
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    sget-object v3, Lcom/nuance/nmdp/speechkit/cp;->g:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NMASResourceImpl.createCommand() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/nmdp/speechkit/cp;->e:Ljava/lang/Object;

    move-object/from16 v24, v0

    monitor-enter v24

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    invoke-virtual {v2}, Lcom/nuance/nmdp/speechkit/dk;->b()V

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/nmdp/speechkit/cp;->a:Lcom/nuance/nmdp/speechkit/cm;

    invoke-static {}, Lcom/nuance/nmdp/speechkit/cm;->b()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/nuance/nmdp/speechkit/cp;->d:I

    if-nez v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/nmdp/speechkit/cp;->h:Lcom/nuance/nmdp/speechkit/bi;

    new-instance v3, Lcom/nuance/nmdp/speechkit/bi$a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/nuance/nmdp/speechkit/bi$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/nuance/nmdp/speechkit/cp;->h:Lcom/nuance/nmdp/speechkit/bi;

    invoke-interface {v5}, Lcom/nuance/nmdp/speechkit/bi;->a()[Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-interface {v2, v3, v0, v4}, Lcom/nuance/nmdp/speechkit/bi;->a(Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bi$b;Ljava/lang/Object;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/nmdp/speechkit/cp;->a:Lcom/nuance/nmdp/speechkit/cm;

    invoke-virtual {v2}, Lcom/nuance/nmdp/speechkit/cm;->f()B

    move-result v2

    move-object/from16 v0, p0

    iput-byte v2, v0, Lcom/nuance/nmdp/speechkit/cp;->f:B

    new-instance v2, Lcom/nuance/nmdp/speechkit/dk;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/nuance/nmdp/speechkit/cp;->h:Lcom/nuance/nmdp/speechkit/bi;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/nmdp/speechkit/cp;->c:Lcom/nuance/nmdp/speechkit/cf;

    check-cast v4, Lcom/nuance/nmdp/speechkit/cl;

    iget-object v6, v4, Lcom/nuance/nmdp/speechkit/cl;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/nuance/nmdp/speechkit/cp;->a:Lcom/nuance/nmdp/speechkit/cm;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/nmdp/speechkit/cp;->c:Lcom/nuance/nmdp/speechkit/cf;

    check-cast v4, Lcom/nuance/nmdp/speechkit/cl;

    invoke-virtual {v4}, Lcom/nuance/nmdp/speechkit/cl;->d()Ljava/lang/String;

    move-result-object v8

    const-string v9, "1"

    sget-object v10, Lcom/nuance/nmdp/speechkit/cp;->k:Ljava/lang/String;

    const-string v12, "enus"

    const-string v13, "ne"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/nmdp/speechkit/cp;->c:Lcom/nuance/nmdp/speechkit/cf;

    check-cast v4, Lcom/nuance/nmdp/speechkit/cl;

    invoke-virtual {v4}, Lcom/nuance/nmdp/speechkit/cl;->e()Lcom/nuance/nmdp/speechkit/bf;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/nmdp/speechkit/cp;->c:Lcom/nuance/nmdp/speechkit/cf;

    check-cast v4, Lcom/nuance/nmdp/speechkit/cl;

    invoke-virtual {v4}, Lcom/nuance/nmdp/speechkit/cl;->d()Ljava/lang/String;

    move-result-object v18

    const-string v19, ""

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/nmdp/speechkit/cp;->i:Lcom/nuance/nmdp/speechkit/ck;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/nuance/nmdp/speechkit/cp;->f:B

    move/from16 v23, v0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v11, p3

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v17, p6

    move-object/from16 v20, p7

    move-object/from16 v21, p0

    invoke-direct/range {v2 .. v23}, Lcom/nuance/nmdp/speechkit/dk;-><init>(Lcom/nuance/nmdp/speechkit/bi;Lcom/nuance/nmdp/speechkit/dq;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/nmdp/speechkit/cm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/nmdp/speechkit/bf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/nmdp/speechkit/dn;Lcom/nuance/nmdp/speechkit/cp;Lcom/nuance/nmdp/speechkit/ck;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    monitor-exit v24
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v2

    :catchall_0
    move-exception v2

    monitor-exit v24

    throw v2
.end method

.method public final a()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/nmdp/speechkit/cj;
        }
    .end annotation

    const/4 v3, 0x0

    sget-object v0, Lcom/nuance/nmdp/speechkit/cp;->g:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "freeResource() disconnectTimeout:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/dk;->b()V

    :cond_0
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/cp;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/cp;->d:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/nuance/nmdp/speechkit/cp;->d:I

    new-instance v0, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/cp;->h:Lcom/nuance/nmdp/speechkit/bi;

    new-instance v3, Lcom/nuance/nmdp/speechkit/bi$a;

    const/4 v4, 0x3

    invoke-direct {v3, v4, v0}, Lcom/nuance/nmdp/speechkit/bi$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/cp;->h:Lcom/nuance/nmdp/speechkit/bi;

    invoke-interface {v4}, Lcom/nuance/nmdp/speechkit/bi;->a()[Ljava/lang/Object;

    invoke-interface {v2, v3, p0, v0}, Lcom/nuance/nmdp/speechkit/bi;->a(Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bi$b;Ljava/lang/Object;)V

    monitor-exit v1

    return-void

    :cond_1
    new-instance v0, Lcom/nuance/nmdp/speechkit/cj;

    const-string v2, "the resource was unloaded. "

    invoke-direct {v0, v2}, Lcom/nuance/nmdp/speechkit/cj;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(BS)V
    .locals 1

    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/cp;->f:B

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/dk;->b()V

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/nuance/nmdp/speechkit/co;->a(BS)V

    goto :goto_0
.end method

.method public final a(B[B)V
    .locals 4

    invoke-static {p2}, Lcom/nuance/nmdp/speechkit/cz;->a([B)Lcom/nuance/nmdp/speechkit/cy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/cy;->e()S

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    sget-object v1, Lcom/nuance/nmdp/speechkit/cp;->g:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Session.parseXModeMsgBcpData() Unknown command: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/cy;->e()S

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/nuance/nmdp/speechkit/cp;->a(B)V

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    check-cast v0, Lcom/nuance/nmdp/speechkit/dg;

    invoke-virtual {v1, v0, p1}, Lcom/nuance/nmdp/speechkit/dk;->a(Lcom/nuance/nmdp/speechkit/dg;B)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    check-cast v0, Lcom/nuance/nmdp/speechkit/de;

    invoke-virtual {v1, v0, p1}, Lcom/nuance/nmdp/speechkit/dk;->a(Lcom/nuance/nmdp/speechkit/de;B)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/nuance/nmdp/speechkit/cp;->a(B)V

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    check-cast v0, Lcom/nuance/nmdp/speechkit/dh;

    invoke-virtual {v1, v0, p1}, Lcom/nuance/nmdp/speechkit/dk;->a(Lcom/nuance/nmdp/speechkit/dh;B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7201
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    move-object v0, p1

    check-cast v0, Lcom/nuance/nmdp/speechkit/bi$a;

    iget-byte v0, v0, Lcom/nuance/nmdp/speechkit/bi$a;->a:B

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/nuance/nmdp/speechkit/co;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :pswitch_1
    invoke-super {p0, p0}, Lcom/nuance/nmdp/speechkit/co;->a(Lcom/nuance/nmdp/speechkit/cn;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->a:Lcom/nuance/nmdp/speechkit/cm;

    invoke-virtual {v0, p0}, Lcom/nuance/nmdp/speechkit/cm;->b(Lcom/nuance/nmdp/speechkit/cn;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->i:Lcom/nuance/nmdp/speechkit/ck;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(S)V
    .locals 3

    sget-object v0, Lcom/nuance/nmdp/speechkit/cp;->g:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSessionDisconnected() reasonCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    invoke-virtual {v0, p1}, Lcom/nuance/nmdp/speechkit/dk;->a(S)V

    :cond_0
    invoke-super {p0, p1}, Lcom/nuance/nmdp/speechkit/co;->a(S)V

    return-void
.end method

.method public final a([B)V
    .locals 1

    invoke-super {p0, p1}, Lcom/nuance/nmdp/speechkit/co;->a([B)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    invoke-virtual {v0, p1}, Lcom/nuance/nmdp/speechkit/dk;->a([B)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/cp;->j:Lcom/nuance/nmdp/speechkit/dk;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/dk;->c()V

    return-void
.end method

.method public final e()J
    .locals 2

    invoke-super {p0}, Lcom/nuance/nmdp/speechkit/co;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()Lcom/nuance/nmdp/speechkit/dn;
    .locals 1

    new-instance v0, Lcom/nuance/nmdp/speechkit/cv;

    invoke-direct {v0}, Lcom/nuance/nmdp/speechkit/cv;-><init>()V

    return-object v0
.end method
