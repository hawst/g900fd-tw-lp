.class public final Lcom/nuance/nmdp/speechkit/dx$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/nmdp/speechkit/dx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# instance fields
.field private synthetic a:Lcom/nuance/nmdp/speechkit/dy;


# direct methods
.method constructor <init>(Lcom/nuance/nmdp/speechkit/dy;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-static {}, Lcom/nuance/nmdp/speechkit/dy;->e()Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/nuance/nmdp/speechkit/dy;->e()Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    const-string v1, "BluetoothHeadsetOEM reflected onServiceConnected()"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->c(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->a(Lcom/nuance/nmdp/speechkit/dy;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->b(Lcom/nuance/nmdp/speechkit/dy;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->c(Lcom/nuance/nmdp/speechkit/dy;)Z

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->a(Lcom/nuance/nmdp/speechkit/dy;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->d(Lcom/nuance/nmdp/speechkit/dy;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 2

    invoke-static {}, Lcom/nuance/nmdp/speechkit/dy;->e()Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/nuance/nmdp/speechkit/dy;->e()Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    const-string v1, "BluetoothHeadsetOEM reflected onServiceDisconnected()"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->c(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->a(Lcom/nuance/nmdp/speechkit/dy;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->d(Lcom/nuance/nmdp/speechkit/dy;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->b(Lcom/nuance/nmdp/speechkit/dy;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->c(Lcom/nuance/nmdp/speechkit/dy;)Z

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/dx$a;->a:Lcom/nuance/nmdp/speechkit/dy;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/dy;->a(Lcom/nuance/nmdp/speechkit/dy;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
