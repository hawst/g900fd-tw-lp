.class public final Lcom/nuance/nmdp/speechkit/y;
.super Lcom/nuance/nmdp/speechkit/q;


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/r;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/nuance/nmdp/speechkit/q;-><init>(Lcom/nuance/nmdp/speechkit/r;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/y;->g()V

    return-void
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/y;->a:Lcom/nuance/nmdp/speechkit/r;

    new-instance v1, Lcom/nuance/nmdp/speechkit/v;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/y;->a:Lcom/nuance/nmdp/speechkit/r;

    invoke-direct {v1, v2}, Lcom/nuance/nmdp/speechkit/v;-><init>(Lcom/nuance/nmdp/speechkit/r;)V

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/r;->a(Lcom/nuance/nmdp/speechkit/q;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/y;->a:Lcom/nuance/nmdp/speechkit/r;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/r;->e()V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/y;->a:Lcom/nuance/nmdp/speechkit/r;

    const-string v1, "PING"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/r;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/y;->a:Lcom/nuance/nmdp/speechkit/r;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/r;->g()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error starting PingingState"

    invoke-static {p0, v1, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/y;->g()V

    goto :goto_0
.end method

.method protected final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p2}, Lcom/nuance/nmdp/speechkit/y;->c(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/dn;)V
    .locals 0

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/y;->g()V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Query error"

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/y;->c(Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Query retry"

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/y;->c(Ljava/lang/String;)V

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/y;->g()V

    return-void
.end method

.method public final e()V
    .locals 1

    const-string v0, "Create command failed"

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/y;->c(Ljava/lang/String;)V

    return-void
.end method

.method public final f()V
    .locals 1

    const-string v0, "Command event"

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/y;->c(Ljava/lang/String;)V

    return-void
.end method
