.class public final Lcom/nuance/nmdp/speechkit/u;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

.field private b:Lcom/nuance/nmdp/speechkit/cf;

.field private c:Lcom/nuance/nmdp/speechkit/bf;

.field private d:Lcom/nuance/nmdp/speechkit/bf;

.field private final e:Lcom/nuance/nmdp/speechkit/ch;

.field private f:Lcom/nuance/nmdp/speechkit/s;

.field private g:Ljava/lang/String;

.field private h:Z

.field private final i:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/s;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    new-instance v0, Lcom/nuance/nmdp/speechkit/u$4;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/u$4;-><init>(Lcom/nuance/nmdp/speechkit/u;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->e:Lcom/nuance/nmdp/speechkit/ch;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->g:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/u;->h:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->i:Ljava/lang/Object;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->p()Lcom/nuance/nmdp/speechkit/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->c:Lcom/nuance/nmdp/speechkit/bf;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->q()Lcom/nuance/nmdp/speechkit/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->d:Lcom/nuance/nmdp/speechkit/bf;

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/u;->e()Lcom/nuance/nmdp/speechkit/cf;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/u;->h:Z

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/u;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/u;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/u;)V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "Restarting NMSP manager"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->p()Lcom/nuance/nmdp/speechkit/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->c:Lcom/nuance/nmdp/speechkit/bf;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->q()Lcom/nuance/nmdp/speechkit/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->d:Lcom/nuance/nmdp/speechkit/bf;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/cf;->a_()V

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/u;->e()Lcom/nuance/nmdp/speechkit/cf;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    if-nez v0, :cond_0

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/u;->h:Z

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/u;Lcom/nuance/nmdp/speechkit/SpeechKit$a;)V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/u;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->i:Ljava/lang/Object;

    return-object v0
.end method

.method private e()Lcom/nuance/nmdp/speechkit/cf;
    .locals 9

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->e()I

    move-result v0

    int-to-short v1, v0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v2}, Lcom/nuance/nmdp/speechkit/s;->r()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v3}, Lcom/nuance/nmdp/speechkit/s;->f()Z

    move-result v3

    new-instance v7, Lcom/nuance/nmdp/speechkit/aw;

    invoke-direct {v7}, Lcom/nuance/nmdp/speechkit/aw;-><init>()V

    new-instance v4, Lcom/nuance/nmdp/speechkit/bz;

    const-string v5, "Android_Context"

    iget-object v6, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v6}, Lcom/nuance/nmdp/speechkit/s;->a()Ljava/lang/Object;

    move-result-object v6

    sget-object v8, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    invoke-direct {v4, v5, v6, v8}, Lcom/nuance/nmdp/speechkit/bz;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bz$a;)V

    invoke-virtual {v7, v4}, Lcom/nuance/nmdp/speechkit/aw;->a(Ljava/lang/Object;)V

    if-eqz v2, :cond_0

    new-instance v4, Lcom/nuance/nmdp/speechkit/bz;

    const-string v5, "SocketConnectionSetting"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    sget-object v6, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    invoke-direct {v4, v5, v2, v6}, Lcom/nuance/nmdp/speechkit/bz;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bz$a;)V

    invoke-virtual {v7, v4}, Lcom/nuance/nmdp/speechkit/aw;->a(Ljava/lang/Object;)V

    :cond_0
    if-eqz v3, :cond_1

    new-instance v2, Lcom/nuance/nmdp/speechkit/bz;

    const-string v3, "SSL_Socket_Enable"

    const-string v4, "TRUE"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    sget-object v5, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    invoke-direct {v2, v3, v4, v5}, Lcom/nuance/nmdp/speechkit/bz;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bz$a;)V

    invoke-virtual {v7, v2}, Lcom/nuance/nmdp/speechkit/aw;->a(Ljava/lang/Object;)V

    :cond_1
    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v2}, Lcom/nuance/nmdp/speechkit/s;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v3}, Lcom/nuance/nmdp/speechkit/s;->c()[B

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v4}, Lcom/nuance/nmdp/speechkit/s;->n()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/nuance/nmdp/speechkit/u;->d:Lcom/nuance/nmdp/speechkit/bf;

    iget-object v6, p0, Lcom/nuance/nmdp/speechkit/u;->c:Lcom/nuance/nmdp/speechkit/bf;

    invoke-virtual {v7}, Lcom/nuance/nmdp/speechkit/aw;->f()Ljava/util/Vector;

    move-result-object v7

    iget-object v8, p0, Lcom/nuance/nmdp/speechkit/u;->e:Lcom/nuance/nmdp/speechkit/ch;

    invoke-static/range {v0 .. v8}, Lcom/nuance/nmdp/speechkit/cg;->a(Ljava/lang/String;SLjava/lang/String;[BLjava/lang/String;Lcom/nuance/nmdp/speechkit/bf;Lcom/nuance/nmdp/speechkit/bf;Ljava/util/Vector;Lcom/nuance/nmdp/speechkit/ch;)Lcom/nuance/nmdp/speechkit/cf;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Unable to create NMSP manager"

    invoke-static {p0, v1, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->p()Lcom/nuance/nmdp/speechkit/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/s;->q()Lcom/nuance/nmdp/speechkit/bf;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/u;->c:Lcom/nuance/nmdp/speechkit/bf;

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/u;->d:Lcom/nuance/nmdp/speechkit/bf;

    if-eq v2, v1, :cond_1

    :cond_0
    const-string v2, "Supported codecs changed, restarting NMSP manager"

    invoke-static {p0, v2}, Lcom/nuance/nmdp/speechkit/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->c:Lcom/nuance/nmdp/speechkit/bf;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->d:Lcom/nuance/nmdp/speechkit/bf;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/cf;->a_()V

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/u;->e()Lcom/nuance/nmdp/speechkit/cf;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    if-nez v0, :cond_1

    iput-object v3, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/u;->h:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/nuance/nmdp/speechkit/p;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/nuance/nmdp/speechkit/u;->h:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/u;->f()V

    new-instance v1, Lcom/nuance/nmdp/speechkit/u$3;

    invoke-direct {v1, p0, p1}, Lcom/nuance/nmdp/speechkit/u$3;-><init>(Lcom/nuance/nmdp/speechkit/u;Lcom/nuance/nmdp/speechkit/p;)V

    new-instance v0, Lcom/nuance/nmdp/speechkit/x;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    invoke-direct {v0, v2, v3, v1}, Lcom/nuance/nmdp/speechkit/x;-><init>(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/s;Lcom/nuance/nmdp/speechkit/p;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/nmdp/speechkit/ao;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;
    .locals 9

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/u;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/SpeechKit$a;->b()V

    :cond_1
    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/u;->f()V

    new-instance v0, Lcom/nuance/nmdp/speechkit/as;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    iget-object v7, p0, Lcom/nuance/nmdp/speechkit/u;->c:Lcom/nuance/nmdp/speechkit/bf;

    new-instance v8, Lcom/nuance/nmdp/speechkit/u$2;

    invoke-direct {v8, p0, p5}, Lcom/nuance/nmdp/speechkit/u$2;-><init>(Lcom/nuance/nmdp/speechkit/u;Lcom/nuance/nmdp/speechkit/ao;)V

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/nuance/nmdp/speechkit/as;-><init>(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/s;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/nmdp/speechkit/bf;Lcom/nuance/nmdp/speechkit/ao;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ae;Lcom/nuance/nmdp/speechkit/ac;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;
    .locals 14

    iget-boolean v1, p0, Lcom/nuance/nmdp/speechkit/u;->h:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    invoke-interface {v1}, Lcom/nuance/nmdp/speechkit/SpeechKit$a;->b()V

    :cond_1
    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/u;->f()V

    new-instance v1, Lcom/nuance/nmdp/speechkit/ah;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    new-instance v13, Lcom/nuance/nmdp/speechkit/u$1;

    move-object/from16 v0, p10

    invoke-direct {v13, p0, v0}, Lcom/nuance/nmdp/speechkit/u$1;-><init>(Lcom/nuance/nmdp/speechkit/u;Lcom/nuance/nmdp/speechkit/ac;)V

    move-object v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    invoke-direct/range {v1 .. v13}, Lcom/nuance/nmdp/speechkit/ah;-><init>(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/s;Ljava/lang/String;ZZLjava/lang/String;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ae;Lcom/nuance/nmdp/speechkit/ac;)V

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/u;->h:Z

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/SpeechKit$a;->b()V

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/cf;->a_()V

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->b:Lcom/nuance/nmdp/speechkit/cf;

    :cond_1
    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->f:Lcom/nuance/nmdp/speechkit/s;

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/u;->h:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/u;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->g:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/SpeechKit$a;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/u;->a:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    :cond_0
    return-void
.end method
