.class public Lcom/nuance/nmdp/speechkit/dp;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/nuance/nmdp/speechkit/bh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/nuance/nmdp/speechkit/dp;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Class;)Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    sput-object v0, Lcom/nuance/nmdp/speechkit/dp;->a:Lcom/nuance/nmdp/speechkit/bh;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/ck;)Lcom/nuance/nmdp/speechkit/do;
    .locals 2

    sget-object v0, Lcom/nuance/nmdp/speechkit/dp;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/dp;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "createNMASResource"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    if-nez p0, :cond_1

    sget-object v0, Lcom/nuance/nmdp/speechkit/dp;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "manager is null"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "manager can not be null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p1, :cond_2

    sget-object v0, Lcom/nuance/nmdp/speechkit/dp;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "nmasListener is null"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "nmasListener can not be null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/nuance/nmdp/speechkit/cp;

    check-cast p0, Lcom/nuance/nmdp/speechkit/cl;

    invoke-direct {v0, p0, p1}, Lcom/nuance/nmdp/speechkit/cp;-><init>(Lcom/nuance/nmdp/speechkit/cl;Lcom/nuance/nmdp/speechkit/ck;)V

    return-object v0
.end method
