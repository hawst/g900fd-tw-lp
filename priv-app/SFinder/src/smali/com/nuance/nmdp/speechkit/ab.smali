.class final Lcom/nuance/nmdp/speechkit/ab;
.super Lcom/nuance/nmdp/speechkit/ai;


# instance fields
.field private final c:[Ljava/lang/String;

.field private final d:[I

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/ah;[Ljava/lang/String;[ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/nuance/nmdp/speechkit/ai;-><init>(Lcom/nuance/nmdp/speechkit/ah;)V

    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/ab;->c:[Ljava/lang/String;

    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/ab;->d:[I

    iput-object p4, p0, Lcom/nuance/nmdp/speechkit/ab;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ab;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Recognition result top choice: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ab;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ab;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ab;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Recognition suggestion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ab;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ab;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ah;->f()V

    invoke-virtual {p0}, Lcom/nuance/nmdp/speechkit/ab;->m()Lcom/nuance/nmdp/speechkit/ac;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ab;->b:Lcom/nuance/nmdp/speechkit/ah;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/ab;->c:[Ljava/lang/String;

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/ab;->d:[I

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/ab;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/nuance/nmdp/speechkit/ac;->a(Lcom/nuance/nmdp/speechkit/SpeechKit$a;[Ljava/lang/String;[ILjava/lang/String;)V

    invoke-virtual {p0}, Lcom/nuance/nmdp/speechkit/ab;->m()Lcom/nuance/nmdp/speechkit/ac;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ab;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/ac;->a(Lcom/nuance/nmdp/speechkit/SpeechKit$a;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ab;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ah;->l()V

    return-void
.end method
