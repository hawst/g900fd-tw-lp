.class public Lcom/nuance/nmdp/speechkit/df;
.super Lcom/nuance/nmdp/speechkit/cy;


# static fields
.field private static final a:Lcom/nuance/nmdp/speechkit/bh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/nuance/nmdp/speechkit/df;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Class;)Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    sput-object v0, Lcom/nuance/nmdp/speechkit/df;->a:Lcom/nuance/nmdp/speechkit/bh;

    return-void
.end method

.method public constructor <init>(Lcom/nuance/nmdp/speechkit/db;)V
    .locals 5

    const/16 v4, 0xe0

    const/4 v3, 0x0

    const/16 v2, 0xc1

    const/16 v0, 0x203

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/cy;-><init>(S)V

    sget-object v0, Lcom/nuance/nmdp/speechkit/df;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/df;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "PDXQueryParameter()"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    const-string v0, "name"

    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/db;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;Ljava/lang/String;S)V

    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/db;->c()B

    move-result v0

    const/16 v1, 0x7f

    if-ne v0, v1, :cond_1

    const-string v0, "type"

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;I)V

    :goto_0
    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/db;->c()B

    move-result v0

    sparse-switch v0, :sswitch_data_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/df;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXQueryParameter() Unknown parameter type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/db;->c()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_1
    const-string v0, "type"

    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/db;->c()B

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;I)V

    goto :goto_0

    :sswitch_0
    const-string v0, "buffer_id"

    check-cast p1, Lcom/nuance/nmdp/speechkit/cr;

    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/cr;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;I)V

    goto :goto_1

    :sswitch_1
    const-string v0, "text"

    invoke-virtual {p0, v0, v3, v2}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;Ljava/lang/String;S)V

    goto :goto_1

    :sswitch_2
    const-string v0, "data"

    invoke-virtual {p0, v0, v3, v2}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    :sswitch_3
    const-string v0, "dict"

    check-cast p1, Lcom/nuance/nmdp/speechkit/cu;

    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/cu;->a()[B

    move-result-object v1

    invoke-virtual {p0, v0, v1, v4}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    :sswitch_4
    const-string v0, "dict"

    invoke-virtual {v3}, Lcom/nuance/nmdp/speechkit/cv;->b()[B

    move-result-object v1

    invoke-virtual {p0, v0, v1, v4}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    :sswitch_5
    const-string v0, "text"

    invoke-virtual {p0, v0, v3, v2}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;Ljava/lang/String;S)V

    goto :goto_1

    :sswitch_6
    const-string v0, "dict"

    check-cast p1, Lcom/nuance/nmdp/speechkit/dj;

    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/dj;->d()[B

    move-result-object v1

    invoke-virtual {p0, v0, v1, v4}, Lcom/nuance/nmdp/speechkit/df;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_5
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_4
        0x8 -> :sswitch_4
        0x7f -> :sswitch_6
    .end sparse-switch
.end method
