.class final Lcom/nuance/nmdp/speechkit/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/Recognition;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/nmdp/speechkit/a$a;
    }
.end annotation


# instance fields
.field private a:[Ljava/lang/String;

.field private b:[I

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;[ILjava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/a;->a:[Ljava/lang/String;

    :goto_0
    if-eqz p2, :cond_1

    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/a;->b:[I

    :goto_1
    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/a;->c:Ljava/lang/String;

    return-void

    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/a;->a:[Ljava/lang/String;

    goto :goto_0

    :cond_1
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/a;->b:[I

    goto :goto_1
.end method


# virtual methods
.method public final getResult(I)Lcom/nuance/nmdp/speechkit/Recognition$Result;
    .locals 3

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/a;->b:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "index must be >= 0 and < getResultCount()."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/nuance/nmdp/speechkit/a$a;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/a;->a:[Ljava/lang/String;

    aget-object v1, v1, p1

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/a;->b:[I

    aget v2, v2, p1

    invoke-direct {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/a$a;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public final getResultCount()I
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/a;->b:[I

    array-length v0, v0

    return v0
.end method

.method public final getSuggestion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/a;->c:Ljava/lang/String;

    return-object v0
.end method
