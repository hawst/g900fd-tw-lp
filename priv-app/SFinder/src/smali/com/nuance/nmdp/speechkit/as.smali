.class public final Lcom/nuance/nmdp/speechkit/as;
.super Lcom/nuance/nmdp/speechkit/r;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/SpeechKit$a;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/nuance/nmdp/speechkit/ax;

.field private i:Lcom/nuance/nmdp/speechkit/ba;


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/s;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/nmdp/speechkit/bf;Lcom/nuance/nmdp/speechkit/ao;)V
    .locals 6

    invoke-direct {p0, p1, p2, p8}, Lcom/nuance/nmdp/speechkit/r;-><init>(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/s;Lcom/nuance/nmdp/speechkit/p;)V

    if-eqz p5, :cond_0

    iput-object p5, p0, Lcom/nuance/nmdp/speechkit/as;->b:Ljava/lang/String;

    :cond_0
    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/as;->f:Ljava/lang/String;

    if-eqz p6, :cond_1

    const-string v0, "ssml"

    :goto_0
    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->g:Ljava/lang/String;

    iput-object p4, p0, Lcom/nuance/nmdp/speechkit/as;->e:Ljava/lang/String;

    new-instance v0, Lcom/nuance/nmdp/speechkit/as$1;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/as$1;-><init>(Lcom/nuance/nmdp/speechkit/as;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->h:Lcom/nuance/nmdp/speechkit/ax;

    new-instance v0, Lcom/nuance/nmdp/speechkit/ba;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/as;->c:Lcom/nuance/nmdp/speechkit/cf;

    invoke-virtual {p2}, Lcom/nuance/nmdp/speechkit/s;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/as;->h:Lcom/nuance/nmdp/speechkit/ax;

    move-object v2, p0

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/nuance/nmdp/speechkit/ba;-><init>(Lcom/nuance/nmdp/speechkit/cf;Ljava/lang/Object;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/ax;Lcom/nuance/nmdp/speechkit/bf;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->i:Lcom/nuance/nmdp/speechkit/ba;

    new-instance v0, Lcom/nuance/nmdp/speechkit/aq;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/aq;-><init>(Lcom/nuance/nmdp/speechkit/as;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->a:Lcom/nuance/nmdp/speechkit/q;

    return-void

    :cond_1
    const-string v0, "text"

    goto :goto_0
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/as;)Lcom/nuance/nmdp/speechkit/ap;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->a:Lcom/nuance/nmdp/speechkit/q;

    check-cast v0, Lcom/nuance/nmdp/speechkit/ap;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/nuance/nmdp/speechkit/dn;)V
    .locals 2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "tts_voice"

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/as;->e:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "tts_language"

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/as;->b:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->i:Lcom/nuance/nmdp/speechkit/ba;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ba;->a()V

    return-void
.end method

.method public final l()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->i:Lcom/nuance/nmdp/speechkit/ba;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ba;->b()V

    return-void
.end method

.method public final m()Lcom/nuance/nmdp/speechkit/bx;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/as;->i:Lcom/nuance/nmdp/speechkit/ba;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ba;->c()Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    move-result-object v0

    return-object v0
.end method
