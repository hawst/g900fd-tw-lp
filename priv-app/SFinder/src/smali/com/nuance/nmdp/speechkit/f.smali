.class final Lcom/nuance/nmdp/speechkit/f;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/nuance/nmdp/speechkit/f;

.field private static b:Ljava/lang/Object;


# instance fields
.field private c:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

.field private d:Lcom/nuance/nmdp/speechkit/u;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private h:Z

.field private i:Ljava/lang/Object;

.field private final j:Lcom/nuance/nmdp/speechkit/aw;

.field private k:Lcom/nuance/nmdp/speechkit/Prompt;

.field private l:Lcom/nuance/nmdp/speechkit/Prompt;

.field private m:Lcom/nuance/nmdp/speechkit/Prompt;

.field private n:Lcom/nuance/nmdp/speechkit/Prompt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;IZ[B)V
    .locals 7

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/f;->e:Ljava/lang/String;

    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/f;->f:Ljava/lang/String;

    iput p4, p0, Lcom/nuance/nmdp/speechkit/f;->g:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->d:Lcom/nuance/nmdp/speechkit/u;

    new-instance v0, Lcom/nuance/nmdp/speechkit/aw;

    invoke-direct {v0}, Lcom/nuance/nmdp/speechkit/aw;-><init>()V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->j:Lcom/nuance/nmdp/speechkit/aw;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->i:Ljava/lang/Object;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->n:Lcom/nuance/nmdp/speechkit/Prompt;

    new-instance v0, Lcom/nuance/nmdp/speechkit/s;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/f;->f:Ljava/lang/String;

    iget v3, p0, Lcom/nuance/nmdp/speechkit/f;->g:I

    iget-object v5, p0, Lcom/nuance/nmdp/speechkit/f;->e:Ljava/lang/String;

    move-object v1, p1

    move v4, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/nuance/nmdp/speechkit/s;-><init>(Ljava/lang/Object;Ljava/lang/String;IZLjava/lang/String;[B)V

    new-instance v1, Lcom/nuance/nmdp/speechkit/f$1;

    invoke-direct {v1, p0, v0}, Lcom/nuance/nmdp/speechkit/f$1;-><init>(Lcom/nuance/nmdp/speechkit/f;Lcom/nuance/nmdp/speechkit/s;)V

    invoke-static {v1}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/f;Lcom/nuance/nmdp/speechkit/Prompt;)Lcom/nuance/nmdp/speechkit/Prompt;
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/f;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    return-object p1
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;IZ[B)Lcom/nuance/nmdp/speechkit/f;
    .locals 8

    const-string v0, "appContext"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "id"

    invoke-static {p1, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "host"

    invoke-static {p2, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "applicationKey"

    invoke-static {p5, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-ltz p3, :cond_0

    const v0, 0xffff

    if-le p3, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "port must be between 0 and 65535"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/RuntimeException;)V

    :cond_1
    sget-object v7, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v7

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "Initializing SpeechKit"

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/nuance/nmdp/speechkit/av;->a()V

    :cond_2
    sget-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    iget-object v1, v0, Lcom/nuance/nmdp/speechkit/f;->e:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/nuance/nmdp/speechkit/f;->f:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget v0, v0, Lcom/nuance/nmdp/speechkit/f;->g:I

    if-ne v0, p3, :cond_5

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    const/4 v0, 0x0

    const-string v1, "Releasing old SpeechKit before creating new instance"

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    invoke-direct {v0}, Lcom/nuance/nmdp/speechkit/f;->k()V

    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    :cond_3
    sget-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    const-string v1, "Creating fresh SpeechKit instance"

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/nuance/nmdp/speechkit/f;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/nuance/nmdp/speechkit/f;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;IZ[B)V

    sput-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    :cond_4
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    return-object v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/f;)Lcom/nuance/nmdp/speechkit/u;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->d:Lcom/nuance/nmdp/speechkit/u;

    return-object v0
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/f;Lcom/nuance/nmdp/speechkit/u;)Lcom/nuance/nmdp/speechkit/u;
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/f;->d:Lcom/nuance/nmdp/speechkit/u;

    return-object p1
.end method

.method static final a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must not be null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/RuntimeException;)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/RuntimeException;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/j;->c(Ljava/lang/Object;Ljava/lang/String;)V

    throw p0
.end method

.method static final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must not be null or empty"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/RuntimeException;)V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/f;Lcom/nuance/nmdp/speechkit/Prompt;)Lcom/nuance/nmdp/speechkit/Prompt;
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/f;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    return-object p1
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/f;)Lcom/nuance/nmdp/speechkit/aw;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->j:Lcom/nuance/nmdp/speechkit/aw;

    return-object v0
.end method

.method static b()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/nuance/nmdp/speechkit/f;Lcom/nuance/nmdp/speechkit/Prompt;)Lcom/nuance/nmdp/speechkit/Prompt;
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/f;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    return-object p1
.end method

.method static synthetic d(Lcom/nuance/nmdp/speechkit/f;Lcom/nuance/nmdp/speechkit/Prompt;)Lcom/nuance/nmdp/speechkit/Prompt;
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/f;->n:Lcom/nuance/nmdp/speechkit/Prompt;

    return-object p1
.end method

.method static final f()V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SpeechKit instance is released"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/RuntimeException;)V

    return-void
.end method

.method private k()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    new-instance v0, Lcom/nuance/nmdp/speechkit/f$2;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/f$2;-><init>(Lcom/nuance/nmdp/speechkit/f;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILjava/lang/String;Lcom/nuance/nmdp/speechkit/Recognizer$Listener;Ljava/lang/Object;)Lcom/nuance/nmdp/speechkit/Recognizer;
    .locals 8

    const-string v0, "type"

    invoke-static {p1, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "language"

    invoke-static {p3, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/nuance/nmdp/speechkit/f;->f()V

    :cond_0
    new-instance v0, Lcom/nuance/nmdp/speechkit/c;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/nuance/nmdp/speechkit/c;-><init>(Lcom/nuance/nmdp/speechkit/f;Ljava/lang/String;ILjava/lang/String;Lcom/nuance/nmdp/speechkit/Recognizer$Listener;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/f;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    invoke-virtual {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/c;->setPrompt(ILcom/nuance/nmdp/speechkit/Prompt;)V

    :cond_1
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/f;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    invoke-virtual {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/c;->setPrompt(ILcom/nuance/nmdp/speechkit/Prompt;)V

    :cond_2
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/f;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    invoke-virtual {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/c;->setPrompt(ILcom/nuance/nmdp/speechkit/Prompt;)V

    :cond_3
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->n:Lcom/nuance/nmdp/speechkit/Prompt;

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/f;->n:Lcom/nuance/nmdp/speechkit/Prompt;

    invoke-virtual {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/c;->setPrompt(ILcom/nuance/nmdp/speechkit/Prompt;)V

    :cond_4
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/Vocalizer$Listener;Ljava/lang/Object;)Lcom/nuance/nmdp/speechkit/Vocalizer;
    .locals 7

    const-string v0, "language"

    invoke-static {p1, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v6, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/nuance/nmdp/speechkit/f;->f()V

    :cond_0
    new-instance v0, Lcom/nuance/nmdp/speechkit/h;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/nuance/nmdp/speechkit/h;-><init>(Lcom/nuance/nmdp/speechkit/f;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/nmdp/speechkit/Vocalizer$Listener;Ljava/lang/Object;)V

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method final a(Lcom/nuance/nmdp/speechkit/Prompt;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->j:Lcom/nuance/nmdp/speechkit/aw;

    invoke-virtual {v0, p1}, Lcom/nuance/nmdp/speechkit/aw;->c(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->j:Lcom/nuance/nmdp/speechkit/aw;

    invoke-virtual {v0, p1}, Lcom/nuance/nmdp/speechkit/aw;->b(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lcom/nuance/nmdp/speechkit/Prompt;->a()Lcom/nuance/nmdp/speechkit/ay;

    move-result-object v0

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/ay;->c()V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    if-ne p1, v0, :cond_1

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->k:Lcom/nuance/nmdp/speechkit/Prompt;

    :cond_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    if-ne p1, v0, :cond_2

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->l:Lcom/nuance/nmdp/speechkit/Prompt;

    :cond_2
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    if-ne p1, v0, :cond_3

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->m:Lcom/nuance/nmdp/speechkit/Prompt;

    :cond_3
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->n:Lcom/nuance/nmdp/speechkit/Prompt;

    if-ne p1, v0, :cond_4

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/f;->n:Lcom/nuance/nmdp/speechkit/Prompt;

    :cond_4
    return-void
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/Prompt;Lcom/nuance/nmdp/speechkit/Prompt;Lcom/nuance/nmdp/speechkit/Prompt;Lcom/nuance/nmdp/speechkit/Prompt;)V
    .locals 6

    sget-object v1, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/nuance/nmdp/speechkit/f;->f()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Lcom/nuance/nmdp/speechkit/f$5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/nuance/nmdp/speechkit/f$5;-><init>(Lcom/nuance/nmdp/speechkit/f;Lcom/nuance/nmdp/speechkit/Prompt;Lcom/nuance/nmdp/speechkit/Prompt;Lcom/nuance/nmdp/speechkit/Prompt;Lcom/nuance/nmdp/speechkit/Prompt;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/SpeechKit$a;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/f;->c:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    return-void
.end method

.method final a(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/f;->i:Ljava/lang/Object;

    return-void
.end method

.method final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    return v0
.end method

.method public final b(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/Vocalizer$Listener;Ljava/lang/Object;)Lcom/nuance/nmdp/speechkit/Vocalizer;
    .locals 7

    const-string v0, "voice"

    invoke-static {p1, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v6, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/nuance/nmdp/speechkit/f;->f()V

    :cond_0
    new-instance v0, Lcom/nuance/nmdp/speechkit/h;

    const/4 v3, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/nuance/nmdp/speechkit/h;-><init>(Lcom/nuance/nmdp/speechkit/f;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/nmdp/speechkit/Vocalizer$Listener;Ljava/lang/Object;)V

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final b(Lcom/nuance/nmdp/speechkit/Prompt;)V
    .locals 2

    sget-object v1, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->j:Lcom/nuance/nmdp/speechkit/aw;

    invoke-virtual {v0, p1}, Lcom/nuance/nmdp/speechkit/aw;->a(Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final c()Lcom/nuance/nmdp/speechkit/u;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->d:Lcom/nuance/nmdp/speechkit/u;

    return-object v0
.end method

.method final d()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->i:Ljava/lang/Object;

    return-object v0
.end method

.method final e()V
    .locals 2

    sget-object v1, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/nuance/nmdp/speechkit/f;->f()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()V
    .locals 2

    sget-object v1, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    if-ne p0, v0, :cond_1

    const-string v0, "Releasing SpeechKit instance"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/f;->k()V

    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/nmdp/speechkit/f;->a:Lcom/nuance/nmdp/speechkit/f;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->c:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->c:Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    :cond_0
    new-instance v0, Lcom/nuance/nmdp/speechkit/f$3;

    invoke-direct {v0}, Lcom/nuance/nmdp/speechkit/f$3;-><init>()V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    const-string v0, "SpeechKit instance already released"

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->b(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()V
    .locals 2

    sget-object v1, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/nuance/nmdp/speechkit/f;->f()V

    :cond_0
    new-instance v0, Lcom/nuance/nmdp/speechkit/f$4;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/f$4;-><init>(Lcom/nuance/nmdp/speechkit/f;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    sget-object v1, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->d:Lcom/nuance/nmdp/speechkit/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/f;->d:Lcom/nuance/nmdp/speechkit/u;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/u;->c()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final j()V
    .locals 2

    sget-object v1, Lcom/nuance/nmdp/speechkit/f;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/f;->h:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/nuance/nmdp/speechkit/f;->f()V

    :cond_0
    new-instance v0, Lcom/nuance/nmdp/speechkit/f$6;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/f$6;-><init>(Lcom/nuance/nmdp/speechkit/f;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
