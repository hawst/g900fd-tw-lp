.class final Lcom/nuance/nmdp/speechkit/c$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/Recognizer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/nmdp/speechkit/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/nuance/nmdp/speechkit/c;


# direct methods
.method constructor <init>(Lcom/nuance/nmdp/speechkit/c;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/c$3;->a:Lcom/nuance/nmdp/speechkit/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Lcom/nuance/nmdp/speechkit/Recognizer;Lcom/nuance/nmdp/speechkit/SpeechError;)V
    .locals 2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c$3;->a:Lcom/nuance/nmdp/speechkit/c;

    new-instance v1, Lcom/nuance/nmdp/speechkit/c$3$1;

    invoke-direct {v1, p0, p2}, Lcom/nuance/nmdp/speechkit/c$3$1;-><init>(Lcom/nuance/nmdp/speechkit/c$3;Lcom/nuance/nmdp/speechkit/SpeechError;)V

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/c;->a(Lcom/nuance/nmdp/speechkit/c;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onRecordingBegin(Lcom/nuance/nmdp/speechkit/Recognizer;)V
    .locals 2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c$3;->a:Lcom/nuance/nmdp/speechkit/c;

    new-instance v1, Lcom/nuance/nmdp/speechkit/c$3$2;

    invoke-direct {v1, p0}, Lcom/nuance/nmdp/speechkit/c$3$2;-><init>(Lcom/nuance/nmdp/speechkit/c$3;)V

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/c;->b(Lcom/nuance/nmdp/speechkit/c;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onRecordingDone(Lcom/nuance/nmdp/speechkit/Recognizer;)V
    .locals 2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c$3;->a:Lcom/nuance/nmdp/speechkit/c;

    new-instance v1, Lcom/nuance/nmdp/speechkit/c$3$3;

    invoke-direct {v1, p0}, Lcom/nuance/nmdp/speechkit/c$3$3;-><init>(Lcom/nuance/nmdp/speechkit/c$3;)V

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/c;->c(Lcom/nuance/nmdp/speechkit/c;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onResults(Lcom/nuance/nmdp/speechkit/Recognizer;Lcom/nuance/nmdp/speechkit/Recognition;)V
    .locals 2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c$3;->a:Lcom/nuance/nmdp/speechkit/c;

    new-instance v1, Lcom/nuance/nmdp/speechkit/c$3$4;

    invoke-direct {v1, p0, p2}, Lcom/nuance/nmdp/speechkit/c$3$4;-><init>(Lcom/nuance/nmdp/speechkit/c$3;Lcom/nuance/nmdp/speechkit/Recognition;)V

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/c;->d(Lcom/nuance/nmdp/speechkit/c;Ljava/lang/Runnable;)V

    return-void
.end method
