.class public Lcom/nuance/nmdp/speechkit/de;
.super Lcom/nuance/nmdp/speechkit/cy;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/dr;


# static fields
.field private static final a:Lcom/nuance/nmdp/speechkit/bh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/nuance/nmdp/speechkit/de;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Class;)Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    sput-object v0, Lcom/nuance/nmdp/speechkit/de;->a:Lcom/nuance/nmdp/speechkit/bh;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    const/16 v0, 0x7202

    invoke-direct {p0, v0, p1}, Lcom/nuance/nmdp/speechkit/cy;-><init>(S[B)V

    return-void
.end method


# virtual methods
.method public final g()I
    .locals 2

    sget-object v0, Lcom/nuance/nmdp/speechkit/de;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "PDXQueryError.getError()"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    const-string v0, "error"

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/de;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/nuance/nmdp/speechkit/de;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "PDXQueryError.getDescription()"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    const-string v0, "description"

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/de;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
