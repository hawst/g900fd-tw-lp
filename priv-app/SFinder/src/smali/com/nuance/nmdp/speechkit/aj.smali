.class final Lcom/nuance/nmdp/speechkit/aj;
.super Lcom/nuance/nmdp/speechkit/z;


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/ah;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/nuance/nmdp/speechkit/z;-><init>(Lcom/nuance/nmdp/speechkit/ah;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    const/4 v3, 0x0

    const/4 v5, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ah;->e()V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    const-string v1, "NMDP_ASR_CMD"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/ah;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ah;->h()Lcom/nuance/nmdp/speechkit/dn;

    move-result-object v0

    const-string v1, "start"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/dn;->b(Ljava/lang/String;I)V

    const-string v1, "end"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/dn;->b(Ljava/lang/String;I)V

    const-string v1, "text"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    const-string v2, "REQUEST_INFO"

    invoke-virtual {v1, v2, v0}, Lcom/nuance/nmdp/speechkit/ah;->a(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/dn;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ah;->n()V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ah;->o()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error starting RecordStartingState"

    invoke-static {p0, v1, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v6, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    new-instance v0, Lcom/nuance/nmdp/speechkit/aa;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    const/4 v2, 0x3

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/nuance/nmdp/speechkit/aa;-><init>(Lcom/nuance/nmdp/speechkit/ah;ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v6, v0}, Lcom/nuance/nmdp/speechkit/ah;->a(Lcom/nuance/nmdp/speechkit/q;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/aj;->a(I)V

    return-void
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/aj;->a(I)V

    return-void
.end method

.method public final h()V
    .locals 3

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    new-instance v1, Lcom/nuance/nmdp/speechkit/ag;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/aj;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-direct {v1, v2}, Lcom/nuance/nmdp/speechkit/ag;-><init>(Lcom/nuance/nmdp/speechkit/ah;)V

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/ah;->a(Lcom/nuance/nmdp/speechkit/q;)V

    return-void
.end method

.method public final i()V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/aj;->a(I)V

    return-void
.end method

.method public final j()V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/aj;->a(I)V

    return-void
.end method
