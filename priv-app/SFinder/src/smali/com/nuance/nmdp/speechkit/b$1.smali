.class final Lcom/nuance/nmdp/speechkit/b$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/ac;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/nmdp/speechkit/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/nuance/nmdp/speechkit/b;


# direct methods
.method constructor <init>(Lcom/nuance/nmdp/speechkit/b;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/nuance/nmdp/speechkit/SpeechKit$a;)V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->a(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->b(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    :cond_0
    return-void
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/SpeechKit$a;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->a(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->c(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    new-instance v2, Lcom/nuance/nmdp/speechkit/d;

    invoke-direct {v2, p2, p3, p4}, Lcom/nuance/nmdp/speechkit/d;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/Recognizer$Listener;->onError(Lcom/nuance/nmdp/speechkit/Recognizer;Lcom/nuance/nmdp/speechkit/SpeechError;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/SpeechKit$a;[Ljava/lang/String;[ILjava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->a(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->c(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    new-instance v2, Lcom/nuance/nmdp/speechkit/a;

    invoke-direct {v2, p2, p3, p4}, Lcom/nuance/nmdp/speechkit/a;-><init>([Ljava/lang/String;[ILjava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/Recognizer$Listener;->onResults(Lcom/nuance/nmdp/speechkit/Recognizer;Lcom/nuance/nmdp/speechkit/Recognition;)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/nuance/nmdp/speechkit/SpeechKit$a;)V
    .locals 2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->a(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->c(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/Recognizer$Listener;->onRecordingBegin(Lcom/nuance/nmdp/speechkit/Recognizer;)V

    :cond_0
    return-void
.end method

.method public final c(Lcom/nuance/nmdp/speechkit/SpeechKit$a;)V
    .locals 2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->a(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/SpeechKit$a;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/b;->c(Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/b$1;->a:Lcom/nuance/nmdp/speechkit/b;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/Recognizer$Listener;->onRecordingDone(Lcom/nuance/nmdp/speechkit/Recognizer;)V

    :cond_0
    return-void
.end method
