.class final Lcom/nuance/nmdp/speechkit/c;
.super Lcom/nuance/nmdp/speechkit/i;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/Recognizer;


# instance fields
.field private b:Lcom/nuance/nmdp/speechkit/b;

.field private c:Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

.field private final d:Lcom/nuance/nmdp/speechkit/f;

.field private e:F

.field private final f:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/f;Ljava/lang/String;ILjava/lang/String;Lcom/nuance/nmdp/speechkit/Recognizer$Listener;Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0, p6}, Lcom/nuance/nmdp/speechkit/i;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/c;->d:Lcom/nuance/nmdp/speechkit/f;

    iput-object p5, p0, Lcom/nuance/nmdp/speechkit/c;->c:Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    const/4 v0, 0x0

    iput v0, p0, Lcom/nuance/nmdp/speechkit/c;->e:F

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/c;->f:Ljava/lang/Object;

    new-instance v0, Lcom/nuance/nmdp/speechkit/c$1;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/nuance/nmdp/speechkit/c$1;-><init>(Lcom/nuance/nmdp/speechkit/c;Ljava/lang/String;ILjava/lang/String;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/c;F)F
    .locals 0

    iput p1, p0, Lcom/nuance/nmdp/speechkit/c;->e:F

    return p1
.end method

.method private a()Lcom/nuance/nmdp/speechkit/Recognizer$Listener;
    .locals 2

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/c;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c;->c:Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/c;Lcom/nuance/nmdp/speechkit/b;)Lcom/nuance/nmdp/speechkit/b;
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/c;->b:Lcom/nuance/nmdp/speechkit/b;

    return-object p1
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/c;)Lcom/nuance/nmdp/speechkit/f;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c;->d:Lcom/nuance/nmdp/speechkit/f;

    return-object v0
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/c;Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/nuance/nmdp/speechkit/c;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/c;)Lcom/nuance/nmdp/speechkit/ae;
    .locals 1

    new-instance v0, Lcom/nuance/nmdp/speechkit/c$2;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/c$2;-><init>(Lcom/nuance/nmdp/speechkit/c;)V

    return-object v0
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/c;Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/nuance/nmdp/speechkit/c;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic c(Lcom/nuance/nmdp/speechkit/c;)Lcom/nuance/nmdp/speechkit/Recognizer$Listener;
    .locals 1

    new-instance v0, Lcom/nuance/nmdp/speechkit/c$3;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/c$3;-><init>(Lcom/nuance/nmdp/speechkit/c;)V

    return-object v0
.end method

.method static synthetic c(Lcom/nuance/nmdp/speechkit/c;Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/nuance/nmdp/speechkit/c;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic d(Lcom/nuance/nmdp/speechkit/c;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lcom/nuance/nmdp/speechkit/c;Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/nuance/nmdp/speechkit/c;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic e(Lcom/nuance/nmdp/speechkit/c;)Lcom/nuance/nmdp/speechkit/Recognizer$Listener;
    .locals 1

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/c;->a()Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/nuance/nmdp/speechkit/c;)Lcom/nuance/nmdp/speechkit/b;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c;->b:Lcom/nuance/nmdp/speechkit/b;

    return-object v0
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c;->d:Lcom/nuance/nmdp/speechkit/f;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/f;->e()V

    new-instance v0, Lcom/nuance/nmdp/speechkit/c$7;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/c$7;-><init>(Lcom/nuance/nmdp/speechkit/c;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final getAudioLevel()F
    .locals 2

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/c;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/c;->e:F

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final setListener(Lcom/nuance/nmdp/speechkit/Recognizer$Listener;)V
    .locals 2

    const-string v0, "listener"

    invoke-static {p1, v0}, Lcom/nuance/nmdp/speechkit/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/c;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/c;->c:Lcom/nuance/nmdp/speechkit/Recognizer$Listener;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final setPrompt(ILcom/nuance/nmdp/speechkit/Prompt;)V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c;->d:Lcom/nuance/nmdp/speechkit/f;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/f;->e()V

    new-instance v0, Lcom/nuance/nmdp/speechkit/c$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/nuance/nmdp/speechkit/c$4;-><init>(Lcom/nuance/nmdp/speechkit/c;ILcom/nuance/nmdp/speechkit/Prompt;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final start()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c;->d:Lcom/nuance/nmdp/speechkit/f;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/f;->e()V

    new-instance v0, Lcom/nuance/nmdp/speechkit/c$5;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/c$5;-><init>(Lcom/nuance/nmdp/speechkit/c;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final stopRecording()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/c;->d:Lcom/nuance/nmdp/speechkit/f;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/f;->e()V

    new-instance v0, Lcom/nuance/nmdp/speechkit/c$6;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/c$6;-><init>(Lcom/nuance/nmdp/speechkit/c;)V

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    return-void
.end method
