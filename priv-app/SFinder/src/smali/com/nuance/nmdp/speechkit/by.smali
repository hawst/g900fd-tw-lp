.class public abstract Lcom/nuance/nmdp/speechkit/by;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/nuance/nmdp/speechkit/bh;


# instance fields
.field private b:Ljava/lang/String;

.field private c:S

.field private d:Ljava/lang/String;

.field private e:Lcom/nuance/nmdp/speechkit/bi;

.field private f:Lcom/nuance/nmdp/speechkit/bf;

.field private g:Lcom/nuance/nmdp/speechkit/bf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/nuance/nmdp/speechkit/by;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Class;)Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    sput-object v0, Lcom/nuance/nmdp/speechkit/by;->a:Lcom/nuance/nmdp/speechkit/bh;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;SLjava/lang/String;Lcom/nuance/nmdp/speechkit/bf;Lcom/nuance/nmdp/speechkit/bf;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/nuance/nmdp/speechkit/by;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/by;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "in NMSPManager() gateway IP ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Port ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_3

    const-string v0, " gatewayIP is null"

    :cond_1
    :goto_0
    if-gtz p2, :cond_2

    const-string v0, " gatewayPort should be greater than 0"

    :cond_2
    if-eqz v0, :cond_4

    sget-object v1, Lcom/nuance/nmdp/speechkit/by;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NMSPManager "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v3, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    const-string v0, " gatewayIP is empty"

    goto :goto_0

    :cond_4
    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/by;->b:Ljava/lang/String;

    iput-short p2, p0, Lcom/nuance/nmdp/speechkit/by;->c:S

    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/by;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/nuance/nmdp/speechkit/by;->f:Lcom/nuance/nmdp/speechkit/bf;

    iput-object p5, p0, Lcom/nuance/nmdp/speechkit/by;->g:Lcom/nuance/nmdp/speechkit/bf;

    new-instance v0, Lcom/nuance/nmsp/client/sdk/oem/e;

    invoke-direct {v0}, Lcom/nuance/nmsp/client/sdk/oem/e;-><init>()V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/by;->e:Lcom/nuance/nmdp/speechkit/bi;

    return-void
.end method

.method public static g()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Lcom/nuance/nmdp/speechkit/bi;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/by;->e:Lcom/nuance/nmdp/speechkit/bi;

    return-object v0
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/bf;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/by;->f:Lcom/nuance/nmdp/speechkit/bf;

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/by;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/nuance/nmdp/speechkit/bf;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/by;->g:Lcom/nuance/nmdp/speechkit/bf;

    return-void
.end method

.method public final c()S
    .locals 1

    iget-short v0, p0, Lcom/nuance/nmdp/speechkit/by;->c:S

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/by;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/nuance/nmdp/speechkit/bf;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/by;->f:Lcom/nuance/nmdp/speechkit/bf;

    return-object v0
.end method

.method public final f()Lcom/nuance/nmdp/speechkit/bf;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/by;->g:Lcom/nuance/nmdp/speechkit/bf;

    return-object v0
.end method
