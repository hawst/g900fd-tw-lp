.class public Lcom/nuance/nmdp/speechkit/bw;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/bi$b;
.implements Lcom/nuance/nmdp/speechkit/bk$a;
.implements Lcom/nuance/nmdp/speechkit/bk$d;
.implements Lcom/nuance/nmdp/speechkit/bk$e;
.implements Lcom/nuance/nmdp/speechkit/bk$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/nmdp/speechkit/bw$a;
    }
.end annotation


# static fields
.field private static c:Lcom/nuance/nmdp/speechkit/bh;


# instance fields
.field protected a:B

.field public b:[B

.field private d:Lcom/nuance/nmdp/speechkit/bl;

.field private e:I

.field private f:Lcom/nuance/nmdp/speechkit/bl;

.field private g:I

.field private h:Lcom/nuance/nmdp/speechkit/bl;

.field private i:Ljava/lang/String;

.field private j:S

.field private k:Ljava/util/Vector;

.field private l:Z

.field private m:Ljava/util/Vector;

.field private n:Lcom/nuance/nmdp/speechkit/bi;

.field private o:Ljava/lang/Object;

.field private p:Lcom/nuance/nmdp/speechkit/bn;

.field private q:S

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:[B

.field private u:Lcom/nuance/nmdp/speechkit/bk;

.field private v:Ljava/lang/String;

.field private w:I

.field private x:S

.field private y:S

.field private z:Lcom/nuance/nmdp/speechkit/bw$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/nuance/nmdp/speechkit/bw;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Class;)Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    sput-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;SLjava/lang/String;[BLjava/lang/String;Lcom/nuance/nmdp/speechkit/bw$a;Ljava/util/Vector;Lcom/nuance/nmdp/speechkit/bi;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1e

    iput v0, p0, Lcom/nuance/nmdp/speechkit/bw;->e:I

    const/16 v0, 0x32

    iput v0, p0, Lcom/nuance/nmdp/speechkit/bw;->g:I

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->i:Ljava/lang/String;

    iput-short v2, p0, Lcom/nuance/nmdp/speechkit/bw;->j:S

    iput-boolean v2, p0, Lcom/nuance/nmdp/speechkit/bw;->l:Z

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->m:Ljava/util/Vector;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iput-short v2, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    const-string v0, "Not specified"

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->r:Ljava/lang/String;

    const-string v0, "Not specified"

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->s:Ljava/lang/String;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->t:[B

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    const-string v0, ""

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->v:Ljava/lang/String;

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/bw;->i:Ljava/lang/String;

    iput-short p2, p0, Lcom/nuance/nmdp/speechkit/bw;->j:S

    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/bw;->r:Ljava/lang/String;

    iput-object p4, p0, Lcom/nuance/nmdp/speechkit/bw;->t:[B

    iput-object p5, p0, Lcom/nuance/nmdp/speechkit/bw;->s:Ljava/lang/String;

    iput-object p6, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    if-eqz p7, :cond_3

    iput-object p7, p0, Lcom/nuance/nmdp/speechkit/bw;->k:Ljava/util/Vector;

    :goto_0
    iput-object p8, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode() server: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " port: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p7}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/nmdp/speechkit/bz;

    sget-object v2, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v2}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "XMode() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->d()Lcom/nuance/nmdp/speechkit/bz$a;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->b()[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_2
    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->d()Lcom/nuance/nmdp/speechkit/bz$a;

    move-result-object v2

    sget-object v3, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    if-ne v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "IdleSessionTimeout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->b()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    iput v0, p0, Lcom/nuance/nmdp/speechkit/bw;->g:I

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->k:Ljava/util/Vector;

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ConnectionTimeout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->b()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/nuance/nmdp/speechkit/bw;->e:I

    goto/16 :goto_1

    :cond_5
    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Socket_Enable"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Cert_Summary"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Cert_Data"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_SelfSigned_Cert"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_6
    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Socket_Enable"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->b()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    const-string v3, "TRUE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->b()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/nuance/nmdp/speechkit/bw;->l:Z

    :cond_8
    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->m:Ljava/util/Vector;

    if-nez v2, :cond_9

    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->m:Ljava/util/Vector;

    :cond_9
    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->m:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_a
    const/4 v0, 0x3

    iput-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    return-void
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/bw;)S
    .locals 1

    const/4 v0, 0x3

    iput-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    return v0
.end method

.method private a(B)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    new-instance v1, Lcom/nuance/nmdp/speechkit/bi$a;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/nuance/nmdp/speechkit/bi$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    invoke-interface {v3}, Lcom/nuance/nmdp/speechkit/bi;->a()[Ljava/lang/Object;

    invoke-interface {v0, v1, p0, v2}, Lcom/nuance/nmdp/speechkit/bi;->a(Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bi$b;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "XMode.sendCmdMsg() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Lcom/nuance/nmdp/speechkit/bn;[B)V
    .locals 7

    const/16 v6, 0x10

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.parseXModeMsg() protocol: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v2, p1, Lcom/nuance/nmdp/speechkit/bn;->a:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cmd: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p1, Lcom/nuance/nmdp/speechkit/bn;->b:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget-byte v0, p1, Lcom/nuance/nmdp/speechkit/bn;->a:B

    sparse-switch v0, :sswitch_data_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.parseXModeMsg() unknown protocol: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v2, p1, Lcom/nuance/nmdp/speechkit/bn;->a:B

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    iget-short v0, p1, Lcom/nuance/nmdp/speechkit/bn;->b:S

    sparse-switch v0, :sswitch_data_1

    :goto_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    invoke-interface {v0, p1, p2}, Lcom/nuance/nmdp/speechkit/bw$a;->a(Lcom/nuance/nmdp/speechkit/bn;[B)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->f:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    new-array v0, v6, [B

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->b:[B

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->b:[B

    invoke-static {p2, v3, v0, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->b:[B

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->b:[B

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->b:[B

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/bh;->b([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->v:Ljava/lang/String;

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->v:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->f()V

    :try_start_0
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received COP_Connected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_2
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->g()V

    new-instance v0, Lcom/nuance/nmdp/speechkit/bw$5;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/bw$5;-><init>(Lcom/nuance/nmdp/speechkit/bw;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget v2, p0, Lcom/nuance/nmdp/speechkit/bw;->g:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;J)V

    goto :goto_1

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/bh;->g()V

    throw v0

    :sswitch_3
    invoke-static {p2, v3}, Lcom/nuance/nmdp/speechkit/bp;->b([BI)I

    move-result v0

    iput v0, p0, Lcom/nuance/nmdp/speechkit/bw;->w:I

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto :goto_1

    :sswitch_4
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    :cond_3
    iput-byte v4, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    const/4 v0, 0x6

    iput-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.parseXModeMsgCopDisconnect() Received COP DISCONNECT. "

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->d(Ljava/lang/Object;)V

    :cond_4
    invoke-direct {p0, v5}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto/16 :goto_1

    :sswitch_5
    const/4 v0, 0x7

    iput-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    iput-byte v4, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.parseXModeMsgCopConnectFailed() COP CONNECT failure. "

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    :cond_5
    invoke-direct {p0, v5}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto/16 :goto_1

    :sswitch_6
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/nuance/nmdp/speechkit/bw$6;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/bw$6;-><init>(Lcom/nuance/nmdp/speechkit/bw;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget v2, p0, Lcom/nuance/nmdp/speechkit/bw;->g:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;J)V

    :cond_6
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    invoke-interface {v0, p1, p2}, Lcom/nuance/nmdp/speechkit/bw$a;->a(Lcom/nuance/nmdp/speechkit/bn;[B)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lcom/nuance/nmdp/speechkit/bw$7;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/bw$7;-><init>(Lcom/nuance/nmdp/speechkit/bw;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget v2, p0, Lcom/nuance/nmdp/speechkit/bw;->g:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;J)V

    :cond_7
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    invoke-interface {v0, p1, p2}, Lcom/nuance/nmdp/speechkit/bw$a;->a(Lcom/nuance/nmdp/speechkit/bn;[B)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_7
        0x2 -> :sswitch_6
        0x3 -> :sswitch_1
        0xf -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x101 -> :sswitch_2
        0x102 -> :sswitch_3
        0x200 -> :sswitch_4
        0x300 -> :sswitch_5
    .end sparse-switch
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/bw;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    return-void
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/bw;)I
    .locals 1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/bw;->g:I

    return v0
.end method

.method static synthetic c()Lcom/nuance/nmdp/speechkit/bh;
    .locals 1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.closeSocketCallback() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x3

    iput-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->d:Lcom/nuance/nmdp/speechkit/bl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->d:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    iput-object v3, p0, Lcom/nuance/nmdp/speechkit/bw;->d:Lcom/nuance/nmdp/speechkit/bl;

    :cond_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    iput-object v3, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    :cond_2
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    iget-short v1, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bw$a;->a(S)V

    :cond_3
    iput-object v3, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    iput-object v3, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iput-object v3, p0, Lcom/nuance/nmdp/speechkit/bw;->b:[B

    const-string v0, ""

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->v:Ljava/lang/String;

    return-void
.end method

.method public final a(I)V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.startStreaming() audio id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-eq v0, v3, :cond_1

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x6

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/nuance/nmdp/speechkit/bp;->a(I[BI)V

    iget-short v1, p0, Lcom/nuance/nmdp/speechkit/bw;->x:S

    const/4 v2, 0x4

    invoke-static {v1, v0, v2}, Lcom/nuance/nmdp/speechkit/bp;->a(S[BI)V

    const/16 v1, 0x12

    const/16 v2, 0x101

    invoke-static {v3, v1, v2, v0}, Lcom/nuance/nmdp/speechkit/bm;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_VAP_RECORD_BEGIN"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/bw;->a([BLjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/bk$c;Ljava/lang/Object;)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.openSocketCallback() "

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Lcom/nuance/nmdp/speechkit/bk$c;->a:Lcom/nuance/nmdp/speechkit/bk$c;

    if-ne p1, v0, :cond_3

    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-nez v0, :cond_2

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x2

    iput-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bk;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/nuance/nmdp/speechkit/bk$c;->b:Lcom/nuance/nmdp/speechkit/bk$c;

    if-ne p1, v0, :cond_5

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.openSocketCallback() NETWORK_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    :cond_4
    const/4 v0, 0x3

    iput-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    iput-short v3, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    iget-short v1, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bw$a;->a(S)V

    iput-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    iput-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iput-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->b:[B

    const-string v0, ""

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->v:Ljava/lang/String;

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/nuance/nmdp/speechkit/bk$c;->c:Lcom/nuance/nmdp/speechkit/bk$c;

    if-ne p1, v0, :cond_1

    iput-short v3, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.openSocketCallback() NETWORK_MEMORY_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/bk$c;Ljava/lang/Object;IILjava/lang/Object;)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x3

    const/4 v1, 0x1

    check-cast p5, Ljava/lang/String;

    sget-object v0, Lcom/nuance/nmdp/speechkit/bk$c;->a:Lcom/nuance/nmdp/speechkit/bk$c;

    if-ne p1, v0, :cond_2

    if-ne p3, p4, :cond_2

    const-string v0, "SEND_COP_CONNECT"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "SEND_COP_DISCONNECT"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    invoke-interface {v0, p2}, Lcom/nuance/nmdp/speechkit/bk;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/nuance/nmdp/speechkit/bk$c;->b:Lcom/nuance/nmdp/speechkit/bk$c;

    if-ne p1, v0, :cond_4

    iget-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    if-eq v0, v1, :cond_3

    iget-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    if-eq v0, v2, :cond_3

    iput-short v3, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.writeSocketCallback() NETWORK_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    :cond_3
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/nuance/nmdp/speechkit/bk$c;->c:Lcom/nuance/nmdp/speechkit/bk$c;

    if-ne p1, v0, :cond_0

    iget-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    if-eq v0, v1, :cond_0

    iget-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    if-eq v0, v2, :cond_0

    iput-short v3, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.writeSocketCallback() NETWORK_MEMORY_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/bk$c;Ljava/lang/Object;[BIILjava/lang/Object;)V
    .locals 8

    const-wide/16 v6, 0x14

    const/16 v5, 0x8

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x5

    check-cast p6, Ljava/lang/String;

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "Read callback"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0, p3}, Lcom/nuance/nmdp/speechkit/bh;->a([B)V

    :cond_1
    sget-object v0, Lcom/nuance/nmdp/speechkit/bk$c;->a:Lcom/nuance/nmdp/speechkit/bk$c;

    if-ne p1, v0, :cond_d

    const-string v0, "READ_XMODE_HEADER"

    invoke-virtual {p6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    if-nez p5, :cond_3

    new-instance v0, Lcom/nuance/nmdp/speechkit/bw$2;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/bw$2;-><init>(Lcom/nuance/nmdp/speechkit/bw;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->d:Lcom/nuance/nmdp/speechkit/bl;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->d:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1, v6, v7}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;J)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    if-ne p5, p4, :cond_7

    new-instance v0, Lcom/nuance/nmdp/speechkit/bn;

    invoke-direct {v0, p3}, Lcom/nuance/nmdp/speechkit/bn;-><init>([B)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iget v0, v0, Lcom/nuance/nmdp/speechkit/bn;->c:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/bw;->a(Lcom/nuance/nmdp/speechkit/bn;[B)V

    invoke-direct {p0, v2}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iget v0, v0, Lcom/nuance/nmdp/speechkit/bn;->c:I

    const v1, 0x7d000

    if-gt v0, v1, :cond_5

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iget v0, v0, Lcom/nuance/nmdp/speechkit/bn;->c:I

    if-gez v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    invoke-interface {v0, p2}, Lcom/nuance/nmdp/speechkit/bk;->b(Ljava/lang/Object;)V

    invoke-direct {p0, v2}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto :goto_0

    :cond_6
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto :goto_0

    :cond_7
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "----***---- readSocketCallback fatal error in readSocketCallback NET_CONTEXT_READ_XMODE_HEADER bytesRead:["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] bufferLen:["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    goto :goto_0

    :cond_8
    const-string v0, "READ_XMODE_PAYLOAD"

    invoke-virtual {p6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p5, :cond_a

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iget-short v2, v2, Lcom/nuance/nmdp/speechkit/bn;->b:S

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " payload not read bytesRead is 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_9
    new-instance v0, Lcom/nuance/nmdp/speechkit/bw$3;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/bw$3;-><init>(Lcom/nuance/nmdp/speechkit/bw;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->d:Lcom/nuance/nmdp/speechkit/bl;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->d:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1, v6, v7}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;J)V

    goto/16 :goto_0

    :cond_a
    if-ne p5, p4, :cond_c

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iget v0, v0, Lcom/nuance/nmdp/speechkit/bn;->c:I

    if-gt v0, p4, :cond_b

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    invoke-direct {p0, v0, p3}, Lcom/nuance/nmdp/speechkit/bw;->a(Lcom/nuance/nmdp/speechkit/bn;[B)V

    :cond_b
    invoke-direct {p0, v2}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto/16 :goto_0

    :cond_c
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "----***---- readSocketCallback fatal error in readSocketCallback NET_CONTEXT_READ_XMODE_PAYLOAD bytesRead:["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] bufferLen:["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_d
    sget-object v0, Lcom/nuance/nmdp/speechkit/bk$c;->b:Lcom/nuance/nmdp/speechkit/bk$c;

    if-ne p1, v0, :cond_f

    iget-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    if-eq v0, v3, :cond_e

    iget-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    if-eq v0, v4, :cond_e

    iput-short v5, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.readSocketCallback() NETWORK_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    :cond_e
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto/16 :goto_0

    :cond_f
    sget-object v0, Lcom/nuance/nmdp/speechkit/bk$c;->c:Lcom/nuance/nmdp/speechkit/bk$c;

    if-ne p1, v0, :cond_2

    iget-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    if-eq v0, v3, :cond_2

    iget-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    if-eq v0, v4, :cond_2

    iput-short v5, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.readSocketCallback() NETWORK_MEMORY_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 12

    const/4 v4, 0x1

    const/16 v10, 0x17

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v3, 0x0

    check-cast p1, Lcom/nuance/nmdp/speechkit/bi$a;

    iget-byte v0, p1, Lcom/nuance/nmdp/speechkit/bi$a;->a:B

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.handleMessage() CMD_CONNECT"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_1
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-nez v0, :cond_2

    invoke-direct {p0, v8}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    iget-short v1, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bw$a;->a(S)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.handleMessage() CMD_OPEN_SOCKET"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_3
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-nez v0, :cond_5

    new-instance v0, Lcom/nuance/nmsp/client/sdk/oem/g;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    invoke-direct {v0, v1}, Lcom/nuance/nmsp/client/sdk/oem/g;-><init>(Lcom/nuance/nmdp/speechkit/bi;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/bw;->l:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->i:Ljava/lang/String;

    iget-short v2, p0, Lcom/nuance/nmdp/speechkit/bw;->j:S

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/bw;->m:Ljava/util/Vector;

    move-object v4, p0

    move-object v5, p0

    invoke-interface/range {v0 .. v5}, Lcom/nuance/nmdp/speechkit/bk;->a(Ljava/lang/String;ILjava/util/Vector;Lcom/nuance/nmdp/speechkit/bk$d;Lcom/nuance/nmdp/speechkit/bk$a;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->i:Ljava/lang/String;

    iget-short v2, p0, Lcom/nuance/nmdp/speechkit/bw;->j:S

    invoke-interface {v0, v1, v2, p0, p0}, Lcom/nuance/nmdp/speechkit/bk;->a(Ljava/lang/String;ILcom/nuance/nmdp/speechkit/bk$d;Lcom/nuance/nmdp/speechkit/bk$a;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    iget-short v1, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bw$a;->a(S)V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.handleMessage() CMD_COP_CONNECT"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_6
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<?xml version=\"1.0\"?><cc><s></s><t>7</t><b>20091023</b><tsc>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v1, p0, Lcom/nuance/nmdp/speechkit/bw;->x:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</tsc><fsc>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/nuance/nmdp/speechkit/bw;->y:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</fsc><nmaid>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</nmaid><uid>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</uid>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->k:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v5

    move-object v1, v0

    move v2, v3

    :goto_1
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/nmdp/speechkit/bz;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->d()Lcom/nuance/nmdp/speechkit/bz$a;

    move-result-object v6

    sget-object v7, Lcom/nuance/nmdp/speechkit/bz$a;->b:Lcom/nuance/nmdp/speechkit/bz$a;

    if-ne v6, v7, :cond_10

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->b()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "<nmsp p=\""

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "\" v=\""

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v6}, Lcom/nuance/nmdp/speechkit/bs;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "\"/>"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Ping_IntervalSecs"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    move-object v2, v1

    move v1, v4

    :goto_2
    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->d()Lcom/nuance/nmdp/speechkit/bz$a;

    move-result-object v6

    sget-object v7, Lcom/nuance/nmdp/speechkit/bz$a;->c:Lcom/nuance/nmdp/speechkit/bz$a;

    if-ne v6, v7, :cond_f

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->b()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "<app p=\""

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\" v=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lcom/nuance/nmdp/speechkit/bs;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"/>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    move v2, v1

    move-object v1, v0

    goto/16 :goto_1

    :cond_7
    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<nmsp p=\"Ping_IntervalSecs\" v=\"0\"/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</cc>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v1, v0

    add-int/lit8 v2, v1, 0x4

    add-int/lit8 v2, v2, 0x1

    new-array v5, v2, [B

    add-int/lit8 v2, v2, -0x4

    invoke-static {v2, v5, v3}, Lcom/nuance/nmdp/speechkit/bp;->a(I[BI)V

    aput-byte v3, v5, v9

    const/4 v2, 0x5

    invoke-static {v0, v3, v5, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v0, 0x100

    invoke-static {v8, v10, v0, v5}, Lcom/nuance/nmdp/speechkit/bm;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_COP_CONNECT"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/bw;->a([BLjava/lang/Object;)V

    new-instance v0, Lcom/nuance/nmdp/speechkit/bw$4;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/bw$4;-><init>(Lcom/nuance/nmdp/speechkit/bw;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->f:Lcom/nuance/nmdp/speechkit/bl;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->f:Lcom/nuance/nmdp/speechkit/bl;

    iget v2, p0, Lcom/nuance/nmdp/speechkit/bw;->e:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;J)V

    iput-byte v4, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    new-array v3, v0, [B

    const-string v6, "READ_XMODE_HEADER"

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    sget-object v2, Lcom/nuance/nmdp/speechkit/bk$b;->a:Lcom/nuance/nmdp/speechkit/bk$b;

    const/16 v4, 0x8

    move-object v5, p0

    invoke-interface/range {v0 .. v6}, Lcom/nuance/nmdp/speechkit/bk;->a(Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bk$b;[BILcom/nuance/nmdp/speechkit/bk$e;Ljava/lang/Object;)Lcom/nuance/nmdp/speechkit/bk$c;

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iget v0, v0, Lcom/nuance/nmdp/speechkit/bn;->c:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iget v0, v0, Lcom/nuance/nmdp/speechkit/bn;->c:I

    const v1, 0x7d000

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->p:Lcom/nuance/nmdp/speechkit/bn;

    iget v0, v0, Lcom/nuance/nmdp/speechkit/bn;->c:I

    new-array v3, v0, [B

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const-string v6, "READ_XMODE_PAYLOAD"

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    sget-object v2, Lcom/nuance/nmdp/speechkit/bk$b;->a:Lcom/nuance/nmdp/speechkit/bk$b;

    array-length v4, v3

    move-object v5, p0

    invoke-interface/range {v0 .. v6}, Lcom/nuance/nmdp/speechkit/bk;->a(Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bk$b;[BILcom/nuance/nmdp/speechkit/bk$e;Ljava/lang/Object;)Lcom/nuance/nmdp/speechkit/bk$c;

    goto/16 :goto_0

    :pswitch_5
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.handleMessage() CMD_DISCONNECT"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_9
    const/4 v0, 0x2

    new-array v0, v0, [B

    invoke-static {v3, v0, v3}, Lcom/nuance/nmdp/speechkit/bp;->a(S[BI)V

    const/16 v1, 0x200

    invoke-static {v8, v10, v1, v0}, Lcom/nuance/nmdp/speechkit/bm;->a(BBS[B)[B

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/nuance/nmdp/speechkit/bk;->b(Ljava/lang/Object;)V

    const-string v1, "SEND_COP_DISCONNECT"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/bw;->a([BLjava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_6
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.handleMessage() CMD_CLOSE_SOCKET"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_a
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bk;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_7
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.handleMessage() CMD_COP_PING_RESPONSE"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_b
    const/16 v0, 0x8

    new-array v0, v0, [B

    iget v1, p0, Lcom/nuance/nmdp/speechkit/bw;->w:I

    invoke-static {v1, v0, v3}, Lcom/nuance/nmdp/speechkit/bp;->a(I[BI)V

    invoke-static {v3, v0, v9}, Lcom/nuance/nmdp/speechkit/bp;->a(I[BI)V

    const/16 v1, 0x103

    invoke-static {v8, v10, v1, v0}, Lcom/nuance/nmdp/speechkit/bm;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_COP_PING_RESPONSE"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/bw;->a([BLjava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_8
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.handleMessage() CMD_COP_CONNECT_TIMED_OUT"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_c
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-ne v0, v4, :cond_0

    const/4 v0, 0x2

    iput-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    const/4 v0, 0x5

    iput-short v0, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.handleCopConnectTimeout() COP CONNECT timed out. "

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    :cond_d
    invoke-direct {p0, v9}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto/16 :goto_0

    :pswitch_9
    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "XMode.handleMessage() CMD_COP_CONFIRM"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_e
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->t:[B

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->b:[B

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/bj;->b([B[B)[B

    move-result-object v0

    array-length v1, v0

    add-int/lit8 v1, v1, 0x4

    new-array v1, v1, [B

    array-length v2, v0

    invoke-static {v2, v1, v3}, Lcom/nuance/nmdp/speechkit/bp;->a(I[BI)V

    array-length v2, v0

    invoke-static {v0, v3, v1, v9, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v0, 0x106

    invoke-static {v8, v10, v0, v1}, Lcom/nuance/nmdp/speechkit/bm;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_COP_CONFIRM"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/bw;->a([BLjava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p1, Lcom/nuance/nmdp/speechkit/bi$a;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p1, Lcom/nuance/nmdp/speechkit/bi$a;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    goto/16 :goto_0

    :cond_f
    move-object v0, v2

    goto/16 :goto_3

    :cond_10
    move-object v11, v1

    move v1, v2

    move-object v2, v11

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final a(SS)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.connect() codec: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-eq v0, v3, :cond_3

    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-eqz v0, :cond_3

    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->d:Lcom/nuance/nmdp/speechkit/bl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->d:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    :cond_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    :cond_2
    iput-object v4, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    iput-object v4, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iput-object v4, p0, Lcom/nuance/nmdp/speechkit/bw;->b:[B

    const-string v0, ""

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->v:Ljava/lang/String;

    iput-short p1, p0, Lcom/nuance/nmdp/speechkit/bw;->x:S

    iput-short p2, p0, Lcom/nuance/nmdp/speechkit/bw;->y:S

    iput-byte v5, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    invoke-direct {p0, v3}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iput-short p1, p0, Lcom/nuance/nmdp/speechkit/bw;->x:S

    iput-short p2, p0, Lcom/nuance/nmdp/speechkit/bw;->y:S

    iput-byte v5, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    invoke-direct {p0, v3}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto :goto_0
.end method

.method public final a([BI)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.sendVapRecordMsg() audio id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-eq v0, v4, :cond_1

    :goto_0
    return-void

    :cond_1
    array-length v0, p1

    add-int/lit8 v1, v0, 0x8

    new-array v1, v1, [B

    invoke-static {p2, v1, v3}, Lcom/nuance/nmdp/speechkit/bp;->a(I[BI)V

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/nuance/nmdp/speechkit/bp;->a(I[BI)V

    const/16 v2, 0x8

    invoke-static {p1, v3, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v0, 0x12

    const/16 v2, 0x201

    invoke-static {v4, v0, v2, v1}, Lcom/nuance/nmdp/speechkit/bm;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_VAP_RECORD"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/bw;->a([BLjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a([BLjava/lang/Object;)V
    .locals 6

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.sendXModeMsg() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/nuance/nmdp/speechkit/bw$1;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/bw$1;-><init>(Lcom/nuance/nmdp/speechkit/bw;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->n:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget v2, p0, Lcom/nuance/nmdp/speechkit/bw;->g:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;J)V

    :cond_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    array-length v3, p1

    move-object v2, p1

    move-object v4, p0

    move-object v5, p2

    invoke-interface/range {v0 .. v5}, Lcom/nuance/nmdp/speechkit/bk;->a(Ljava/lang/Object;[BILcom/nuance/nmdp/speechkit/bk$f;Ljava/lang/Object;)Lcom/nuance/nmdp/speechkit/bk$c;

    :cond_2
    return-void
.end method

.method public final b()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.disconnect() state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v2, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", socket:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iput-short v3, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->z:Lcom/nuance/nmdp/speechkit/bw$a;

    iget-short v1, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bw$a;->a(S)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-eq v0, v4, :cond_1

    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-nez v0, :cond_3

    iput-short v3, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    iput-byte v4, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/bw;->u:Lcom/nuance/nmdp/speechkit/bk;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/bw;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bk;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-ne v0, v3, :cond_1

    iput-byte v4, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    iput-short v3, p0, Lcom/nuance/nmdp/speechkit/bw;->q:S

    invoke-direct {p0, v4}, Lcom/nuance/nmdp/speechkit/bw;->a(B)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bw;->c:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.sendVapRecordEnd() audio id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget-byte v0, p0, Lcom/nuance/nmdp/speechkit/bw;->a:B

    if-eq v0, v3, :cond_1

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/nuance/nmdp/speechkit/bp;->a(I[BI)V

    const/16 v1, 0x12

    const/16 v2, 0x100

    invoke-static {v3, v1, v2, v0}, Lcom/nuance/nmdp/speechkit/bm;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_VAP_RECORD_END"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/bw;->a([BLjava/lang/Object;)V

    goto :goto_0
.end method
