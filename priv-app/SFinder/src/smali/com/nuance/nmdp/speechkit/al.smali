.class final Lcom/nuance/nmdp/speechkit/al;
.super Lcom/nuance/nmdp/speechkit/z;


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/ah;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/nuance/nmdp/speechkit/z;-><init>(Lcom/nuance/nmdp/speechkit/ah;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/al;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/ah;->g()V
    :try_end_0
    .catch Lcom/nuance/nmdp/speechkit/t; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error ending command"

    invoke-static {p0, v1, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/al;->a(I)V

    goto :goto_0
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/dn;)V
    .locals 10

    const/4 v1, 0x0

    :try_start_0
    const-string v0, "transcriptions"

    invoke-interface {p1, v0}, Lcom/nuance/nmdp/speechkit/dn;->g(Ljava/lang/String;)Lcom/nuance/nmdp/speechkit/dt;

    move-result-object v4

    invoke-interface {v4}, Lcom/nuance/nmdp/speechkit/dt;->a()I

    move-result v5

    if-lez v5, :cond_0

    new-array v6, v5, [Ljava/lang/String;

    new-array v7, v5, [I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    const-string v0, "prompt"

    invoke-interface {p1, v0}, Lcom/nuance/nmdp/speechkit/dn;->e(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    :goto_0
    :try_start_2
    const-string v2, "confidences"

    invoke-interface {p1, v2}, Lcom/nuance/nmdp/speechkit/dn;->g(Ljava/lang/String;)Lcom/nuance/nmdp/speechkit/dt;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    :try_start_3
    invoke-interface {v2}, Lcom/nuance/nmdp/speechkit/dt;->a()I

    move-result v3

    if-eq v5, v3, :cond_3

    const-string v3, "Size mismatch between transcriptions and confidences"

    invoke-static {p0, v3}, Lcom/nuance/nmdp/speechkit/j;->b(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    :goto_1
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_2

    :try_start_4
    invoke-interface {v4, v3}, Lcom/nuance/nmdp/speechkit/dt;->c(I)Ljava/lang/String;

    move-result-object v2

    const/16 v8, 0xd

    const/16 v9, 0x20

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v3

    if-eqz v1, :cond_1

    invoke-interface {v1, v3}, Lcom/nuance/nmdp/speechkit/dt;->b(I)I

    move-result v2

    :goto_3
    aput v2, v7, v3

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v2

    :goto_4
    const-string v2, "Unable to extract confidences from result"

    invoke-static {p0, v2}, Lcom/nuance/nmdp/speechkit/j;->b(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "Unable to extract transcriptions from result"

    invoke-static {p0, v1, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/al;->a(I)V

    :goto_5
    return-void

    :cond_1
    const/16 v2, 0x64

    goto :goto_3

    :cond_2
    :try_start_5
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/al;->b:Lcom/nuance/nmdp/speechkit/ah;

    new-instance v2, Lcom/nuance/nmdp/speechkit/ab;

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/al;->b:Lcom/nuance/nmdp/speechkit/ah;

    invoke-direct {v2, v3, v6, v7, v0}, Lcom/nuance/nmdp/speechkit/ab;-><init>(Lcom/nuance/nmdp/speechkit/ah;[Ljava/lang/String;[ILjava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/nuance/nmdp/speechkit/ah;->a(Lcom/nuance/nmdp/speechkit/q;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_5

    :catch_3
    move-exception v1

    move-object v1, v2

    goto :goto_4

    :cond_3
    move-object v1, v2

    goto :goto_1
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/al;->a(I)V

    return-void
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/al;->a(I)V

    return-void
.end method
