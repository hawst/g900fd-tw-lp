.class public abstract Lcom/nuance/nmdp/speechkit/r;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/SpeechKit$a;


# instance fields
.field protected a:Lcom/nuance/nmdp/speechkit/q;

.field protected b:Ljava/lang/String;

.field protected final c:Lcom/nuance/nmdp/speechkit/cf;

.field protected final d:Lcom/nuance/nmdp/speechkit/s;

.field private e:Lcom/nuance/nmdp/speechkit/p;

.field private f:Lcom/nuance/nmdp/speechkit/do;

.field private g:Lcom/nuance/nmdp/speechkit/dm;

.field private final h:Lcom/nuance/nmdp/speechkit/ck;

.field private final i:Lcom/nuance/nmdp/speechkit/dq;


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/s;Lcom/nuance/nmdp/speechkit/p;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/r;->e:Lcom/nuance/nmdp/speechkit/p;

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/r;->c:Lcom/nuance/nmdp/speechkit/cf;

    iput-object p2, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {p2}, Lcom/nuance/nmdp/speechkit/s;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->b:Ljava/lang/String;

    new-instance v0, Lcom/nuance/nmdp/speechkit/r$1;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/r$1;-><init>(Lcom/nuance/nmdp/speechkit/r;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->h:Lcom/nuance/nmdp/speechkit/ck;

    new-instance v0, Lcom/nuance/nmdp/speechkit/r$2;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/r$2;-><init>(Lcom/nuance/nmdp/speechkit/r;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->i:Lcom/nuance/nmdp/speechkit/dq;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/r;->g:Lcom/nuance/nmdp/speechkit/dm;

    return-void
.end method

.method private a(Lcom/nuance/nmdp/speechkit/bg$j;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/nmdp/speechkit/t;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->g:Lcom/nuance/nmdp/speechkit/dm;

    invoke-interface {v0, p1}, Lcom/nuance/nmdp/speechkit/dm;->a(Lcom/nuance/nmdp/speechkit/bg$j;)V
    :try_end_0
    .catch Lcom/nuance/nmdp/speechkit/ca; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/nuance/nmdp/speechkit/cb; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/nuance/nmdp/speechkit/t;

    const-string v2, "Error sending parameter (TransactionAlreadyFinishedException)"

    invoke-direct {v1, v2, v0}, Lcom/nuance/nmdp/speechkit/t;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/nuance/nmdp/speechkit/t;

    const-string v2, "Error sending parameter (TransactionExpiredException)"

    invoke-direct {v1, v2, v0}, Lcom/nuance/nmdp/speechkit/t;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->a:Lcom/nuance/nmdp/speechkit/q;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/q;->c()V

    return-void
.end method

.method protected a(Lcom/nuance/nmdp/speechkit/dn;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/q;)V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->a:Lcom/nuance/nmdp/speechkit/q;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/q;->b()V

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/r;->a:Lcom/nuance/nmdp/speechkit/q;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->a:Lcom/nuance/nmdp/speechkit/q;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/q;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/nmdp/speechkit/cj;
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/nuance/nmdp/speechkit/r;->h()Lcom/nuance/nmdp/speechkit/dn;

    move-result-object v7

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->g()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->i()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/s;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v2}, Lcom/nuance/nmdp/speechkit/s;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-static {}, Lcom/nuance/nmdp/speechkit/s;->o()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-static {}, Lcom/nuance/nmdp/speechkit/s;->l()Ljava/lang/String;

    move-result-object v4

    iget-object v8, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v8}, Lcom/nuance/nmdp/speechkit/s;->b()Ljava/lang/String;

    move-result-object v8

    const-string v9, "ui_language"

    iget-object v10, p0, Lcom/nuance/nmdp/speechkit/r;->b:Ljava/lang/String;

    invoke-interface {v7, v9, v10}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "phone_submodel"

    invoke-interface {v7, v9, v6}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "phone_OS"

    invoke-interface {v7, v9, v0}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-interface {v7, v0, v1}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "nmdp_version"

    invoke-interface {v7, v0, v4}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "nmaid"

    invoke-interface {v7, v0, v8}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "network_type"

    invoke-interface {v7, v0, v2}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x64

    new-array v0, v0, [B

    :try_start_0
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v1, v0}, Lcom/nuance/nmdp/speechkit/s;->a([B)[B
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    const-string v1, "app_transaction_id"

    invoke-interface {v7, v1, v0}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;[B)V

    invoke-virtual {p0, v7}, Lcom/nuance/nmdp/speechkit/r;->a(Lcom/nuance/nmdp/speechkit/dn;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/r;->i:Lcom/nuance/nmdp/speechkit/dq;

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/r;->b:Ljava/lang/String;

    move-object v2, p1

    invoke-interface/range {v0 .. v7}, Lcom/nuance/nmdp/speechkit/do;->a(Lcom/nuance/nmdp/speechkit/dq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/nmdp/speechkit/dn;)Lcom/nuance/nmdp/speechkit/dm;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->g:Lcom/nuance/nmdp/speechkit/dm;

    :cond_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/dn;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/nmdp/speechkit/t;
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    invoke-interface {v0, p1, p2}, Lcom/nuance/nmdp/speechkit/do;->a(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/dn;)Lcom/nuance/nmdp/speechkit/bg$j;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/r;->a(Lcom/nuance/nmdp/speechkit/bg$j;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/nuance/nmdp/speechkit/bx;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/nmdp/speechkit/t;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/nuance/nmdp/speechkit/r;->h()Lcom/nuance/nmdp/speechkit/dn;

    move-result-object v0

    const-string v1, "tts_input"

    invoke-interface {v0, v1, p2}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "tts_type"

    invoke-interface {v0, v1, p1}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    const-string v2, "TEXT_TO_READ"

    invoke-interface {v1, v2, v0, p3}, Lcom/nuance/nmdp/speechkit/do;->a(Ljava/lang/String;Lcom/nuance/nmdp/speechkit/dn;Lcom/nuance/nmdp/speechkit/bx;)Lcom/nuance/nmdp/speechkit/bg$j;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/r;->a(Lcom/nuance/nmdp/speechkit/bg$j;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/nuance/nmdp/speechkit/bx;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/nmdp/speechkit/t;
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    invoke-interface {v0, p1}, Lcom/nuance/nmdp/speechkit/do;->a(Ljava/lang/String;)Lcom/nuance/nmdp/speechkit/bx;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/r;->a(Lcom/nuance/nmdp/speechkit/bg$j;)V

    return-object v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->a:Lcom/nuance/nmdp/speechkit/q;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/q;->d()V

    return-void
.end method

.method public final d()Lcom/nuance/nmdp/speechkit/p;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->e:Lcom/nuance/nmdp/speechkit/p;

    return-object v0
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->c:Lcom/nuance/nmdp/speechkit/cf;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/r;->h:Lcom/nuance/nmdp/speechkit/ck;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/r;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v2}, Lcom/nuance/nmdp/speechkit/s;->b()Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/nuance/nmdp/speechkit/dp;->a(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/ck;)Lcom/nuance/nmdp/speechkit/do;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/do;->a()V
    :try_end_0
    .catch Lcom/nuance/nmdp/speechkit/cj; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final g()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/nmdp/speechkit/t;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->g:Lcom/nuance/nmdp/speechkit/dm;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/dm;->a()V
    :try_end_0
    .catch Lcom/nuance/nmdp/speechkit/ca; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/nuance/nmdp/speechkit/cb; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/nuance/nmdp/speechkit/t;

    const-string v2, "Error ending PDX command (TransactionAlreadyFinishedException)"

    invoke-direct {v1, v2, v0}, Lcom/nuance/nmdp/speechkit/t;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/nuance/nmdp/speechkit/t;

    const-string v2, "Error ending PDX command (TransactionExpiredException)"

    invoke-direct {v1, v2, v0}, Lcom/nuance/nmdp/speechkit/t;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final h()Lcom/nuance/nmdp/speechkit/dn;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/r;->f:Lcom/nuance/nmdp/speechkit/do;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/do;->g()Lcom/nuance/nmdp/speechkit/dn;

    move-result-object v0

    return-object v0
.end method
