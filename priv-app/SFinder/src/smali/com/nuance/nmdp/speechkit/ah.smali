.class public final Lcom/nuance/nmdp/speechkit/ah;
.super Lcom/nuance/nmdp/speechkit/r;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/SpeechKit$a;


# instance fields
.field private final e:Lcom/nuance/nmdp/speechkit/ae;

.field private final f:Lcom/nuance/nmdp/speechkit/az;

.field private g:Lcom/nuance/nmdp/speechkit/bc;

.field private final h:Lcom/nuance/nmdp/speechkit/bb;

.field private final i:Lcom/nuance/nmdp/speechkit/bb;

.field private final j:Lcom/nuance/nmdp/speechkit/bb;

.field private final k:Lcom/nuance/nmdp/speechkit/bb;

.field private final l:Ljava/lang/String;

.field private final m:Z

.field private final n:Z

.field private final o:Lcom/nuance/nmdp/speechkit/ax;


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/s;Ljava/lang/String;ZZLjava/lang/String;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ay;Lcom/nuance/nmdp/speechkit/ae;Lcom/nuance/nmdp/speechkit/ac;)V
    .locals 3

    invoke-direct {p0, p1, p2, p12}, Lcom/nuance/nmdp/speechkit/r;-><init>(Lcom/nuance/nmdp/speechkit/cf;Lcom/nuance/nmdp/speechkit/s;Lcom/nuance/nmdp/speechkit/p;)V

    iput-object p6, p0, Lcom/nuance/nmdp/speechkit/ah;->b:Ljava/lang/String;

    iput-object p11, p0, Lcom/nuance/nmdp/speechkit/ah;->e:Lcom/nuance/nmdp/speechkit/ae;

    new-instance v0, Lcom/nuance/nmdp/speechkit/ah$1;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/ah$1;-><init>(Lcom/nuance/nmdp/speechkit/ah;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->f:Lcom/nuance/nmdp/speechkit/az;

    new-instance v0, Lcom/nuance/nmdp/speechkit/ah$2;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/ah$2;-><init>(Lcom/nuance/nmdp/speechkit/ah;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->o:Lcom/nuance/nmdp/speechkit/ax;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->g:Lcom/nuance/nmdp/speechkit/bc;

    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/ah;->l:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/nuance/nmdp/speechkit/ah;->m:Z

    iput-boolean p5, p0, Lcom/nuance/nmdp/speechkit/ah;->n:Z

    if-nez p7, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->h:Lcom/nuance/nmdp/speechkit/bb;

    if-nez p8, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->i:Lcom/nuance/nmdp/speechkit/bb;

    if-nez p9, :cond_2

    const/4 v0, 0x0

    :goto_2
    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->j:Lcom/nuance/nmdp/speechkit/bb;

    if-nez p10, :cond_3

    const/4 v0, 0x0

    :goto_3
    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->k:Lcom/nuance/nmdp/speechkit/bb;

    new-instance v0, Lcom/nuance/nmdp/speechkit/af;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/af;-><init>(Lcom/nuance/nmdp/speechkit/ah;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->a:Lcom/nuance/nmdp/speechkit/q;

    return-void

    :cond_0
    new-instance v0, Lcom/nuance/nmdp/speechkit/bb;

    invoke-virtual {p2}, Lcom/nuance/nmdp/speechkit/s;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/ah;->o:Lcom/nuance/nmdp/speechkit/ax;

    invoke-direct {v0, p7, v1, p0, v2}, Lcom/nuance/nmdp/speechkit/bb;-><init>(Lcom/nuance/nmdp/speechkit/ay;Ljava/lang/Object;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/ax;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/nuance/nmdp/speechkit/bb;

    invoke-virtual {p2}, Lcom/nuance/nmdp/speechkit/s;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/ah;->o:Lcom/nuance/nmdp/speechkit/ax;

    invoke-direct {v0, p8, v1, p0, v2}, Lcom/nuance/nmdp/speechkit/bb;-><init>(Lcom/nuance/nmdp/speechkit/ay;Ljava/lang/Object;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/ax;)V

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/nuance/nmdp/speechkit/bb;

    invoke-virtual {p2}, Lcom/nuance/nmdp/speechkit/s;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/ah;->o:Lcom/nuance/nmdp/speechkit/ax;

    invoke-direct {v0, p9, v1, p0, v2}, Lcom/nuance/nmdp/speechkit/bb;-><init>(Lcom/nuance/nmdp/speechkit/ay;Ljava/lang/Object;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/ax;)V

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/nuance/nmdp/speechkit/bb;

    invoke-virtual {p2}, Lcom/nuance/nmdp/speechkit/s;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/ah;->o:Lcom/nuance/nmdp/speechkit/ax;

    invoke-direct {v0, p10, v1, p0, v2}, Lcom/nuance/nmdp/speechkit/bb;-><init>(Lcom/nuance/nmdp/speechkit/ay;Ljava/lang/Object;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/ax;)V

    goto :goto_3
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/ah;)Lcom/nuance/nmdp/speechkit/ae;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->e:Lcom/nuance/nmdp/speechkit/ae;

    return-object v0
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/ah;)Lcom/nuance/nmdp/speechkit/ad;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->a:Lcom/nuance/nmdp/speechkit/q;

    check-cast v0, Lcom/nuance/nmdp/speechkit/ad;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/nuance/nmdp/speechkit/dn;)V
    .locals 2

    const-string v0, "dictation_language"

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ah;->b:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "dictation_type"

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ah;->l:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/nuance/nmdp/speechkit/dn;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->a:Lcom/nuance/nmdp/speechkit/q;

    check-cast v0, Lcom/nuance/nmdp/speechkit/ad;

    invoke-interface {v0}, Lcom/nuance/nmdp/speechkit/ad;->j()V

    return-void
.end method

.method final i()Z
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->h:Lcom/nuance/nmdp/speechkit/bb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->h:Lcom/nuance/nmdp/speechkit/bb;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bb;->a()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final j()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->h:Lcom/nuance/nmdp/speechkit/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->h:Lcom/nuance/nmdp/speechkit/bb;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bb;->b()V

    :cond_0
    return-void
.end method

.method final k()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->i:Lcom/nuance/nmdp/speechkit/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->i:Lcom/nuance/nmdp/speechkit/bb;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bb;->a()V

    :cond_0
    return-void
.end method

.method final l()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->j:Lcom/nuance/nmdp/speechkit/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->j:Lcom/nuance/nmdp/speechkit/bb;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bb;->a()V

    :cond_0
    return-void
.end method

.method final m()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->k:Lcom/nuance/nmdp/speechkit/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->k:Lcom/nuance/nmdp/speechkit/bb;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bb;->a()V

    :cond_0
    return-void
.end method

.method public final n()V
    .locals 7

    new-instance v0, Lcom/nuance/nmdp/speechkit/bc;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ah;->c:Lcom/nuance/nmdp/speechkit/cf;

    iget-boolean v2, p0, Lcom/nuance/nmdp/speechkit/ah;->m:Z

    iget-boolean v3, p0, Lcom/nuance/nmdp/speechkit/ah;->n:Z

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/ah;->d:Lcom/nuance/nmdp/speechkit/s;

    invoke-virtual {v4}, Lcom/nuance/nmdp/speechkit/s;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lcom/nuance/nmdp/speechkit/ah;->f:Lcom/nuance/nmdp/speechkit/az;

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/nuance/nmdp/speechkit/bc;-><init>(Lcom/nuance/nmdp/speechkit/cf;ZZLjava/lang/Object;Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/az;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->g:Lcom/nuance/nmdp/speechkit/bc;

    return-void
.end method

.method public final o()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->g:Lcom/nuance/nmdp/speechkit/bc;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bc;->a()V

    return-void
.end method

.method public final p()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/nmdp/speechkit/t;
        }
    .end annotation

    const-string v0, "AUDIO_INFO"

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/ah;->b(Ljava/lang/String;)Lcom/nuance/nmdp/speechkit/bx;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ah;->g:Lcom/nuance/nmdp/speechkit/bc;

    invoke-virtual {v1, v0}, Lcom/nuance/nmdp/speechkit/bc;->a(Lcom/nuance/nmdp/speechkit/bx;)V

    return-void
.end method

.method public final q()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->g:Lcom/nuance/nmdp/speechkit/bc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->g:Lcom/nuance/nmdp/speechkit/bc;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bc;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ah;->g:Lcom/nuance/nmdp/speechkit/bc;

    :cond_0
    return-void
.end method
