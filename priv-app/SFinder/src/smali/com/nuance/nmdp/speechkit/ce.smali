.class public Lcom/nuance/nmdp/speechkit/ce;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/nmdp/speechkit/bg$a;
.implements Lcom/nuance/nmdp/speechkit/bg$d;
.implements Lcom/nuance/nmdp/speechkit/bg$e;
.implements Lcom/nuance/nmdp/speechkit/bg$f;
.implements Lcom/nuance/nmdp/speechkit/bg$g;
.implements Lcom/nuance/nmdp/speechkit/bg$j;
.implements Lcom/nuance/nmdp/speechkit/bg$l;
.implements Lcom/nuance/nmdp/speechkit/bg$m;
.implements Lcom/nuance/nmdp/speechkit/bi$b;
.implements Lcom/nuance/nmdp/speechkit/bv;


# static fields
.field private static final a:Lcom/nuance/nmdp/speechkit/bh;


# instance fields
.field private b:Lcom/nuance/nmdp/speechkit/bf;

.field private c:Lcom/nuance/nmdp/speechkit/bg;

.field private d:Lcom/nuance/nmdp/speechkit/bu;

.field private e:Lcom/nuance/nmdp/speechkit/bi;

.field private f:Lcom/nuance/nmdp/speechkit/bx;

.field private g:I

.field private h:Lcom/nuance/nmdp/speechkit/bl;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lcom/nuance/nmdp/speechkit/bg$h;

.field private m:Ljava/util/Vector;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/nuance/nmdp/speechkit/ce;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Class;)Lcom/nuance/nmdp/speechkit/bh;

    move-result-object v0

    sput-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    return-void
.end method

.method public constructor <init>(Lcom/nuance/nmdp/speechkit/bu;Lcom/nuance/nmdp/speechkit/cf;Ljava/util/Vector;Lcom/nuance/nmdp/speechkit/bd;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    iput-boolean v3, p0, Lcom/nuance/nmdp/speechkit/ce;->i:Z

    iput-boolean v3, p0, Lcom/nuance/nmdp/speechkit/ce;->j:Z

    iput-boolean v3, p0, Lcom/nuance/nmdp/speechkit/ce;->k:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-static {p3}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/util/Vector;)V

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/ce;->d:Lcom/nuance/nmdp/speechkit/bu;

    move-object v0, p2

    check-cast v0, Lcom/nuance/nmdp/speechkit/cl;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/cl;->e()Lcom/nuance/nmdp/speechkit/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->b:Lcom/nuance/nmdp/speechkit/bf;

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/ce;->h:Lcom/nuance/nmdp/speechkit/bl;

    check-cast p2, Lcom/nuance/nmdp/speechkit/cl;

    invoke-virtual {p2}, Lcom/nuance/nmdp/speechkit/cl;->a()Lcom/nuance/nmdp/speechkit/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/ce;->m:Ljava/util/Vector;

    const-string v0, "ep.enable"

    invoke-static {p3, v0}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/util/Vector;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/ce;->i:Z

    const-string v0, "NMSP_DEFINES_RECORDER_CONTINUES_ON_ENDPOINTER_AND_TIMER_STOPPING"

    invoke-static {p3, v0}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/util/Vector;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/ce;->j:Z

    const-string v0, "NMSP_DEFINES_CAPTURING_CONTINUES_ON_ENDPOINTER"

    invoke-static {p3, v0}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/util/Vector;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/nuance/nmdp/speechkit/ce;->k:Z

    new-instance v0, Lcom/nuance/nmsp/client/sdk/oem/AudioSystemOEM;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/ce;->b:Lcom/nuance/nmdp/speechkit/bf;

    invoke-direct {v0, v1, v2, p3}, Lcom/nuance/nmsp/client/sdk/oem/AudioSystemOEM;-><init>(Lcom/nuance/nmdp/speechkit/bi;Lcom/nuance/nmdp/speechkit/bf;Ljava/util/Vector;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->c:Lcom/nuance/nmdp/speechkit/bg;

    sget-object v0, Lcom/nuance/nmdp/speechkit/bd;->c:Lcom/nuance/nmdp/speechkit/bd;

    invoke-virtual {p4, v0}, Lcom/nuance/nmdp/speechkit/bd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/nuance/nmdp/speechkit/bg$h;->c:Lcom/nuance/nmdp/speechkit/bg$h;

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->l:Lcom/nuance/nmdp/speechkit/bg$h;

    :cond_0
    :goto_0
    iput v3, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    return-void

    :cond_1
    sget-object v0, Lcom/nuance/nmdp/speechkit/bd;->a:Lcom/nuance/nmdp/speechkit/bd;

    invoke-virtual {p4, v0}, Lcom/nuance/nmdp/speechkit/bd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/nuance/nmdp/speechkit/bg$h;->a:Lcom/nuance/nmdp/speechkit/bg$h;

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->l:Lcom/nuance/nmdp/speechkit/bg$h;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/nuance/nmdp/speechkit/bd;->b:Lcom/nuance/nmdp/speechkit/bd;

    invoke-virtual {p4, v0}, Lcom/nuance/nmdp/speechkit/bd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/nuance/nmdp/speechkit/bg$h;->b:Lcom/nuance/nmdp/speechkit/bg$h;

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->l:Lcom/nuance/nmdp/speechkit/bg$h;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/nuance/nmdp/speechkit/bd;->d:Lcom/nuance/nmdp/speechkit/bd;

    invoke-virtual {p4, v0}, Lcom/nuance/nmdp/speechkit/bd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/bg$h;->d:Lcom/nuance/nmdp/speechkit/bg$h;

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->l:Lcom/nuance/nmdp/speechkit/bg$h;

    goto :goto_0
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/ce;)Lcom/nuance/nmdp/speechkit/bl;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->h:Lcom/nuance/nmdp/speechkit/bl;

    return-object v0
.end method

.method private a(Lcom/nuance/nmdp/speechkit/bx;I)V
    .locals 12

    const/4 v11, 0x0

    const/4 v10, 0x1

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl.handleStartRecording("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") _state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    new-instance v0, Lcom/nuance/nmsp/client/sdk/oem/b;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ce;->m:Ljava/util/Vector;

    invoke-direct {v0, v1}, Lcom/nuance/nmsp/client/sdk/oem/b;-><init>(Ljava/util/Vector;)V

    invoke-virtual {v0}, Lcom/nuance/nmsp/client/sdk/oem/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->b:Lcom/nuance/nmdp/speechkit/bf;

    invoke-static {v0}, Lcom/nuance/nmdp/speechkit/bs;->c(Lcom/nuance/nmdp/speechkit/bf;)Lcom/nuance/nmdp/speechkit/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->b:Lcom/nuance/nmdp/speechkit/bf;

    :cond_1
    iput p2, p0, Lcom/nuance/nmdp/speechkit/ce;->g:I

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-nez v0, :cond_3

    iput-object p1, p0, Lcom/nuance/nmdp/speechkit/ce;->f:Lcom/nuance/nmdp/speechkit/bx;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->c:Lcom/nuance/nmdp/speechkit/bg;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ce;->l:Lcom/nuance/nmdp/speechkit/bg$h;

    iget-boolean v2, p0, Lcom/nuance/nmdp/speechkit/ce;->i:Z

    if-eqz v2, :cond_4

    if-eqz p1, :cond_4

    move v2, v10

    :goto_0
    move-object v3, p0

    move-object v4, p0

    move-object v5, p0

    move-object v6, p0

    move-object v7, p0

    move-object v8, p0

    move-object v9, p0

    invoke-interface/range {v0 .. v9}, Lcom/nuance/nmdp/speechkit/bg;->a(Lcom/nuance/nmdp/speechkit/bg$h;ZLcom/nuance/nmdp/speechkit/bg$a;Lcom/nuance/nmdp/speechkit/bg$m;Lcom/nuance/nmdp/speechkit/bg$g;Lcom/nuance/nmdp/speechkit/bg$l;Lcom/nuance/nmdp/speechkit/bg$d;Lcom/nuance/nmdp/speechkit/bg$e;Lcom/nuance/nmdp/speechkit/bg$f;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "RecorderImpl.handleStartRecording() startRecording() failed!!!"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    :cond_2
    const/16 v0, 0x8

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const-string v0, "RECORD_ERROR"

    invoke-direct {p0, v0, v11}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/ce;->i:Z

    if-eqz v0, :cond_7

    if-nez p1, :cond_6

    iput v10, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    :goto_2
    const-string v0, "STARTED"

    invoke-direct {p0, v0, v11}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    const/4 v0, 0x2

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_2

    :cond_7
    if-nez p1, :cond_8

    iput v10, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_2

    :cond_8
    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->h()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->d:Lcom/nuance/nmdp/speechkit/bu;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->d:Lcom/nuance/nmdp/speechkit/bu;

    invoke-interface {v0, p0, p1, p2}, Lcom/nuance/nmdp/speechkit/bu;->a(Lcom/nuance/nmdp/speechkit/bv;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/bh;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got an exp while calling NMSPAudioRecordListener.recorderUpdate("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] msg ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Vector;)V
    .locals 4

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/nmdp/speechkit/bz;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->d()Lcom/nuance/nmdp/speechkit/bz$a;

    move-result-object v2

    sget-object v3, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    if-eq v2, v3, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Parameter type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->d()Lcom/nuance/nmdp/speechkit/bz$a;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not allowed. "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    return-void
.end method

.method private static a(Ljava/util/Vector;Ljava/lang/String;)Z
    .locals 5

    const/4 v1, 0x0

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/nmdp/speechkit/bz;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->d()Lcom/nuance/nmdp/speechkit/bz$a;

    move-result-object v3

    sget-object v4, Lcom/nuance/nmdp/speechkit/bz$a;->a:Lcom/nuance/nmdp/speechkit/bz$a;

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bz;->b()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    const-string v0, "TRUE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private b(Z)V
    .locals 4

    const/4 v3, 0x4

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl.handleStopRecording() _state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-eq v0, v3, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_5

    :cond_1
    if-eqz p1, :cond_2

    const-string v0, "CAPTURE_TIMEOUT"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-ne v0, v3, :cond_3

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->i()V

    :cond_3
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->c:Lcom/nuance/nmdp/speechkit/bg;

    invoke-interface {v0, p0}, Lcom/nuance/nmdp/speechkit/bg;->a(Lcom/nuance/nmdp/speechkit/bg$m;)V

    const/16 v0, 0x9

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    :cond_4
    :goto_0
    return-void

    :cond_5
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    :cond_6
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->c:Lcom/nuance/nmdp/speechkit/bg;

    invoke-interface {v0, p0}, Lcom/nuance/nmdp/speechkit/bg;->a(Lcom/nuance/nmdp/speechkit/bg$m;)V

    const/4 v0, 0x7

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/ce;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/ce;->j:Z

    return v0
.end method

.method static synthetic c(Lcom/nuance/nmdp/speechkit/ce;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/nuance/nmdp/speechkit/ce;->b(Z)V

    return-void
.end method

.method private h()V
    .locals 4

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->g:I

    if-lez v0, :cond_0

    new-instance v0, Lcom/nuance/nmdp/speechkit/ce$1;

    invoke-direct {v0, p0}, Lcom/nuance/nmdp/speechkit/ce$1;-><init>(Lcom/nuance/nmdp/speechkit/ce;)V

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ce;->h:Lcom/nuance/nmdp/speechkit/bl;

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->g:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;J)V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->h:Lcom/nuance/nmdp/speechkit/bl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ce;->h:Lcom/nuance/nmdp/speechkit/bl;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/bi;->a(Lcom/nuance/nmdp/speechkit/bl;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->h:Lcom/nuance/nmdp/speechkit/bl;

    :cond_0
    return-void
.end method

.method private j()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->f:Lcom/nuance/nmdp/speechkit/bx;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/nuance/nmdp/speechkit/bx;->a([BIIZ)V
    :try_end_0
    .catch Lcom/nuance/nmdp/speechkit/cc; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "RecorderImpl::finishAudioSink send the last audio buffer from recorder"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/bh;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RecorderImpl.finishAudioSink() TransactionProcessingException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/nmdp/speechkit/bh;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl::endOfSpeechCallback() _state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const-string v0, "END_OF_SPEECH"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/ce;->k:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nuance/nmdp/speechkit/ce;->a(Z)V

    :cond_1
    return-void
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/bg$b;)V
    .locals 6

    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x4

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl.stopCallback() _state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    :cond_1
    const-string v0, "STOPPED"

    invoke-direct {p0, v0, v5}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iput v4, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-eq v0, v3, :cond_4

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    :cond_4
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-ne v0, v3, :cond_5

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->i()V

    :cond_5
    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->j()V

    const-string v0, "STOPPED"

    invoke-direct {p0, v0, v5}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iput v4, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0
.end method

.method public final a(Lcom/nuance/nmdp/speechkit/bx;)V
    .locals 7

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl.startCapturing("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "audioSink cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    new-instance v1, Lcom/nuance/nmdp/speechkit/bi$a;

    const/4 v2, 0x4

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/Integer;

    const v6, 0xea60

    invoke-direct {v5, v6}, Ljava/lang/Integer;-><init>(I)V

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    invoke-interface {v3}, Lcom/nuance/nmdp/speechkit/bi;->a()[Ljava/lang/Object;

    invoke-interface {v0, v1, p0, v2}, Lcom/nuance/nmdp/speechkit/bi;->a(Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bi$b;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    check-cast p1, Lcom/nuance/nmdp/speechkit/bi$a;

    iget-byte v0, p1, Lcom/nuance/nmdp/speechkit/bi$a;->a:B

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/ce;->a(Lcom/nuance/nmdp/speechkit/bx;I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Lcom/nuance/nmdp/speechkit/bi$a;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    aget-object v1, v0, v1

    check-cast v1, Lcom/nuance/nmdp/speechkit/bx;

    aget-object v0, v0, v5

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/nuance/nmdp/speechkit/ce;->a(Lcom/nuance/nmdp/speechkit/bx;I)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v1}, Lcom/nuance/nmdp/speechkit/ce;->b(Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Lcom/nuance/nmdp/speechkit/bi$a;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    aget-object v1, v0, v1

    check-cast v1, Lcom/nuance/nmdp/speechkit/bx;

    aget-object v0, v0, v5

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v2, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v2}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RecorderImpl.handleStartCapturing("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") _state:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_1
    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->g:I

    iput-object v1, p0, Lcom/nuance/nmdp/speechkit/ce;->f:Lcom/nuance/nmdp/speechkit/bx;

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-ne v0, v5, :cond_3

    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/ce;->i:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->c:Lcom/nuance/nmdp/speechkit/bg;

    invoke-interface {v0, p0}, Lcom/nuance/nmdp/speechkit/bg;->a(Lcom/nuance/nmdp/speechkit/bg$e;)V

    iput v6, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->h()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    iput v6, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {p0, v1}, Lcom/nuance/nmdp/speechkit/ce;->a(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x6

    const/4 v3, 0x4

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl.handleStopCapturing() _state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->j()V

    if-eqz p1, :cond_1

    const-string v0, "CAPTURE_TIMEOUT"

    invoke-direct {p0, v0, v5}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-ne v0, v3, :cond_7

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->j()V

    if-eqz p1, :cond_4

    const-string v0, "CAPTURE_TIMEOUT"

    invoke-direct {p0, v0, v5}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_4
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-ne v0, v3, :cond_5

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->i()V

    :cond_5
    iget-boolean v0, p0, Lcom/nuance/nmdp/speechkit/ce;->i:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->c:Lcom/nuance/nmdp/speechkit/bg;

    invoke-interface {v0, p0}, Lcom/nuance/nmdp/speechkit/bg;->a(Lcom/nuance/nmdp/speechkit/bg$f;)V

    iput v4, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0

    :cond_6
    const/4 v0, 0x1

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0

    :cond_7
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->j()V

    iput v4, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0
.end method

.method public final a([BLjava/lang/Object;Lcom/nuance/nmdp/speechkit/bg$i;Lcom/nuance/nmdp/speechkit/bg$i;Ljava/lang/Float;)V
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RecorderImpl.audioCallback() _state:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget v1, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/ce;->b:Lcom/nuance/nmdp/speechkit/bf;

    invoke-static {}, Lcom/nuance/nmdp/speechkit/bs;->a()Z

    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/ce;->b:Lcom/nuance/nmdp/speechkit/bf;

    invoke-static {v2}, Lcom/nuance/nmdp/speechkit/bs;->b(Lcom/nuance/nmdp/speechkit/bf;)Z

    move-result v2

    if-eqz v2, :cond_4

    check-cast p2, [B

    iget v0, p4, Lcom/nuance/nmdp/speechkit/bg$i;->a:I

    :goto_0
    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "========================= Recorder::audioCallback len["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] ======================"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/ce;->f:Lcom/nuance/nmdp/speechkit/bx;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, p2, v2, v0, v3}, Lcom/nuance/nmdp/speechkit/bx;->a([BIIZ)V
    :try_end_0
    .catch Lcom/nuance/nmdp/speechkit/cc; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const-string v0, "BUFFER_RECORDED"

    invoke-direct {p0, v0, p5}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    return-void

    :cond_4
    iget-object v2, p0, Lcom/nuance/nmdp/speechkit/ce;->b:Lcom/nuance/nmdp/speechkit/bf;

    invoke-static {v2}, Lcom/nuance/nmdp/speechkit/bs;->a(Lcom/nuance/nmdp/speechkit/bf;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget v0, p3, Lcom/nuance/nmdp/speechkit/bg$i;->a:I

    move-object p2, p1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v1}, Lcom/nuance/nmdp/speechkit/bh;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RecorderImpl.audioCallback() TransactionProcessingException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/nmdp/speechkit/bh;->d(Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    move-object p2, v1

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl::endPointerStartedCallback() _state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->h()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->c:Lcom/nuance/nmdp/speechkit/bg;

    invoke-interface {v0, p0}, Lcom/nuance/nmdp/speechkit/bg;->a(Lcom/nuance/nmdp/speechkit/bg$f;)V

    const/4 v0, 0x6

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl::endPointerStoppedCallback() _state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->c:Lcom/nuance/nmdp/speechkit/bg;

    invoke-interface {v0, p0}, Lcom/nuance/nmdp/speechkit/bg;->a(Lcom/nuance/nmdp/speechkit/bg$e;)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    const/4 v3, 0x4

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl.errorCallback() _state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-eq v0, v3, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    :cond_1
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    if-ne v0, v3, :cond_2

    invoke-direct {p0}, Lcom/nuance/nmdp/speechkit/ce;->i()V

    :cond_2
    const-string v0, "RECORD_ERROR"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x7

    iput v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    :cond_3
    return-void
.end method

.method public final e()V
    .locals 3

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecorderImpl::startOfSpeechCallback() _state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/nuance/nmdp/speechkit/ce;->n:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const-string v0, "START_OF_SPEECH"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/nmdp/speechkit/ce;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final f()V
    .locals 4

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "RecorderImpl.startRecording()"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    new-instance v1, Lcom/nuance/nmdp/speechkit/bi$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    invoke-interface {v3}, Lcom/nuance/nmdp/speechkit/bi;->a()[Ljava/lang/Object;

    invoke-interface {v0, v1, p0, v2}, Lcom/nuance/nmdp/speechkit/bi;->a(Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bi$b;Ljava/lang/Object;)V

    return-void
.end method

.method public final g()V
    .locals 4

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nuance/nmdp/speechkit/ce;->a:Lcom/nuance/nmdp/speechkit/bh;

    const-string v1, "RecorderImpl.stop()"

    invoke-virtual {v0, v1}, Lcom/nuance/nmdp/speechkit/bh;->b(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    new-instance v1, Lcom/nuance/nmdp/speechkit/bi$a;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/nuance/nmdp/speechkit/bi$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/ce;->e:Lcom/nuance/nmdp/speechkit/bi;

    invoke-interface {v3}, Lcom/nuance/nmdp/speechkit/bi;->a()[Ljava/lang/Object;

    invoke-interface {v0, v1, p0, v2}, Lcom/nuance/nmdp/speechkit/bi;->a(Ljava/lang/Object;Lcom/nuance/nmdp/speechkit/bi$b;Ljava/lang/Object;)V

    return-void
.end method
