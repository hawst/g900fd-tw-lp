.class public abstract Lcom/nuance/nmdp/speechkit/o;
.super Lcom/nuance/nmdp/speechkit/q;


# instance fields
.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/nuance/nmdp/speechkit/r;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/nuance/nmdp/speechkit/q;-><init>(Lcom/nuance/nmdp/speechkit/r;)V

    iput p2, p0, Lcom/nuance/nmdp/speechkit/o;->b:I

    iput-object p3, p0, Lcom/nuance/nmdp/speechkit/o;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/nuance/nmdp/speechkit/o;->d:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/nuance/nmdp/speechkit/o;->e:Z

    return-void
.end method

.method static synthetic a(Lcom/nuance/nmdp/speechkit/o;)I
    .locals 1

    iget v0, p0, Lcom/nuance/nmdp/speechkit/o;->b:I

    return v0
.end method

.method static synthetic b(Lcom/nuance/nmdp/speechkit/o;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/o;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/nuance/nmdp/speechkit/o;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/o;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Transaction error code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/nuance/nmdp/speechkit/o;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/o;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Transaction error text: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/o;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/o;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Transaction suggestion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/o;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/nuance/nmdp/speechkit/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/o;->a:Lcom/nuance/nmdp/speechkit/r;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/r;->f()V

    iget-object v0, p0, Lcom/nuance/nmdp/speechkit/o;->a:Lcom/nuance/nmdp/speechkit/r;

    invoke-virtual {v0}, Lcom/nuance/nmdp/speechkit/r;->d()Lcom/nuance/nmdp/speechkit/p;

    move-result-object v0

    iget-boolean v1, p0, Lcom/nuance/nmdp/speechkit/o;->e:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/o;->a:Lcom/nuance/nmdp/speechkit/r;

    iget v2, p0, Lcom/nuance/nmdp/speechkit/o;->b:I

    iget-object v3, p0, Lcom/nuance/nmdp/speechkit/o;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/nuance/nmdp/speechkit/o;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/nuance/nmdp/speechkit/p;->a(Lcom/nuance/nmdp/speechkit/SpeechKit$a;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/nuance/nmdp/speechkit/o;->a:Lcom/nuance/nmdp/speechkit/r;

    invoke-interface {v0, v1}, Lcom/nuance/nmdp/speechkit/p;->a(Lcom/nuance/nmdp/speechkit/SpeechKit$a;)V

    :goto_0
    return-void

    :cond_2
    new-instance v1, Lcom/nuance/nmdp/speechkit/o$1;

    invoke-direct {v1, p0, v0}, Lcom/nuance/nmdp/speechkit/o$1;-><init>(Lcom/nuance/nmdp/speechkit/o;Lcom/nuance/nmdp/speechkit/p;)V

    invoke-static {v1}, Lcom/nuance/nmdp/speechkit/av;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
