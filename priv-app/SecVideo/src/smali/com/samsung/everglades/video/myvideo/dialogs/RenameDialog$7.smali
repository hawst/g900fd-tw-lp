.class Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;
.super Ljava/lang/Object;
.source "RenameDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

.field final synthetic val$alertEditText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->val$alertEditText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0c00df

    const v4, 0x7f0c0005

    const/4 v5, 0x0

    .line 210
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$500(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$500(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->val$alertEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$000(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showToast(Ljava/lang/String;I)V
    invoke-static {v2, v3, v5}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$200(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;I)V

    .line 236
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->val$alertEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 216
    .local v1, "filenameToRename":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->fileAlreadyExist(Ljava/lang/String;)Z
    invoke-static {v2, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$600(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 217
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$000(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showToast(Ljava/lang/String;I)V
    invoke-static {v2, v3, v5}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$200(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;I)V

    goto :goto_0

    .line 221
    :cond_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->val$alertEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$702(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 222
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$700(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 223
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$000(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0086

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showToast(Ljava/lang/String;I)V
    invoke-static {v2, v3, v5}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$200(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;I)V

    goto :goto_0

    .line 228
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$900(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->rename(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$1000(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 234
    :goto_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 235
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->cancelSelectionMode()V
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$1200(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V

    goto :goto_0

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->handleException(Ljava/lang/Exception;I)V
    invoke-static {v2, v0, v6}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$1100(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/Exception;I)V

    goto :goto_1

    .line 231
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/StringIndexOutOfBoundsException;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->handleException(Ljava/lang/Exception;I)V
    invoke-static {v2, v0, v6}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$1100(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/Exception;I)V

    goto :goto_1
.end method
