.class Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;
.super Landroid/os/Handler;
.source "DeviceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V
    .locals 0

    .prologue
    .line 814
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v6, 0x1f4

    .line 817
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DeviceListFragment : handleMessage - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 818
    iget v3, p1, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    .line 867
    :cond_0
    :goto_0
    return-void

    .line 820
    :sswitch_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$700(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$800(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 821
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$700(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showRefreshBtn()V

    .line 822
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$700(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$800(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$900(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 823
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$700(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImgBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    .line 827
    :sswitch_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 828
    .local v0, "activity":Landroid/app/Activity;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1000(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSignedIn()Z

    move-result v2

    .line 829
    .local v2, "isSignedIn":Z
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1000(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSamsungAccountExists()Z

    move-result v1

    .line 830
    .local v1, "hasSamsungAccount":Z
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mMenu:Landroid/view/Menu;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1100(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/view/Menu;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 831
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mMenu:Landroid/view/Menu;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1100(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/view/Menu;

    move-result-object v3

    const v4, 0x7f090072

    invoke-interface {v3, v4, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 832
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mMenu:Landroid/view/Menu;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1100(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/view/Menu;

    move-result-object v4

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->prepareOptionMenu(Landroid/view/Menu;)V
    invoke-static {v3, v4}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1200(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;Landroid/view/Menu;)V

    .line 835
    :cond_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 836
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    .line 837
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateDeviceList()Z
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1300(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 838
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$600(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$600(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x12c

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 846
    :goto_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$600(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 847
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$600(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$600(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 840
    :cond_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->refresh()V

    goto :goto_1

    .line 843
    :cond_3
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->refresh()V

    goto :goto_1

    .line 851
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "hasSamsungAccount":Z
    .end local v2    # "isSignedIn":Z
    :sswitch_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1400(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showRefreshBtn()V

    .line 852
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$300(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 853
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1400(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$300(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$900(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 854
    :cond_4
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1400(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImgBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto/16 :goto_0

    .line 857
    :sswitch_3
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1500(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/widget/ListView;

    move-result-object v3

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setListViewHeight(Landroid/widget/ListView;)V
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1600(Landroid/widget/ListView;)V

    goto/16 :goto_0

    .line 860
    :sswitch_4
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 861
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 818
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_2
        0x12c -> :sswitch_3
        0x190 -> :sswitch_4
        0x1f4 -> :sswitch_0
    .end sparse-switch
.end method
