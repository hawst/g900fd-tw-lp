.class Lcom/samsung/everglades/video/myvideo/common/CloudUtil$4;
.super Ljava/lang/Object;
.source "CloudUtil.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->showFirstCloudActivationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)V
    .locals 0

    .prologue
    .line 1247
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$4;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1249
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1250
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "com.sec.android.cloudagent.DROPBOX_AUTH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1251
    const-string v2, "requestSignIn"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1254
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$4;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$000(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 1255
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$4;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$000(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const/16 v3, 0x1194

    invoke-virtual {v2, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1260
    :cond_0
    :goto_0
    return-void

    .line 1257
    :catch_0
    move-exception v0

    .line 1258
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "CloudUtil"

    const-string v3, "No activity to handle com.sec.android.cloudagent.DROPBOX_AUTH"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
