.class final Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$SimpleGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "FolderHoverViewer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SimpleGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;


# direct methods
.method private constructor <init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 0

    .prologue
    .line 436
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$SimpleGestureListener;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
    .param p2, "x1"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$1;

    .prologue
    .line 436
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$SimpleGestureListener;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v0, 0x1

    .line 440
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x43480000    # 200.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 441
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$SimpleGestureListener;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    const/16 v2, 0xa

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->moveFolderAirView(I)V
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1900(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V

    .line 448
    :goto_0
    return v0

    .line 444
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    const/high16 v2, -0x3cb80000    # -200.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 445
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$SimpleGestureListener;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    const/16 v2, 0xb

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->moveFolderAirView(I)V
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1900(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V

    goto :goto_0

    .line 448
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
