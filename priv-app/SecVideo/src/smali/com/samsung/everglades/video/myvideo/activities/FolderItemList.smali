.class public Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;
.super Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;
.source "FolderItemList.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;-><init>()V

    return-void
.end method

.method private getFolderName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 24
    const/4 v1, 0x0

    .line 25
    .local v1, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 27
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 28
    const-string v2, "bucket_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    :cond_0
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 17
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->getFolderName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->setTitle(Ljava/lang/String;)V

    .line 18
    if-nez p1, :cond_0

    .line 19
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->setupView()V

    .line 21
    :cond_0
    return-void
.end method

.method protected updateContentFragment()V
    .locals 5

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 36
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 37
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "args":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 40
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_0
    const-string v1, "list_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 41
    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;-><init>()V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 42
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f090005

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    sget-object v4, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 44
    return-void
.end method
