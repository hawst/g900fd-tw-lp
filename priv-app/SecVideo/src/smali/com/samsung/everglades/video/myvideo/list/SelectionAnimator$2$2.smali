.class Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SelectionAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->onPreDraw()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

.field final synthetic val$childCount:I


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;I)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iput p2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;->val$childCount:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 156
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;->val$childCount:I

    if-ge v1, v2, :cond_1

    .line 157
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v2, v2, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v2, v2, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 158
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v2, v2, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f09002c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 159
    .local v0, "cb":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 160
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 161
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 156
    .end local v0    # "cb":Landroid/view/View;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 165
    :cond_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v2, v2, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 166
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v2, v2, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$400(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v2

    if-nez v2, :cond_2

    .line 167
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$2;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v2, v2, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwGridView;->setEnabled(Z)V

    .line 170
    :cond_2
    return-void
.end method
