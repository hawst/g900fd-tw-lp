.class Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Queue"
.end annotation


# static fields
.field private static final QUEUE_SIZE:I = 0x1e

.field private static mQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mQueueInProcess:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    .line 182
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized QueueInProcessContain(Ljava/lang/String;)Z
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 216
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    const/4 v0, 0x1

    .line 219
    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 216
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized get()Ljava/lang/String;
    .locals 3

    .prologue
    .line 192
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 196
    :goto_0
    monitor-exit v1

    return-object v0

    .line 195
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 196
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peekLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized isEmpty()Z
    .locals 2

    .prologue
    .line 205
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized put(Ljava/lang/String;)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 185
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 186
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x1e

    if-le v0, v2, :cond_0

    .line 187
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    :cond_0
    monitor-exit v1

    return-void

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized removeQueueInProcess(Ljava/lang/String;)Z
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 209
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :cond_0
    const/4 v0, 0x1

    monitor-exit v1

    return v0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized size()I
    .locals 2

    .prologue
    .line 201
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
