.class public Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
.super Landroid/app/Fragment;
.source "VideoListFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/ActionMode$Callback;
.implements Lcom/samsung/everglades/video/myvideo/common/Utils$OnOptionsChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;,
        Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Fragment;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/samsung/everglades/video/myvideo/common/Utils$OnOptionsChangedListener;",
        "Landroid/view/ActionMode$Callback;"
    }
.end annotation


# static fields
.field private static CHECKBOX_FLOATING_DEFAULT_DURATION:I = 0x0

.field private static final COUNT_OF_LIST_FOLD:I = 0x1

.field private static final COUNT_OF_THUMBNAIL_LAND:I = 0x4

.field private static final COUNT_OF_THUMBNAIL_VERT:I = 0x2

.field private static final DELAY_RENAME_LIVE_THUMB:I = 0x7d0

.field private static final HANDLE_NOTIFY_DATA_CHANGED:I = 0x64

.field private static final HANDLE_RENAME_LIVE_THUMB:I = 0x190

.field private static final HANDLE_RESTART_LIVE_THUMB:I = 0x1f4

.field private static final HANDLE_RESTART_LOADER:I = 0xc8

.field private static final HANDLE_START_LIVE_THUMB:I = 0x12c

.field private static final MAX_SIZE_SPINNER_ADAPTER:I = 0x3

.field public static isFromSharevia:Z

.field private static mOnPrivateModeListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;


# instance fields
.field private isDeletePending:Z

.field private mActionBarSelectAllCheckBox:Landroid/widget/CheckBox;

.field private mActionBarSelectAllLayout:Landroid/widget/LinearLayout;

.field private mActionMode:Landroid/view/ActionMode;

.field private mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

.field private mAddDeleteGridAnimator:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;

.field private mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

.field private mBucketId:I

.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field mContainerPaddingLeft:I

.field private mContentCountText:Landroid/widget/TextView;

.field mDuration:I

.field private mEmptyViewStub:Landroid/view/View;

.field private final mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public mGridSortAnimator:Lcom/sec/android/touchwiz/animator/TwGridSortAnimator;

.field private mHandler:Landroid/os/Handler;

.field public mIsDropboxFolder:Z

.field private mIsInEditMode:Z

.field private mLastLoadedCount:I

.field private mListScrollListener:Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;

.field private mListShown:Z

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

.field private mListfixedLayout:Landroid/widget/LinearLayout;

.field private mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

.field private mMenu:Landroid/view/Menu;

.field private mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

.field mOnAddDeleteAnimationListener:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator$OnAddDeleteListener;

.field private mOnFolderChangedListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;

.field private mOnItemClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;

.field private mOnLongClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;

.field private mOrientation:I

.field private mProgressLayout:Landroid/widget/RelativeLayout;

.field public mRenamedFileInfoBundle:[Ljava/lang/Object;

.field private mRoot:Landroid/view/View;

.field private mSLinkDeviceId:I

.field private mScrollState:I

.field private mSearchKey:Ljava/lang/String;

.field private final mSecretModeReceiver:Landroid/content/BroadcastReceiver;

.field private mSecretModeStateFilter:Landroid/content/IntentFilter;

.field private mSelectAllCheckBox:Landroid/widget/CheckBox;

.field private mSelectAllLayout:Landroid/widget/LinearLayout;

.field private mSelectAllStateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

.field private mSelectAnimator:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

.field private mSelectedItemCountText:Landroid/widget/TextView;

.field private mSelectionModeActionbar:Landroid/view/View;

.field private mSelectionModeTitle:Landroid/widget/TextView;

.field private mSelectionSpinner:Landroid/widget/Spinner;

.field private mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

.field private mViewOptionMenu:I

.field private mViewTreeObserver:Landroid/view/ViewTreeObserver;

.field num:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    const/16 v0, 0x1f4

    sput v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->CHECKBOX_FLOATING_DEFAULT_DURATION:I

    .line 155
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isFromSharevia:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 161
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 126
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    .line 128
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    .line 131
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBucketId:I

    .line 132
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mIsDropboxFolder:Z

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    .line 134
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mScrollState:I

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListShown:Z

    .line 139
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mIsInEditMode:Z

    .line 140
    sget v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->CHECKBOX_FLOATING_DEFAULT_DURATION:I

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mDuration:I

    .line 141
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mContainerPaddingLeft:I

    .line 143
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewOptionMenu:I

    .line 153
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isDeletePending:Z

    .line 587
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$1;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 645
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$4;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$4;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllStateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    .line 1006
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListScrollListener:Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;

    .line 1032
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnItemClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;

    .line 1102
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnLongClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;

    .line 1266
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->num:I

    .line 1524
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    .line 1872
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$18;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$18;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnAddDeleteAnimationListener:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator$OnAddDeleteListener;

    .line 1993
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$19;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$19;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    .line 161
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setNumberOfColumns()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;Landroid/view/View;Landroid/database/Cursor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/database/Cursor;

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateSelectedFolder(Landroid/view/View;Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSearchKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBucketId:I

    return v0
.end method

.method static synthetic access$1400(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .param p1, "x1"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->newLongClick(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;ILandroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->searchLongClick(ILandroid/view/View;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListShown:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    return-object v0
.end method

.method static synthetic access$1800()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnPrivateModeListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectAll(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionBarSelectAllCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;Ljava/util/Map;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .param p1, "x1"    # Ljava/util/Map;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->putTempItemsToMultiSelector(Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mScrollState:I

    return v0
.end method

.method static synthetic access$802(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .param p1, "x1"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mScrollState:I

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    return-object v0
.end method

.method private createOptionMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1793
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 1794
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenu(Landroid/view/Menu;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 1795
    const/4 v1, 0x7

    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 1796
    return-void
.end method

.method private getColumnsCount()I
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 774
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 775
    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOrientation:I

    if-ne v3, v2, :cond_0

    .line 776
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isMultiWindow(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 777
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 778
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080080

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 779
    .local v0, "columnWidth":I
    div-int v2, v1, v0

    .line 788
    .end local v0    # "columnWidth":I
    .end local v1    # "width":I
    :cond_0
    :goto_0
    return v2

    .line 782
    :cond_1
    const/4 v2, 0x4

    goto :goto_0

    .line 788
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private getNumofDigit(I)I
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 1283
    const/4 v0, 0x0

    .line 1284
    .local v0, "num":I
    :goto_0
    const/16 v1, 0xa

    if-lt p1, v1, :cond_0

    .line 1285
    add-int/lit8 v0, v0, 0x1

    .line 1286
    div-int/lit8 p1, p1, 0xa

    goto :goto_0

    .line 1288
    :cond_0
    return v0
.end method

.method private handleNearbyListUpdater(Z)V
    .locals 2
    .param p1, "start"    # Z

    .prologue
    .line 929
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 930
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    if-nez v0, :cond_0

    .line 931
    const-string v0, "handleNearbyListUpdater() : create ListUpdater"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 932
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    .line 935
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->handle(Z)V

    .line 937
    :cond_1
    return-void
.end method

.method private initArgs()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 187
    const-string v5, "VideoListFragment - initArgs() enter"

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 189
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v4

    .line 190
    .local v4, "pref":Lcom/samsung/everglades/video/myvideo/common/Pref;
    const/4 v3, 0x0

    .line 191
    .local v3, "listType":I
    const/4 v2, 0x0

    .line 192
    .local v2, "listAttr":I
    const/4 v1, 0x0

    .line 193
    .local v1, "listAdditions":I
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 194
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 195
    const-string v5, "list_type"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 196
    const-string v5, "list_attribute"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 197
    const-string v5, "list_additional_infos"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 199
    const-string v5, "bucket_id"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBucketId:I

    .line 200
    const-string v5, "dropbox_folder"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mIsDropboxFolder:Z

    .line 201
    const-string v5, "search_key"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSearchKey:Ljava/lang/String;

    .line 202
    const-string v5, "s_link_device_id"

    const/4 v6, -0x1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSLinkDeviceId:I

    .line 207
    :goto_0
    new-instance v5, Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-direct {v5, v3, v2, v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;-><init>(III)V

    iput-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 208
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VideoListFragment - initArgs() : listType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 209
    const-string v5, "VideoListFragment - initArgs() end"

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 210
    return-void

    .line 204
    :cond_0
    const-string v5, "lastViewAs"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v3

    goto :goto_0
.end method

.method private initGlobalVars()V
    .locals 2

    .prologue
    .line 890
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-nez v0, :cond_0

    .line 891
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 894
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    if-nez v0, :cond_1

    .line 895
    new-instance v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    .line 898
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    if-nez v0, :cond_2

    .line 899
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    .line 902
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setContext(Landroid/content/Context;)V

    .line 903
    return-void
.end method

.method private initListView(Landroid/view/View;)V
    .locals 4
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 664
    const v0, 0x7f09004a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwGridView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    .line 665
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    const/high16 v1, 0x100000

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setDrawingCacheQuality(I)V

    .line 666
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setTextFilterEnabled(Z)V

    .line 667
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->onWindowFocusChanged(Z)V

    .line 668
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListScrollListener:Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setOnScrollListener(Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;)V

    .line 669
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnItemClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setOnItemClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;)V

    .line 670
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnLongClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setOnItemLongClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;)V

    .line 674
    :cond_0
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceSearchFileLists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 675
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setTwMultiSelectedListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnTwMultiSelectedListener;)V

    .line 717
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->setEnableDragBlock(Z)V

    .line 719
    :cond_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setNumberOfColumns()V

    .line 720
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setListView()V

    .line 722
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v0, :cond_2

    .line 723
    new-instance v0, Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;-><init>(Landroid/content/Context;Lcom/sec/android/touchwiz/widget/TwGridView;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAddDeleteGridAnimator:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;

    .line 724
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAddDeleteGridAnimator:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnAddDeleteAnimationListener:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator$OnAddDeleteListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;->setOnAddDeleteListener(Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator$OnAddDeleteListener;)V

    .line 725
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;-><init>(Landroid/content/Context;Lcom/sec/android/touchwiz/widget/TwGridView;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAnimator:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    .line 727
    :cond_2
    return-void
.end method

.method private initSpinner()V
    .locals 2

    .prologue
    .line 1185
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    if-nez v0, :cond_0

    .line 1186
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->initSpinnerAdapter()V

    .line 1188
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1216
    return-void
.end method

.method private initSpinnerAdapter()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 1219
    const-string v1, "initSpinnerAdapter()"

    invoke-static {v4, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 1220
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    .line 1221
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    const v2, 0x7f040017

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;->setDropDownViewResource(I)V

    .line 1222
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v0

    .line 1223
    .local v0, "selectedCount":I
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00b9

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;->add(Ljava/lang/Object;)V

    .line 1224
    return-void
.end method

.method private isFolderListFragment()Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 584
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "list_type"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static newInstance()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .locals 1

    .prologue
    .line 164
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;-><init>()V

    .line 165
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    return-object v0
.end method

.method private newLongClick(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v6, 0x1

    .line 1342
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1343
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v3

    const-string v4, "lastCloudViewOption"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewOptionMenu:I

    .line 1345
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->setSelectionMode()V

    .line 1346
    invoke-virtual {p0, v6, v6, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectionMode(ZZI)V

    .line 1348
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v3, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1349
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-static {v3, v0, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v2

    .line 1350
    .local v2, "uri":Landroid/net/Uri;
    const-string v3, "_data"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1351
    .local v1, "filePath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->put(Landroid/net/Uri;Ljava/lang/String;)Z

    .line 1353
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->invalidateActionMode()V

    .line 1355
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method private prepareOptionMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1799
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mMenu:Landroid/view/Menu;

    .line 1800
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 1801
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    if-eqz v1, :cond_0

    .line 1802
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenu(Landroid/view/Menu;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setCount(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 1806
    :goto_0
    const/16 v1, 0x8

    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 1807
    return-void

    .line 1804
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenu(Landroid/view/Menu;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    goto :goto_0
.end method

.method private putTempItemsToMultiSelector(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 730
    .local p1, "tempMap":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/Uri;Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 742
    :goto_0
    return-void

    .line 732
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    .line 734
    .local v2, "mulitSelector":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 735
    .local v1, "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/net/Uri;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->has(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 736
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->remove(Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_1

    .line 738
    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->put(Landroid/net/Uri;Ljava/lang/String;)Z

    goto :goto_1

    .line 741
    .end local v1    # "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/net/Uri;Ljava/lang/String;>;"
    :cond_2
    invoke-interface {p1}, Ljava/util/Map;->clear()V

    goto :goto_0
.end method

.method private registerSecretModeStateReceiver()V
    .locals 3

    .prologue
    .line 1988
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSecretModeStateFilter:Landroid/content/IntentFilter;

    .line 1989
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSecretModeStateFilter:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1990
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSecretModeStateFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1991
    return-void
.end method

.method private releaseLiveThumbnail()V
    .locals 1

    .prologue
    .line 971
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setVideoWallOn(Z)V

    .line 972
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    if-eqz v0, :cond_0

    .line 973
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->releaseTransThread()V

    .line 974
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->close()V

    .line 976
    :cond_0
    return-void
.end method

.method private resetViewCloudOption()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1656
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    .line 1657
    .local v0, "pref":Lcom/samsung/everglades/video/myvideo/common/Pref;
    const-string v2, "lastCloudViewOption"

    invoke-virtual {v0, v2, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    .line 1659
    .local v1, "viewOption":I
    if-nez v1, :cond_1

    .line 1666
    :cond_0
    :goto_0
    return-void

    .line 1660
    :cond_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCloudVideoAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isTcloudVideoAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1661
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VideoListFragment - resetViewCloudOption() viewOption = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1663
    const-string v2, "lastCloudViewOption"

    invoke-virtual {v0, v2, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 1664
    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    goto :goto_0
.end method

.method private searchLongClick(ILandroid/view/View;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1292
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1293
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isMultiWindow(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isScaleWindow(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1294
    invoke-virtual {p0, p2, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setOnItemLongClickDrag(Landroid/view/View;Landroid/database/Cursor;)V

    .line 1298
    :goto_0
    return-void

    .line 1296
    :cond_0
    new-instance v1, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-direct {v1, v2, v0, v3}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->showDialog()V

    goto :goto_0
.end method

.method private selectOptionMenu(I)V
    .locals 3
    .param p1, "itemId"    # I

    .prologue
    .line 1810
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 1811
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    if-eqz v1, :cond_0

    .line 1812
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBucketId:I

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setBucketId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setCount(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setCursor(Landroid/database/Cursor;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSLinkDeviceId:I

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setSLinkDeviceId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 1818
    :goto_0
    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 1819
    return-void

    .line 1815
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBucketId:I

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setBucketId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSLinkDeviceId:I

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setSLinkDeviceId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    goto :goto_0
.end method

.method private setActionBarSelectAllLayout(Landroid/view/View;)V
    .locals 2
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 630
    const v0, 0x7f09003e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionBarSelectAllLayout:Landroid/widget/LinearLayout;

    .line 631
    const v0, 0x7f09003f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionBarSelectAllCheckBox:Landroid/widget/CheckBox;

    .line 632
    const v0, 0x7f090040

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectedItemCountText:Landroid/widget/TextView;

    .line 634
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionBarSelectAllLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$3;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$3;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 643
    return-void
.end method

.method private setBezelMgr(Z)V
    .locals 1
    .param p1, "register"    # Z

    .prologue
    .line 1843
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    if-nez v0, :cond_1

    .line 1854
    :cond_0
    :goto_0
    return-void

    .line 1845
    :cond_1
    if-eqz p1, :cond_2

    .line 1847
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isTypical()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->registerListener()V

    goto :goto_0

    .line 1852
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->unregisterListener()V

    goto :goto_0
.end method

.method private setCloudCountChangedListener()V
    .locals 2

    .prologue
    .line 940
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$8;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$8;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->setOnCloudDBCountChanged(Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;)V

    .line 949
    return-void
.end method

.method private setContentCountTextLayout(Landroid/view/View;)V
    .locals 1
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 600
    const v0, 0x7f090046

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListfixedLayout:Landroid/widget/LinearLayout;

    .line 601
    const v0, 0x7f090047

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mContentCountText:Landroid/widget/TextView;

    .line 602
    return-void
.end method

.method private setEmptyView()V
    .locals 3

    .prologue
    .line 793
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 825
    :cond_0
    :goto_0
    return-void

    .line 794
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 796
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 797
    .local v0, "root":Landroid/view/ViewGroup;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mEmptyViewStub:Landroid/view/View;

    if-nez v1, :cond_3

    .line 798
    const v1, 0x7f09004d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mEmptyViewStub:Landroid/view/View;

    .line 799
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mEmptyViewStub:Landroid/view/View;

    instance-of v1, v1, Landroid/view/ViewStub;

    if-eqz v1, :cond_3

    .line 800
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mEmptyViewStub:Landroid/view/View;

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 804
    :cond_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceSearchFileLists()Z

    move-result v1

    if-nez v1, :cond_5

    .line 805
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 806
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setNearbyFileEmptyText(Landroid/view/View;)V

    .line 821
    :goto_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mEmptyViewStub:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 822
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mEmptyViewStub:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->setEmptyView(Landroid/view/View;)V

    goto :goto_0

    .line 808
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setNormalEmptyText(Landroid/view/View;)V

    goto :goto_1

    .line 811
    :cond_5
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSearchKey:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 812
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbySearchFileList()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 813
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setNearbyFileEmptyText(Landroid/view/View;)V

    goto :goto_1

    .line 815
    :cond_6
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setNormalEmptyText(Landroid/view/View;)V

    goto :goto_1

    .line 818
    :cond_7
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSearchEmptyText(Landroid/view/View;)V

    goto :goto_1
.end method

.method private setImageCacheListener()V
    .locals 1

    .prologue
    .line 979
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setVideoListOnUpdatedListener(Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;)V

    .line 990
    return-void
.end method

.method private setImageCacheListener4FolderList()V
    .locals 1

    .prologue
    .line 993
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$11;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$11;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setFolderListUpdatedListener(Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;)V

    .line 1004
    return-void
.end method

.method private setListShown(Z)V
    .locals 5
    .param p1, "shown"    # Z

    .prologue
    const/4 v4, 0x0

    .line 848
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 849
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 852
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 887
    :cond_1
    :goto_0
    return-void

    .line 856
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListShown:Z

    if-ne v0, p1, :cond_3

    .line 857
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setListShown() : show = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 861
    :cond_3
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListShown:Z

    .line 863
    if-eqz p1, :cond_4

    .line 864
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$6;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$6;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 881
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0, v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->setVisibility(I)V

    goto :goto_0

    .line 883
    :cond_4
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 884
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 885
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setNearbyFileEmptyText(Landroid/view/View;)V
    .locals 3
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 840
    const v1, 0x7f090024

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 841
    .local v0, "tv":Landroid/widget/TextView;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 842
    const v1, 0x7f0c008f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 843
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 844
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 845
    return-void
.end method

.method private setNormalEmptyText(Landroid/view/View;)V
    .locals 3
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 828
    const v1, 0x7f090024

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 829
    .local v0, "tv":Landroid/widget/TextView;
    const v1, 0x7f0c008b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 830
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 831
    return-void
.end method

.method private setNumberOfColumns()V
    .locals 2

    .prologue
    .line 768
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    if-eqz v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getColumnsCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setNumColumns(I)V

    .line 771
    :cond_0
    return-void
.end method

.method public static setOnPrivateModeListener(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;)V
    .locals 0
    .param p0, "l"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;

    .prologue
    .line 1984
    sput-object p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnPrivateModeListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;

    .line 1985
    return-void
.end method

.method private setSearchEmptyText(Landroid/view/View;)V
    .locals 3
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 834
    const v1, 0x7f090024

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 835
    .local v0, "tv":Landroid/widget/TextView;
    const v1, 0x7f0c0089

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 836
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 837
    return-void
.end method

.method private setSelectAll(Z)V
    .locals 3
    .param p1, "isSelectAll"    # Z

    .prologue
    .line 1822
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    if-eqz v2, :cond_0

    .line 1823
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->getCount()I

    move-result v0

    .line 1824
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1825
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v2, v1, p1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setItemChecked(IZ)V

    .line 1824
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1828
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->checkChanged(Z)V

    .line 1829
    return-void
.end method

.method private setSelectAllLayout(Landroid/view/View;)V
    .locals 2
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 605
    const v0, 0x7f090048

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    .line 606
    const v0, 0x7f09003f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    .line 608
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$2;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$2;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 627
    return-void
.end method

.method private setSelectAllStateChangeListener()V
    .locals 1

    .prologue
    .line 656
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllStateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->setOnSelectAllCheckedStateUpdateListener(Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;)V

    .line 661
    :goto_0
    return-void

    .line 659
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->setOnSelectAllCheckedStateUpdateListener(Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;)V

    goto :goto_0
.end method

.method private setSwipeable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1863
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/everglades/video/VideoMain;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    if-eqz v0, :cond_0

    .line 1864
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->setSwipeable(Z)V

    .line 1866
    :cond_0
    return-void
.end method

.method private setTabState(ZLandroid/widget/TabWidget;)V
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "tab"    # Landroid/widget/TabWidget;

    .prologue
    .line 1510
    invoke-virtual {p2}, Landroid/widget/TabWidget;->getChildCount()I

    move-result v2

    .line 1511
    .local v2, "tabCount":I
    const/4 v1, 0x0

    .line 1513
    .local v1, "opacity":F
    if-eqz p1, :cond_0

    .line 1514
    const v1, 0x3ecccccd    # 0.4f

    .line 1519
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 1520
    invoke-virtual {p2, v0}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1519
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1516
    .end local v0    # "i":I
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1522
    .restart local v0    # "i":I
    :cond_1
    return-void
.end method

.method private setupAdapter()V
    .locals 7

    .prologue
    .line 906
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    if-nez v0, :cond_0

    .line 907
    const/4 v0, 0x1

    const-string v1, "setupAdapter() : adapter is null"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 908
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getView()Landroid/view/View;

    move-result-object v4

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOrientation:I

    invoke-direct/range {v0 .. v5}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;-><init>(Landroid/content/Context;Lcom/samsung/everglades/video/myvideo/common/ListType;Landroid/database/Cursor;Landroid/view/View;I)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    .line 910
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setCloudCountChangedListener()V

    .line 911
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v0, :cond_1

    .line 912
    new-instance v6, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$7;

    invoke-direct {v6, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$7;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    .line 922
    .local v6, "onSortListener":Lcom/sec/android/touchwiz/animator/TwGridSortAnimator$OnSortListener;
    new-instance v0, Lcom/sec/android/touchwiz/animator/TwGridSortAnimator;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-direct {v0, v1, v6}, Lcom/sec/android/touchwiz/animator/TwGridSortAnimator;-><init>(Lcom/sec/android/touchwiz/widget/TwGridView;Lcom/sec/android/touchwiz/animator/TwGridSortAnimator$OnSortListener;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mGridSortAnimator:Lcom/sec/android/touchwiz/animator/TwGridSortAnimator;

    .line 924
    .end local v6    # "onSortListener":Lcom/sec/android/touchwiz/animator/TwGridSortAnimator$OnSortListener;
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 925
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setListShown(Z)V

    .line 926
    return-void
.end method

.method private setupLiveThumbnail()V
    .locals 2

    .prologue
    .line 952
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$9;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$9;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setUpdatedLiveThumbnailListener(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;)V

    .line 965
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 966
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 968
    :cond_0
    return-void
.end method

.method private setupView(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 554
    const-string v0, "VideoListFragment - setupView() enter"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 555
    const v0, 0x7f04001c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    .line 557
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isFolderListFragment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 558
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 563
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewTreeObserver:Landroid/view/ViewTreeObserver;

    .line 564
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewTreeObserver:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 566
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    const v1, 0x7f09004b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    .line 570
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setContentCountTextLayout(Landroid/view/View;)V

    .line 571
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectAllLayout(Landroid/view/View;)V

    .line 572
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateOrientation()V

    .line 574
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    if-nez v0, :cond_1

    .line 575
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->initListView(Landroid/view/View;)V

    .line 578
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListShown:Z

    .line 579
    const-string v0, "VideoListFragment - setupView() end"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 580
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    return-object v0

    .line 560
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method private updateLiveThumnbnailState()V
    .locals 4

    .prologue
    const/16 v2, 0x1f4

    const/16 v1, 0x190

    .line 459
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 460
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isContentChanged()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 461
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setVideoWallOn(Z)V

    .line 462
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 465
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 471
    :cond_1
    :goto_0
    return-void

    .line 467
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 468
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private updateOrientation()V
    .locals 1

    .prologue
    .line 550
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOrientation:I

    .line 551
    return-void
.end method

.method private declared-synchronized updateSelectAllState(I)V
    .locals 4
    .param p1, "count"    # I

    .prologue
    .line 1612
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_1

    .line 1613
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    .line 1614
    .local v0, "multiSelector":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v1

    if-eq v1, p1, :cond_0

    .line 1615
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VideoListFragment - updateSelectAllState() count :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 1616
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1618
    :cond_0
    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->setTotalItemsCountInList(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1620
    .end local v0    # "multiSelector":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    :cond_1
    monitor-exit p0

    return-void

    .line 1612
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private updateSelectedFolder(Landroid/view/View;Landroid/database/Cursor;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x1

    .line 1086
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOrientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1087
    const-string v3, "bucket_id"

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1088
    .local v1, "selectedBucketId":I
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v3

    const-string v4, "lastFolder"

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoFirstBucketId()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    .line 1090
    .local v0, "lastBucketId":I
    if-ne v0, v1, :cond_1

    .line 1099
    .end local v0    # "lastBucketId":I
    .end local v1    # "selectedBucketId":I
    :cond_0
    :goto_0
    return v2

    .line 1092
    .restart local v0    # "lastBucketId":I
    .restart local v1    # "selectedBucketId":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v3, v4, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateFolderView(Lcom/sec/android/touchwiz/widget/TwGridView;Landroid/view/View;)V

    .line 1094
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnFolderChangedListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;

    if-eqz v3, :cond_0

    .line 1095
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnFolderChangedListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;

    invoke-interface {v3, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;->onFolderChanged(I)V

    goto :goto_0

    .line 1099
    .end local v0    # "lastBucketId":I
    .end local v1    # "selectedBucketId":I
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private updateSelectionSpinner()V
    .locals 11

    .prologue
    const v10, 0x7f0c00e0

    const v9, 0x7f0c00b8

    const/4 v8, 0x1

    .line 1227
    const-string v3, "updateSelectionSpinner()"

    invoke-static {v8, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 1228
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1229
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v1

    .line 1231
    .local v1, "selectedCount":I
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00b9

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1232
    .local v2, "string":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;->clear()V

    .line 1233
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    invoke-virtual {v3, v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;->add(Ljava/lang/Object;)V

    .line 1234
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->isAllItemSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1235
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;->add(Ljava/lang/Object;)V

    .line 1244
    :goto_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1246
    return-void

    .line 1237
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v3

    if-ge v3, v8, :cond_1

    .line 1238
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 1240
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;->add(Ljava/lang/Object;)V

    .line 1241
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private updateVideoCounter()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 439
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mContentCountText:Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 456
    :goto_0
    return-void

    .line 441
    :cond_0
    const/4 v0, 0x0

    .line 442
    .local v0, "countText":Ljava/lang/String;
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    if-ne v1, v3, :cond_2

    .line 443
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 444
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 455
    :goto_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mContentCountText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 446
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 449
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 450
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0018

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 452
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001c

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public cancelSelectionMode()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1404
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectionMode(ZZI)V

    .line 1405
    invoke-virtual {p0, v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->showAnimation(Z)V

    .line 1407
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 1408
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1410
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->resetContentShowType()V

    .line 1411
    return-void
.end method

.method public changeSelectAllState(Z)V
    .locals 4
    .param p1, "selectAll"    # Z

    .prologue
    .line 1623
    if-eqz p1, :cond_0

    .line 1624
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->selectAll(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;Landroid/widget/BaseAdapter;)V

    .line 1628
    :goto_0
    return-void

    .line 1626
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->deSelectAll(Landroid/widget/BaseAdapter;)V

    goto :goto_0
.end method

.method public checkChanged(Z)V
    .locals 3
    .param p1, "isChecked"    # Z

    .prologue
    .line 1644
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 1645
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    if-eqz v1, :cond_0

    .line 1649
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    const v2, 0x7f09003f

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setIsChecked(Z)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setCursor(Landroid/database/Cursor;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setAdapter(Landroid/widget/BaseAdapter;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 1651
    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 1653
    :cond_0
    return-void
.end method

.method public closeMenu()V
    .locals 1

    .prologue
    .line 1857
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 1858
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->close()V

    .line 1860
    :cond_0
    return-void
.end method

.method public deleteCompleted()V
    .locals 2

    .prologue
    .line 1945
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v1, :cond_0

    .line 1946
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isDeletePending:Z

    .line 1948
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAddDeleteGridAnimator:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;

    if-eqz v1, :cond_0

    .line 1949
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAddDeleteGridAnimator:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;

    invoke-virtual {v1}, Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;->deleteFromAdapterCompleted()V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1955
    :cond_0
    :goto_0
    return-void

    .line 1951
    :catch_0
    move-exception v0

    .line 1952
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "NoSuchMethodError deleteFromAdapterCompleted"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disableSelectAllState()V
    .locals 2

    .prologue
    .line 1631
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 1632
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1633
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1635
    :cond_0
    return-void
.end method

.method public getCheckedItemPositions()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1898
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1900
    .local v2, "ia":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->getCount()I

    move-result v3

    .line 1901
    .local v3, "len":I
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 1903
    .local v0, "checked":Landroid/util/SparseBooleanArray;
    if-eqz v0, :cond_1

    .line 1904
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_2

    .line 1905
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 1906
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1904
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1912
    .end local v1    # "i":I
    :cond_1
    const/4 v2, 0x0

    .end local v2    # "ia":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_2
    return-object v2
.end method

.method public getItemPositions(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1916
    .local p1, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1917
    .local v0, "Ia":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_1

    const/4 v0, 0x0

    .line 1928
    .end local v0    # "Ia":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_0
    return-object v0

    .line 1919
    .restart local v0    # "Ia":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1920
    .local v1, "URI":Landroid/net/Uri;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v6}, Lcom/sec/android/touchwiz/widget/TwGridView;->getCount()I

    move-result v4

    .line 1921
    .local v4, "len":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 1922
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v6, v3}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 1923
    .local v2, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-static {v6, v2, v7}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v5

    .line 1924
    .local v5, "mUri":Landroid/net/Uri;
    invoke-virtual {v5, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1925
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1921
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object v0
.end method

.method protected initLoader()V
    .locals 3

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 245
    return-void
.end method

.method public invalidateActionMode()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1147
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionMode:Landroid/view/ActionMode;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mMenu:Landroid/view/Menu;

    if-nez v5, :cond_1

    .line 1182
    :cond_0
    :goto_0
    return-void

    .line 1151
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v2

    .line 1152
    .local v2, "selectedCount":I
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v5}, Lcom/sec/android/touchwiz/widget/TwGridView;->getCount()I

    move-result v0

    .line 1153
    .local v0, "listItemCount":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalidateActionMode() : selectedCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / listItemCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1155
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00b9

    new-array v8, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1156
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0003

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v3, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1157
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinnerAdapter:Lcom/samsung/everglades/video/myvideo/list/SelectionSpinnerAdapter;

    if-eqz v5, :cond_2

    .line 1158
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateSelectionSpinner()V

    .line 1159
    invoke-virtual {p0, v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateSpinnerWidth(I)V

    .line 1164
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateActionBarSelectLayout()V

    .line 1167
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeleteAttr()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1168
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mMenu:Landroid/view/Menu;

    const v6, 0x7f090071

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1169
    .local v1, "mItem":Landroid/view/MenuItem;
    if-eqz v1, :cond_3

    .line 1170
    if-lez v2, :cond_4

    :goto_1
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1181
    .end local v1    # "mItem":Landroid/view/MenuItem;
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mMenu:Landroid/view/Menu;

    invoke-direct {p0, v3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->prepareOptionMenu(Landroid/view/Menu;)V

    goto/16 :goto_0

    .restart local v1    # "mItem":Landroid/view/MenuItem;
    :cond_4
    move v3, v4

    .line 1170
    goto :goto_1

    .line 1173
    .end local v1    # "mItem":Landroid/view/MenuItem;
    :cond_5
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1174
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mMenu:Landroid/view/Menu;

    const v6, 0x7f090088

    if-lez v2, :cond_6

    :goto_3
    invoke-interface {v5, v6, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_2

    :cond_6
    move v3, v4

    goto :goto_3

    .line 1175
    :cond_7
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1176
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mMenu:Landroid/view/Menu;

    const v6, 0x7f090087

    if-lez v2, :cond_8

    :goto_4
    invoke-interface {v5, v6, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_2

    :cond_8
    move v3, v4

    goto :goto_4

    .line 1178
    :cond_9
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mMenu:Landroid/view/Menu;

    const v6, 0x7f09007a

    if-lez v2, :cond_a

    :goto_5
    invoke-interface {v5, v6, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_2

    :cond_a
    move v3, v4

    goto :goto_5
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 1970
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    if-eqz v0, :cond_0

    .line 1971
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v0

    .line 1973
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyChanged(I)V
    .locals 1
    .param p1, "msg"    # I

    .prologue
    .line 1638
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    if-eqz v0, :cond_0

    .line 1639
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->notifyChanged(I)V

    .line 1641
    :cond_0
    return-void
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1760
    const-string v1, "onActionItemClicked"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1767
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1768
    .local v0, "itemId":I
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->selectOptionMenu(I)V

    .line 1770
    const/4 v1, 0x0

    return v1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 231
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 232
    const-string v0, "VideoListFragment - onActivityCreated() enter"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 234
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setHasOptionsMenu(Z)V

    .line 235
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateOrientation()V

    .line 237
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setupAdapter()V

    .line 238
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->initLoader()V

    .line 240
    const-string v0, "VideoListFragment - onActivityCreated() end"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 510
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 512
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOrientation:I

    if-eq v3, v4, :cond_2

    .line 513
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOrientation:I

    .line 515
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 516
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isFolderListFragment()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 517
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070033

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 523
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setNumberOfColumns()V

    .line 524
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setEmptyView()V

    .line 525
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setListView()V

    .line 527
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    if-eqz v3, :cond_1

    .line 528
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOrientation:I

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->setOrientation(I)V

    .line 531
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 532
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 533
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 534
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 536
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 537
    .local v1, "l":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v3, -0x2

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 538
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 540
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v3}, Landroid/view/ActionMode;->getCustomView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f09003b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 541
    .local v0, "actionModeTitle":Landroid/widget/TextView;
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0003

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 545
    .end local v0    # "actionModeTitle":Landroid/widget/TextView;
    .end local v1    # "l":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 546
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->forceStop()V

    .line 547
    return-void

    .line 519
    :cond_3
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRoot:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070032

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 179
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 180
    const-string v0, "VideoListFragment - onCreate() enter"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 181
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->initGlobalVars()V

    .line 182
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->initArgs()V

    .line 183
    const-string v0, "VideoListFragment - onCreate() end"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 7
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f090039

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 1706
    const-string v0, "onCreateActionMode"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1708
    invoke-direct {p0, p2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->createOptionMenu(Landroid/view/Menu;)V

    .line 1709
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeActionbar:Landroid/view/View;

    .line 1710
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeActionbar:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1711
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeActionbar:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 1713
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeActionbar:Landroid/view/View;

    const v1, 0x7f09003d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    .line 1714
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeActionbar:Landroid/view/View;

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeTitle:Landroid/widget/TextView;

    .line 1716
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeActionbar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1719
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    .line 1728
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1729
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1730
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1731
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeActionbar:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setActionBarSelectAllLayout(Landroid/view/View;)V

    .line 1732
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionBarSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1739
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeActionbar:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    .line 1740
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionModeActionbar:Landroid/view/View;

    const v1, 0x7f09003a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1750
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 6
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBucketId:I

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSearchKey:Ljava/lang/String;

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSLinkDeviceId:I

    invoke-direct/range {v0 .. v5}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;-><init>(Landroid/content/Context;Lcom/samsung/everglades/video/myvideo/common/ListType;ILjava/lang/String;I)V

    .line 371
    .local v0, "l":Landroid/content/AsyncTaskLoader;, "Landroid/content/AsyncTaskLoader<Landroid/database/Cursor;>;"
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/content/AsyncTaskLoader;->setUpdateThrottle(J)V

    .line 372
    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 480
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectAttr()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeleteAttr()Z

    move-result v0

    if-nez v0, :cond_0

    .line 481
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->createOptionMenu(Landroid/view/Menu;)V

    .line 483
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 226
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setupView(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 348
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->setOnCloudDBCountChanged(Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;)V

    .line 350
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 353
    :cond_0
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    .line 354
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    .line 355
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    .line 357
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    if-eqz v0, :cond_1

    .line 358
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->destroy()V

    .line 359
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBezelMgr:Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    .line 362
    :cond_1
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->popupCreated:Z

    .line 364
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 365
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    .line 366
    return-void
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 5
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 1775
    const-string v3, "onDestroyActionMode"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1776
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1777
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->cancelSelectionMode()V

    .line 1778
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->setNextFocusUpId(I)V

    .line 1780
    :cond_0
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceSearchFileLists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1781
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v3}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildCount()I

    move-result v1

    .line 1782
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 1783
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v3, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f09002c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1784
    .local v0, "checkBox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_1

    .line 1785
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1782
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1788
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    :cond_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->setEnableDragBlock(Z)V

    .line 1790
    .end local v1    # "childCount":I
    .end local v2    # "i":I
    :cond_3
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 318
    const-string v0, "VideoListFragment - onDestroyView()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 319
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewTreeObserver:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewTreeObserver:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewTreeObserver:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 323
    :cond_0
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    .line 325
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    if-eqz v0, :cond_2

    .line 326
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->setOnScrollListener(Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;)V

    .line 327
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->setOnItemClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;)V

    .line 328
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->setOnItemLongClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;)V

    .line 329
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v0, :cond_1

    .line 330
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->setTwMultiSelectedListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnTwMultiSelectedListener;)V

    .line 332
    :cond_1
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    .line 334
    :cond_2
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mEmptyViewStub:Landroid/view/View;

    .line 335
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    .line 337
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v0, :cond_3

    .line 338
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAddDeleteGridAnimator:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;

    .line 339
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAnimator:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    .line 340
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mGridSortAnimator:Lcom/sec/android/touchwiz/animator/TwGridSortAnimator;

    .line 343
    :cond_3
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 344
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 377
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    if-eqz v2, :cond_0

    .line 378
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->notifyDataSetInvalidated()V

    .line 379
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v2, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 381
    :cond_0
    const-string v2, "VideoListFragment - onLoadFinished() enter"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 383
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isDeletePending:Z

    if-eqz v2, :cond_1

    .line 384
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->deleteCompleted()V

    .line 387
    :cond_1
    if-nez p2, :cond_5

    move v0, v1

    .line 389
    .local v0, "count":I
    :goto_0
    if-nez p2, :cond_6

    .line 390
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    .line 413
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VideoListFragment - onLoadFinished() mLastLoadedCount :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 414
    invoke-direct {p0, v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setListShown(Z)V

    .line 415
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setEmptyView()V

    .line 416
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateLiveThumnbnailState()V

    .line 418
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListfixedLayout:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_3

    .line 419
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceSearchFileLists()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 420
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListfixedLayout:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 426
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudAvailable()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 427
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->setOnlyDropboxContent(I)V

    .line 429
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isKorSKTModel()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 430
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->setOnlyTcloudContent(I)V

    .line 434
    :cond_4
    const-string v1, "VideoListFragment - onLoadFinished() end"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 435
    const-string v1, "VerificationLog"

    const-string v2, "Executed"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    return-void

    .line 387
    .end local v0    # "count":I
    :cond_5
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0

    .line 392
    .restart local v0    # "count":I
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VideoListFragment - onLoadFinished() count :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 393
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectAttr()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeleteAttr()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 394
    :cond_7
    if-lez v0, :cond_9

    .line 395
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    if-eq v0, v2, :cond_8

    .line 396
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateContentChanged(Z)V

    .line 397
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->setTotalItemsCountInList(I)V

    .line 398
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->invalidateActionMode()V

    .line 402
    :goto_3
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateSelectAllState(I)V

    .line 409
    :goto_4
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    .line 410
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateVideoCounter()V

    goto/16 :goto_1

    .line 400
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateContentChanged(Z)V

    goto :goto_3

    .line 404
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->stopActionMode()V

    goto :goto_4

    .line 407
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_4

    .line 422
    :cond_b
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListfixedLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 90
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 475
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 476
    return-void
.end method

.method public onOptionChanged()V
    .locals 4

    .prologue
    .line 1693
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .line 1694
    .local v0, "f":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolderContent()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceFileLists()Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    move-object v1, v0

    .line 1698
    check-cast v1, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .line 1699
    .local v1, "folder":Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFolderListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    .line 1703
    .end local v1    # "folder":Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    :goto_0
    return-void

    .line 1701
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 494
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 495
    .local v2, "itemId":I
    const v3, 0x7f090077

    if-ne v2, v3, :cond_0

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSplitFolderContent()Z

    move-result v3

    if-nez v3, :cond_2

    .line 496
    :cond_0
    invoke-direct {p0, v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->selectOptionMenu(I)V

    .line 505
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    return v3

    .line 498
    :cond_2
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .line 499
    .local v0, "f":Landroid/app/Fragment;
    instance-of v3, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v3, :cond_1

    move-object v1, v0

    .line 500
    check-cast v1, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .line 501
    .local v1, "folder":Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFolderListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v3

    invoke-direct {v3, v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->selectOptionMenu(I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 294
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 296
    const-string v0, "VideoListFragment - onPause()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 301
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->handleNearbyListUpdater(Z)V

    .line 302
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->unregisterCloudDBObserver()V

    .line 303
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->setOnOptionsChangedListener(Lcom/samsung/everglades/video/myvideo/common/Utils$OnOptionsChangedListener;)V

    .line 304
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectAllStateChangeListener()V

    .line 305
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->releaseLiveThumbnail()V

    .line 306
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setVideoListOnUpdatedListener(Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;)V

    .line 307
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setFolderListUpdatedListener(Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;)V

    .line 308
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setUpdatedLiveThumbnailListener(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;)V

    .line 309
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 310
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->dismissFolderHoverViewer()V

    .line 311
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSecretModeStateFilter:Landroid/content/IntentFilter;

    if-eqz v0, :cond_1

    .line 312
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 314
    :cond_1
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1754
    const-string v0, "onPrepareActionMode"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1755
    invoke-direct {p0, p2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->prepareOptionMenu(Landroid/view/Menu;)V

    .line 1756
    const/4 v0, 0x0

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 487
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectAttr()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeleteAttr()Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->prepareOptionMenu(Landroid/view/Menu;)V

    .line 490
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, -0x1

    .line 249
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 250
    const-string v5, "VideoListFragment - onResume() enter"

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 251
    const-string v5, "VerificationLog"

    const-string v6, "onResume"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->resetViewCloudOption()V

    .line 253
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setupLiveThumbnail()V

    .line 254
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->registerCloudDBObserver()V

    .line 255
    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->handleNearbyListUpdater(Z)V

    .line 256
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSplitFolderContent()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 257
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setImageCacheListener4FolderList()V

    .line 261
    :goto_0
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->setOnOptionsChangedListener(Lcom/samsung/everglades/video/myvideo/common/Utils$OnOptionsChangedListener;)V

    .line 262
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectAllStateChangeListener()V

    .line 264
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectAttr()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeleteAttr()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateContentChanged()V

    .line 268
    :cond_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->registerSecretModeStateReceiver()V

    .line 269
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5, v6, v7}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setContextAndListType(Landroid/content/Context;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 271
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 272
    .local v2, "feature":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, "VPAP"

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    const-string v5, "VPVA"

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    const-string v5, "VPSB"

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v3, "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v5

    const-string v6, "autoPlay"

    invoke-virtual {v5, v6, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    const/16 v0, 0x3e8

    .line 278
    .local v0, "autoplay":I
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 283
    .local v1, "extra":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v5

    const-string v6, "sortorder"

    invoke-virtual {v5, v6, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 287
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v2, v3, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 289
    const-string v4, "VideoListFragment - onResume() end"

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 290
    return-void

    .line 259
    .end local v0    # "autoplay":I
    .end local v1    # "extra":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v2    # "feature":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setImageCacheListener()V

    goto/16 :goto_0

    .restart local v2    # "feature":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v3    # "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_3
    move v0, v4

    .line 277
    goto :goto_1
.end method

.method public openSearchList()V
    .locals 1

    .prologue
    .line 1869
    const v0, 0x7f09006e

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->selectOptionMenu(I)V

    .line 1870
    return-void
.end method

.method public resetContentShowType()V
    .locals 4

    .prologue
    .line 1414
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "lastCloudViewOption"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    .line 1415
    .local v0, "optionMenu":I
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewOptionMenu:I

    if-eq v1, v0, :cond_0

    .line 1416
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "optionMenu() : mViewOptionMenu = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewOptionMenu:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", optionMenu = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 1417
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "lastCloudViewOption"

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewOptionMenu:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 1418
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1419
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    .line 1422
    :cond_0
    return-void
.end method

.method public restartLoader()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1593
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1594
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceSearchFileLists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1595
    invoke-direct {p0, v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setListShown(Z)V

    .line 1597
    :cond_0
    const/4 v0, 0x1

    const-string v1, "VideoListFragment : restartLoader()"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 1598
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 1600
    :cond_1
    return-void
.end method

.method public search(Ljava/lang/String;)V
    .locals 1
    .param p1, "searchKey"    # Ljava/lang/String;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSearchKey:Ljava/lang/String;

    .line 170
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->setSearchKey(Ljava/lang/String;)V

    .line 174
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    .line 175
    return-void
.end method

.method public setBucketId(I)V
    .locals 0
    .param p1, "bucketId"    # I

    .prologue
    .line 221
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBucketId:I

    .line 222
    return-void
.end method

.method public setCounterSpinnerText(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    .line 1260
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    const v2, 0x7f09005a

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1261
    .local v0, "tv":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 1262
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00b9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1264
    :cond_0
    return-void
.end method

.method public setDeletePending(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1932
    .local p1, "ia":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v1, :cond_0

    .line 1933
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isDeletePending:Z

    .line 1935
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAddDeleteGridAnimator:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 1936
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAddDeleteGridAnimator:Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;

    invoke-virtual {v1, p1}, Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator;->setDeletePending(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1942
    :cond_0
    :goto_0
    return-void

    .line 1938
    :catch_0
    move-exception v0

    .line 1939
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "NoSuchMethodError setDeletePending"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setGridSort()V
    .locals 2

    .prologue
    .line 1958
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v1, :cond_0

    .line 1960
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mGridSortAnimator:Lcom/sec/android/touchwiz/animator/TwGridSortAnimator;

    if-eqz v1, :cond_0

    .line 1961
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mGridSortAnimator:Lcom/sec/android/touchwiz/animator/TwGridSortAnimator;

    invoke-virtual {v1}, Lcom/sec/android/touchwiz/animator/TwGridSortAnimator;->sortTheGrid()V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1967
    :cond_0
    :goto_0
    return-void

    .line 1963
    :catch_0
    move-exception v0

    .line 1964
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "NoSuchMethodError setGridSort"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 0
    .param p1, "lt"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 214
    return-void
.end method

.method public setListView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 745
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    if-nez v2, :cond_1

    .line 765
    :cond_0
    :goto_0
    return-void

    .line 747
    :cond_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 748
    const/4 v1, 0x0

    .line 749
    .local v1, "left":I
    const/4 v0, 0x0

    .line 750
    .local v0, "bottom":I
    const-string v2, "setListView() Set margin for grid view"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 751
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOrientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 752
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080087

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 753
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080086

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 759
    :goto_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    const/high16 v3, 0x2000000

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwGridView;->setScrollBarStyle(I)V

    .line 760
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v2, v1, v4, v1, v0}, Lcom/sec/android/touchwiz/widget/TwGridView;->setPaddingRelative(IIII)V

    goto :goto_0

    .line 755
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080084

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 756
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080085

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_1

    .line 761
    .end local v0    # "bottom":I
    .end local v1    # "left":I
    :cond_3
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isJustList()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 762
    :cond_4
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v2, v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->setScrollBarStyle(I)V

    .line 763
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v2, v4, v4, v4, v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->setPaddingRelative(IIII)V

    goto :goto_0
.end method

.method public setOnFolderChangedListener(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;

    .prologue
    .line 1082
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnFolderChangedListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;

    .line 1083
    return-void
.end method

.method public setOnItemLongClickDrag(Landroid/view/View;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x0

    .line 1316
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->get()Ljava/util/ArrayList;

    move-result-object v0

    .line 1317
    .local v0, "arraylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_2

    .line 1318
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 1319
    .local v5, "startUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "selectedUri"

    invoke-static {v8, v9, v5}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v1

    .line 1321
    .local v1, "dragData":Landroid/content/ClipData;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v8

    if-lez v8, :cond_2

    .line 1322
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 1323
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    .line 1324
    .local v7, "uri":Landroid/net/Uri;
    if-eqz v7, :cond_0

    .line 1325
    new-instance v8, Landroid/content/ClipData$Item;

    invoke-direct {v8, v7}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v1, v8}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    .line 1322
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1329
    .end local v7    # "uri":Landroid/net/Uri;
    :cond_1
    const v8, 0x7f090054

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1330
    .local v6, "tv":Landroid/widget/TextView;
    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 1331
    .local v3, "mStr":Ljava/lang/String;
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1332
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1334
    new-instance v4, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v4, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 1335
    .local v4, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    invoke-virtual {p1, v1, v4, p1, v10}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 1336
    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1339
    .end local v1    # "dragData":Landroid/content/ClipData;
    .end local v2    # "i":I
    .end local v3    # "mStr":Ljava/lang/String;
    .end local v4    # "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    .end local v5    # "startUri":Landroid/net/Uri;
    .end local v6    # "tv":Landroid/widget/TextView;
    :cond_2
    return-void
.end method

.method public setOnItemLongClickDrag(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x0

    .line 1301
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/String;

    const-string v6, "text/uri-list"

    aput-object v6, v5, v8

    .line 1304
    .local v5, "strs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-static {v6, p2, v7}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v3

    .line 1305
    .local v3, "mUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v2

    .line 1306
    .local v2, "mDB":Lcom/samsung/everglades/video/myvideo/common/DB;
    new-instance v1, Landroid/content/ClipData$Item;

    invoke-direct {v1, v3}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    .line 1307
    .local v1, "item":Landroid/content/ClipData$Item;
    new-instance v0, Landroid/content/ClipData;

    const-string v6, "selectedUri"

    invoke-direct {v0, v6, v5, v1}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    .line 1309
    .local v0, "dragData":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v6

    if-lez v6, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->isLocalContent(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1310
    new-instance v4, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v4, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 1311
    .local v4, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    invoke-virtual {p1, v0, v4, p1, v8}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 1313
    .end local v4    # "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    :cond_0
    return-void
.end method

.method public setRenamedFileInfo(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 5
    .param p1, "newFilePath"    # Ljava/lang/String;
    .param p2, "oldFileName"    # Ljava/lang/String;
    .param p3, "oldId"    # J

    .prologue
    const/4 v3, 0x1

    .line 1833
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    .line 1835
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1836
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    aput-object p2, v0, v3

    .line 1837
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 1839
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->setContentChanged(Z)V

    .line 1840
    return-void
.end method

.method public setSelectContentViewMode(Z)V
    .locals 4
    .param p1, "isReset"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1358
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1359
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->setSelectionMode()V

    .line 1361
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "lastCloudViewOption"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewOptionMenu:I

    .line 1362
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, p1, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectionMode(ZZI)V

    .line 1364
    sput v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mSelectMode:I

    .line 1365
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->put(Landroid/net/Uri;Ljava/lang/String;)Z

    .line 1366
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->invalidateActionMode()V

    .line 1368
    :cond_0
    return-void
.end method

.method public setSelectionMode(ZZI)V
    .locals 8
    .param p1, "enable"    # Z
    .param p2, "isReset"    # Z
    .param p3, "position"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1425
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    if-nez v6, :cond_1

    .line 1507
    :cond_0
    :goto_0
    return-void

    .line 1426
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x1020013

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TabWidget;

    .line 1428
    .local v3, "tab":Landroid/widget/TabWidget;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v6}, Lcom/sec/android/touchwiz/widget/TwGridView;->clearChoices()V

    .line 1430
    if-eqz p1, :cond_6

    .line 1431
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1433
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    if-eqz v6, :cond_2

    .line 1434
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1437
    :cond_2
    invoke-virtual {p0, v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->showAnimation(Z)V

    .line 1439
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionMode:Landroid/view/ActionMode;

    .line 1440
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/sec/android/touchwiz/widget/TwGridView;->setChoiceMode(I)V

    .line 1443
    const/4 v6, -0x1

    if-eq p3, v6, :cond_3

    .line 1444
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v6, p3, v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->setItemChecked(IZ)V

    .line 1448
    :cond_3
    if-eqz p2, :cond_5

    .line 1449
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getAfterReset(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    .line 1453
    .local v2, "multiSelector":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    :goto_1
    iget v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLastLoadedCount:I

    invoke-virtual {v2, v6}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->setTotalItemsCountInList(I)V

    .line 1455
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->checkEditMenu(Landroid/content/Context;)V

    .line 1457
    if-eqz v3, :cond_4

    .line 1459
    invoke-virtual {v3, v5}, Landroid/widget/TabWidget;->setEnabled(Z)V

    .line 1460
    invoke-direct {p0, v4, v3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setTabState(ZLandroid/widget/TabWidget;)V

    .line 1501
    .end local v2    # "multiSelector":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    :cond_4
    :goto_2
    if-nez p1, :cond_8

    :goto_3
    invoke-direct {p0, v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSwipeable(Z)V

    .line 1502
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setBezelMgr(Z)V

    .line 1503
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    if-eqz v4, :cond_0

    .line 1504
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v4, v5}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 1505
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 1451
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    .restart local v2    # "multiSelector":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    goto :goto_1

    .line 1468
    .end local v2    # "multiSelector":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    :cond_6
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1469
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v6, v5}, Lcom/sec/android/touchwiz/widget/TwGridView;->setChoiceMode(I)V

    .line 1470
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->destroyInstance()V

    .line 1471
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/common/ListType;->setNormalMode()V

    .line 1472
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 1478
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .line 1479
    .local v0, "f":Landroid/app/Fragment;
    instance-of v6, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v6, :cond_7

    move-object v1, v0

    .line 1480
    check-cast v1, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .line 1481
    .local v1, "folder":Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->isSelectionMode()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1483
    invoke-virtual {v3, v4}, Landroid/widget/TabWidget;->setEnabled(Z)V

    .line 1484
    invoke-direct {p0, v5, v3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setTabState(ZLandroid/widget/TabWidget;)V

    goto :goto_2

    .line 1490
    .end local v1    # "folder":Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    :cond_7
    if-eqz v3, :cond_4

    .line 1492
    invoke-virtual {v3, v4}, Landroid/widget/TabWidget;->setEnabled(Z)V

    .line 1493
    invoke-direct {p0, v5, v3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setTabState(ZLandroid/widget/TabWidget;)V

    goto :goto_2

    .end local v0    # "f":Landroid/app/Fragment;
    :cond_8
    move v4, v5

    .line 1501
    goto :goto_3
.end method

.method public setSelectionModeforDelete()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1384
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    if-eqz v0, :cond_2

    .line 1385
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "lastCloudViewOption"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewOptionMenu:I

    .line 1386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setSelectionModeforDelete : viewType = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewOptionMenu:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1388
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mViewOptionMenu:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCloudVideoAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isTcloudVideoAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1389
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "lastCloudViewOption"

    invoke-virtual {v0, v1, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 1390
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    .line 1393
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->setSelectionModeForDelete()V

    .line 1395
    const/4 v0, -0x1

    invoke-virtual {p0, v3, v3, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectionMode(ZZI)V

    .line 1397
    sput v3, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mSelectMode:I

    .line 1398
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->put(Landroid/net/Uri;Ljava/lang/String;)Z

    .line 1399
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->invalidateActionMode()V

    .line 1401
    :cond_2
    return-void
.end method

.method public showAnimation(Z)V
    .locals 1
    .param p1, "isInEditMode"    # Z

    .prologue
    .line 1371
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-nez v0, :cond_1

    .line 1381
    :cond_0
    :goto_0
    return-void

    .line 1373
    :cond_1
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mIsInEditMode:Z

    if-nez v0, :cond_2

    .line 1374
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mIsInEditMode:Z

    .line 1375
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAnimator:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->ShowCheckBox()V

    .line 1377
    :cond_2
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mIsInEditMode:Z

    if-eqz v0, :cond_0

    .line 1378
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mIsInEditMode:Z

    .line 1379
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectAnimator:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->hideCheckBox()V

    goto :goto_0
.end method

.method public stopActionMode()V
    .locals 1

    .prologue
    .line 1139
    const-string v0, "stopActionMode"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1141
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 1142
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1144
    :cond_0
    return-void
.end method

.method public updateActionBarSelectLayout()V
    .locals 9

    .prologue
    .line 1249
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1250
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v2

    .line 1251
    .local v2, "selectedCount":I
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->isAllItemSelected()Z

    move-result v1

    .line 1253
    .local v1, "isAllItemSelected":Z
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mActionBarSelectAllCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1255
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectedItemCountText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00b9

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1256
    return-void
.end method

.method public updateSpinnerWidth(I)V
    .locals 5
    .param p1, "count"    # I

    .prologue
    .line 1269
    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->num:I

    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getNumofDigit(I)I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 1270
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getNumofDigit(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->num:I

    .line 1271
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1272
    .local v0, "lParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080010

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 1273
    .local v2, "width":I
    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->num:I

    mul-int/lit8 v3, v3, 0x2c

    add-int/2addr v2, v3

    .line 1274
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1275
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1276
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1277
    add-int/lit8 v1, v2, -0xa

    .line 1278
    .local v1, "w":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSelectionSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setDropDownWidth(I)V

    .line 1280
    .end local v0    # "lParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "w":I
    .end local v2    # "width":I
    :cond_0
    return-void
.end method
