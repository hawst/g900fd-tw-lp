.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$1;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 1

    .prologue
    .line 590
    const-string v0, "VideoListFragment - onGlobalLayout()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 591
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isMultiWindow(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v0

    if-nez v0, :cond_0

    .line 592
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 593
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->forceStop()V

    .line 595
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setNumberOfColumns()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$100(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V

    .line 596
    return-void
.end method
