.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;
.super Landroid/os/Handler;
.source "VideoListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 1524
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v8, 0x190

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1526
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "VideoListFragment : handleMessage("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 1527
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isResumed()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1528
    iget v4, p1, Landroid/os/Message;->what:I

    sparse-switch v4, :sswitch_data_0

    .line 1589
    :cond_0
    :goto_0
    return-void

    .line 1530
    :sswitch_0
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListShown:Z
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1600(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$900(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$900(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 1534
    :sswitch_1
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    goto :goto_0

    .line 1538
    :sswitch_2
    const-string v4, "HANDLE_START_LIVE_THUMB"

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1539
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x12c

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1540
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1541
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->open()V

    .line 1542
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setVideoWallOn(Z)V

    .line 1543
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->createTransThread(Z)V

    goto :goto_0

    .line 1548
    :sswitch_3
    const-string v4, "HANDLE_RESTART_LIVE_THUMB"

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1549
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x1f4

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1550
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1551
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1552
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->createTransThread(Z)V

    goto/16 :goto_0

    .line 1554
    :cond_1
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->releaseTransThread()V

    goto/16 :goto_0

    .line 1560
    :sswitch_4
    const-string v4, "HANDLE_RENAME_LIVE_THUMB"

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1561
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1563
    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/common/Utils;->setContentChanged(Z)V

    .line 1565
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 1567
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    aget-object v4, v4, v7

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1568
    .local v2, "old_vid":J
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    aget-object v1, v4, v6

    check-cast v1, Ljava/lang/String;

    .line 1569
    .local v1, "oldFileName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    const/4 v5, 0x2

    aget-object v0, v4, v5

    check-cast v0, Ljava/lang/String;

    .line 1571
    .local v0, "newFilePath":Ljava/lang/String;
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 1573
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1574
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v1, v0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->renameLiveThumbnailFile(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1575
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1576
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x64

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1578
    :cond_2
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mRenamedFileInfoBundle:[Ljava/lang/Object;

    goto/16 :goto_0

    .line 1580
    :cond_3
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$16;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v4

    const-wide/16 v6, 0x7d0

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1528
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_4
        0x1f4 -> :sswitch_3
    .end sparse-switch
.end method
