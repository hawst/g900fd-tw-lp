.class public Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
.super Ljava/lang/Object;
.source "ASFUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/common/ASFUtil$4;,
        Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;,
        Lcom/samsung/everglades/video/myvideo/common/ASFUtil$NotificationHandler;
    }
.end annotation


# static fields
.field public static final ALLSHARE_VIDEO_DATE:Ljava/lang/String; = "date_modified"

.field public static final ALLSHARE_VIDEO_DEVICE:Ljava/lang/String; = "device"

.field public static final ALLSHARE_VIDEO_DURATION:Ljava/lang/String; = "duration"

.field public static final ALLSHARE_VIDEO_EXTENSION:Ljava/lang/String; = "extension"

.field public static final ALLSHARE_VIDEO_ID:Ljava/lang/String; = "_id"

.field public static final ALLSHARE_VIDEO_ITEM_SEED:Ljava/lang/String; = "seed"

.field public static final ALLSHARE_VIDEO_MIME_TYPE:Ljava/lang/String; = "mime_type"

.field public static final ALLSHARE_VIDEO_RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final ALLSHARE_VIDEO_SEEKMODE:Ljava/lang/String; = "seekmode"

.field public static final ALLSHARE_VIDEO_SIZE:Ljava/lang/String; = "_size"

.field public static final ALLSHARE_VIDEO_THUMBNAIL_DATA:Ljava/lang/String; = "thumbnail"

.field public static final ALLSHARE_VIDEO_TITLE:Ljava/lang/String; = "title"

.field public static final ALLSHARE_VIDEO_URI_DATA:Ljava/lang/String; = "_data"

.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.videoplayer.provider"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final SCHEME:Ljava/lang/String; = "content://"

.field public static final VIDEO_ALLSHARE_CANCEL_GET_REMOTE_FILES:I = 0xe

.field public static final VIDEO_ALLSHARE_CLOSE_PROCESSING_DIALOG:I = 0x10

.field public static final VIDEO_ALLSHARE_COMPLETED_DOWNLOAD_SUBTITLE:I = 0xa

.field public static final VIDEO_ALLSHARE_COMPLETED_GET_REMOTE_FILES:I = 0xc

.field public static final VIDEO_ALLSHARE_DEVICE_CHANGED:I = 0x3

.field public static final VIDEO_ALLSHARE_DISCONNECT:I = 0x6

.field public static final VIDEO_ALLSHARE_DISCONNECTED_SELECTED_REMOTE_DEVICE:I = 0xf

.field public static final VIDEO_ALLSHARE_ERROR_GET_REMOTE_FILES:I = 0xd

.field public static final VIDEO_ALLSHARE_ERROR_STATE:I = 0x7

.field public static final VIDEO_ALLSHARE_FINISH_CONNECT:I = 0x5

.field public static final VIDEO_ALLSHARE_NOTIFICATION:I = 0x0

.field public static final VIDEO_ALLSHARE_REMOTE_DEVICE_CHANGED:I = 0x9

.field public static final VIDEO_ALLSHARE_START_CONNECT:I = 0x4

.field public static final VIDEO_ALLSHARE_START_GET_REMOTE_FILES:I = 0xb

.field public static final VIDEO_ALLSHARE_START_SEEK:I = 0x8

.field public static final VIDEO_ALLSHARE_STATE_CHANGED:I = 0x1

.field public static final VIDEO_ALLSHARE_VOLUME_CHANGED:I = 0x2

.field private static mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;


# instance fields
.field public final DRM_PROTECTED_ERROR:I

.field public final DRM_PROTECTED_FALSE:I

.field public final DRM_PROTECTED_TRUE:I

.field private mAllShareDBIndex:I

.field private mApplicationContext:Landroid/content/Context;

.field private mContextForUI:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

.field private final mFlatBrowseResponseListener:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

.field private mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

.field private mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

.field private mNotificationHandler:Lcom/samsung/everglades/video/myvideo/common/ASFUtil$NotificationHandler;

.field private mObservers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final mRemoteDeviceDiscoveryListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

.field private mRemoteDeviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mRemoteDeviceServiceConnectListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

.field private mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

.field private mSelectedDeviceProvider:Lcom/samsung/android/allshare/media/Provider;

.field private mSelectedRemoteDevice:Lcom/samsung/android/allshare/Device;

.field private mUseASF:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 100
    const-string v0, "content://com.sec.android.app.videoplayer.provider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 47
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    .line 48
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mContextForUI:Landroid/content/Context;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    .line 50
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

    .line 51
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    .line 53
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedRemoteDevice:Lcom/samsung/android/allshare/Device;

    .line 54
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedDeviceProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 55
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    .line 56
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    .line 57
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    .line 59
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->DRM_PROTECTED_ERROR:I

    .line 62
    iput v2, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->DRM_PROTECTED_FALSE:I

    .line 63
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->DRM_PROTECTED_TRUE:I

    .line 370
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;-><init>(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceServiceConnectListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    .line 407
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;-><init>(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceDiscoveryListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 440
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;-><init>(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatBrowseResponseListener:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    .line 602
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$NotificationHandler;

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$NotificationHandler;-><init>(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mNotificationHandler:Lcom/samsung/everglades/video/myvideo/common/ASFUtil$NotificationHandler;

    .line 624
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mObservers:Ljava/util/List;

    .line 122
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    .line 123
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 124
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/ServiceProvider;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Lcom/samsung/android/allshare/ServiceProvider;)Lcom/samsung/android/allshare/ServiceProvider;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .param p1, "x1"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->handleNotification(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/DeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Lcom/samsung/android/allshare/DeviceFinder;)Lcom/samsung/android/allshare/DeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .param p1, "x1"    # Lcom/samsung/android/allshare/DeviceFinder;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceDiscoveryListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->updateRemoteDeviceList()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/Device;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedRemoteDevice:Lcom/samsung/android/allshare/Device;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/extension/UniqueItemArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Lcom/samsung/android/allshare/extension/UniqueItemArray;)Lcom/samsung/android/allshare/extension/UniqueItemArray;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .param p1, "x1"    # Lcom/samsung/android/allshare/extension/UniqueItemArray;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->updateRemoteFilesDB()V

    return-void
.end method

.method static synthetic access$902(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mAllShareDBIndex:I

    return p1
.end method

.method private checkAvailableStorage(J)Z
    .locals 5
    .param p1, "contentSize"    # J

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getAvailableStorage()J

    move-result-wide v0

    .line 246
    .local v0, "availableSize":J
    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    .line 247
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ASFUtil - checkAvailableStorage() contents size to download is bigger than available size. availableSize :  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 248
    const/4 v2, 0x0

    .line 251
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static destroyInstance()V
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-eqz v0, :cond_0

    .line 113
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 115
    :cond_0
    return-void
.end method

.method private download(Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "selectedFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v3, 0x0

    const/4 v13, 0x1

    .line 285
    const-string v0, "downloadRemoteFiles(Uri)"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 287
    if-nez p1, :cond_0

    .line 288
    const-string v0, "download. selectedFiles is null"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 346
    :goto_0
    return-void

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    if-nez v0, :cond_1

    .line 293
    const-string v0, "downLoadRemoteFiles. downLoadRemoteFiles is null"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 297
    :cond_1
    new-array v2, v13, [Ljava/lang/String;

    const-string v0, "seed"

    aput-object v0, v2, v3

    .line 301
    .local v2, "cols":[Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 302
    .local v9, "fileCnt":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadRemoteFiles. fileCnt : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 303
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v11, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-ge v10, v9, :cond_6

    .line 307
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 308
    .local v1, "tmpUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 309
    .local v6, "ItemSeed":Ljava/lang/String;
    const/4 v7, 0x0

    .line 310
    .local v7, "c":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 311
    .local v12, "tmpItem":Lcom/samsung/android/allshare/Item;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadRemoteFiles. selectedfile uri  : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v13, v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 314
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 316
    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 317
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 318
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 319
    const/4 v0, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadRemoteFiles. ItemSeed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    :goto_2
    if-eqz v7, :cond_2

    .line 327
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 331
    :cond_2
    :goto_3
    if-eqz v6, :cond_3

    .line 332
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/ItemExtractor;->create(Ljava/lang/String;)Lcom/samsung/android/allshare/Item;

    move-result-object v12

    .line 333
    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 321
    :cond_4
    :try_start_1
    const-string v0, "downloadRemoteFiles. c is null."

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 323
    :catch_0
    move-exception v8

    .line 324
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRemoteItemSeed() "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 326
    if-eqz v7, :cond_2

    .line 327
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 326
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_5

    .line 327
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 339
    .end local v1    # "tmpUri":Landroid/net/Uri;
    .end local v6    # "ItemSeed":Ljava/lang/String;
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v12    # "tmpItem":Lcom/samsung/android/allshare/Item;
    :cond_6
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ServiceProvider;->getDownloader()Lcom/samsung/android/allshare/extension/SECDownloader;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    .line 341
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    if-eqz v0, :cond_7

    .line 342
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSecDownloadManager:Lcom/samsung/android/allshare/extension/SECDownloader;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedRemoteDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v11}, Lcom/samsung/android/allshare/extension/SECDownloader;->Download(Ljava/lang/String;Ljava/util/ArrayList;)Z

    goto/16 :goto_0

    .line 344
    :cond_7
    const-string v0, "downLoadRemoteFiles. mSecDownloadManager is null"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private getAvailableStorage()J
    .locals 10

    .prologue
    const-wide/16 v6, 0x0

    .line 256
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "NearbyDownloadTo"

    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 257
    .local v5, "storageDirectory":Ljava/lang/String;
    if-nez v5, :cond_0

    move-wide v0, v6

    .line 280
    :goto_0
    return-wide v0

    .line 261
    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 262
    .local v3, "folder":Ljava/io/File;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    .line 263
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 267
    :cond_1
    :try_start_0
    new-instance v4, Landroid/os/StatFs;

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 268
    .local v4, "stat":Landroid/os/StatFs;
    const-wide/16 v0, 0x0

    .line 269
    .local v0, "avaliableSize":J
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x12

    if-ge v8, v9, :cond_2

    .line 270
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v6, v6

    mul-long v0, v8, v6

    goto :goto_0

    .line 272
    :cond_2
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v8

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSizeLong()J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    mul-long v0, v8, v6

    goto :goto_0

    .line 275
    .end local v0    # "avaliableSize":J
    .end local v4    # "stat":Landroid/os/StatFs;
    :catch_0
    move-exception v2

    .line 279
    .local v2, "ex":Ljava/lang/RuntimeException;
    const-string v8, "ASFUtil - getAvailableStorage() exception. return 0"

    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    move-wide v0, v6

    .line 280
    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-nez v0, :cond_0

    .line 104
    const-string v0, "ASFUtil : create instance"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 108
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleNotification(II)V
    .locals 4
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 611
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleNotification key = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 612
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mObservers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 614
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 615
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;

    invoke-interface {v1, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;->OnAllShareStateChanged(II)V

    goto :goto_0

    .line 617
    :cond_0
    return-void
.end method

.method private updateRemoteDeviceList()V
    .locals 3

    .prologue
    .line 428
    const-string v1, "updateRemoteDeviceList"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 430
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 432
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    if-eqz v1, :cond_0

    .line 433
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/DeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/allshare/extension/DeviceChecker;->getDeviceCheckedList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 434
    .local v0, "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 437
    .end local v0    # "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    :cond_0
    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->notifySettingChanged(II)V

    .line 438
    return-void
.end method

.method private updateRemoteFile([Landroid/content/ContentValues;)I
    .locals 4
    .param p1, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 587
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateRemoteFile E. values size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 588
    const/4 v0, 0x0

    .line 590
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 591
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    .line 594
    :cond_0
    return v0
.end method

.method private updateRemoteFilesDB()V
    .locals 24

    .prologue
    .line 484
    const-string v20, "updateRemoteFilesDB"

    invoke-static/range {v20 .. v20}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->size()I

    move-result v16

    .line 487
    .local v16, "totalCnt":I
    const/16 v20, 0x1

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/extension/FlatProvider;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " : searchResponseListener un-registerd. mUniqueItemArray.size() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 489
    move/from16 v0, v16

    new-array v0, v0, [Landroid/content/ContentValues;

    move-object/from16 v18, v0

    .line 490
    .local v18, "values":[Landroid/content/ContentValues;
    const/4 v2, 0x0

    .line 491
    .local v2, "_id":I
    const/4 v3, 0x0

    .line 493
    .local v3, "count":I
    const/16 v19, 0x0

    .line 494
    .local v19, "videoItem":Lcom/samsung/android/allshare/Item;
    const/16 v17, 0x0

    .line 495
    .local v17, "uri":Ljava/lang/String;
    const-wide/16 v6, -0x1

    .line 496
    .local v6, "duration":J
    const/4 v15, 0x0

    .line 497
    .local v15, "thumbnailUri":Landroid/net/Uri;
    const/4 v13, 0x0

    .line 498
    .local v13, "seed":Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    const/4 v11, 0x0

    .line 499
    .local v11, "itemSeed":Ljava/lang/String;
    const/4 v14, 0x0

    .line 500
    .local v14, "thumbnailData":Ljava/lang/String;
    const/4 v4, 0x0

    .line 502
    .local v4, "date":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mAllShareDBIndex:I

    .local v10, "i":I
    :goto_0
    move/from16 v0, v16

    if-ge v10, v0, :cond_5

    .line 503
    const/16 v19, 0x0

    .line 504
    const/16 v17, 0x0

    .line 505
    const-wide/16 v6, -0x1

    .line 506
    const/4 v15, 0x0

    .line 507
    const/4 v13, 0x0

    .line 508
    const/4 v11, 0x0

    .line 509
    const/4 v14, 0x0

    .line 510
    const/4 v4, 0x0

    .line 512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->get(I)Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "videoItem":Lcom/samsung/android/allshare/Item;
    check-cast v19, Lcom/samsung/android/allshare/Item;

    .line 514
    .restart local v19    # "videoItem":Lcom/samsung/android/allshare/Item;
    if-eqz v19, :cond_2

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v20

    if-eqz v20, :cond_2

    .line 515
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v17

    .line 523
    if-eqz v17, :cond_4

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_4

    .line 524
    add-int/lit8 v2, v10, 0x1

    .line 525
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getDuration()J

    move-result-wide v20

    const-wide/16 v22, 0x3e8

    mul-long v6, v20, v22

    .line 526
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v15

    .line 527
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/allshare/extension/ItemExtractor;->extract(Lcom/samsung/android/allshare/Item;)Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;

    move-result-object v13

    .line 529
    if-eqz v13, :cond_0

    .line 530
    invoke-virtual {v13}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getSeedString()Ljava/lang/String;

    move-result-object v11

    .line 533
    :cond_0
    if-nez v15, :cond_3

    .line 534
    const-string v14, "NULL"

    .line 539
    :goto_1
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 540
    .local v12, "newValue":Landroid/content/ContentValues;
    const-string v20, "_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 541
    const-string v20, "title"

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    const-string v20, "device"

    const-string v21, "UnKnown"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const-string v20, "duration"

    long-to-int v0, v6

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 544
    const-string v20, "_size"

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 545
    const-string v20, "mime_type"

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    const-string v20, "thumbnail"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    const-string v20, "resolution"

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getResolution()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const-string v20, "_data"

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    const-string v20, "seed"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    const-string v20, "extension"

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getExtension()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getSeekMode()Lcom/samsung/android/allshare/Item$SeekMode;

    move-result-object v20

    if-eqz v20, :cond_1

    .line 552
    const-string v20, "seekmode"

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getSeekMode()Lcom/samsung/android/allshare/Item$SeekMode;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/Item$SeekMode;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    :cond_1
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getDate()Ljava/util/Date;

    move-result-object v9

    .line 557
    .local v9, "fileDate":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v5

    .line 558
    .local v5, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v5, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 559
    const-string v20, "date_modified"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 564
    .end local v5    # "dateFormat":Ljava/text/DateFormat;
    .end local v9    # "fileDate":Ljava/util/Date;
    :goto_2
    const/16 v20, 0x1

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "updateRemoteFilesDB. insert DB . Title : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", newDuration : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", size : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", mime type : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", resolution : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getResolution()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", extension : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getExtension()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", videoItem.getURI() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", videoItem.getThumbnail() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", count : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 568
    aput-object v12, v18, v3

    .line 576
    add-int/lit8 v3, v3, 0x1

    .line 502
    .end local v12    # "newValue":Landroid/content/ContentValues;
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 517
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->remove(I)Ljava/lang/Object;

    .line 518
    add-int/lit8 v10, v10, -0x1

    .line 519
    add-int/lit8 v16, v16, -0x1

    .line 520
    goto :goto_3

    .line 536
    :cond_3
    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_1

    .line 560
    .restart local v12    # "newValue":Landroid/content/ContentValues;
    :catch_0
    move-exception v8

    .line 561
    .local v8, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v8}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_2

    .line 570
    .end local v8    # "e":Ljava/lang/NullPointerException;
    .end local v12    # "newValue":Landroid/content/ContentValues;
    :cond_4
    const/16 v20, 0x1

    const-string v21, "updateRemoteFilesDB. Video uri is NULL"

    invoke-static/range {v20 .. v21}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->remove(I)Ljava/lang/Object;

    .line 572
    add-int/lit8 v10, v10, -0x1

    .line 573
    add-int/lit8 v16, v16, -0x1

    .line 574
    goto :goto_3

    .line 579
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->size()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mAllShareDBIndex:I

    .line 581
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->updateRemoteFile([Landroid/content/ContentValues;)I

    move-result v20

    if-gtz v20, :cond_6

    .line 582
    const/16 v20, 0x1

    const-string v21, "updateRemoteFilesDB. fail to update DB"

    invoke-static/range {v20 .. v21}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 584
    :cond_6
    return-void
.end method


# virtual methods
.method public clearObserver()V
    .locals 2

    .prologue
    .line 634
    const/4 v0, 0x1

    const-string v1, "clearObserver"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 635
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mObservers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 636
    return-void
.end method

.method public createASFRemoteServiceProvider()V
    .locals 3

    .prologue
    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "createASFRemoteServiceProvider() : mUseASF = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 128
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceServiceConnectListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    const-string v2, "com.samsung.android.allshare.media"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    .line 131
    :cond_0
    return-void
.end method

.method public destroyASFRemoteServiceProvider()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 134
    const-string v0, "destroyASFRemoteServiceProvider"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 135
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z

    .line 137
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 139
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

    invoke-virtual {v0, v1, v1}, Lcom/samsung/android/allshare/DeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 141
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 146
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    .line 148
    :cond_1
    return-void
.end method

.method public downloadRemoteFiles(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v9, 0x0

    .line 214
    if-nez p1, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    const-wide/16 v2, 0x0

    .line 220
    .local v2, "downloadContentSize":J
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 223
    .local v1, "downloadContentCnt":I
    const/4 v5, 0x1

    if-lt v1, v5, :cond_0

    .line 227
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v1, :cond_2

    .line 228
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    invoke-virtual {v8, v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getSize(Landroid/net/Uri;)J

    move-result-wide v6

    .line 229
    .local v6, "size":J
    add-long/2addr v2, v6

    .line 227
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 232
    .end local v6    # "size":J
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mContextForUI:Landroid/content/Context;

    .line 233
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    .line 235
    :cond_3
    invoke-direct {p0, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->checkAvailableStorage(J)Z

    move-result v5

    if-nez v5, :cond_4

    .line 236
    const v5, 0x7f0c0042

    invoke-static {v0, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 238
    :cond_4
    const v5, 0x7f0c00c4

    invoke-static {v0, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 239
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->download(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public getRemoteDeviceFiles(Z)V
    .locals 3
    .param p1, "mode"    # Z

    .prologue
    const/4 v2, 0x0

    .line 349
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedDeviceProvider:Lcom/samsung/android/allshare/media/Provider;

    if-nez v0, :cond_0

    .line 361
    :goto_0
    return-void

    .line 351
    :cond_0
    if-eqz p1, :cond_2

    .line 352
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    if-nez v0, :cond_1

    .line 353
    new-instance v0, Lcom/samsung/android/allshare/extension/FlatProvider;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedDeviceProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/extension/FlatProvider;-><init>(Lcom/samsung/android/allshare/media/Provider;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 357
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatBrowseResponseListener:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/extension/FlatProvider;->startFlatBrowse(Lcom/samsung/android/allshare/Item$MediaType;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    goto :goto_0

    .line 359
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatBrowseResponseListener:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/extension/FlatProvider;->cancelFlatBrowse(Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    goto :goto_0
.end method

.method public getRemoteDeviceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    return-object v0
.end method

.method public getSelectedDmsNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedRemoteDevice:Lcom/samsung/android/allshare/Device;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedRemoteDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v0

    .line 367
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedRemoteProvider()Lcom/samsung/android/allshare/media/Provider;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedDeviceProvider:Lcom/samsung/android/allshare/media/Provider;

    return-object v0
.end method

.method public isMyDevice(Lcom/samsung/android/allshare/Device;)Z
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 155
    const-string v0, "isMyDevice()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/extension/DeviceChecker;->isMyLocalProvider(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x1

    .line 160
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifySettingChanged(II)V
    .locals 3
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 598
    const/4 v0, 0x1

    const-string v1, "notifySettingChanged E"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 599
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mNotificationHandler:Lcom/samsung/everglades/video/myvideo/common/ASFUtil$NotificationHandler;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mNotificationHandler:Lcom/samsung/everglades/video/myvideo/common/ASFUtil$NotificationHandler;

    const/4 v2, 0x0

    invoke-static {v1, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$NotificationHandler;->sendMessage(Landroid/os/Message;)Z

    .line 600
    return-void
.end method

.method public refreshRemoteDevice()V
    .locals 1

    .prologue
    .line 206
    const-string v0, "refreshRemoteDeviceFinder()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceFinder;->refresh()V

    .line 211
    :cond_0
    return-void
.end method

.method public setContextForUI(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mContextForUI:Landroid/content/Context;

    .line 119
    return-void
.end method

.method public setOnAllShareDeviceChangedObserver(Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;)V
    .locals 3
    .param p1, "o"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;

    .prologue
    .line 627
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOnAllShareDeviceChangedObserver E. o = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 628
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mObservers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 631
    :cond_0
    return-void
.end method

.method public setSelectedRemoteDevice(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 192
    const-string v0, "setSelectedRemoteDevice()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedRemoteDevice:Lcom/samsung/android/allshare/Device;

    .line 195
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/media/Provider;

    check-cast v0, Lcom/samsung/android/allshare/media/Provider;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedDeviceProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 197
    new-instance v0, Lcom/samsung/android/allshare/extension/FlatProvider;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedDeviceProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/extension/FlatProvider;-><init>(Lcom/samsung/android/allshare/media/Provider;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    .line 199
    :cond_0
    return-void
.end method
