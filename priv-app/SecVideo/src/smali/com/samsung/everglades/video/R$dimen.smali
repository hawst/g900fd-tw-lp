.class public final Lcom/samsung/everglades/video/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final actionbar_back_btn_left_padding:I = 0x7f080000

.field public static final actionbar_back_btn_right_cab_padding:I = 0x7f080001

.field public static final actionbar_back_btn_right_padding:I = 0x7f080002

.field public static final actionbar_bar_height:I = 0x7f080003

.field public static final actionbar_bar_title_max_size:I = 0x7f080004

.field public static final actionbar_btn_margin:I = 0x7f080005

.field public static final actionbar_divider_height:I = 0x7f080006

.field public static final actionbar_divider_width:I = 0x7f080007

.field public static final actionbar_main_title_left_margin:I = 0x7f080008

.field public static final actionbar_remove_text_size:I = 0x7f080009

.field public static final actionbar_spinner_dropdown_left_margin:I = 0x7f08000a

.field public static final actionbar_spinner_dropdown_vertical_offset:I = 0x7f08000b

.field public static final actionbar_spinner_dropdown_width:I = 0x7f08000c

.field public static final actionbar_spinner_height:I = 0x7f08000d

.field public static final actionbar_spinner_left_margin:I = 0x7f08000e

.field public static final actionbar_spinner_style_text_size:I = 0x7f08000f

.field public static final actionbar_spinner_width:I = 0x7f080010

.field public static final actionbar_title_left_margin:I = 0x7f080011

.field public static final actionbar_title_right_margin:I = 0x7f080012

.field public static final actionbar_title_size:I = 0x7f080013

.field public static final checkbox_margin_right:I = 0x7f080014

.field public static final checkbox_margin_top:I = 0x7f080015

.field public static final checkbox_width_and_height:I = 0x7f080016

.field public static final details_list_padding:I = 0x7f080017

.field public static final details_row_height:I = 0x7f080018

.field public static final details_row_text_bottom_margin:I = 0x7f080019

.field public static final details_row_text_top_margin:I = 0x7f08001a

.field public static final details_row_title_text_left_margin:I = 0x7f08001b

.field public static final details_row_title_textsize:I = 0x7f08001c

.field public static final details_row_value_text_left_margin:I = 0x7f08001d

.field public static final details_row_value_text_right_margin:I = 0x7f08001e

.field public static final details_row_value_textsize:I = 0x7f08001f

.field public static final device_empty_view_top_bottom_margin:I = 0x7f080020

.field public static final device_list_layout_height:I = 0x7f080021

.field public static final device_list_text_margin:I = 0x7f080022

.field public static final device_list_text_size:I = 0x7f080023

.field public static final device_row_dlna_height:I = 0x7f080024

.field public static final device_row_dlna_txt_size:I = 0x7f080025

.field public static final device_row_slink_first_txt_size:I = 0x7f080026

.field public static final device_row_slink_height:I = 0x7f080027

.field public static final device_row_slink_network_txt_left_margin:I = 0x7f080028

.field public static final device_row_slink_network_txt_size:I = 0x7f080029

.field public static final device_row_slink_second_txt_size:I = 0x7f08002a

.field public static final device_section_bottom_margin:I = 0x7f08002b

.field public static final device_section_height:I = 0x7f08002c

.field public static final device_section_refresh_icon_size:I = 0x7f08002d

.field public static final device_section_refresh_layout_width:I = 0x7f08002e

.field public static final device_section_refresh_left_margin:I = 0x7f08002f

.field public static final device_section_top_margin:I = 0x7f080030

.field public static final device_section_txt_size:I = 0x7f080031

.field public static final device_side_margin:I = 0x7f080032

.field public static final device_text_right_margin:I = 0x7f080033

.field public static final device_top_margin:I = 0x7f080034

.field public static final empty_device_text_size:I = 0x7f080035

.field public static final empty_local_btn_text_size:I = 0x7f080036

.field public static final empty_local_image_layout_marginBottom:I = 0x7f080037

.field public static final empty_local_image_size:I = 0x7f080038

.field public static final empty_local_text_margin_left:I = 0x7f080039

.field public static final empty_local_text_margin_right:I = 0x7f08003a

.field public static final empty_local_text_size:I = 0x7f08003b

.field public static final folder_left_layout_width:I = 0x7f08003c

.field public static final folder_margin_layout_width:I = 0x7f08003d

.field public static final folder_margin_left:I = 0x7f08003e

.field public static final folder_margin_left_press:I = 0x7f08003f

.field public static final folderpreview_image_size:I = 0x7f080040

.field public static final folderpreview_padding:I = 0x7f080041

.field public static final folderpreview_start_margin:I = 0x7f080042

.field public static final folderpreview_start_margin_X:I = 0x7f080043

.field public static final folderpreview_start_margin_Y:I = 0x7f080044

.field public static final list_content_margin:I = 0x7f080045

.field public static final list_end_text_size:I = 0x7f080046

.field public static final list_menu_height:I = 0x7f080047

.field public static final list_menu_margin:I = 0x7f080048

.field public static final list_only_tab_margin:I = 0x7f080049

.field public static final list_row_cloud_height:I = 0x7f08004a

.field public static final list_row_cloud_width:I = 0x7f08004b

.field public static final list_row_folder_icon_height:I = 0x7f08004c

.field public static final list_row_folder_icon_width:I = 0x7f08004d

.field public static final list_row_image_frame_height:I = 0x7f08004e

.field public static final list_row_image_frame_width:I = 0x7f08004f

.field public static final list_row_item_bottom_line_height:I = 0x7f080050

.field public static final list_row_item_bottom_line_margin:I = 0x7f080051

.field public static final list_row_item_bottom_margin:I = 0x7f080052

.field public static final list_row_item_top_margin:I = 0x7f080053

.field public static final list_row_progressbar_bottom_margin:I = 0x7f080054

.field public static final list_row_progressbar_height:I = 0x7f080055

.field public static final list_row_progressbar_margin:I = 0x7f080056

.field public static final list_row_text_first_row_text_size:I = 0x7f080057

.field public static final list_row_text_layout_left_margin:I = 0x7f080058

.field public static final list_row_text_layout_start_margin:I = 0x7f080059

.field public static final list_row_text_second_row_text_size:I = 0x7f08005a

.field public static final list_row_total_height:I = 0x7f08005b

.field public static final list_row_total_width:I = 0x7f08005c

.field public static final list_row_total_width_port:I = 0x7f08005d

.field public static final list_tab_marginTop:I = 0x7f08005e

.field public static final move_to_knox_choice_popup_bottom_margin:I = 0x7f08005f

.field public static final move_to_knox_choice_popup_text_size:I = 0x7f080060

.field public static final move_to_knox_choice_popup_textview_height:I = 0x7f080061

.field public static final myvideo_horizontal_margin:I = 0x7f080062

.field public static final personal_folder_icon_height:I = 0x7f080063

.field public static final personal_folder_icon_width:I = 0x7f080064

.field public static final popup_button_image_size:I = 0x7f080065

.field public static final popup_button_layout_height:I = 0x7f080066

.field public static final popup_button_margin:I = 0x7f080067

.field public static final rename_content_layout_bottom_padding:I = 0x7f080068

.field public static final rename_content_layout_left_padding:I = 0x7f080069

.field public static final rename_content_layout_right_padding:I = 0x7f08006a

.field public static final rename_content_layout_top_padding:I = 0x7f08006b

.field public static final search_view_back_btn_width:I = 0x7f08006c

.field public static final search_view_back_layout_width:I = 0x7f08006d

.field public static final selectall_text_size:I = 0x7f08006e

.field public static final tabhost_height:I = 0x7f08006f

.field public static final thumb_row_content_width:I = 0x7f080070

.field public static final thumb_row_content_width_land:I = 0x7f080071

.field public static final thumb_row_image_frame_height:I = 0x7f080072

.field public static final thumb_row_image_frame_height_land:I = 0x7f080073

.field public static final thumb_row_image_top_margin:I = 0x7f080074

.field public static final thumb_row_item_bottom_margin:I = 0x7f080075

.field public static final thumb_row_item_bottom_margin_land:I = 0x7f080076

.field public static final thumb_row_text_first_row_height:I = 0x7f080077

.field public static final thumb_row_text_first_row_text_size:I = 0x7f080078

.field public static final thumb_row_text_layout_top_margin:I = 0x7f080079

.field public static final thumb_row_text_layout_top_margin_land:I = 0x7f08007a

.field public static final thumb_row_text_second_row_height:I = 0x7f08007b

.field public static final thumb_row_text_second_row_text_size:I = 0x7f08007c

.field public static final thumb_row_text_second_row_top_margin:I = 0x7f08007d

.field public static final thumb_row_total_height:I = 0x7f08007e

.field public static final thumb_row_total_height_land:I = 0x7f08007f

.field public static final thumb_row_total_width:I = 0x7f080080

.field public static final thumb_row_total_width_land:I = 0x7f080081

.field public static final thumbnail_view_horizontal_spacing_land:I = 0x7f080082

.field public static final thumbnail_view_horizontal_spacing_port:I = 0x7f080083

.field public static final thumbnail_view_margin:I = 0x7f080084

.field public static final thumbnail_view_margin_bottom:I = 0x7f080085

.field public static final thumbnail_view_margin_bottom_land:I = 0x7f080086

.field public static final thumbnail_view_margin_land:I = 0x7f080087


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
