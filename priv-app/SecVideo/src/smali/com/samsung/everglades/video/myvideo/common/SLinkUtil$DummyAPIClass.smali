.class Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$DummyAPIClass;
.super Ljava/lang/Object;
.source "SLinkUtil.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DummyAPIClass"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$1;

    .prologue
    .line 510
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$DummyAPIClass;-><init>()V

    return-void
.end method


# virtual methods
.method public acquireWakeLock(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 577
    return-void
.end method

.method public callScsCoreInitServiceIfNeeded()V
    .locals 0

    .prologue
    .line 519
    return-void
.end method

.method public deleteFile(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 583
    return-void
.end method

.method public deregisterDevice(I)Z
    .locals 1
    .param p1, "deviceId"    # I

    .prologue
    .line 599
    const/4 v0, 0x0

    return v0
.end method

.method public getDeviceIcon(JLandroid/database/Cursor;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "deviceId"    # J
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 546
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDeviceNameByCurosr(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 542
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDevicesCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 522
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFileCursorByDeviceId(ILjava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "deviceId"    # I
    .param p2, "strFilter"    # Ljava/lang/String;

    .prologue
    .line 526
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHowToUseViewIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 570
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLocalDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 538
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;
    .locals 1
    .param p1, "data"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 594
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNetworkModeByCursor(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 550
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSignInActivityIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 566
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized getThumbId(Ljava/lang/String;)J
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 562
    monitor-enter p0

    const-wide/16 v0, -0x1

    monitor-exit p0

    return-wide v0
.end method

.method public getUriById(J)Landroid/net/Uri;
    .locals 1
    .param p1, "videoId"    # J

    .prologue
    .line 530
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoCount(I)I
    .locals 1
    .param p1, "deviceId"    # I

    .prologue
    .line 534
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized isSLinkThumbId(Ljava/lang/String;)Z
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 558
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public isSamsungAccountExists()Z
    .locals 1

    .prologue
    .line 516
    const/4 v0, 0x0

    return v0
.end method

.method public isSignedIn()Z
    .locals 1

    .prologue
    .line 512
    const/4 v0, 0x0

    return v0
.end method

.method public releaseWakeLock()V
    .locals 0

    .prologue
    .line 579
    return-void
.end method

.method public requestDownload(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 573
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    return-void
.end method

.method public requestRefresh()V
    .locals 0

    .prologue
    .line 581
    return-void
.end method

.method public requestShareVia(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 590
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public sendToAnotherDevice(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 586
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public setSyncPriority()V
    .locals 0

    .prologue
    .line 575
    return-void
.end method

.method public thumbIdToString(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 554
    const/4 v0, 0x0

    return-object v0
.end method
