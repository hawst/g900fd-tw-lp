.class Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$8;
.super Ljava/lang/Object;
.source "MovePrivateOperation.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->createFolderPickerPopup(Ljava/util/ArrayList;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

.field final synthetic val$playList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 813
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$8;->val$playList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 817
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/storage"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$8;->val$playList:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string v3, "Path"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->selectFolder(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$2100(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/lang/String;)V

    .line 819
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mFolderPickerPopup:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$2000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 820
    return-void
.end method
