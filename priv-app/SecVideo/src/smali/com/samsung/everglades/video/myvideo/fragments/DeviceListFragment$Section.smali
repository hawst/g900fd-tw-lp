.class Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
.super Ljava/lang/Object;
.source "DeviceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Section"
.end annotation


# instance fields
.field mRefreshImage:Landroid/widget/ImageView;

.field mRefreshImgBtn:Landroid/widget/LinearLayout;

.field mRefreshProgress:Landroid/widget/ProgressBar;

.field mSection:Landroid/widget/FrameLayout;

.field mSectionCount:Landroid/widget/TextView;

.field mSectionTitle:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;Landroid/view/View;II)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "id"    # I
    .param p4, "pos"    # I

    .prologue
    const v4, 0x7f09001d

    .line 124
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    if-eqz p2, :cond_0

    .line 126
    check-cast p2, Landroid/widget/FrameLayout;

    .end local p2    # "view":Landroid/view/View;
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    .line 128
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    const v1, 0x7f090019

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionTitle:Landroid/widget/TextView;

    .line 129
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionTitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionTitle:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    const v1, 0x7f09001a

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    .line 134
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    const v1, 0x7f09001b

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImgBtn:Landroid/widget/LinearLayout;

    .line 135
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImgBtn:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    const v1, 0x7f09001e

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshProgress:Landroid/widget/ProgressBar;

    .line 137
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    .line 139
    if-nez p4, :cond_1

    .line 140
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    const v1, 0x7f090020

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showRefreshProgress()V

    return-void
.end method

.method private showRefreshProgress()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImgBtn:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showViewItem(Landroid/view/View;Z)V

    .line 154
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showViewItem(Landroid/view/View;Z)V

    .line 155
    return-void
.end method

.method private showViewItem(Landroid/view/View;Z)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "show"    # Z

    .prologue
    .line 158
    if-eqz p2, :cond_1

    const/4 v0, 0x0

    .line 159
    .local v0, "visible":I
    :goto_0
    if-eqz p1, :cond_0

    .line 160
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 162
    :cond_0
    return-void

    .line 158
    .end local v0    # "visible":I
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public showRefreshBtn()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImgBtn:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showViewItem(Landroid/view/View;Z)V

    .line 149
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showViewItem(Landroid/view/View;Z)V

    .line 150
    return-void
.end method
