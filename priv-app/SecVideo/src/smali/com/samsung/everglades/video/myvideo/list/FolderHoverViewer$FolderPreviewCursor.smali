.class Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
.super Ljava/lang/Object;
.source "FolderHoverViewer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FolderPreviewCursor"
.end annotation


# instance fields
.field private isCloud:Z

.field private isSKTCloud:Z

.field private mBucketID:I

.field private mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;


# direct methods
.method public constructor <init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 538
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 532
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    .line 533
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isCloud:Z

    .line 534
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isSKTCloud:Z

    .line 535
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mBucketID:I

    .line 536
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mContext:Landroid/content/Context;

    .line 538
    return-void
.end method


# virtual methods
.method public closeCursor()V
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 583
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    .line 585
    :cond_0
    return-void
.end method

.method public getBucketID()I
    .locals 1

    .prologue
    .line 596
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mBucketID:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 591
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getUriFromCursor(Ljava/lang/String;)Landroid/net/Uri;
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 600
    const/4 v0, 0x0

    .line 601
    .local v0, "FilePath":Ljava/lang/String;
    const/4 v5, 0x0

    .line 603
    .local v5, "result":Landroid/net/Uri;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 604
    .local v4, "originIndex":I
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 606
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 607
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    const-string v8, "_data"

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 609
    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 618
    :cond_0
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 620
    .local v2, "id":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-ltz v6, :cond_1

    .line 621
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isDropboxCloudCursor()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 622
    sget-object v6, Lcom/samsung/everglades/video/myvideo/common/DB;->CLOUD_DB_URI:Landroid/net/Uri;

    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 630
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 632
    return-object v5

    .line 613
    .end local v2    # "id":J
    :cond_2
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 606
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 623
    .restart local v2    # "id":J
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isSKTCloudCursor()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 624
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$2100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getSKTCloudVideoUriById(J)Landroid/net/Uri;

    move-result-object v5

    goto :goto_1

    .line 626
    :cond_4
    sget-object v6, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    goto :goto_1
.end method

.method public isDropboxCloudCursor()Z
    .locals 1

    .prologue
    .line 573
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isCloud:Z

    return v0
.end method

.method public isSKTCloudCursor()Z
    .locals 1

    .prologue
    .line 577
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isSKTCloud:Z

    return v0
.end method

.method public setCursor(Landroid/net/Uri;I)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bucketID"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 545
    iput p2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mBucketID:I

    .line 547
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 548
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 549
    iput-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    .line 552
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    .line 553
    .local v0, "cloudUtil":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 554
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isCloud:Z

    .line 555
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isSKTCloud:Z

    .line 556
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mBucketID:I

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getCursorByBucketId(I)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    .line 567
    :goto_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 568
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 570
    :cond_1
    return-void

    .line 557
    :cond_2
    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 558
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isCloud:Z

    .line 559
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isSKTCloud:Z

    .line 560
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mBucketID:I

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getCursorByBucketId(I)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 562
    :cond_3
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isCloud:Z

    .line 563
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isSKTCloud:Z

    .line 564
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$2200(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mBucketID:I

    invoke-virtual {v1, v2, v4, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoCursor(ILjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->mCursor:Landroid/database/Cursor;

    goto :goto_0
.end method
