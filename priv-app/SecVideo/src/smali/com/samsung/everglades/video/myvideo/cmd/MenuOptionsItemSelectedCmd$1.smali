.class Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;
.super Ljava/lang/Object;
.source "MenuOptionsItemSelectedCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->autoPlayMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v2, 0x0

    .line 165
    packed-switch p2, :pswitch_data_0

    .line 182
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 183
    return-void

    .line 167
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->updateAutoPlay(Z)V
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$000(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;Z)V

    .line 168
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # getter for: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$100(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # getter for: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$100(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "VPAP"

    const/16 v2, 0x3e8

    invoke-static {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 174
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # invokes: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->updateAutoPlay(Z)V
    invoke-static {v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$000(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;Z)V

    .line 175
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # getter for: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$100(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # getter for: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$100(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "VPAP"

    invoke-static {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
