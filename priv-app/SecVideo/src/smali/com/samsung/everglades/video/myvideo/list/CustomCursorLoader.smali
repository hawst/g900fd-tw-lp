.class public Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;
.super Landroid/content/AsyncTaskLoader;
.source "CustomCursorLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/AsyncTaskLoader",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field mBucketId:I

.field mCancellationSignal:Landroid/os/CancellationSignal;

.field mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field mCursor:Landroid/database/Cursor;

.field mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field final mObserver:Landroid/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field mSLinkDeviceId:I

.field mSearchKey:Ljava/lang/String;

.field mSeasonId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mBucketId:I

    .line 53
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/everglades/video/myvideo/common/ListType;ILjava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p3, "bucketId"    # I
    .param p4, "searchKey"    # Ljava/lang/String;
    .param p5, "sLinkDeviceId"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mBucketId:I

    .line 58
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    .line 59
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 60
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 61
    iput-object p4, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSearchKey:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 63
    iput p3, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mBucketId:I

    .line 64
    iput p5, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSLinkDeviceId:I

    .line 65
    return-void
.end method

.method private getCursor()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 182
    const/4 v0, 0x0

    .line 184
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 245
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 187
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->shouldLocalOnly()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mBucketId:I

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSearchKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2, v3, v5, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoCursor(ILjava/lang/String;ZLcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 189
    :cond_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->shouldCloudOnly()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 190
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->noDeleteAndNoShareVia()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSearchKey:Ljava/lang/String;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mBucketId:I

    invoke-virtual {v1, v4, v2, v3, v5}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;IZ)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 194
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->noCloudDownloadCase()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 195
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mBucketId:I

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSearchKey:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoCursor(ILjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    .line 198
    :cond_3
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->noDeleteAndNoShareVia()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSearchKey:Ljava/lang/String;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mBucketId:I

    invoke-virtual {v1, v0, v2, v3, v5}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;IZ)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 205
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->noCloudDownloadCase()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 206
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mBucketId:I

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSearchKey:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoCursor(ILjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    .line 209
    :cond_4
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->noDeleteAndNoShareVia()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSearchKey:Ljava/lang/String;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mBucketId:I

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;IZ)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 215
    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->shouldLocalOnly()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 216
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoCursorFolder(Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 217
    :cond_5
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->shouldCloudOnly()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 218
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->noDeleteAndNoShareVia()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getMergedCursorFolder(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto/16 :goto_0

    .line 222
    :cond_6
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->noCloudDownloadCase()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 223
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoCursorFolder(Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/database/Cursor;

    move-result-object v0

    .line 226
    :cond_7
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->noDeleteAndNoShareVia()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getMergedCursorFolder(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto/16 :goto_0

    .line 234
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSearchKey:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v5, v2, v5, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoCursor(ILjava/lang/String;ZLcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/database/Cursor;

    move-result-object v0

    .line 235
    goto/16 :goto_0

    .line 239
    :pswitch_5
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSLinkDeviceId:I

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mSearchKey:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getFileCursorByDeviceId(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 240
    goto/16 :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private noCloudDownloadCase()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isCloudDownloadAttr()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private noDeleteAndNoShareVia()Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeleteAttr()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isShareViaAttr()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldCloudOnly()Z
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isContentInDropbox()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isContentInTcloud()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldLocalOnly()Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isLocalOnly()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isContentInPhone()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancelLoadInBackground()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->cancelLoadInBackground()V

    .line 97
    monitor-enter p0

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    .line 101
    :cond_0
    monitor-exit p0

    .line 102
    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deliverResult(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->isReset()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    if-eqz p1, :cond_0

    .line 118
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    .line 123
    .local v0, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    .line 125
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 129
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 130
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->deliverResult(Landroid/database/Cursor;)V

    return-void
.end method

.method public loadInBackground()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 70
    monitor-enter p0

    .line 71
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->isLoadInBackgroundCanceled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    new-instance v1, Landroid/os/OperationCanceledException;

    invoke-direct {v1}, Landroid/os/OperationCanceledException;-><init>()V

    throw v1

    .line 75
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 74
    :cond_0
    :try_start_1
    new-instance v1, Landroid/os/CancellationSignal;

    invoke-direct {v1}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 75
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :try_start_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 79
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 81
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 82
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->registerContentObserver(Landroid/database/Cursor;Landroid/database/ContentObserver;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 87
    :cond_1
    monitor-enter p0

    .line 88
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 89
    monitor-exit p0

    return-object v0

    :catchall_1
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    .line 87
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_2
    move-exception v1

    monitor-enter p0

    .line 88
    const/4 v2, 0x0

    :try_start_4
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 89
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v1

    :catchall_3
    move-exception v1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v1
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public onCanceled(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 163
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 166
    :cond_0
    return-void
.end method

.method public bridge synthetic onCanceled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->onCanceled(Landroid/database/Cursor;)V

    return-void
.end method

.method protected onReset()V
    .locals 1

    .prologue
    .line 170
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onReset()V

    .line 173
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->onStopLoading()V

    .line 175
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 178
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    .line 179
    return-void
.end method

.method protected onStartLoading()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->deliverResult(Landroid/database/Cursor;)V

    .line 147
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->takeContentChanged()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_2

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->forceLoad()V

    .line 150
    :cond_2
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->cancelLoad()Z

    .line 159
    return-void
.end method

.method registerContentObserver(Landroid/database/Cursor;Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 110
    return-void
.end method
