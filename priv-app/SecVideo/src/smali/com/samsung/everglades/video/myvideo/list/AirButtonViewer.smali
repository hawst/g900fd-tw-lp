.class public Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;
.super Ljava/lang/Object;
.source "AirButtonViewer.java"


# static fields
.field private static final ID_DELETE:I = 0x2

.field private static final ID_DETAILS:I = 0x4

.field private static final ID_RENAME:I = 0x1

.field private static final ID_SHARE_VIA:I

.field private static final STRING_RESOURCES:[I


# instance fields
.field private mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

.field private mBaidu:Z

.field private mCallback:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

.field private mCloud:Z

.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mDrm:Z

.field private mDrmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

.field private mDropbox:Z

.field private mFolder:Z

.field private mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;",
            ">;"
        }
    .end annotation
.end field

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mNormal:Z

.field private mOptionsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mOrientation:I

.field private mSearch:Z

.field private mSkt:Z

.field private mThumbHoverViewer:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->STRING_RESOURCES:[I

    return-void

    :array_0
    .array-data 4
        0x7f0c005b
        0x7f0c00ab
        0x7f0c005a
        0x7f0c005f
        0x7f0c005e
        0x7f0c0062
        0x7f0c0063
        0x7f0c000e
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p4, "orientation"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrm:Z

    .line 45
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mNormal:Z

    .line 46
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mFolder:Z

    .line 47
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSkt:Z

    .line 48
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDropbox:Z

    .line 49
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mBaidu:Z

    .line 50
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloud:Z

    .line 51
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSearch:Z

    .line 62
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    .line 63
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOrientation:I

    .line 131
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCallback:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    .line 76
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    .line 77
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 78
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 79
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 80
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    .line 81
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    .line 82
    iput p4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOrientation:I

    .line 83
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mThumbHoverViewer:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p4, "thViewer"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrm:Z

    .line 45
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mNormal:Z

    .line 46
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mFolder:Z

    .line 47
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSkt:Z

    .line 48
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDropbox:Z

    .line 49
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mBaidu:Z

    .line 50
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloud:Z

    .line 51
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSearch:Z

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    .line 63
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOrientation:I

    .line 131
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCallback:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    .line 66
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    .line 67
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 68
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 69
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    .line 70
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 71
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    .line 72
    iput-object p4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mThumbHoverViewer:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->performAction(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;)Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mThumbHoverViewer:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    return-object v0
.end method

.method private initBooleans()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 166
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isDrmContent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrm:Z

    .line 168
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isTypical()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mNormal:Z

    .line 169
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mFolder:Z

    .line 170
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSkt:Z

    .line 171
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDropbox:Z

    .line 172
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/net/Uri;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mBaidu:Z

    .line 173
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSkt:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDropbox:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mBaidu:Z

    if-eqz v1, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloud:Z

    .line 174
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSearch:Z

    .line 175
    return-void

    :cond_2
    move v1, v3

    .line 167
    goto :goto_0
.end method

.method private initLists()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 151
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 153
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mItemList:Ljava/util/ArrayList;

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 158
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    .line 161
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mItemList:Ljava/util/ArrayList;

    .line 163
    return-void
.end method

.method private performAction(I)V
    .locals 3
    .param p1, "which"    # I

    .prologue
    .line 215
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 216
    const-string v1, "slook.Viewer performAction() mOptionsList is null"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 221
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "slook.Viewer:performAction which : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mOptionsList.get(which) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 222
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 224
    .local v0, "selectedId":I
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 226
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->singleShareVia()V

    goto :goto_0

    .line 230
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->singleDelete()V

    goto :goto_0

    .line 234
    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->showDetails()V

    goto :goto_0

    .line 238
    :pswitch_4
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->showRenameDialog()V

    goto :goto_0

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private showDetails()V
    .locals 4

    .prologue
    .line 253
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->showDialog()V

    .line 254
    return-void
.end method

.method private showRenameDialog()V
    .locals 3

    .prologue
    .line 257
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showDialog()V

    .line 258
    return-void
.end method

.method private singleDelete()V
    .locals 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleDelete(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 250
    return-void
.end method

.method private singleShareVia()V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleShareVia(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 246
    return-void
.end method

.method private updateOptionsList()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 178
    const/4 v0, 0x0

    .line 179
    .local v0, "count":I
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mNormal:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSearch:Z

    if-eqz v2, :cond_1

    .line 180
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloud:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrm:Z

    if-nez v2, :cond_1

    .line 181
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mItemList:Ljava/util/ArrayList;

    new-instance v3, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02000a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v6, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->STRING_RESOURCES:[I

    aget v6, v6, v9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v7}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    .line 188
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mNormal:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mFolder:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSearch:Z

    if-eqz v2, :cond_3

    .line 189
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloud:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrm:Z

    if-nez v2, :cond_3

    .line 190
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mItemList:Ljava/util/ArrayList;

    new-instance v3, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02000b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v6, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->STRING_RESOURCES:[I

    aget v6, v6, v8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v7}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    .line 197
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_3
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mNormal:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrm:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mFolder:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mSearch:Z

    if-eqz v2, :cond_5

    .line 198
    :cond_4
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloud:Z

    if-nez v2, :cond_5

    .line 199
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mItemList:Ljava/util/ArrayList;

    new-instance v3, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020008

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v6, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->STRING_RESOURCES:[I

    aget v6, v6, v10

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v7}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    .line 206
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_5
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mFolder:Z

    if-nez v2, :cond_6

    .line 207
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mItemList:Ljava/util/ArrayList;

    new-instance v3, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020009

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v6, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->STRING_RESOURCES:[I

    aget v6, v6, v11

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v7}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOptionsList:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    .line 212
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_6
    return-void
.end method


# virtual methods
.method public addAirButton(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 102
    :try_start_0
    new-instance v1, Lcom/samsung/android/sdk/look/Slook;

    invoke-direct {v1}, Lcom/samsung/android/sdk/look/Slook;-><init>()V

    .line 103
    .local v1, "slook":Lcom/samsung/android/sdk/look/Slook;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/look/Slook;->getVersionCode()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_0

    .line 104
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-nez v2, :cond_0

    .line 105
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->createMenuWidgetFromView(Landroid/view/View;)Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 113
    .end local v1    # "slook":Lcom/samsung/android/sdk/look/Slook;
    :cond_0
    :goto_0
    return-void

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    const-string v2, "slook.Viewer - NoClassDefFoundError"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 110
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "slook.Viewer - exception"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public createMenuWidgetFromView(Landroid/view/View;)Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 116
    new-instance v0, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->getAdapterMenuList()Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;I)V

    .line 117
    .local v0, "airButtonWidget":Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCallback:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setItemSelectListener(Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V

    .line 118
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setGravity(I)V

    .line 119
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setDirection(I)V

    .line 120
    invoke-virtual {v0, v3, v3}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->setPosition(II)V

    .line 121
    return-object v0
.end method

.method public dismiss()V
    .locals 2

    .prologue
    .line 269
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-eqz v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 271
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "slook.Viewer dismiss() Exception"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAdapterMenuList()Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;
    .locals 2

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->initLists()V

    .line 126
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->initBooleans()V

    .line 127
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->updateOptionsList()V

    .line 128
    new-instance v0, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mItemList:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public isThereAirButton()Z
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-nez v0, :cond_0

    .line 262
    const/4 v0, 0x0

    .line 264
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mAirButtonIsThere()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x1

    .line 146
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldBlockDownload()Z
    .locals 3

    .prologue
    .line 277
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "application/x-dtcp1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updateInfo(Landroid/content/Context;Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;ILandroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p4, "orientation"    # I
    .param p5, "v"    # Landroid/view/View;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mContext:Landroid/content/Context;

    .line 88
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 89
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 90
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 91
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mUri:Landroid/net/Uri;

    .line 92
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mDrmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    .line 93
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOrientation:I

    if-eq v0, p4, :cond_1

    .line 94
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButton:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;->dismiss()V

    .line 95
    :cond_0
    invoke-virtual {p0, p5}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->addAirButton(Landroid/view/View;)V

    .line 96
    iput p4, p0, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mOrientation:I

    .line 98
    :cond_1
    return-void
.end method
