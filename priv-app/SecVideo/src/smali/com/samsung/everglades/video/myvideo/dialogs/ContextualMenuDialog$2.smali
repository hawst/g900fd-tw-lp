.class Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;
.super Ljava/lang/Object;
.source "ContextualMenuDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x0

    .line 404
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->access$102(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Landroid/net/Uri;)Landroid/net/Uri;

    .line 405
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->access$202(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Landroid/content/Context;)Landroid/content/Context;

    .line 406
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCursor:Landroid/database/Cursor;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->access$302(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 407
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->access$402(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 408
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->access$502(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 409
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->access$602(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 410
    return-void
.end method
