.class Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;
.super Ljava/lang/Object;
.source "SelectionAnimator.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->ShowCheckBox()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 44
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildCount()I

    move-result v3

    .line 45
    .local v3, "childCount":I
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/touchwiz/widget/TwGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 46
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/sec/android/touchwiz/widget/TwGridView;->setEnabled(Z)V

    .line 49
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_1

    .line 50
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f09002c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 51
    .local v1, "cb":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 52
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 49
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 54
    .end local v1    # "cb":Landroid/view/View;
    :cond_1
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->getChbTransX()I
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$100(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)I

    move-result v2

    .line 56
    .local v2, "chbTranslationX":I
    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-static {v5}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 57
    .local v0, "animator":Landroid/animation/ValueAnimator;
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->CHECKBOX_FLOATING_DEFAULT_DURATION:I
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$300(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->SINE_IN_OUT_70:Landroid/view/animation/Interpolator;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$200(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Landroid/view/animation/Interpolator;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 58
    new-instance v5, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;

    invoke-direct {v5, p0, v3, v2}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;II)V

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 82
    new-instance v5, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$2;

    invoke-direct {v5, p0}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;)V

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 93
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 94
    return v8

    .line 56
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
