.class Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;
.super Landroid/os/Handler;
.source "MoveKNOXOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x1

    .line 334
    monitor-enter p0

    :try_start_0
    iget v2, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v2, :pswitch_data_0

    .line 435
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 336
    :pswitch_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mErrOccured:Z
    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$502(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;Z)Z

    .line 337
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    if-nez v2, :cond_1

    .line 338
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    new-instance v3, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;-><init>(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;Landroid/content/Context;)V

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$602(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    .line 341
    :cond_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->getTotalFileCount()I

    move-result v2

    if-ne v2, v6, :cond_2

    .line 342
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->setMax(I)V

    .line 347
    :goto_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->initCount()V

    .line 348
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 334
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 344
    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->getTotalFileCount()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->setMax(I)V

    goto :goto_1

    .line 352
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 353
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 354
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->getCurrentFileCount()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->setCount(I)V

    goto/16 :goto_0

    .line 359
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 360
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 361
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 362
    .local v1, "progress":I
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->setProgress(I)V

    goto/16 :goto_0

    .line 367
    .end local v1    # "progress":I
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 368
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 369
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 370
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 375
    :pswitch_4
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 376
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 377
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->closeProgressDialog()V
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$700(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V

    .line 378
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->getCurrentFileCount()I

    move-result v3

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->showToast(I)V
    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$800(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;I)V

    .line 379
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mOnMoveFinishedListener:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$900(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 380
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mOnMoveFinishedListener:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$900(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;->onMoveKNOXFinished()V

    goto/16 :goto_0

    .line 385
    :pswitch_5
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0c0084

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c007b

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c0097

    new-instance v4, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;

    invoke-direct {v4, p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 407
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->closeProgressDialog()V
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$700(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V

    goto/16 :goto_0

    .line 410
    :pswitch_6
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 411
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 413
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 414
    .local v0, "err":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 415
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 420
    :goto_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 421
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->dismiss()V

    .line 423
    :cond_3
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->closeProgressDialog()V
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$700(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V

    .line 424
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mErrOccured:Z
    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$502(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;Z)Z

    goto/16 :goto_0

    .line 417
    :cond_4
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c0077

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 428
    .end local v0    # "err":Ljava/lang/String;
    :pswitch_7
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->setKeepScreenOn()V
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$1000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V

    goto/16 :goto_0

    .line 432
    :pswitch_8
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->clearKeepScreenOn()V
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$1100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 334
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_2
    .end packed-switch
.end method
