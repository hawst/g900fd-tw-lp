.class public interface abstract Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;
.super Ljava/lang/Object;
.source "ISLinkUtil.java"


# virtual methods
.method public abstract acquireWakeLock(Landroid/content/Context;)V
.end method

.method public abstract callScsCoreInitServiceIfNeeded()V
.end method

.method public abstract deleteFile(Landroid/net/Uri;)V
.end method

.method public abstract deregisterDevice(I)Z
.end method

.method public abstract getDeviceIcon(JLandroid/database/Cursor;)Landroid/graphics/Bitmap;
.end method

.method public abstract getDeviceNameByCurosr(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract getDevicesCursor()Landroid/database/Cursor;
.end method

.method public abstract getFileCursorByDeviceId(ILjava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract getHowToUseViewIntent()Landroid/content/Intent;
.end method

.method public abstract getLocalDeviceName()Ljava/lang/String;
.end method

.method public abstract getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNetworkModeByCursor(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract getSignInActivityIntent()Landroid/content/Intent;
.end method

.method public abstract getThumbId(Ljava/lang/String;)J
.end method

.method public abstract getUriById(J)Landroid/net/Uri;
.end method

.method public abstract getVideoCount(I)I
.end method

.method public abstract isSLinkThumbId(Ljava/lang/String;)Z
.end method

.method public abstract isSamsungAccountExists()Z
.end method

.method public abstract isSignedIn()Z
.end method

.method public abstract releaseWakeLock()V
.end method

.method public abstract requestDownload(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract requestRefresh()V
.end method

.method public abstract requestShareVia(Ljava/util/ArrayList;)Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation
.end method

.method public abstract sendToAnotherDevice(Ljava/util/ArrayList;)Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation
.end method

.method public abstract setSyncPriority()V
.end method

.method public abstract thumbIdToString(Landroid/database/Cursor;)Ljava/lang/String;
.end method
