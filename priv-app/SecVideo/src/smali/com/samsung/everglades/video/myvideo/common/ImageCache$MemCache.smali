.class Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MemCache"
.end annotation


# static fields
.field private static mLruCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createLruCache()Landroid/util/LruCache;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$000()Landroid/content/Context;

    move-result-object v2

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    invoke-virtual {v2}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    .line 166
    .local v1, "memClass":I
    const/high16 v2, 0x100000

    mul-int/2addr v2, v1

    div-int/lit8 v0, v2, 0x4

    .line 167
    .local v0, "cacheSize":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ImageCache - MemCache - cacheSize:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$000()Landroid/content/Context;

    move-result-object v3

    int-to-long v4, v0

    invoke-static {v3, v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " memClass :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 169
    new-instance v2, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache$1;

    invoke-direct {v2, v0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache$1;-><init>(I)V

    return-object v2
.end method

.method public static declared-synchronized get(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 128
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 129
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :goto_0
    monitor-exit v1

    return-object v0

    .line 131
    :cond_0
    :try_start_1
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$000()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 132
    const/4 v0, 0x0

    goto :goto_0

    .line 134
    :cond_1
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->getCache()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getCache()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-nez v0, :cond_0

    .line 157
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->createLruCache()Landroid/util/LruCache;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    .line 159
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    return-object v0
.end method

.method public static declared-synchronized put(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 140
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 141
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0, p0, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ImageCache - CURRENT CACHED SIZE:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$000()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v3}, Landroid/util/LruCache;->size()I

    move-result v3

    int-to-long v4, v3

    invoke-static {v2, v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    monitor-exit v1

    return-void

    .line 143
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->getCache()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static remove(Ljava/lang/String;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 150
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 151
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    :cond_0
    return-void
.end method
