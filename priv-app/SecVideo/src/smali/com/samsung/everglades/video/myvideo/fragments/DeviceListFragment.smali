.class public Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;
.super Landroid/app/Fragment;
.source "DeviceListFragment.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    }
.end annotation


# static fields
.field private static final KEY_DEVICE_UDN:Ljava/lang/String; = "KEY_DEVICE_UDN"

.field private static final MSG_NEARBY_DEVICE_REFRESH_BUTTON_STOP:I = 0xc8

.field private static final MSG_NOTIFY_DATA_CHANGED:I = 0x190

.field private static final MSG_SAMSUNG_LINK_PROVIDER_OBSERVER_CALLED:I = 0x64

.field private static final MSG_SLINK_REFRESH_BUTTON_STOP:I = 0x1f4

.field private static final MSG_UPDATE_LISTVIEW_HEIGHT:I = 0x12c

.field private static final TIME_TO_EXPIRE:I = 0x1388


# instance fields
.field private mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mDisabledByKnoxToast:Landroid/widget/Toast;

.field private mGoToButton:Landroid/widget/Button;

.field private final mHandler:Landroid/os/Handler;

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mMenu:Landroid/view/Menu;

.field private mMessage:Landroid/widget/TextView;

.field private mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

.field private mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

.field private mNearbyDevicesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mNearbyDevicesListView:Landroid/widget/ListView;

.field private mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

.field private mNetWorkFilter:Landroid/content/IntentFilter;

.field private final mNetWorkReceiver:Landroid/content/BroadcastReceiver;

.field private mOnNearbyDeviceItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mSLinkDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

.field private mSLinkDeviceEmptyView:Landroid/widget/RelativeLayout;

.field private mSLinkDevicesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSLinkDevicesListView:Landroid/widget/ListView;

.field private mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

.field private mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

.field private mWifiConntected:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 87
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    .line 89
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    .line 93
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    .line 95
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    .line 97
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mWifiConntected:Z

    .line 370
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mDisabledByKnoxToast:Landroid/widget/Toast;

    .line 650
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mOnNearbyDeviceItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 802
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$8;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$8;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mContentObserver:Landroid/database/ContentObserver;

    .line 814
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$9;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    .line 877
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$10;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$10;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNetWorkReceiver:Landroid/content/BroadcastReceiver;

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSlinkRefresh()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateNearbyRefresh()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;Landroid/view/Menu;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;
    .param p1, "x1"    # Landroid/view/Menu;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->prepareOptionMenu(Landroid/view/Menu;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateDeviceList()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1600(Landroid/widget/ListView;)V
    .locals 0
    .param p0, "x0"    # Landroid/widget/ListView;

    .prologue
    .line 66
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setListViewHeight(Landroid/widget/ListView;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mWifiConntected:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mWifiConntected:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setNoVideoNearby()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;
    .param p1, "x1"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private addNearbyDeviceList()V
    .locals 8

    .prologue
    .line 479
    const/4 v2, 0x0

    .line 480
    .local v2, "remoteDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/Device;>;"
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-eqz v3, :cond_2

    .line 481
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getRemoteDeviceList()Ljava/util/List;

    move-result-object v2

    .line 483
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 484
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 485
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 486
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 487
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v3, v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->isMyDevice(Lcom/samsung/android/allshare/Device;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 488
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    new-instance v4, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 491
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v3, v3, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method private addSLinkDeviceList()V
    .locals 14

    .prologue
    .line 518
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 523
    :goto_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 525
    .local v7, "disableDevicesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;>;"
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    if-eqz v0, :cond_5

    .line 526
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getDevicesCursor()Landroid/database/Cursor;

    move-result-object v6

    .line 527
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 528
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setSLinkEmptyViewVisible(Z)V

    .line 529
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 530
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0, v6}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getDeviceNameByCurosr(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 531
    .local v2, "deviceName":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0, v6}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getNetworkModeByCursor(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 532
    .local v4, "networkMode":Ljava/lang/String;
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 533
    .local v11, "id":I
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 534
    .local v8, "deviceId":J
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0, v11}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getVideoCount(I)I

    move-result v5

    .line 535
    .local v5, "videoCount":I
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0, v8, v9, v6}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getDeviceIcon(JLandroid/database/Cursor;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 537
    .local v3, "deviceIcon":Landroid/graphics/Bitmap;
    const-string v0, "OFF"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 538
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, ""

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;I)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 542
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 521
    .end local v2    # "deviceName":Ljava/lang/String;
    .end local v3    # "deviceIcon":Landroid/graphics/Bitmap;
    .end local v4    # "networkMode":Ljava/lang/String;
    .end local v5    # "videoCount":I
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "disableDevicesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;>;"
    .end local v8    # "deviceId":J
    .end local v11    # "id":I
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    goto :goto_0

    .line 540
    .restart local v2    # "deviceName":Ljava/lang/String;
    .restart local v3    # "deviceIcon":Landroid/graphics/Bitmap;
    .restart local v4    # "networkMode":Ljava/lang/String;
    .restart local v5    # "videoCount":I
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "disableDevicesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;>;"
    .restart local v8    # "deviceId":J
    .restart local v11    # "id":I
    :cond_1
    iget-object v12, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;I)V

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 545
    .end local v2    # "deviceName":Ljava/lang/String;
    .end local v3    # "deviceIcon":Landroid/graphics/Bitmap;
    .end local v4    # "networkMode":Ljava/lang/String;
    .end local v5    # "videoCount":I
    .end local v8    # "deviceId":J
    .end local v11    # "id":I
    :cond_2
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v10, v0, :cond_3

    .line 546
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 549
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 551
    .end local v10    # "i":I
    :cond_4
    if-eqz v6, :cond_5

    .line 552
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 554
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 555
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 556
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->refresh()V

    .line 561
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_5
    return-void
.end method

.method private getTalkbackStringForBtn(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 3
    .param p1, "stringSeq"    # Ljava/lang/CharSequence;

    .prologue
    .line 763
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initNearbyDevicesList()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 424
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    .line 427
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    .line 428
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setNearbyDevices()V

    .line 429
    return-void
.end method

.method private initSLinkDevicesList()V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 448
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    .line 451
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    .line 452
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setSLinkDevices()V

    .line 453
    return-void
.end method

.method private prepareOptionMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 279
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mMenu:Landroid/view/Menu;

    .line 280
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 281
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenu(Landroid/view/Menu;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 282
    const/16 v1, 0x8

    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 283
    return-void
.end method

.method private registerContentObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 793
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 794
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->CONTENT_URI_DEVICE:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 795
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->CONTENT_URI_VIDEOS:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 796
    return-void
.end method

.method private registerNetWorkReciver()V
    .locals 3

    .prologue
    .line 871
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNetWorkFilter:Landroid/content/IntentFilter;

    .line 872
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNetWorkFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 873
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNetWorkReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNetWorkFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 875
    return-void
.end method

.method private setFocusSequence()V
    .locals 5

    .prologue
    const v4, 0x7f090022

    const v3, 0x7f09001d

    const v1, 0x7f09000c

    const v2, 0x7f090020

    .line 354
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mGoToButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setNextFocusDownId(I)V

    .line 356
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mGoToButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusDownId(I)V

    .line 357
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mGoToButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setNextFocusUpId(I)V

    .line 358
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setNextFocusUpId(I)V

    .line 366
    :goto_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    const v1, 0x7f09000f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setNextFocusDownId(I)V

    .line 367
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setNextFocusUpId(I)V

    .line 368
    return-void

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setNextFocusDownId(I)V

    .line 361
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setNextFocusDownId(I)V

    .line 362
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setNextFocusUpId(I)V

    .line 363
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setNextFocusUpId(I)V

    goto :goto_0
.end method

.method private setImageCacheListener()V
    .locals 1

    .prologue
    .line 891
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$11;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$11;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setDevcieListOnUpdatedListener(Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;)V

    .line 900
    return-void
.end method

.method private static setListViewHeight(Landroid/widget/ListView;)V
    .locals 9
    .param p0, "devicelistView"    # Landroid/widget/ListView;

    .prologue
    .line 456
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 457
    .local v3, "myListAdapter":Landroid/widget/ListAdapter;
    if-nez v3, :cond_0

    .line 476
    :goto_0
    return-void

    .line 460
    :cond_0
    const/4 v6, 0x0

    .line 461
    .local v6, "totalHeight":I
    invoke-virtual {p0}, Landroid/widget/ListView;->getWidth()I

    move-result v7

    const/high16 v8, -0x80000000

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 462
    .local v0, "desiredWidth":I
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 463
    .local v1, "deviceCount":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DeviceListFragment - setListViewHeight() : DeviceCount = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 465
    const/4 v5, 0x0

    .local v5, "size":I
    :goto_1
    if-ge v5, v1, :cond_1

    .line 466
    const/4 v7, 0x0

    invoke-interface {v3, v5, v7, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 467
    .local v2, "listItem":Landroid/view/View;
    const/4 v7, 0x0

    invoke-virtual {v2, v0, v7}, Landroid/view/View;->measure(II)V

    .line 468
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    .line 465
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 471
    .end local v2    # "listItem":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 473
    .local v4, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v7

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v7, v8

    add-int/2addr v7, v6

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 474
    invoke-virtual {p0, v4}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 475
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    goto :goto_0
.end method

.method private setNearbyDeviceAdapter()V
    .locals 3

    .prologue
    .line 408
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->initNearbyDevicesList()V

    .line 409
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->clear()V

    .line 411
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 415
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    .line 416
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 417
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setListViewHeight(Landroid/widget/ListView;)V

    .line 419
    :cond_1
    return-void
.end method

.method private setNearbyDevices()V
    .locals 3

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-nez v1, :cond_2

    .line 501
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 504
    :cond_2
    const/4 v0, 0x0

    .line 505
    .local v0, "remoteDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/Device;>;"
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.samsung.android.nearby.mediaserver"

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPackageAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 506
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getRemoteDeviceList()Ljava/util/List;

    move-result-object v0

    .line 509
    :cond_3
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_5

    .line 510
    :cond_4
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 511
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setNoVideoNearby()V

    goto :goto_0

    .line 513
    :cond_5
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->addNearbyDeviceList()V

    goto :goto_0
.end method

.method private setNoVideoNearby()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 767
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 768
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 769
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isChinaModel()Z

    move-result v3

    if-nez v3, :cond_1

    .line 770
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    const v4, 0x7f0c008e

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 782
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080032

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 783
    .local v0, "margin":I
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080034

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 785
    .local v1, "margin_top":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 786
    .local v2, "param":Landroid/widget/LinearLayout$LayoutParams;
    iget v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v4, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v2, v3, v1, v4, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 787
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 788
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 789
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v3, v3, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    invoke-direct {p0, v5}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 790
    :cond_0
    return-void

    .line 772
    .end local v0    # "margin":I
    .end local v1    # "margin_top":I
    .end local v2    # "param":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    const v4, 0x7f0c0090

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 775
    :cond_2
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isChinaModel()Z

    move-result v3

    if-nez v3, :cond_3

    .line 776
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    const v4, 0x7f0c0093

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 778
    :cond_3
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    const v4, 0x7f0c0094

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private setNoVideoSLink()V
    .locals 3

    .prologue
    .line 707
    const/4 v0, 0x0

    .line 708
    .local v0, "str":Ljava/lang/CharSequence;
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setSLinkEmptyViewVisible(Z)V

    .line 709
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSignedIn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 710
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mMessage:Landroid/widget/TextView;

    const v2, 0x7f0c008c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 711
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 712
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mGoToButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 713
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mGoToButton:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$6;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$6;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 757
    :goto_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mGoToButton:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getTalkbackStringForBtn(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 759
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 760
    return-void

    .line 731
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSamsungAccountExists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 732
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mMessage:Landroid/widget/TextView;

    const v2, 0x7f0c0092

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 733
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 738
    :goto_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mGoToButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 739
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mGoToButton:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$7;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$7;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 735
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mMessage:Landroid/widget/TextView;

    const v2, 0x7f0c0091

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 736
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private setSLinkDeviceAdapter()V
    .locals 3

    .prologue
    .line 432
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->initSLinkDevicesList()V

    .line 433
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->clear()V

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 439
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    .line 440
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 441
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setListViewHeight(Landroid/widget/ListView;)V

    .line 443
    :cond_1
    return-void
.end method

.method private setSLinkDevices()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 564
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 587
    :cond_0
    :goto_0
    return-void

    .line 567
    :cond_1
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSLinkProviderAvailable()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 568
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 569
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getDevicesCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 570
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSignedIn()Z

    move-result v2

    .line 571
    .local v2, "isSignedIn":Z
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSamsungAccountExists()Z

    move-result v1

    .line 572
    .local v1, "hasSamsungAccount":Z
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setSLinkDevices - isSignedIn:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 573
    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_4

    .line 574
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->addSLinkDeviceList()V

    .line 579
    :goto_1
    if-eqz v0, :cond_2

    .line 580
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 583
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "VPSL"

    if-eqz v2, :cond_3

    const/16 v3, 0x3e8

    :cond_3
    invoke-static {v4, v5, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 576
    :cond_4
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setNoVideoSLink()V

    goto :goto_1

    .line 585
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "hasSamsungAccount":Z
    .end local v2    # "isSignedIn":Z
    :cond_5
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v3, v3, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSection:Landroid/widget/FrameLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setSLinkEmptyViewVisible(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 689
    if-eqz p1, :cond_2

    .line 690
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 693
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceEmptyView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 694
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceEmptyView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 704
    :cond_1
    :goto_0
    return-void

    .line 697
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_3

    .line 698
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 700
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceEmptyView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 701
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceEmptyView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupDevicesView(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v5, 0x100000

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 286
    const v0, 0x7f09000b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceEmptyView:Landroid/widget/RelativeLayout;

    .line 287
    const v0, 0x7f090022

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mGoToButton:Landroid/widget/Button;

    .line 288
    const v0, 0x7f090021

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mMessage:Landroid/widget/TextView;

    .line 290
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    const v1, 0x7f09000a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c00a4

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;Landroid/view/View;II)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    .line 291
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImgBtn:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$1;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$1;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$2;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$2;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 309
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showRefreshBtn()V

    .line 311
    const v0, 0x7f09000c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    .line 312
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 315
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setDrawingCacheQuality(I)V

    .line 316
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 320
    :cond_0
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    const v1, 0x7f09000d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c0087

    invoke-direct {v0, p0, v1, v2, v4}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;Landroid/view/View;II)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    .line 321
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImgBtn:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$3;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$3;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshImage:Landroid/widget/ImageView;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$4;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$4;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 339
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showRefreshBtn()V

    .line 341
    const v0, 0x7f09000e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;

    .line 343
    const v0, 0x7f09000f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    .line 344
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 345
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setDrawingCacheQuality(I)V

    .line 347
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 348
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mOnNearbyDeviceItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 350
    :cond_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setFocusSequence()V

    .line 351
    return-void
.end method

.method private unregisterContentObserver()V
    .locals 2

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 800
    return-void
.end method

.method private updateDeviceList()Z
    .locals 1

    .prologue
    .line 610
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->addSLinkDeviceList()V

    .line 611
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDeviceArrayAdapter:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->notifyDataSetChanged()V

    .line 613
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 617
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 615
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateNearbyRefresh()V
    .locals 6

    .prologue
    const/16 v3, 0xc8

    .line 373
    const/4 v1, 0x1

    const-string v2, "updateNearbyRefresh()"

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 374
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnoxMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 375
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mDisabledByKnoxToast:Landroid/widget/Toast;

    if-nez v1, :cond_0

    .line 376
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "knox_feature_disabled_toast"

    const-string v4, "string"

    const-string v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mDisabledByKnoxToast:Landroid/widget/Toast;

    .line 377
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mDisabledByKnoxToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 378
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mDisabledByKnoxToast:Landroid/widget/Toast;

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showRefreshProgress()V
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->access$200(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;)V

    .line 382
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    const v2, 0x7f0c00b5

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 383
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v2, v2, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 384
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->requestFocus()Z

    .line 385
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_2

    .line 386
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 387
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 389
    :cond_2
    new-instance v1, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v0

    .line 390
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    const/16 v1, 0xf

    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto :goto_0
.end method

.method private updateRefresh()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showRefreshBtn()V

    .line 240
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showRefreshBtn()V

    .line 245
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateSectionCountText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbySection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 248
    :cond_1
    return-void
.end method

.method private updateSectionCountText(I)Ljava/lang/String;
    .locals 5
    .param p1, "count"    # I

    .prologue
    const/4 v2, 0x1

    .line 590
    if-ne p1, v2, :cond_0

    .line 591
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 593
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0017

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private updateSlinkRefresh()V
    .locals 4

    .prologue
    const/16 v2, 0x1f4

    .line 395
    const/4 v0, 0x1

    const-string v1, "updateSlinkRefresh()"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 396
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->requestRefresh()V

    .line 397
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->showRefreshProgress()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->access$200(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;)V

    .line 398
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    const v1, 0x7f0c00b5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 399
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mSectionCount:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 400
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSlinkSection:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$Section;->mRefreshProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->requestFocus()Z

    .line 401
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 403
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 405
    :cond_0
    return-void
.end method


# virtual methods
.method public OnAllShareStateChanged(II)V
    .locals 3
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 904
    const-string v1, "DevicesFragment - OnStateChanged"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 905
    const/4 v0, 0x0

    .line 906
    .local v0, "deviceCnt":I
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "com.samsung.android.nearby.mediaserver"

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPackageAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 907
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getRemoteDeviceList()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 908
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getRemoteDeviceList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 912
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DevicesFragment - deviceCnt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 914
    const/16 v1, 0x9

    if-ne p1, v1, :cond_1

    .line 915
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->refresh()V

    .line 917
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 258
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 259
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenu(Landroid/view/Menu;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 260
    const/4 v1, 0x7

    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 261
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 171
    const-string v1, "DevicesFragment - onCreateView() enter"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 173
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setHasOptionsMenu(Z)V

    .line 174
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setContext(Landroid/content/Context;)V

    .line 175
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 176
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 177
    new-instance v1, Lcom/samsung/everglades/video/myvideo/common/ListType;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 179
    const v1, 0x7f040003

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 180
    .local v0, "root":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setupDevicesView(Landroid/view/View;)V

    .line 181
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNetWorkFilter:Landroid/content/IntentFilter;

    .line 253
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 254
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 270
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 271
    .local v1, "itemId":I
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 272
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 273
    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 275
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    return v2
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 219
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 220
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->clearObserver()V

    .line 223
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->unregisterContentObserver()V

    .line 224
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->releaseWakeLock()V

    .line 227
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setDevcieListOnUpdatedListener(Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;)V

    .line 229
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNetWorkFilter:Landroid/content/IntentFilter;

    if-eqz v0, :cond_2

    .line 230
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNetWorkReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 233
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 234
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->updateRefresh()V

    .line 235
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 265
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->prepareOptionMenu(Landroid/view/Menu;)V

    .line 266
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 186
    const-string v0, "DevicesFragment - onResume() enter"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 187
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 188
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->setOnAllShareDeviceChangedObserver(Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;)V

    .line 189
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mWifiConntected:Z

    .line 191
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setImageCacheListener()V

    .line 193
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setSLinkDeviceAdapter()V

    .line 194
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 198
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setNearbyDeviceAdapter()V

    .line 199
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 203
    :cond_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->registerContentObserver()V

    .line 204
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->registerNetWorkReciver()V

    .line 206
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->acquireWakeLock(Landroid/content/Context;)V

    .line 208
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->setSyncPriority()V

    .line 209
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->updateEnabledAndGetBeRefreshed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->refresh()V

    .line 213
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setFocusSequence()V

    .line 214
    const-string v0, "DevicesFragment - onResume() end"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 215
    return-void
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 597
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setSLinkDeviceAdapter()V

    .line 598
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mSLinkDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 602
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setNearbyDeviceAdapter()V

    .line 603
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 604
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 607
    :cond_1
    return-void
.end method
