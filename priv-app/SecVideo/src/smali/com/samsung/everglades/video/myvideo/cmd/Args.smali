.class public Lcom/samsung/everglades/video/myvideo/cmd/Args;
.super Ljava/lang/Object;
.source "Args.java"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mBucketId:I

.field private mCount:I

.field private mCursor:Landroid/database/Cursor;

.field private mDbId:J

.field private mIsChecked:Z

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mMenu:Landroid/view/Menu;

.field private mMenuItemId:I

.field private mNIC:Ljava/lang/String;

.field private mPath:Ljava/lang/String;

.field private mProviderName:Ljava/lang/String;

.field private mSLinkDeviceId:I

.field private mSearchKey:Ljava/lang/String;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mBucketId:I

    .line 17
    iput v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mSLinkDeviceId:I

    .line 18
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mDbId:J

    .line 22
    iput v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mMenuItemId:I

    .line 23
    iput v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mCount:I

    .line 24
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mIsChecked:Z

    .line 30
    return-void
.end method


# virtual methods
.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public getAdapter()Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mAdapter:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mBucketId:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mCount:I

    return v0
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getDbId()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mDbId:J

    return-wide v0
.end method

.method public getIsChecked()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mIsChecked:Z

    return v0
.end method

.method public getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object v0
.end method

.method public getMenu()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method public getMenuItemId()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mMenuItemId:I

    return v0
.end method

.method public getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mNIC:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mProviderName:Ljava/lang/String;

    return-object v0
.end method

.method public getSLinkDeviceId()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mSLinkDeviceId:I

    return v0
.end method

.method public getSearchKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mSearchKey:Ljava/lang/String;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mView:Landroid/view/View;

    return-object v0
.end method

.method public setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mActivity:Landroid/app/Activity;

    .line 98
    return-object p0
.end method

.method public setAdapter(Landroid/widget/BaseAdapter;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "adapter"    # Landroid/widget/BaseAdapter;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mAdapter:Landroid/widget/BaseAdapter;

    .line 158
    return-object p0
.end method

.method public setBucketId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "bucketId"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mBucketId:I

    .line 118
    return-object p0
.end method

.method public setCount(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 147
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mCount:I

    .line 148
    return-object p0
.end method

.method public setCursor(Landroid/database/Cursor;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mCursor:Landroid/database/Cursor;

    .line 108
    return-object p0
.end method

.method public setDbId(J)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 122
    iput-wide p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mDbId:J

    .line 123
    return-object p0
.end method

.method public setIsChecked(Z)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "isChecked"    # Z

    .prologue
    .line 152
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mIsChecked:Z

    .line 153
    return-object p0
.end method

.method public setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 103
    return-object p0
.end method

.method public setMenu(Landroid/view/Menu;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mMenu:Landroid/view/Menu;

    .line 138
    return-object p0
.end method

.method public setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mMenuItemId:I

    .line 143
    return-object p0
.end method

.method public setNIC(Ljava/lang/String;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mNIC:Ljava/lang/String;

    .line 173
    return-object p0
.end method

.method public setPath(Ljava/lang/String;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mPath:Ljava/lang/String;

    .line 163
    return-object p0
.end method

.method public setProviderName(Ljava/lang/String;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mProviderName:Ljava/lang/String;

    .line 133
    return-object p0
.end method

.method public setSLinkDeviceId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 167
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mSLinkDeviceId:I

    .line 168
    return-object p0
.end method

.method public setSearchKey(Ljava/lang/String;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "searchKey"    # Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mSearchKey:Ljava/lang/String;

    .line 128
    return-object p0
.end method

.method public setView(Landroid/view/View;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Args;->mView:Landroid/view/View;

    .line 113
    return-object p0
.end method
