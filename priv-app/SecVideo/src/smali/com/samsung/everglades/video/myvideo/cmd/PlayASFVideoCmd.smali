.class public Lcom/samsung/everglades/video/myvideo/cmd/PlayASFVideoCmd;
.super Ljava/lang/Object;
.source "PlayASFVideoCmd.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/cmd/ICommand;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 12
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 19
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 20
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 22
    .local v0, "c":Landroid/database/Cursor;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getProviderName()Ljava/lang/String;

    move-result-object v6

    .line 23
    .local v6, "providerName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getNIC()Ljava/lang/String;

    move-result-object v4

    .line 25
    .local v4, "nic":Ljava/lang/String;
    const-string v8, "_data"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 26
    .local v5, "path":Ljava/lang/String;
    sget-object v8, Lcom/samsung/everglades/video/myvideo/common/DB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    const-string v9, "_id"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    .line 28
    .local v7, "uri":Landroid/net/Uri;
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v9, "video/*"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 29
    .local v3, "i":Landroid/content/Intent;
    const-string v8, "com.sec.android.app.videoplayer"

    const-string v9, "com.sec.android.app.videoplayer.activity.MoviePlayer"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    const-string v8, "CurrentProviderName"

    invoke-virtual {v3, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    const-string v8, "ListType"

    const/16 v9, 0xb

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 32
    const-string v8, "filePath"

    invoke-virtual {v3, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    const-string v8, "uri"

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 34
    const-string v8, "autoplay"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v9

    const-string v10, "autoPlay"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadBoolean(Ljava/lang/String;Z)Z

    move-result v9

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 35
    const-string v8, "NIC"

    invoke-virtual {v3, v8, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    const-string v8, "sortby"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v9

    const-string v10, "allsharesortorder"

    const/4 v11, 0x2

    invoke-virtual {v9, v10, v11}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v9

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 39
    :try_start_0
    invoke-virtual {v1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v2

    .line 41
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
