.class Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;
.super Landroid/os/Handler;
.source "FolderHoverViewer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v5, 0x9

    .line 313
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 314
    .local v2, "startIndex":I
    iget v1, p1, Landroid/os/Message;->arg2:I

    .line 315
    .local v1, "mode":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCount()I

    move-result v4

    # setter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mTotalCount:I
    invoke-static {v3, v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1302(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)I

    .line 316
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCount()I

    move-result v3

    sub-int v0, v3, v2

    .line 318
    .local v0, "displayThumbCount":I
    if-lt v0, v5, :cond_0

    .line 319
    const/16 v0, 0x9

    .line 322
    :cond_0
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 340
    :cond_1
    :goto_0
    return-void

    .line 324
    :pswitch_0
    if-nez v2, :cond_2

    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    .line 325
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->updateThumbnailLayout(I)V
    invoke-static {v3, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1400(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V

    .line 326
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->createFolderPreviewPopup(I)V
    invoke-static {v3, v0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1500(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V

    goto :goto_0

    .line 327
    :cond_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1600(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/widget/PopupWindow;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1600(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/widget/PopupWindow;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 328
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCount()I

    move-result v3

    if-le v3, v5, :cond_1

    .line 329
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->updateThumbnailLayout(I)V
    invoke-static {v3, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1400(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V

    goto :goto_0

    .line 335
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->dismissPopup()V
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    goto :goto_0

    .line 322
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
