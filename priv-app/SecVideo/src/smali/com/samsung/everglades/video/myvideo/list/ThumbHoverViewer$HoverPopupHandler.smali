.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;
.super Landroid/os/Handler;
.source "ThumbHoverViewer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HoverPopupHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method private constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .param p2, "x1"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x5

    .line 148
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 200
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 150
    :pswitch_1
    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mThumbHoverPopupSuspend:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$000()Z

    move-result v0

    if-nez v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->checkStopCondition()Z
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isOnCall(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    const-string v0, "ThumbHoverViewer - handleMessage() onCall"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->initViews()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$300(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    .line 159
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 164
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 170
    :pswitch_2
    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mThumbHoverPopupSuspend:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$000()Z

    move-result v0

    if-nez v0, :cond_3

    .line 171
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->show()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$500(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    goto :goto_0

    .line 174
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 176
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 183
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->dismiss()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$600(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    goto/16 :goto_0

    .line 191
    :pswitch_4
    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mThumbHoverPopupSuspend:Z
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$002(Z)Z

    goto/16 :goto_0

    .line 195
    :pswitch_5
    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mThumbHoverPopupSuspend:Z
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$002(Z)Z

    goto/16 :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
