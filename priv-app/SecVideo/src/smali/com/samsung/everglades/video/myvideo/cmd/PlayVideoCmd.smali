.class public Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;
.super Ljava/lang/Object;
.source "PlayVideoCmd.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/cmd/ICommand;


# instance fields
.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mPath:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private callPlayer(Lcom/samsung/everglades/video/myvideo/cmd/Args;)Z
    .locals 1
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPopupPlayerRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->openPopupPlayer(Lcom/samsung/everglades/video/myvideo/cmd/Args;)Z

    move-result v0

    .line 67
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->openFullPlayer(Lcom/samsung/everglades/video/myvideo/cmd/Args;)Z

    move-result v0

    goto :goto_0
.end method

.method private initVariables(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 3
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 43
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mPath:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    .line 45
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 46
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 47
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 49
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 51
    .local v0, "c":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    if-nez v1, :cond_0

    .line 52
    new-instance v1, Lcom/samsung/everglades/video/myvideo/common/ListType;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 55
    :cond_0
    if-eqz v0, :cond_1

    .line 56
    const-string v1, "_data"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mPath:Ljava/lang/String;

    .line 57
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mUri:Landroid/net/Uri;

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method private openFullPlayer(Lcom/samsung/everglades/video/myvideo/cmd/Args;)Z
    .locals 7
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    const/4 v6, 0x0

    .line 87
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 89
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "video/*"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 90
    .local v2, "i":Landroid/content/Intent;
    const-string v3, "com.sec.android.app.videoplayer"

    const-string v4, "com.sec.android.app.videoplayer.activity.MoviePlayer"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const-string v3, "bucket"

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getBucketId()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 92
    const-string v3, "search"

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getSearchKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v3, "ListType"

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListType()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 94
    const-string v3, "filePath"

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    const-string v3, "uri"

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    const-string v3, "autoplay"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v4

    const-string v5, "autoPlay"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 98
    const-string v3, "sortby"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v4

    const-string v5, "sortorder"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 99
    const-string v3, "cloud_option"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v4

    const-string v5, "lastCloudViewOption"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 101
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 102
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private openPopupPlayer(Lcom/samsung/everglades/video/myvideo/cmd/Args;)Z
    .locals 10
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 109
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v4

    .line 111
    .local v4, "util":Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mPath:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isDrmContent(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 131
    :goto_0
    return v5

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 115
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v2

    .line 117
    .local v2, "db":Lcom/samsung/everglades/video/myvideo/common/DB;
    new-instance v0, Landroid/content/ComponentName;

    const-string v7, "com.sec.android.app.videoplayer"

    const-string v8, "com.sec.android.app.videoplayer.miniapp.MiniVideoPlayerService"

    invoke-direct {v0, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.action.MINI_MODE_SERVICE"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    .line 120
    .local v3, "i":Landroid/content/Intent;
    const-string v7, "filePath"

    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    const-string v7, "currentID"

    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v8}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v8

    invoke-virtual {v3, v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 122
    const-string v7, "bucketid"

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getBucketId()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 123
    const-string v7, "resumePos"

    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v8}, Lcom/samsung/everglades/video/myvideo/common/DB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    const-string v7, "ListKey"

    const/16 v8, 0x14

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 125
    const-string v7, "uri"

    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const-string v7, "startFromList"

    invoke-virtual {v3, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 127
    const-string v7, "subtitleActivation"

    invoke-virtual {v3, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 128
    const-string v7, "autoplay"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v8

    const-string v9, "autoPlay"

    invoke-virtual {v8, v9, v5}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v3, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 130
    invoke-virtual {v1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move v5, v6

    .line 131
    goto :goto_0
.end method

.method private shouldStopPlay()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 72
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isContentReady(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isChinaModel()Z

    move-result v1

    if-nez v1, :cond_1

    .line 75
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0001

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 79
    :goto_0
    const/4 v0, 0x1

    .line 83
    :cond_0
    return v0

    .line 77
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0c010a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 3
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->initVariables(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 33
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->shouldStopPlay()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->callPlayer(Lcom/samsung/everglades/video/myvideo/cmd/Args;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "lastPlayedItem"

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
