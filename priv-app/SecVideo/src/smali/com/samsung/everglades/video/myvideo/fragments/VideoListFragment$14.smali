.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 1102
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)Z
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/touchwiz/widget/TwAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Lcom/sec/android/touchwiz/widget/TwAdapterView;, "Lcom/sec/android/touchwiz/widget/TwAdapterView<*>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1105
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isResumed()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1134
    :cond_0
    :goto_0
    return v3

    .line 1109
    :cond_1
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceSearchFileLists()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeleteAttr()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1110
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v5, v4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->showAnimation(Z)V

    .line 1113
    :cond_2
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1114
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$900(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 1115
    .local v1, "cursor":Landroid/database/Cursor;
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v6

    invoke-static {v5, v1, v6}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v2

    .line 1116
    .local v2, "mUri":Landroid/net/Uri;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    const-string v6, "cloud"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_3
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isKorSKTModel()Z

    move-result v5

    if-eqz v5, :cond_4

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.skp.tcloud"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1119
    :cond_4
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isMultiWindow(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isJustList()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1120
    :cond_5
    const v5, 0x7f09002c

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1121
    .local v0, "checkBox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1122
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v3, p2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setOnItemLongClickDrag(Landroid/view/View;)V

    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "mUri":Landroid/net/Uri;
    :goto_1
    move v3, v4

    .line 1134
    goto/16 :goto_0

    .line 1129
    :cond_6
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceSearchFileLists()Z

    move-result v3

    if-nez v3, :cond_7

    .line 1130
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->newLongClick(I)V
    invoke-static {v3, p3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1400(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;I)V

    goto :goto_1

    .line 1132
    :cond_7
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$14;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->searchLongClick(ILandroid/view/View;)V
    invoke-static {v3, p3, p2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;ILandroid/view/View;)V

    goto :goto_1
.end method
