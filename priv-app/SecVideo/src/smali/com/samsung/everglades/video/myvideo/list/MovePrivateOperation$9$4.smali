.class Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$4;
.super Ljava/lang/Object;
.source "MovePrivateOperation.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;)V
    .locals 0

    .prologue
    .line 904
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$4;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    .line 908
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 909
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$4;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRename:Z
    invoke-static {v1, v3}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$602(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    .line 910
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$4;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mReplace:Z
    invoke-static {v1, v3}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$902(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    .line 911
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$4;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameCancellation:Z
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$702(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    .line 913
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$4;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$2200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Ljava/lang/Runnable;

    move-result-object v2

    monitor-enter v2

    .line 915
    :try_start_0
    const-string v1, "MovePrivateOperation - cancel notify()"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 916
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$4;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$2200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 920
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 921
    if-eqz p1, :cond_0

    .line 922
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 925
    :cond_0
    return v3

    .line 917
    :catch_0
    move-exception v0

    .line 918
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 920
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
