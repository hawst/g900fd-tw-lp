.class public Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
.super Ljava/lang/Object;
.source "CloudUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;,
        Lcom/samsung/everglades/video/myvideo/common/CloudUtil$CloudCountChecker;,
        Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithTcloud;,
        Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithDropbox;
    }
.end annotation


# static fields
.field public static final ACTION_DROPBOX_AUTH:Ljava/lang/String; = "com.sec.android.cloudagent.DROPBOX_AUTH"

.field public static final ACTION_SETUP_DROPBOX_ACCOUNT:I = 0x1194

.field private static final BAIDU_SERVER:Ljava/lang/String; = "BAIDU"

.field public static final BUCKET_DISPLAY_NAME:Ljava/lang/String; = "bucket_display_name"

.field public static final BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field public static final CLOUD_CACHED_PATH:Ljava/lang/String; = "cloud_cached_path"

.field public static final CLOUD_IS_CACHED:Ljava/lang/String; = "cloud_is_cached"

.field public static final CLOUD_SERVER_ID:Ljava/lang/String; = "cloud_server_id"

.field public static final CLOUD_THUMB_PATH:Ljava/lang/String; = "cloud_thumb_path"

.field public static final CLOUD_URI:Landroid/net/Uri;

.field public static final DATA:Ljava/lang/String; = "_data"

.field public static final DATE_MODIFIED:Ljava/lang/String; = "date_modified"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "_display_name"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field private static final FOLDER_SORT_ORDER:Ljava/lang/String; = "bucket_display_name COLLATE LOCALIZED ASC"

.field private static final HANDLE_CLOUD_DB_COUNT_CHECKER:I = 0x64

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final MIME_TYPE:Ljava/lang/String; = "mime_type"

.field public static final REQUEST_SIGNIN:Ljava/lang/String; = "requestSignIn"

.field public static final SIZE:Ljava/lang/String; = "_size"

.field public static SKTCLOUD_IS_CACHED:Ljava/lang/String; = null

.field public static SKTCLOUD_SERVER_ID:Ljava/lang/String; = null

.field public static SKTCLOUD_URI:Landroid/net/Uri; = null

.field private static final SORT_ORDER:Ljava/lang/String; = "_display_name COLLATE LOCALIZED ASC"

.field private static final TAG:Ljava/lang/String; = "CloudUtil"

.field public static final TITLE:Ljava/lang/String; = "title"

.field private static isCheckedSKTCloudServiceFeature:Z

.field private static mBaiduCloudAgentExistence:Z

.field private static mCloudAgentExistence:Z

.field private static mCloudCount:I

.field private static mSKTCloudAgentExistence:Z

.field private static mSKTCloudServiceEnable:Z

.field private static mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;


# instance fields
.field mCloudContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mHandler:Landroid/os/Handler;

.field private final mHandlerThread:Ljava/lang/Thread;

.field private mOnCloudDBCountChanged:Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;

.field private mOnlyDropboxContent:Z

.field private mOnlyTcloudContent:Z

.field private mResolver:Landroid/content/ContentResolver;

.field mSKTCloudContentObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Videos;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    .line 65
    sput-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    .line 66
    sput-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_SERVER_ID:Ljava/lang/String;

    .line 67
    sput-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_IS_CACHED:Ljava/lang/String;

    .line 74
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    .line 75
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    .line 76
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudServiceEnable:Z

    .line 77
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCheckedSKTCloudServiceFeature:Z

    .line 78
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    .line 1409
    const/4 v0, -0x1

    sput v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudCount:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    .line 81
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 82
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    .line 86
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyDropboxContent:Z

    .line 87
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyTcloudContent:Z

    .line 1364
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$5;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$5;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandlerThread:Ljava/lang/Thread;

    .line 1393
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$6;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$6;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudContentObserver:Landroid/database/ContentObserver;

    .line 1401
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$7;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$7;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudContentObserver:Landroid/database/ContentObserver;

    .line 90
    if-eqz p1, :cond_2

    .line 91
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    .line 92
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    .line 93
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 94
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->checkDropboxCloudAgentExistence()V

    .line 95
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->syncWithDropbox()V

    .line 96
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->checkSKTCloudServiceFeature()V

    .line 97
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isChinaModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->checkBaiduCloudAgentExistence()V

    .line 100
    :cond_0
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudServiceEnable:Z

    if-eqz v0, :cond_1

    .line 101
    invoke-static {}, Lcom/skp/tcloud/service/lib/TcloudStore$Videos;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    .line 102
    const-string v0, "tcloud_server_id"

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_SERVER_ID:Ljava/lang/String;

    .line 103
    const-string v0, "tcloud_is_cached"

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_IS_CACHED:Ljava/lang/String;

    .line 104
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->checkSKTCloudAgentExistence()V

    .line 105
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->initAndSyncSKTCloud()V

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandlerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_2

    .line 108
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandlerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 111
    :cond_2
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 39
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    return v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 39
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 39
    sget v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudCount:I

    return v0
.end method

.method static synthetic access$502(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 39
    sput p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudCount:I

    return p0
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnCloudDBCountChanged:Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;

    return-object v0
.end method

.method private checkBaiduCloudAgentExistence()V
    .locals 4

    .prologue
    .line 1125
    const/4 v1, 0x0

    .line 1127
    .local v1, "vendorName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v2, :cond_2

    .line 1128
    :cond_0
    const/4 v2, 0x0

    sput-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    .line 1139
    :cond_1
    :goto_0
    return-void

    .line 1131
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/cloudagent/CloudStore$API;->getCloudVendorName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1132
    if-eqz v1, :cond_1

    const-string v2, "BAIDU"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1133
    const/4 v2, 0x1

    sput-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1135
    :catch_0
    move-exception v0

    .line 1136
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "CloudUtil"

    const-string v3, "IllegalArgumentException e"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private checkDropboxCloudAgentExistence()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1107
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 1108
    sput-boolean v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    .line 1122
    :goto_0
    return-void

    .line 1110
    :cond_0
    const/4 v0, 0x0

    .line 1112
    .local v0, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.cloudagent"

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1116
    :goto_1
    if-nez v0, :cond_1

    .line 1117
    sput-boolean v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    goto :goto_0

    .line 1119
    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    goto :goto_0

    .line 1113
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private checkSKTCloudAgentExistence()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1463
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 1464
    sput-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    .line 1474
    :goto_0
    return-void

    .line 1465
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->existsTcloudAccount(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1466
    sput-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    goto :goto_0

    .line 1469
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->isCloudAvailable(Landroid/content/Context;)Z

    move-result v1

    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1470
    :catch_0
    move-exception v0

    .line 1471
    .local v0, "e":Ljava/lang/Exception;
    sput-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    goto :goto_0
.end method

.method private checkSKTCloudServiceFeature()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1444
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCheckedSKTCloudServiceFeature:Z

    if-eqz v1, :cond_0

    .line 1460
    :goto_0
    return-void

    .line 1446
    :cond_0
    const/4 v0, 0x0

    .line 1449
    .local v0, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.skp.tcloud.agent"

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1453
    :goto_1
    if-nez v0, :cond_1

    .line 1454
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudServiceEnable:Z

    .line 1459
    :goto_2
    sput-boolean v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCheckedSKTCloudServiceFeature:Z

    goto :goto_0

    .line 1456
    :cond_1
    sput-boolean v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudServiceEnable:Z

    goto :goto_2

    .line 1450
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public static declared-synchronized destroyInstance()V
    .locals 2

    .prologue
    .line 122
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 124
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :cond_0
    monitor-exit v1

    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private existsTcloudAccount(Landroid/content/Context;)Z
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 1477
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1478
    .local v1, "am":Landroid/accounts/AccountManager;
    const/4 v0, 0x0

    .line 1480
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v1, :cond_0

    .line 1481
    const-string v2, "com.skp.tcloud"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 1484
    :cond_0
    if-eqz v0, :cond_1

    array-length v2, v0

    if-lez v2, :cond_1

    .line 1485
    const/4 v2, 0x1

    .line 1487
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 114
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-nez v0, :cond_0

    .line 115
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 118
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static final getCloudFolderColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 140
    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "cloud_server_id"

    aput-object v2, v0, v1

    .line 143
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method private static final getCloudVideoColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 133
    const/16 v1, 0x9

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "cloud_server_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "cloud_is_cached"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "resumePos"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "isPlayed"

    aput-object v2, v0, v1

    .line 136
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method private getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 1269
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 1270
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 1271
    const/4 p2, 0x0

    .line 1274
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1275
    const/4 p1, 0x0

    .line 1284
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_0
    return-object p1

    .line 1277
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isLocalOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1280
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object p1, p2

    .line 1281
    goto :goto_0

    .line 1284
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mergeCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method private getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "searchKey"    # Ljava/lang/String;

    .prologue
    .line 161
    if-nez p1, :cond_0

    const-string v0, "_"

    .line 167
    :goto_0
    return-object v0

    .line 163
    :cond_0
    const-string v0, "!"

    const-string v1, "!!"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 164
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 165
    const-string v0, "_"

    const-string v1, "!_"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 166
    const-string v0, "%"

    const-string v1, "!%"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    .line 167
    goto :goto_0
.end method

.method private static final getSKTCloudFolderColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 154
    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_SERVER_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 157
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method private static final getSKTCloudVideoColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 147
    const/16 v1, 0x9

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_SERVER_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_IS_CACHED:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "resumePos"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "isPlayed"

    aput-object v2, v0, v1

    .line 150
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method private getSelectionString(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "searchKey"    # Ljava/lang/String;
    .param p2, "bucketID"    # I

    .prologue
    .line 181
    const/4 v0, 0x0

    .line 182
    .local v0, "where":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bucket_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_display_name like \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 189
    if-eqz v0, :cond_2

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 195
    :cond_1
    :goto_0
    return-object v0

    .line 192
    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method private getSortOrderString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 305
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "sortorder"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    .line 307
    .local v0, "sortOrder":I
    packed-switch v0, :pswitch_data_0

    .line 324
    const-string v1, "_display_name COLLATE LOCALIZED ASC"

    :goto_0
    return-object v1

    .line 309
    :pswitch_0
    const-string v1, "isPlayed DESC"

    goto :goto_0

    .line 312
    :pswitch_1
    const-string v1, "date_modified DESC"

    goto :goto_0

    .line 315
    :pswitch_2
    const-string v1, "_display_name COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 318
    :pswitch_3
    const-string v1, "_size DESC"

    goto :goto_0

    .line 321
    :pswitch_4
    const-string v1, "mime_type ASC"

    goto :goto_0

    .line 307
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 1303
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initAndSyncSKTCloud()V
    .locals 1

    .prologue
    .line 1492
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1498
    :cond_0
    :goto_0
    return-void

    .line 1494
    :cond_1
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_0

    .line 1495
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->init(Landroid/content/Context;)V

    .line 1496
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->syncWithTcloud()V

    goto :goto_0
.end method

.method private isCloudOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 1295
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDataOn()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1314
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 1319
    :cond_0
    :goto_0
    return v1

    .line 1315
    :cond_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1316
    .local v0, "tpManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 1317
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isFirstTime()Z
    .locals 3

    .prologue
    .line 1232
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1234
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "is_first_time_without_dropbox_account"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private isLocalOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 1299
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isViewDropbox()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 171
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "lastCloudViewOption"

    invoke-virtual {v2, v3, v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    .line 172
    .local v0, "viewOption":I
    if-eqz v0, :cond_0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private isViewTcloud()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 176
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "lastCloudViewOption"

    invoke-virtual {v2, v3, v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    .line 177
    .local v0, "viewOption":I
    if-eqz v0, :cond_0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private isWifiOn()Z
    .locals 3

    .prologue
    .line 1307
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 1310
    :goto_0
    return v1

    .line 1309
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1310
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method private mergeCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 1288
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/database/Cursor;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 1291
    .local v0, "cursorArray":[Landroid/database/Cursor;
    new-instance v1, Landroid/database/MergeCursor;

    invoke-direct {v1, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v1
.end method

.method private queryMergedCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "where"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 200
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getCloudVideoColumns()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getSortOrderString()Ljava/lang/String;

    move-result-object v5

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 203
    :goto_0
    return-object v0

    .line 201
    :catch_0
    move-exception v6

    .line 202
    .local v6, "e":Landroid/database/sqlite/SQLiteException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloudUtil - queryMergedCursor - SQLiteException :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    move-object v0, v7

    .line 203
    goto :goto_0
.end method

.method private queryMergedCursorFolder()Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 252
    const-string v3, "1 = 1) GROUP BY ( bucket_id"

    .line 253
    .local v3, "where":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getCloudFolderColumns()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 254
    .local v6, "cloudCursor":Landroid/database/Cursor;
    return-object v6
.end method

.method private showFirstCloudActivationDialog()V
    .locals 3

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 1266
    :goto_0
    return-void

    .line 1246
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c00c9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0044

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0097

    new-instance v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$4;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$4;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c000d

    new-instance v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$3;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$3;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private syncWithDropbox()V
    .locals 2

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1145
    :cond_0
    :goto_0
    return-void

    .line 1144
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCloudActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithDropbox;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithDropbox;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithDropbox;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private syncWithTcloud()V
    .locals 2

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1151
    :cond_0
    :goto_0
    return-void

    .line 1150
    :cond_1
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithTcloud;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithTcloud;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithTcloud;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private takeMeHome()V
    .locals 3

    .prologue
    .line 1238
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 1241
    :goto_0
    return-void

    .line 1240
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "is_first_time_without_dropbox_account"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public checkInitializedState()V
    .locals 1

    .prologue
    .line 1094
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_1

    .line 1095
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isFirstTime()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1096
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCloudActivated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1097
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->showFirstCloudActivationDialog()V

    .line 1099
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->takeMeHome()V

    .line 1104
    :cond_1
    :goto_0
    return-void

    .line 1101
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->syncWithDropbox()V

    goto :goto_0
.end method

.method public deleteCloudFolder(I)I
    .locals 4
    .param p1, "bucketId"    # I

    .prologue
    .line 1089
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bucket_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1090
    .local v0, "where":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public deleteContent(J)Z
    .locals 7
    .param p1, "videoId"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1071
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v3, :cond_2

    :cond_0
    move v1, v2

    .line 1077
    :cond_1
    :goto_0
    return v1

    .line 1073
    :cond_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1074
    .local v0, "deletedContentCount":I
    if-eq v0, v1, :cond_1

    move v1, v2

    .line 1077
    goto :goto_0
.end method

.method public deleteContent(Landroid/net/Uri;)Z
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1082
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v3, :cond_2

    :cond_0
    move v1, v2

    .line 1085
    :cond_1
    :goto_0
    return v1

    .line 1084
    :cond_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v3, p1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1085
    .local v0, "deletedContentCount":I
    if-eq v0, v1, :cond_1

    move v1, v2

    goto :goto_0
.end method

.method public downloadCloudFile(Landroid/net/Uri;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1173
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 1193
    :cond_0
    :goto_0
    return-void

    .line 1175
    :cond_1
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v1, :cond_0

    .line 1176
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f0c00c4

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1177
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$1;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Landroid/net/Uri;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1191
    .local v0, "loadingThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public downloadCloudFile(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1196
    .local p1, "selectedListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object v1, p1

    .line 1198
    .local v1, "mUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 1219
    :cond_0
    :goto_0
    return-void

    .line 1200
    :cond_1
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v2, :cond_0

    .line 1201
    :cond_2
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$2;

    invoke-direct {v2, p0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$2;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Ljava/util/ArrayList;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1217
    .local v0, "loadingThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public getCloudCachedPath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v10, 0x0

    .line 1038
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_1

    move-object v9, v10

    .line 1067
    :cond_0
    :goto_0
    return-object v9

    .line 1040
    :cond_1
    if-nez p1, :cond_2

    move-object v9, v10

    goto :goto_0

    .line 1042
    :cond_2
    const/4 v7, 0x0

    .line 1043
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 1044
    .local v6, "cached":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1045
    .local v9, "localPath":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "cloud_is_cached"

    aput-object v0, v2, v1

    const-string v0, "cloud_cached_path"

    aput-object v0, v2, v3

    .line 1050
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1051
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1052
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1053
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 1058
    :cond_3
    if-eqz v7, :cond_4

    .line 1059
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1060
    const/4 v7, 0x0

    .line 1064
    :cond_4
    :goto_1
    if-eqz v6, :cond_5

    const-string v0, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    move-object v9, v10

    .line 1067
    goto :goto_0

    .line 1055
    :catch_0
    move-exception v8

    .line 1056
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058
    if-eqz v7, :cond_4

    .line 1059
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1060
    const/4 v7, 0x0

    goto :goto_1

    .line 1058
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_6

    .line 1059
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1060
    const/4 v7, 0x0

    :cond_6
    throw v0
.end method

.method public getCloudTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "videoTitle"    # Ljava/lang/String;

    .prologue
    .line 640
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 641
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 642
    :cond_0
    return-object p1
.end method

.method public getCursorByBucketId(I)Landroid/database/Cursor;
    .locals 7
    .param p1, "bucketID"    # I

    .prologue
    const/4 v2, 0x0

    .line 238
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_2

    :cond_1
    move-object v6, v2

    .line 248
    :goto_0
    return-object v6

    .line 240
    :cond_2
    const/4 v6, 0x0

    .line 241
    .local v6, "cloudCursor":Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 243
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudFolder(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 244
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    const-string v5, "_display_name COLLATE LOCALIZED ASC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    .line 246
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    const-string v5, "_display_name COLLATE LOCALIZED ASC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0
.end method

.method public getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;IZ)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "searchKey"    # Ljava/lang/String;
    .param p3, "bucketID"    # I
    .param p4, "searchList"    # Z

    .prologue
    .line 208
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_2

    :cond_1
    move-object v0, p1

    .line 223
    :goto_0
    return-object v0

    .line 210
    :cond_2
    const/4 v3, 0x0

    .line 211
    .local v3, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 213
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p2, p3}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getSelectionString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 215
    if-nez p4, :cond_3

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isViewDropbox()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 216
    :cond_3
    invoke-direct {p0, v3}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->queryMergedCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 219
    :cond_4
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_6

    if-nez p4, :cond_5

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isViewTcloud()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 220
    :cond_5
    invoke-direct {p0, p1, v6}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    .line 221
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getSKTCloudVideoColumns()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getSortOrderString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 223
    :cond_6
    invoke-direct {p0, p1, v6}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public getMergedCursorDropboxOnly(Landroid/database/Cursor;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "searchKey"    # Ljava/lang/String;
    .param p3, "bucketID"    # I

    .prologue
    .line 227
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v2, :cond_1

    .line 234
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 229
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    const/4 v1, 0x0

    .line 230
    .local v1, "where":Ljava/lang/String;
    const/4 v0, 0x0

    .line 232
    .local v0, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p2, p3}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getSelectionString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 233
    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->queryMergedCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 234
    invoke-direct {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method public getMergedCursorFolder(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 258
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_3

    :cond_1
    move-object v4, p1

    .line 283
    :cond_2
    :goto_0
    return-object v4

    .line 260
    :cond_3
    const/4 v6, 0x0

    .line 262
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isViewDropbox()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 263
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->queryMergedCursorFolder()Landroid/database/Cursor;

    move-result-object v6

    .line 266
    :cond_4
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isViewTcloud()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 267
    invoke-direct {p0, p1, v6}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 268
    invoke-direct {p0, p1, v6}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    .line 270
    :cond_5
    const-string v3, "1 = 1 GROUP BY bucket_id"

    .line 271
    .local v3, "where":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getSKTCloudFolderColumns()[Ljava/lang/String;

    move-result-object v2

    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 273
    .end local v3    # "where":Ljava/lang/String;
    :cond_6
    invoke-direct {p0, p1, v6}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 276
    invoke-direct {p0, p1, v6}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isLocalOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v4, p1

    .line 277
    goto :goto_0

    .line 279
    :cond_7
    invoke-direct {p0, p1, v6}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v4, v6

    .line 280
    goto :goto_0

    .line 283
    :cond_8
    invoke-direct {p0, p1, v6}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mergeCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0
.end method

.method public getMergedCursorFolderDropboxOnly(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 287
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v1, :cond_1

    .line 301
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 289
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->queryMergedCursorFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 291
    .local v0, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 292
    const/4 p1, 0x0

    goto :goto_0

    .line 294
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isLocalOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 297
    invoke-direct {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object p1, v0

    .line 298
    goto :goto_0

    .line 301
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mergeCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method public getProviderUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 626
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_1

    .line 627
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 628
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    .line 636
    :goto_0
    return-object v0

    .line 630
    :cond_1
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_2

    .line 631
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 632
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    goto :goto_0

    .line 636
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSKTCloudVideoUriById(J)Landroid/net/Uri;
    .locals 3
    .param p1, "id"    # J

    .prologue
    const/4 v0, 0x0

    .line 1501
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 1506
    :cond_0
    :goto_0
    return-object v0

    .line 1503
    :cond_1
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v1, :cond_0

    .line 1504
    invoke-static {p1, p2}, Lcom/skp/tcloud/service/lib/TcloudStore$Videos;->getVideoUri(J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getStringDateTaken(Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 845
    const/4 v7, 0x0

    .line 846
    .local v7, "dateString":Ljava/lang/String;
    const/4 v6, 0x0

    .line 847
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "date_modified"

    aput-object v0, v2, v1

    .line 852
    .local v2, "cols":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 853
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 854
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 855
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 860
    :cond_0
    if-eqz v6, :cond_1

    .line 861
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 862
    const/4 v6, 0x0

    .line 866
    :cond_1
    :goto_0
    return-object v7

    .line 857
    :catch_0
    move-exception v8

    .line 858
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 860
    if-eqz v6, :cond_1

    .line 861
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 862
    const/4 v6, 0x0

    goto :goto_0

    .line 860
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 861
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 862
    const/4 v6, 0x0

    :cond_2
    throw v0
.end method

.method public getTcloudCount()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 749
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v0, :cond_1

    const/4 v6, 0x0

    .line 758
    :cond_0
    :goto_0
    return v6

    .line 751
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 752
    .local v7, "checkCursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 753
    .local v6, "checkCount":I
    if-eqz v7, :cond_0

    .line 754
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 755
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 756
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public getUriByFilepath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 646
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 650
    :cond_0
    :goto_0
    return-object v0

    .line 648
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 650
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v1, p1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public isBaiduCachedContent(J)Z
    .locals 5
    .param p1, "videoId"    # J

    .prologue
    const/4 v1, 0x0

    .line 1029
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v2, :cond_1

    .line 1034
    :cond_0
    :goto_0
    return v1

    .line 1030
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    .line 1032
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1034
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduCachedContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isBaiduCachedContent(Landroid/database/Cursor;)Z
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 1012
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v3, :cond_1

    .line 1025
    :cond_0
    :goto_0
    return v2

    .line 1013
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/database/Cursor;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1015
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1018
    const/4 v1, 0x0

    .line 1020
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    const-string v3, "cloud_is_cached"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1025
    :goto_1
    if-eqz v1, :cond_0

    const-string v3, "1"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 1021
    :catch_0
    move-exception v0

    .line 1022
    .local v0, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public isBaiduCachedContent(Landroid/net/Uri;)Z
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 977
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v0, :cond_1

    .line 999
    :cond_0
    :goto_0
    return v10

    .line 978
    :cond_1
    if-eqz p1, :cond_0

    .line 980
    const/4 v7, 0x0

    .line 981
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 982
    .local v6, "cached":Ljava/lang/String;
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "cloud_is_cached"

    aput-object v0, v2, v10

    .line 987
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 989
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 993
    :cond_2
    if-eqz v7, :cond_3

    .line 994
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 995
    const/4 v7, 0x0

    .line 999
    :cond_3
    :goto_1
    if-eqz v6, :cond_5

    const-string v0, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v9

    :goto_2
    move v10, v0

    goto :goto_0

    .line 990
    :catch_0
    move-exception v8

    .line 991
    .local v8, "e":Landroid/database/CursorIndexOutOfBoundsException;
    :try_start_1
    invoke-virtual {v8}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 993
    if-eqz v7, :cond_3

    .line 994
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 995
    const/4 v7, 0x0

    goto :goto_1

    .line 993
    .end local v8    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 994
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 995
    const/4 v7, 0x0

    :cond_4
    throw v0

    :cond_5
    move v0, v10

    .line 999
    goto :goto_2
.end method

.method public isBaiduCachedContent(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1003
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v2, :cond_1

    .line 1008
    :cond_0
    :goto_0
    return v1

    .line 1004
    :cond_1
    if-eqz p1, :cond_0

    .line 1006
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1008
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduCachedContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isBaiduCloudAvailable()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 683
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v3, :cond_1

    :cond_0
    move v1, v2

    .line 693
    :goto_0
    return v1

    .line 685
    :cond_1
    const/4 v1, 0x0

    .line 688
    .local v1, "isAvailable":Z
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/cloudagent/CloudStore$API;->isCloudAvailable(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 689
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move v1, v2

    .line 690
    goto :goto_0
.end method

.method public isBaiduContent(J)Z
    .locals 3
    .param p1, "videoId"    # J

    .prologue
    .line 620
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 622
    :goto_0
    return v1

    .line 621
    :cond_1
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 622
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isBaiduContent(Landroid/database/Cursor;)Z
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    .line 578
    sget-boolean v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v4, :cond_0

    sget-boolean v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v4, :cond_1

    .line 595
    :cond_0
    :goto_0
    return v3

    .line 579
    :cond_1
    if-eqz p1, :cond_0

    .line 581
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 583
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 585
    :cond_2
    const-string v4, "cloud_server_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 586
    .local v2, "inServerIndex":I
    const/4 v1, 0x0

    .line 588
    .local v1, "inCloud":Ljava/lang/String;
    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    .line 589
    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 595
    :cond_3
    :goto_1
    if-eqz v1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    .line 591
    :catch_0
    move-exception v0

    .line 592
    .local v0, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public isBaiduContent(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 610
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 616
    :cond_0
    :goto_0
    return v0

    .line 611
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 613
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_2

    .line 614
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 616
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isBaiduContent(Ljava/lang/String;)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 599
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 605
    :cond_0
    :goto_0
    return v0

    .line 600
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 602
    const-wide/16 v2, -0x1

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v1, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 603
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isBaiduServerOnlyContent(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 800
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 801
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduCachedContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isBaiduServerOnlyContent(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 795
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 796
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCloudAvailable()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 697
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 702
    :cond_0
    :goto_0
    return v0

    .line 699
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCloudAvailable()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudAvailable()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduCloudAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 700
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCloudServerOnlyContent(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 762
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 767
    :cond_0
    :goto_0
    return v0

    .line 764
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxServerOnlyContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudServerOnlyContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduServerOnlyContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 765
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCloudTypeContent(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 453
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 458
    :cond_0
    :goto_0
    return v0

    .line 455
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 456
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCloudTypeContent(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 462
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 467
    :cond_0
    :goto_0
    return v0

    .line 464
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 465
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized isCloudTypeContent(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 471
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 476
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 473
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 474
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 471
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isContentInDropbox()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1518
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1519
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "lastCloudViewOption"

    invoke-virtual {v1, v2, v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1521
    :cond_0
    return v0
.end method

.method public isContentInPhone()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1510
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1511
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "lastCloudViewOption"

    invoke-virtual {v2, v3, v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1513
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 1511
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1513
    goto :goto_0
.end method

.method public isContentInTcloud()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1526
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1527
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "lastCloudViewOption"

    invoke-virtual {v1, v2, v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1529
    :cond_0
    return v0
.end method

.method public isContentReady(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 811
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 813
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCachedContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCachedContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduCachedContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDataNetworkConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isContentReady(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 805
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 807
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDataNetworkConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDataNetworkConnected()Z
    .locals 1

    .prologue
    .line 371
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isWifiOn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDataOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    :cond_0
    const/4 v0, 0x1

    .line 374
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDropboxCachedContent(J)Z
    .locals 5
    .param p1, "videoId"    # J

    .prologue
    const/4 v1, 0x0

    .line 901
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v2, :cond_1

    .line 908
    :cond_0
    :goto_0
    return v1

    .line 902
    :cond_1
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v2, :cond_0

    .line 904
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    .line 906
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 908
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCachedContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isDropboxCachedContent(Landroid/database/Cursor;)Z
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 881
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v3, :cond_1

    .line 896
    :cond_0
    :goto_0
    return v2

    .line 882
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/database/Cursor;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 884
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v3, :cond_0

    .line 886
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 889
    const/4 v1, 0x0

    .line 891
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    const-string v3, "cloud_is_cached"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 896
    :goto_1
    if-eqz v1, :cond_0

    const-string v3, "1"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 892
    :catch_0
    move-exception v0

    .line 893
    .local v0, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public isDropboxCachedContent(Landroid/net/Uri;)Z
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 817
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_1

    .line 841
    :cond_0
    :goto_0
    return v10

    .line 818
    :cond_1
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v0, :cond_0

    .line 820
    if-eqz p1, :cond_0

    .line 822
    const/4 v7, 0x0

    .line 823
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 824
    .local v6, "cached":Ljava/lang/String;
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "cloud_is_cached"

    aput-object v0, v2, v10

    .line 829
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 831
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 835
    :cond_2
    if-eqz v7, :cond_3

    .line 836
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 837
    const/4 v7, 0x0

    .line 841
    :cond_3
    :goto_1
    if-eqz v6, :cond_5

    const-string v0, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v9

    :goto_2
    move v10, v0

    goto :goto_0

    .line 832
    :catch_0
    move-exception v8

    .line 833
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 835
    if-eqz v7, :cond_3

    .line 836
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 837
    const/4 v7, 0x0

    goto :goto_1

    .line 835
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 836
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 837
    const/4 v7, 0x0

    :cond_4
    throw v0

    :cond_5
    move v0, v10

    .line 841
    goto :goto_2
.end method

.method public isDropboxCachedContent(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 870
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v2, :cond_1

    .line 877
    :cond_0
    :goto_0
    return v1

    .line 871
    :cond_1
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v2, :cond_0

    .line 873
    if-eqz p1, :cond_0

    .line 875
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 877
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCachedContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isDropboxCloudAccountCheck(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 706
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 711
    :cond_0
    :goto_0
    return v0

    .line 708
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudAvailable()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 709
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCloudVideoAvailable()Z

    move-result v0

    goto :goto_0
.end method

.method public isDropboxCloudActivated()Z
    .locals 1

    .prologue
    .line 129
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    return v0
.end method

.method public isDropboxCloudAvailable()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 654
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v3, :cond_0

    move v1, v2

    .line 665
    :goto_0
    return v1

    .line 655
    :cond_0
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-eqz v3, :cond_1

    move v1, v2

    goto :goto_0

    .line 657
    :cond_1
    const/4 v1, 0x0

    .line 660
    .local v1, "isAvailable":Z
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/cloudagent/CloudStore$API;->isCloudAvailable(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 661
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move v1, v2

    .line 662
    goto :goto_0
.end method

.method public isDropboxCloudVideoAvailable()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 715
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    move v0, v8

    .line 728
    :goto_0
    return v0

    .line 717
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 719
    .local v7, "checkCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 720
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 721
    .local v6, "checkCount":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 722
    const/4 v7, 0x0

    .line 723
    if-lez v6, :cond_1

    .line 724
    const/4 v0, 0x1

    goto :goto_0

    .end local v6    # "checkCount":I
    :cond_1
    move v0, v8

    .line 728
    goto :goto_0
.end method

.method public isDropboxContent(J)Z
    .locals 3
    .param p1, "videoId"    # J

    .prologue
    const/4 v1, 0x0

    .line 445
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v2, :cond_1

    .line 449
    :cond_0
    :goto_0
    return v1

    .line 446
    :cond_1
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v2, :cond_0

    .line 448
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 449
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isDropboxContent(Landroid/database/Cursor;)Z
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    .line 394
    sget-boolean v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v4, :cond_1

    .line 413
    :cond_0
    :goto_0
    return v3

    .line 395
    :cond_1
    sget-boolean v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v4, :cond_0

    .line 397
    if-eqz p1, :cond_0

    .line 399
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 401
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 403
    :cond_2
    const-string v4, "cloud_server_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 404
    .local v2, "inServerIndex":I
    const/4 v1, 0x0

    .line 406
    .local v1, "inCloud":Ljava/lang/String;
    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    .line 407
    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 413
    :cond_3
    :goto_1
    if-eqz v1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    .line 409
    :catch_0
    move-exception v0

    .line 410
    .local v0, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public isDropboxContent(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 432
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 441
    :cond_0
    :goto_0
    return v0

    .line 433
    :cond_1
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_0

    .line 435
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 437
    const-string v1, "CloudUtil"

    const-string v2, "isDropboxContent(uri)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_2

    .line 439
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 441
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isDropboxContent(Ljava/lang/String;)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 417
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 427
    :cond_0
    :goto_0
    return v0

    .line 418
    :cond_1
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-nez v1, :cond_0

    .line 420
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 422
    const-string v1, "CloudUtil"

    const-string v2, "isDropboxContent(string)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    const-wide/16 v2, -0x1

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v1, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 425
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDropboxFolder(I)Z
    .locals 8
    .param p1, "bucketID"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 480
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    move v0, v7

    .line 497
    :goto_0
    return v0

    .line 481
    :cond_0
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mBaiduCloudAgentExistence:Z

    if-eqz v0, :cond_1

    move v0, v7

    goto :goto_0

    .line 483
    :cond_1
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    move v0, v7

    goto :goto_0

    .line 485
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_3

    move v0, v7

    goto :goto_0

    .line 487
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 488
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_5

    .line 489
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 490
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 491
    const/4 v6, 0x0

    .line 492
    const/4 v0, 0x1

    goto :goto_0

    .line 494
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 495
    const/4 v6, 0x0

    :cond_5
    move v0, v7

    .line 497
    goto :goto_0
.end method

.method public isDropboxServerOnlyContent(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 777
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 779
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCachedContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDropboxServerOnlyContent(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 771
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 773
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isOnlyDropboxContent()Z
    .locals 1

    .prologue
    .line 1560
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyDropboxContent:Z

    return v0
.end method

.method public isOnlyTcloudContent()Z
    .locals 1

    .prologue
    .line 1590
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyTcloudContent:Z

    return v0
.end method

.method public isSKTCachedContent(J)Z
    .locals 5
    .param p1, "videoId"    # J

    .prologue
    const/4 v1, 0x0

    .line 967
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v2, :cond_1

    .line 973
    :cond_0
    :goto_0
    return v1

    .line 969
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    .line 971
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 973
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCachedContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isSKTCachedContent(Landroid/database/Cursor;)Z
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 949
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v3, :cond_1

    .line 963
    :cond_0
    :goto_0
    return v2

    .line 951
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/database/Cursor;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduCachedContent(Landroid/database/Cursor;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 953
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 956
    const/4 v1, 0x0

    .line 958
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_IS_CACHED:Ljava/lang/String;

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 963
    :goto_1
    if-eqz v1, :cond_0

    const-string v3, "1"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 959
    :catch_0
    move-exception v0

    .line 960
    .local v0, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public isSKTCachedContent(Landroid/net/Uri;)Z
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 912
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v0, :cond_1

    .line 935
    :cond_0
    :goto_0
    return v10

    .line 914
    :cond_1
    if-eqz p1, :cond_0

    .line 916
    const/4 v7, 0x0

    .line 917
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 918
    .local v6, "cached":Ljava/lang/String;
    new-array v2, v9, [Ljava/lang/String;

    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_IS_CACHED:Ljava/lang/String;

    aput-object v0, v2, v10

    .line 923
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 925
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 929
    :cond_2
    if-eqz v7, :cond_3

    .line 930
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 931
    const/4 v7, 0x0

    .line 935
    :cond_3
    :goto_1
    if-eqz v6, :cond_5

    const-string v0, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v9

    :goto_2
    move v10, v0

    goto :goto_0

    .line 926
    :catch_0
    move-exception v8

    .line 927
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 929
    if-eqz v7, :cond_3

    .line 930
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 931
    const/4 v7, 0x0

    goto :goto_1

    .line 929
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 930
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 931
    const/4 v7, 0x0

    :cond_4
    throw v0

    :cond_5
    move v0, v10

    .line 935
    goto :goto_2
.end method

.method public isSKTCachedContent(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 939
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v2, :cond_1

    .line 945
    :cond_0
    :goto_0
    return v1

    .line 941
    :cond_1
    if-eqz p1, :cond_0

    .line 943
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 945
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCachedContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isSKTCloudAvailable()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 669
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v3, :cond_0

    move v1, v2

    .line 679
    :goto_0
    return v1

    .line 671
    :cond_0
    const/4 v1, 0x0

    .line 674
    .local v1, "isAvailable":Z
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->isCloudAvailable(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 675
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move v1, v2

    .line 676
    goto :goto_0
.end method

.method public isSKTCloudContent(J)Z
    .locals 5
    .param p1, "videoId"    # J

    .prologue
    .line 551
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 554
    :goto_0
    return v1

    .line 553
    :cond_0
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 554
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isSKTCloudContent(Landroid/database/Cursor;)Z
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    .line 501
    sget-boolean v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v4, :cond_1

    .line 519
    :cond_0
    :goto_0
    return v3

    .line 503
    :cond_1
    if-eqz p1, :cond_0

    .line 505
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 507
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 509
    :cond_2
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_SERVER_ID:Ljava/lang/String;

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 510
    .local v2, "inServerIndex":I
    const/4 v1, 0x0

    .line 512
    .local v1, "inCloud":Ljava/lang/String;
    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    .line 513
    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 519
    :cond_3
    :goto_1
    if-eqz v1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    .line 515
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public isSKTCloudContent(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 537
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 547
    :cond_0
    :goto_0
    return v0

    .line 539
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 541
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 543
    const-string v1, "CloudUtil"

    const-string v2, "isSKTCloudContent(uri)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_2

    .line 545
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 547
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isSKTCloudContent(Ljava/lang/String;)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 523
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 532
    :cond_0
    :goto_0
    return v0

    .line 525
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 527
    const-string v1, "CloudUtil"

    const-string v2, "isSKTCloudContent(string)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    const-wide/16 v2, -0x1

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v1, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 530
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSKTCloudFolder(I)Z
    .locals 8
    .param p1, "bucketID"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 558
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v0, :cond_0

    move v0, v7

    .line 574
    :goto_0
    return v0

    .line 560
    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    move v0, v7

    goto :goto_0

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_2

    move v0, v7

    goto :goto_0

    .line 564
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 565
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    .line 566
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 567
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 568
    const/4 v6, 0x0

    .line 569
    const/4 v0, 0x1

    goto :goto_0

    .line 571
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 572
    const/4 v6, 0x0

    :cond_4
    move v0, v7

    .line 574
    goto :goto_0
.end method

.method public isSKTCloudServerOnlyContent(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 789
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 791
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCachedContent(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSKTCloudServerOnlyContent(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 783
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 785
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isTcloudVideoAvailable()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 732
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v0, :cond_0

    move v0, v8

    .line 745
    :goto_0
    return v0

    .line 734
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 736
    .local v7, "checkCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 737
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 738
    .local v6, "checkCount":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 739
    const/4 v7, 0x0

    .line 740
    if-lez v6, :cond_1

    .line 741
    const/4 v0, 0x1

    goto :goto_0

    .end local v6    # "checkCount":I
    :cond_1
    move v0, v8

    .line 745
    goto :goto_0
.end method

.method public makeAvailableOffline(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1160
    :cond_0
    :goto_0
    return-void

    .line 1157
    :cond_1
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    .line 1158
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/android/cloudagent/CloudStore$API;->makeAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public makeAvailableOffline(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1163
    .local p1, "selectedListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 1170
    :cond_0
    return-void

    .line 1165
    :cond_1
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    .line 1166
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1167
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/cloudagent/CloudStore$API;->makeAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public openWifiConnecting()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 378
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 391
    :goto_0
    return-void

    .line 380
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.WIFI_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 381
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 382
    const/high16 v2, 0x30800000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 383
    const-string v2, "wifiSettingsAppear"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 385
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    const/high16 v3, 0x20000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 386
    :catch_0
    move-exception v0

    .line 387
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Activity Not found!!!"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 388
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 389
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    const-string v3, "Target Activity Not Found"

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public registerCloudDBObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1380
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    .line 1381
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1382
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1384
    :cond_0
    return-void
.end method

.method public requestThumbnail(J)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "videoId"    # J

    .prologue
    const/4 v2, 0x0

    .line 329
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v3, :cond_0

    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v3, :cond_2

    .line 343
    :cond_1
    :goto_0
    return-object v2

    .line 331
    :cond_2
    const-string v3, "CloudUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestThumbnail() : videoId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    const-wide/16 v4, -0x1

    cmp-long v3, p1, v4

    if-nez v3, :cond_1

    .line 333
    invoke-virtual {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(J)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 334
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 336
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lcom/skp/tcloud/service/lib/TcloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Lcom/skp/tcloud/service/lib/TcloudException;
    invoke-virtual {v0}, Lcom/skp/tcloud/service/lib/TcloudException;->printStackTrace()V

    goto :goto_0

    .line 342
    .end local v0    # "e":Lcom/skp/tcloud/service/lib/TcloudException;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_3
    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 343
    .restart local v1    # "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/cloudagent/CloudStore$API;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public requestThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 348
    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v3, :cond_0

    sget-boolean v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v3, :cond_2

    .line 366
    :cond_1
    :goto_0
    return-object v2

    .line 350
    :cond_2
    if-eqz p1, :cond_1

    .line 352
    const-string v3, "CloudUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestThumbnail() : filePath:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 354
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "id"

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    sget-object v6, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v5, p1, v6}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 356
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lcom/skp/tcloud/service/lib/TcloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Lcom/skp/tcloud/service/lib/TcloudException;
    invoke-virtual {v0}, Lcom/skp/tcloud/service/lib/TcloudException;->printStackTrace()V

    goto :goto_0

    .line 362
    .end local v0    # "e":Lcom/skp/tcloud/service/lib/TcloudException;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_3
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getUriByFilepath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 363
    .restart local v1    # "uri":Landroid/net/Uri;
    if-eqz v1, :cond_1

    .line 364
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/cloudagent/CloudStore$API;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public revertAvailableOffline(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1222
    .local p1, "selectedListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 1229
    :cond_0
    return-void

    .line 1224
    :cond_1
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    .line 1225
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1226
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/cloudagent/CloudStore$API;->revertAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public setOnCloudDBCountChanged(Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;

    .prologue
    .line 1439
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnCloudDBCountChanged:Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;

    .line 1440
    return-void
.end method

.method public setOnlyDropboxContent(I)V
    .locals 10
    .param p1, "localcontent"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 1534
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    .line 1535
    iput-boolean v8, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyDropboxContent:Z

    .line 1557
    :goto_0
    return-void

    .line 1539
    :cond_0
    if-ge p1, v9, :cond_2

    .line 1540
    iput-boolean v8, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyDropboxContent:Z

    .line 1556
    :cond_1
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloudUtil - setOnlyDropboxContent : localcontent - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mOnlyDropboxContent : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyDropboxContent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 1542
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1544
    .local v7, "checkCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 1545
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 1546
    .local v6, "checkCount":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1547
    const/4 v7, 0x0

    .line 1548
    if-ne v6, p1, :cond_3

    .line 1549
    iput-boolean v9, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyDropboxContent:Z

    goto :goto_1

    .line 1551
    :cond_3
    iput-boolean v8, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyDropboxContent:Z

    goto :goto_1
.end method

.method public setOnlyTcloudContent(I)V
    .locals 10
    .param p1, "localcontent"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 1564
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-nez v0, :cond_0

    .line 1565
    iput-boolean v8, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyTcloudContent:Z

    .line 1587
    :goto_0
    return-void

    .line 1569
    :cond_0
    if-ge p1, v9, :cond_2

    .line 1570
    iput-boolean v8, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyTcloudContent:Z

    .line 1586
    :cond_1
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloudUtil - setOnlyTcloudContent : localcontent - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mOnlyTcloudContent : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyTcloudContent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 1572
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1574
    .local v7, "checkCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 1575
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 1576
    .local v6, "checkCount":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1577
    const/4 v7, 0x0

    .line 1578
    if-ne v6, p1, :cond_3

    .line 1579
    iput-boolean v9, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyTcloudContent:Z

    goto :goto_1

    .line 1581
    :cond_3
    iput-boolean v8, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnlyTcloudContent:Z

    goto :goto_1
.end method

.method public unregisterCloudDBObserver()V
    .locals 2

    .prologue
    .line 1387
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    .line 1388
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1389
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mSKTCloudContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1391
    :cond_0
    return-void
.end method
