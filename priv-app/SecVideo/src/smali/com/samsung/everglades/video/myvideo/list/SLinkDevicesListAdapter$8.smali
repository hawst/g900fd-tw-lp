.class Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;
.super Ljava/lang/Object;
.source "SLinkDevicesListAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->checkAndShowRegisteredDeviceInfoDialog(Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;

.field final synthetic val$item:Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

.field final synthetic val$pref:Lcom/samsung/everglades/video/myvideo/common/Pref;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Landroid/widget/CheckBox;Lcom/samsung/everglades/video/myvideo/common/Pref;Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;->val$checkBox:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;->val$pref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    iput-object p4, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;->val$item:Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;->val$pref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    const-string v1, "samsung_link_charge_warning_popup_confirmed"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;Z)V

    .line 236
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 237
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;->val$item:Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->openSLinkFileList(Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$500(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V

    .line 238
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mDialogDevice:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$402(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 239
    return-void
.end method
