.class Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;
.super Ljava/lang/Object;
.source "FolderHoverViewer.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 146
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 208
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v8, 0x1

    :goto_1
    return v8

    .line 148
    :pswitch_1
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$300(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, p2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x0

    goto :goto_1

    .line 149
    :cond_1
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$500(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$500(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x0

    goto :goto_1

    .line 151
    :cond_2
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 152
    .local v6, "titleEllipsized":Ljava/lang/Boolean;
    const v8, 0x7f090001

    invoke-virtual {p1, v8}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 153
    .local v1, "firstRowText":Landroid/widget/TextView;
    if-eqz v1, :cond_3

    .line 154
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 155
    .local v0, "firstLayout":Landroid/text/Layout;
    if-eqz v0, :cond_3

    .line 156
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    .line 157
    .local v2, "lines":I
    if-lez v2, :cond_3

    add-int/lit8 v8, v2, -0x1

    invoke-virtual {v0, v8}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v8

    if-lez v8, :cond_3

    .line 158
    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 163
    .end local v0    # "firstLayout":Landroid/text/Layout;
    .end local v2    # "lines":I
    :cond_3
    const v8, 0x7f090002

    invoke-virtual {p1, v8}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 164
    .local v5, "secondRowText":Landroid/widget/TextView;
    if-eqz v5, :cond_4

    .line 165
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    .line 166
    .local v4, "secondLayout":Landroid/text/Layout;
    if-eqz v4, :cond_4

    .line 167
    invoke-virtual {v4}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    .line 168
    .restart local v2    # "lines":I
    if-lez v2, :cond_4

    add-int/lit8 v8, v2, -0x1

    invoke-virtual {v4, v8}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v8

    if-lez v8, :cond_4

    .line 169
    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 174
    .end local v2    # "lines":I
    .end local v4    # "secondLayout":Landroid/text/Layout;
    :cond_4
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_5

    .line 175
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$300(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->setHapticEffect(Landroid/content/Context;Landroid/view/View;)V

    .line 178
    :cond_5
    const/4 v3, -0x1

    .line 179
    .local v3, "mBucketID":I
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    const/16 v9, 0xa

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->updateHoveringIcon(I)V
    invoke-static {v8, v9}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$600(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V

    .line 181
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    invoke-interface {v8}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    check-cast v8, Landroid/view/View;

    iget-object v9, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHoverRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$700(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 183
    const v8, 0x7f090003

    invoke-virtual {p1, v8}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    .line 184
    .local v7, "uri":Landroid/net/Uri;
    const/high16 v8, 0x7f090000

    invoke-virtual {p1, v8}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 186
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "FolderHoverViewer - onHover() ENTER - uri"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 188
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    const/4 v9, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I
    invoke-static {v8, v9}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$802(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)I

    .line 190
    if-eqz v7, :cond_0

    .line 191
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v8

    invoke-virtual {v8, v7, v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->setCursor(Landroid/net/Uri;I)V

    .line 192
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->ensureLocalBitmap()V
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$900(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    .line 193
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    const/4 v9, 0x0

    const-wide/16 v10, 0x190

    const/4 v12, 0x1

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->requestShowPopup(IJI)V
    invoke-static {v8, v9, v10, v11, v12}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1000(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;IJI)V

    goto/16 :goto_0

    .line 201
    .end local v1    # "firstRowText":Landroid/widget/TextView;
    .end local v3    # "mBucketID":I
    .end local v5    # "secondRowText":Landroid/widget/TextView;
    .end local v6    # "titleEllipsized":Ljava/lang/Boolean;
    .end local v7    # "uri":Landroid/net/Uri;
    :pswitch_2
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$300(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, p2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v8, 0x0

    goto/16 :goto_1

    .line 202
    :cond_6
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->requestHidePopup()V
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    goto/16 :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
