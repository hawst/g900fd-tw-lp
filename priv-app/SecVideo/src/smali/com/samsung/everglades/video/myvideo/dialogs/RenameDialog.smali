.class public Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;
.super Ljava/lang/Object;
.source "RenameDialog.java"


# static fields
.field public static final INVALID_CHAR:[Ljava/lang/String;


# instance fields
.field private final MAX_NAME_LENGTH:I

.field private mCancelButton:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mDialog:Landroid/app/AlertDialog;

.field private mInvalid_Toast:Landroid/widget/Toast;

.field private mOrgFilePath:Ljava/lang/String;

.field private mOrgName:Ljava/lang/String;

.field private mString:Ljava/lang/String;

.field private mToast:Landroid/widget/Toast;

.field private mUri:Landroid/net/Uri;

.field private mUriObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\\"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->INVALID_CHAR:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgFilePath:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgName:Ljava/lang/String;

    .line 54
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->MAX_NAME_LENGTH:I

    .line 384
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUriObserver:Landroid/database/ContentObserver;

    .line 60
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUri:Landroid/net/Uri;

    .line 62
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 63
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->registerObserver()V

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showInvalidToast(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->rename(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/Exception;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;
    .param p1, "x1"    # Ljava/lang/Exception;
    .param p2, "x2"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->handleException(Ljava/lang/Exception;I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->cancelSelectionMode()V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showToast(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->unregisterObserver()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->fileAlreadyExist(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mCancelButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgFilePath:Ljava/lang/String;

    return-object v0
.end method

.method private cancelSelectionMode()V
    .locals 2

    .prologue
    .line 417
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getCurrentVideoListFragment(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    .line 418
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    if-eqz v0, :cond_0

    .line 419
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->stopActionMode()V

    .line 421
    :cond_0
    return-void
.end method

.method private fileAlreadyExist(Ljava/lang/String;)Z
    .locals 9
    .param p1, "filenameToRename"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 263
    const/4 v1, 0x0

    .line 264
    .local v1, "fileFullPath":Ljava/lang/String;
    const/4 v2, 0x0

    .line 266
    .local v2, "filePath":Ljava/lang/String;
    :try_start_0
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-eqz v7, :cond_1

    .line 267
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgFilePath:Ljava/lang/String;

    .line 272
    :goto_0
    const/4 v7, 0x0

    const-string v8, "/"

    invoke-virtual {v1, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 273
    const-string v7, "/"

    invoke-virtual {v1, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 274
    .local v5, "orgFileName":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v5, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 275
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 276
    .local v4, "newFileFullPath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 278
    .local v3, "newFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 279
    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 280
    const/4 v6, 0x1

    .line 290
    .end local v3    # "newFile":Ljava/io/File;
    .end local v4    # "newFileFullPath":Ljava/lang/String;
    .end local v5    # "orgFileName":Ljava/lang/String;
    :cond_0
    :goto_1
    return v6

    .line 269
    :cond_1
    const-string v1, ""
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 285
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 288
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 289
    .local v0, "e":Ljava/lang/StringIndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/StringIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method private handleException(Ljava/lang/Exception;I)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;
    .param p2, "resId"    # I

    .prologue
    .line 338
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 339
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showToast(Ljava/lang/String;I)V

    .line 340
    return-void
.end method

.method private registerObserver()V
    .locals 4

    .prologue
    .line 375
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUri:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUriObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 378
    :cond_0
    return-void
.end method

.method private rename(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "dstPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x2e

    .line 295
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 300
    .local v3, "srcFile":Ljava/io/File;
    const/16 v5, 0x2f

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 302
    .local v4, "srcFileName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 304
    .local v1, "dstFilePath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 306
    .local v0, "dstFile":Ljava/io/File;
    invoke-virtual {v3, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 307
    new-instance v5, Ljava/io/IOException;

    const-string v6, "File.renameTo() returns false"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 310
    :cond_2
    :try_start_0
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->remove(Ljava/lang/String;)V

    .line 311
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-direct {p0, v1, v4, v6, v7}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->renameLiveThumbnail(Ljava/lang/String;Ljava/lang/String;J)V

    .line 312
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.MEDIA_SCAN"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v5, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 313
    :catch_0
    move-exception v2

    .line 314
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private renameLiveThumbnail(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p1, "dstFilePath"    # Ljava/lang/String;
    .param p2, "srcFileName"    # Ljava/lang/String;
    .param p3, "videoId"    # J

    .prologue
    .line 320
    const/4 v0, 0x0

    .line 321
    .local v0, "videoListFragment":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/samsung/everglades/video/VideoMain;

    if-eqz v1, :cond_3

    .line 322
    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v1, :cond_2

    .line 323
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .end local v0    # "videoListFragment":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 331
    .restart local v0    # "videoListFragment":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 332
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setRenamedFileInfo(Ljava/lang/String;Ljava/lang/String;J)V

    .line 335
    :cond_1
    return-void

    .line 324
    :cond_2
    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v1, :cond_0

    .line 325
    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v1, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getVideoListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    goto :goto_0

    .line 327
    :cond_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;

    if-eqz v1, :cond_0

    .line 328
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;->getVideoListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    goto :goto_0
.end method

.method private setCurrentFilePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 369
    const/4 v0, 0x0

    .line 370
    .local v0, "videoFilePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 371
    return-object v0
.end method

.method private setCurrentName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 363
    const/4 v0, 0x0

    .line 364
    .local v0, "videoTitle":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 365
    return-object v0
.end method

.method private showInvalidToast(Ljava/lang/String;I)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "lenght"    # I

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mInvalid_Toast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mInvalid_Toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mInvalid_Toast:Landroid/widget/Toast;

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mInvalid_Toast:Landroid/widget/Toast;

    .line 357
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mInvalid_Toast:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 358
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mInvalid_Toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 360
    :cond_1
    return-void
.end method

.method private showToast(Ljava/lang/String;I)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "lenght"    # I

    .prologue
    .line 343
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 345
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mToast:Landroid/widget/Toast;

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mToast:Landroid/widget/Toast;

    .line 348
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 349
    :cond_1
    return-void
.end method

.method private unregisterObserver()V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUriObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 382
    return-void
.end method


# virtual methods
.method protected deleteInDatabase(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 405
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 413
    :cond_0
    :goto_0
    return v1

    .line 409
    :cond_1
    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 410
    .local v0, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "_data = ?"

    new-array v5, v2, [Ljava/lang/String;

    aput-object p1, v5, v1

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    move v1, v2

    .line 411
    goto :goto_0
.end method

.method public showDialog()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/16 v10, 0x32

    const/4 v9, 0x0

    .line 67
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->setCurrentName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;

    .line 68
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->setCurrentFilePath()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgFilePath:Ljava/lang/String;

    .line 69
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const/high16 v7, 0x7f040000

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 71
    .local v0, "alertDialogueView":Landroid/view/View;
    const v6, 0x7f090004

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 72
    .local v1, "alertEditText":Landroid/widget/EditText;
    const v6, 0x84001

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 75
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v10, :cond_0

    .line 76
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 78
    invoke-virtual {v1}, Landroid/widget/EditText;->selectAll()V

    .line 86
    :goto_0
    new-instance v2, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$1;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V

    .line 136
    .local v2, "filter":Landroid/text/InputFilter;
    new-instance v3, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$2;

    invoke-direct {v3, p0, v10}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$2;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;I)V

    .line 147
    .local v3, "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    const-string v6, "inputType=filename"

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 148
    const/4 v6, 0x2

    new-array v6, v6, [Landroid/text/InputFilter;

    aput-object v2, v6, v9

    aput-object v3, v6, v11

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 149
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgName:Ljava/lang/String;

    .line 151
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f0c00ab

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x104000a

    new-instance v8, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$5;

    invoke-direct {v8, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$5;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/high16 v7, 0x1040000

    new-instance v8, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$4;

    invoke-direct {v8, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$4;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$3;

    invoke-direct {v7, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$3;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;

    .line 169
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 170
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 171
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v7, -0x2

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mCancelButton:Landroid/widget/Button;

    .line 172
    new-instance v6, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;

    invoke-direct {v6, p0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Landroid/widget/EditText;)V

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 200
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v4

    .line 201
    .local v4, "positiveButton":Landroid/widget/Button;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgName:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 202
    invoke-virtual {v4, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 207
    :goto_1
    new-instance v6, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;

    invoke-direct {v6, p0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$7;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Landroid/widget/EditText;)V

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    new-instance v5, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$8;

    invoke-direct {v5, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$8;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V

    .line 257
    .local v5, "textWatcher":Landroid/text/TextWatcher;
    invoke-virtual {v1, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 258
    invoke-virtual {v1, v11}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 259
    return-void

    .line 81
    .end local v2    # "filter":Landroid/text/InputFilter;
    .end local v3    # "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    .end local v4    # "positiveButton":Landroid/widget/Button;
    .end local v5    # "textWatcher":Landroid/text/TextWatcher;
    :cond_0
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 83
    invoke-virtual {v1}, Landroid/widget/EditText;->selectAll()V

    goto/16 :goto_0

    .line 204
    .restart local v2    # "filter":Landroid/text/InputFilter;
    .restart local v3    # "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    .restart local v4    # "positiveButton":Landroid/widget/Button;
    :cond_1
    invoke-virtual {v4, v11}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1
.end method
