.class public Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "VideoViewPager.java"


# instance fields
.field private mSwipeable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->mSwipeable:Z

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attr"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->mSwipeable:Z

    .line 21
    return-void
.end method


# virtual methods
.method public executeKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 57
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->mSwipeable:Z

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "executeKeyEvent() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 59
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 62
    :goto_0
    return v0

    .line 61
    :cond_0
    const-string v0, "executeKeyEvent() : return false"

    invoke-static {v2, v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 62
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSwipeable()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->mSwipeable:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 25
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->mSwipeable:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_SWIPE:Z

    if-eqz v1, :cond_0

    .line 27
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 32
    :goto_0
    return v1

    .line 28
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 32
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 37
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->mSwipeable:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_SWIPE:Z

    if-eqz v1, :cond_0

    .line 39
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 44
    :goto_0
    return v1

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 44
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSwipeable(Z)V
    .locals 3
    .param p1, "swipeable"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->mSwipeable:Z

    .line 49
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSwipeable() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->mSwipeable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 50
    return-void
.end method
