.class Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;
.super Landroid/database/ContentObserver;
.source "RenameDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 384
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 8
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v2, 0x0

    .line 387
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$1300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 388
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$000(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$1300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 389
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$9;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    :cond_1
    :goto_0
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 401
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    return-void

    .line 393
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 394
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method
