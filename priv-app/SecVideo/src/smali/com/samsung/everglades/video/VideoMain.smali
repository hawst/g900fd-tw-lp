.class public Lcom/samsung/everglades/video/VideoMain;
.super Landroid/app/Activity;
.source "VideoMain.java"


# static fields
.field public static final DEVICE_PAGE:I = 0x1

.field private static final HANDLE_ASF_CREATE_PROVIDER:I = 0x64

.field public static final NUM_PAGES:I = 0x2

.field public static final PERSONAL_PAGE:I

.field public static isSwipe:Z

.field private static mDeviceFragment:Landroid/app/Fragment;

.field public static mListFragment:Landroid/app/Fragment;

.field public static viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;


# instance fields
.field private mAllShareUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

.field public mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

.field private final mHandler:Landroid/os/Handler;

.field private mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

.field private mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

.field private mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

.field private mTabHost:Landroid/widget/TabHost;

.field public mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

.field pagerAdapter:Landroid/support/v13/app/FragmentStatePagerAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .line 64
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/everglades/video/VideoMain;->isSwipe:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabHost:Landroid/widget/TabHost;

    .line 56
    iput-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    .line 66
    iput-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mAllShareUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 258
    new-instance v0, Lcom/samsung/everglades/video/VideoMain$2;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/VideoMain;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/VideoMain$2;-><init>(Lcom/samsung/everglades/video/VideoMain;Landroid/app/FragmentManager;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->pagerAdapter:Landroid/support/v13/app/FragmentStatePagerAdapter;

    .line 325
    new-instance v0, Lcom/samsung/everglades/video/VideoMain$3;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/VideoMain$3;-><init>(Lcom/samsung/everglades/video/VideoMain;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/VideoMain;)Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/VideoMain;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/VideoMain;)Lcom/samsung/everglades/video/myvideo/common/Pref;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/VideoMain;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    return-object v0
.end method

.method static synthetic access$202(Landroid/app/Fragment;)Landroid/app/Fragment;
    .locals 0
    .param p0, "x0"    # Landroid/app/Fragment;

    .prologue
    .line 48
    sput-object p0, Lcom/samsung/everglades/video/VideoMain;->mDeviceFragment:Landroid/app/Fragment;

    return-object p0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/VideoMain;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/VideoMain;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mAllShareUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/everglades/video/VideoMain;Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/VideoMain;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/everglades/video/VideoMain;->mAllShareUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/VideoMain;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/VideoMain;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/everglades/video/VideoMain;->destroyApp()V

    return-void
.end method

.method private destroyApp()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 356
    const-string v0, "VideoMain - destroyApp()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 357
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    if-eqz v0, :cond_0

    .line 358
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->removeAllViews()V

    .line 359
    sput-object v1, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    .line 360
    sput-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .line 363
    :cond_0
    iput-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    .line 364
    iput-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->pagerAdapter:Landroid/support/v13/app/FragmentStatePagerAdapter;

    .line 365
    iput-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mTabHost:Landroid/widget/TabHost;

    .line 367
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 368
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mAllShareUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-eqz v0, :cond_2

    .line 372
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mAllShareUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->destroyASFRemoteServiceProvider()V

    .line 373
    iput-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mAllShareUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 376
    :cond_2
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->destroyInstance()V

    .line 377
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->disconnectKnoxContainerManager()V

    .line 378
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/DB;->destroyInstance()V

    .line 379
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->destroyInstance()V

    .line 380
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->destroyInstance()V

    .line 381
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->destroyInstance()V

    .line 382
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->destroyInstance()V

    .line 383
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->shutdownThreads()V

    .line 384
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Pref;->destroyInstance()V

    .line 385
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->destroyInstance()V

    .line 386
    invoke-direct {p0}, Lcom/samsung/everglades/video/VideoMain;->unregisterLocalBroadcastManger()V

    .line 387
    return-void
.end method

.method private registerLocalBroadcastManger()V
    .locals 3

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/samsung/everglades/video/VideoMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    .line 406
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 407
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.everglades.video.FINISH_BY_MULTIWINDOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 409
    new-instance v1, Lcom/samsung/everglades/video/VideoMain$4;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/VideoMain$4;-><init>(Lcom/samsung/everglades/video/VideoMain;)V

    iput-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 418
    iget-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v2, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 419
    return-void
.end method

.method private setCustomActionBar()V
    .locals 2

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/samsung/everglades/video/VideoMain;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 321
    .local v0, "bar":Landroid/app/ActionBar;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 322
    const v1, 0x7f0c0052

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 323
    return-void
.end method

.method private setTabhost(Landroid/view/View;)V
    .locals 2
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 312
    const v0, 0x1020012

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabHost:Landroid/widget/TabHost;

    .line 313
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabHost:Landroid/widget/TabHost;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setVisibility(I)V

    .line 315
    new-instance v0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    .line 316
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    iget-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->init(Landroid/widget/TabHost;)V

    .line 317
    return-void
.end method

.method private setViewPager()V
    .locals 4

    .prologue
    .line 209
    const v0, 0x7f090041

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/VideoMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    sput-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    .line 210
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    iget-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->pagerAdapter:Landroid/support/v13/app/FragmentStatePagerAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 212
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    new-instance v1, Lcom/samsung/everglades/video/VideoMain$1;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/VideoMain$1;-><init>(Lcom/samsung/everglades/video/VideoMain;)V

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 255
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    iget-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    const-string v2, "lastTab"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->setCurrentItem(I)V

    .line 256
    return-void
.end method

.method private setupApp()V
    .locals 4

    .prologue
    const/16 v2, 0x64

    .line 344
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/VideoMain;->setVolumeControlStream(I)V

    .line 345
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->callScsCoreInitServiceIfNeeded()V

    .line 347
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 349
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 352
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/VideoMain;->registerLocalBroadcastManger()V

    .line 353
    return-void
.end method

.method private setupView(Landroid/view/View;)V
    .locals 1
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 194
    const-string v0, "VideoMain - setupView() enter"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 195
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/VideoMain;->setContentView(Landroid/view/View;)V

    .line 196
    invoke-direct {p0}, Lcom/samsung/everglades/video/VideoMain;->setCustomActionBar()V

    .line 197
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/VideoMain;->setTabhost(Landroid/view/View;)V

    .line 198
    invoke-direct {p0}, Lcom/samsung/everglades/video/VideoMain;->setViewPager()V

    .line 199
    const-string v0, "VideoMain - setupView() exit"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 200
    return-void
.end method

.method private unregisterLocalBroadcastManger()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 422
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 424
    iput-object v2, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    .line 425
    iput-object v2, p0, Lcom/samsung/everglades/video/VideoMain;->mLocalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 427
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelSelectionMode()V
    .locals 2

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "videoListFragment":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    if-eqz v1, :cond_1

    .line 182
    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v1, :cond_2

    .line 183
    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v1, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getVideoListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    .line 187
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 188
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->stopActionMode()V

    .line 191
    :cond_1
    return-void

    .line 184
    :cond_2
    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v1, v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v1, :cond_0

    .line 185
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .end local v0    # "videoListFragment":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .restart local v0    # "videoListFragment":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 146
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 147
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabHost:Landroid/widget/TabHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->updateTabWidgetGUI()V

    .line 149
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->getCurrentTabId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/VideoMain;->updateTabMenu(I)V

    .line 151
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->ismENGMode()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-wide v2, Lcom/samsung/everglades/video/myvideo/common/Utils;->mVideoLaunchingTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 75
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/samsung/everglades/video/myvideo/common/Utils;->mVideoLaunchingTime:J

    .line 77
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    const-string v2, "VerificationLog"

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-virtual {p0}, Lcom/samsung/everglades/video/VideoMain;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->enableStatusBarOpenByNotification(Landroid/view/Window;)V

    .line 80
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/everglades/video/VideoMain;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    .line 81
    const-string v2, "layout_inflater"

    invoke-virtual {p0, v2}, Lcom/samsung/everglades/video/VideoMain;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 82
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040018

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 83
    .local v1, "mainView":Landroid/view/View;
    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/VideoMain;->setupView(Landroid/view/View;)V

    .line 84
    invoke-direct {p0}, Lcom/samsung/everglades/video/VideoMain;->setupApp()V

    .line 85
    const-string v2, "VideoMain - onCreate() exit"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 110
    const-string v0, "VideoMain - onDestroy()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 111
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->ismENGMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/samsung/everglades/video/myvideo/common/Utils;->mVideoLaunchingTime:J

    .line 115
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 116
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 120
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 121
    const/4 v0, 0x0

    .line 141
    :goto_0
    return v0

    .line 124
    :cond_0
    const/16 v0, 0x52

    if-ne p1, v0, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 125
    invoke-virtual {p0}, Lcom/samsung/everglades/video/VideoMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.samsung.android.app.galaxyfinder"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPackageAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    const-string v0, "VideoMain - Not support Galaxy finder"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 127
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v0, :cond_2

    .line 128
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFolderListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->openSearchList()V

    .line 141
    :cond_1
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 129
    :cond_2
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_1

    .line 130
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->openSearchList()V

    goto :goto_1

    .line 133
    :cond_3
    const/16 v0, 0x54

    if-ne p1, v0, :cond_1

    .line 134
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v0, :cond_4

    .line 135
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFolderListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->openSearchList()V

    goto :goto_1

    .line 136
    :cond_4
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_1

    .line 137
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->openSearchList()V

    goto :goto_1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 100
    invoke-virtual {p0}, Lcom/samsung/everglades/video/VideoMain;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "VideoMain - onPause() - Finished"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 102
    invoke-direct {p0}, Lcom/samsung/everglades/video/VideoMain;->destroyApp()V

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    const-string v0, "VideoMain - onPause()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 91
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->getCurrentTabId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/VideoMain;->updateTabMenu(I)V

    .line 94
    :cond_0
    const-string v0, "VideoMain - onResume()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 433
    return-void
.end method

.method public setViewPagerSwipeable(Z)V
    .locals 1
    .param p1, "isSwipeable"    # Z

    .prologue
    .line 203
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    if-eqz v0, :cond_0

    .line 204
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->setSwipeable(Z)V

    .line 206
    :cond_0
    return-void
.end method

.method public switchFragment()V
    .locals 2

    .prologue
    .line 390
    const-string v0, "VideoMain - switch2FolderFragment()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 391
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v0, :cond_0

    .line 392
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->removeFragment()V

    .line 394
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_1

    .line 395
    invoke-virtual {p0}, Lcom/samsung/everglades/video/VideoMain;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 397
    :cond_1
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .line 398
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    if-eqz v0, :cond_2

    .line 399
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 401
    :cond_2
    return-void
.end method

.method public updateTabMenu(I)V
    .locals 3
    .param p1, "tabId"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 154
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v0, :cond_1

    .line 155
    if-nez p1, :cond_2

    .line 156
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    .line 157
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/Fragment;->setMenuVisibility(Z)V

    .line 158
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->updateMenuVisibility()V

    .line 162
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mDeviceFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_1

    .line 163
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mDeviceFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v2}, Landroid/app/Fragment;->setMenuVisibility(Z)V

    .line 177
    :cond_1
    :goto_0
    return-void

    .line 165
    :cond_2
    if-ne p1, v1, :cond_1

    .line 166
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_3

    .line 167
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v2}, Landroid/app/Fragment;->setMenuVisibility(Z)V

    .line 168
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v0, :cond_3

    .line 169
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->disableMenu()V

    .line 172
    :cond_3
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mDeviceFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_1

    .line 173
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mDeviceFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/Fragment;->setMenuVisibility(Z)V

    goto :goto_0
.end method
