.class Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;
.super Landroid/os/Handler;
.source "DeleteOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 314
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 274
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-nez v1, :cond_1

    .line 275
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$602(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/app/ProgressDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/app/ProgressDialog;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 279
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/app/ProgressDialog;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 280
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/app/ProgressDialog;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 281
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/app/ProgressDialog;

    move-result-object v1

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8$1;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 291
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 272
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 295
    :pswitch_1
    :try_start_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "errorMsg":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 297
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 300
    .end local v0    # "errorMsg":Ljava/lang/String;
    :cond_2
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->closeProgressDialog()V
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$700(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    .line 301
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mOnDeleteFinishedListener:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$800(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 302
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mOnDeleteFinishedListener:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$800(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;->onDeleteFinished()V

    goto/16 :goto_0

    .line 307
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->setKeepScreenOn()V
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$900(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    goto/16 :goto_0

    .line 311
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->clearKeepScreenOn()V
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$1000(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 272
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
