.class Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
.super Ljava/lang/Object;
.source "MoveKNOXOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomProgressDialog"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCountText:Landroid/widget/TextView;

.field private mDialog:Landroid/app/Dialog;

.field private mPercentage:Landroid/widget/TextView;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mTotalCnt:I

.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;


# direct methods
.method public constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 446
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mContext:Landroid/content/Context;

    .line 448
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->createDialog()V

    .line 449
    return-void
.end method

.method private createDialog()V
    .locals 6

    .prologue
    .line 452
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 454
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040012

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 455
    .local v1, "progressView":Landroid/view/View;
    const v3, 0x7f090034

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mPercentage:Landroid/widget/TextView;

    .line 456
    const v3, 0x7f090033

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    .line 457
    const v3, 0x7f090032

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    .line 459
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 460
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 462
    const v2, 0x7f0c007c

    .line 463
    .local v2, "stringId":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mRequestedCmdType:I
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$1200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 464
    const v2, 0x7f0c00a7

    .line 467
    :cond_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 469
    const v3, 0x7f0c000d

    new-instance v4, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog$1;

    invoke-direct {v4, p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 479
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    .line 480
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 515
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    .line 517
    :cond_0
    return-void
.end method

.method public initCount()V
    .locals 3

    .prologue
    .line 506
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 510
    :goto_0
    return-void

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mTotalCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setCount(I)V
    .locals 3
    .param p1, "cnt"    # I

    .prologue
    .line 499
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 503
    :goto_0
    return-void

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mTotalCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setMax(I)V
    .locals 0
    .param p1, "max"    # I

    .prologue
    .line 487
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mTotalCnt:I

    .line 488
    return-void
.end method

.method public setProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 491
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mPercentage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mPercentage:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 484
    return-void
.end method
