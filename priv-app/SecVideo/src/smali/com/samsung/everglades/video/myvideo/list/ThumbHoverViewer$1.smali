.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->initViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private runVideoPlayer()V
    .locals 4

    .prologue
    .line 348
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 349
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setPath(Ljava/lang/String;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 350
    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 351
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x2

    .line 326
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 344
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 328
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 331
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 335
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 336
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->suspendHoverPopupHandler(I)V

    .line 337
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->forceStop()V

    .line 338
    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonLayoutHeight:I
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$800(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->abandonAudioFocus()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$900(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    .line 340
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;->runVideoPlayer()V

    goto :goto_0

    .line 326
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
