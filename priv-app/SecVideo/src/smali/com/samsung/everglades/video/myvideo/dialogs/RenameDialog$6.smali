.class Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;
.super Ljava/lang/Object;
.source "RenameDialog.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

.field final synthetic val$alertEditText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->val$alertEditText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 175
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    .line 176
    const/16 v2, 0x42

    if-eq p2, v2, :cond_0

    const/16 v2, 0x17

    if-ne p2, v2, :cond_3

    .line 177
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$500(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$500(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->val$alertEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 196
    :cond_1
    :goto_0
    return v1

    .line 181
    :cond_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->val$alertEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 182
    .local v0, "filenameToRename":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->fileAlreadyExist(Ljava/lang/String;)Z
    invoke-static {v2, v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$600(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 186
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->val$alertEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$702(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 187
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mString:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$700(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 190
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mCancelButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$800(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/widget/Button;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 191
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$6;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mCancelButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$800(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    .line 196
    .end local v0    # "filenameToRename":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
