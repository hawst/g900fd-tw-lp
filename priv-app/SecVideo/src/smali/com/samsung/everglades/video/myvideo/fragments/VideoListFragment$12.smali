.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Lcom/sec/android/touchwiz/widget/TwAbsListView;III)V
    .locals 0
    .param p1, "view"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 1029
    return-void
.end method

.method public onScrollStateChanged(Lcom/sec/android/touchwiz/widget/TwAbsListView;I)V
    .locals 3
    .param p1, "view"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "newScrollState"    # I

    .prologue
    const/16 v2, 0x64

    .line 1009
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mScrollState:I
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$800(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)I

    move-result v0

    .line 1010
    .local v0, "oldScrollState":I
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1011
    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1012
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1013
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1014
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setVideoWallOn(Z)V

    .line 1018
    :cond_0
    if-nez v0, :cond_1

    .line 1019
    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1020
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setVideoWallOn(Z)V

    .line 1021
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 1022
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->forceStop()V

    .line 1025
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$12;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # setter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mScrollState:I
    invoke-static {v1, p2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$802(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;I)I

    .line 1026
    return-void
.end method
