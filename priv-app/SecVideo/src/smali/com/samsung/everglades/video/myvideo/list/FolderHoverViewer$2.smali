.class Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;
.super Ljava/lang/Object;
.source "FolderHoverViewer.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->initFolderHoverView(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x1

    .line 119
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mFilepath:[Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$000(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, p3

    if-eqz v2, :cond_1

    .line 120
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mFilepath:[Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$000(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, p3

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getUriFromCursor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 122
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_1

    .line 123
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 127
    :cond_0
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 128
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$300(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getBucketID()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setBucketId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 129
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$300(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mFilepath:[Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$000(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, p3

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setPath(Ljava/lang/String;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 130
    invoke-static {v4, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 132
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->dismissPopup()V
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    .line 135
    .end local v0    # "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_1
    return-void
.end method
