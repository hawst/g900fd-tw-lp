.class Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
.super Ljava/lang/Thread;
.source "MoveKNOXOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MoveKNOXFilesThread"
.end annotation


# instance fields
.field private abortOpertion:Z

.field private mCmdType:I

.field private mContext:Landroid/content/Context;

.field private mCurrentFileCount:I

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mFailedFileCount:I

.field private mFilePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mRcpm:Landroid/os/RCPManager;

.field private mThreadId:J

.field private mTotalCount:I


# direct methods
.method public constructor <init>(Ljava/util/Iterator;Landroid/os/Handler;Landroid/content/Context;II)V
    .locals 2
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "cmdType"    # I
    .param p5, "knox2DestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/os/Handler;",
            "Landroid/content/Context;",
            "II)V"
        }
    .end annotation

    .prologue
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 154
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 143
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mIterator:Ljava/util/Iterator;

    .line 144
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFilePaths:Ljava/util/ArrayList;

    .line 145
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->abortOpertion:Z

    .line 146
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 148
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mTotalCount:I

    .line 149
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCurrentFileCount:I

    .line 150
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFailedFileCount:I

    .line 155
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mIterator:Ljava/util/Iterator;

    .line 156
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mHandler:Landroid/os/Handler;

    .line 157
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mContext:Landroid/content/Context;

    .line 158
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 159
    iput p4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCmdType:I

    .line 160
    return-void
.end method

.method private moveKNOXFiles(Ljava/util/Iterator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    const/4 v3, 0x3

    .line 206
    if-nez p1, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->abortOpertion:Z

    if-nez v1, :cond_0

    .line 215
    :try_start_0
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCmdType:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCmdType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 216
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->moveToKnox(Ljava/util/Iterator;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 222
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 217
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCmdType:I

    if-ne v1, v3, :cond_4

    .line 218
    const-string v1, "moveKNOXFiles : MOVE_FOLDERS is TBD"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :cond_4
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCmdType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 220
    const-string v1, "moveKNOXFiles : REMOVE_FOLDERS is TBD"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private moveToKnox(Ljava/util/Iterator;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    const/4 v7, 0x0

    .line 228
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFilePaths:Ljava/util/ArrayList;

    .line 229
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 230
    iget-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->abortOpertion:Z

    if-eqz v3, :cond_2

    .line 263
    :cond_1
    :goto_1
    return-void

    .line 233
    :cond_2
    const/4 v1, 0x0

    .line 234
    .local v1, "filePath":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 235
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v3, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 236
    if-eqz v1, :cond_0

    .line 237
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 241
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_3
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnox2(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 242
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mContext:Landroid/content/Context;

    const-string v4, "rcp"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/RCPManager;

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mRcpm:Landroid/os/RCPManager;

    .line 244
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mRcpm:Landroid/os/RCPManager;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFilePaths:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/RCPManager;->moveFilesForApp(ILjava/util/List;Ljava/util/List;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mThreadId:J

    .line 245
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 250
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_4
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mTotalCount:I

    .line 251
    iput v7, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCurrentFileCount:I

    .line 252
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mHandler:Landroid/os/Handler;

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCmdType:I

    iget v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mTotalCount:I

    invoke-static {v4, v7, v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 254
    :try_start_1
    iget v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCmdType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 255
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->getContainerInstallerManagerInstance()Lcom/sec/knox/containeragent/ContainerInstallerManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFilePaths:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayRequest(Ljava/util/List;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 258
    :catch_1
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 260
    const-string v3, "moveKNOXFiles : ContainerInstallerManagerInstance is null"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public cancelKnoxFileProcess()V
    .locals 4

    .prologue
    .line 167
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFilePaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 169
    :try_start_0
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->getContainerInstallerManagerInstance()Lcom/sec/knox/containeragent/ContainerInstallerManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFilePaths:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayCancel(Ljava/util/List;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 173
    const-string v1, "cancelMovetoNox : ContainerInstallerManagerInstance is null"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getCurrentFileCount()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCurrentFileCount:I

    return v0
.end method

.method public getTotalFileCount()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mTotalCount:I

    return v0
.end method

.method public incFailedFileCount()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFailedFileCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFailedFileCount:I

    .line 193
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mFailedFileCount:I

    return v0
.end method

.method public incMovedFileCount()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCurrentFileCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCurrentFileCount:I

    .line 188
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mCurrentFileCount:I

    return v0
.end method

.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 198
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 199
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->mIterator:Ljava/util/Iterator;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->moveKNOXFiles(Ljava/util/Iterator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->abortOpertion:Z

    .line 203
    return-void

    .line 201
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->abortOpertion:Z

    throw v0
.end method

.method public declared-synchronized setAbortOperation(Z)V
    .locals 1
    .param p1, "isAborted"    # Z

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->abortOpertion:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    monitor-exit p0

    return-void

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
