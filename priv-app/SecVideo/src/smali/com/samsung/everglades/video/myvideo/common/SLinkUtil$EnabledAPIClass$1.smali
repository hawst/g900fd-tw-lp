.class Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass$1;
.super Ljava/lang/Object;
.source "SLinkUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->callScsCoreInitServiceIfNeeded()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass$1;->this$1:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 247
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass$1;->this$1:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->isSignedIn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 248
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.app.videoplayer"

    const-string v3, "com.sec.android.app.videoplayer.slink.SCSCoreInitService"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 250
    .local v1, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass$1;->this$1:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;

    iget-object v2, v2, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 252
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method
