.class Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FolderHoverViewer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThumbGridAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private ThumbList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;


# direct methods
.method public constructor <init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1
    .param p2, "c"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "arrays":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x0

    .line 394
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .line 395
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 391
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->ThumbList:Ljava/util/ArrayList;

    .line 392
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->mContext:Landroid/content/Context;

    .line 396
    iput-object p4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->ThumbList:Ljava/util/ArrayList;

    .line 397
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->mContext:Landroid/content/Context;

    .line 398
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertview"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 401
    move-object v3, p2

    .line 402
    .local v3, "v":Landroid/view/View;
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 403
    .local v4, "vi":Landroid/view/LayoutInflater;
    const v5, 0x7f04001a

    invoke-virtual {v4, v5, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 404
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 406
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->ThumbList:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    .line 425
    :goto_0
    return-object v3

    .line 408
    :cond_0
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->ThumbList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 409
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const v5, 0x7f090044

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 411
    .local v2, "thumbImage":Landroid/widget/ImageView;
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 412
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 413
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 415
    const v5, 0x7f090043

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 416
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCount()I

    move-result v5

    const/4 v6, 0x2

    if-gt v5, v6, :cond_1

    .line 417
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1700(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/widget/GridView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/GridView;->getHeight()I

    move-result v5

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 423
    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 418
    :cond_1
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCount()I

    move-result v5

    const/4 v6, 0x6

    if-gt v5, v6, :cond_2

    .line 419
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1700(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/widget/GridView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/GridView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1

    .line 421
    :cond_2
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$1700(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/widget/GridView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/GridView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x3

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1
.end method
