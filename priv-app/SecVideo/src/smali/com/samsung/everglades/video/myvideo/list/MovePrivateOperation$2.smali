.class Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;
.super Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;
.source "MovePrivateOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->MoveFilesToPrivate(Ljava/util/ArrayList;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    invoke-direct {p0}, Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChange(II)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "extInfo"    # I

    .prologue
    const/4 v2, 0x1

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MovePrivateOperation - state : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 123
    if-nez p1, :cond_1

    .line 124
    const-string v0, "state == PrivateModeManager.PREPARED"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->registerClient(Lcom/samsung/android/privatemode/IPrivateModeClient;)Landroid/os/IBinder;

    move-result-object v1

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$202(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Landroid/os/IBinder;)Landroid/os/IBinder;

    .line 127
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_0

    .line 128
    const-string v0, "PrivateModeClient is not registered!!"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    if-ne p1, v2, :cond_2

    .line 132
    const-string v0, "state == PrivateModeManager.MOUNTED"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$300()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->startMoveFiles(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 135
    const-string v0, "state == PrivateModeManager.CANCELLED"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mMoveFilesThread:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;

    if-eqz v0, :cond_3

    .line 137
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static {v0, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$002(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    goto :goto_0

    .line 139
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    goto :goto_0
.end method
