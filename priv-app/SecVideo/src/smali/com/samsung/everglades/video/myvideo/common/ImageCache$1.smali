.class Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;
.super Ljava/lang/Object;
.source "ImageCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/common/ImageCache;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/common/ImageCache;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 258
    const/4 v0, 0x0

    .line 259
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$400()Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$400()Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->requestThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    # invokes: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->downScaler(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$500(Lcom/samsung/everglades/video/myvideo/common/ImageCache;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 268
    :goto_0
    return-object v0

    .line 261
    :cond_0
    const-string v1, "http:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 262
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    # invokes: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->downloadBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v2, p1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$600(Lcom/samsung/everglades/video/myvideo/common/ImageCache;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    # invokes: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->downScaler(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$500(Lcom/samsung/everglades/video/myvideo/common/ImageCache;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 263
    :cond_1
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$700()Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSLinkThumbId(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 264
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$700()Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getThumbId(Ljava/lang/String;)J

    move-result-wide v2

    # invokes: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getSLinkBitmap(J)Landroid/graphics/Bitmap;
    invoke-static {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$800(Lcom/samsung/everglades/video/myvideo/common/ImageCache;J)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 266
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    # invokes: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getLocalBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$900(Lcom/samsung/everglades/video/myvideo/common/ImageCache;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 227
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 254
    :cond_0
    return-void

    .line 230
    :cond_1
    :goto_0
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 231
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->get()Ljava/lang/String;

    move-result-object v1

    .line 233
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_1

    .line 236
    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 237
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->removeQueueInProcess(Ljava/lang/String;)Z

    .line 239
    if-eqz v0, :cond_1

    .line 240
    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 241
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnVideoListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$100()Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 242
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnVideoListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$100()Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;->onUpdated()V

    .line 245
    :cond_2
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnDeviceListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$200()Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 246
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnDeviceListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$200()Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;->onUpdated()V

    .line 249
    :cond_3
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnFolderListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$300()Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 250
    # getter for: Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnFolderListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->access$300()Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;->onFolderListUpdated()V

    goto :goto_0
.end method
