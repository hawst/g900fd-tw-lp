.class Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$11;
.super Ljava/lang/Object;
.source "MenuOptionsItemSelectedCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->showEditPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V
    .locals 0

    .prologue
    .line 730
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$11;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 732
    packed-switch p2, :pswitch_data_0

    .line 742
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 743
    return-void

    .line 734
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$11;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # getter for: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$500(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    const/16 v1, 0x77

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$11;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # getter for: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$600(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(ILcom/samsung/everglades/video/myvideo/common/ListType;)V

    goto :goto_0

    .line 737
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$11;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->callVideoEditorMenu()V

    goto :goto_0

    .line 732
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
