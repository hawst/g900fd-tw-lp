.class public final Lcom/samsung/everglades/video/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final actionbar_divider_line:I = 0x7f070000

.field public static final actionbar_dropdown_text:I = 0x7f070001

.field public static final actionbar_dropdown_text_dim:I = 0x7f070002

.field public static final actionbar_select_text_color:I = 0x7f070003

.field public static final actionbar_text:I = 0x7f070004

.field public static final actionbar_text_shadow:I = 0x7f070005

.field public static final black:I = 0x7f070006

.field public static final button_text:I = 0x7f070007

.field public static final button_text_disable:I = 0x7f070008

.field public static final button_text_disable_light:I = 0x7f070009

.field public static final button_text_press:I = 0x7f07000a

.field public static final checked_bitmap_color_list:I = 0x7f070044

.field public static final default_thumbnail_background:I = 0x7f07000b

.field public static final density_tag_text:I = 0x7f07000c

.field public static final details_episode_header:I = 0x7f07000d

.field public static final details_file_size:I = 0x7f07000e

.field public static final details_overview_title:I = 0x7f07000f

.field public static final details_subtitle:I = 0x7f070010

.field public static final details_title:I = 0x7f070011

.field public static final details_value:I = 0x7f070012

.field public static final device_list_dim_color:I = 0x7f070013

.field public static final device_list_text:I = 0x7f070014

.field public static final device_row_dlna_txt_color:I = 0x7f070015

.field public static final device_row_slink_first_txt_color:I = 0x7f070016

.field public static final download_size_text:I = 0x7f070017

.field public static final downloading_text:I = 0x7f070018

.field public static final empty_view_device_text_color:I = 0x7f070019

.field public static final empty_view_text_color:I = 0x7f07001a

.field public static final empty_view_text_color_search:I = 0x7f07001b

.field public static final folder_list_text_focused:I = 0x7f07001c

.field public static final folder_list_text_normal:I = 0x7f07001d

.field public static final folderpreview_hover_divider_color:I = 0x7f07001e

.field public static final hovering_second_line:I = 0x7f07001f

.field public static final license_row_text:I = 0x7f070020

.field public static final list_default_text:I = 0x7f070021

.field public static final list_divider:I = 0x7f070022

.field public static final list_end_text:I = 0x7f070023

.field public static final list_first_text:I = 0x7f070024

.field public static final list_row_item_bottom_line_color:I = 0x7f070025

.field public static final list_section_text:I = 0x7f070026

.field public static final list_title_text:I = 0x7f070027

.field public static final menu_divider:I = 0x7f070028

.field public static final menu_text:I = 0x7f070029

.field public static final menu_text_a30:I = 0x7f07002a

.field public static final no_item:I = 0x7f07002b

.field public static final normal_select_text:I = 0x7f07002c

.field public static final normal_text:I = 0x7f07002d

.field public static final personal_border_color:I = 0x7f07002e

.field public static final popup_text_color:I = 0x7f07002f

.field public static final pressed_text:I = 0x7f070030

.field public static final s_link_charge_warning_info_color:I = 0x7f070031

.field public static final screen_background:I = 0x7f070032

.field public static final screen_background_folder:I = 0x7f070033

.field public static final search_hint_text:I = 0x7f070034

.field public static final search_hint_text_dark:I = 0x7f070035

.field public static final selectall_text:I = 0x7f070036

.field public static final spinner_disable:I = 0x7f070037

.field public static final spinner_noarmal_bg:I = 0x7f070038

.field public static final spinner_normal:I = 0x7f070039

.field public static final spinner_select:I = 0x7f07003a

.field public static final sub_info_text:I = 0x7f07003b

.field public static final tab_default_text:I = 0x7f07003c

.field public static final tab_select_text:I = 0x7f07003d

.field public static final tab_text_shadow:I = 0x7f07003e

.field public static final transparent:I = 0x7f07003f

.field public static final unchecked_bitmap_color_list:I = 0x7f070045

.field public static final videoapp_color:I = 0x7f070040

.field public static final viewas_divider:I = 0x7f070041

.field public static final white:I = 0x7f070042

.field public static final white_30:I = 0x7f070043


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
