.class Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;
.super Ljava/lang/Object;
.source "SelectionAnimator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->onPreDraw()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

.field final synthetic val$chbTranslationX:I

.field final synthetic val$childCount:I


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;II)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

    iput p2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->val$childCount:I

    iput p3, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->val$chbTranslationX:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6
    .param p1, "anim"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 61
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v4

    if-nez v4, :cond_1

    .line 79
    :cond_0
    return-void

    .line 64
    :cond_1
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v1

    .line 67
    .local v1, "fraction":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->val$childCount:I

    if-ge v2, v4, :cond_0

    .line 68
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$400(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isListOrFolder()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 69
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f09004f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 70
    .local v3, "row":Landroid/view/View;
    if-eqz v3, :cond_2

    .line 71
    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->val$chbTranslationX:I

    int-to-float v4, v4

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, v1

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationX(F)V

    .line 73
    .end local v3    # "row":Landroid/view/View;
    :cond_2
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 74
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f09002c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 75
    .local v0, "cb":Landroid/view/View;
    if-eqz v0, :cond_3

    .line 76
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 67
    .end local v0    # "cb":Landroid/view/View;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
