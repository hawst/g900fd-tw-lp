.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->initListView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field tempMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 1

    .prologue
    .line 675
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->tempMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 0
    .param p1, "startX"    # I
    .param p2, "startY"    # I

    .prologue
    .line 704
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 3
    .param p1, "endX"    # I
    .param p2, "endY"    # I

    .prologue
    const/16 v2, 0x64

    .line 707
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectContentViewMode(Z)V

    .line 709
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 711
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->tempMap:Ljava/util/Map;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->putTempItemsToMultiSelector(Ljava/util/Map;)V
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$600(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;Ljava/util/Map;)V

    .line 714
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->invalidateActionMode()V

    .line 715
    return-void
.end method

.method public onTwMultiSelected(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJZZZ)V
    .locals 7
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "isShiftpress"    # Z
    .param p7, "isCtrlpress"    # Z
    .param p8, "isPenpress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/touchwiz/widget/TwAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 680
    .local p1, "parent":Lcom/sec/android/touchwiz/widget/TwAdapterView;, "Lcom/sec/android/touchwiz/widget/TwAdapterView<*>;"
    invoke-virtual {p1}, Lcom/sec/android/touchwiz/widget/TwAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v5

    check-cast v5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    invoke-virtual {v5, p3}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 681
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v5, "_data"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 682
    .local v3, "index":I
    const/4 v2, 0x0

    .line 684
    .local v2, "filePath":Ljava/lang/String;
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 685
    :try_start_0
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 691
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v6

    invoke-static {v5, v0, v6}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v4

    .line 694
    .local v4, "uri":Landroid/net/Uri;
    :try_start_1
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->tempMap:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 695
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->tempMap:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 702
    :goto_1
    return-void

    .line 687
    .end local v4    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 688
    .local v1, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v1}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 697
    .end local v1    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_1
    :try_start_2
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$5;->tempMap:Ljava/util/Map;

    invoke-interface {v5, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 699
    :catch_1
    move-exception v1

    .line 700
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
