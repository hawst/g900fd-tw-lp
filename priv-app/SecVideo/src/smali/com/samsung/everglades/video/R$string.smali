.class public final Lcom/samsung/everglades/video/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final IDS_COM_POP_EXPIREAFTERFIRSTUSE:I = 0x7f0c0000

.field public static final WiFi_connection_required:I = 0x7f0c0001

.field public static final add_to:I = 0x7f0c0002

.field public static final all_content:I = 0x7f0c0003

.field public static final allshare_download_complete:I = 0x7f0c0004

.field public static final already_exists:I = 0x7f0c0005

.field public static final app_name:I = 0x7f0c0006

.field public static final autoplayeoff:I = 0x7f0c0007

.field public static final autoplayeon:I = 0x7f0c0008

.field public static final autoplayofftoast:I = 0x7f0c0009

.field public static final autoplayontoast:I = 0x7f0c000a

.field public static final autoplaysettingtitle:I = 0x7f0c000b

.field public static final baidu:I = 0x7f0c000c

.field public static final cancel:I = 0x7f0c000d

.field public static final change_quality:I = 0x7f0c000e

.field public static final close:I = 0x7f0c000f

.field public static final connection_check_title_wifi:I = 0x7f0c0010

.field public static final connection_check_wifi:I = 0x7f0c0011

.field public static final content:I = 0x7f0c0012

.field public static final content_in_baidu:I = 0x7f0c0013

.field public static final content_in_dropbox:I = 0x7f0c0014

.field public static final content_in_phone:I = 0x7f0c0015

.field public static final content_in_tcloud:I = 0x7f0c0016

.field public static final count_devices:I = 0x7f0c0017

.field public static final count_folders:I = 0x7f0c0018

.field public static final count_single_device:I = 0x7f0c0019

.field public static final count_single_folder:I = 0x7f0c001a

.field public static final count_single_video:I = 0x7f0c001b

.field public static final count_video:I = 0x7f0c001c

.field public static final delete_complete:I = 0x7f0c001d

.field public static final delete_fail:I = 0x7f0c001e

.field public static final delete_folder_popup_all:I = 0x7f0c010b

.field public static final delete_folder_popup_nitems:I = 0x7f0c001f

.field public static final delete_folder_popup_text:I = 0x7f0c0020

.field public static final delete_hub_popup_multi:I = 0x7f0c010c

.field public static final delete_hub_popup_single:I = 0x7f0c010d

.field public static final delete_hub_popup_text:I = 0x7f0c010e

.field public static final delete_list_title:I = 0x7f0c0021

.field public static final delete_popup_all:I = 0x7f0c010f

.field public static final delete_popup_multi:I = 0x7f0c0022

.field public static final delete_popup_nitems:I = 0x7f0c0110

.field public static final delete_popup_single:I = 0x7f0c0111

.field public static final delete_popup_text:I = 0x7f0c0023

.field public static final deregisterd_device:I = 0x7f0c0024

.field public static final deregisterd_device_message:I = 0x7f0c0025

.field public static final deregistered_progress_message:I = 0x7f0c0026

.field public static final details_audio_channel:I = 0x7f0c0027

.field public static final details_date:I = 0x7f0c0028

.field public static final details_duration:I = 0x7f0c0029

.field public static final details_file_name:I = 0x7f0c002a

.field public static final details_file_size:I = 0x7f0c002b

.field public static final details_folder_name:I = 0x7f0c002c

.field public static final details_folder_size:I = 0x7f0c002d

.field public static final details_format:I = 0x7f0c002e

.field public static final details_forwarding:I = 0x7f0c002f

.field public static final details_resolution:I = 0x7f0c0030

.field public static final devices:I = 0x7f0c0031

.field public static final director:I = 0x7f0c0112

.field public static final disconnected_dms:I = 0x7f0c0032

.field public static final do_not_ask_again:I = 0x7f0c0033

.field public static final done:I = 0x7f0c0034

.field public static final dot:I = 0x7f0c0035

.field public static final download:I = 0x7f0c0036

.field public static final download_popup_title:I = 0x7f0c0037

.field public static final downloaded:I = 0x7f0c0038

.field public static final downloading:I = 0x7f0c0039

.field public static final dropbox:I = 0x7f0c003a

.field public static final dropbox_selected_content_download:I = 0x7f0c003b

.field public static final empty_video:I = 0x7f0c003c

.field public static final episode_n:I = 0x7f0c0113

.field public static final error_device_not_found:I = 0x7f0c003d

.field public static final error_failed_to_download:I = 0x7f0c003e

.field public static final error_file_already_exists:I = 0x7f0c003f

.field public static final error_file_error:I = 0x7f0c0040

.field public static final error_network_error_occurred:I = 0x7f0c0041

.field public static final error_not_enough_memory:I = 0x7f0c0042

.field public static final error_unknown:I = 0x7f0c0043

.field public static final first_sync_notice:I = 0x7f0c0044

.field public static final folders_selected:I = 0x7f0c0045

.field public static final genre:I = 0x7f0c0114

.field public static final go_to_samsung_link:I = 0x7f0c0046

.field public static final go_to_samsung_link_help:I = 0x7f0c0047

.field public static final go_to_samsung_link_register:I = 0x7f0c0048

.field public static final go_to_store:I = 0x7f0c0115

.field public static final help:I = 0x7f0c0049

.field public static final impossible:I = 0x7f0c004a

.field public static final installed:I = 0x7f0c004b

.field public static final invalid_character:I = 0x7f0c004c

.field public static final item_moved_to:I = 0x7f0c004d

.field public static final items_moved_to:I = 0x7f0c004e

.field public static final knox_mode:I = 0x7f0c004f

.field public static final language:I = 0x7f0c0050

.field public static final large:I = 0x7f0c0051

.field public static final library_title:I = 0x7f0c0052

.field public static final loading:I = 0x7f0c0053

.field public static final location:I = 0x7f0c0054

.field public static final make_available_offline:I = 0x7f0c0116

.field public static final max_char_reached_msg:I = 0x7f0c0055

.field public static final max_character:I = 0x7f0c0056

.field public static final mediascanner_finished:I = 0x7f0c0057

.field public static final mediascanner_started:I = 0x7f0c0058

.field public static final medium:I = 0x7f0c0059

.field public static final menu_Delete:I = 0x7f0c005a

.field public static final menu_ShareVia:I = 0x7f0c005b

.field public static final menu_content_to_display:I = 0x7f0c005c

.field public static final menu_default_list:I = 0x7f0c005d

.field public static final menu_details:I = 0x7f0c005e

.field public static final menu_download:I = 0x7f0c005f

.field public static final menu_edit:I = 0x7f0c0060

.field public static final menu_folder_list:I = 0x7f0c0061

.field public static final menu_more_contents:I = 0x7f0c0062

.field public static final menu_more_episodes:I = 0x7f0c0063

.field public static final menu_select:I = 0x7f0c0064

.field public static final menu_select_baidu:I = 0x7f0c0065

.field public static final menu_select_dropbox:I = 0x7f0c0066

.field public static final menu_select_local:I = 0x7f0c0067

.field public static final menu_select_tcloud:I = 0x7f0c0068

.field public static final menu_sendto_another_device:I = 0x7f0c0069

.field public static final menu_studio:I = 0x7f0c006a

.field public static final menu_tag_buddy:I = 0x7f0c006b

.field public static final menu_thumbnail_list:I = 0x7f0c006c

.field public static final menu_upload:I = 0x7f0c0117

.field public static final menu_video_editor:I = 0x7f0c006d

.field public static final menu_view_as:I = 0x7f0c0118

.field public static final menu_view_by:I = 0x7f0c006e

.field public static final menu_view_downloadable:I = 0x7f0c0119

.field public static final menu_view_on_baidu:I = 0x7f0c006f

.field public static final menu_view_on_device:I = 0x7f0c0070

.field public static final menu_view_on_dropbox:I = 0x7f0c0071

.field public static final menu_view_on_tcloud:I = 0x7f0c0072

.field public static final mins:I = 0x7f0c0073

.field public static final mono:I = 0x7f0c0074

.field public static final more_menu:I = 0x7f0c0075

.field public static final move:I = 0x7f0c0076

.field public static final move_fail:I = 0x7f0c0077

.field public static final move_file_exist:I = 0x7f0c0078

.field public static final move_knox_fail:I = 0x7f0c0079

.field public static final move_knox_not_enough_space:I = 0x7f0c007a

.field public static final move_not_enough_space:I = 0x7f0c007b

.field public static final move_to_knox:I = 0x7f0c007c

.field public static final move_to_one_item:I = 0x7f0c007d

.field public static final move_to_one_item_exist_items:I = 0x7f0c007e

.field public static final move_to_one_item_exists_item:I = 0x7f0c007f

.field public static final move_to_private:I = 0x7f0c0080

.field public static final move_to_select_exist_items:I = 0x7f0c0081

.field public static final move_to_select_exists_item:I = 0x7f0c0082

.field public static final move_to_select_items:I = 0x7f0c0083

.field public static final movies:I = 0x7f0c011a

.field public static final moving:I = 0x7f0c0084

.field public static final my_video:I = 0x7f0c0085

.field public static final name_cannot_empty:I = 0x7f0c0086

.field public static final nearby_device:I = 0x7f0c0087

.field public static final need_to_replace_message:I = 0x7f0c0088

.field public static final need_to_sign_in_samsung_account:I = 0x7f0c011b

.field public static final no_matches_search:I = 0x7f0c0089

.field public static final no_matches_search_item:I = 0x7f0c011c

.field public static final no_online_contents:I = 0x7f0c008a

.field public static final no_video:I = 0x7f0c008b

.field public static final no_video_details_info_s_link:I = 0x7f0c008c

.field public static final no_video_info:I = 0x7f0c008d

.field public static final no_video_info_away:I = 0x7f0c011d

.field public static final no_video_info_nearby:I = 0x7f0c008e

.field public static final no_video_info_nearby_contents:I = 0x7f0c008f

.field public static final no_video_info_nearby_for_chn:I = 0x7f0c0090

.field public static final no_video_info_s_link_no_sign_in:I = 0x7f0c0091

.field public static final no_video_info_s_link_sign_in:I = 0x7f0c0092

.field public static final no_video_no_network_info_nearby:I = 0x7f0c0093

.field public static final no_video_no_network_info_nearby_for_chn:I = 0x7f0c0094

.field public static final no_videos:I = 0x7f0c0095

.field public static final not_connected:I = 0x7f0c0096

.field public static final ok:I = 0x7f0c0097

.field public static final over_capacity:I = 0x7f0c0098

.field public static final paused:I = 0x7f0c0099

.field public static final pending:I = 0x7f0c011e

.field public static final personal:I = 0x7f0c009a

.field public static final personal_mode:I = 0x7f0c009b

.field public static final play:I = 0x7f0c009c

.field public static final playback:I = 0x7f0c009d

.field public static final playback_from:I = 0x7f0c009e

.field public static final playback_until:I = 0x7f0c009f

.field public static final possible:I = 0x7f0c00a0

.field public static final processing_popup:I = 0x7f0c00a1

.field public static final recently_played:I = 0x7f0c00a2

.field public static final refresh:I = 0x7f0c00a3

.field public static final registerd_device:I = 0x7f0c00a4

.field public static final remove:I = 0x7f0c00a5

.field public static final remove_from_items:I = 0x7f0c00a6

.field public static final remove_from_knox:I = 0x7f0c00a7

.field public static final remove_from_one_item:I = 0x7f0c00a8

.field public static final remove_from_private:I = 0x7f0c00a9

.field public static final remove_offline:I = 0x7f0c00aa

.field public static final rename:I = 0x7f0c00ab

.field public static final rental_info_after_activate:I = 0x7f0c011f

.field public static final rental_info_before_activate:I = 0x7f0c0120

.field public static final replace:I = 0x7f0c00ac

.field public static final row_item_count:I = 0x7f0c00ad

.field public static final runtime:I = 0x7f0c00ae

.field public static final s_link_charge_warning_info:I = 0x7f0c00af

.field public static final s_link_load_content:I = 0x7f0c00b0

.field public static final s_link_load_the_content_question:I = 0x7f0c00b1

.field public static final s_link_sharevia_download:I = 0x7f0c00b2

.field public static final samsung_apps_download_message:I = 0x7f0c00b3

.field public static final search:I = 0x7f0c00b4

.field public static final search_device:I = 0x7f0c00b5

.field public static final search_empty:I = 0x7f0c00b6

.field public static final search_hint:I = 0x7f0c00b7

.field public static final select_all:I = 0x7f0c00b8

.field public static final select_quality:I = 0x7f0c0121

.field public static final selected:I = 0x7f0c00b9

.field public static final series_details:I = 0x7f0c0122

.field public static final series_n:I = 0x7f0c0123

.field public static final settings:I = 0x7f0c00ba

.field public static final sign_in:I = 0x7f0c00bb

.field public static final small:I = 0x7f0c00bc

.field public static final sortby_recently_viewed:I = 0x7f0c00bd

.field public static final sortyby:I = 0x7f0c00be

.field public static final sortybyadded:I = 0x7f0c00bf

.field public static final sortybydate:I = 0x7f0c0124

.field public static final sortybysize:I = 0x7f0c00c0

.field public static final sortybytitle:I = 0x7f0c00c1

.field public static final sortybytype:I = 0x7f0c00c2

.field public static final stars:I = 0x7f0c0125

.field public static final start_contents_download:I = 0x7f0c00c3

.field public static final start_download:I = 0x7f0c00c4

.field public static final stereo:I = 0x7f0c00c5

.field public static final stms_appgroup:I = 0x7f0c00c6

.field public static final stms_version:I = 0x7f0c00c7

.field public static final subtitles:I = 0x7f0c00c8

.field public static final suspended:I = 0x7f0c0126

.field public static final sync_with_dropbox_cloud:I = 0x7f0c00c9

.field public static final talkback_back:I = 0x7f0c00ca

.field public static final talkback_delete_query:I = 0x7f0c00cb

.field public static final talkback_go_to_details:I = 0x7f0c0127

.field public static final talkback_goto_view_thumbnail:I = 0x7f0c00cc

.field public static final talkback_navigate_up:I = 0x7f0c00cd

.field public static final talkback_private:I = 0x7f0c00ce

.field public static final talkback_search_query:I = 0x7f0c00cf

.field public static final talkback_select_button:I = 0x7f0c00d0

.field public static final talkback_tab:I = 0x7f0c00d1

.field public static final talkback_tab_count:I = 0x7f0c00d2

.field public static final talkback_tap:I = 0x7f0c0128

.field public static final talkback_tap_to_play_video:I = 0x7f0c00d3

.field public static final talkback_view:I = 0x7f0c00d4

.field public static final tcloud:I = 0x7f0c00d5

.field public static final times:I = 0x7f0c00d6

.field public static final title_unknown:I = 0x7f0c00d7

.field public static final total_file_size:I = 0x7f0c00d8

.field public static final tts_header:I = 0x7f0c00d9

.field public static final tts_hours:I = 0x7f0c00da

.field public static final tts_minutes:I = 0x7f0c00db

.field public static final tts_seconds:I = 0x7f0c00dc

.field public static final un_download:I = 0x7f0c00dd

.field public static final un_download_msg:I = 0x7f0c00de

.field public static final unable_to_rename:I = 0x7f0c00df

.field public static final unselect_all:I = 0x7f0c00e0

.field public static final validity_period:I = 0x7f0c0129

.field public static final video_allshare_devices_found:I = 0x7f0c00e1

.field public static final video_allshare_download_fail:I = 0x7f0c00e2

.field public static final video_allshare_local_dms_full:I = 0x7f0c00e3

.field public static final video_allshare_no_contents_location:I = 0x7f0c00e4

.field public static final video_allshare_no_devices_found:I = 0x7f0c00e5

.field public static final video_allshare_scan_for_nearby_devices:I = 0x7f0c00e6

.field public static final video_allshare_scan_started:I = 0x7f0c00e7

.field public static final video_detail_drm_constraint_type_count:I = 0x7f0c00e8

.field public static final video_detail_drm_constraint_type_date:I = 0x7f0c00e9

.field public static final video_detail_drm_constraint_type_interval:I = 0x7f0c00ea

.field public static final video_detail_drm_constraint_type_time:I = 0x7f0c00eb

.field public static final video_detail_drm_constraint_type_unlimited:I = 0x7f0c00ec

.field public static final video_detail_drm_permission_type_display:I = 0x7f0c00ed

.field public static final video_detail_drm_permission_type_excute:I = 0x7f0c00ee

.field public static final video_detail_drm_permission_type_play:I = 0x7f0c00ef

.field public static final video_detail_drm_permission_type_print:I = 0x7f0c00f0

.field public static final video_drm_divx_back_key:I = 0x7f0c00f1

.field public static final video_drm_divx_continue:I = 0x7f0c00f2

.field public static final video_drm_divx_not_authorized:I = 0x7f0c00f3

.field public static final video_drm_divx_register_at:I = 0x7f0c00f4

.field public static final video_drm_divx_register_code:I = 0x7f0c00f5

.field public static final video_drm_divx_rental_expired1:I = 0x7f0c00f6

.field public static final video_drm_divx_rental_expired2:I = 0x7f0c00f7

.field public static final video_drm_first_render:I = 0x7f0c00f8

.field public static final video_drm_item_expired:I = 0x7f0c00f9

.field public static final video_drm_locked:I = 0x7f0c00fa

.field public static final video_drm_play_now_q:I = 0x7f0c00fb

.field public static final video_drm_this_item_no_longer_use:I = 0x7f0c00fc

.field public static final video_drm_unlock_it_q:I = 0x7f0c00fd

.field public static final video_drm_use_1_time:I = 0x7f0c00fe

.field public static final video_drm_use_n_times:I = 0x7f0c00ff

.field public static final video_editor_download_message:I = 0x7f0c0100

.field public static final video_expired:I = 0x7f0c0101

.field public static final video_hub_hevc_fhd_play:I = 0x7f0c0102

.field public static final video_hub_licensce_expired:I = 0x7f0c0103

.field public static final video_hub_licsense_days_remaining:I = 0x7f0c0104

.field public static final video_player:I = 0x7f0c0105

.field public static final video_selected:I = 0x7f0c0106

.field public static final videos_selected:I = 0x7f0c0107

.field public static final wi_fi:I = 0x7f0c0108

.field public static final wlan:I = 0x7f0c0109

.field public static final wlan_connection_required_for_chn:I = 0x7f0c010a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
