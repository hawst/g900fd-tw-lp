.class public Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;
.super Ljava/lang/Object;
.source "SingleSelectCmd.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/cmd/ICommand;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFilePath:Ljava/lang/String;

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private checkFalse(Landroid/widget/CheckBox;Landroid/net/Uri;)V
    .locals 1
    .param p1, "checkBox"    # Landroid/widget/CheckBox;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 44
    invoke-direct {p0, p2}, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->remove(Landroid/net/Uri;)V

    .line 45
    return-void
.end method

.method private checkTrue(Landroid/widget/CheckBox;Landroid/net/Uri;)V
    .locals 1
    .param p1, "checkBox"    # Landroid/widget/CheckBox;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 48
    invoke-direct {p0, p2}, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->put(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 51
    :cond_0
    return-void
.end method

.method private put(Landroid/net/Uri;)Z
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 54
    if-nez p1, :cond_0

    move v0, v2

    .line 69
    :goto_0
    return v0

    .line 55
    :cond_0
    const/4 v0, 0x0

    .line 56
    .local v0, "isSuccess":Z
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    .line 58
    .local v1, "mulitSelector":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeleteAttr()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 59
    const/4 v2, 0x1

    sput v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mSelectMode:I

    .line 64
    :goto_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isShareViaAttr()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, p1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->putBeforCheckCapacity(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 61
    :cond_1
    sput v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mSelectMode:I

    goto :goto_1

    .line 67
    :cond_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, p1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->put(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private remove(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mFilePath:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->remove(Landroid/net/Uri;Ljava/lang/String;)V

    .line 74
    return-void
.end method


# virtual methods
.method public execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 6
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 25
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mContext:Landroid/content/Context;

    .line 26
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 27
    .local v0, "c":Landroid/database/Cursor;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 28
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getView()Landroid/view/View;

    move-result-object v3

    .line 29
    .local v3, "view":Landroid/view/View;
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-static {v4, v0, v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v2

    .line 30
    .local v2, "uri":Landroid/net/Uri;
    const-string v4, "_data"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->mFilePath:Ljava/lang/String;

    .line 31
    const v4, 0x7f09002c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 32
    .local v1, "checkBox":Landroid/widget/CheckBox;
    if-eqz v1, :cond_0

    .line 33
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 34
    invoke-direct {p0, v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->checkFalse(Landroid/widget/CheckBox;Landroid/net/Uri;)V

    .line 38
    :goto_0
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 40
    :cond_0
    return-void

    .line 36
    :cond_1
    invoke-direct {p0, v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;->checkTrue(Landroid/widget/CheckBox;Landroid/net/Uri;)V

    goto :goto_0
.end method
