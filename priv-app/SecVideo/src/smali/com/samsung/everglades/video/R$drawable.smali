.class public final Lcom/samsung/everglades/video/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_check_box_animator_list:I = 0x7f020000

.field public static final baidu_icon:I = 0x7f020001

.field public static final btn_check_to_off_mtrl:I = 0x7f020002

.field public static final btn_check_to_on_mtrl:I = 0x7f020003

.field public static final custom_check_box:I = 0x7f020004

.field public static final custom_progress_bar_thumbnail:I = 0x7f020005

.field public static final f_airview_popup_picker_bg_dark:I = 0x7f020006

.field public static final fairview_internet_folder_hover_bg_light:I = 0x7f020007

.field public static final ic_delete:I = 0x7f020008

.field public static final ic_more_info:I = 0x7f020009

.field public static final ic_rename:I = 0x7f02000a

.field public static final ic_share_via:I = 0x7f02000b

.field public static final library_grid_thumbnail_default:I = 0x7f02000c

.field public static final library_icon_refresh:I = 0x7f02000d

.field public static final library_icon_refresh_focused:I = 0x7f02000e

.field public static final library_icon_refresh_pressed:I = 0x7f02000f

.field public static final library_icon_refresh_state:I = 0x7f020010

.field public static final library_list_icon_dropbox:I = 0x7f020011

.field public static final library_list_icon_folder:I = 0x7f020012

.field public static final library_list_progress_bar:I = 0x7f020013

.field public static final library_list_progress_bg:I = 0x7f020014

.field public static final library_list_thumbnail_default:I = 0x7f020015

.field public static final list_focus:I = 0x7f020016

.field public static final list_press:I = 0x7f020017

.field public static final list_select:I = 0x7f020018

.field public static final selector_deregisterd_delete_btn:I = 0x7f020019

.field public static final selector_select_list_view_effect:I = 0x7f02001a

.field public static final selector_select_list_view_item:I = 0x7f02001b

.field public static final selector_splitview_divider:I = 0x7f02001c

.field public static final tw_action_bar_icon_delete:I = 0x7f02001d

.field public static final tw_action_bar_icon_search_holo_dark:I = 0x7f02001e

.field public static final tw_action_bar_icon_share_via:I = 0x7f02001f

.field public static final tw_actionbar_btn_download_normal:I = 0x7f020020

.field public static final tw_btn_check_to_on_mtrl_000:I = 0x7f020021

.field public static final tw_btn_check_to_on_mtrl_001:I = 0x7f020022

.field public static final tw_btn_check_to_on_mtrl_002:I = 0x7f020023

.field public static final tw_btn_check_to_on_mtrl_003:I = 0x7f020024

.field public static final tw_btn_check_to_on_mtrl_004:I = 0x7f020025

.field public static final tw_btn_check_to_on_mtrl_005:I = 0x7f020026

.field public static final tw_btn_check_to_on_mtrl_006:I = 0x7f020027

.field public static final tw_btn_check_to_on_mtrl_007:I = 0x7f020028

.field public static final tw_btn_check_to_on_mtrl_008:I = 0x7f020029

.field public static final tw_btn_check_to_on_mtrl_009:I = 0x7f02002a

.field public static final tw_btn_check_to_on_mtrl_010:I = 0x7f02002b

.field public static final tw_btn_check_to_on_mtrl_011:I = 0x7f02002c

.field public static final tw_btn_check_to_on_mtrl_012:I = 0x7f02002d

.field public static final tw_btn_check_to_on_mtrl_013:I = 0x7f02002e

.field public static final tw_btn_check_to_on_mtrl_014:I = 0x7f02002f

.field public static final tw_btn_check_to_on_mtrl_015:I = 0x7f020030

.field public static final tw_ic_ab_back_holo_dark:I = 0x7f020031

.field public static final tw_list_focused_holo_dark:I = 0x7f020032

.field public static final tw_list_icon_minus_focused_holo_dark:I = 0x7f020033

.field public static final tw_list_icon_minus_holo_dark:I = 0x7f020034

.field public static final tw_list_icon_minus_pressed_holo_dark:I = 0x7f020035

.field public static final tw_list_pressed_holo_dark:I = 0x7f020036

.field public static final tw_list_selected_holo_dark:I = 0x7f020037

.field public static final tw_preference_contents_list_left_split_default_dark:I = 0x7f020038

.field public static final tw_preference_contents_list_left_split_default_press_dark:I = 0x7f020039

.field public static final tw_select_all_bg_holo_dark:I = 0x7f02003a

.field public static final tw_split_list_selected_holo_dark:I = 0x7f02003b

.field public static final video_air_view_zoom_in_btn_icon_delete:I = 0x7f02003c

.field public static final video_air_view_zoom_in_btn_icon_delete_focused:I = 0x7f02003d

.field public static final video_air_view_zoom_in_btn_icon_delete_pressed:I = 0x7f02003e

.field public static final video_air_view_zoom_in_btn_icon_detail:I = 0x7f02003f

.field public static final video_air_view_zoom_in_btn_icon_detail_focused:I = 0x7f020040

.field public static final video_air_view_zoom_in_btn_icon_detail_pressed:I = 0x7f020041

.field public static final video_air_view_zoom_in_btn_icon_rename:I = 0x7f020042

.field public static final video_air_view_zoom_in_btn_icon_rename_focused:I = 0x7f020043

.field public static final video_air_view_zoom_in_btn_icon_rename_pressed:I = 0x7f020044

.field public static final video_air_view_zoom_in_btn_icon_share:I = 0x7f020045

.field public static final video_air_view_zoom_in_btn_icon_share_focused:I = 0x7f020046

.field public static final video_air_view_zoom_in_btn_icon_share_pressed:I = 0x7f020047

.field public static final video_grid_ic_privatemode:I = 0x7f020048

.field public static final video_img_no_video:I = 0x7f020049

.field public static final video_slipon_number_bg:I = 0x7f02004a

.field public static final video_tcloud_icon:I = 0x7f02004b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
