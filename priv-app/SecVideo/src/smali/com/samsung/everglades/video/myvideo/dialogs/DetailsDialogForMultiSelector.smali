.class public Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;
.super Ljava/lang/Object;
.source "DetailsDialogForMultiSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;
    }
.end annotation


# instance fields
.field private COLON:Ljava/lang/String;

.field private mAdapter:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;

.field private mContext:Landroid/content/Context;

.field private mInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mSelectedItemCount:I

.field private mTotalSize:J

.field mUpdateTotalSizeListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mTotalSize:J

    .line 35
    const-string v0, " : "

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->COLON:Ljava/lang/String;

    .line 67
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$2;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$2;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 95
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$3;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$3;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mUpdateTotalSizeListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;

    .line 38
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 40
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mUpdateTotalSizeListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->setOnUpdateTotalSizeListener(Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;)V

    .line 41
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$102(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object p1
.end method

.method static synthetic access$202(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mSelectedItemCount:I

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mTotalSize:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;
    .param p1, "x1"    # J

    .prologue
    .line 22
    iput-wide p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mTotalSize:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;)Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mAdapter:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->COLON:Ljava/lang/String;

    return-object v0
.end method

.method private createInfoList()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 80
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mInfoList:Ljava/util/ArrayList;

    .line 81
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v2

    iput v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mSelectedItemCount:I

    .line 84
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0045

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mSelectedItemCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "fileSizeStr":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    iget-wide v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mTotalSize:J

    invoke-static {v2, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "sizeText":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mInfoList:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00d8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->COLON:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    return-void

    .line 87
    .end local v0    # "fileSizeStr":Ljava/lang/String;
    .end local v1    # "sizeText":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0107

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mSelectedItemCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "fileSizeStr":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public showDialog()V
    .locals 7

    .prologue
    const/16 v6, 0x100

    .line 44
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->createInfoList()V

    .line 46
    new-instance v3, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mInfoList:Ljava/util/ArrayList;

    invoke-direct {v3, p0, v4, v5}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mAdapter:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;

    .line 48
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 49
    .local v1, "listPadding":I
    new-instance v2, Landroid/widget/ListView;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 50
    .local v2, "listView":Landroid/widget/ListView;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mAdapter:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 51
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 52
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 54
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0c005e

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0c0097

    new-instance v5, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$1;

    invoke-direct {v5, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$1;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 62
    .local v0, "dialog":Landroid/app/AlertDialog;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 63
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 64
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 65
    return-void
.end method
