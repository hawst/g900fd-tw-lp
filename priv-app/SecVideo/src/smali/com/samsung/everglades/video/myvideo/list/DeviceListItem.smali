.class public Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;
.super Ljava/lang/Object;
.source "DeviceListItem.java"


# static fields
.field private static final NO_NETWORK_MODE:Ljava/lang/String; = "OFF"


# instance fields
.field private mDeviceID:Ljava/lang/String;

.field private mDeviceIcon:Landroid/graphics/Bitmap;

.field private mDeviceIconUri:Landroid/net/Uri;

.field private mDeviceName:Ljava/lang/String;

.field private mNetworkMode:Ljava/lang/String;

.field private mVideoCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceName:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;I)V
    .locals 0
    .param p1, "devcieId"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "icon"    # Landroid/graphics/Bitmap;
    .param p4, "networkMode"    # Ljava/lang/String;
    .param p5, "videoCount"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceID:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceName:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceIcon:Landroid/graphics/Bitmap;

    .line 38
    iput-object p4, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mNetworkMode:Ljava/lang/String;

    .line 39
    iput p5, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mVideoCount:I

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1
    .param p1, "devcieId"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "deviceIconUri"    # Landroid/net/Uri;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceID:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceName:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceIconUri:Landroid/net/Uri;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mNetworkMode:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mVideoCount:I

    .line 32
    return-void
.end method


# virtual methods
.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getDeviceIconUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceIconUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mNetworkMode:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoCount()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mVideoCount:I

    return v0
.end method

.method public noNetworkMode()Z
    .locals 2

    .prologue
    .line 63
    const-string v0, "OFF"

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->mNetworkMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
