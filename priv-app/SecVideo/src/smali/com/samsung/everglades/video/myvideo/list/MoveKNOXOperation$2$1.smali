.class Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;
.super Ljava/lang/Object;
.source "MoveKNOXOperation.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v3, 0x1

    .line 389
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->setAbortOperation(Z)V

    .line 390
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->cancelKnoxFileProcess()V

    .line 391
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v2

    monitor-enter v2

    .line 393
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 399
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mOnMoveFinishedListener:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$900(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 400
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mOnMoveFinishedListener:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$900(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;->onMoveKNOXFinished()V

    .line 402
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 403
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 404
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 405
    return-void

    .line 394
    :catch_0
    move-exception v0

    .line 395
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 397
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
