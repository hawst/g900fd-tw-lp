.class public Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
.super Ljava/lang/Thread;
.source "DeleteOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteFilesThread"
.end annotation


# instance fields
.field private abortOpertion:Z

.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mHandler:Landroid/os/Handler;

.field private mIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;


# direct methods
.method public constructor <init>(Ljava/util/Iterator;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 2
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/os/Handler;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    .line 213
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 208
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mIterator:Ljava/util/Iterator;

    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->abortOpertion:Z

    .line 210
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 211
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 214
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mIterator:Ljava/util/Iterator;

    .line 215
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mHandler:Landroid/os/Handler;

    .line 216
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mContext:Landroid/content/Context;

    .line 217
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 218
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 219
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 220
    return-void
.end method

.method private deleteFiles(Ljava/util/Iterator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 238
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    if-nez p1, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->abortOpertion:Z

    if-nez v2, :cond_0

    .line 247
    :goto_1
    :try_start_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 248
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->abortOpertion:Z

    if-nez v2, :cond_0

    .line 252
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 253
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v2, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 254
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v2, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->deleteContent(Landroid/net/Uri;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 263
    .end local v1    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 265
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 255
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SLinkMedia"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 256
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v2, v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->deleteFile(Landroid/net/Uri;)V

    goto :goto_1

    .line 257
    :cond_3
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v2, v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->isSecretContent(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 258
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v2, v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->deleteSecretContent(Landroid/net/Uri;)I

    goto :goto_1

    .line 260
    :cond_4
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v2, v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->deleteVideo(Landroid/net/Uri;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 229
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mIterator:Ljava/util/Iterator;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->deleteFiles(Ljava/util/Iterator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 232
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 233
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->abortOpertion:Z

    .line 235
    return-void

    .line 231
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 232
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 233
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->abortOpertion:Z

    throw v0
.end method

.method public declared-synchronized setAbortOperation(Z)V
    .locals 1
    .param p1, "isAborted"    # Z

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->abortOpertion:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    monitor-exit p0

    return-void

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
