.class public Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
.super Ljava/lang/Object;
.source "FolderHoverViewer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;,
        Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;,
        Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$SimpleGestureListener;,
        Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;
    }
.end annotation


# static fields
.field private static final DISMISS_POPUP:I = 0x2

.field private static final HOVER_DETECT_TIME_MS:I = 0x190

.field private static final HOVER_ENTER:I = 0x1

.field private static final IMAGEVIEW_MAX_COUNT:I = 0x9

.field private static final MOVE_PAGE:I = 0x0

.field private static final SHOW_POPUP:I = 0x1

.field private static final SWIPE_LEFT:I = 0xa

.field private static final SWIPE_MIN_DISTACE:I = 0xc8

.field private static final SWIPE_RIGHT:I = 0xb


# instance fields
.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private mContext:Landroid/content/Context;

.field private mCurrentIndex:I

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mEnsureLocalBitmapsTask:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

.field private mFilepath:[Ljava/lang/String;

.field private mFolderHoverListener:Landroid/view/View$OnHoverListener;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mHandler:Landroid/os/Handler;

.field private mHoverRect:Landroid/graphics/Rect;

.field private mInternalHoverListener:Landroid/view/View$OnHoverListener;

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mPage:I

.field private mPopup:Landroid/widget/PopupWindow;

.field private mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

.field private mPreviewGridView:Landroid/widget/GridView;

.field private mPreviewLayout:Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;

.field private mRootView:Landroid/view/View;

.field private mThumbGridAdapter:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;

.field private mThumbList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mThumbList:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHoverRect:Landroid/graphics/Rect;

    .line 63
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    .line 64
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mEnsureLocalBitmapsTask:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    .line 71
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mFilepath:[Ljava/lang/String;

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    .line 75
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mGestureDetector:Landroid/view/GestureDetector;

    .line 144
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$3;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mFolderHoverListener:Landroid/view/View$OnHoverListener;

    .line 212
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$4;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$4;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mInternalHoverListener:Landroid/view/View$OnHoverListener;

    .line 309
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$5;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    .line 78
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mRootView:Landroid/view/View;

    .line 80
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 81
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 82
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->initFolderHoverView(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mFilepath:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;IJI)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
    .param p1, "x1"    # I
    .param p2, "x2"    # J
    .param p4, "x3"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->requestShowPopup(IJI)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->requestHidePopup()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->removeDismissPopupMsg()V

    return-void
.end method

.method static synthetic access$1302(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mTotalCount:I

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->updateThumbnailLayout(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->createFolderPreviewPopup(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->moveFolderAirView(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/DB;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->dismissPopup()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->updateHoveringIcon(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHoverRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$802(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->ensureLocalBitmap()V

    return-void
.end method

.method private createFolderPreviewPopup(I)V
    .locals 7
    .param p1, "itemCount"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v4, -0x2

    .line 508
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 509
    .local v0, "startMarginX":I
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 510
    .local v1, "startMarginY":I
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_0

    .line 511
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    .line 516
    :goto_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mRootView:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHoverRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHoverRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v1

    invoke-virtual {v2, v3, v6, v4, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 517
    return-void

    .line 513
    :cond_0
    new-instance v2, Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewLayout:Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;

    invoke-direct {v2, v3, v4, v4, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    goto :goto_0
.end method

.method private dismissPopup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 285
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    .line 287
    const/4 v1, 0x1

    :try_start_0
    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->updateHoveringIcon(I)V

    .line 288
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    .line 295
    :cond_0
    :goto_0
    const-string v1, "FolderHoverViewer - dimissPopup()"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 296
    return-void

    .line 289
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPopup:Landroid/widget/PopupWindow;

    throw v1
.end method

.method private ensureLocalBitmap()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 454
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mEnsureLocalBitmapsTask:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    if-nez v0, :cond_0

    .line 455
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    invoke-direct {v0, p0, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$1;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mEnsureLocalBitmapsTask:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    .line 456
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mEnsureLocalBitmapsTask:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mEnsureLocalBitmapsTask:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 459
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mEnsureLocalBitmapsTask:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    .line 460
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    invoke-direct {v0, p0, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$1;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mEnsureLocalBitmapsTask:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    .line 461
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mEnsureLocalBitmapsTask:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 463
    :cond_1
    return-void
.end method

.method private initFolderHoverView(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewLayout:Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;

    const v1, 0x7f04001b

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewLayout:Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewLayout:Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewLayout:Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mInternalHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 96
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewLayout:Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;

    const v1, 0x7f090045

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/view/HoverInterceptLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    .line 97
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVerticalScrollBarEnabled(Z)V

    .line 100
    :cond_1
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mThumbList:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;

    const v2, 0x7f04001a

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mThumbList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mThumbGridAdapter:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;

    .line 104
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mThumbGridAdapter:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 106
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 107
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$1;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 116
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 138
    :cond_2
    return-void
.end method

.method private moveFolderAirView(I)V
    .locals 6
    .param p1, "swipeEvent"    # I

    .prologue
    const-wide/16 v4, 0x190

    const/4 v2, 0x0

    .line 233
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FolderHoverViewer - moveFolderAirView() ENTER - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 234
    packed-switch p1, :pswitch_data_0

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 236
    :pswitch_0
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    .line 237
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    .line 238
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    mul-int/lit8 v0, v0, 0x9

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCurrentIndex:I

    .line 239
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCurrentIndex:I

    invoke-direct {p0, v0, v4, v5, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->requestShowPopup(IJI)V

    goto :goto_0

    .line 240
    :cond_1
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    if-nez v0, :cond_0

    .line 241
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mTotalCount:I

    rem-int/lit8 v0, v0, 0x9

    if-eqz v0, :cond_2

    .line 242
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mTotalCount:I

    div-int/lit8 v0, v0, 0x9

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    .line 246
    :goto_1
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    mul-int/lit8 v0, v0, 0x9

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCurrentIndex:I

    .line 247
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCurrentIndex:I

    invoke-direct {p0, v0, v4, v5, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->requestShowPopup(IJI)V

    goto :goto_0

    .line 244
    :cond_2
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mTotalCount:I

    div-int/lit8 v0, v0, 0x9

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    goto :goto_1

    .line 252
    :pswitch_1
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x9

    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mTotalCount:I

    if-ge v0, v1, :cond_3

    .line 253
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    .line 254
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCurrentIndex:I

    invoke-direct {p0, v0, v4, v5, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->requestShowPopup(IJI)V

    goto :goto_0

    .line 256
    :cond_3
    iput v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPage:I

    .line 257
    iput v2, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCurrentIndex:I

    .line 258
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCurrentIndex:I

    invoke-direct {p0, v0, v4, v5, v2}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->requestShowPopup(IJI)V

    goto :goto_0

    .line 234
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private removeDismissPopupMsg()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 279
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 282
    :cond_0
    return-void
.end method

.method private removeShowPopupMsg()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 273
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 276
    :cond_0
    return-void
.end method

.method private requestHidePopup()V
    .locals 4

    .prologue
    .line 305
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 306
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x19a

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 307
    return-void
.end method

.method private requestShowPopup(IJI)V
    .locals 4
    .param p1, "startIndex"    # I
    .param p2, "delayMillis"    # J
    .param p4, "mode"    # I

    .prologue
    .line 299
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, p4, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 300
    .local v0, "message":Landroid/os/Message;
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->removeShowPopupMsg()V

    .line 301
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 302
    return-void
.end method

.method private setFilePath(ILjava/lang/String;)V
    .locals 1
    .param p1, "n"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 344
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mFilepath:[Ljava/lang/String;

    aput-object p2, v0, p1

    .line 345
    return-void
.end method

.method private updateHoveringIcon(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 266
    const/4 v1, -0x1

    :try_start_0
    invoke-static {p1, v1}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    :goto_0
    return-void

    .line 267
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateThumbnailLayout(I)V
    .locals 7
    .param p1, "startIndex"    # I

    .prologue
    const/4 v6, 0x1

    .line 348
    const/4 v3, 0x0

    .line 349
    .local v3, "thumbFilePath":Ljava/lang/String;
    const/4 v0, 0x0

    .line 350
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 352
    .local v1, "cursor":Landroid/database/Cursor;
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 353
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 354
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mThumbList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 356
    move v2, p1

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCount()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 357
    const-string v4, "_data"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 358
    if-eqz v3, :cond_2

    .line 359
    rem-int/lit8 v4, v2, 0x9

    invoke-direct {p0, v4, v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->setFilePath(ILjava/lang/String;)V

    .line 360
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isDropboxCloudCursor()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isSKTCloudCursor()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 361
    :cond_0
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v4, v3}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->requestThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 365
    :goto_1
    if-nez v0, :cond_1

    .line 366
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020049

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 368
    :cond_1
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mThumbList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 380
    :cond_3
    :goto_2
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCount()I

    move-result v4

    if-gt v4, v6, :cond_7

    .line 381
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    invoke-virtual {v4, v6}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 387
    :goto_3
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mThumbGridAdapter:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$ThumbGridAdapter;->notifyDataSetChanged()V

    .line 388
    return-void

    .line 363
    :cond_4
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 375
    :cond_5
    rem-int/lit8 v4, v2, 0x9

    const/16 v5, 0x8

    if-ne v4, v5, :cond_6

    .line 376
    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCurrentIndex:I

    goto :goto_2

    .line 356
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 382
    :cond_7
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCount()I

    move-result v4

    const/4 v5, 0x4

    if-gt v4, v5, :cond_8

    .line 383
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_3

    .line 385
    :cond_8
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewGridView:Landroid/widget/GridView;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_3
.end method


# virtual methods
.method public dismissFolderPreviewPopup()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 520
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 524
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->dismissPopup()V

    .line 526
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    if-eqz v0, :cond_1

    .line 527
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->closeCursor()V

    .line 529
    :cond_1
    return-void
.end method

.method public getFolderHoverListener()Landroid/view/View$OnHoverListener;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mFolderHoverListener:Landroid/view/View$OnHoverListener;

    return-object v0
.end method

.method public getGestureDetector()Landroid/view/GestureDetector;
    .locals 4

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mGestureDetector:Landroid/view/GestureDetector;

    if-nez v0, :cond_0

    .line 431
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$SimpleGestureListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$SimpleGestureListener;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mGestureDetector:Landroid/view/GestureDetector;

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method public setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 0
    .param p1, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 142
    return-void
.end method
