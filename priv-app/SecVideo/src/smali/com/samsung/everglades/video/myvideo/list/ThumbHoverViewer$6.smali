.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$6;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->playVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$6;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1, "player"    # Landroid/media/MediaPlayer;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 481
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ThumbHoverViewer - onError :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 482
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->reset()V

    .line 483
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$6;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 484
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$6;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 485
    const/4 v0, 0x0

    return v0
.end method
