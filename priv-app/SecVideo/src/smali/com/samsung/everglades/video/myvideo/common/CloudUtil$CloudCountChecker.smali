.class Lcom/samsung/everglades/video/myvideo/common/CloudUtil$CloudCountChecker;
.super Landroid/os/AsyncTask;
.source "CloudUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CloudCountChecker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;


# direct methods
.method private constructor <init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)V
    .locals 0

    .prologue
    .line 1411
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$CloudCountChecker;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Lcom/samsung/everglades/video/myvideo/common/CloudUtil$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    .param p2, "x1"    # Lcom/samsung/everglades/video/myvideo/common/CloudUtil$1;

    .prologue
    .line 1411
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$CloudCountChecker;-><init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1411
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$CloudCountChecker;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v4, 0x0

    .line 1414
    const/4 v1, -0x1

    .line 1415
    .local v1, "count":I
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$CloudCountChecker;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    const/4 v3, 0x0

    invoke-virtual {v2, v4, v4, v3}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getMergedCursorDropboxOnly(Landroid/database/Cursor;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    .line 1417
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    .line 1418
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 1420
    if-lez v1, :cond_1

    # getter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudCount:I
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$500()I

    move-result v2

    if-eq v2, v1, :cond_1

    .line 1421
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$CloudCountChecker;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnCloudDBCountChanged:Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$600(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1422
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$CloudCountChecker;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mOnCloudDBCountChanged:Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$600(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;->onCountChanged()V

    .line 1424
    :cond_0
    # setter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudCount:I
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$502(I)I

    .line 1426
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1428
    :cond_2
    return-object v4
.end method
