.class Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;
.super Landroid/content/BroadcastReceiver;
.source "MoveKNOXOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const v9, 0x7f0c0079

    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 270
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 271
    .local v0, "action":Ljava/lang/String;
    const-string v6, "PACKAGENAME"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 272
    .local v3, "packagename":Ljava/lang/String;
    const-string v6, "SUCCESSCNT"

    invoke-virtual {p2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 273
    .local v5, "successcnt":I
    const/4 v2, 0x0

    .line 274
    .local v2, "msg":Ljava/lang/String;
    if-eqz v3, :cond_0

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 275
    const-string v6, "com.sec.knox.container.FileRelayDone"

    if-ne v0, v6, :cond_1

    .line 276
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->incMovedFileCount()I

    .line 277
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 278
    const-string v6, "Broadcast receive : INTENT_FILE_RELAY_DONE"

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    const-string v6, "com.sec.knox.container.FileRelayComplete"

    if-ne v0, v6, :cond_4

    .line 280
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 281
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 284
    if-lez v5, :cond_3

    .line 285
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f0b0000

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v5, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 309
    :cond_2
    :goto_1
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v2, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 310
    const-string v6, "Broadcast receive : INTENT_FILE_RELAY_COMPLETE"

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 287
    :cond_3
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->errno:I
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$300(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 301
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 304
    :goto_2
    if-eqz v2, :cond_2

    .line 305
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v2, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 295
    :pswitch_0
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 296
    goto :goto_2

    .line 298
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c007a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 299
    goto :goto_2

    .line 311
    :cond_4
    const-string v6, "com.sec.knox.container.FileRelayExist"

    if-ne v0, v6, :cond_5

    .line 312
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->incFailedFileCount()I

    .line 313
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;

    move-result-object v6

    const-string v7, "EXIST"

    invoke-static {v6, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 314
    const-string v6, "Broadcast receive : INTENT_FILE_RELAY_EXIST"

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 315
    :cond_5
    const-string v6, "com.sec.knox.container.FileRelayFail"

    if-ne v0, v6, :cond_6

    .line 316
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    const-string v7, "ERRORCODE"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->errno:I
    invoke-static {v6, v7}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$302(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;I)I

    .line 317
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->incFailedFileCount()I

    .line 318
    const-string v6, "Broadcast receive : INTENT_FILE_RELAY_FAIL"

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 319
    :cond_6
    const-string v6, "com.sec.knox.container.FileRelayProgress"

    if-ne v0, v6, :cond_0

    .line 320
    const-string v6, "PROGRESS"

    invoke-virtual {p2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 321
    .local v4, "progress":I
    const/4 v1, 0x0

    .line 322
    .local v1, "mesg":Landroid/os/Message;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMessage:Landroid/os/Message;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Message;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 323
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMessage:Landroid/os/Message;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Message;

    move-result-object v6

    invoke-static {v6}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 324
    iput v4, v1, Landroid/os/Message;->arg1:I

    .line 325
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;->this$0:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
