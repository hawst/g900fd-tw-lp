.class public Lcom/samsung/everglades/video/myvideo/common/ListType;
.super Ljava/lang/Object;
.source "ListType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/common/ListType$Additions;,
        Lcom/samsung/everglades/video/myvideo/common/ListType$Attribute;,
        Lcom/samsung/everglades/video/myvideo/common/ListType$Type;
    }
.end annotation


# instance fields
.field private mAdditions:I

.field private mAttribute:I

.field private mType:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    .line 37
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    .line 38
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAdditions:I

    .line 41
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    .line 42
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    .line 43
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAdditions:I

    .line 44
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "attribute"    # I

    .prologue
    const/4 v0, -0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    .line 37
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    .line 38
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAdditions:I

    .line 47
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    .line 48
    iput p2, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAdditions:I

    .line 50
    return-void
.end method

.method public constructor <init>(III)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "attribute"    # I
    .param p3, "additions"    # I

    .prologue
    const/4 v0, -0x1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    .line 37
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    .line 38
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAdditions:I

    .line 53
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    .line 54
    iput p2, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    .line 55
    iput p3, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAdditions:I

    .line 56
    return-void
.end method


# virtual methods
.method public getAdditions()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAdditions:I

    return v0
.end method

.method public getListAttribute()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    return v0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    return v0
.end method

.method public isCloudDownloadAttr()Z
    .locals 2

    .prologue
    .line 178
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeleteAttr()Z
    .locals 2

    .prologue
    .line 150
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeviceFileLists()Z
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeviceSearchFileLists()Z
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbySearchFileList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkSearchFileList()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDevicesList()Z
    .locals 2

    .prologue
    .line 105
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDropboxFolderAttr()Z
    .locals 2

    .prologue
    .line 190
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFolder()Z
    .locals 2

    .prologue
    .line 97
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFolderContent()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 204
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAdditions:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGrid()Z
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isJustList()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 93
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isKNOXAttr()Z
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isMoveKNOXAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isRemoveKNOXAttr()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isListOrFolder()Z
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isJustList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLocalOnly()Z
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isVideoEditorAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyDownloadAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isShareViaAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isKNOXAttr()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMoveKNOXAttr()Z
    .locals 2

    .prologue
    .line 154
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNearbyDownloadAttr()Z
    .locals 2

    .prologue
    .line 174
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNearbyFileList()Z
    .locals 2

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNearbySearchFileList()Z
    .locals 2

    .prologue
    .line 117
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNormalAttr()Z
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNormalMenuSupports()Z
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isJustList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemoveKNOXAttr()Z
    .locals 2

    .prologue
    .line 158
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSLinkDownloadAttr()Z
    .locals 2

    .prologue
    .line 182
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSLinkFileList()Z
    .locals 2

    .prologue
    .line 113
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSLinkSearchFileList()Z
    .locals 2

    .prologue
    .line 121
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearch()Z
    .locals 2

    .prologue
    .line 133
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectAttr()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 146
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeleteAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isVideoEditorAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyDownloadAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isCloudDownloadAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkDownloadAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isShareViaAttr()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isKNOXAttr()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShareViaAttr()Z
    .locals 2

    .prologue
    .line 170
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSplitFolderContent()Z
    .locals 2

    .prologue
    .line 208
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAdditions:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTypical()Z
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isJustList()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoEditorAttr()Z
    .locals 2

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setNormalMode()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    .line 68
    return-void
.end method

.method public setSelectionMode()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    .line 60
    return-void
.end method

.method public setSelectionModeForDelete()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mAttribute:I

    .line 64
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/common/ListType;->mType:I

    .line 86
    return-void
.end method
