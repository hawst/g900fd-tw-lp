.class public Lcom/samsung/everglades/video/myvideo/cmd/ScanForNearbyDevicesCmd;
.super Ljava/lang/Object;
.source "ScanForNearbyDevicesCmd.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/cmd/ICommand;


# instance fields
.field private mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

.field private mActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private refreshRemoteDevices()V
    .locals 1

    .prologue
    .line 24
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->CHINA:Z

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/ScanForNearbyDevicesCmd;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->createASFRemoteServiceProvider()V

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/ScanForNearbyDevicesCmd;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->refreshRemoteDevice()V

    .line 28
    return-void
.end method


# virtual methods
.method public execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 1
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 18
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/ScanForNearbyDevicesCmd;->mActivity:Landroid/app/Activity;

    .line 19
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/ScanForNearbyDevicesCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/ScanForNearbyDevicesCmd;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 20
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/ScanForNearbyDevicesCmd;->refreshRemoteDevices()V

    .line 21
    return-void
.end method
