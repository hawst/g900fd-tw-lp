.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->initSpinner()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 1188
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v1, 0x0

    .line 1191
    packed-switch p3, :pswitch_data_0

    .line 1208
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateSpinnerWidth(I)V

    .line 1209
    return-void

    .line 1196
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->isAllItemSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1197
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectAll(Z)V
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$300(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;Z)V

    .line 1201
    :goto_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->invalidateActionMode()V

    goto :goto_0

    .line 1199
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectAll(Z)V
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$300(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;Z)V

    goto :goto_1

    .line 1204
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectAll(Z)V
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$300(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;Z)V

    .line 1205
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$15;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->invalidateActionMode()V

    goto :goto_0

    .line 1191
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1213
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
