.class public Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;
.super Ljava/lang/Object;
.source "SelectionAnimator.java"


# instance fields
.field private CHECKBOX_FLOATING_DEFAULT_DURATION:I

.field private SINE_IN_OUT_70:Landroid/view/animation/Interpolator;

.field private mContext:Landroid/content/Context;

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mView:Lcom/sec/android/touchwiz/widget/TwGridView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/touchwiz/widget/TwGridView;Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 1
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "v"    # Lcom/sec/android/touchwiz/widget/TwGridView;
    .param p3, "t"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->CHECKBOX_FLOATING_DEFAULT_DURATION:I

    .line 28
    new-instance v0, Landroid/view/animation/interpolator/SineInOut70;

    invoke-direct {v0}, Landroid/view/animation/interpolator/SineInOut70;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->SINE_IN_OUT_70:Landroid/view/animation/Interpolator;

    .line 31
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mContext:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    .line 33
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->getChbTransX()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Landroid/view/animation/Interpolator;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->SINE_IN_OUT_70:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    .prologue
    .line 18
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->CHECKBOX_FLOATING_DEFAULT_DURATION:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getChbTransX()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 179
    const/4 v2, 0x0

    .line 180
    .local v2, "val":I
    const/4 v0, 0x0

    .line 181
    .local v0, "chb":Landroid/view/View;
    const/4 v1, 0x0

    .line 183
    .local v1, "chklayoutparams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 184
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f09002c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 187
    :cond_0
    if-eqz v0, :cond_1

    .line 188
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "chklayoutparams":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 189
    .restart local v1    # "chklayoutparams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    neg-int v3, v3

    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v2, v3, v4

    .line 191
    :cond_1
    return v2
.end method


# virtual methods
.method public ShowCheckBox()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    if-nez v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwGridView;->invalidate()V

    .line 41
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0
.end method

.method public hideCheckBox()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    if-nez v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0
.end method
