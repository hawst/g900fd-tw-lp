.class Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$10;
.super Landroid/content/BroadcastReceiver;
.source "DeviceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V
    .locals 0

    .prologue
    .line 877
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 880
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v0

    .line 881
    .local v0, "currentState":Z
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mWifiConntected:Z
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1800(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 882
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # setter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mWifiConntected:Z
    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1802(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;Z)Z

    .line 883
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1900(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesEmptyTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$1900(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 884
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setNoVideoNearby()V
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$2000(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V

    .line 887
    :cond_0
    return-void
.end method
