.class public Lcom/samsung/everglades/video/myvideo/cmd/OpenFolderCmd;
.super Ljava/lang/Object;
.source "OpenFolderCmd.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/cmd/ICommand;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkDropboxFolder(Landroid/content/Context;I)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bucketId"    # I

    .prologue
    .line 50
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxFolder(I)Z

    move-result v0

    .line 53
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 11
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    const/4 v10, 0x1

    .line 18
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 19
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 20
    .local v1, "c":Landroid/database/Cursor;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_0

    .line 21
    const/4 v0, 0x0

    .line 24
    .local v0, "bucketId":I
    :try_start_0
    const-string v7, "bucket_id"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 30
    const/4 v6, -0x1

    .line 31
    .local v6, "listAttr":I
    invoke-virtual {p0, v2, v0}, Lcom/samsung/everglades/video/myvideo/cmd/OpenFolderCmd;->checkDropboxFolder(Landroid/content/Context;I)Z

    move-result v5

    .line 32
    .local v5, "isDropboxFolder":Z
    if-eqz v5, :cond_1

    .line 33
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v7

    const-string v8, "is_dropbox_folder_contents"

    invoke-virtual {v7, v8, v10}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;Z)V

    .line 34
    const/16 v6, 0xa

    .line 39
    :goto_0
    new-instance v4, Landroid/content/Intent;

    const-class v7, Lcom/samsung/everglades/video/myvideo/activities/FolderItemList;

    invoke-direct {v4, v2, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 40
    .local v4, "intent":Landroid/content/Intent;
    const-string v7, "bucket_id"

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 41
    const-string v7, "list_attribute"

    invoke-virtual {v4, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 42
    const-string v7, "list_additional_infos"

    invoke-virtual {v4, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 43
    const-string v7, "dropbox_folder"

    invoke-virtual {v4, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 44
    const-string v7, "bucket_name"

    const-string v8, "bucket_display_name"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    invoke-virtual {v2, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 47
    .end local v0    # "bucketId":I
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v5    # "isDropboxFolder":Z
    .end local v6    # "listAttr":I
    :cond_0
    :goto_1
    return-void

    .line 25
    .restart local v0    # "bucketId":I
    :catch_0
    move-exception v3

    .line 26
    .local v3, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v3}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 36
    .end local v3    # "e":Ljava/lang/IllegalStateException;
    .restart local v5    # "isDropboxFolder":Z
    .restart local v6    # "listAttr":I
    :cond_1
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v7

    const-string v8, "is_dropbox_folder_contents"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;Z)V

    goto :goto_0
.end method
