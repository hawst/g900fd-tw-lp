.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 957
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 958
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 961
    const-string v0, "ThumbHoverViewer - surfaceCreated()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 962
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, p1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2502(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 963
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->playVideo()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2600(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    .line 964
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v2, 0x0

    .line 967
    const-string v0, "ThumbHoverViewer - surfaceDestroyed()"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 968
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2502(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 970
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2700(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 971
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setAIAContext(Z)V
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2800(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Z)V

    .line 972
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2700(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 973
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0, v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2702(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    .line 975
    :cond_0
    return-void
.end method
