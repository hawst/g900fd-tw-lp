.class public interface abstract Lcom/samsung/everglades/video/myvideo/common/ListType$Attribute;
.super Ljava/lang/Object;
.source "ListType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/ListType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Attribute"
.end annotation


# static fields
.field public static final CLOUD_DOWNLOAD:I = 0x6

.field public static final DELETE:I = 0x2

.field public static final DROPBOX_FOLDER_CONTENT:I = 0xa

.field public static final MOVE_KNOX:I = 0x8

.field public static final NEARBY_DOWNLOAD:I = 0x5

.field public static final NORMAL:I = 0x0

.field public static final REMOVE_KNOX:I = 0x9

.field public static final SELECT:I = 0x1

.field public static final SHARE_VIA:I = 0x3

.field public static final S_LINK_DOWNLOAD:I = 0x7

.field public static final VIDEO_EDITOR:I = 0x4
