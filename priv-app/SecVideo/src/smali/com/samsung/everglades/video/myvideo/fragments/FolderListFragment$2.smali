.class Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;
.super Ljava/lang/Object;
.source "FolderListFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 110
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getWindowWidth()I
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$400(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)I

    move-result v0

    .line 112
    .local v0, "width":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FolderListFragment - onGlobalLayout() width :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 113
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mGlobalLayoutWidth:I
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 115
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # setter for: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mGlobalLayoutWidth:I
    invoke-static {v1, v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$502(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;I)I

    .line 116
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isMultiWindow(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$200(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 117
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->setSplitWidth(ZZ)V

    .line 121
    :cond_0
    return-void
.end method
