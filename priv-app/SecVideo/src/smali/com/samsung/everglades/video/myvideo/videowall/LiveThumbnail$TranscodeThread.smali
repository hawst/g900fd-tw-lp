.class public Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;
.super Ljava/lang/Thread;
.source "LiveThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TranscodeThread"
.end annotation


# instance fields
.field private cChapter:I

.field private cName:Ljava/lang/String;

.field private cPath:Ljava/lang/String;

.field private cVid:J

.field private cthumbtime:I

.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;


# direct methods
.method public constructor <init>(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 167
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    .line 168
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 162
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cPath:Ljava/lang/String;

    .line 163
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cName:Ljava/lang/String;

    .line 169
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 181
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cVid:J

    iget v6, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cChapter:I

    iget v7, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cthumbtime:I

    # invokes: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JII)V
    invoke-static/range {v1 .. v7}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$700(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;Ljava/lang/String;Ljava/lang/String;JII)V

    .line 182
    return-void
.end method

.method public setArgs(Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 1
    .param p1, "pPath"    # Ljava/lang/String;
    .param p2, "pName"    # Ljava/lang/String;
    .param p3, "pVideoid"    # J
    .param p5, "pChapter"    # I
    .param p6, "pthumbtime"    # I

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cPath:Ljava/lang/String;

    .line 173
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cName:Ljava/lang/String;

    .line 174
    iput-wide p3, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cVid:J

    .line 175
    iput p5, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cChapter:I

    .line 176
    iput p6, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->cthumbtime:I

    .line 177
    return-void
.end method
