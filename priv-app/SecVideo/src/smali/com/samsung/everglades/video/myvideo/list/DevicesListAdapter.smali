.class public Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "DevicesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private deviceIcon:Landroid/widget/ImageView;

.field private deviceTitle:Landroid/widget/TextView;

.field private listDivider:Landroid/widget/LinearLayout;

.field private vi:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 31
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->vi:Landroid/view/LayoutInflater;

    .line 32
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 36
    move-object v2, p2

    .line 38
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_0

    .line 39
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->vi:Landroid/view/LayoutInflater;

    const v4, 0x7f040004

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 41
    :cond_0
    const v3, 0x7f090010

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->deviceIcon:Landroid/widget/ImageView;

    .line 42
    const v3, 0x7f090011

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->deviceTitle:Landroid/widget/TextView;

    .line 43
    const v3, 0x7f090012

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->listDivider:Landroid/widget/LinearLayout;

    .line 44
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    .line 46
    .local v1, "deviceItem":Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;
    if-eqz v1, :cond_2

    .line 47
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->deviceTitle:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    .line 48
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->deviceTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getDeviceIconUri()Landroid/net/Uri;

    move-result-object v3

    if-nez v3, :cond_3

    .line 52
    const-string v3, "DeviceArrayAdapter no thumbnail"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 53
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->deviceIcon:Landroid/widget/ImageView;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    :cond_2
    :goto_0
    return-object v2

    .line 57
    :cond_3
    const/4 v0, 0x0

    .line 58
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getDeviceIconUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 60
    if-nez v0, :cond_4

    .line 61
    const-string v3, "DeviceArrayAdapter downloader == null"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 62
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->deviceIcon:Landroid/widget/ImageView;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_2

    .line 68
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->listDivider:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 64
    :cond_4
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DevicesListAdapter;->deviceIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method
