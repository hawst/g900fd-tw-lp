.class public Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;
.super Ljava/lang/Object;
.source "NearbyListUpdater.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

.field private mContext:Landroid/content/Context;

.field private mProgress:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mContext:Landroid/content/Context;

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    .line 25
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getNIC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 109
    .local v0, "nic":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getSelectedDmsNIC()Ljava/lang/String;

    move-result-object v0

    .line 113
    :cond_0
    return-object v0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    const/4 v0, 0x0

    .line 100
    .local v0, "providerName":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getSelectedRemoteProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getSelectedRemoteProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v0

    .line 103
    :cond_0
    return-object v0
.end method

.method public handle(Z)V
    .locals 3
    .param p1, "start"    # Z

    .prologue
    .line 28
    if-nez p1, :cond_0

    .line 29
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getRemoteDeviceFiles(Z)V

    .line 41
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->TAG:Ljava/lang/String;

    const-string v2, "mProgress DISMISS - Exception"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public notifyChanged(I)V
    .locals 6
    .param p1, "msg"    # I

    .prologue
    const/4 v5, 0x1

    .line 44
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifyChanged : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    packed-switch p1, :pswitch_data_0

    .line 95
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 51
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_0

    .line 52
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 54
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v1

    .line 56
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->TAG:Ljava/lang/String;

    const-string v3, "mProgressHandler SHOW_PROGRESS - Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 62
    .end local v1    # "e":Ljava/lang/Exception;
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    if-nez v2, :cond_1

    .line 63
    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    .line 64
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00a1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 66
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 67
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 68
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    new-instance v3, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater$1;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;)V

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 74
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    new-instance v3, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater$2;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;)V

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 84
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    :try_start_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 87
    :catch_1
    move-exception v1

    .line 88
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->TAG:Ljava/lang/String;

    const-string v3, "mProgress SHOW - Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
