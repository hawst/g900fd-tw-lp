.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$8;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/common/CloudUtil$OnCloudDBCountChanged;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setCloudCountChangedListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 940
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$8;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCountChanged()V
    .locals 2

    .prologue
    const/16 v1, 0xc8

    .line 943
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$8;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$8;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 944
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$8;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 945
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$8;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 947
    :cond_0
    return-void
.end method
