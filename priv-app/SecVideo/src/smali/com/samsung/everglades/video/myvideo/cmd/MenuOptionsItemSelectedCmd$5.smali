.class Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$5;
.super Ljava/lang/Object;
.source "MenuOptionsItemSelectedCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->viewByMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V
    .locals 0

    .prologue
    .line 483
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$5;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 487
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$5;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # invokes: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->viewBy(I)V
    invoke-static {v2, p2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$400(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;I)V

    .line 488
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$5;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # getter for: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$100(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 489
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$5;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # getter for: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$500(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 490
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f06000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 491
    .local v0, "arraylist":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$5;->this$0:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    # getter for: Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->access$100(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/app/Activity;

    move-result-object v2

    const-string v3, "VPVA"

    const/4 v4, -0x1

    aget-object v5, v0, p2

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    .line 493
    .end local v0    # "arraylist":[Ljava/lang/String;
    .end local v1    # "res":Landroid/content/res/Resources;
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 494
    return-void
.end method
