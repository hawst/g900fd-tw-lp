.class Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$3;
.super Ljava/lang/Object;
.source "MovePrivateOperation.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;)V
    .locals 0

    .prologue
    .line 882
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$3;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v2, 0x0

    .line 886
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$3;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRename:Z
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$602(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    .line 887
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$3;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mReplace:Z
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$902(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    .line 888
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$3;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameCancellation:Z
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$702(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    .line 890
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$3;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$2200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Ljava/lang/Runnable;

    move-result-object v2

    monitor-enter v2

    .line 892
    :try_start_0
    const-string v1, "MovePrivateOperation - cancel notify()"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 893
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$3;->this$1:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$2200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 897
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 898
    if-eqz p1, :cond_0

    .line 899
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 901
    :cond_0
    return-void

    .line 894
    :catch_0
    move-exception v0

    .line 895
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 897
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
