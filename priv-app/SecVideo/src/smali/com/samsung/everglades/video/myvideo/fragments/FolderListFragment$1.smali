.class Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;
.super Ljava/lang/Object;
.source "FolderListFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

.field x:I


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 57
    const/4 v0, 0x0

    .line 59
    .local v0, "left":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isMultiWindow(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 60
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getleftpositionofMultiwindow(Landroid/app/Activity;)I

    move-result v0

    .line 63
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 103
    :cond_1
    :goto_0
    return v6

    .line 65
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    instance-of v3, v3, Lcom/samsung/everglades/video/VideoMain;

    if-eqz v3, :cond_2

    .line 67
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/samsung/everglades/video/VideoMain;

    invoke-virtual {v3, v5}, Lcom/samsung/everglades/video/VideoMain;->setViewPagerSwipeable(Z)V

    .line 69
    :cond_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 70
    .local v1, "param":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08003f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 71
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMarginImg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$100(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setPressed(Z)V

    goto :goto_0

    .line 76
    .end local v1    # "param":Landroid/widget/LinearLayout$LayoutParams;
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMarginImg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$100(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 77
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v3

    const-string v4, "splitwidth"

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->x:I

    invoke-virtual {v3, v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 79
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    instance-of v3, v3, Lcom/samsung/everglades/video/VideoMain;

    if-eqz v3, :cond_1

    .line 80
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/samsung/everglades/video/VideoMain;

    invoke-virtual {v3, v6}, Lcom/samsung/everglades/video/VideoMain;->setViewPagerSwipeable(Z)V

    goto/16 :goto_0

    .line 85
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v3, v0

    iput v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->x:I

    .line 87
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$200(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 88
    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->x:I

    sget v4, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_MIN_WIDTH_L:I

    if-ge v3, v4, :cond_4

    .line 89
    const/4 v3, 0x5

    iput v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->x:I

    .line 95
    :cond_3
    :goto_1
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->x:I

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 96
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v2, v5, v5, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 97
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderList:Landroid/widget/FrameLayout;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->access$300(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 90
    .end local v2    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->x:I

    sget v4, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_MAX_WIDTH_L:I

    if-le v3, v4, :cond_3

    .line 91
    sget v3, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_MAX_WIDTH_L:I

    iput v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;->x:I

    goto :goto_1

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
