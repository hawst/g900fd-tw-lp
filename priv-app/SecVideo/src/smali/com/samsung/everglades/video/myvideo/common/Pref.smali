.class public Lcom/samsung/everglades/video/myvideo/common/Pref;
.super Ljava/lang/Object;
.source "Pref.java"


# static fields
.field public static final ALLSHARE_SORT_ORDER:Ljava/lang/String; = "allsharesortorder"

.field private static final APPPREFS:Ljava/lang/String; = "SharedPreferences"

.field public static final AUTO_PLAY:Ljava/lang/String; = "autoPlay"

.field public static final IS_DROPBOX_FOLDER_CONTENTS:Ljava/lang/String; = "is_dropbox_folder_contents"

.field public static final IS_FIRST_TIME_DROPBOX:Ljava/lang/String; = "is_first_time_without_dropbox_account"

.field public static final LAST_FOLDER:Ljava/lang/String; = "lastFolder"

.field public static final LAST_PLAYED_ITEM:Ljava/lang/String; = "lastPlayedItem"

.field public static final LAST_TAB:Ljava/lang/String; = "lastTab"

.field private static final MODE:I = 0x0

.field public static final SAMSUNG_LINK_CHARGE_WARNING_POPUP_CONFIRMED:Ljava/lang/String; = "samsung_link_charge_warning_popup_confirmed"

.field public static final SHOW_WIFI_POPUP:Ljava/lang/String; = "showwifipopup"

.field public static final SLINK_SORT_ORDER:Ljava/lang/String; = "slinksortorder"

.field public static final SORT_ORDER:Ljava/lang/String; = "sortorder"

.field public static final SPLIT_WIDTH:Ljava/lang/String; = "splitwidth"

.field private static final TAG:Ljava/lang/String; = "Pref"

.field public static final VIEW_AS_DOWNLOAD_OPTION:Ljava/lang/String; = "lastDownloadViewAs"

.field public static final VIEW_AS_OPTION:Ljava/lang/String; = "lastViewAs"

.field public static final VIEW_CLOUD_OPTION:Ljava/lang/String; = "lastCloudViewOption"

.field public static final VIEW_DOWNLOAD_OPTION:Ljava/lang/String; = "lastDownloadViewOption"

.field private static mContext:Landroid/content/Context;

.field private static mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/Pref;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/Pref;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/Pref;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static destroyInstance()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/Pref;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/Pref;

    if-eqz v0, :cond_0

    .line 48
    sput-object v1, Lcom/samsung/everglades/video/myvideo/common/Pref;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/Pref;

    .line 51
    :cond_0
    sput-object v1, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    .line 52
    return-void
.end method

.method public static get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/Pref;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/Pref;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/Pref;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;-><init>()V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/Pref;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/Pref;

    .line 41
    :cond_0
    sput-object p0, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    .line 42
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/Pref;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/Pref;

    return-object v0
.end method


# virtual methods
.method public loadBoolean(Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # Z

    .prologue
    .line 131
    :try_start_0
    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    const-string v3, "SharedPreferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 132
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 136
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return p2

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Pref"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadInt(Ljava/lang/String;I)I
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # I

    .prologue
    .line 108
    const/4 v1, 0x0

    .line 110
    .local v1, "key":I
    :try_start_0
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    const-string v4, "SharedPreferences"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 111
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 116
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return v1

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    move v1, p2

    .line 114
    const-string v3, "Pref"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadLong(Ljava/lang/String;J)J
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # J

    .prologue
    .line 121
    :try_start_0
    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    const-string v3, "SharedPreferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 122
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 126
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-wide p2

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Pref"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;

    .prologue
    .line 97
    const/4 v1, 0x0

    .line 99
    .local v1, "key":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    const-string v4, "SharedPreferences"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 100
    .local v2, "pref":Landroid/content/SharedPreferences;
    const/4 v3, 0x0

    invoke-interface {v2, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 104
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-object v1

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Pref"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public saveState(Ljava/lang/String;I)V
    .locals 7
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # I

    .prologue
    .line 56
    :try_start_0
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    const-string v5, "SharedPreferences"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 57
    .local v3, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 58
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 59
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 60
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 66
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "Pref"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 63
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 64
    .local v2, "oome":Ljava/lang/OutOfMemoryError;
    const-string v4, "Pref"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "saveState"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public saveState(Ljava/lang/String;J)V
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # J

    .prologue
    .line 81
    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    const-string v3, "SharedPreferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 82
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 83
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 84
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 85
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 86
    return-void
.end method

.method public saveState(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 70
    :try_start_0
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    const-string v4, "SharedPreferences"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 71
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 72
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 73
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 74
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Pref"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public saveState(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # Z

    .prologue
    .line 89
    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/Pref;->mContext:Landroid/content/Context;

    const-string v3, "SharedPreferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 90
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 91
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 92
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 93
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 94
    return-void
.end method
