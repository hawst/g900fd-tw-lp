.class Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$6;
.super Ljava/lang/Object;
.source "DeviceListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->setNoVideoSLink()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V
    .locals 0

    .prologue
    .line 713
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$6;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 716
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$6;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getHowToUseViewIntent()Landroid/content/Intent;

    move-result-object v1

    .line 717
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 719
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$6;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 728
    :goto_0
    return-void

    .line 720
    :catch_0
    move-exception v0

    .line 721
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Activity Not found!!!"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 722
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 723
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$6;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 726
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    const-string v2, "DevicesFragment : fail to how to use intent"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method
