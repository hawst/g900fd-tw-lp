.class Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;
.super Ljava/lang/Object;
.source "DetailsDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->access$002(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Landroid/net/Uri;)Landroid/net/Uri;

    .line 83
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->access$102(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Landroid/content/Context;)Landroid/content/Context;

    .line 84
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->access$202(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Lcom/samsung/everglades/video/myvideo/common/DB;)Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 85
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->access$300(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 86
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->access$302(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 87
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->access$402(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 88
    return-void
.end method
