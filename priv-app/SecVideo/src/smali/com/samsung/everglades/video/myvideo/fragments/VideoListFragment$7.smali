.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$7;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/animator/TwGridSortAnimator$OnSortListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setupAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 912
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$7;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSort()V
    .locals 5

    .prologue
    .line 915
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.videoplayer.SORT_BY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 916
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$7;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 920
    :goto_0
    return-void

    .line 917
    :cond_0
    const-string v1, "sortby"

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$7;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "sortorder"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 918
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$7;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 919
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->callOptionChanged()V

    goto :goto_0
.end method
