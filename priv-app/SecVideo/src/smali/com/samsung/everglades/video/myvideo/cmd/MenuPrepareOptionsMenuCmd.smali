.class public Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;
.super Ljava/lang/Object;
.source "MenuPrepareOptionsMenuCmd.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/cmd/ICommand;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mMenu:Landroid/view/Menu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private checkVideoEditor4Knox()V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateEditMenu(Z)V

    .line 273
    :cond_0
    return-void
.end method

.method private hideNormalCountDependentMenus()V
    .locals 3

    .prologue
    .line 140
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getLocalVideoCount()I

    move-result v0

    .line 141
    .local v0, "localCounts":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MenuPrepareOptionsMenuCmd - hideNormalCountDependentMenus() localCounts :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 143
    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 144
    const v1, 0x7f090075

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 146
    :cond_0
    return-void
.end method

.method private multipleSelectedCase(IIII)V
    .locals 6
    .param p1, "cloudItemCount"    # I
    .param p2, "privateItemCount"    # I
    .param p3, "drmItemCount"    # I
    .param p4, "selectedItemCount"    # I

    .prologue
    const v5, 0x7f090081

    const v4, 0x7f090080

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 203
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v1, 0x7f09007a

    invoke-interface {v0, v1, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 204
    const v0, 0x7f090086

    invoke-direct {p0, v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 206
    if-lez p1, :cond_2

    .line 207
    if-ne p1, p4, :cond_0

    .line 208
    const v0, 0x7f09007c

    invoke-direct {p0, v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 210
    :cond_0
    invoke-direct {p0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateEditMenu(Z)V

    .line 241
    :cond_1
    :goto_0
    return-void

    .line 212
    :cond_2
    const v0, 0x7f090071

    invoke-direct {p0, v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 213
    if-lez p3, :cond_3

    .line 214
    invoke-direct {p0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateEditMenu(Z)V

    goto :goto_0

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-nez v0, :cond_4

    .line 217
    const v0, 0x7f09007b

    invoke-direct {p0, v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 218
    invoke-direct {p0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateSendtoAnotherDeviceMenu(Z)V

    .line 220
    :cond_4
    invoke-direct {p0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateEditMenu(Z)V

    .line 222
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPrivateMounted(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 223
    if-lez p2, :cond_6

    .line 224
    if-ne p2, p4, :cond_5

    .line 225
    invoke-direct {p0, v4, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 226
    invoke-direct {p0, v5, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 228
    :cond_5
    invoke-direct {p0, v4, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 229
    invoke-direct {p0, v5, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 232
    :cond_6
    invoke-direct {p0, v4, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 233
    invoke-direct {p0, v5, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 235
    :cond_7
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPrivateReady(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    invoke-direct {p0, v4, v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 237
    invoke-direct {p0, v5, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0
.end method

.method private setVisibility(IZ)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 276
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    invoke-interface {v1, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 277
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 278
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 280
    :cond_0
    return-void
.end method

.method private setVisibilityAndEnabled(IZ)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "value"    # Z

    .prologue
    .line 283
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    invoke-interface {v1, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 284
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 285
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 287
    :cond_0
    return-void
.end method

.method private singleSelectedCase(III)V
    .locals 5
    .param p1, "cloudItemCount"    # I
    .param p2, "privateItemCount"    # I
    .param p3, "drmItemCount"    # I

    .prologue
    const v4, 0x7f090080

    const/4 v3, 0x1

    .line 171
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v1, 0x7f09007a

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 172
    if-lez p1, :cond_1

    .line 173
    const v0, 0x7f09007c

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 199
    :cond_0
    :goto_0
    const v0, 0x7f090086

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 200
    return-void

    .line 175
    :cond_1
    const v0, 0x7f090071

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 176
    if-gtz p3, :cond_3

    .line 177
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-nez v0, :cond_2

    .line 178
    const v0, 0x7f09007b

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 179
    invoke-direct {p0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateSendtoAnotherDeviceMenu(Z)V

    .line 181
    :cond_2
    invoke-direct {p0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateEditMenu(Z)V

    .line 184
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-nez v0, :cond_4

    .line 185
    const v0, 0x7f090085

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 188
    :cond_4
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPrivateMounted(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 189
    if-lez p2, :cond_5

    .line 190
    const v0, 0x7f090081

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 192
    :cond_5
    invoke-direct {p0, v4, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 194
    :cond_6
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPrivateReady(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    invoke-direct {p0, v4, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0
.end method

.method private updateEditMenu(Z)V
    .locals 10
    .param p1, "visible"    # Z

    .prologue
    const v9, 0x7f09007f

    const v8, 0x7f09007d

    const/4 v7, 0x1

    const v6, 0x7f09007e

    const/4 v5, 0x0

    .line 290
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateEditMenu() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/everglades/video/myvideo/common/Utils;->EDIT_MENU_STATE:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 291
    sget v3, Lcom/samsung/everglades/video/myvideo/common/Utils;->EDIT_MENU_STATE:I

    packed-switch v3, :pswitch_data_0

    .line 321
    invoke-direct {p0, v9, v5}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 322
    invoke-direct {p0, v8, v5}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 323
    invoke-direct {p0, v6, v5}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 326
    :goto_0
    return-void

    .line 293
    :pswitch_0
    invoke-direct {p0, v8, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 296
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    const-string v4, "com.sec.android.app.storycam"

    invoke-static {v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPackageAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 297
    .local v0, "isAvailableVideoClip":Z
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    .line 298
    .local v1, "ms":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v2

    .line 299
    .local v2, "selectedItemCount":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 300
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getNumberOfItemFromFolers()I

    move-result v2

    .line 302
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateEditMenu() : isAvailableVideoClip = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " selectedItemCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 303
    if-eqz v0, :cond_2

    .line 304
    const/16 v3, 0xf

    if-gt v2, v3, :cond_1

    if-eqz p1, :cond_1

    .line 305
    invoke-direct {p0, v6, v7}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 307
    :cond_1
    invoke-direct {p0, v6, v5}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 310
    :cond_2
    if-ne v2, v7, :cond_3

    .line 311
    invoke-direct {p0, v6, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 313
    :cond_3
    invoke-direct {p0, v6, v5}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 318
    .end local v0    # "isAvailableVideoClip":Z
    .end local v1    # "ms":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    .end local v2    # "selectedItemCount":I
    :pswitch_2
    invoke-direct {p0, v9, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_0

    .line 291
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateKnox4SelectionMode(I)V
    .locals 5
    .param p1, "cloudItemCount"    # I

    .prologue
    const v4, 0x7f090083

    const/4 v3, 0x1

    const v2, 0x7f090082

    const/4 v1, 0x0

    .line 244
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnoxFileRelayAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    if-lez p1, :cond_1

    .line 246
    invoke-direct {p0, v2, v1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibilityAndEnabled(IZ)V

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnox2(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 249
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isSupportMoveTo(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 250
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    invoke-direct {p0, v2, v1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibilityAndEnabled(IZ)V

    .line 252
    invoke-direct {p0, v4, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibilityAndEnabled(IZ)V

    goto :goto_0

    .line 254
    :cond_2
    invoke-direct {p0, v2, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibilityAndEnabled(IZ)V

    .line 255
    invoke-direct {p0, v4, v1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibilityAndEnabled(IZ)V

    goto :goto_0

    .line 258
    :cond_3
    invoke-direct {p0, v2, v1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibilityAndEnabled(IZ)V

    .line 259
    invoke-direct {p0, v4, v1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibilityAndEnabled(IZ)V

    goto :goto_0

    .line 262
    :cond_4
    invoke-direct {p0, v2, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibilityAndEnabled(IZ)V

    goto :goto_0
.end method

.method private updateNewSelectionListMenus()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 149
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    .line 150
    .local v2, "mMultiSelector":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v4

    .line 152
    .local v4, "selectedItemCount":I
    if-lez v4, :cond_2

    .line 153
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getCloudSelectedItemCount()I

    move-result v0

    .line 154
    .local v0, "cloudItemCount":I
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getPrivateItemCount()I

    move-result v3

    .line 155
    .local v3, "privateItemCount":I
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getDrmItemCount()I

    move-result v1

    .line 157
    .local v1, "drmItemCount":I
    if-le v4, v6, :cond_1

    .line 158
    invoke-direct {p0, v0, v3, v1, v4}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->multipleSelectedCase(IIII)V

    .line 163
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateKnox4SelectionMode(I)V

    .line 164
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->checkVideoEditor4Knox()V

    .line 168
    .end local v0    # "cloudItemCount":I
    .end local v1    # "drmItemCount":I
    .end local v3    # "privateItemCount":I
    :goto_1
    return-void

    .line 159
    .restart local v0    # "cloudItemCount":I
    .restart local v1    # "drmItemCount":I
    .restart local v3    # "privateItemCount":I
    :cond_1
    if-ne v4, v6, :cond_0

    .line 160
    invoke-direct {p0, v0, v3, v1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->singleSelectedCase(III)V

    goto :goto_0

    .line 166
    .end local v0    # "cloudItemCount":I
    .end local v1    # "drmItemCount":I
    .end local v3    # "privateItemCount":I
    :cond_2
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v6, 0x7f09007a

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_1
.end method

.method private updatePersonalMenus()V
    .locals 5

    .prologue
    const v4, 0x7f090078

    const/4 v3, 0x0

    .line 94
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v1, 0x7f090074

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 96
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateVZCloud()V

    .line 98
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxCloudVideoAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isTcloudVideoAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 99
    :cond_0
    invoke-direct {p0, v4, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 102
    :cond_1
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isContentInDropbox()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isContentInTcloud()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDropboxFolderAttr()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "is_dropbox_folder_contents"

    invoke-virtual {v0, v1, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolderContent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104
    :cond_2
    const v0, 0x7f090075

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 107
    :cond_3
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->hideNormalCountDependentMenus()V

    .line 109
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 110
    const v0, 0x7f090070

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 113
    :cond_4
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolderContent()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 114
    invoke-direct {p0, v4, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 115
    const v0, 0x7f090077

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 118
    :cond_5
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSplitFolderContent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 119
    invoke-direct {p0, v4, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 122
    :cond_6
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 123
    const v0, 0x7f090079

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 125
    :cond_7
    return-void
.end method

.method private updatePersonalMenusForCloud()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 128
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v1, 0x7f090074

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 129
    const v0, 0x7f09006e

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 130
    const v0, 0x7f090078

    invoke-direct {p0, v0, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 131
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateVZCloud()V

    .line 132
    return-void
.end method

.method private updateSendtoAnotherDeviceMenu(Z)V
    .locals 9
    .param p1, "visible"    # Z

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f090084

    .line 329
    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateSendtoAnotherDeviceMenu() visible = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 330
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v3

    .line 331
    .local v3, "slink":Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    const/4 v1, 0x0

    .line 333
    .local v1, "isDeviceConnected":Z
    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSLinkProviderAvailable()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSignedIn()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 334
    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getDevicesCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 335
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_4

    .line 336
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 337
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_0

    .line 338
    invoke-virtual {v3, v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getNetworkModeByCursor(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 339
    .local v2, "networkMode":Ljava/lang/String;
    const-string v4, "OFF"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 340
    const/4 v1, 0x1

    .line 345
    .end local v2    # "networkMode":Ljava/lang/String;
    :cond_0
    if-eqz v1, :cond_3

    .line 346
    invoke-direct {p0, v7, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    .line 354
    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_1

    .line 355
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 361
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_2
    return-void

    .line 343
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v2    # "networkMode":Ljava/lang/String;
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 348
    .end local v2    # "networkMode":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, v7, v8}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_1

    .line 351
    :cond_4
    invoke-direct {p0, v7, v8}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_1

    .line 358
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_5
    invoke-direct {p0, v7, v8}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->setVisibility(IZ)V

    goto :goto_2
.end method

.method private updateVZCloud()V
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v2, 0x7f090076

    invoke-static {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/VZCloudUtils;->prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V

    .line 137
    return-void
.end method


# virtual methods
.method public execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 6
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 35
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    .line 36
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getMenu()Landroid/view/Menu;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    .line 37
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 38
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getCount()I

    move-result v1

    .line 39
    .local v1, "counts":I
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 40
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListAttribute()I

    move-result v0

    .line 42
    .local v0, "attribute":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MenuPrepareOptionsMenuCmd listType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 43
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MenuPrepareOptionsMenuCmd attribute: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 44
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MenuPrepareOptionsMenuCmd counts: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 46
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 89
    :cond_0
    :goto_0
    :pswitch_0
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 90
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->forceStop()V

    .line 91
    return-void

    .line 50
    :pswitch_1
    if-ne v0, v2, :cond_1

    .line 51
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateNewSelectionListMenus()V

    goto :goto_0

    .line 52
    :cond_1
    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 53
    if-gtz v1, :cond_3

    .line 54
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v4, "lastCloudViewOption"

    invoke-virtual {v2, v4, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_2

    .line 55
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v4, 0x7f090074

    invoke-interface {v2, v4, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 56
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updateVZCloud()V

    goto :goto_0

    .line 58
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updatePersonalMenusForCloud()V

    goto :goto_0

    .line 61
    :cond_3
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->updatePersonalMenus()V

    goto :goto_0

    .line 67
    :pswitch_2
    if-nez v0, :cond_0

    .line 68
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v5, 0x7f090089

    if-lez v1, :cond_4

    :goto_1
    invoke-interface {v4, v5, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_1

    .line 73
    :pswitch_3
    if-nez v0, :cond_0

    .line 74
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v5, 0x7f09006d

    if-lez v1, :cond_5

    :goto_2
    invoke-interface {v4, v5, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_0

    :cond_5
    move v2, v3

    goto :goto_2

    .line 79
    :pswitch_4
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 80
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mMenu:Landroid/view/Menu;

    const v3, 0x7f090072

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSignedIn()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
