.class Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$1;
.super Ljava/lang/Object;
.source "RenameDialog.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$1;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 12
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "spanned"    # Landroid/text/Spanned;
    .param p5, "dstStart"    # I
    .param p6, "dstEnd"    # I

    .prologue
    .line 89
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/EmojiList;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 90
    iget-object v9, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$1;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v10, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$1;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$000(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c004c

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showInvalidToast(Ljava/lang/String;I)V
    invoke-static {v9, v10, v11}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$100(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;I)V

    .line 91
    const-string v7, ""

    .line 132
    :goto_0
    return-object v7

    .line 93
    :cond_0
    sub-int v9, p3, p2

    const/16 v10, 0x64

    if-le v9, v10, :cond_1

    add-int/lit8 p3, p2, 0x64

    .line 94
    :cond_1
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 95
    .local v6, "origTxt":Ljava/lang/String;
    move-object v7, v6

    .line 96
    .local v7, "validTxt":Ljava/lang/String;
    const/4 v4, 0x0

    .line 98
    .local v4, "invalidFlag":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v9, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->INVALID_CHAR:[Ljava/lang/String;

    array-length v9, v9

    if-ge v1, v9, :cond_5

    .line 99
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    .line 100
    .local v8, "validTxtLength":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    if-ge v5, v8, :cond_4

    .line 101
    sget-object v9, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v9, v9, v1

    invoke-virtual {v7, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 102
    .local v3, "index":I
    if-ltz v3, :cond_3

    .line 103
    const/4 v4, 0x1

    .line 104
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v3, v9, :cond_2

    .line 106
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v10, 0x0

    invoke-virtual {v7, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v3, 0x1

    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 100
    :cond_2
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 112
    :cond_3
    sget-object v9, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v9, v9, v1

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 113
    .local v0, "c":C
    const/16 v9, 0x21

    if-lt v0, v9, :cond_2

    const/16 v9, 0x7e

    if-ge v0, v9, :cond_2

    const/16 v9, 0x3f

    if-eq v0, v9, :cond_2

    .line 114
    const v9, 0xfee0

    add-int/2addr v9, v0

    int-to-char v0, v9

    .line 115
    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 116
    .local v2, "iDBC":I
    if-ltz v2, :cond_2

    .line 117
    const/4 v4, 0x1

    .line 118
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v2, v9, :cond_2

    .line 120
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v10, 0x0

    invoke-virtual {v7, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v2, 0x1

    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 98
    .end local v0    # "c":C
    .end local v2    # "iDBC":I
    .end local v3    # "index":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 128
    .end local v5    # "j":I
    .end local v8    # "validTxtLength":I
    :cond_5
    if-eqz v4, :cond_6

    .line 129
    iget-object v9, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$1;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v10, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$1;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$000(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c004c

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    # invokes: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showInvalidToast(Ljava/lang/String;I)V
    invoke-static {v9, v10, v11}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$100(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 132
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_0
.end method
