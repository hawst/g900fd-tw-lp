.class Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;
.super Landroid/os/AsyncTask;
.source "FolderHoverViewer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EnsureLocalBitmaps"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field static final MAX_SIZE:I = 0x32


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;


# direct methods
.method private constructor <init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V
    .locals 0

    .prologue
    .line 465
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;
    .param p2, "x1"    # Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$1;

    .prologue
    .line 465
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;-><init>(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)V

    return-void
.end method

.method private preCallBitmap()V
    .locals 7

    .prologue
    const/16 v5, 0x32

    .line 474
    const/4 v4, 0x0

    .line 475
    .local v4, "thumbFilePath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 477
    .local v1, "cursor":Landroid/database/Cursor;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 478
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 479
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->getCount()I

    move-result v0

    .line 480
    .local v0, "count":I
    if-le v0, v5, :cond_0

    move v0, v5

    .line 481
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_3

    .line 483
    :try_start_0
    const-string v5, "_data"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 488
    :goto_1
    if-eqz v4, :cond_2

    .line 489
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isDropboxCloudCursor()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mPreviewCursor:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$FolderPreviewCursor;->isSKTCloudCursor()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 490
    :cond_1
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->this$0:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->access$2100(Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->requestThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 497
    :cond_2
    :goto_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-nez v5, :cond_5

    .line 504
    :cond_3
    return-void

    .line 484
    :catch_0
    move-exception v2

    .line 485
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 492
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    goto :goto_2

    .line 500
    :catch_1
    move-exception v2

    .line 501
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 481
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 465
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 469
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer$EnsureLocalBitmaps;->preCallBitmap()V

    .line 470
    const/4 v0, 0x0

    return-object v0
.end method
