.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$3;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->playVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "player"    # Landroid/media/MediaPlayer;

    .prologue
    .line 447
    const-string v1, "ThumbHoverViewer - onPrepared."

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 449
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1500(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/AudioUtil;->isRingerModeNormal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 450
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->requestAudioFocus()Z
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1600(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 451
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 464
    :goto_0
    return-void

    .line 456
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v2

    long-to-int v0, v2

    .line 458
    .local v0, "currentTime":I
    if-eqz v0, :cond_1

    .line 459
    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0

    .line 461
    :cond_1
    const-string v1, "start player"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 462
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0
.end method
