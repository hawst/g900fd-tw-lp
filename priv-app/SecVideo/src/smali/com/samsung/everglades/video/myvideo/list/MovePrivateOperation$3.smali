.class Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;
.super Landroid/os/Handler;
.source "MovePrivateOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 536
    monitor-enter p0

    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 599
    :goto_0
    monitor-exit p0

    return-void

    .line 538
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$002(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    .line 539
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget v2, p1, Landroid/os/Message;->arg2:I

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mTotalCnt:I
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1102(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)I

    .line 541
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressDialog:Landroid/app/Dialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/app/Dialog;

    move-result-object v1

    if-nez v1, :cond_0

    .line 542
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->createProgressDialog()Landroid/app/Dialog;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1300(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/app/Dialog;

    move-result-object v2

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressDialog:Landroid/app/Dialog;
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1202(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 544
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x0

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->updateCount(I)V
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1400(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)V

    .line 545
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x0

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->updateProgressPercentage(I)V
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1500(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)V

    .line 546
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressDialog:Landroid/app/Dialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 536
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 551
    :pswitch_1
    :try_start_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget v2, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->updateProgressPercentage(I)V
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1500(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)V

    goto :goto_0

    .line 555
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget v2, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->updateCount(I)V
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1400(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)V

    goto :goto_0

    .line 559
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 560
    .local v0, "alertMsg":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 561
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showToast(Ljava/lang/CharSequence;I)V

    .line 563
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 564
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 566
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$002(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    goto :goto_0

    .line 570
    .end local v0    # "alertMsg":Ljava/lang/String;
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->closeProgressDialog()V
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1600(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    .line 571
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 572
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 574
    :cond_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$002(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    goto/16 :goto_0

    .line 578
    :pswitch_5
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mSuccessMoveCnt:I
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$800(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)I

    move-result v2

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showFinishToast(I)V
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1700(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)V

    .line 579
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->closeProgressDialog()V
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1600(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    .line 580
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 581
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$100(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 583
    :cond_4
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$002(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    goto/16 :goto_0

    .line 587
    :pswitch_6
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->setKeepScreenOn()V
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1800(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    goto/16 :goto_0

    .line 591
    :pswitch_7
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->clearKeepScreenOn()V
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1900(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    goto/16 :goto_0

    .line 595
    :pswitch_8
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const v2, 0x7f0c0077

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showToast(II)V

    .line 596
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$002(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 536
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_8
        :pswitch_3
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
