.class Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;
.super Ljava/lang/Object;
.source "ASFUtil.java"

# interfaces
.implements Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 3

    .prologue
    .line 442
    const/4 v0, 0x1

    const-string v1, "mFlatBrowseResponseListener onCancel()"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->notifySettingChanged(II)V

    .line 444
    return-void
.end method

.method public onError(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 478
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mFlatBrowseResponseListener onError() error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->notifySettingChanged(II)V

    .line 480
    return-void
.end method

.method public onFinish()V
    .locals 3

    .prologue
    .line 448
    const/4 v0, 0x1

    const-string v1, "mFlatBrowseResponseListener onFinish()"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->notifySettingChanged(II)V

    .line 450
    return-void
.end method

.method public onProgress(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 454
    .local p1, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mFlatBrowseResponseListener onProgress() size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 456
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$700(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/extension/UniqueItemArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->addAll(Ljava/util/Collection;)Z

    .line 458
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # invokes: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->updateRemoteFilesDB()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$800(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->notifySettingChanged(II)V

    .line 461
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 465
    const/4 v0, 0x1

    const-string v1, "mFlatBrowseResponseListener. onStart()"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 467
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$700(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/extension/UniqueItemArray;

    move-result-object v0

    if-nez v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    new-instance v1, Lcom/samsung/android/allshare/extension/UniqueItemArray;

    invoke-direct {v1}, Lcom/samsung/android/allshare/extension/UniqueItemArray;-><init>()V

    # setter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$702(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Lcom/samsung/android/allshare/extension/UniqueItemArray;)Lcom/samsung/android/allshare/extension/UniqueItemArray;

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$700(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/extension/UniqueItemArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->clear()V

    .line 472
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # setter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mAllShareDBIndex:I
    invoke-static {v0, v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$902(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;I)I

    .line 473
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$3;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->notifySettingChanged(II)V

    .line 474
    return-void
.end method
