.class public Lcom/samsung/everglades/video/myvideo/videowall/ListView;
.super Landroid/widget/ImageView;
.source "ListView.java"


# instance fields
.field private count:J

.field private delayTime:J

.field private fps:I

.field private ht:I

.field private interval:I

.field public mBitmap:Landroid/graphics/Bitmap;

.field private movieindex:I

.field private nextFrame:Z

.field private nowTime:J

.field public prebitmap:Landroid/graphics/Bitmap;

.field private pretime:J

.field private ptX:I

.field private ptY:I

.field private res:I

.field private setVideo:Z

.field private vcount:J

.field private wd:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 10
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    .line 12
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->prebitmap:Landroid/graphics/Bitmap;

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->nextFrame:Z

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setVideo:Z

    .line 18
    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->count:J

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->vcount:J

    .line 20
    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->pretime:J

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->nowTime:J

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->delayTime:J

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->movieindex:I

    .line 32
    return-void
.end method


# virtual methods
.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 38
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const-wide/16 v8, 0x1

    const-wide/16 v6, 0x0

    .line 69
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 71
    iget-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->count:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    .line 73
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->pretime:J

    .line 74
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->wd:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->ptX:I

    .line 75
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->ht:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->ptY:I

    .line 78
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setVideo:Z

    if-eqz v2, :cond_6

    .line 79
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->nextFrame:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->isVideoWallOn()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 80
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->prebitmap:Landroid/graphics/Bitmap;

    .line 82
    :cond_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->movieindex:I

    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_render(Landroid/graphics/Bitmap;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->res:I

    .line 83
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->res:I

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->movieindex:I

    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_render(Landroid/graphics/Bitmap;I)I

    .line 84
    :cond_2
    iget-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->vcount:J

    add-long/2addr v2, v8

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->vcount:J

    .line 87
    :cond_3
    iget-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->delayTime:J

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->pretime:J

    .line 89
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->nowTime:J

    .line 90
    iget-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->nowTime:J

    iget-wide v4, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->pretime:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->delayTime:J

    .line 92
    iget-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->vcount:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_9

    .line 93
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 94
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->ptX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->ptY:I

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 96
    :cond_5
    iget-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->delayTime:J

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->interval:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_8

    .line 97
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->nextFrame:Z

    .line 108
    :cond_6
    :goto_0
    iget-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->count:J

    add-long/2addr v2, v8

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->count:J

    .line 110
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->isVideoWallOn()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 111
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->invalidate()V

    .line 113
    :cond_7
    return-void

    .line 99
    :cond_8
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->nextFrame:Z

    .line 100
    iget-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->delayTime:J

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->interval:I

    int-to-long v4, v4

    sub-long v0, v2, v4

    .line 101
    .local v0, "tmp":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->pretime:J

    goto :goto_0

    .line 104
    .end local v0    # "tmp":J
    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->pretime:J

    goto :goto_0
.end method

.method public pauseView()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setVideo:Z

    .line 66
    return-void
.end method

.method public setBitmapView(I)V
    .locals 4
    .param p1, "idx"    # I

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x1

    .line 48
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->movieindex:I

    .line 51
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setVideo:Z

    .line 52
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->nextFrame:Z

    .line 54
    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->vcount:J

    .line 55
    iput-wide v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->count:J

    .line 57
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->wd:I

    iget v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->ht:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->mBitmap:Landroid/graphics/Bitmap;

    .line 59
    :cond_0
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->isVideoWallOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->invalidate()V

    .line 62
    :cond_1
    return-void
.end method

.method public setView(III)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "framerate"    # I

    .prologue
    .line 41
    iput p3, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->fps:I

    .line 42
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->wd:I

    .line 43
    iput p2, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->ht:I

    .line 44
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->fps:I

    if-lez v0, :cond_0

    const/16 v0, 0x3e8

    iget v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->fps:I

    div-int/2addr v0, v1

    :goto_0
    iput v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->interval:I

    .line 45
    return-void

    .line 44
    :cond_0
    const/16 v0, 0xc8

    goto :goto_0
.end method
