.class Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;
.super Ljava/lang/Object;
.source "SLinkDevicesListAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 176
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$300(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$300(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;

    iget-object v2, v2, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->val$deviceItem:Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getDeviceID()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->deregisterDevice(I)Z

    .line 181
    :cond_0
    const-wide/16 v2, 0x5dc

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :goto_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$300(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 187
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$300(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->requestRefresh()V

    .line 189
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$200(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 190
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
