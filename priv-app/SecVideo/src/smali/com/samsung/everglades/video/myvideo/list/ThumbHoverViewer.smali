.class public Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;
    }
.end annotation


# static fields
.field public static final DETECT_DELAY:I = 0x190

.field private static final HOVER_DETECT_MS:I = 0xa

.field private static final HOVER_DETECT_TIME_MS:I = 0x12c

.field private static final HOVER_SUSPEND_DELAY:I = 0x3e8

.field public static final MSG_DISMISS_POPUP:I = 0x2

.field public static final MSG_ERROR_POPUP:I = 0x3

.field public static final MSG_SHOW:I = 0x1

.field public static final MSG_SHOW_POPUP:I = 0x0

.field public static final MSG_SHOW_POPUP_DISABLE:I = 0x4

.field public static final MSG_SHOW_POPUP_ENABLE:I = 0x5

.field private static mHasAudioFocus:Z

.field private static mIsAirButtonShowing:Z

.field private static mIsSPenHovering:Z

.field private static mThumbHoverPopupSuspend:Z

.field private static mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# instance fields
.field private imageViewB:Landroid/widget/ImageView;

.field private imageViewB2:Landroid/widget/ImageView;

.field private imageViewL:Landroid/widget/ImageView;

.field private imageViewL2:Landroid/widget/ImageView;

.field private imageViewR:Landroid/widget/ImageView;

.field private imageViewR2:Landroid/widget/ImageView;

.field private imageViewT:Landroid/widget/ImageView;

.field private imageViewT2:Landroid/widget/ImageView;

.field private mActionHoverEnter:Z

.field private mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

.field private mBaidu:Z

.field private mButtonHoverListener:Landroid/view/View$OnHoverListener;

.field private mButtonLayoutHeight:I

.field private mButtonTouchListener:Landroid/view/View$OnTouchListener;

.field private mCloud:Z

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mDelete:Landroid/widget/ImageView;

.field private mDetails:Landroid/widget/ImageView;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mDrm:Z

.field private mDropbox:Z

.field private mFolder:Z

.field private mHandler:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;

.field private mHoverRect:Landroid/graphics/Rect;

.field private mHoverView:Landroid/view/View;

.field public mListHoverListener:Landroid/view/View$OnHoverListener;

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNormal:Z

.field private mPopupButtonLayout:Landroid/widget/RelativeLayout;

.field private mPortHeightRate:F

.field private mPortWidthRate:F

.field private mPreviewDialog:Landroid/app/Dialog;

.field private mRename:Landroid/widget/ImageView;

.field private final mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSearch:Z

.field private mSharevia:Landroid/widget/ImageView;

.field private mSkt:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceView:Landroid/view/SurfaceView;

.field private mUri:Landroid/net/Uri;

.field private mVertHeightRate:F

.field private mVertWidthRate:F

.field private mVideoViewLayout:Landroid/widget/RelativeLayout;

.field private noVideoView:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .line 97
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsAirButtonShowing:Z

    .line 99
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHasAudioFocus:Z

    .line 101
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z

    .line 103
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mThumbHoverPopupSuspend:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    .line 62
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    .line 70
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    .line 72
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 88
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDrm:Z

    .line 89
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mNormal:Z

    .line 90
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mFolder:Z

    .line 91
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSkt:Z

    .line 92
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDropbox:Z

    .line 93
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mBaidu:Z

    .line 94
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mCloud:Z

    .line 95
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSearch:Z

    .line 105
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    .line 107
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 121
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mActionHoverEnter:Z

    .line 591
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 660
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonHoverListener:Landroid/view/View$OnHoverListener;

    .line 779
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$10;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$10;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 957
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$11;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 995
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListHoverListener:Landroid/view/View$OnHoverListener;

    .line 123
    return-void
.end method

.method private abandonAudioFocus()V
    .locals 2

    .prologue
    .line 804
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHasAudioFocus:Z

    if-eqz v0, :cond_0

    .line 805
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHasAudioFocus:Z

    .line 806
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/AudioUtil;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    .line 808
    :cond_0
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 53
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mThumbHoverPopupSuspend:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 53
    sput-boolean p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mThumbHoverPopupSuspend:Z

    return p0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->checkStopCondition()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/DB;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    return-object v0
.end method

.method static synthetic access$1200()Z
    .locals 1

    .prologue
    .line 53
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsAirButtonShowing:Z

    return v0
.end method

.method static synthetic access$1202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 53
    sput-boolean p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsAirButtonShowing:Z

    return p0
.end method

.method static synthetic access$1300()Z
    .locals 1

    .prologue
    .line 53
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z

    return v0
.end method

.method static synthetic access$1302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 53
    sput-boolean p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z

    return p0
.end method

.method static synthetic access$1400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/AudioUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->requestAudioFocus()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Landroid/media/MediaPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .param p1, "x1"    # Landroid/media/MediaPlayer;
    .param p2, "x2"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setAIAContext(Landroid/media/MediaPlayer;Z)V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->noVideoView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/view/SurfaceView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceView:Landroid/view/SurfaceView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->playVideo()V

    return-void
.end method

.method static synthetic access$2700(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .param p1, "x1"    # Landroid/media/MediaPlayer;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setAIAContext(Z)V

    return-void
.end method

.method static synthetic access$2900(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mActionHoverEnter:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mActionHoverEnter:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->initViews()V

    return-void
.end method

.method static synthetic access$3000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->sendDelayedMessage(I)V

    return-void
.end method

.method static synthetic access$3102(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->show()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->dismiss()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonLayoutHeight:I

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->abandonAudioFocus()V

    return-void
.end method

.method private checkStopCondition()Z
    .locals 14

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 1067
    iget-object v9, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    .line 1069
    .local v9, "v":Landroid/view/View;
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 1070
    .local v7, "titleEllipsized":Ljava/lang/Boolean;
    const v10, 0x7f090001

    invoke-virtual {v9, v10}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1071
    .local v4, "firstRowText":Landroid/widget/TextView;
    if-eqz v4, :cond_0

    .line 1072
    invoke-virtual {v4}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    .line 1073
    .local v3, "firstLayout":Landroid/text/Layout;
    if-eqz v3, :cond_0

    .line 1074
    invoke-virtual {v3}, Landroid/text/Layout;->getLineCount()I

    move-result v6

    .line 1075
    .local v6, "lines":I
    if-lez v6, :cond_0

    add-int/lit8 v10, v6, -0x1

    invoke-virtual {v3, v10}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v10

    if-lez v10, :cond_0

    .line 1076
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 1081
    .end local v3    # "firstLayout":Landroid/text/Layout;
    .end local v6    # "lines":I
    :cond_0
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-nez v10, :cond_1

    .line 1082
    iget-object v10, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v10, v9}, Lcom/samsung/everglades/video/myvideo/common/Utils;->setHapticEffect(Landroid/content/Context;Landroid/view/View;)V

    .line 1085
    :cond_1
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1086
    .local v5, "hoverRect":Landroid/graphics/Rect;
    iget-object v10, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    .line 1088
    .local v0, "cloudUtil":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    invoke-interface {v10}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1090
    const v10, 0x7f090003

    invoke-virtual {v9, v10}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    .line 1091
    .local v8, "uri":Landroid/net/Uri;
    invoke-direct {p0, v5, v8}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setParam(Landroid/graphics/Rect;Landroid/net/Uri;)V

    .line 1093
    invoke-virtual {v0, v8}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/net/Uri;)Z

    move-result v10

    if-eqz v10, :cond_2

    move v10, v11

    .line 1106
    :goto_0
    return v10

    .line 1097
    :cond_2
    iget-object v10, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v10, v8}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1099
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 1100
    iget-object v10, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v1

    .line 1101
    .local v1, "drmUtil":Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    const/4 v10, -0x1

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v13

    if-eq v10, v13, :cond_3

    move v10, v11

    .line 1102
    goto :goto_0

    .line 1105
    .end local v1    # "drmUtil":Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ThumbHoverViewer - checkStopCondition() - uri"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    move v10, v12

    .line 1106
    goto :goto_0
.end method

.method public static destroyInstance()V
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .line 136
    :cond_0
    return-void
.end method

.method private dismiss()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 869
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mActionHoverEnter:Z

    .line 870
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    if-eqz v1, :cond_0

    .line 871
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->dismiss()V

    .line 872
    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    .line 875
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->isShow()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 876
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->abandonAudioFocus()V

    .line 877
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    .line 878
    invoke-direct {p0, v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setAIAContext(Z)V

    .line 879
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 882
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 886
    :goto_0
    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    .line 889
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_3

    .line 890
    invoke-direct {p0, v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setAIAContext(Z)V

    .line 891
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 894
    :cond_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v1, :cond_4

    .line 895
    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 897
    :cond_4
    return-void

    .line 883
    :catch_0
    move-exception v0

    .line 884
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private extractData(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "dataType"    # I

    .prologue
    .line 900
    if-nez p1, :cond_0

    const/4 v1, 0x0

    .line 916
    :goto_0
    return-object v1

    .line 902
    :cond_0
    const/4 v1, 0x0

    .line 903
    .local v1, "extracted":Ljava/lang/String;
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 906
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v2, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 907
    invoke-virtual {v2, p2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 913
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 908
    :catch_0
    move-exception v0

    .line 909
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 913
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 910
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 911
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 913
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v3
.end method

.method private getCheckRoation()I
    .locals 9

    .prologue
    .line 211
    const-string v4, "0"

    .line 212
    .local v4, "rotation":Ljava/lang/String;
    const/4 v0, 0x0

    .line 213
    .local v0, "dimensionsString":Ljava/lang/String;
    const/4 v3, 0x0

    .line 214
    .local v3, "mVideoWidth":I
    const/4 v2, 0x0

    .line 216
    .local v2, "mVideoHeight":I
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-nez v7, :cond_0

    .line 217
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 220
    :cond_0
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-eqz v7, :cond_1

    .line 221
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Lcom/samsung/everglades/video/myvideo/common/DB;->getResolution(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 222
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x18

    invoke-direct {p0, v7, v8}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->extractData(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 225
    :cond_1
    if-eqz v0, :cond_2

    .line 226
    const-string v7, "x"

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 227
    .local v1, "index":I
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    .line 229
    .local v5, "totalLenght":I
    const/4 v7, 0x0

    invoke-virtual {v0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 230
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v0, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 233
    .end local v1    # "index":I
    .end local v5    # "totalLenght":I
    :cond_2
    if-eqz v4, :cond_3

    .end local v4    # "rotation":Ljava/lang/String;
    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 234
    .local v6, "videoRotation":I
    sparse-switch v6, :sswitch_data_0

    .line 242
    if-ge v3, v2, :cond_4

    .line 243
    const/16 v6, 0x5a

    .line 249
    :goto_1
    return v6

    .line 233
    .end local v6    # "videoRotation":I
    .restart local v4    # "rotation":Ljava/lang/String;
    :cond_3
    const-string v4, "0"

    goto :goto_0

    .line 237
    .end local v4    # "rotation":Ljava/lang/String;
    .restart local v6    # "videoRotation":I
    :sswitch_0
    const/16 v6, 0x5a

    .line 238
    goto :goto_1

    .line 245
    :cond_4
    const/4 v6, 0x0

    goto :goto_1

    .line 234
    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0x10e -> :sswitch_0
    .end sparse-switch
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHandler:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHandler:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHandler:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$HoverPopupHandler;

    return-object v0
.end method

.method public static getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;-><init>()V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    .line 129
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    return-object v0
.end method

.method private getThumbnailBitmap(Landroid/net/Uri;J)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "durationTime"    # J

    .prologue
    .line 564
    const/4 v4, 0x0

    .line 565
    .local v4, "thumb":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 567
    .local v3, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getCheckRoation()I

    move-result v2

    .line 568
    .local v2, "m_VideoRotation":I
    const/16 v6, 0x5a

    if-ne v2, v6, :cond_0

    .line 569
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortHeightRate:F

    mul-float/2addr v6, v7

    float-to-int v1, v6

    .line 570
    .local v1, "height":I
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortWidthRate:F

    mul-float/2addr v6, v7

    float-to-int v5, v6

    .line 576
    .local v5, "width":I
    :goto_0
    :try_start_0
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v6, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 577
    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v1, v6, v7}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 578
    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, p2

    invoke-virtual {v3, v6, v7}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 583
    :try_start_1
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 588
    :goto_1
    return-object v4

    .line 572
    .end local v1    # "height":I
    .end local v5    # "width":I
    :cond_0
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertHeightRate:F

    mul-float/2addr v6, v7

    float-to-int v1, v6

    .line 573
    .restart local v1    # "height":I
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertWidthRate:F

    mul-float/2addr v6, v7

    float-to-int v5, v6

    .restart local v5    # "width":I
    goto :goto_0

    .line 584
    :catch_0
    move-exception v0

    .line 585
    .local v0, "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 579
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 580
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 583
    :try_start_3
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 584
    :catch_2
    move-exception v0

    .line 585
    .local v0, "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 582
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    .line 583
    :try_start_4
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3

    .line 586
    :goto_2
    throw v6

    .line 584
    :catch_3
    move-exception v0

    .line 585
    .restart local v0    # "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private initBooleans()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 746
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 747
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isDrmContent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDrm:Z

    .line 748
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isTypical()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mNormal:Z

    .line 749
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mFolder:Z

    .line 750
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v1

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSkt:Z

    .line 751
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v1

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDropbox:Z

    .line 752
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v1

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/net/Uri;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mBaidu:Z

    .line 753
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSkt:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDropbox:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mBaidu:Z

    if-eqz v1, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mCloud:Z

    .line 754
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSearch:Z

    .line 755
    return-void

    :cond_2
    move v1, v3

    .line 747
    goto :goto_0
.end method

.method private initViews()V
    .locals 9

    .prologue
    const v8, 0x7f0a0004

    const v5, 0x7f0a0001

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    .line 253
    const-string v1, "ThumbHoverViewer - initViews() called"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 254
    new-instance v1, Landroid/app/Dialog;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    .line 255
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    .line 257
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    const v2, 0x7f040021

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 258
    .local v0, "rootView":Landroid/widget/RelativeLayout;
    const v1, 0x7f090063

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewT:Landroid/widget/ImageView;

    .line 259
    const v1, 0x7f090064

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewT2:Landroid/widget/ImageView;

    .line 260
    const v1, 0x7f090065

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewB:Landroid/widget/ImageView;

    .line 261
    const v1, 0x7f090066

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewB2:Landroid/widget/ImageView;

    .line 262
    const v1, 0x7f090067

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewL:Landroid/widget/ImageView;

    .line 263
    const v1, 0x7f090068

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewL2:Landroid/widget/ImageView;

    .line 264
    const v1, 0x7f090069

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewR:Landroid/widget/ImageView;

    .line 265
    const v1, 0x7f09006a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewR2:Landroid/widget/ImageView;

    .line 267
    const v1, 0x7f09005c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVideoViewLayout:Landroid/widget/RelativeLayout;

    .line 268
    const v1, 0x7f09005e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->noVideoView:Landroid/widget/ImageView;

    .line 270
    const v1, 0x7f09005d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceView:Landroid/view/SurfaceView;

    .line 271
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 272
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 274
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    :goto_0
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonLayoutHeight:I

    .line 276
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z

    if-eqz v1, :cond_0

    .line 277
    const v1, 0x7f09005f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPopupButtonLayout:Landroid/widget/RelativeLayout;

    .line 278
    const v1, 0x7f090060

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;

    .line 279
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setHoverPopupAttr(Landroid/widget/ImageView;)V

    .line 282
    const v1, 0x7f090061

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;

    .line 283
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c005b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setHoverPopupAttr(Landroid/widget/ImageView;)V

    .line 286
    const v1, 0x7f090062

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;

    .line 287
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c005a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setHoverPopupAttr(Landroid/widget/ImageView;)V

    .line 290
    const v1, 0x7f090042

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;

    .line 291
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c005e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setHoverPopupAttr(Landroid/widget/ImageView;)V

    .line 294
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setPopupButtonListener()V

    .line 295
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->showPopupButton()V

    .line 298
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setDialogProperties()V

    .line 299
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setDialogPosition()V

    .line 301
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 303
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 304
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortHeightRate:F

    .line 305
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortWidthRate:F

    .line 306
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertHeightRate:F

    .line 307
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertWidthRate:F

    .line 314
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initViews - mPortHeightRate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortHeightRate:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mPortWidthRate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortWidthRate:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 316
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setDialogWindow()V

    .line 319
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-nez v1, :cond_1

    .line 320
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->addAirButton(Landroid/view/View;)V

    .line 323
    :cond_1
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 354
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 412
    return-void

    .line 274
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 309
    :cond_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortHeightRate:F

    .line 310
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortWidthRate:F

    .line 311
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertHeightRate:F

    .line 312
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertWidthRate:F

    goto/16 :goto_1
.end method

.method private isShow()Z
    .locals 1

    .prologue
    .line 861
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 862
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    .line 864
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWfdConnected()Z
    .locals 2

    .prologue
    .line 1122
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1123
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1124
    const/4 v0, 0x1

    .line 1127
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private playVideo()V
    .locals 5

    .prologue
    .line 420
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v1, :cond_1

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    if-nez v1, :cond_2

    .line 425
    new-instance v1, Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/AudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    .line 428
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-nez v1, :cond_3

    .line 429
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 432
    :cond_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 433
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 438
    :cond_4
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_5

    .line 439
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setAIAContext(Z)V

    .line 440
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 441
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 444
    :cond_5
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 445
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$3;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$3;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 466
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$4;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$4;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 471
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$5;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$5;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 479
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$6;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$6;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 488
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$7;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$7;-><init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 515
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "content://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 516
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 521
    :goto_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->isWfdConnected()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 522
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x9c4

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 525
    :cond_6
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 527
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/AudioUtil;->isRingerModeNormal()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 528
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 533
    :goto_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 534
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setAIAContext(Z)V

    .line 535
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 536
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-nez v1, :cond_7

    .line 537
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->showBoarders()V

    .line 540
    :cond_7
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceView:Landroid/view/SurfaceView;

    if-eqz v1, :cond_0

    .line 542
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setSurfaceViewImage()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    .line 544
    :catch_0
    move-exception v0

    .line 545
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 518
    .end local v0    # "e":Ljava/io/IOException;
    :cond_8
    :try_start_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 546
    :catch_1
    move-exception v0

    .line 547
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 530
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_9
    :try_start_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 548
    :catch_2
    move-exception v0

    .line 549
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private requestAudioFocus()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 791
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHasAudioFocus:Z

    if-nez v2, :cond_1

    .line 792
    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHasAudioFocus:Z

    .line 793
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioUtil:Lcom/samsung/everglades/video/myvideo/common/AudioUtil;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/AudioUtil;->gainAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    .line 794
    .local v0, "ret":I
    if-ne v0, v1, :cond_0

    .line 800
    .end local v0    # "ret":I
    :goto_0
    return v1

    .line 797
    .restart local v0    # "ret":I
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHasAudioFocus:Z

    .line 800
    .end local v0    # "ret":I
    :cond_1
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHasAudioFocus:Z

    goto :goto_0
.end method

.method private sendDelayedMessage(I)V
    .locals 4
    .param p1, "ms"    # I

    .prologue
    const/4 v1, 0x0

    .line 920
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mThumbHoverPopupSuspend:Z

    if-nez v0, :cond_1

    .line 921
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 922
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 924
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 926
    :cond_1
    return-void
.end method

.method private setAIAContext(Landroid/media/MediaPlayer;Z)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "set"    # Z

    .prologue
    .line 1115
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/media/MediaPlayer;->setAIAContext(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1119
    :goto_0
    return-void

    .line 1116
    :catch_0
    move-exception v0

    .line 1117
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ThumbHoverViewer - setAIAContext didn\'t work - maybe this device doesn\'t support it"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setAIAContext(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0, v0, p1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->setAIAContext(Landroid/media/MediaPlayer;Z)V

    .line 1111
    return-void
.end method

.method private setDialogPosition()V
    .locals 3

    .prologue
    .line 843
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 844
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 845
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x800033

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 846
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 847
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 848
    return-void
.end method

.method private setDialogProperties()V
    .locals 3

    .prologue
    .line 837
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 838
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 839
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 840
    return-void
.end method

.method private setDialogWindow()V
    .locals 5

    .prologue
    .line 811
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 812
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 813
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getCheckRoation()I

    move-result v1

    .line 815
    .local v1, "m_VideoRotation":I
    const/16 v3, 0x5a

    if-ne v1, v3, :cond_0

    .line 816
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortHeightRate:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 817
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortWidthRate:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 823
    :goto_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 824
    .local v2, "window":Landroid/view/Window;
    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 825
    return-void

    .line 819
    .end local v2    # "window":Landroid/view/Window;
    :cond_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertHeightRate:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 820
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertWidthRate:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_0
.end method

.method private setHoverPopupAttr(Landroid/widget/ImageView;)V
    .locals 3
    .param p1, "item"    # Landroid/widget/ImageView;

    .prologue
    .line 1131
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 1132
    invoke-virtual {p1}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 1134
    .local v0, "hoverPopupWindow":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 1135
    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 1136
    const/16 v1, 0x5153

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 1137
    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 1139
    :cond_0
    return-void
.end method

.method private setParam(Landroid/graphics/Rect;Landroid/net/Uri;)V
    .locals 1
    .param p1, "hoverRect"    # Landroid/graphics/Rect;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 990
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverRect:Landroid/graphics/Rect;

    .line 991
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    .line 992
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 993
    return-void
.end method

.method private setPopupButtonListener()V
    .locals 2

    .prologue
    .line 735
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 736
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 737
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 738
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 739
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 740
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 741
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 742
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mButtonHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 743
    return-void
.end method

.method private setSurfaceViewImage()V
    .locals 6

    .prologue
    .line 554
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v4

    long-to-int v0, v4

    .line 556
    .local v0, "currentTime":I
    const/4 v2, 0x0

    .line 557
    .local v2, "imageDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    .line 558
    .local v1, "imageBitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;

    int-to-long v4, v0

    invoke-direct {p0, v3, v4, v5}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getThumbnailBitmap(Landroid/net/Uri;J)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 559
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "imageDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 560
    .restart local v2    # "imageDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3, v2}, Landroid/view/SurfaceView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 561
    return-void
.end method

.method private setVideoView()V
    .locals 6

    .prologue
    .line 828
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getCheckRoation()I

    move-result v0

    .line 829
    .local v0, "m_VideoRotation":I
    const/16 v1, 0x5a

    if-ne v0, v1, :cond_0

    .line 830
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVideoViewLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortWidthRate:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPortHeightRate:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 834
    :goto_0
    return-void

    .line 832
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVideoViewLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertHeightRate:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mVertWidthRate:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private show()V
    .locals 3

    .prologue
    .line 853
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 854
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 858
    :goto_0
    return-void

    .line 855
    :catch_0
    move-exception v0

    .line 856
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private showBoarders()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 979
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewT:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 980
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewT2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 981
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewB:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 982
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewB2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 983
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewL:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 984
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewL2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 985
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewR:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 986
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->imageViewR2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 987
    return-void
.end method

.method private showPopupButton()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 758
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->initBooleans()V

    .line 759
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mPopupButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 760
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mNormal:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mFolder:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSearch:Z

    if-eqz v0, :cond_1

    .line 761
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mCloud:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDrm:Z

    if-nez v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 765
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mNormal:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mFolder:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSearch:Z

    if-eqz v0, :cond_3

    .line 766
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mCloud:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDrm:Z

    if-nez v0, :cond_3

    .line 767
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 770
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mNormal:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDrm:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mFolder:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSearch:Z

    if-eqz v0, :cond_5

    .line 771
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mCloud:Z

    if-nez v0, :cond_5

    .line 772
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 776
    :cond_5
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 777
    return-void
.end method


# virtual methods
.method public dismissPopupMessage()V
    .locals 2

    .prologue
    .line 415
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 416
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 417
    return-void
.end method

.method public forceStop()V
    .locals 0

    .prologue
    .line 945
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->dismiss()V

    .line 946
    return-void
.end method

.method public removeDelayedMessage()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 929
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 930
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 933
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 934
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 937
    :cond_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 938
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 941
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 942
    return-void
.end method

.method public setContextAndListType(Landroid/content/Context;Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 140
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->forceStop()V

    .line 141
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    .line 142
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 143
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 144
    return-void
.end method

.method public suspendHoverPopupHandler(I)V
    .locals 4
    .param p1, "ms"    # I

    .prologue
    const/4 v1, 0x5

    .line 949
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mThumbHoverPopupSuspend:Z

    .line 951
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 952
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 954
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 955
    return-void
.end method
