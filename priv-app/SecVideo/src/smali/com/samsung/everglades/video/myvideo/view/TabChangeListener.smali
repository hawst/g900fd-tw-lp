.class public Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;
.super Ljava/lang/Object;
.source "TabChangeListener.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/view/TabChangeListener$DummyTabFactory;
    }
.end annotation


# static fields
.field private static final TAB_CHANGED:I = 0x1

.field private static final TAB_DEVICES:Ljava/lang/String; = "devices"

.field private static final TAB_PERSONAL:Ljava/lang/String; = "personal"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCurrentTabId:I

.field private final mHandler:Landroid/os/Handler;

.field private mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

.field private mTabHost:Landroid/widget/TabHost;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    .line 122
    new-instance v0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener$1;-><init>(Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mHandler:Landroid/os/Handler;

    .line 40
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    .line 41
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    .line 42
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->performTabChange()V

    return-void
.end method

.method private getTabIndexById(I)I
    .locals 1
    .param p1, "id"    # I

    .prologue
    const/4 v0, 0x1

    .line 197
    if-ne p1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTabSpec(Ljava/lang/String;I)Landroid/widget/TabHost$TabSpec;
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "labelId"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, p2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    new-instance v1, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener$DummyTabFactory;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener$DummyTabFactory;-><init>(Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;Lcom/samsung/everglades/video/myvideo/view/TabChangeListener$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    return-object v0
.end method

.method private performTabChange()V
    .locals 5

    .prologue
    .line 132
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    const-string v3, "lastTab"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    .line 134
    .local v0, "id":I
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    if-ne v2, v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 138
    :cond_0
    sget-object v2, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    if-nez v2, :cond_1

    .line 139
    const-string v2, "performTabChange() : viewPager is null"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_1
    sget-object v2, Lcom/samsung/everglades/video/VideoMain;->viewPager:Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    sget-boolean v4, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_SWIPE:Z

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/view/VideoViewPager;->setCurrentItem(IZ)V

    .line 142
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    instance-of v2, v2, Lcom/samsung/everglades/video/VideoMain;

    if-eqz v2, :cond_2

    .line 143
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/samsung/everglades/video/VideoMain;

    .line 144
    .local v1, "videoMain":Lcom/samsung/everglades/video/VideoMain;
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/VideoMain;->updateTabMenu(I)V

    .line 145
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v2, :cond_2

    .line 146
    invoke-virtual {v1}, Lcom/samsung/everglades/video/VideoMain;->cancelSelectionMode()V

    .line 149
    .end local v1    # "videoMain":Lcom/samsung/everglades/video/VideoMain;
    :cond_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    const-string v3, "lastTab"

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 150
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTabChanged() : mCurrentTabId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setupTabs()V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    const-string v1, "personal"

    const v2, 0x7f0c009a

    invoke-direct {p0, v1, v2}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->getTabSpec(Ljava/lang/String;I)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 67
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isNoSupportTab()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    const-string v1, "devices"

    const v2, 0x7f0c0031

    invoke-direct {p0, v1, v2}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->getTabSpec(Ljava/lang/String;I)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 70
    :cond_0
    return-void
.end method


# virtual methods
.method public getCurrentTabId()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    return v0
.end method

.method public init(Landroid/widget/TabHost;)V
    .locals 4
    .param p1, "tabHost"    # Landroid/widget/TabHost;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    .line 46
    invoke-virtual {p1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    .line 48
    .local v0, "widget":Landroid/widget/TabWidget;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isNoSupportTab()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->setY(F)V

    .line 52
    :cond_0
    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {p1}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 56
    :cond_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->setupTabs()V

    .line 57
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->updateTabWidgetGUI()V

    .line 58
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    const-string v2, "lastTab"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    .line 59
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->getTabIndexById(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 60
    invoke-virtual {p1, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 61
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->updateTabWidgetGUI()V

    .line 62
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 4
    .param p1, "tabId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onTabChanged() : tabId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 99
    const-string v0, "personal"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    .line 108
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 110
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 116
    :cond_1
    :goto_1
    return-void

    .line 101
    :cond_2
    const-string v0, "devices"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iput v3, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    .line 103
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    const-string v1, "VPDT"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 113
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1
.end method

.method public onTabChangedByViewPager(I)V
    .locals 5
    .param p1, "viewPage"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    .line 88
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;

    const-string v3, "lastTab"

    iget v4, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 89
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    .line 90
    .local v1, "widget":Landroid/widget/TabWidget;
    invoke-virtual {v1}, Landroid/widget/TabWidget;->getDescendantFocusability()I

    move-result v0

    .line 91
    .local v0, "oldFocusability":I
    const/high16 v2, 0x60000

    invoke-virtual {v1, v2}, Landroid/widget/TabWidget;->setDescendantFocusability(I)V

    .line 92
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mCurrentTabId:I

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 93
    invoke-virtual {v1, v0}, Landroid/widget/TabWidget;->setDescendantFocusability(I)V

    .line 94
    return-void
.end method

.method public updateTabWidgetGUI()V
    .locals 12

    .prologue
    .line 155
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    if-nez v6, :cond_1

    .line 194
    :cond_0
    return-void

    .line 157
    :cond_1
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v6}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v3

    .line 159
    .local v3, "tabWidget":Landroid/widget/TabWidget;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    if-eqz v6, :cond_0

    .line 160
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v3}, Landroid/widget/TabWidget;->getChildCount()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 161
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isNoSupportTab()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 162
    invoke-virtual {v3}, Landroid/widget/TabWidget;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 163
    .local v4, "tabhost_params":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v6, 0x0

    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 164
    invoke-virtual {v3, v4}, Landroid/widget/TabWidget;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    .end local v4    # "tabhost_params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    invoke-virtual {v3, v0}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 167
    .local v2, "tabLayout":Landroid/view/View;
    const v6, 0x1020016

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 169
    .local v5, "title":Landroid/widget/TextView;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 170
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3}, Landroid/widget/TabWidget;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 171
    .restart local v4    # "tabhost_params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    .line 172
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080047

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 173
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080047

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 181
    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    invoke-virtual {v3, v4}, Landroid/widget/TabWidget;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    const/4 v6, 0x1

    const/high16 v7, 0x41600000    # 14.0f

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 184
    const/16 v6, 0x11

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 185
    invoke-virtual {v3}, Landroid/widget/TabWidget;->getWidth()I

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v3}, Landroid/widget/TabWidget;->getChildCount()I

    move-result v6

    if-eqz v6, :cond_3

    .line 186
    invoke-virtual {v3}, Landroid/widget/TabWidget;->getWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/widget/TabWidget;->getChildCount()I

    move-result v7

    div-int/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setWidth(I)V

    .line 189
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c00d1

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c00d2

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v11, v0, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 176
    :cond_4
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08006f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 177
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08006f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_2
.end method
