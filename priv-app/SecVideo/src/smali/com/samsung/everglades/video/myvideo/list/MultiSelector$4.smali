.class Lcom/samsung/everglades/video/myvideo/list/MultiSelector$4;
.super Landroid/os/Handler;
.source "MultiSelector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;)V
    .locals 0

    .prologue
    .line 949
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$4;->this$0:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 951
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 952
    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 965
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 954
    :pswitch_0
    :try_start_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 955
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "size"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 956
    .local v2, "size":J
    # getter for: Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnUpdateTotalSizeListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->access$500()Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 957
    # getter for: Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnUpdateTotalSizeListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->access$500()Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;

    move-result-object v1

    invoke-interface {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;->onUpdate(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 951
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "size":J
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 952
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
