.class public Lcom/samsung/everglades/video/myvideo/common/DB;
.super Ljava/lang/Object;
.source "DB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/common/DB$VideoPlayerDB;
    }
.end annotation


# static fields
.field public static final ALLSHARE_DB_URI:Landroid/net/Uri;

.field public static final CLOUD_DB_URI:Landroid/net/Uri;

.field public static final COLUMN_FLAG_COUNT:Ljava/lang/String; = "-100"

.field public static final DEFAULT_WHERE_FOLDER_BASE:Ljava/lang/String; = "_data not like \'/storage/sdcard0/cloudagent/cache%\'"

.field public static final DEFAULT_WHERE_NORMAL:Ljava/lang/String; = "length(trim(_data)) > 0 AND _data not like \'/storage/sdcard0/cloudagent/cache%\'"

.field public static final EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

.field public static final INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

.field public static final SEARCH_ESCAPE_STRING:Ljava/lang/String; = " escape \'!\'"

.field public static final TITLE_ASCENDING_ORDER:Ljava/lang/String; = " COLLATE LOCALIZED ASC"

.field public static final VIDEO_DB_COLUMN_IS_SECRET_BOX:Ljava/lang/String; = "is_secretbox"

.field public static final VIDEO_DB_COLUMN_RECENTLY_PLAYED:Ljava/lang/String; = "isPlayed"

.field public static final VIDEO_DB_COLUMN_RESUME_POS:Ljava/lang/String; = "resumePos"

.field private static mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private static mInstance:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private static mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 30
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 32
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->CLOUD_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->CLOUD_DB_URI:Landroid/net/Uri;

    .line 34
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    .line 51
    sput-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->mInstance:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 815
    sput-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 817
    sput-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    .line 49
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    .line 54
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    .line 55
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    .line 56
    return-void
.end method

.method private addPrivatePathClause(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 3
    .param p1, "stringBuilder"    # Ljava/lang/StringBuilder;

    .prologue
    .line 248
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getPrivateDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 250
    .local v0, "secretModePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 251
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPrivateMounted(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 252
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND _data not like \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    :cond_0
    return-object p1
.end method

.method private checkKNOXPath(Lcom/samsung/everglades/video/myvideo/common/ListType;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 2
    .param p1, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p2, "stringBuilder"    # Ljava/lang/StringBuilder;

    .prologue
    .line 260
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isKNOXAttr()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AND _data like \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkKNOXPath:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 264
    return-object p2
.end method

.method public static declared-synchronized destroyInstance()V
    .locals 2

    .prologue
    .line 102
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/DB;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->mInstance:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-eqz v0, :cond_0

    .line 103
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->mInstance:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 105
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 106
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    monitor-exit v1

    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private excludeDRMFiles(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 21
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 301
    if-nez p1, :cond_0

    const/4 v4, 0x0

    .line 351
    :goto_0
    return-object v4

    .line 303
    :cond_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_1

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v19

    if-nez v19, :cond_2

    .line 304
    :cond_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 305
    const/4 v4, 0x0

    goto :goto_0

    .line 308
    :cond_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    move/from16 v0, v19

    new-array v12, v0, [I

    .line 309
    .local v12, "noDRMIndex":[I
    const/4 v2, 0x0

    .line 310
    .local v2, "Index":I
    const/4 v10, 0x0

    .line 311
    .local v10, "noDRMCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v6

    .line 314
    .local v6, "drmUtil":Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    :cond_3
    const-string v19, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 315
    .local v5, "data":Ljava/lang/String;
    invoke-virtual {v6, v5}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isNonDrmContent(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 316
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "noDRMCount":I
    .local v11, "noDRMCount":I
    aput v2, v12, v10

    move v10, v11

    .line 318
    .end local v11    # "noDRMCount":I
    .restart local v10    # "noDRMCount":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 319
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v19

    if-nez v19, :cond_3

    .line 321
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    move/from16 v0, v19

    if-ne v10, v0, :cond_5

    .line 322
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-object/from16 v4, p1

    .line 323
    goto :goto_0

    .line 324
    :cond_5
    if-nez v10, :cond_6

    .line 325
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 326
    const/16 p1, 0x0

    .line 327
    const/4 v4, 0x0

    goto :goto_0

    .line 330
    :cond_6
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/DB;->getDefaultVideoColumns()[Ljava/lang/String;

    move-result-object v3

    .line 331
    .local v3, "cols":[Ljava/lang/String;
    new-instance v4, Landroid/database/MatrixCursor;

    invoke-direct {v4, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 333
    .local v4, "cursorToReturn":Landroid/database/MatrixCursor;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v10, :cond_7

    .line 334
    aget v19, v12, v8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 335
    const-string v19, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 336
    .local v18, "videoId":Ljava/lang/String;
    const-string v19, "title"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 337
    .local v17, "title":Ljava/lang/String;
    const-string v19, "duration"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 338
    .local v7, "duration":Ljava/lang/String;
    const-string v19, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 339
    const-string v19, "isPlayed"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 340
    .local v13, "played":I
    const-string v19, "resumePos"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 341
    .local v14, "resumePos":J
    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    .line 342
    .local v16, "resumePosition":Ljava/lang/String;
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 343
    .local v9, "isPlayed":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "isPlayed = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", resumePosition : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 344
    const/16 v19, 0x6

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v18, v19, v20

    const/16 v20, 0x1

    aput-object v17, v19, v20

    const/16 v20, 0x2

    aput-object v7, v19, v20

    const/16 v20, 0x3

    aput-object v5, v19, v20

    const/16 v20, 0x4

    aput-object v16, v19, v20

    const/16 v20, 0x5

    aput-object v9, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 333
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 349
    .end local v7    # "duration":Ljava/lang/String;
    .end local v9    # "isPlayed":Ljava/lang/String;
    .end local v13    # "played":I
    .end local v14    # "resumePos":J
    .end local v16    # "resumePosition":Ljava/lang/String;
    .end local v17    # "title":Ljava/lang/String;
    .end local v18    # "videoId":Ljava/lang/String;
    :cond_7
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "excludeDRMFiles. cursorToReturn count = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v4}, Landroid/database/MatrixCursor;->getCount()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 350
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->moveToFirst()Z

    goto/16 :goto_0
.end method

.method public static final getAllShareVideoColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    const/4 v1, 0x7

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "thumbnail"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "seed"

    aput-object v2, v0, v1

    .line 90
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static final getDefaultVideoColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 59
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "resumePos"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "isPlayed"

    aput-object v2, v0, v1

    .line 67
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method private static final getFolderColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 71
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    .line 77
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/DB;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->mInstance:Lcom/samsung/everglades/video/myvideo/common/DB;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/DB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->mInstance:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 98
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->mInstance:Lcom/samsung/everglades/video/myvideo/common/DB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 291
    if-nez p0, :cond_0

    const-string p0, "_"

    .line 297
    :goto_0
    return-object p0

    .line 293
    :cond_0
    const-string v0, "!"

    const-string v1, "!!"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 294
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 295
    const-string v0, "_"

    const-string v1, "!_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 296
    const-string v0, "%"

    const-string v1, "!%"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 297
    goto :goto_0
.end method

.method private getSortOrderString(Lcom/samsung/everglades/video/myvideo/common/ListType;)Ljava/lang/String;
    .locals 5
    .param p1, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    const/4 v4, 0x0

    .line 110
    const/4 v1, 0x0

    .line 111
    .local v1, "sortOrder":I
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyDownloadAttr()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "allsharesortorder"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    .line 113
    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 114
    add-int/lit8 v1, v1, 0x2

    .line 119
    :cond_1
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DB getSortOrderString() order:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 121
    packed-switch v1, :pswitch_data_0

    .line 142
    const-string v2, "title COLLATE LOCALIZED ASC"

    :goto_1
    return-object v2

    .line 117
    :cond_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "sortorder"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    .line 123
    :pswitch_0
    const-string v2, "isPlayed DESC"

    goto :goto_1

    .line 126
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v2, "(date_added * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    const-string v2, "(date_modified * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    const-string v2, "(datetaken * 1) DESC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 133
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :pswitch_2
    const-string v2, "title COLLATE LOCALIZED ASC"

    goto :goto_1

    .line 136
    :pswitch_3
    const-string v2, "(_size * 1) DESC"

    goto :goto_1

    .line 139
    :pswitch_4
    const-string v2, "mime_type ASC"

    goto :goto_1

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 820
    const/4 v2, 0x0

    .line 822
    .local v2, "uri":Landroid/net/Uri;
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-nez v3, :cond_0

    .line 823
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v3

    sput-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 826
    :cond_0
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    if-nez v3, :cond_1

    .line 827
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v3

    sput-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 830
    :cond_1
    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_3

    .line 831
    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 832
    .local v0, "id":J
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-eqz v3, :cond_3

    .line 833
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v3, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/database/Cursor;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v3, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 834
    :cond_2
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->CLOUD_DB_URI:Landroid/net/Uri;

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 846
    .end local v0    # "id":J
    :cond_3
    :goto_0
    return-object v2

    .line 835
    .restart local v0    # "id":J
    :cond_4
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v3, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 836
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    goto :goto_0

    .line 837
    :cond_5
    invoke-virtual {p2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 838
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0

    .line 839
    :cond_6
    invoke-virtual {p2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 840
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v3, v0, v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getUriById(J)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0

    .line 842
    :cond_7
    sget-object v3, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public deleteLocalFolder(I)I
    .locals 7
    .param p1, "bucketId"    # I

    .prologue
    .line 682
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bucket_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 683
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileListsInCurrentFolder(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 685
    .local v1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v1, :cond_0

    .line 686
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 687
    .local v0, "fileIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 688
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 689
    .local v2, "fileUri":Landroid/net/Uri;
    invoke-virtual {p0, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->remove(Ljava/lang/String;)V

    goto :goto_0

    .line 693
    .end local v0    # "fileIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    .end local v2    # "fileUri":Landroid/net/Uri;
    :cond_0
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    return v4
.end method

.method public deleteSecretContent(Landroid/net/Uri;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 507
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 508
    .local v0, "srcFile":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 509
    :cond_0
    const-string v1, ""

    .line 510
    .local v1, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v1, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public deleteVideo(J)I
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 670
    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 671
    .local v1, "uri":Landroid/net/Uri;
    const-string v0, ""

    .line 672
    .local v0, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public deleteVideo(Landroid/net/Uri;)I
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 676
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->remove(Ljava/lang/String;)V

    .line 677
    const-string v0, ""

    .line 678
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public fetchInt(Landroid/net/Uri;Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v8, -0x1

    .line 753
    if-nez p1, :cond_1

    move v0, v8

    .line 770
    :cond_0
    :goto_0
    return v0

    .line 755
    :cond_1
    const/4 v6, 0x0

    .line 756
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 760
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 761
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 762
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 767
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move v0, v8

    .line 770
    goto :goto_0

    .line 764
    :catch_0
    move-exception v7

    .line 765
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "returnInt() uri:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " column:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " e:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 767
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public fetchLong(Landroid/net/Uri;Ljava/lang/String;)J
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x0

    .line 774
    if-nez p1, :cond_1

    move-wide v0, v8

    .line 791
    :cond_0
    :goto_0
    return-wide v0

    .line 776
    :cond_1
    const/4 v6, 0x0

    .line 777
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 781
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 782
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 783
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 788
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-wide v0, v8

    .line 791
    goto :goto_0

    .line 785
    :catch_0
    move-exception v7

    .line 786
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "returnLong() uri:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " column:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " e:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 788
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 795
    if-nez p1, :cond_1

    move-object v0, v8

    .line 812
    :cond_0
    :goto_0
    return-object v0

    .line 797
    :cond_1
    const/4 v6, 0x0

    .line 798
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 802
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 803
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 804
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 809
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v8

    .line 812
    goto :goto_0

    .line 806
    :catch_0
    move-exception v7

    .line 807
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "returnString() uri:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " column:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " e:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 809
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getBucketID(Landroid/net/Uri;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 372
    const-string v0, "bucket_id"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchInt(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getBucketIdbyPath(Ljava/lang/String;)I
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 547
    const/4 v7, -0x1

    .line 548
    .local v7, "id":I
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "bucket_id"

    aput-object v0, v2, v1

    .line 551
    .local v2, "cols":[Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 553
    .local v9, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data = ?"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    new-array v4, v3, [Ljava/lang/String;

    aput-object p1, v4, v1

    .line 557
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 559
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 561
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 562
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 564
    .local v8, "sBucketId":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 568
    .end local v8    # "sBucketId":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 570
    :cond_1
    return v7

    .line 568
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getCurrentFolderSizeOnExternalDB(Landroid/net/Uri;)J
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 380
    new-array v2, v11, [Ljava/lang/String;

    const-string v0, "sum(_size)"

    aput-object v0, v2, v1

    .line 383
    .local v2, "column":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_display_name LIKE \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFolderName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 384
    .local v10, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 385
    .local v6, "cursor":Landroid/database/Cursor;
    const-wide/16 v8, 0x0

    .line 387
    .local v8, "folderSize":J
    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/DB;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->CLOUD_DB_URI:Landroid/net/Uri;

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 392
    :goto_0
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v11, :cond_2

    .line 393
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 394
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 401
    :goto_1
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 403
    :cond_0
    :goto_2
    return-wide v8

    .line 390
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    goto :goto_0

    .line 396
    :cond_2
    const-wide/16 v8, 0x0

    goto :goto_1

    .line 398
    :catch_0
    move-exception v7

    .line 399
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getCurrentFolderSizeOnExternalDB - SQLiteException :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 401
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getDateTaken(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 431
    const-string v0, "date_modified"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDisplayName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 459
    const-string v0, "_display_name"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDurationTime(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 435
    const-string v0, "duration"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFileId(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 439
    const-string v0, "_id"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFileIdByPath(Ljava/lang/String;)J
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, -0x1

    .line 608
    if-nez p1, :cond_1

    move-wide v0, v2

    .line 620
    :cond_0
    :goto_0
    return-wide v0

    .line 610
    :cond_1
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 612
    .local v0, "id":J
    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/DB;->CLOUD_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 614
    :cond_2
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    if-eqz v4, :cond_3

    .line 615
    cmp-long v4, v0, v2

    if-nez v4, :cond_3

    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 618
    :cond_3
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/DB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 697
    const-wide/16 v8, -0x1

    .line 698
    .local v8, "id":J
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 701
    .local v2, "cols":[Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 702
    .local v10, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data LIKE \'%\' || ? || \'%\'"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 703
    const/4 v6, 0x0

    .line 705
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v4, v1

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 708
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 709
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 714
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 717
    :cond_1
    :goto_0
    return-wide v8

    .line 711
    :catch_0
    move-exception v7

    .line 712
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 714
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getFileListsInCurrentFolder(I)Ljava/util/ArrayList;
    .locals 9
    .param p1, "bucketId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 407
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v1

    .line 410
    .local v2, "column":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 411
    .local v8, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 412
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 415
    .local v7, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 416
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 417
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 419
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 425
    :goto_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 427
    :cond_1
    return-object v7

    .line 422
    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    .line 425
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getFilePath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 451
    const-string v0, "_data"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFolderName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 475
    const-string v0, "bucket_display_name"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFolderNameByBucketID(I)Ljava/lang/String;
    .locals 9
    .param p1, "bucketid"    # I

    .prologue
    const/4 v1, 0x0

    .line 479
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "bucket_display_name"

    aput-object v0, v2, v1

    .line 482
    .local v2, "cols":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 483
    .local v3, "selection":Ljava/lang/String;
    const/4 v8, 0x0

    .line 484
    .local v8, "foldername":Ljava/lang/String;
    const/4 v6, 0x0

    .line 487
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 488
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 489
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 494
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 497
    :cond_1
    :goto_0
    return-object v8

    .line 491
    :catch_0
    move-exception v7

    .line 492
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getFileNameByBucketID() "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 494
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getLastPlayedPathFromVideoDB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DB$VideoPlayerDB;->getLastPlayedFromVideoPlayerDB(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalVideoCount()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 355
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "count(*) AS count"

    aput-object v0, v2, v6

    .line 359
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 361
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 362
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 363
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 364
    .local v6, "count":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 368
    .end local v6    # "count":I
    :cond_0
    return v6
.end method

.method public getMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 463
    const-string v0, "mime_type"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteThumbnailPath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 721
    if-nez p1, :cond_0

    .line 722
    const-string v8, ""

    .line 748
    :goto_0
    return-object v8

    .line 725
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getFileNameById. uri : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 727
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "thumbnail"

    aput-object v0, v2, v3

    .line 730
    .local v2, "cols":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 731
    .local v8, "name":Ljava/lang/String;
    const/4 v6, 0x0

    .line 734
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 735
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 736
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 737
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 742
    :cond_1
    if-eqz v6, :cond_2

    .line 743
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 747
    :cond_2
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getRemoteThumbnailPath. thumbnail path : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 739
    :catch_0
    move-exception v7

    .line 740
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getRemoteThumbnailPath() "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742
    if-eqz v6, :cond_2

    .line 743
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 742
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 743
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getResolution(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 467
    const-string v0, "resolution"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResumePosition(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 443
    const-string v0, "resumePos"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSecretBox(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 447
    const-string v0, "is_secretbox"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSize(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 376
    const-string v0, "_size"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getStringDateTaken(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 471
    const-string v0, "date_modified"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 455
    const-string v0, "title"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUriByPath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const-wide/16 v8, -0x1

    .line 574
    if-eqz p1, :cond_0

    const-string v4, ""

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-object v2, v3

    .line 604
    :cond_1
    :goto_0
    return-object v2

    .line 576
    :cond_2
    const/4 v2, 0x0

    .line 577
    .local v2, "uri":Landroid/net/Uri;
    const-wide/16 v0, -0x1

    .line 579
    .local v0, "id":J
    cmp-long v4, v0, v8

    if-nez v4, :cond_3

    .line 580
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 581
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 584
    :cond_3
    cmp-long v4, v0, v8

    if-nez v4, :cond_4

    .line 585
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/DB;->CLOUD_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 586
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/DB;->CLOUD_DB_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 589
    :cond_4
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    if-eqz v4, :cond_5

    .line 590
    cmp-long v4, v0, v8

    if-nez v4, :cond_5

    .line 591
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 592
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->SKTCLOUD_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 596
    :cond_5
    cmp-long v4, v0, v8

    if-nez v4, :cond_6

    .line 597
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/DB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 598
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/DB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 601
    :cond_6
    cmp-long v4, v0, v8

    if-nez v4, :cond_1

    move-object v2, v3

    .line 602
    goto :goto_0
.end method

.method public getVideoCursor(ILjava/lang/String;Z)Landroid/database/Cursor;
    .locals 2
    .param p1, "bucketId"    # I
    .param p2, "strFilter"    # Ljava/lang/String;
    .param p3, "excludeDRM"    # Z

    .prologue
    .line 147
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/ListType;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;-><init>(I)V

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoCursor(ILjava/lang/String;ZLcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCursor(ILjava/lang/String;ZLcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/database/Cursor;
    .locals 11
    .param p1, "bucketId"    # I
    .param p2, "strFilter"    # Ljava/lang/String;
    .param p3, "excludeDRM"    # Z
    .param p4, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    const/4 v10, 0x0

    .line 151
    const/4 v1, 0x0

    .line 152
    .local v1, "uri":Landroid/net/Uri;
    const/4 v5, 0x0

    .line 153
    .local v5, "orderByClause":Ljava/lang/String;
    const/4 v2, 0x0

    .line 154
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 156
    .local v6, "cursorVideo":Landroid/database/Cursor;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .local v9, "stringBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {p4}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p4}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyDownloadAttr()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 159
    :cond_0
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    .line 160
    invoke-direct {p0, p4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getSortOrderString(Lcom/samsung/everglades/video/myvideo/common/ListType;)Ljava/lang/String;

    move-result-object v5

    .line 161
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/DB;->getAllShareVideoColumns()[Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-virtual {p4}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyDownloadAttr()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    const-string v0, "mime_type NOT LIKE \'application/x-dtcp1%\'"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 182
    invoke-virtual {p4}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbySearchFileList()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "title like \'%"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "%\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " escape \'!\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_2
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 199
    if-eqz p3, :cond_3

    .line 200
    invoke-direct {p0, v6}, Lcom/samsung/everglades/video/myvideo/common/DB;->excludeDRMFiles(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v6

    .line 203
    :cond_3
    if-nez v6, :cond_7

    move-object v0, v10

    .line 212
    :goto_2
    return-object v0

    .line 167
    :cond_4
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 168
    invoke-direct {p0, p4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getSortOrderString(Lcom/samsung/everglades/video/myvideo/common/ListType;)Ljava/lang/String;

    move-result-object v5

    .line 169
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/DB;->getDefaultVideoColumns()[Ljava/lang/String;

    move-result-object v2

    .line 171
    const-string v0, "length(trim(_data)) > 0 AND _data not like \'/storage/sdcard0/cloudagent/cache%\'"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    if-eqz p1, :cond_5

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND bucket_id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    :cond_5
    invoke-direct {p0, p4, v9}, Lcom/samsung/everglades/video/myvideo/common/DB;->checkKNOXPath(Lcom/samsung/everglades/video/myvideo/common/ListType;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 178
    invoke-direct {p0, v9}, Lcom/samsung/everglades/video/myvideo/common/DB;->addPrivatePathClause(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto :goto_0

    .line 185
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND title like \'%"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "%\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " escape \'!\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 191
    :catch_0
    move-exception v7

    .line 192
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVideoCursor - SQLiteException :"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    move-object v0, v10

    .line 193
    goto :goto_2

    .line 194
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 195
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVideoCursor - UnsupportedOperationException :"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    move-object v0, v10

    .line 196
    goto/16 :goto_2

    .line 205
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_7
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_8

    .line 206
    const-string v0, "cursorVideo.getCount() <= 0"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    move-object v0, v6

    .line 207
    goto/16 :goto_2

    .line 210
    :cond_8
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-object v0, v6

    .line 212
    goto/16 :goto_2
.end method

.method public getVideoCursorFolder(Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/database/Cursor;
    .locals 11
    .param p1, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    const/4 v10, 0x0

    .line 216
    const/4 v6, 0x0

    .line 218
    .local v6, "cursorVideo":Landroid/database/Cursor;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .local v9, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v0, "_data not like \'/storage/sdcard0/cloudagent/cache%\'"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC"

    .line 222
    .local v5, "orderByClause":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-direct {p0, v9}, Lcom/samsung/everglades/video/myvideo/common/DB;->addPrivatePathClause(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 224
    :cond_0
    const-string v0, ")  GROUP BY (bucket_id"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFolderColumns()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 236
    if-nez v6, :cond_1

    move-object v0, v10

    .line 244
    :goto_0
    return-object v0

    .line 228
    :catch_0
    move-exception v7

    .line 229
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getVideoCursorFolder - SQLiteException :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    move-object v0, v10

    .line 230
    goto :goto_0

    .line 231
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 232
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getVideoCursorFolder - UnsupportedOperationException :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    move-object v0, v10

    .line 233
    goto :goto_0

    .line 238
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 239
    const-string v0, "getVideoCursorFolder - cursorVideo.getCount() <= 0"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    move-object v0, v6

    .line 240
    goto :goto_0

    .line 243
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-object v0, v6

    .line 244
    goto :goto_0
.end method

.method public getVideoFirstBucketId()I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 268
    const/4 v6, 0x0

    .line 269
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "bucket_id"

    aput-object v0, v2, v8

    .line 272
    .local v2, "projection":[Ljava/lang/String;
    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC LIMIT 1"

    .line 275
    .local v5, "orderByClause":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    const-string v3, "_data not like \'/storage/sdcard0/cloudagent/cache%\'"

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 276
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 277
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 284
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 287
    :cond_0
    :goto_0
    return v0

    .line 284
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move v0, v8

    .line 287
    goto :goto_0

    .line 281
    :catch_0
    move-exception v7

    .line 282
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getVideoFirstBucketId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 284
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public isLocalContent(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 538
    const/4 v0, 0x0

    .line 539
    .local v0, "path":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 540
    if-eqz v0, :cond_0

    .line 541
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->isLocalContent(Ljava/lang/String;)Z

    move-result v1

    .line 543
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isLocalContent(Ljava/lang/String;)Z
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 532
    const-wide/16 v0, -0x1

    .line 533
    .local v0, "id":J
    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 534
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isMediaFolder(I)Z
    .locals 8
    .param p1, "bucketID"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 514
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    move v0, v7

    .line 528
    :goto_0
    return v0

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 518
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 519
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 520
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 521
    const/4 v6, 0x0

    .line 522
    const/4 v0, 0x1

    goto :goto_0

    .line 524
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 525
    const/4 v6, 0x0

    :cond_2
    move v0, v7

    .line 528
    goto :goto_0
.end method

.method public isSecretContent(Landroid/net/Uri;)Z
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 501
    const-wide/16 v0, 0x0

    .line 502
    .local v0, "secret_box":J
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getSecretBox(Landroid/net/Uri;)J

    move-result-wide v0

    .line 503
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setContentDuration(Landroid/net/Uri;J)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "pos"    # J

    .prologue
    .line 640
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setContentDuration() - pos : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 641
    const/4 v0, 0x0

    .line 643
    .local v0, "UpdateCnt":I
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 644
    .local v2, "newValues":Landroid/content/ContentValues;
    const-string v3, "duration"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 647
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 652
    :goto_0
    return v0

    .line 648
    :catch_0
    move-exception v1

    .line 649
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setResumePosition - Exception occured:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setResumePosition(Landroid/net/Uri;J)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "pos"    # J

    .prologue
    .line 624
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setResumePosition() - pos : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 625
    const/4 v0, 0x0

    .line 627
    .local v0, "UpdateCnt":I
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 628
    .local v2, "newValues":Landroid/content/ContentValues;
    const-string v3, "resumePos"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 631
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 636
    :goto_0
    return v0

    .line 632
    :catch_0
    move-exception v1

    .line 633
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setResumePosition - Exception occured:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updatePlayedState(Landroid/net/Uri;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 656
    const/4 v0, 0x0

    .line 657
    .local v0, "UpdateCnt":I
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 658
    .local v2, "newValues":Landroid/content/ContentValues;
    const-string v3, "isPlayed"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 661
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/DB;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 666
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updatePlayedState. return = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 667
    return-void

    .line 662
    :catch_0
    move-exception v1

    .line 663
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updatePlayedState - Exception occured:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method
