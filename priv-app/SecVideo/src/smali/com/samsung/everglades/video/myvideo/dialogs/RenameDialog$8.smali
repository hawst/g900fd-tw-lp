.class Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$8;
.super Ljava/lang/Object;
.source "RenameDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$8;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v2, -0x1

    .line 245
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$8;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 246
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$8;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mOrgName:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$500(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$8;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 252
    :cond_1
    :goto_0
    return-void

    .line 249
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog$8;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->access$300(Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 255
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 241
    return-void
.end method
