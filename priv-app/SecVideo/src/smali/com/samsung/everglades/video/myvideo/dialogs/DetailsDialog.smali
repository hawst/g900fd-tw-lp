.class public Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;
.super Ljava/lang/Object;
.source "DetailsDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$DetailAdapter;
    }
.end annotation


# instance fields
.field private COLON:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, " : "

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    .line 79
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$2;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 42
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    .line 44
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 45
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 46
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$102(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$202(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Lcom/samsung/everglades/video/myvideo/common/DB;)Lcom/samsung/everglades/video/myvideo/common/DB;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/common/DB;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$402(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object p1
.end method

.method private createDataList()V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    .line 93
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setFolderName()V

    .line 95
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setFolderSize()V

    .line 96
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setFolderLocation()V

    .line 107
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setName()V

    .line 99
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setFormat()V

    .line 100
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setResolution()V

    .line 101
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setFileSize()V

    .line 102
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setForwarding()V

    .line 103
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setAudioChannel()V

    .line 104
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setDateAndTime()V

    .line 105
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->setLocation()V

    goto :goto_0
.end method

.method private createDetailsDialog()V
    .locals 8

    .prologue
    const/16 v7, 0x100

    .line 54
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$DetailAdapter;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v5, v6}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$DetailAdapter;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 56
    .local v0, "adapter":Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$DetailAdapter;
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080017

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 57
    .local v3, "listPadding":I
    new-instance v4, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 58
    .local v4, "listView":Landroid/widget/ListView;
    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 59
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 60
    const/high16 v5, 0x2000000

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 61
    invoke-virtual {v4, v3, v3, v3, v3}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 63
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-direct {v2, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 64
    .local v2, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f0c005e

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 65
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 66
    const v5, 0x7f0c0097

    new-instance v6, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$1;

    invoke-direct {v6, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog$1;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;)V

    invoke-virtual {v2, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 73
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 74
    .local v1, "dialog":Landroid/app/AlertDialog;
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 75
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 76
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 77
    return-void
.end method

.method private isDrmContent(Ljava/lang/String;)Z
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v0

    .line 195
    .local v0, "drmUtil":Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isDrmContent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    const/4 v1, 0x1

    .line 198
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isSupportForwarding(Ljava/lang/String;)Z
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    .line 187
    .local v0, "cloudUtil":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    :cond_0
    const/4 v1, 0x0

    .line 190
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private setAudioChannel()V
    .locals 5

    .prologue
    .line 202
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getAudioChannel(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "audioText":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 205
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0027

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    :cond_0
    return-void
.end method

.method private setDateAndTime()V
    .locals 11

    .prologue
    .line 210
    const/4 v3, 0x0

    .line 211
    .local v3, "dateString":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    .line 213
    .local v0, "cloudUtil":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v7}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 214
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v7}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getStringDateTaken(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 215
    if-eqz v3, :cond_0

    .line 216
    new-instance v1, Ljava/util/Date;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {v1, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 217
    .local v1, "date":Ljava/util/Date;
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 218
    .local v2, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 235
    .end local v1    # "date":Ljava/util/Date;
    .end local v2    # "dateFormat":Ljava/text/DateFormat;
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 236
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c0028

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    :cond_1
    return-void

    .line 220
    :cond_2
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v7}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 221
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Lcom/samsung/everglades/video/myvideo/common/DB;->getStringDateTaken(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 223
    :cond_3
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "date_format"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 225
    .local v6, "settingFormat":Ljava/lang/String;
    if-nez v6, :cond_4

    .line 226
    const-string v6, "dd/MM/yyyy"

    .line 228
    :cond_4
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Lcom/samsung/everglades/video/myvideo/common/DB;->getDateTaken(Landroid/net/Uri;)J

    move-result-wide v4

    .line 229
    .local v4, "dateTaken":J
    new-instance v1, Ljava/util/Date;

    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v4

    invoke-direct {v1, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 231
    .restart local v1    # "date":Ljava/util/Date;
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 232
    .restart local v2    # "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private setFileSize()V
    .locals 7

    .prologue
    .line 154
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getSize(Landroid/net/Uri;)J

    move-result-wide v0

    .line 155
    .local v0, "fileSize":J
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    .line 156
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v3, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 157
    .local v2, "sizeText":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c002b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    .end local v2    # "sizeText":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setFolderLocation()V
    .locals 9

    .prologue
    .line 250
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    .line 251
    .local v0, "cloudUtil":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 252
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "filePath":Ljava/lang/String;
    const/4 v4, 0x0

    .line 254
    .local v4, "value":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 255
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v5}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 256
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c003a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 278
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0054

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    :cond_1
    return-void

    .line 257
    .restart local v1    # "filePath":Ljava/lang/String;
    .restart local v4    # "value":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v5}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 258
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c00d5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 259
    :cond_3
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v5}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 260
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 262
    :cond_4
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 263
    .local v3, "path":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 264
    const/4 v2, -0x1

    .line 265
    .local v2, "index":I
    const-string v5, "/storage"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 266
    const-string v5, "/storage"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 271
    :cond_5
    :goto_1
    const/16 v5, 0x2f

    invoke-virtual {v3, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 272
    const/4 v5, -0x1

    if-eq v2, v5, :cond_6

    .line 273
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 275
    :cond_6
    move-object v4, v3

    goto/16 :goto_0

    .line 267
    :cond_7
    const-string v5, "/mnt"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 268
    const-string v5, "/mnt"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private setFolderName()V
    .locals 6

    .prologue
    .line 125
    const/4 v1, 0x0

    .line 126
    .local v1, "titleName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    .line 128
    .local v0, "cloudUtil":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 129
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFolderName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    :goto_0
    if-eqz v1, :cond_0

    .line 135
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c002c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    :cond_0
    return-void

    .line 131
    :cond_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFolderName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private setFolderSize()V
    .locals 7

    .prologue
    .line 162
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getCurrentFolderSizeOnExternalDB(Landroid/net/Uri;)J

    move-result-wide v0

    .line 163
    .local v0, "fileSize":J
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    .line 164
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v3, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 165
    .local v2, "sizeText":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c002d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    .end local v2    # "sizeText":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setFormat()V
    .locals 5

    .prologue
    .line 140
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 142
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c002e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    :cond_0
    return-void
.end method

.method private setForwarding()V
    .locals 6

    .prologue
    .line 170
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "filePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 172
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->isSupportForwarding(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 174
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->isDrmContent(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    const v1, 0x7f0c004a

    .line 179
    .local v1, "stringId":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c002f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    .end local v1    # "stringId":I
    :cond_0
    return-void

    .line 177
    :cond_1
    const v1, 0x7f0c00a0

    .restart local v1    # "stringId":I
    goto :goto_0
.end method

.method private setLocation()V
    .locals 5

    .prologue
    .line 241
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isTypical()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "filePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 244
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0054

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    .end local v0    # "filePath":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setName()V
    .locals 6

    .prologue
    .line 110
    const/4 v1, 0x0

    .line 111
    .local v1, "titleName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    .line 113
    .local v0, "cloudUtil":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 119
    :goto_0
    if-eqz v1, :cond_0

    .line 120
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c002a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_0
    return-void

    .line 116
    :cond_1
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private setResolution()V
    .locals 5

    .prologue
    .line 147
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getResolution(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "resolution":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 149
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mInfoList:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0030

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->COLON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_0
    return-void
.end method


# virtual methods
.method public showDialog()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->createDataList()V

    .line 50
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->createDetailsDialog()V

    .line 51
    return-void
.end method
