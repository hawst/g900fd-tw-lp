.class Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;
.super Ljava/lang/Object;
.source "ASFUtil.java"

# interfaces
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    .locals 3
    .param p1, "sprovider"    # Lcom/samsung/android/allshare/ServiceProvider;
    .param p2, "state"    # Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .prologue
    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mRemoteDeviceServiceConnectListener onCreated. state : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 375
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$4;->$SwitchMap$com$samsung$android$allshare$ServiceConnector$ServiceState:[I

    invoke-virtual {p2}, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 391
    .end local p1    # "sprovider":Lcom/samsung/android/allshare/ServiceProvider;
    :goto_0
    :pswitch_0
    return-void

    .line 380
    .restart local p1    # "sprovider":Lcom/samsung/android/allshare/ServiceProvider;
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$002(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Z)Z

    .line 381
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    check-cast p1, Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .end local p1    # "sprovider":Lcom/samsung/android/allshare/ServiceProvider;
    # setter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;
    invoke-static {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$102(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Lcom/samsung/android/allshare/ServiceProvider;)Lcom/samsung/android/allshare/ServiceProvider;

    .line 382
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$100(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/ServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/ServiceProvider;->getDeviceFinder()Lcom/samsung/android/allshare/DeviceFinder;

    move-result-object v1

    # setter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$202(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Lcom/samsung/android/allshare/DeviceFinder;)Lcom/samsung/android/allshare/DeviceFinder;

    .line 383
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceFinder:Lcom/samsung/android/allshare/DeviceFinder;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/DeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceDiscoveryListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$300(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/DeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 384
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # invokes: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->updateRemoteDeviceList()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$400(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V

    goto :goto_0

    .line 375
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    .locals 2
    .param p1, "sprovider"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    .line 395
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mRemoteDeviceServiceConnectListener onDeleted. mUseASF : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$000(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$000(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mUseASF:Z
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$002(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;Z)Z

    .line 401
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mDeviceServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$100(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/ServiceProvider;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 402
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$1;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->createASFRemoteServiceProvider()V

    .line 404
    :cond_0
    return-void
.end method
