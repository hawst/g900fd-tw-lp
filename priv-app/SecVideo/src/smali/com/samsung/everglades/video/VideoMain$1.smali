.class Lcom/samsung/everglades/video/VideoMain$1;
.super Ljava/lang/Object;
.source "VideoMain.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/VideoMain;->setViewPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/VideoMain;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/VideoMain;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/samsung/everglades/video/VideoMain$1;->this$0:Lcom/samsung/everglades/video/VideoMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 241
    if-nez p1, :cond_2

    .line 242
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/everglades/video/VideoMain;->isSwipe:Z

    .line 243
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v0, :cond_1

    .line 244
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFolderListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 245
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFolderListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->closeMenu()V

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->closeMenu()V

    goto :goto_0

    .line 251
    :cond_2
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/everglades/video/VideoMain;->isSwipe:Z

    goto :goto_0
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 237
    return-void
.end method

.method public onPageSelected(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 215
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPageSelected() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 216
    move v1, p1

    .line 217
    .local v1, "positionTemp":I
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 218
    .local v0, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/samsung/everglades/video/VideoMain$1$1;

    invoke-direct {v2, p0, v1}, Lcom/samsung/everglades/video/VideoMain$1$1;-><init>(Lcom/samsung/everglades/video/VideoMain$1;I)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 234
    return-void
.end method
