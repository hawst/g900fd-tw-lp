.class public Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
.super Ljava/lang/Object;
.source "LiveThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;,
        Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static duration:I

.field private static mUpdateLiveThumbnailListener:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;

.field public static mVideoWallHeight:I

.field public static mVideoWallWidth:I

.field public static mVideoWallfps:I

.field private static transcodingThreadinterrupt:I

.field private static videowall:Z


# instance fields
.field private drmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

.field private mContext:Landroid/content/Context;

.field private mFileMgr:Lcom/sec/android/videowall/FileMgr;

.field private mRunningState:Z

.field private realTranscode:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15
    const-class v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    .line 18
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->videowall:Z

    .line 19
    sput v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I

    sput v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->duration:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->realTranscode:Ljava/lang/Thread;

    .line 23
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mRunningState:Z

    .line 28
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_create()V

    .line 29
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_setDevice(Landroid/content/Context;)V

    .line 30
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mContext:Landroid/content/Context;

    .line 32
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_create()V

    .line 33
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_setDevice(Landroid/content/Context;)V

    .line 34
    new-instance v0, Lcom/sec/android/videowall/FileMgr;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/videowall/FileMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->drmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    .line 36
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->initVideoWall()V

    .line 38
    :cond_0
    return-void
.end method

.method private CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 15
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "vid"    # J
    .param p5, "thischapter"    # I
    .param p6, "durthumbtime"    # I

    .prologue
    .line 186
    sget v13, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallWidth:I

    .line 187
    .local v13, "wd":I
    sget v11, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallHeight:I

    .line 188
    .local v11, "ht":I
    const/4 v12, 0x0

    .line 189
    .local v12, "res":I
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    invoke-virtual {v0}, Lcom/sec/android/videowall/FileMgr;->hasFreeSpace()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "tumbFileName":Ljava/lang/String;
    sget v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I

    if-nez v0, :cond_0

    .line 192
    sget v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->duration:I

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getNumberOfChapter()I

    move-result v2

    div-int/2addr v0, v2

    mul-int v2, v0, p5

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getNumberOfChapter()I

    move-result v4

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getThumbnailFps()I

    move-result v7

    const/4 v9, 0x0

    sget v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->duration:I

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getNumberOfChapter()I

    move-result v3

    div-int v10, v0, v3

    move-object/from16 v0, p1

    move/from16 v3, p5

    move v5, v13

    move v6, v11

    move/from16 v8, p6

    invoke-static/range {v0 .. v10}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_transcode(Ljava/lang/String;Ljava/lang/String;IIIIIIIZI)I

    move-result v12

    .line 195
    :cond_0
    const/4 v0, 0x1

    if-ne v12, v0, :cond_1

    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " transcode success!!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 204
    .end local v1    # "tumbFileName":Ljava/lang/String;
    :goto_0
    return-void

    .line 198
    .restart local v1    # "tumbFileName":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " transcode fail!!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 199
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/videowall/FileMgr;->deleteLastThumbnail(Ljava/lang/String;Ljava/lang/String;JI)V

    goto :goto_0

    .line 202
    .end local v1    # "tumbFileName":Ljava/lang/String;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " thumbnail is existed!!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/sec/android/videowall/FileMgr;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 12
    sget v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->drmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 12
    sget v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->duration:I

    return v0
.end method

.method static synthetic access$502(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 12
    sput p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->duration:I

    return p0
.end method

.method static synthetic access$600()Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mUpdateLiveThumbnailListener:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # J
    .param p5, "x4"    # I
    .param p6, "x5"    # I

    .prologue
    .line 12
    invoke-direct/range {p0 .. p6}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void
.end method

.method private initVideoWall()V
    .locals 2

    .prologue
    .line 41
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getListViewThumbnailWidthEverglades()I

    move-result v0

    sput v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallWidth:I

    .line 42
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getListViewThumbnailHeightEverglades()I

    move-result v0

    sput v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallHeight:I

    .line 44
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getThumbnailFps()I

    move-result v0

    sput v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallfps:I

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " initVideoWall E. mVideoWallWidth = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mVideoWallHeight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mVideoWallfps = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallfps:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public static isVideoWallOn()Z
    .locals 1

    .prologue
    .line 251
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->videowall:Z

    return v0
.end method

.method public static setUpdatedLiveThumbnailListener(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;)V
    .locals 0
    .param p0, "l"    # Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;

    .prologue
    .line 264
    sput-object p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mUpdateLiveThumbnailListener:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;

    .line 265
    return-void
.end method

.method public static setVideoWallOn(Z)V
    .locals 2
    .param p0, "on"    # Z

    .prologue
    .line 255
    sput-boolean p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->videowall:Z

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " setVideoWallOn : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 257
    return-void
.end method


# virtual methods
.method protected CreateLiveThumbnailforListView(Ljava/lang/String;Ljava/lang/String;JI)I
    .locals 15
    .param p1, "sPathName"    # Ljava/lang/String;
    .param p2, "sOnlyFileName"    # Ljava/lang/String;
    .param p3, "vid"    # J
    .param p5, "thumbTime"    # I

    .prologue
    .line 135
    sget v4, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    .line 158
    :goto_0
    return v4

    .line 137
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " transcode start!! : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 138
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 140
    .local v12, "start":J
    new-instance v3, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;-><init>(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;Landroid/content/Context;)V

    .line 141
    .local v3, "transThreadArr":Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;
    const/4 v8, -0x1

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v9, p5

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->setArgs(Ljava/lang/String;Ljava/lang/String;JII)V

    .line 142
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->setPriority(I)V

    .line 143
    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->start()V

    .line 145
    if-eqz v3, :cond_1

    .line 147
    :try_start_0
    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$TranscodeThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " End Transcoding for List !!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 154
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 155
    .local v10, "end":J
    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " transcode End!! : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v10, v12

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 157
    const-wide/16 v4, 0x64

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    .line 158
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 148
    .end local v10    # "end":J
    :catch_0
    move-exception v2

    .line 149
    .local v2, "e":Ljava/lang/InterruptedException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " interrupted exception !!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public cancelCurrentTransJob()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->realTranscode:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->realTranscode:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Cancel current transcoding"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 228
    const/4 v0, 0x1

    sput v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I

    .line 229
    sget v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_stopTranscoding(I)I

    .line 231
    :cond_0
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 245
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_close()I

    .line 248
    :cond_0
    return-void
.end method

.method public createTransThread(Z)V
    .locals 4
    .param p1, "GridListType"    # Z

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->isAliveTransThread()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " thread already have been created."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz p1, :cond_3

    .line 63
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " createTransThread()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 64
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    invoke-virtual {v1, v3}, Lcom/sec/android/videowall/FileMgr;->setInterruptflag(Z)V

    .line 65
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/videowall/FileMgr;->getVideoList(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    .line 66
    .local v0, "videoCursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 70
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_2

    .line 71
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 75
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 77
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;-><init>(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->realTranscode:Ljava/lang/Thread;

    .line 122
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->realTranscode:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 123
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mRunningState:Z

    .line 127
    sget-object v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mUpdateLiveThumbnailListener:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;

    if-eqz v1, :cond_3

    .line 128
    sget-object v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mUpdateLiveThumbnailListener:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;

    invoke-interface {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;->UpdateLiveThumbnail()V

    .line 131
    .end local v0    # "videoCursor":Landroid/database/Cursor;
    :cond_3
    sput v3, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I

    goto :goto_0
.end method

.method public isAliveTransThread()Z
    .locals 3

    .prologue
    .line 234
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isAliveTransThread : mRunningState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mRunningState:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 235
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mRunningState:Z

    return v0
.end method

.method public open()V
    .locals 1

    .prologue
    .line 239
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_open(Z)I

    .line 242
    :cond_0
    return-void
.end method

.method public releaseTransThread()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 207
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->realTranscode:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->realTranscode:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Thread isAlive"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 209
    sput v3, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I

    .line 210
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    invoke-virtual {v1, v3}, Lcom/sec/android/videowall/FileMgr;->setInterruptflag(Z)V

    .line 211
    const-wide/16 v2, 0x12c

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 212
    sget v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_stopTranscoding(I)I

    .line 215
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " transThread.join();"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 216
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->realTranscode:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mRunningState:Z

    .line 222
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stop thread"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 223
    return-void

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public renameLiveThumbnailFile(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "old_vid"    # J
    .param p3, "oldFileName"    # Ljava/lang/String;
    .param p4, "newFilePath"    # Ljava/lang/String;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/videowall/FileMgr;->renameLiveThumbnailFile(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 53
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
