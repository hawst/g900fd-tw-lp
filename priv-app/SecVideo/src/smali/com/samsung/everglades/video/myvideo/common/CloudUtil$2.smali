.class Lcom/samsung/everglades/video/myvideo/common/CloudUtil$2;
.super Ljava/lang/Object;
.source "CloudUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->downloadCloudFile(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field final synthetic val$mUriList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1201
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$2;->val$mUriList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1203
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$2;->val$mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 1205
    .local v2, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v3, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1206
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$000(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->download(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1210
    :catch_0
    move-exception v0

    .line 1211
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "CloudUtil"

    const-string v4, "Failed download com.sec.android.cloudagent selectedListUri"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    const-string v3, "CloudUtil"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1208
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$000(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/android/cloudagent/CloudStore$API;->download(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1215
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_1
    return-void
.end method
