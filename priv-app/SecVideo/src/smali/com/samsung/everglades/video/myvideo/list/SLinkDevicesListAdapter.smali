.class public Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SLinkDevicesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static networkMode:Landroid/widget/TextView;

.field private static secondLineText:Landroid/widget/TextView;


# instance fields
.field private deviceDelete:Landroid/widget/ImageView;

.field private deviceIcon:Landroid/widget/ImageView;

.field private deviceTitle:Landroid/widget/TextView;

.field private listDivider:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mDialogDevice:Landroid/app/AlertDialog;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

.field private vi_slink:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;>;"
    const/4 v1, 0x0

    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 44
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mDialog:Landroid/app/AlertDialog;

    .line 47
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 52
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    .line 53
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->vi_slink:Landroid/view/LayoutInflater;

    .line 54
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 55
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->checkAndShowRegisteredDeviceInfoDialog(Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mDialogDevice:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->openSLinkFileList(Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V

    return-void
.end method

.method private checkAndShowRegisteredDeviceInfoDialog(Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V
    .locals 10
    .param p1, "item"    # Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    .prologue
    const/4 v9, 0x0

    .line 202
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 203
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 204
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v5

    .line 205
    .local v5, "pref":Lcom/samsung/everglades/video/myvideo/common/Pref;
    const-string v7, "samsung_link_charge_warning_popup_confirmed"

    invoke-virtual {v5, v7, v9}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 207
    .local v3, "hasPopupConfirmed":Z
    if-nez v3, :cond_3

    .line 208
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mDialogDevice:Landroid/app/AlertDialog;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mDialogDevice:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 254
    .end local v3    # "hasPopupConfirmed":Z
    .end local v5    # "pref":Lcom/samsung/everglades/video/myvideo/common/Pref;
    :cond_0
    :goto_0
    return-void

    .line 211
    .restart local v3    # "hasPopupConfirmed":Z
    .restart local v5    # "pref":Lcom/samsung/everglades/video/myvideo/common/Pref;
    :cond_1
    const/4 v1, 0x0

    .line 212
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    sget-boolean v7, Lcom/samsung/everglades/video/myvideo/common/Features;->LIGHT_THEME:Z

    if-eqz v7, :cond_2

    .line 213
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    const/4 v7, 0x5

    invoke-direct {v1, v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 218
    .restart local v1    # "builder":Landroid/app/AlertDialog$Builder;
    :goto_1
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 219
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f040013

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 220
    .local v6, "view":Landroid/view/View;
    const v7, 0x7f090036

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 222
    .local v2, "checkBox":Landroid/widget/CheckBox;
    const v7, 0x7f090035

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v8, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$6;

    invoke-direct {v8, p0, v2}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$6;-><init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Landroid/widget/CheckBox;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    const v7, 0x7f0c00b0

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f0c0097

    new-instance v9, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;

    invoke-direct {v9, p0, v2, v5, p1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$8;-><init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Landroid/widget/CheckBox;Lcom/samsung/everglades/video/myvideo/common/Pref;Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f0c000d

    new-instance v9, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$7;

    invoke-direct {v9, p0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$7;-><init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mDialogDevice:Landroid/app/AlertDialog;

    .line 247
    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mDialogDevice:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 215
    .end local v2    # "checkBox":Landroid/widget/CheckBox;
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .end local v6    # "view":Landroid/view/View;
    :cond_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    const/4 v7, 0x4

    invoke-direct {v1, v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .restart local v1    # "builder":Landroid/app/AlertDialog$Builder;
    goto :goto_1

    .line 251
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_3
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->openSLinkFileList(Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V

    goto :goto_0
.end method

.method private openSLinkFileList(Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V
    .locals 4
    .param p1, "item"    # Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 258
    .local v0, "a":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 259
    const-string v2, "DevicesFragment - openSLinkFileList"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 260
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 261
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "list_type"

    const/16 v3, 0xc

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 262
    const-string v2, "s_link_file_list_title"

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    const-string v2, "s_link_device_id"

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getDeviceID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 264
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 266
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v9, 0x7f070013

    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 60
    move-object v4, p2

    .line 61
    .local v4, "v":Landroid/view/View;
    if-nez v4, :cond_0

    .line 62
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->vi_slink:Landroid/view/LayoutInflater;

    const v6, 0x7f040005

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 65
    :cond_0
    const v5, 0x7f090014

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 67
    .local v2, "deviceItemView":Landroid/widget/LinearLayout;
    invoke-virtual {v4, v8}, Landroid/view/View;->setFocusable(Z)V

    .line 68
    const v5, 0x7f090015

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceIcon:Landroid/widget/ImageView;

    .line 69
    const v5, 0x7f090016

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceTitle:Landroid/widget/TextView;

    .line 70
    const v5, 0x7f090018

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    sput-object v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->networkMode:Landroid/widget/TextView;

    .line 71
    const v5, 0x7f090017

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    sput-object v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->secondLineText:Landroid/widget/TextView;

    .line 72
    const v5, 0x7f090013

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceDelete:Landroid/widget/ImageView;

    .line 73
    const v5, 0x7f090012

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->listDivider:Landroid/widget/LinearLayout;

    .line 75
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceDelete:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0024

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceDelete:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 77
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceDelete:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 79
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceDelete:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    .line 81
    .local v3, "hoverPopupWindow":Landroid/widget/HoverPopupWindow;
    if-eqz v3, :cond_1

    .line 82
    const/16 v5, 0x12c

    invoke-virtual {v3, v5}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 83
    const/16 v5, 0x5153

    invoke-virtual {v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 84
    const/4 v5, 0x3

    invoke-virtual {v3, v10, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 87
    :cond_1
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceDelete:Landroid/widget/ImageView;

    new-instance v6, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$1;

    invoke-direct {v6, p0, p1, p3}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;ILandroid/view/ViewGroup;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    .line 95
    .local v1, "deviceItem":Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;
    if-eqz v1, :cond_2

    .line 96
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->noNetworkMode()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 98
    sget-object v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->networkMode:Landroid/widget/TextView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 100
    sget-object v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->secondLineText:Landroid/widget/TextView;

    const v6, 0x7f0c0096

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 101
    sget-object v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->secondLineText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 109
    :goto_0
    const/4 v0, 0x0

    .line 110
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getDeviceIcon()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 112
    if-nez v0, :cond_4

    .line 113
    const-string v5, "SLinkDevicesListAdapter downloader == null"

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 114
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceIcon:Landroid/widget/ImageView;

    new-instance v6, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v6, v10}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 119
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne p1, v5, :cond_2

    .line 120
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->listDivider:Landroid/widget/LinearLayout;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 125
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    new-instance v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$2;

    invoke-direct {v5, p0, v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    new-instance v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$3;

    invoke-direct {v5, p0, v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$3;-><init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 150
    return-object v4

    .line 103
    :cond_3
    sget-object v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->networkMode:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getNetworkMode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    sget-object v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->networkMode:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070016

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 106
    sget-object v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->secondLineText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c001c

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getVideoCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    sget-object v5, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->secondLineText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070027

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 116
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->deviceIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public showPopupDeregistered(ILandroid/view/ViewGroup;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 155
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 156
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    .line 157
    .local v0, "deviceItem":Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;
    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 161
    :cond_0
    const v2, 0x7f0c0025

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 162
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0c000d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$4;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$4;-><init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 168
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0024

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;

    invoke-direct {v3, p0, v0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;-><init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 197
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mDialog:Landroid/app/AlertDialog;

    .line 198
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 199
    return-void
.end method
