.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 1032
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/touchwiz/widget/TwAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1035
    .local p1, "parent":Lcom/sec/android/touchwiz/widget/TwAdapterView;, "Lcom/sec/android/touchwiz/widget/TwAdapterView<*>;"
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isResumed()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1072
    :cond_0
    :goto_0
    return-void

    .line 1039
    :cond_1
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 1040
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->forceStop()V

    .line 1042
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mAdapter:Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$900(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;

    move-result-object v6

    invoke-virtual {v6, p3}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 1043
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    .line 1044
    .local v4, "providerName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1045
    .local v3, "nic":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1046
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1047
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->getProviderName()Ljava/lang/String;

    move-result-object v4

    .line 1048
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mNearbyListUpdater:Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/list/NearbyListUpdater;->getNIC()Ljava/lang/String;

    move-result-object v3

    .line 1058
    :cond_2
    :goto_1
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->isVideoWallOn()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1059
    const/4 v6, 0x1

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setVideoWallOn(Z)V

    .line 1062
    :cond_3
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # invokes: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->updateSelectedFolder(Landroid/view/View;Landroid/database/Cursor;)Z
    invoke-static {v6, p2, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1100(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;Landroid/view/View;Landroid/database/Cursor;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1064
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 1065
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setCursor(Landroid/database/Cursor;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setView(Landroid/view/View;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mBucketId:I
    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1300(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setBucketId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mSearchKey:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1200(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setSearchKey(Ljava/lang/String;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setProviderName(Ljava/lang/String;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setNIC(Ljava/lang/String;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 1067
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v6

    invoke-static {v6, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->execute(Lcom/samsung/everglades/video/myvideo/common/ListType;Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 1069
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1070
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->invalidateActionMode()V

    goto/16 :goto_0

    .line 1050
    .end local v0    # "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    :cond_4
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$000(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceSearchFileLists()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1051
    :cond_5
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f090038

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/SearchView;

    .line 1052
    .local v5, "searchView":Landroid/widget/SearchView;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$13;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v6}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "input_method"

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 1053
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v5, :cond_2

    .line 1054
    invoke-virtual {v5}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto/16 :goto_1
.end method
