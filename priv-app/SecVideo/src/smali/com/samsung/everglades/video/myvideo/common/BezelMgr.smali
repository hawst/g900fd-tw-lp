.class public Lcom/samsung/everglades/video/myvideo/common/BezelMgr;
.super Ljava/lang/Object;
.source "BezelMgr.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private bRegistered:Z

.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

.field private mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-class v0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->TAG:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 15
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 17
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mContext:Landroid/content/Context;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->bRegistered:Z

    .line 22
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mContext:Landroid/content/Context;

    .line 23
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mContext:Landroid/content/Context;

    const-string v1, "quickconnect"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 24
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/BezelMgr$1;-><init>(Lcom/samsung/everglades/video/myvideo/common/BezelMgr;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/common/BezelMgr;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/BezelMgr;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/quickconnect/QuickConnectManager;->terminate()V

    .line 60
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 62
    :cond_0
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 63
    return-void
.end method

.method public registerListener()V
    .locals 2

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->bRegistered:Z

    if-nez v0, :cond_1

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->bRegistered:Z

    .line 38
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->TAG:Ljava/lang/String;

    const-string v1, "registerListener. "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_1
    return-void
.end method

.method public unregisterListener()V
    .locals 2

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->bRegistered:Z

    if-eqz v0, :cond_1

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->bRegistered:Z

    .line 49
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/BezelMgr;->TAG:Ljava/lang/String;

    const-string v1, "unregisterListener. "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_1
    return-void
.end method
