.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 591
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 594
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 655
    :goto_0
    return v0

    .line 596
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 597
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 599
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 615
    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 616
    const/4 v0, 0x1

    goto :goto_0

    .line 601
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020044

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 604
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020047

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 607
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f02003e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 610
    :sswitch_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2300(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020041

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 619
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 652
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->forceStop()V

    goto :goto_0

    .line 621
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020042

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 622
    new-instance v1, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showDialog()V

    .line 623
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 624
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "VPTH"

    const-string v3, "Rename"

    invoke-static {v1, v2, v5, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_2

    .line 628
    :sswitch_5
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020045

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 629
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleShareVia(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 630
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 631
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "VPTH"

    const-string v3, "Share Via"

    invoke-static {v1, v2, v5, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_2

    .line 635
    :sswitch_6
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f02003c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 636
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleDelete(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 637
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 638
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "VPTH"

    const-string v3, "Delete"

    invoke-static {v1, v2, v5, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_2

    .line 642
    :sswitch_7
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2300(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f02003f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 643
    new-instance v1, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->showDialog()V

    .line 644
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 645
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "VPTH"

    const-string v3, "Details"

    invoke-static {v1, v2, v5, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_2

    .line 594
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 599
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_3
        0x7f090060 -> :sswitch_0
        0x7f090061 -> :sswitch_1
        0x7f090062 -> :sswitch_2
    .end sparse-switch

    .line 619
    :sswitch_data_1
    .sparse-switch
        0x7f090042 -> :sswitch_7
        0x7f090060 -> :sswitch_4
        0x7f090061 -> :sswitch_5
        0x7f090062 -> :sswitch_6
    .end sparse-switch
.end method
