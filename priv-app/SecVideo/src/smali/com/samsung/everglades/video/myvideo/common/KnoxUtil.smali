.class public Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;
.super Ljava/lang/Object;
.source "KnoxUtil.java"


# static fields
.field private static mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static connectKnoxContainerInstallerManager(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 20
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_1

    .line 21
    sput-object v3, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-nez v1, :cond_0

    .line 27
    :try_start_0
    new-instance v1, Lcom/sec/knox/containeragent/ContainerInstallerManager;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil$1;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil$1;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/sec/knox/containeragent/ContainerInstallerManager;-><init>(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    sput-object v1, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sput-object v3, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    goto :goto_0

    .line 42
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    sput-object v3, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    goto :goto_0
.end method

.method public static disconnectKnoxContainerManager()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v0, :cond_0

    .line 50
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v0}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->unbindContainerManager()V

    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 53
    :cond_0
    return-void
.end method

.method public static getContainerInstallerManagerInstance()Lcom/sec/knox/containeragent/ContainerInstallerManager;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    return-object v0
.end method

.method public static isKnox2(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 108
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v1, "2.0"

    const-string v2, "version"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    const/4 v1, 0x1

    .line 111
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isKnoxFileRelayAvailable(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 56
    const/4 v0, 0x0

    .line 57
    .local v0, "available":Z
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x12

    if-ge v5, v6, :cond_1

    move v4, v0

    .line 82
    :cond_0
    :goto_0
    return v4

    .line 61
    :cond_1
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnox2(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 62
    const-string v5, "persona"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PersonaManager;

    .line 63
    .local v2, "mPersona":Landroid/os/PersonaManager;
    invoke-virtual {v2}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v3

    .line 64
    .local v3, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 65
    const/4 v4, 0x1

    goto :goto_0

    .line 70
    .end local v2    # "mPersona":Landroid/os/PersonaManager;
    .end local v3    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    :cond_2
    sget-object v5, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v5, :cond_0

    .line 72
    :try_start_0
    sget-object v4, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v4}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->isKNOXFileRelayAvailable()Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_1
    move v4, v0

    .line 82
    goto :goto_0

    .line 73
    :catch_0
    move-exception v1

    .line 74
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    const/4 v0, 0x0

    .line 77
    goto :goto_1

    .line 75
    .end local v1    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 76
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    const/4 v0, 0x0

    .line 77
    goto :goto_1
.end method

.method public static isKnoxMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnox2(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 89
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v1, "true"

    const-string v2, "isKnoxMode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    const/4 v1, 0x1

    .line 93
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSupportMoveTo(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnox2(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 99
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v1, "true"

    const-string v2, "isSupportMoveTo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    const/4 v1, 0x1

    .line 103
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
