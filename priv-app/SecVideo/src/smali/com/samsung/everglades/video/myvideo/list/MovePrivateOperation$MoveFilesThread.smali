.class Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;
.super Ljava/lang/Thread;
.source "MovePrivateOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoveFilesThread"
.end annotation


# instance fields
.field private COPY_BUFFER_SIZE:I

.field private copyFolder:Z

.field private fileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mArraySize:I

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mHandler:Landroid/os/Handler;

.field private mIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;


# direct methods
.method public constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/util/Iterator;Landroid/os/Handler;Landroid/content/Context;Z)V
    .locals 1
    .param p3, "handler"    # Landroid/os/Handler;
    .param p4, "context"    # Landroid/content/Context;
    .param p5, "isFolder"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/os/Handler;",
            "Landroid/content/Context;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    const/4 v0, 0x0

    .line 227
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 218
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mIterator:Ljava/util/Iterator;

    .line 221
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->copyFolder:Z

    .line 225
    const/16 v0, 0x2000

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->COPY_BUFFER_SIZE:I

    .line 228
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mIterator:Ljava/util/Iterator;

    .line 229
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    .line 230
    iput-object p4, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mContext:Landroid/content/Context;

    .line 231
    iput-boolean p5, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->copyFolder:Z

    .line 232
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 233
    iget-object v0, p1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    .line 234
    return-void
.end method

.method private copySubtitleFiles(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "dest"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 495
    const/4 v7, 0x4

    new-array v6, v7, [Ljava/lang/String;

    const-string v7, ".srt"

    aput-object v7, v6, v9

    const/4 v7, 0x1

    const-string v8, ".smi"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, ".sub"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, ".xml"

    aput-object v8, v6, v7

    .line 498
    .local v6, "subtitleExt":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 499
    .local v5, "sourceSubtitleFilename":Ljava/lang/String;
    const/4 v1, 0x0

    .line 503
    .local v1, "destSubtitleFilename":Ljava/lang/String;
    const-string v7, "."

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {p1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 505
    array-length v3, v6

    .local v3, "i":I
    :goto_0
    if-lez v3, :cond_1

    .line 506
    add-int/lit8 v7, v3, -0x1

    aget-object v7, v6, v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 508
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 509
    .local v4, "sourceFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 510
    const-string v7, "."

    invoke-virtual {p2, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {p2, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 511
    add-int/lit8 v7, v3, -0x1

    aget-object v7, v6, v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 512
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 514
    .local v0, "destFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {p0, v4, v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 515
    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    .end local v0    # "destFile":Ljava/io/File;
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 517
    .restart local v0    # "destFile":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 519
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 523
    .end local v0    # "destFile":Ljava/io/File;
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "sourceFile":Ljava/io/File;
    :cond_1
    return-void
.end method

.method private createProgressDialog()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 251
    iget-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->copyFolder:Z

    if-eqz v3, :cond_2

    .line 252
    const/4 v1, 0x0

    .line 253
    .local v1, "cnt":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    if-ge v2, v3, :cond_1

    .line 254
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v3, v3, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-virtual {v4, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getBucketID(Landroid/net/Uri;)I

    move-result v0

    .line 255
    .local v0, "bucketId":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v3, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileListsInCurrentFolder(I)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->fileList:Ljava/util/ArrayList;

    .line 256
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->fileList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 257
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->fileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v1, v3

    .line 253
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 260
    .end local v0    # "bucketId":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    invoke-static {v4, v6, v6, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 264
    .end local v1    # "cnt":I
    .end local v2    # "i":I
    :goto_1
    return-void

    .line 262
    :cond_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    invoke-static {v4, v6, v6, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method

.method private deleteVideo(Landroid/net/Uri;Ljava/io/File;)V
    .locals 2
    .param p1, "fileuri"    # Landroid/net/Uri;
    .param p2, "srcFile"    # Ljava/io/File;

    .prologue
    .line 526
    const-string v0, "startMoveFiles : deleteVideo() "

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 527
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->deleteVideo(Landroid/net/Uri;)I

    .line 528
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mType:I
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$1000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 529
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    .line 531
    :cond_0
    return-void
.end method

.method private moveFiles(Ljava/util/Iterator;)V
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 267
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    const-string v22, "MovePrivateOperation - moveFiles() E"

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 268
    invoke-direct/range {p0 .. p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->createProgressDialog()V

    .line 269
    if-nez p1, :cond_1

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    const/16 v23, 0x5

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 278
    :cond_2
    new-instance v4, Ljava/io/File;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$300()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 280
    .local v4, "destFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 284
    const/4 v13, 0x0

    .line 286
    .local v13, "moveProgressCount":I
    :cond_3
    :goto_1
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    const/16 v23, 0x5

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 418
    :catch_0
    move-exception v7

    .line 419
    .local v7, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    const/16 v23, 0x4

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 292
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_1
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/net/Uri;

    .line 293
    .local v21, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    .line 294
    .local v9, "filePath":Ljava/lang/String;
    new-instance v15, Ljava/io/File;

    invoke-direct {v15, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 296
    .local v15, "sourceFile":Ljava/io/File;
    const/16 v22, 0x0

    const-string v23, "/"

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 298
    .local v17, "sourceFolderPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->copyFolder:Z

    move/from16 v22, v0

    if-eqz v22, :cond_c

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getBucketID(Landroid/net/Uri;)I

    move-result v3

    .line 300
    .local v3, "bucketId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFileListsInCurrentFolder(I)Ljava/util/ArrayList;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->fileList:Ljava/util/ArrayList;

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->fileList:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->fileList:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 306
    .local v8, "fileIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFolderNameByBucketID(I)Ljava/lang/String;

    move-result-object v11

    .line 307
    .local v11, "folderName":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$300()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v5, v0, v11}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    .local v5, "destFolder":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v22

    if-nez v22, :cond_5

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 310
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    const/16 v23, 0x5

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 315
    :cond_6
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/Uri;

    .line 316
    .local v10, "fileUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v20

    .line 317
    .local v20, "srcFilePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lcom/samsung/everglades/video/myvideo/common/DB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v19

    .line 318
    .local v19, "srcFileName":Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 319
    .local v18, "srcFile":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v6, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 321
    .local v6, "dstFile":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->isDirectory()Z

    move-result v22

    if-eqz v22, :cond_7

    .line 362
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_5

    .line 364
    new-instance v16, Ljava/io/File;

    invoke-direct/range {v16 .. v17}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 365
    .local v16, "sourceFolder":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_3

    .line 366
    const-string v22, "MovePrivateOperation - sourceFolder exists - remove folder"

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 367
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 323
    .end local v16    # "sourceFolder":Ljava/io/File;
    :cond_7
    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$300()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v13, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 325
    const-string v22, "MovePrivateOperation - moveFiles() - skip folder! "

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_2

    .line 329
    :cond_8
    const/4 v12, 0x0

    .line 330
    .local v12, "index":I
    move-object/from16 v14, v19

    .line 331
    .local v14, "newFileName":Ljava/lang/String;
    :goto_3
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_9

    .line 332
    const-string v22, "MovePrivateOperation - File with the same name already exists"

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showRenamePopup(Ljava/lang/String;)V
    invoke-static {v0, v14}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$500(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/lang/String;)V

    .line 335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRename:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 336
    add-int/lit8 v12, v12, 0x1

    .line 337
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v23, 0x0

    const/16 v24, 0x2e

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v24

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " ("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x2e

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v23

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 338
    new-instance v6, Ljava/io/File;

    .end local v6    # "dstFile":Ljava/io/File;
    invoke-direct {v6, v5, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v6    # "dstFile":Ljava/io/File;
    goto :goto_3

    .line 344
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameCancellation:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$700(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v13, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameCancellation:Z
    invoke-static/range {v22 .. v23}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$702(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    goto/16 :goto_2

    .line 350
    :cond_a
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v6}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 351
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v10, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->deleteVideo(Landroid/net/Uri;Ljava/io/File;)V

    .line 352
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->copySubtitleFiles(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # operator++ for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mSuccessMoveCnt:I
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$808(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)I

    .line 354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v13, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 356
    const-string v22, "MovePrivateOperation - folder - copyFile() - success"

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    new-instance v23, Landroid/content/Intent;

    const-string v24, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v25

    invoke-direct/range {v23 .. v25}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 360
    :cond_b
    const-string v22, "MovePrivateOperation - copyFile fail"

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 370
    .end local v3    # "bucketId":I
    .end local v5    # "destFolder":Ljava/io/File;
    .end local v6    # "dstFile":Ljava/io/File;
    .end local v8    # "fileIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    .end local v10    # "fileUri":Landroid/net/Uri;
    .end local v11    # "folderName":Ljava/lang/String;
    .end local v12    # "index":I
    .end local v14    # "newFileName":Ljava/lang/String;
    .end local v18    # "srcFile":Ljava/io/File;
    .end local v19    # "srcFileName":Ljava/lang/String;
    .end local v20    # "srcFilePath":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v19

    .line 371
    .restart local v19    # "srcFileName":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$300()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-direct {v6, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    .restart local v6    # "dstFile":Ljava/io/File;
    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$300()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v13, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 374
    const-string v22, "MovePrivateOperation - moveFiles() - skip item! "

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 378
    :cond_d
    const/4 v12, 0x0

    .line 379
    .restart local v12    # "index":I
    move-object/from16 v14, v19

    .line 380
    .restart local v14    # "newFileName":Ljava/lang/String;
    :goto_4
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_f

    .line 381
    const-string v22, "MovePrivateOperation - File with the same name already exists"

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    # invokes: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showRenamePopup(Ljava/lang/String;)V
    invoke-static {v0, v14}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$500(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/lang/String;)V

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRename:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$600(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 385
    add-int/lit8 v12, v12, 0x1

    .line 386
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v23, 0x0

    const/16 v24, 0x2e

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v24

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " ("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x2e

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v23

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 387
    new-instance v6, Ljava/io/File;

    .end local v6    # "dstFile":Ljava/io/File;
    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$300()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v6, v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v6    # "dstFile":Ljava/io/File;
    goto :goto_4

    .line 389
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mReplace:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$900(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v22

    if-eqz v22, :cond_f

    .line 390
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$300()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x2f

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->remove(Ljava/lang/String;)V

    .line 391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mReplace:Z
    invoke-static/range {v22 .. v23}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$902(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    .line 397
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameCancellation:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$700(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v22

    if-eqz v22, :cond_10

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v13, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameCancellation:Z
    invoke-static/range {v22 .. v23}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$702(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z

    goto/16 :goto_1

    .line 403
    :cond_10
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v6}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v22

    if-eqz v22, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v22

    if-nez v22, :cond_11

    .line 404
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v15}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->deleteVideo(Landroid/net/Uri;Ljava/io/File;)V

    .line 405
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v9, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->copySubtitleFiles(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v22, v0

    # operator++ for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mSuccessMoveCnt:I
    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$808(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)I

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v13, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 409
    const-string v22, "MovePrivateOperation - copyFile() - success"

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    new-instance v23, Landroid/content/Intent;

    const-string v24, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v25

    invoke-direct/range {v23 .. v25}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 413
    :cond_11
    const-string v22, "MovePrivateOperation - copyFile fail"

    invoke-static/range {v22 .. v22}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method


# virtual methods
.method public copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 22
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "dstFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 424
    const-string v17, "MovePrivateOperation - copyFile() E"

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 425
    const/4 v7, 0x0

    .line 426
    .local v7, "from":Ljava/io/FileInputStream;
    const/4 v15, 0x0

    .line 429
    .local v15, "to":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 430
    .end local v7    # "from":Ljava/io/FileInputStream;
    .local v8, "from":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v16, Ljava/io/FileOutputStream;

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 431
    .end local v15    # "to":Ljava/io/FileOutputStream;
    .local v16, "to":Ljava/io/FileOutputStream;
    :try_start_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->COPY_BUFFER_SIZE:I

    move/from16 v17, v0

    move/from16 v0, v17

    new-array v4, v0, [B

    .line 433
    .local v4, "buffer":[B
    const-wide/16 v12, 0x0

    .line 434
    .local v12, "readLength":J
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v10

    .line 435
    .local v10, "length":J
    const/4 v9, 0x0

    .line 436
    .local v9, "percentage":I
    :goto_0
    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->COPY_BUFFER_SIZE:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v4, v0, v1}, Ljava/io/FileInputStream;->read([BII)I

    move-result v5

    .local v5, "bytesRead":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v5, v0, :cond_0

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 452
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 453
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 463
    :cond_1
    if-eqz v8, :cond_2

    .line 464
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 470
    :cond_2
    :goto_1
    if-eqz v16, :cond_3

    .line 471
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 477
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x5

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 479
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_f

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v18

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v20

    cmp-long v17, v18, v20

    if-eqz v17, :cond_f

    .line 480
    const-string v17, "Failed to move"

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 481
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v14

    .line 482
    .local v14, "ret":Z
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "delete destination file - "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 483
    if-nez v14, :cond_4

    .line 484
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v14

    .line 485
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Try one more time to delete incomplete file - "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 487
    :cond_4
    const/16 v17, 0x0

    move-object/from16 v15, v16

    .end local v16    # "to":Ljava/io/FileOutputStream;
    .restart local v15    # "to":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 491
    .end local v4    # "buffer":[B
    .end local v5    # "bytesRead":I
    .end local v8    # "from":Ljava/io/FileInputStream;
    .end local v9    # "percentage":I
    .end local v10    # "length":J
    .end local v12    # "readLength":J
    .end local v14    # "ret":Z
    .restart local v7    # "from":Ljava/io/FileInputStream;
    :goto_3
    return v17

    .line 440
    .end local v7    # "from":Ljava/io/FileInputStream;
    .end local v15    # "to":Ljava/io/FileOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "bytesRead":I
    .restart local v8    # "from":Ljava/io/FileInputStream;
    .restart local v9    # "percentage":I
    .restart local v10    # "length":J
    .restart local v12    # "readLength":J
    .restart local v16    # "to":Ljava/io/FileOutputStream;
    :cond_5
    const/16 v17, 0x0

    :try_start_5
    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1, v5}, Ljava/io/FileOutputStream;->write([BII)V

    .line 441
    int-to-long v0, v5

    move-wide/from16 v18, v0

    add-long v12, v12, v18

    .line 443
    cmp-long v17, v12, v10

    if-nez v17, :cond_a

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    move/from16 v21, v0

    invoke-static/range {v18 .. v21}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 445
    const-string v17, "MovePrivateOperation - copyFile() complete"

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 456
    .end local v4    # "buffer":[B
    .end local v5    # "bytesRead":I
    .end local v9    # "percentage":I
    .end local v10    # "length":J
    .end local v12    # "readLength":J
    :catch_0
    move-exception v6

    move-object/from16 v15, v16

    .end local v16    # "to":Ljava/io/FileOutputStream;
    .restart local v15    # "to":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 457
    .end local v8    # "from":Ljava/io/FileInputStream;
    .local v6, "e":Ljava/lang/Exception;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    :goto_4
    :try_start_6
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "copyFile Exception : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 458
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 459
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 463
    :cond_6
    if-eqz v7, :cond_7

    .line 464
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 470
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_5
    if-eqz v15, :cond_8

    .line 471
    :try_start_8
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 477
    :cond_8
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v17

    if-eqz v17, :cond_10

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x5

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 479
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_10

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v18

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v20

    cmp-long v17, v18, v20

    if-eqz v17, :cond_10

    .line 480
    const-string v17, "Failed to move"

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 481
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v14

    .line 482
    .restart local v14    # "ret":Z
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "delete destination file - "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 483
    if-nez v14, :cond_9

    .line 484
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v14

    .line 485
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Try one more time to delete incomplete file - "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 487
    :cond_9
    const/16 v17, 0x0

    goto/16 :goto_3

    .line 447
    .end local v7    # "from":Ljava/io/FileInputStream;
    .end local v14    # "ret":Z
    .end local v15    # "to":Ljava/io/FileOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "bytesRead":I
    .restart local v8    # "from":Ljava/io/FileInputStream;
    .restart local v9    # "percentage":I
    .restart local v10    # "length":J
    .restart local v12    # "readLength":J
    .restart local v16    # "to":Ljava/io/FileOutputStream;
    :cond_a
    const-wide/16 v18, 0x64

    mul-long v18, v18, v12

    :try_start_9
    div-long v18, v18, v10

    move-wide/from16 v0, v18

    long-to-int v9, v0

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mArraySize:I

    move/from16 v20, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v9, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 462
    .end local v4    # "buffer":[B
    .end local v5    # "bytesRead":I
    .end local v9    # "percentage":I
    .end local v10    # "length":J
    .end local v12    # "readLength":J
    :catchall_0
    move-exception v17

    move-object/from16 v15, v16

    .end local v16    # "to":Ljava/io/FileOutputStream;
    .restart local v15    # "to":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 463
    .end local v8    # "from":Ljava/io/FileInputStream;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    :goto_7
    if-eqz v7, :cond_b

    .line 464
    :try_start_a
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 470
    :cond_b
    :goto_8
    if-eqz v15, :cond_c

    .line 471
    :try_start_b
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    .line 477
    :cond_c
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x5

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 479
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_e

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v18

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v20

    cmp-long v18, v18, v20

    if-eqz v18, :cond_e

    .line 480
    const-string v17, "Failed to move"

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 481
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v14

    .line 482
    .restart local v14    # "ret":Z
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "delete destination file - "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 483
    if-nez v14, :cond_d

    .line 484
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v14

    .line 485
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Try one more time to delete incomplete file - "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 487
    :cond_d
    const/16 v17, 0x0

    goto/16 :goto_3

    .line 466
    .end local v7    # "from":Ljava/io/FileInputStream;
    .end local v14    # "ret":Z
    .end local v15    # "to":Ljava/io/FileOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "bytesRead":I
    .restart local v8    # "from":Ljava/io/FileInputStream;
    .restart local v9    # "percentage":I
    .restart local v10    # "length":J
    .restart local v12    # "readLength":J
    .restart local v16    # "to":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v6

    .line 467
    .local v6, "e":Ljava/io/IOException;
    const-string v17, "copyFile : Fail to close the outputStream"

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 473
    .end local v6    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v6

    .line 474
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v17, "copyFile : Fail to close the inputStream"

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 466
    .end local v4    # "buffer":[B
    .end local v5    # "bytesRead":I
    .end local v8    # "from":Ljava/io/FileInputStream;
    .end local v9    # "percentage":I
    .end local v10    # "length":J
    .end local v12    # "readLength":J
    .end local v16    # "to":Ljava/io/FileOutputStream;
    .local v6, "e":Ljava/lang/Exception;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    .restart local v15    # "to":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v6

    .line 467
    .local v6, "e":Ljava/io/IOException;
    const-string v17, "copyFile : Fail to close the outputStream"

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 473
    .end local v6    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v6

    .line 474
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v17, "copyFile : Fail to close the inputStream"

    invoke-static/range {v17 .. v17}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 466
    .end local v6    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v6

    .line 467
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v18, "copyFile : Fail to close the outputStream"

    invoke-static/range {v18 .. v18}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 473
    .end local v6    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v6

    .line 474
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v18, "copyFile : Fail to close the inputStream"

    invoke-static/range {v18 .. v18}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_9

    .line 487
    .end local v6    # "e":Ljava/io/IOException;
    :cond_e
    throw v17

    .end local v7    # "from":Ljava/io/FileInputStream;
    .end local v15    # "to":Ljava/io/FileOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "bytesRead":I
    .restart local v8    # "from":Ljava/io/FileInputStream;
    .restart local v9    # "percentage":I
    .restart local v10    # "length":J
    .restart local v12    # "readLength":J
    .restart local v16    # "to":Ljava/io/FileOutputStream;
    :cond_f
    move-object/from16 v15, v16

    .end local v16    # "to":Ljava/io/FileOutputStream;
    .restart local v15    # "to":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 491
    .end local v4    # "buffer":[B
    .end local v5    # "bytesRead":I
    .end local v8    # "from":Ljava/io/FileInputStream;
    .end local v9    # "percentage":I
    .end local v10    # "length":J
    .end local v12    # "readLength":J
    .restart local v7    # "from":Ljava/io/FileInputStream;
    :cond_10
    const/16 v17, 0x1

    goto/16 :goto_3

    .line 462
    :catchall_1
    move-exception v17

    goto/16 :goto_7

    .end local v7    # "from":Ljava/io/FileInputStream;
    .restart local v8    # "from":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v17

    move-object v7, v8

    .end local v8    # "from":Ljava/io/FileInputStream;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .line 456
    :catch_7
    move-exception v6

    goto/16 :goto_4

    .end local v7    # "from":Ljava/io/FileInputStream;
    .restart local v8    # "from":Ljava/io/FileInputStream;
    :catch_8
    move-exception v6

    move-object v7, v8

    .end local v8    # "from":Ljava/io/FileInputStream;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    goto/16 :goto_4
.end method

.method public run()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 239
    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mIterator:Ljava/util/Iterator;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->moveFiles(Ljava/util/Iterator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 246
    :goto_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 248
    return-void

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 241
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 246
    :goto_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    throw v0

    .line 244
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
