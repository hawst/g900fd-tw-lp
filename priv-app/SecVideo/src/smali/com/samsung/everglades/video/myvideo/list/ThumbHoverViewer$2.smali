.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->initViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 356
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v1

    if-ne v1, v2, :cond_0

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-nez v1, :cond_0

    .line 357
    const-string v1, "ThumbHoverViewer - initViews() BUTTON_SECONDARY"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 358
    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsAirButtonShowing:Z
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1202(Z)Z

    .line 360
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 409
    :cond_1
    :goto_0
    :pswitch_0
    return v3

    .line 362
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 363
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 365
    :cond_2
    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1300()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 367
    const/16 v1, 0xa

    const/4 v2, -0x1

    :try_start_0
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    :cond_3
    :goto_1
    const-string v1, "ThumbHoverViewer - initViews() inner hover listener ENTER"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 377
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 378
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 380
    :cond_4
    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1300()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 382
    const/16 v1, 0xa

    const/4 v2, -0x1

    :try_start_1
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 383
    :catch_1
    move-exception v0

    .line 384
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 390
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mAirButtonViewer:Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/AirButtonViewer;->mAirButtonIsThere()Z

    move-result v1

    if-eqz v1, :cond_5

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsAirButtonShowing:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1200()Z

    move-result v1

    if-nez v1, :cond_6

    .line 391
    :cond_5
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->dismissPopupMessage()V

    .line 393
    :cond_6
    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1300()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 395
    const/4 v1, 0x1

    const/4 v2, -0x1

    :try_start_2
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 400
    :cond_7
    :goto_2
    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1302(Z)Z

    .line 401
    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsAirButtonShowing:Z
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1202(Z)Z

    .line 402
    const-string v1, "ThumbHoverViewer - initViews() inner hover listener EXIT"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 396
    :catch_2
    move-exception v0

    .line 397
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 360
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
