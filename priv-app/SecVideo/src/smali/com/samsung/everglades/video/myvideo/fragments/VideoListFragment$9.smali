.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$9;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setupLiveThumbnail()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 952
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public UpdateLiveThumbnail()V
    .locals 4

    .prologue
    const/16 v2, 0x64

    .line 955
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 956
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setVideoWallOn(Z)V

    .line 957
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 958
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 959
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$9;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 962
    :cond_0
    return-void
.end method
