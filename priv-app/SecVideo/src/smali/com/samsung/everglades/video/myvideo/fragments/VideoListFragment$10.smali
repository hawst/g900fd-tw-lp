.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setImageCacheListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 979
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdated()V
    .locals 4

    .prologue
    const/16 v2, 0x64

    .line 982
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mScrollState:I
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$800(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 983
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 984
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 985
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$10;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 988
    :cond_0
    return-void
.end method
