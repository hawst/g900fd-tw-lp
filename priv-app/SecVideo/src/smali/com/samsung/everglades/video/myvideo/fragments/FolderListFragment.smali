.class public Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
.super Landroid/app/Fragment;
.source "FolderListFragment.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;


# static fields
.field public static SPLIT_DEFAULT_WIDTH_L:I = 0x0

.field public static final SPLIT_LANDSCAPE_DEFAULT:F = 0.3f

.field public static final SPLIT_LEFT_VIEW_LIMIT:F = 0.08f

.field public static SPLIT_MAX_WIDTH_L:I = 0x0

.field public static SPLIT_MIN_WIDTH_L:I = 0x0

.field public static final SPLIT_RIGHT_VIEW_LIMIT:F = 0.95f


# instance fields
.field private mContentList:Landroid/widget/FrameLayout;

.field private mContentListAttr:I

.field private mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

.field private mFolderList:Landroid/widget/FrameLayout;

.field private mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

.field private mFolderMargin:Landroid/widget/FrameLayout;

.field private mFolderMarginImg:Landroid/widget/ImageView;

.field private final mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mGlobalLayoutWidth:I

.field private mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mOrientation:I

.field private mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

.field private mViewTreeObserver:Landroid/view/ViewTreeObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x1e6

    sput v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_DEFAULT_WIDTH_L:I

    .line 45
    const/16 v0, 0x66

    sput v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_MIN_WIDTH_L:I

    .line 46
    const/16 v0, 0x34c

    sput v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_MAX_WIDTH_L:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListAttr:I

    .line 52
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$1;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 107
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$2;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 388
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment$3;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMarginImg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderList:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getWindowWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mGlobalLayoutWidth:I

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mGlobalLayoutWidth:I

    return p1
.end method

.method private createContentListFragment()V
    .locals 3

    .prologue
    .line 313
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 314
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "list_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 315
    const-string v1, "list_additional_infos"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316
    const-string v1, "bucket_id"

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getCurrentBucketId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 317
    const-string v1, "list_attribute"

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListAttr:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 318
    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;-><init>()V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 319
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 320
    return-void
.end method

.method private createFolderListFragment()V
    .locals 5

    .prologue
    .line 304
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 305
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "lastViewAs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    .line 306
    .local v1, "listType":I
    const-string v2, "list_type"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 307
    new-instance v2, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;-><init>()V

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 308
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v2, p0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setOnFolderChangedListener(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnFolderChangedListener;)V

    .line 309
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v2, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 310
    return-void
.end method

.method private getCurrentBucketId()I
    .locals 4

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    .line 346
    .local v0, "db":Lcom/samsung/everglades/video/myvideo/common/DB;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "lastFolder"

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoFirstBucketId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private getWindowWidth()I
    .locals 2

    .prologue
    .line 187
    const/4 v0, -0x1

    .line 190
    .local v0, "width":I
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 195
    :goto_0
    return v0

    .line 191
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private initArgs()V
    .locals 3

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 147
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 148
    const-string v1, "list_attribute"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListAttr:I

    .line 150
    :cond_0
    return-void
.end method

.method private isInSplitMode()Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 377
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v6

    const-string v7, "lastViewAs"

    invoke-virtual {v6, v7, v5}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v3

    .line 378
    .local v3, "listType":I
    if-ne v3, v8, :cond_0

    move v0, v4

    .line 379
    .local v0, "isFolder":Z
    :goto_0
    iget v6, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I

    if-ne v6, v8, :cond_1

    move v1, v4

    .line 380
    .local v1, "isLandscape":Z
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isMultiWindow(Landroid/content/Context;)Z

    move-result v2

    .line 381
    .local v2, "isMultiWindow":Z
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    :goto_2
    return v4

    .end local v0    # "isFolder":Z
    .end local v1    # "isLandscape":Z
    .end local v2    # "isMultiWindow":Z
    :cond_0
    move v0, v5

    .line 378
    goto :goto_0

    .restart local v0    # "isFolder":Z
    :cond_1
    move v1, v5

    .line 379
    goto :goto_1

    .restart local v1    # "isLandscape":Z
    .restart local v2    # "isMultiWindow":Z
    :cond_2
    move v4, v5

    .line 381
    goto :goto_2
.end method

.method public static newInstance()Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;-><init>()V

    .line 126
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    return-object v0
.end method

.method private setWeightOfFragments()V
    .locals 8

    .prologue
    const/16 v5, 0x8

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 226
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->isInSplitMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 227
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08003c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 228
    .local v0, "left":I
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08003d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 229
    .local v2, "splitBar":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderList:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v0, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 230
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v2, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 231
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 232
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08003e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 233
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 234
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentList:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 235
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 236
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentList:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 242
    .end local v0    # "left":I
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "splitBar":I
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderList:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 239
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 240
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentList:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateOrientation()V
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I

    .line 275
    return-void
.end method


# virtual methods
.method public changeListType()V
    .locals 1

    .prologue
    .line 350
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->setWeightOfFragments()V

    .line 351
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setListView()V

    .line 353
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    .line 355
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->updateMenuVisibility()V

    .line 356
    return-void
.end method

.method public disableMenu()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 278
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setMenuVisibility(Z)V

    .line 280
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setMenuVisibility(Z)V

    .line 282
    :cond_0
    return-void
.end method

.method public getContentListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    return-object v0
.end method

.method public getFolderListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    return-object v0
.end method

.method public getVideoListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .locals 1

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->isInSplitMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 324
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectAttr()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 332
    :goto_0
    return-object v0

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectAttr()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    goto :goto_0

    .line 329
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    goto :goto_0

    .line 332
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    goto :goto_0
.end method

.method public isSelectionMode()Z
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isSelectable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isSelectable()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x0

    .line 169
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 171
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I

    if-eq v0, v1, :cond_2

    .line 172
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I

    .line 173
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->isSelectable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getMultiDelete()Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getMultiDelete()Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->dismissConfirmDeleteFiles()V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->stopActionMode()V

    .line 180
    :cond_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->setWeightOfFragments()V

    .line 181
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->updateMenuVisibility()V

    .line 182
    invoke-virtual {p0, v2, v2}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->setSplitWidth(ZZ)V

    .line 184
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 131
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 132
    const-string v0, "FolderListFragment - onCreate() enter"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 133
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->initArgs()V

    .line 134
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 136
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_1

    .line 139
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 140
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 142
    :cond_1
    const-string v0, "FolderListFragment - onCreate() end"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    const-string v1, "FolderListFragment - onCreateView() enter"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 155
    const v1, 0x7f04000c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 156
    .local v0, "root":Landroid/view/View;
    const v1, 0x7f090028

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderList:Landroid/widget/FrameLayout;

    .line 157
    const v1, 0x7f090029

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;

    .line 158
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 159
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMargin:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 160
    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderMarginImg:Landroid/widget/ImageView;

    .line 161
    const v1, 0x7f09002b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentList:Landroid/widget/FrameLayout;

    .line 162
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mViewTreeObserver:Landroid/view/ViewTreeObserver;

    .line 163
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mViewTreeObserver:Landroid/view/ViewTreeObserver;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 164
    const-string v1, "FolderListFragment - onCreateView() end"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 165
    return-object v0
.end method

.method public onFolderChanged(I)V
    .locals 5
    .param p1, "bucketId"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 360
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "lastFolder"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 362
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "lastTab"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v0

    const-string v1, "lastViewAs"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v4, :cond_1

    .line 364
    :cond_0
    const-string v0, "FolderListFragment - onFolderChanged() : current tab is download tab or current view is not folder view"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 374
    :goto_0
    return-void

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->cancelSelectionMode()V

    .line 368
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->stopActionMode()V

    .line 369
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->initArgs()V

    .line 370
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/common/ListType;

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListAttr:I

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/ListType;-><init>(III)V

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 371
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getCurrentBucketId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setBucketId(I)V

    .line 372
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setListView()V

    .line 373
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->disableMenu()V

    .line 270
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 271
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 246
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 247
    const-string v1, "FolderListFragment - onResume() enter"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 249
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->updateOrientation()V

    .line 250
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-nez v1, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 252
    .local v0, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->createFolderListFragment()V

    .line 253
    const v1, 0x7f090028

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 254
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->createContentListFragment()V

    .line 255
    const v1, 0x7f09002b

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 257
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "lastTab"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 258
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->updateMenuVisibility()V

    .line 262
    :goto_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->setWeightOfFragments()V

    .line 263
    invoke-virtual {p0, v4, v4}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->setSplitWidth(ZZ)V

    .line 264
    const-string v1, "FolderListFragment - onResume() end"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 265
    return-void

    .line 260
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->disableMenu()V

    goto :goto_0
.end method

.method public removeFragment()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 285
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 286
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 287
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 288
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 289
    return-void
.end method

.method public setSplitValue()V
    .locals 4

    .prologue
    .line 213
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 214
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 215
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 217
    .local v0, "deviceWidth":I
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 218
    int-to-float v2, v0

    const v3, 0x3e99999a    # 0.3f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_DEFAULT_WIDTH_L:I

    .line 219
    int-to-float v2, v0

    const v3, 0x3da3d70a    # 0.08f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_MIN_WIDTH_L:I

    .line 220
    int-to-float v2, v0

    const v3, 0x3f733333    # 0.95f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_MAX_WIDTH_L:I

    .line 222
    :cond_0
    return-void
.end method

.method public setSplitWidth(ZZ)V
    .locals 6
    .param p1, "bReset"    # Z
    .param p2, "isCallOnResume"    # Z

    .prologue
    const/4 v5, 0x2

    .line 199
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->setSplitValue()V

    .line 201
    sget v1, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_DEFAULT_WIDTH_L:I

    .line 202
    .local v1, "x":I
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I

    if-ne v2, v5, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "splitwidth"

    sget v4, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->SPLIT_DEFAULT_WIDTH_L:I

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "lastViewAs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v5, :cond_1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mOrientation:I

    if-ne v2, v5, :cond_1

    .line 207
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 208
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderList:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 210
    .end local v0    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    return-void
.end method

.method public updateMenuVisibility()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 292
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_0

    .line 293
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->isInSplitMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setMenuVisibility(Z)V

    .line 295
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setMenuVisibility(Z)V

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setMenuVisibility(Z)V

    .line 298
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->mContentListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setMenuVisibility(Z)V

    goto :goto_0
.end method
