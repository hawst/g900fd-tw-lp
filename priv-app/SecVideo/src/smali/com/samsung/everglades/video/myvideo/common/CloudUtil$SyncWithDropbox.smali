.class Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithDropbox;
.super Landroid/os/AsyncTask;
.source "CloudUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncWithDropbox"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;


# direct methods
.method public constructor <init>(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1324
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithDropbox;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1325
    # setter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;
    invoke-static {p1, p2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$002(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;Landroid/content/Context;)Landroid/content/Context;

    .line 1326
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1323
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithDropbox;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 1329
    # getter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mCloudAgentExistence:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$100()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1331
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/CloudUtil$SyncWithDropbox;->this$0:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->access$000(Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/CloudStore$API;->sync(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1339
    :cond_0
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 1332
    :catch_0
    move-exception v0

    .line 1333
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "CloudUtil"

    const-string v2, "sync() - NullPointerException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1334
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 1335
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "CloudUtil"

    const-string v2, "sync() - Exception"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
