.class public interface abstract Lcom/samsung/everglades/video/myvideo/common/Const;
.super Ljava/lang/Object;
.source "Const.java"


# static fields
.field public static final ACTION_AUTO_PLAY_NEXT:Ljava/lang/String; = "com.sec.android.app.videoplayer.AUTO_PLAY_NEXT"

.field public static final ACTION_CLOUD_OPTION:Ljava/lang/String; = "com.sec.android.app.videoplayer.CLOUD_OPTION"

.field public static final ACTION_FINISH_BY_MULTIWINDOW:Ljava/lang/String; = "com.samsung.everglades.video.FINISH_BY_MULTIWINDOW"

.field public static final ACTION_LAST_FILE_NAME:Ljava/lang/String; = "com.samsung.everglades.video.myvideo.LAST_FILE_NAME"

.field public static final ACTION_MEDIA_MOUNTED:Ljava/lang/String; = "android.intent.action.MEDIA_SCAN"

.field public static final ACTION_SORT_BY:Ljava/lang/String; = "com.sec.android.app.videoplayer.SORT_BY"

.field public static final ACTIVITY_VIDEOPLAYER:Ljava/lang/String; = ".activity.MoviePlayer"

.field public static final API_LEVEL_19:I = 0x13

.field public static final AUTO_PLAY_VALUE:Ljava/lang/String; = "autoplay"

.field public static final BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field public static final BUCKET_NAME:Ljava/lang/String; = "bucket_name"

.field public static final CLOSING_BRACE:Ljava/lang/String; = ")"

.field public static final CLOUD_BAIDU_CONTENT:I = 0x2

.field public static final CLOUD_DROPBOX_CONTENT:I = 0x0

.field public static final CLOUD_OPTION:Ljava/lang/String; = "cloud_option"

.field public static final CLOUD_SKT_CONTENT:I = 0x1

.field public static final DOWNLOAD_MODAL_REQUEST_CODE:I = 0x64

.field public static final EXTERNAL_ROOT_PATH:Ljava/lang/String; = "/storage/extSdCard"

.field public static final HOVER_MODE_AUTO:I = 0x2

.field public static final HOVER_MODE_FINGER:I = 0x1

.field public static final HOVER_MODE_PEN:I = 0x0

.field public static final INTERNAL_ROOT_PATH:Ljava/lang/String;

.field public static final IS_DROPBOX_FOLDER:Ljava/lang/String; = "dropbox_folder"

.field public static final LIST_ADDITIONS:Ljava/lang/String; = "list_additional_infos"

.field public static final LIST_ATTR:Ljava/lang/String; = "list_attribute"

.field public static final LIST_TYPE:Ljava/lang/String; = "list_type"

.field public static final LOGGING_APPID:Ljava/lang/String; = "com.sec.android.app.videoplayer"

.field public static final LOGGING_CONTEXT_FRAMEWORK:Ljava/lang/String; = "com.samsung.android.providers.context"

.field public static final LOGGING_FEATURE_SURVEY_ACTION:Ljava/lang/String; = "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

.field public static final LOGGING_MULTI_FEATURE_SURVEY_ACTION:Ljava/lang/String; = "com.samsung.android.providers.context.log.action.REPORT_MULTI_APP_STATUS_SURVEY"

.field public static final LOGGING_STATUS_SURVEY_ACTION:Ljava/lang/String; = "com.samsung.android.providers.context.log.action.REPORT_APP_STATUS_SURVEY"

.field public static final MENU_EDIT:I = 0x0

.field public static final MENU_EDIT_NONE:I = 0x3

.field public static final MENU_EDIT_STUDIO:I = 0x1

.field public static final MENU_EDIT_VIDEOEDITOR:I = 0x2

.field public static final MINI_PLAYER_ACTION:Ljava/lang/String; = "com.samsung.action.MINI_MODE_SERVICE"

.field public static final MOVE_TO:I = 0x0

.field public static final MOVE_TO_PRIVATE:I = 0x0

.field public static final OPENING_BRACE:Ljava/lang/String; = "("

.field public static final PACKAGE_ALLSHARE_MEDIASERVER:Ljava/lang/String; = "com.samsung.android.nearby.mediaserver"

.field public static final PACKAGE_GALAXY_FINDER:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder"

.field public static final PACKAGE_SAMSUNG_APPS:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final PACKAGE_STUDIO:Ljava/lang/String; = "com.sec.android.mimage.sstudio"

.field public static final PACKAGE_S_LINK:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink"

.field public static final PACKAGE_VIDEO_CLIP_STUDIO:Ljava/lang/String; = "com.sec.android.app.storycam"

.field public static final PACKAGE_VIDEO_MAKER:Ljava/lang/String; = "com.sec.android.app.ve"

.field public static final PACKAGE_VIDEO_MAKER_FULL:Ljava/lang/String; = "com.sec.android.app.vefull"

.field public static final PACKAGE_VIDEO_PLAYER:Ljava/lang/String; = "com.sec.android.app.videoplayer"

.field public static final REMOVE_FROM:I = 0x1

.field public static final REMOVE_FROM_PRIVATE:I = 0x1

.field public static final SAMSUNG_APPS_DOWNLOAD_URL:Ljava/lang/String; = "http://apps.samsung.com/mw/apps311.as"

.field public static final SEARCH_KEY:Ljava/lang/String; = "search_key"

.field public static final SEASON_ID:Ljava/lang/String; = "parent_product_id"

.field public static final SELECTMODE_DELETE:I = 0x1

.field public static final SELECTMODE_NONE:I = 0x0

.field public static final SERVICE_MINI_PLAYER:Ljava/lang/String; = ".miniapp.MiniVideoPlayerService"

.field public static final SORTBY_ALLOWED_ON_ALLSHARE:I = 0x2

.field public static final SORTBY_DATE:I = 0x1

.field public static final SORTBY_LAST:I = 0x0

.field public static final SORTBY_NAME:I = 0x2

.field public static final SORTBY_RECENTLY_VIEWED:I = 0x0

.field public static final SORTBY_SIZE:I = 0x3

.field public static final SORTBY_TYPE:I = 0x4

.field public static final SORT_BY:Ljava/lang/String; = "sortby"

.field public static final SPACE:Ljava/lang/String; = " "

.field public static final STUDIO_MAX_ITEM_COUNT:I = 0xf

.field public static final S_LINK_DEVICE_ID:Ljava/lang/String; = "s_link_device_id"

.field public static final S_LINK_FILE_LIST_TITLE:Ljava/lang/String; = "s_link_file_list_title"

.field public static final TAB_ID_DEVICES:I = 0x1

.field public static final TAB_ID_PERSONAL:I = 0x0

.field public static final VIEW_ALL:I = 0x0

.field public static final VIEW_CONTENT_IN_DROPBOX:I = 0x2

.field public static final VIEW_CONTENT_IN_PHONE:I = 0x1

.field public static final VIEW_CONTENT_IN_TCLOUD:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/Const;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    return-void
.end method
