.class Lcom/samsung/everglades/video/myvideo/common/DB$VideoPlayerDB;
.super Ljava/lang/Object;
.source "DB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/DB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VideoPlayerDB"
.end annotation


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.videoplayer.provider"

.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final LAST_PLAYED_URI:Landroid/net/Uri;

.field private static final PROJECTION_DATA:[Ljava/lang/String;

.field private static final SCHEME:Ljava/lang/String; = "content://"

.field private static final VIDEO_LASTPLAYEDITEM_TABLE:Ljava/lang/String; = "video_lastplayitem"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 858
    const-string v0, "content://com.sec.android.app.videoplayer.provider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB$VideoPlayerDB;->CONTENT_URI:Landroid/net/Uri;

    .line 862
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/DB$VideoPlayerDB;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "video_lastplayitem"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB$VideoPlayerDB;->LAST_PLAYED_URI:Landroid/net/Uri;

    .line 864
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DB$VideoPlayerDB;->PROJECTION_DATA:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLastPlayedFromVideoPlayerDB(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 9
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v3, 0x0

    .line 869
    const/4 v8, 0x0

    .line 871
    .local v8, "path":Ljava/lang/String;
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB$VideoPlayerDB;->LAST_PLAYED_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/DB$VideoPlayerDB;->PROJECTION_DATA:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 872
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 874
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 875
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 879
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 883
    :cond_0
    :goto_0
    return-object v8

    .line 876
    :catch_0
    move-exception v7

    .line 877
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 879
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method
