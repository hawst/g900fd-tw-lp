.class public Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;
.super Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;
.source "SLinkFileList.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;-><init>()V

    return-void
.end method

.method private getTitles()Ljava/lang/String;
    .locals 3

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 52
    .local v0, "args":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 54
    .local v1, "title":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 55
    const-string v2, "s_link_file_list_title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    :cond_0
    return-object v1
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 62
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 64
    const/16 v3, 0x64

    if-ne p1, v3, :cond_0

    .line 65
    if-nez p3, :cond_1

    .line 66
    const-string v3, "SLinkFileList - onActivityResult() share via download cancel"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    .line 72
    .local v1, "downloadUriList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 75
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 76
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 77
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v3, "video/*"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string v4, "android.intent.extra.STREAM"

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 85
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c005b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 81
    :cond_2
    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    const-string v3, "video/*"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;->getTitles()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;->setTitle(Ljava/lang/String;)V

    .line 22
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;->setupView()V

    .line 23
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->onPause()V

    .line 34
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->releaseWakeLock()V

    .line 35
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->onResume()V

    .line 28
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->acquireWakeLock(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method protected setupView()V
    .locals 0

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;->updateContentFragment()V

    .line 40
    return-void
.end method

.method protected updateContentFragment()V
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/activities/SLinkFileList;->setFragment(Landroid/os/Bundle;)V

    .line 48
    :cond_0
    return-void
.end method
