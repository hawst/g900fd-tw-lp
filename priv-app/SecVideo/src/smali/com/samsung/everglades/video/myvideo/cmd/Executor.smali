.class public Lcom/samsung/everglades/video/myvideo/cmd/Executor;
.super Ljava/lang/Object;
.source "Executor.java"


# static fields
.field public static final CREATE_OPTIONS_MENU_CMD:I = 0x7

.field public static final DEVICES_DOWNLOAD_CMD:I = 0x11

.field public static final MENU_DELETE_CMD:I = 0x12

.field public static final OPEN_FOLDER_CMD:I = 0xa

.field public static final PLAY_ASF_VIDEO_CMD:I = 0x4

.field public static final PLAY_S_LINK_VIDEO_CMD:I = 0x5

.field public static final PLAY_VIDEO_CMD:I = 0x1

.field public static final PLAY_VIDEO_ON_SEARCH_CMD:I = 0x6

.field public static final PREPARE_OPTIONS_MENU_CMD:I = 0x8

.field public static final SCAN_FOR_NEABY_DEVICES_CMD:I = 0xf

.field public static final SHORT_CUT_TO_HOME_CMD:I = 0x10

.field public static final SINGLE_ITEM_SELECT_CMD:I = 0xe

.field private static mUniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/Executor;


# instance fields
.field private mMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/everglades/video/myvideo/cmd/ICommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->createCommandMap()V

    .line 33
    return-void
.end method

.method private createCommandMap()V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    .line 63
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/cmd/PlayVideoCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 64
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    const/4 v1, 0x4

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/PlayASFVideoCmd;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/cmd/PlayASFVideoCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 65
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    const/4 v1, 0x5

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/PlaySLinkVideoCmd;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/cmd/PlaySLinkVideoCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    const/4 v1, 0x7

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/MenuCreateOptionsMenuCmd;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuCreateOptionsMenuCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 68
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    const/16 v1, 0x8

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/cmd/MenuPrepareOptionsMenuCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    const/16 v1, 0xa

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/OpenFolderCmd;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/cmd/OpenFolderCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    const/16 v1, 0xe

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/cmd/SingleSelectCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    const/16 v1, 0xf

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/ScanForNearbyDevicesCmd;

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/cmd/ScanForNearbyDevicesCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 75
    :cond_0
    return-void
.end method

.method public static destroyInstance()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    sget-object v0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 54
    sget-object v0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    iput-object v1, v0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    .line 55
    sput-object v1, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    .line 57
    :cond_0
    return-void
.end method

.method public static execute(ILcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 1
    .param p0, "cmdNum"    # I
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 47
    invoke-static {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->setMenuItemIdIfNeeded(ILcom/samsung/everglades/video/myvideo/cmd/Args;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object p1

    .line 48
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->get()Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    move-result-object v0

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->getCommand(I)Lcom/samsung/everglades/video/myvideo/cmd/ICommand;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/ICommand;->execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 49
    return-void
.end method

.method public static execute(Lcom/samsung/everglades/video/myvideo/common/ListType;Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 1
    .param p0, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 43
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->get()Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    move-result-object v0

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->getCommand(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/ICommand;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/ICommand;->execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 44
    return-void
.end method

.method private static get()Lcom/samsung/everglades/video/myvideo/cmd/Executor;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;-><init>()V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    .line 39
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/Executor;

    return-object v0
.end method

.method private getCommand(I)Lcom/samsung/everglades/video/myvideo/cmd/ICommand;
    .locals 3
    .param p1, "cmd"    # I

    .prologue
    .line 97
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->mMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/everglades/video/myvideo/cmd/ICommand;

    .line 99
    .local v0, "command":Lcom/samsung/everglades/video/myvideo/cmd/ICommand;
    if-nez v0, :cond_0

    .line 100
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->getInstance()Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    move-result-object v0

    .line 103
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Executor getCommand() command:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 105
    return-object v0
.end method

.method private getCommand(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/ICommand;
    .locals 2
    .param p1, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    const/16 v0, 0xe

    .line 93
    .local v0, "cmd":I
    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Executor;->getCommand(I)Lcom/samsung/everglades/video/myvideo/cmd/ICommand;

    move-result-object v1

    return-object v1

    .line 83
    .end local v0    # "cmd":I
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 84
    const/16 v0, 0xa

    .restart local v0    # "cmd":I
    goto :goto_0

    .line 85
    .end local v0    # "cmd":I
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    const/4 v0, 0x4

    .restart local v0    # "cmd":I
    goto :goto_0

    .line 87
    .end local v0    # "cmd":I
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 88
    const/4 v0, 0x5

    .restart local v0    # "cmd":I
    goto :goto_0

    .line 90
    .end local v0    # "cmd":I
    :cond_3
    const/4 v0, 0x1

    .restart local v0    # "cmd":I
    goto :goto_0
.end method

.method private static setMenuItemIdIfNeeded(ILcom/samsung/everglades/video/myvideo/cmd/Args;)Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .locals 1
    .param p0, "cmd"    # I
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 109
    packed-switch p0, :pswitch_data_0

    .line 115
    .end local p1    # "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    :goto_0
    return-object p1

    .line 111
    .restart local p1    # "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    :pswitch_0
    const v0, 0x7f090071

    invoke-virtual {p1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object p1

    goto :goto_0

    .line 113
    :pswitch_1
    const v0, 0x7f09007c

    invoke-virtual {p1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    move-result-object p1

    goto :goto_0

    .line 109
    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
