.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 995
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 997
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mListHoverListener getAction : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 998
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-ne v1, v5, :cond_0

    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v1, :cond_0

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1300()Z

    move-result v1

    if-nez v1, :cond_0

    .line 999
    const-string v1, "ThumbHoverViewer - initViews() MODEL_T && TOOL_TYPE_STYLUS"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1000
    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1302(Z)Z

    .line 1002
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1062
    :cond_1
    :goto_0
    :pswitch_0
    return v3

    .line 1004
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mActionHoverEnter:Z
    invoke-static {v1, v4}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2902(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Z)Z

    .line 1005
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1007
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1009
    :cond_2
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-ne v1, v5, :cond_3

    .line 1011
    const/16 v1, 0xa

    const/4 v2, -0x1

    :try_start_0
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1017
    :cond_3
    :goto_1
    const-string v1, "ThumbHoverViewer - onHover() ENTER"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1018
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    const/16 v2, 0x190

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->sendDelayedMessage(I)V
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$3000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;I)V

    .line 1019
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;
    invoke-static {v1, p1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$3102(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Landroid/view/View;)Landroid/view/View;

    goto :goto_0

    .line 1012
    :catch_0
    move-exception v0

    .line 1013
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 1024
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mActionHoverEnter:Z
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2900(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1025
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mActionHoverEnter:Z
    invoke-static {v1, v4}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2902(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Z)Z

    .line 1026
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1028
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1030
    :cond_4
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-ne v1, v5, :cond_5

    .line 1032
    const/16 v1, 0xa

    const/4 v2, -0x1

    :try_start_1
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1038
    :cond_5
    :goto_2
    const-string v1, "ThumbHoverViewer - onHover() MOVE"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1039
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    const/16 v2, 0x190

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->sendDelayedMessage(I)V
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$3000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;I)V

    .line 1040
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mHoverView:Landroid/view/View;
    invoke-static {v1, p1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$3102(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Landroid/view/View;)Landroid/view/View;

    goto/16 :goto_0

    .line 1033
    :catch_1
    move-exception v0

    .line 1034
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 1046
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # setter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mActionHoverEnter:Z
    invoke-static {v1, v3}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2902(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;Z)Z

    .line 1047
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1050
    const/4 v1, 0x1

    const/4 v2, -0x1

    :try_start_2
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1054
    :goto_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$12;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->removeDelayedMessage()V

    .line 1055
    const-string v1, "ThumbHoverViewer - onHover() EXIT "

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1051
    :catch_2
    move-exception v0

    .line 1052
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 1002
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
