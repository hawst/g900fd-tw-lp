.class Lcom/samsung/everglades/video/VideoMain$2;
.super Landroid/support/v13/app/FragmentStatePagerAdapter;
.source "VideoMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/VideoMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/VideoMain;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/VideoMain;Landroid/app/FragmentManager;)V
    .locals 0
    .param p2, "x0"    # Landroid/app/FragmentManager;

    .prologue
    .line 258
    iput-object p1, p0, Lcom/samsung/everglades/video/VideoMain$2;->this$0:Lcom/samsung/everglades/video/VideoMain;

    invoke-direct {p0, p2}, Landroid/support/v13/app/FragmentStatePagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 261
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isNoSupportTab()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    const/4 v0, 0x1

    .line 264
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getItem(I)Landroid/app/Fragment;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 270
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getItem() : position = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 271
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isNoSupportTab()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;-><init>()V

    .line 285
    :goto_0
    return-object v1

    .line 274
    :cond_0
    if-nez p1, :cond_3

    .line 275
    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    if-nez v1, :cond_2

    .line 276
    iget-object v1, p0, Lcom/samsung/everglades/video/VideoMain$2;->this$0:Lcom/samsung/everglades/video/VideoMain;

    # getter for: Lcom/samsung/everglades/video/VideoMain;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;
    invoke-static {v1}, Lcom/samsung/everglades/video/VideoMain;->access$100(Lcom/samsung/everglades/video/VideoMain;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "lastViewAs"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    .line 277
    .local v0, "lt":I
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 278
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->newInstance()Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    move-result-object v1

    goto :goto_0

    .line 280
    :cond_1
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->newInstance()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v1

    goto :goto_0

    .line 283
    .end local v0    # "lt":I
    :cond_2
    sget-object v1, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    goto :goto_0

    .line 285
    :cond_3
    new-instance v1, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-direct {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;-><init>()V

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 292
    const/4 v0, -0x2

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 301
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "instantiateItem() : position = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 302
    invoke-super {p0, p1, p2}, Landroid/support/v13/app/FragmentStatePagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .line 303
    .local v0, "fg":Landroid/app/Fragment;
    if-nez p2, :cond_0

    .line 304
    sput-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .line 306
    .end local v0    # "fg":Landroid/app/Fragment;
    :goto_0
    return-object v0

    .restart local v0    # "fg":Landroid/app/Fragment;
    :cond_0
    # setter for: Lcom/samsung/everglades/video/VideoMain;->mDeviceFragment:Landroid/app/Fragment;
    invoke-static {v0}, Lcom/samsung/everglades/video/VideoMain;->access$202(Landroid/app/Fragment;)Landroid/app/Fragment;

    move-result-object v0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 297
    invoke-super {p0}, Landroid/support/v13/app/FragmentStatePagerAdapter;->notifyDataSetChanged()V

    .line 298
    return-void
.end method
