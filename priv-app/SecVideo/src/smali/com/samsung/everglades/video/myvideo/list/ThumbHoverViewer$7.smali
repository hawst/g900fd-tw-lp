.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$7;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->playVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 488
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v2, 0x0

    .line 490
    sparse-switch p2, :sswitch_data_0

    .line 511
    :goto_0
    :sswitch_0
    return v2

    .line 497
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->noVideoView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1800(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 498
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->noVideoView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1800(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v0

    const-string v1, "#252628"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 499
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->noVideoView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1800(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 506
    :sswitch_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSurfaceView:Landroid/view/SurfaceView;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1900(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/view/SurfaceView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 490
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_2
        0x2bc -> :sswitch_0
        0x320 -> :sswitch_0
        0x321 -> :sswitch_0
        0x3b6 -> :sswitch_0
        0x3b7 -> :sswitch_1
    .end sparse-switch
.end method
