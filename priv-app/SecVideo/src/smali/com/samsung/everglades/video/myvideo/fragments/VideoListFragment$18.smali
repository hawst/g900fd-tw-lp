.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$18;
.super Ljava/lang/Object;
.source "VideoListFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/animator/TwAddDeleteGridAnimator$OnAddDeleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 1872
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$18;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdd()V
    .locals 0

    .prologue
    .line 1888
    return-void
.end method

.method public onAnimationEnd(Z)V
    .locals 0
    .param p1, "isAddAnimation"    # Z

    .prologue
    .line 1894
    return-void
.end method

.method public onAnimationStart(Z)V
    .locals 0
    .param p1, "isAddAnimation"    # Z

    .prologue
    .line 1891
    return-void
.end method

.method public onDelete()V
    .locals 4

    .prologue
    .line 1875
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$18;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    .line 1876
    .local v2, "ms":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    if-eqz v2, :cond_0

    .line 1877
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getMultiDelete()Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    move-result-object v1

    .line 1878
    .local v1, "md":Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
    if-eqz v1, :cond_0

    .line 1879
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->getDeleteFilesThread()Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    move-result-object v0

    .line 1880
    .local v0, "dft":Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    if-eqz v0, :cond_0

    .line 1881
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->start()V

    .line 1885
    .end local v0    # "dft":Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    .end local v1    # "md":Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
    :cond_0
    return-void
.end method
