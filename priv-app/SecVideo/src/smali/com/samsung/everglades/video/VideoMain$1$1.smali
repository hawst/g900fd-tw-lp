.class Lcom/samsung/everglades/video/VideoMain$1$1;
.super Ljava/lang/Object;
.source "VideoMain.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/VideoMain$1;->onPageSelected(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/VideoMain$1;

.field final synthetic val$positionTemp:I


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/VideoMain$1;I)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->this$1:Lcom/samsung/everglades/video/VideoMain$1;

    iput p2, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->val$positionTemp:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 221
    iget-object v2, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->this$1:Lcom/samsung/everglades/video/VideoMain$1;

    iget-object v2, v2, Lcom/samsung/everglades/video/VideoMain$1;->this$0:Lcom/samsung/everglades/video/VideoMain;

    # getter for: Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;
    invoke-static {v2}, Lcom/samsung/everglades/video/VideoMain;->access$000(Lcom/samsung/everglades/video/VideoMain;)Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 222
    iget-object v2, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->this$1:Lcom/samsung/everglades/video/VideoMain$1;

    iget-object v2, v2, Lcom/samsung/everglades/video/VideoMain$1;->this$0:Lcom/samsung/everglades/video/VideoMain;

    # getter for: Lcom/samsung/everglades/video/VideoMain;->mTabChangeListener:Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;
    invoke-static {v2}, Lcom/samsung/everglades/video/VideoMain;->access$000(Lcom/samsung/everglades/video/VideoMain;)Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;

    move-result-object v2

    iget v3, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->val$positionTemp:I

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/view/TabChangeListener;->onTabChangedByViewPager(I)V

    .line 224
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->this$1:Lcom/samsung/everglades/video/VideoMain$1;

    iget-object v2, v2, Lcom/samsung/everglades/video/VideoMain$1;->this$0:Lcom/samsung/everglades/video/VideoMain;

    iget v3, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->val$positionTemp:I

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/VideoMain;->updateTabMenu(I)V

    .line 226
    iget-object v2, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->this$1:Lcom/samsung/everglades/video/VideoMain$1;

    iget-object v2, v2, Lcom/samsung/everglades/video/VideoMain$1;->this$0:Lcom/samsung/everglades/video/VideoMain;

    # getter for: Lcom/samsung/everglades/video/VideoMain;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;
    invoke-static {v2}, Lcom/samsung/everglades/video/VideoMain;->access$100(Lcom/samsung/everglades/video/VideoMain;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "sortorder"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    .line 227
    .local v1, "sort_option":I
    iget-object v2, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->this$1:Lcom/samsung/everglades/video/VideoMain$1;

    iget-object v2, v2, Lcom/samsung/everglades/video/VideoMain$1;->this$0:Lcom/samsung/everglades/video/VideoMain;

    # getter for: Lcom/samsung/everglades/video/VideoMain;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;
    invoke-static {v2}, Lcom/samsung/everglades/video/VideoMain;->access$100(Lcom/samsung/everglades/video/VideoMain;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "slinksortorder"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    .line 228
    .local v0, "slink_option":I
    if-eq v1, v0, :cond_1

    .line 229
    iget-object v2, p0, Lcom/samsung/everglades/video/VideoMain$1$1;->this$1:Lcom/samsung/everglades/video/VideoMain$1;

    iget-object v2, v2, Lcom/samsung/everglades/video/VideoMain$1;->this$0:Lcom/samsung/everglades/video/VideoMain;

    # getter for: Lcom/samsung/everglades/video/VideoMain;->mPref:Lcom/samsung/everglades/video/myvideo/common/Pref;
    invoke-static {v2}, Lcom/samsung/everglades/video/VideoMain;->access$100(Lcom/samsung/everglades/video/VideoMain;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "sortorder"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 230
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->callOptionChanged()V

    .line 232
    :cond_1
    return-void
.end method
