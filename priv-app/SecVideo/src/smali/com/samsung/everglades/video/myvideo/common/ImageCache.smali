.class public Lcom/samsung/everglades/video/myvideo/common/ImageCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;,
        Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;,
        Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;,
        Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;,
        Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;
    }
.end annotation


# static fields
.field private static final LEAST_SIZE_OF_DOWN_SCALE:I = 0x1e000

.field private static final MAX_THREADS:I

.field private static REFERENCE_LENGTH_OF_BITMAP:I

.field private static mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private static mContext:Landroid/content/Context;

.field private static mOnDeviceListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;

.field private static mOnFolderListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;

.field private static mOnVideoListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;

.field private static mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

.field private static mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ImageCache;


# instance fields
.field private mBitmapWorker:Ljava/lang/Runnable;

.field private mThreadPool:Ljava/util/concurrent/ExecutorService;

.field private final userAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sput v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->MAX_THREADS:I

    .line 34
    const/16 v0, 0x100

    sput v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$1;-><init>(Lcom/samsung/everglades/video/myvideo/common/ImageCache;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mBitmapWorker:Ljava/lang/Runnable;

    .line 377
    const-string v0, "asf-dms-icon"

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->userAgent:Ljava/lang/String;

    .line 55
    return-void
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnVideoListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;

    return-object v0
.end method

.method static synthetic access$200()Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnDeviceListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;

    return-object v0
.end method

.method static synthetic access$300()Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnFolderListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;

    return-object v0
.end method

.method static synthetic access$400()Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/common/ImageCache;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ImageCache;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->downScaler(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/common/ImageCache;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ImageCache;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->downloadBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700()Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/common/ImageCache;J)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ImageCache;
    .param p1, "x1"    # J

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getSLinkBitmap(J)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/common/ImageCache;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/ImageCache;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getLocalBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private downScaler(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 442
    if-nez p1, :cond_0

    .line 443
    const-string v4, "downScaler() bitmap is null"

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 444
    const/4 v2, 0x0

    .line 475
    :goto_0
    return-object v2

    .line 447
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v4

    const v5, 0x1e000

    if-le v4, v5, :cond_3

    .line 448
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v3, v4

    .line 449
    .local v3, "width":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v0, v4

    .line 451
    .local v0, "height":F
    const/4 v1, 0x0

    .line 452
    .local v1, "ratio":F
    cmpl-float v4, v3, v0

    if-lez v4, :cond_2

    .line 453
    sget v4, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-lez v4, :cond_1

    .line 454
    sget v4, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 455
    div-float v4, v0, v1

    float-to-int v4, v4

    int-to-float v0, v4

    .line 456
    sget v4, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    int-to-float v3, v4

    .line 466
    :cond_1
    :goto_1
    const/4 v4, 0x0

    cmpl-float v4, v1, v4

    if-lez v4, :cond_3

    .line 467
    float-to-int v4, v3

    float-to-int v5, v0

    const/4 v6, 0x1

    invoke-static {p1, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 468
    .local v2, "resized":Landroid/graphics/Bitmap;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ImageCache - downScaler() SAVED SIZE:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-long v6, v6

    invoke-static {v5, v6, v7}, Lcom/samsung/everglades/video/myvideo/common/Utils;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 471
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 459
    .end local v2    # "resized":Landroid/graphics/Bitmap;
    :cond_2
    sget v4, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    int-to-float v4, v4

    cmpl-float v4, v0, v4

    if-lez v4, :cond_1

    .line 460
    sget v4, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    int-to-float v4, v4

    div-float v1, v0, v4

    .line 461
    div-float v4, v3, v1

    float-to-int v4, v4

    int-to-float v3, v4

    .line 462
    sget v4, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    int-to-float v0, v4

    goto :goto_1

    .end local v0    # "height":F
    .end local v1    # "ratio":F
    .end local v3    # "width":F
    :cond_3
    move-object v2, p1

    .line 475
    goto :goto_0
.end method

.method private downloadBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 380
    if-nez p1, :cond_1

    move-object v0, v8

    .line 438
    :cond_0
    :goto_0
    return-object v0

    .line 383
    :cond_1
    const/4 v0, 0x0

    .line 384
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    .line 385
    .local v4, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    const/4 v6, 0x0

    .line 386
    .local v6, "inputStream":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 387
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    const/4 v1, 0x0

    .line 390
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    :try_start_0
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .local v5, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :try_start_1
    const-string v9, "asf-dms-icon"

    invoke-static {v9}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 401
    invoke-virtual {v1, v5}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    .line 403
    .local v7, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    const/16 v10, 0xc8

    if-eq v9, v10, :cond_5

    .line 404
    const-class v9, Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " while retrieving bitmap from "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 423
    if-eqz v6, :cond_2

    .line 424
    :try_start_2
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 427
    :cond_2
    if-eqz v3, :cond_3

    .line 428
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 431
    :cond_3
    if-eqz v1, :cond_4

    .line 432
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClient;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_4
    :goto_1
    move-object v0, v8

    .line 436
    goto :goto_0

    .line 434
    :catch_0
    move-exception v2

    .line 435
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 410
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_5
    :try_start_3
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 412
    if-eqz v3, :cond_6

    .line 413
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    .line 414
    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 423
    :cond_6
    if-eqz v6, :cond_7

    .line 424
    :try_start_4
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 427
    :cond_7
    if-eqz v3, :cond_8

    .line 428
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 431
    :cond_8
    if-eqz v1, :cond_9

    .line 432
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClient;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_9
    move-object v4, v5

    .line 436
    .end local v5    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto/16 :goto_0

    .line 434
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v5    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :catch_1
    move-exception v2

    .line 435
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v4, v5

    .line 437
    .end local v5    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto/16 :goto_0

    .line 416
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v7    # "response":Lorg/apache/http/HttpResponse;
    :catch_2
    move-exception v2

    .line 417
    .restart local v2    # "e":Ljava/lang/Exception;
    :goto_2
    if-eqz v4, :cond_a

    .line 418
    :try_start_5
    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 420
    :cond_a
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 423
    if-eqz v6, :cond_b

    .line 424
    :try_start_6
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 427
    :cond_b
    if-eqz v3, :cond_c

    .line 428
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 431
    :cond_c
    if-eqz v1, :cond_0

    .line 432
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClient;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 434
    :catch_3
    move-exception v2

    .line 435
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 422
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    .line 423
    :goto_3
    if-eqz v6, :cond_d

    .line 424
    :try_start_7
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 427
    :cond_d
    if-eqz v3, :cond_e

    .line 428
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 431
    :cond_e
    if-eqz v1, :cond_f

    .line 432
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClient;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 436
    :cond_f
    :goto_4
    throw v8

    .line 434
    :catch_4
    move-exception v2

    .line 435
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 422
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v5    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :catchall_1
    move-exception v8

    move-object v4, v5

    .end local v5    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_3

    .line 416
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v5    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :catch_5
    move-exception v2

    move-object v4, v5

    .end local v5    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_2
.end method

.method public static getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 85
    if-nez p0, :cond_1

    move-object v0, v1

    .line 96
    :cond_0
    :goto_0
    return-object v0

    .line 88
    :cond_1
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 89
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 92
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->QueueInProcessContain(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 93
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$Queue;->put(Ljava/lang/String;)V

    .line 94
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getInstance()Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    move-result-object v2

    invoke-direct {v2}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->startWorkerThread()V

    :cond_2
    move-object v0, v1

    .line 96
    goto :goto_0
.end method

.method private static getInstance()Lcom/samsung/everglades/video/myvideo/common/ImageCache;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;-><init>()V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    .line 61
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_LARGE_THUMBNAIL:Z

    if-eqz v0, :cond_1

    .line 62
    const/16 v0, 0x300

    sput v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    .line 68
    :cond_0
    :goto_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    return-object v0

    .line 64
    :cond_1
    const/16 v0, 0x100

    sput v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    goto :goto_0
.end method

.method private getLocalBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 274
    const/4 v5, -0x1

    .line 275
    .local v5, "time":I
    const-wide/16 v2, -0x1

    .line 276
    .local v2, "duration":J
    const/4 v0, 0x0

    .line 277
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 279
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v4, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 281
    const/16 v6, 0x9

    invoke-virtual {v4, v6}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 284
    const-wide/16 v6, 0x3a98

    cmp-long v6, v2, v6

    if-ltz v6, :cond_1

    .line 285
    const v5, 0xe4e1c0

    .line 290
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v6

    if-nez v6, :cond_2

    .line 291
    invoke-direct {p0, v4}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setVideoDownSizeWithoutVW(Landroid/media/MediaMetadataRetriever;)V

    .line 295
    :goto_1
    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 303
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 306
    :goto_2
    return-object v0

    .line 286
    :cond_1
    const-wide/16 v6, 0x3e8

    cmp-long v6, v2, v6

    if-ltz v6, :cond_0

    .line 287
    const v5, 0xf4240

    goto :goto_0

    .line 293
    :cond_2
    :try_start_1
    invoke-direct {p0, v4}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->setVideoDownSize(Landroid/media/MediaMetadataRetriever;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 296
    :catch_0
    move-exception v1

    .line 297
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 303
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_2

    .line 298
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 299
    .local v1, "e":Ljava/lang/IllegalStateException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 303
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_2

    .line 300
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 301
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 303
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_2

    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v6
.end method

.method private getSLinkBitmap(J)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "id"    # J

    .prologue
    const/4 v8, 0x0

    .line 369
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 370
    .local v10, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, v10, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 372
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    sget v6, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    sget v7, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    move-wide v2, p1

    move v9, v8

    invoke-static/range {v1 .. v10}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JJIIZZLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private isRotatedMedia(Landroid/media/MediaMetadataRetriever;)Z
    .locals 4
    .param p1, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/4 v2, 0x0

    .line 350
    const/16 v3, 0x18

    invoke-virtual {p1, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    .line 352
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .end local v1    # "value":Ljava/lang/String;
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 354
    .local v0, "rotate":I
    sparse-switch v0, :sswitch_data_0

    .line 364
    :goto_1
    :sswitch_0
    return v2

    .line 352
    .end local v0    # "rotate":I
    .restart local v1    # "value":Ljava/lang/String;
    :cond_0
    const-string v1, "0"

    goto :goto_0

    .line 361
    .end local v1    # "value":Ljava/lang/String;
    .restart local v0    # "rotate":I
    :sswitch_1
    const/4 v2, 0x1

    goto :goto_1

    .line 354
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_0
        0x10e -> :sswitch_1
    .end sparse-switch
.end method

.method public static remove(Ljava/lang/String;)V
    .locals 0
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 101
    if-nez p0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/ImageCache$MemCache;->remove(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 73
    sput-object p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;

    .line 75
    :cond_0
    return-void
.end method

.method public static setDevcieListOnUpdatedListener(Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;)V
    .locals 0
    .param p0, "l"    # Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;

    .prologue
    .line 491
    sput-object p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnDeviceListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnDevcieListUpdatedListener;

    .line 492
    return-void
.end method

.method public static setFolderListUpdatedListener(Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;)V
    .locals 0
    .param p0, "l"    # Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;

    .prologue
    .line 499
    sput-object p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnFolderListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnFolderListUpdatedListener;

    .line 500
    return-void
.end method

.method private setVideoDownSize(Landroid/media/MediaMetadataRetriever;)V
    .locals 4
    .param p1, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/4 v3, 0x1

    .line 310
    const/4 v0, -0x1

    .line 312
    .local v0, "height":I
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->isRotatedMedia(Landroid/media/MediaMetadataRetriever;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 313
    sget v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallHeight:I

    .line 314
    .local v1, "width":I
    sget v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallWidth:I

    .line 319
    :goto_0
    invoke-virtual {p1, v1, v0, v3, v3}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 320
    return-void

    .line 316
    .end local v1    # "width":I
    :cond_0
    sget v1, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallWidth:I

    .line 317
    .restart local v1    # "width":I
    sget v0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallHeight:I

    goto :goto_0
.end method

.method private setVideoDownSizeWithoutVW(Landroid/media/MediaMetadataRetriever;)V
    .locals 6
    .param p1, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/4 v5, 0x1

    .line 323
    const/high16 v1, -0x40800000    # -1.0f

    .line 324
    .local v1, "ratio":F
    const/4 v0, -0x1

    .line 325
    .local v0, "height":I
    const/16 v3, 0x12

    invoke-virtual {p1, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 327
    .local v2, "width":I
    const/16 v3, 0x13

    invoke-virtual {p1, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 330
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->isRotatedMedia(Landroid/media/MediaMetadataRetriever;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 331
    sget v3, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    div-int/lit8 v2, v3, 0x3

    .line 332
    sget v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    .line 346
    :cond_0
    :goto_0
    invoke-virtual {p1, v2, v0, v5, v5}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 347
    return-void

    .line 334
    :cond_1
    if-le v2, v0, :cond_2

    .line 335
    sget v3, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    if-le v2, v3, :cond_0

    .line 336
    int-to-float v3, v2

    sget v4, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 337
    int-to-float v3, v0

    div-float/2addr v3, v1

    float-to-int v0, v3

    .line 338
    sget v2, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    goto :goto_0

    .line 341
    :cond_2
    sget v3, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    div-int/lit8 v0, v3, 0x3

    .line 342
    sget v2, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->REFERENCE_LENGTH_OF_BITMAP:I

    goto :goto_0
.end method

.method public static setVideoListOnUpdatedListener(Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;)V
    .locals 0
    .param p0, "l"    # Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;

    .prologue
    .line 483
    sput-object p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mOnVideoListUpdatedListener:Lcom/samsung/everglades/video/myvideo/common/ImageCache$OnVideoListUpdatedListener;

    .line 484
    return-void
.end method

.method public static shutdownThreads()V
    .locals 2

    .prologue
    .line 78
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getInstance()Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    .line 79
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getInstance()Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 80
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getInstance()Lcom/samsung/everglades/video/myvideo/common/ImageCache;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 82
    :cond_0
    return-void
.end method

.method private startWorkerThread()V
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-nez v0, :cond_0

    .line 108
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 110
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    if-nez v0, :cond_1

    .line 111
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_2

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MAX_THREADS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->MAX_THREADS:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 116
    sget v0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->MAX_THREADS:I

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->mBitmapWorker:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 120
    return-void
.end method
