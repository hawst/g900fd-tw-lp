.class Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$19;
.super Landroid/content/BroadcastReceiver;
.source "VideoListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)V
    .locals 0

    .prologue
    .line 1993
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$19;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1996
    const-string v0, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1997
    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnPrivateModeListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1800()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1998
    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mOnPrivateModeListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1800()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;->onUpdate()V

    .line 2000
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$19;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2001
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$19;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->mLiveThumbnail:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->access$1700(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;)Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->cancelCurrentTransJob()V

    .line 2003
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$19;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    .line 2005
    :cond_2
    return-void
.end method
