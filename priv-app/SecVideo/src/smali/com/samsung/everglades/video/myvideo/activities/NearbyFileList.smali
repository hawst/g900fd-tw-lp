.class public Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;
.super Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;
.source "NearbyFileList.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;
.implements Ljava/util/Observer;


# instance fields
.field private mAsfErrorDialog:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

.field private mDmsName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;-><init>()V

    return-void
.end method

.method private getDeviceTitle(Lcom/samsung/android/allshare/media/Provider;)Ljava/lang/String;
    .locals 1
    .param p1, "deviceProvider"    # Lcom/samsung/android/allshare/media/Provider;

    .prologue
    .line 56
    if-eqz p1, :cond_0

    .line 57
    invoke-virtual {p1}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mDmsName:Ljava/lang/String;

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mDmsName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public OnAllShareStateChanged(II)V
    .locals 2
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 74
    const/16 v0, 0xf

    if-ne p1, v0, :cond_1

    .line 75
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mAsfErrorDialog:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    .line 76
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mAsfErrorDialog:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    invoke-virtual {v0, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->addObserver(Ljava/util/Observer;)V

    .line 77
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mAsfErrorDialog:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mDmsName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->show(Ljava/lang/String;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->notifyChanged(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v0

    .line 24
    .local v0, "asfUtil":Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    invoke-virtual {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->setContextForUI(Landroid/content/Context;)V

    .line 25
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getSelectedRemoteProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v1

    .line 27
    .local v1, "deviceProvider":Lcom/samsung/android/allshare/media/Provider;
    if-eqz v1, :cond_1

    .line 28
    invoke-virtual {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->setOnAllShareDeviceChangedObserver(Lcom/samsung/everglades/video/myvideo/common/ASFUtil$OnAllShareDeviceChangedObserver;)V

    .line 30
    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->getDeviceTitle(Lcom/samsung/android/allshare/media/Provider;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->setTitle(Ljava/lang/String;)V

    .line 32
    if-nez p1, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->setupView()V

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->finish()V

    goto :goto_0
.end method

.method protected setupView()V
    .locals 0

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->updateContentFragment()V

    .line 43
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 1
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mAsfErrorDialog:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mAsfErrorDialog:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    invoke-virtual {v0, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->deleteObserver(Ljava/util/Observer;)V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->mAsfErrorDialog:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->finish()V

    .line 70
    return-void
.end method

.method protected updateContentFragment()V
    .locals 3

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 48
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 49
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "args":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_0
    const-string v1, "list_type"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 52
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;->setFragment(Landroid/os/Bundle;)V

    .line 53
    return-void
.end method
