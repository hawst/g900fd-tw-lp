.class public Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
.super Ljava/lang/Object;
.source "DeleteOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;,
        Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    }
.end annotation


# static fields
.field private static final ABORT_DELETE_PROGRESS:I = 0x2

.field private static final CLEAR_KEEP_SCREEN_ON:I = 0x4

.field private static final FINISH_DELETE_PROGRESS:I = 0x1

.field private static final SET_KEEP_SCREEN_ON:I = 0x3

.field private static final SHOW_DELETE_PROGRESS:I

.field public static popupCreated:Z


# instance fields
.field private mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDeleteFilesDialog:Landroid/app/AlertDialog;

.field private mDeleteFilesThread:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

.field private final mHandler:Landroid/os/Handler;

.field mOnContentChangedListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;

.field private mOnDeleteFinishedListener:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;

.field private mProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->popupCreated:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mOnContentChangedListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;

    .line 270
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;-><init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mHandler:Landroid/os/Handler;

    .line 42
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    .line 43
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mOnContentChangedListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->setOnContentChangedListener(Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;)V

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->performDeleteFolders()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->clearKeepScreenOn()V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesThread:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesThread:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    return-object p1
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->closeProgressDialog()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mOnDeleteFinishedListener:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->setKeepScreenOn()V

    return-void
.end method

.method private declared-synchronized clearKeepScreenOn()V
    .locals 2

    .prologue
    .line 333
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336
    :cond_0
    monitor-exit p0

    return-void

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized closeProgressDialog()V
    .locals 1

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    :cond_0
    monitor-exit p0

    return-void

    .line 318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private performDeleteFolders()V
    .locals 6

    .prologue
    .line 101
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 102
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 103
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v1

    .line 104
    .local v1, "db":Lcom/samsung/everglades/video/myvideo/common/DB;
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v4

    .line 106
    .local v4, "util":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 107
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 108
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v1, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getBucketID(Landroid/net/Uri;)I

    move-result v0

    .line 109
    .local v0, "bucketId":I
    invoke-virtual {v4, v3}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 110
    invoke-virtual {v4, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->deleteCloudFolder(I)I

    goto :goto_0

    .line 112
    :cond_0
    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->deleteLocalFolder(I)I

    goto :goto_0

    .line 116
    .end local v0    # "bucketId":I
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mOnDeleteFinishedListener:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;

    if-eqz v5, :cond_2

    .line 117
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mOnDeleteFinishedListener:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;

    invoke-interface {v5}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;->onDeleteFinished()V

    .line 120
    .end local v1    # "db":Lcom/samsung/everglades/video/myvideo/common/DB;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    .end local v4    # "util":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    :cond_2
    return-void
.end method

.method private declared-synchronized setKeepScreenOn()V
    .locals 2

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    :cond_0
    monitor-exit p0

    return-void

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private showConfirmDeleteFiles(Ljava/lang/String;)V
    .locals 5
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f0c005a

    const/16 v4, 0x100

    .line 148
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->popupCreated:Z

    if-eqz v1, :cond_0

    .line 194
    :goto_0
    return-void

    .line 151
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 153
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;-><init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0c000d

    new-instance v3, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$6;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$6;-><init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$5;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$5;-><init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 189
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesDialog:Landroid/app/AlertDialog;

    .line 190
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 192
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 193
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->popupCreated:Z

    goto :goto_0
.end method

.method private showConfirmDeleteFolders(Ljava/lang/String;)V
    .locals 6
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const v4, 0x7f0c005a

    const/16 v5, 0x100

    .line 70
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->popupCreated:Z

    if-eqz v2, :cond_0

    .line 98
    :goto_0
    return-void

    .line 74
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 76
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$4;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$4;-><init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c000d

    new-instance v4, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$3;

    invoke-direct {v4, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$3;-><init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$2;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 93
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 94
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Landroid/view/Window;->setFlags(II)V

    .line 96
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 97
    const/4 v2, 0x1

    sput-boolean v2, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->popupCreated:Z

    goto :goto_0
.end method


# virtual methods
.method public deleteFiles(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const v6, 0x7f0c0022

    const/4 v3, 0x1

    .line 123
    if-nez p1, :cond_0

    .line 137
    :goto_0
    return-void

    .line 125
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 126
    .local v1, "size":I
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "msg":Ljava/lang/String;
    if-ne v1, v3, :cond_2

    .line 129
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0023

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;

    .line 136
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->showConfirmDeleteFiles(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :cond_2
    if-le v1, v3, :cond_1

    .line 131
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v6, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public deleteFolders(Ljava/util/ArrayList;Z)V
    .locals 7
    .param p2, "deleteAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v4, 0x1

    .line 53
    if-nez p1, :cond_0

    .line 67
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 58
    .local v1, "size":I
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0020

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "msg":Ljava/lang/String;
    if-le v1, v4, :cond_1

    .line 61
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    const v3, 0x7f0c001f

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 64
    :cond_1
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;

    .line 66
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->showConfirmDeleteFolders(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public dismissConfirmDeleteFiles()V
    .locals 1

    .prologue
    .line 197
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->popupCreated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesDialog:Landroid/app/AlertDialog;

    .line 200
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->popupCreated:Z

    .line 202
    :cond_0
    return-void
.end method

.method public getDeleteFilesThread()Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    .locals 4

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;-><init>(Ljava/util/Iterator;Landroid/os/Handler;Landroid/content/Context;)V

    .line 143
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnDeleteFinishedListener(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;

    .prologue
    .line 343
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mOnDeleteFinishedListener:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;

    .line 344
    return-void
.end method
