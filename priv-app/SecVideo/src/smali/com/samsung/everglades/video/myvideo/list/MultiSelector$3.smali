.class Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;
.super Ljava/lang/Object;
.source "MultiSelector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performDetails(Lcom/samsung/everglades/video/myvideo/common/ListType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

.field final synthetic val$listType:Lcom/samsung/everglades/video/myvideo/common/ListType;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 0

    .prologue
    .line 912
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;->val$listType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 914
    const-wide/16 v6, 0x0

    .line 915
    .local v6, "totalSize":J
    const-wide/16 v2, 0x0

    .line 916
    .local v2, "fileSize":J
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 918
    .local v0, "bundle":Landroid/os/Bundle;
    # getter for: Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->access$200()Ljava/util/LinkedHashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 920
    .local v4, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    :goto_0
    :try_start_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 921
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 922
    .local v5, "uri":Landroid/net/Uri;
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;->val$listType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v8}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 923
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->access$300(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getCurrentFolderSizeOnExternalDB(Landroid/net/Uri;)J

    move-result-wide v2

    .line 927
    :goto_1
    add-long/2addr v6, v2

    .line 928
    const-string v8, "size"

    invoke-virtual {v0, v8, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 929
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->access$400(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->access$400(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;)Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 931
    .end local v5    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 932
    .local v1, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v1}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    .line 934
    .end local v1    # "e":Ljava/util/ConcurrentModificationException;
    :cond_0
    return-void

    .line 925
    .restart local v5    # "uri":Landroid/net/Uri;
    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;->this$0:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;
    invoke-static {v8}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->access$300(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getSize(Landroid/net/Uri;)J
    :try_end_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v2

    goto :goto_1
.end method
