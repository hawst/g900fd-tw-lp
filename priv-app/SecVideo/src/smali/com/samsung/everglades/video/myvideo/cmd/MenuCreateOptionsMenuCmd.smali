.class public Lcom/samsung/everglades/video/myvideo/cmd/MenuCreateOptionsMenuCmd;
.super Ljava/lang/Object;
.source "MenuCreateOptionsMenuCmd.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/cmd/ICommand;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 9
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    const v8, 0x7f0e0001

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 16
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getMenu()Landroid/view/Menu;

    move-result-object v3

    .line 17
    .local v3, "menu":Landroid/view/Menu;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v2

    .line 18
    .local v2, "listType":Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 19
    .local v1, "inflater":Landroid/view/MenuInflater;
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListAttribute()I

    move-result v0

    .line 21
    .local v0, "attribute":I
    invoke-interface {v3}, Landroid/view/Menu;->clear()V

    .line 22
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MenuCrateOptionsMenuCmd listType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 23
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MenuCrateOptionsMenuCmd attribute: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 25
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 66
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 29
    :pswitch_1
    if-ne v0, v7, :cond_1

    .line 30
    invoke-virtual {v1, v8, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 31
    :cond_1
    if-ne v0, v6, :cond_2

    .line 32
    const v4, 0x7f0e0004

    invoke-virtual {v1, v4, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 34
    :cond_2
    const v4, 0x7f0e0003

    invoke-virtual {v1, v4, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 39
    :pswitch_2
    if-ne v0, v6, :cond_3

    .line 40
    const v4, 0x7f0e0006

    invoke-virtual {v1, v4, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 41
    :cond_3
    if-nez v0, :cond_0

    .line 42
    const/high16 v4, 0x7f0e0000

    invoke-virtual {v1, v4, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 47
    :pswitch_3
    if-ne v0, v7, :cond_4

    .line 48
    invoke-virtual {v1, v8, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 49
    :cond_4
    if-ne v0, v6, :cond_5

    .line 50
    const v4, 0x7f0e0005

    invoke-virtual {v1, v4, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 51
    :cond_5
    if-nez v0, :cond_0

    .line 52
    const v4, 0x7f0e0007

    invoke-virtual {v1, v4, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 57
    :pswitch_4
    if-nez v0, :cond_0

    .line 58
    const v4, 0x7f0e0002

    invoke-virtual {v1, v4, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
