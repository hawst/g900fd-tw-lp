.class public Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;
.super Ljava/lang/Object;
.source "ContextualMenuDialog.java"


# static fields
.field private static final ID_DELETE:I = 0x0

.field private static final ID_DETAILS:I = 0xc

.field private static final ID_DOWNLOAD:I = 0xb

.field private static final ID_EDIT:I = 0x2

.field private static final ID_EDIT_STUDIO:I = 0x3

.field private static final ID_EDIT_VIDEOEDITOR:I = 0x4

.field private static final ID_MOVETOKNOX:I = 0x7

.field private static final ID_MOVETOPRIVATE:I = 0x5

.field private static final ID_REMOVEFROMKNOX:I = 0x8

.field private static final ID_REMOVEFROMPRIVATE:I = 0x6

.field private static final ID_RENAME:I = 0xa

.field private static final ID_SENDTOANOTHERDEVICE:I = 0x9

.field private static final ID_SHARE_VIA:I = 0x1

.field private static final MAX:I = 0xd

.field private static final STRING_RESOURCES:[I


# instance fields
.field private mBaidu:Z

.field private mBaseList:Landroid/util/SparseBooleanArray;

.field private mCloud:Z

.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mDevice:Z

.field private mDrm:Z

.field private mDrmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

.field private mDropbox:Z

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mMoveToKnox:Z

.field private mMoveToPrivate:Z

.field private mNearby:Z

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mOptionsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRemoveFromKnox:Z

.field private mRemoveFromPrivate:Z

.field private mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

.field private mSendtoAnotherDevice:Z

.field private mSkt:Z

.field private mSlink:Z

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->STRING_RESOURCES:[I

    return-void

    :array_0
    .array-data 4
        0x7f0c005a
        0x7f0c005b
        0x7f0c0060
        0x7f0c006a
        0x7f0c006d
        0x7f0c0080
        0x7f0c00a9
        0x7f0c007c
        0x7f0c00a7
        0x7f0c0069
        0x7f0c00ab
        0x7f0c005f
        0x7f0c005e
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDrm:Z

    .line 63
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSkt:Z

    .line 64
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDropbox:Z

    .line 65
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaidu:Z

    .line 66
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloud:Z

    .line 67
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mNearby:Z

    .line 68
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSlink:Z

    .line 69
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDevice:Z

    .line 70
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToPrivate:Z

    .line 71
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromPrivate:Z

    .line 72
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToKnox:Z

    .line 73
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromKnox:Z

    .line 74
    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSendtoAnotherDevice:Z

    .line 401
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$2;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 89
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCursor:Landroid/database/Cursor;

    .line 91
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 92
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 93
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 94
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-static {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    .line 95
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 96
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDrmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    .line 98
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->initLists()V

    .line 99
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->initBooleans()V

    .line 100
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->updateOptionsList()V

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->performAction(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$202(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$302(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$402(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Lcom/samsung/everglades/video/myvideo/common/CloudUtil;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    return-object p1
.end method

.method static synthetic access$502(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    return-object p1
.end method

.method static synthetic access$602(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object p1
.end method

.method private cloudDownload()V
    .locals 4

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleCloudDownload(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;Ljava/lang/String;)V

    .line 431
    return-void
.end method

.method private getMenuArray()[Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 297
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 299
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOptionsList:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 300
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOptionsList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 301
    .local v1, "id":I
    sget-object v5, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->STRING_RESOURCES:[I

    aget v3, v5, v1

    .line 302
    .local v3, "resourceId":I
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 303
    .local v4, "str":Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 305
    .end local v1    # "id":I
    .end local v3    # "resourceId":I
    .end local v4    # "str":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/CharSequence;

    .line 307
    .end local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    return-object v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private getTitle()Ljava/lang/String;
    .locals 5

    .prologue
    .line 474
    const/4 v0, 0x0

    .line 475
    .local v0, "videoTitle":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudServerOnlyContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 476
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCursor:Landroid/database/Cursor;

    const-string v4, "_display_name"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 481
    :goto_0
    return-object v0

    .line 478
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCursor:Landroid/database/Cursor;

    const-string v3, "title"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private initBooleans()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 143
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDrmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isDrmContent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDrm:Z

    .line 146
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/net/Uri;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSkt:Z

    .line 147
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/net/Uri;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDropbox:Z

    .line 148
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/net/Uri;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaidu:Z

    .line 149
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSkt:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDropbox:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaidu:Z

    if-eqz v1, :cond_4

    :cond_0
    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloud:Z

    .line 150
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mNearby:Z

    .line 151
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSlink:Z

    .line 152
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mNearby:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSlink:Z

    if-eqz v1, :cond_2

    :cond_1
    move v3, v2

    :cond_2
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDevice:Z

    .line 154
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->initPrivateMode(Ljava/lang/String;)V

    .line 155
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->initKnoxBoolean()V

    .line 156
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->initSlinkBoolean()V

    .line 157
    return-void

    :cond_3
    move v1, v3

    .line 145
    goto :goto_0

    :cond_4
    move v1, v3

    .line 149
    goto :goto_1
.end method

.method private initKnoxBoolean()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 183
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloud:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDevice:Z

    if-eqz v0, :cond_2

    .line 184
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToKnox:Z

    .line 185
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromKnox:Z

    .line 202
    :cond_1
    :goto_0
    return-void

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnoxFileRelayAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnox2(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isSupportMoveTo(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 189
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 190
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToKnox:Z

    .line 191
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromKnox:Z

    goto :goto_0

    .line 193
    :cond_3
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToKnox:Z

    .line 194
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromKnox:Z

    goto :goto_0

    .line 197
    :cond_4
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToKnox:Z

    .line 198
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromKnox:Z

    goto :goto_0
.end method

.method private initLists()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    if-eqz v1, :cond_0

    .line 125
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    .line 126
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    .line 129
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOptionsList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 130
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOptionsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 131
    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOptionsList:Ljava/util/ArrayList;

    .line 134
    :cond_1
    new-instance v1, Landroid/util/SparseBooleanArray;

    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    .line 135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOptionsList:Ljava/util/ArrayList;

    .line 137
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xd

    if-ge v0, v1, :cond_2

    .line 138
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_2
    return-void
.end method

.method private initPrivateMode(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 160
    if-eqz p1, :cond_1

    .line 161
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloud:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDevice:Z

    if-eqz v1, :cond_2

    .line 162
    :cond_0
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToPrivate:Z

    .line 163
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromPrivate:Z

    .line 180
    :cond_1
    :goto_0
    return-void

    .line 165
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPrivateMounted(Landroid/content/Context;)Z

    move-result v0

    .line 167
    .local v0, "isPrivateMode":Z
    if-nez v0, :cond_4

    .line 168
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPrivateReady(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 169
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToPrivate:Z

    .line 173
    :goto_1
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromPrivate:Z

    goto :goto_0

    .line 171
    :cond_3
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToPrivate:Z

    goto :goto_1

    .line 175
    :cond_4
    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getPrivateDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToPrivate:Z

    .line 176
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getPrivateDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    :goto_3
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromPrivate:Z

    goto :goto_0

    :cond_5
    move v1, v3

    .line 175
    goto :goto_2

    :cond_6
    move v2, v3

    .line 176
    goto :goto_3
.end method

.method private initSlinkBoolean()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 205
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloud:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDrm:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mNearby:Z

    if-eqz v2, :cond_2

    .line 206
    :cond_0
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSendtoAnotherDevice:Z

    .line 229
    :cond_1
    :goto_0
    return-void

    .line 207
    :cond_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSLinkProviderAvailable()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->isSignedIn()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 208
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getDevicesCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 209
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 210
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSendtoAnotherDevice:Z

    .line 211
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 212
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_3

    .line 213
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSLinkUtil:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-virtual {v2, v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getNetworkModeByCursor(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 214
    .local v1, "networkMode":Ljava/lang/String;
    const-string v2, "OFF"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 215
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSendtoAnotherDevice:Z

    .line 223
    .end local v1    # "networkMode":Ljava/lang/String;
    :cond_3
    :goto_2
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 224
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 218
    .restart local v1    # "networkMode":Ljava/lang/String;
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 221
    .end local v1    # "networkMode":Ljava/lang/String;
    :cond_5
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSendtoAnotherDevice:Z

    goto :goto_2

    .line 227
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_6
    iput-boolean v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSendtoAnotherDevice:Z

    goto :goto_0
.end method

.method private nearbyDownload()V
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleDLNADownload(Landroid/net/Uri;)V

    .line 423
    return-void
.end method

.method private performAction(I)V
    .locals 2
    .param p1, "which"    # I

    .prologue
    .line 312
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOptionsList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 313
    const-string v1, "ContextualMenuDialog - performAction() mOptionsList is null"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 378
    :goto_0
    return-void

    .line 317
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOptionsList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 319
    .local v0, "selectedId":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 325
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->singleDelete()V

    goto :goto_0

    .line 321
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->singleShareVia()V

    goto :goto_0

    .line 329
    :pswitch_2
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mNearby:Z

    if-eqz v1, :cond_1

    .line 330
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->nearbyDownload()V

    goto :goto_0

    .line 331
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSlink:Z

    if-eqz v1, :cond_2

    .line 332
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->slinkDownload()V

    goto :goto_0

    .line 334
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->cloudDownload()V

    goto :goto_0

    .line 339
    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->showDetails()V

    goto :goto_0

    .line 343
    :pswitch_4
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->singleMoveToPrivate()V

    goto :goto_0

    .line 347
    :pswitch_5
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->singleRemoveFromPrivate()V

    goto :goto_0

    .line 351
    :pswitch_6
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->showRenameDialog()V

    goto :goto_0

    .line 355
    :pswitch_7
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->singleMoveToKnox()V

    goto :goto_0

    .line 359
    :pswitch_8
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->singleRemoveFromKnox()V

    goto :goto_0

    .line 363
    :pswitch_9
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->singleSendToAnotherDevice()V

    goto :goto_0

    .line 367
    :pswitch_a
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->performEdit(I)V

    goto :goto_0

    .line 371
    :pswitch_b
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->performEdit(I)V

    goto :goto_0

    .line 375
    :pswitch_c
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->performEdit(I)V

    goto :goto_0

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_6
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private performEdit(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 381
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->put(Landroid/net/Uri;Ljava/lang/String;)Z

    .line 382
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/Args;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/Args;-><init>()V

    .line 383
    .local v0, "args":Lcom/samsung/everglades/video/myvideo/cmd/Args;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 385
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/samsung/everglades/video/myvideo/activities/SearchList;

    if-eqz v2, :cond_0

    .line 386
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/samsung/everglades/video/myvideo/activities/SearchList;

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setActivity(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 389
    :cond_0
    const/4 v2, 0x2

    if-ne p1, v2, :cond_1

    .line 390
    const v2, 0x7f09007d

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .line 397
    :goto_0
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->getInstance()Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    move-result-object v1

    .line 398
    .local v1, "command":Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;
    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 399
    return-void

    .line 391
    .end local v1    # "command":Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;
    :cond_1
    const/4 v2, 0x3

    if-ne p1, v2, :cond_2

    .line 392
    const v2, 0x7f09007e

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    goto :goto_0

    .line 394
    :cond_2
    const v2, 0x7f09007f

    invoke-virtual {v0, v2}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->setMenuItemId(I)Lcom/samsung/everglades/video/myvideo/cmd/Args;

    goto :goto_0
.end method

.method private showDetails()V
    .locals 4

    .prologue
    .line 434
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->showDialog()V

    .line 435
    return-void
.end method

.method private showRenameDialog()V
    .locals 3

    .prologue
    .line 438
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showDialog()V

    .line 439
    return-void
.end method

.method private singleDelete()V
    .locals 3

    .prologue
    .line 418
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleDelete(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 419
    return-void
.end method

.method private singleMoveToKnox()V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleMoveToKNOX(Landroid/net/Uri;)V

    .line 451
    return-void
.end method

.method private singleMoveToPrivate()V
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleMoveToPrivate(Landroid/net/Uri;)V

    .line 443
    return-void
.end method

.method private singleRemoveFromKnox()V
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleRemoveFromKNOX(Landroid/net/Uri;)V

    .line 455
    return-void
.end method

.method private singleRemoveFromPrivate()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleRemoveFromPrivate(Landroid/net/Uri;)V

    .line 447
    return-void
.end method

.method private singleSendToAnotherDevice()V
    .locals 5

    .prologue
    .line 458
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleSendToAnotherDevice(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 460
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 462
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    :goto_0
    return-void

    .line 463
    :catch_0
    move-exception v0

    .line 464
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Activity Not found!!!"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 465
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 466
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 469
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    const-string v2, "fail to send to another device"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private singleShareVia()V
    .locals 3

    .prologue
    .line 414
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleShareVia(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 415
    return-void
.end method

.method private slinkDownload()V
    .locals 2

    .prologue
    .line 426
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->supportSingleSlinkDownload(Landroid/net/Uri;)V

    .line 427
    return-void
.end method

.method private updateEditMenu()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 279
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->checkEditMenu(Landroid/content/Context;)V

    .line 281
    sget v0, Lcom/samsung/everglades/video/myvideo/common/Utils;->EDIT_MENU_STATE:I

    packed-switch v0, :pswitch_data_0

    .line 294
    :goto_0
    return-void

    .line 283
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_0

    .line 286
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_0

    .line 289
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_0

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateOptionsList()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 232
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 234
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloud:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDevice:Z

    if-eqz v1, :cond_1

    .line 235
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->shouldBlockDownload()Z

    move-result v1

    if-nez v1, :cond_1

    .line 236
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 240
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mCloud:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDevice:Z

    if-nez v1, :cond_2

    .line 241
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 242
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v4, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 244
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDrm:Z

    if-nez v1, :cond_2

    .line 245
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->updateEditMenu()V

    .line 246
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v3, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 250
    :cond_2
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSlink:Z

    if-eqz v1, :cond_3

    .line 251
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v4, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 252
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v3, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 255
    :cond_3
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mSendtoAnotherDevice:Z

    if-eqz v1, :cond_4

    .line 256
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x9

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 259
    :cond_4
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToKnox:Z

    if-eqz v1, :cond_8

    .line 260
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 265
    :cond_5
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mMoveToPrivate:Z

    if-eqz v1, :cond_9

    .line 266
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 271
    :cond_6
    :goto_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    const/16 v1, 0xd

    if-ge v0, v1, :cond_a

    .line 272
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 273
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOptionsList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 261
    .end local v0    # "i":I
    :cond_8
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromKnox:Z

    if-eqz v1, :cond_5

    .line 262
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_0

    .line 267
    :cond_9
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mRemoveFromPrivate:Z

    if-eqz v1, :cond_6

    .line 268
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mBaseList:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_1

    .line 276
    .restart local v0    # "i":I
    :cond_a
    return-void
.end method


# virtual methods
.method public shouldBlockDownload()Z
    .locals 3

    .prologue
    .line 485
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 487
    .local v0, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "application/x-dtcp1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public showDialog()V
    .locals 4

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->getMenuArray()[Ljava/lang/CharSequence;

    move-result-object v1

    .line 106
    .local v1, "menuArray":[Ljava/lang/CharSequence;
    if-nez v1, :cond_0

    .line 107
    const-string v2, "ContextualMenuDialog - showDialog() menuArray is null!!!"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 121
    :goto_0
    return-void

    .line 111
    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$1;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog$1;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 118
    .local v0, "dialog":Landroid/app/AlertDialog;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/ContextualMenuDialog;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 119
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 120
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
