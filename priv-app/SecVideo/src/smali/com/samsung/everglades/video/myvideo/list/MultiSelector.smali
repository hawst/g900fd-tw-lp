.class public Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
.super Ljava/lang/Object;
.source "MultiSelector.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;
.implements Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;,
        Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;,
        Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;
    }
.end annotation


# static fields
.field private static final ACTION_START_SCONNECT_BEZEL:Ljava/lang/String; = "com.samsung.android.sconnect.START"

.field public static final BEZEL_SCONNECT_CMD:I = 0x76

.field public static final CLOUD_DOWNLOAD_CMD:I = 0x6d

.field public static final CLOUD_DOWNLOAD_FOLDER_CMD:I = 0x6e

.field public static final DELETE_CMD:I = 0x65

.field public static final DELETE_FOLDER_CMD:I = 0x66

.field public static final MOVE_TO_KNOX_CMD:I = 0x73

.field public static final MOVE_TO_PRIVATE_CMD:I = 0x6f

.field public static final MOVE_TO_PRIVATE_FOLDER_CMD:I = 0x70

.field private static final MULTISELECTOR_CMD_START:I = 0x64

.field public static final NEARBY_FILE_DOWNLOAD_CMD:I = 0x6a

.field private static final OVER_CAPACITY_LIMIT:I = 0x32

.field public static final REMOVE_FROM_KNOX_CMD:I = 0x74

.field public static final REMOVE_FROM_PRIVATE_CMD:I = 0x71

.field public static final REMOVE_FROM_PRIVATE_FOLDER_CMD:I = 0x72

.field public static final SHARE_VIA_CMD:I = 0x68

.field public static final SHARE_VIA_FOLDER_CMD:I = 0x69

.field public static final SLINK_DOWNLOAD_CMD:I = 0x75

.field public static final STUDIO_CMD:I = 0x77

.field public static final STUDIO_FOLDER_CMD:I = 0x78

.field public static final THEME_CHOOSER:Ljava/lang/String; = "theme"

.field public static final THEME_DEVICE_DEFAULT_DARK:I = 0x1

.field public static final THEME_DEVICE_DEFAULT_LIGHT:I = 0x2

.field public static final UPDATE_TOTAL_SIZE:I = 0x0

.field public static final VIDEO_EDITOR_CMD:I = 0x6b

.field public static final VIDEO_EDITOR_FOLDER_CMD:I = 0x6c

.field private static mHashMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static mOnContentChangedListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;

.field private static mOnSelectAllCheckedStateUpdateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

.field private static mOnUpdateTotalSizeListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;

.field public static mSelectMode:I

.field private static mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;


# instance fields
.field private mBlockUpdateUi:Z

.field private mCloudItemCount:I

.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

.field private mDrmItemCount:I

.field private mHandler:Landroid/os/Handler;

.field private mMultiDelete:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

.field private mPrivateItemCount:I

.field private mTcloudItemCount:I

.field private mTotalItemsCountInList:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    sput v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mSelectMode:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 949
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$4;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$4;-><init>(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHandler:Landroid/os/Handler;

    .line 87
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->reset()V

    .line 88
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 89
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performCloudDownloadFolders(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performCloudDownload(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$200()Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;)Lcom/samsung/everglades/video/myvideo/common/DB;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500()Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnUpdateTotalSizeListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;

    return-object v0
.end method

.method public static destroyInstance()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    .line 102
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 103
    sput-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    .line 106
    :cond_0
    sput-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    .line 107
    return-void
.end method

.method public static getAfterReset(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->destroyInstance()V

    .line 111
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    .line 96
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    iput-object p0, v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    .line 97
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    return-object v0
.end method

.method private getLocalItemsListFromFolders(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 683
    .local p1, "folderArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    .line 688
    .local v2, "col":[Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 689
    .local v10, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 691
    .local v9, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 692
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/Uri;

    .line 693
    .local v11, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {v0, v11}, Lcom/samsung/everglades/video/myvideo/common/DB;->getBucketID(Landroid/net/Uri;)I

    move-result v0

    int-to-long v6, v0

    .line 694
    .local v6, "bucketId":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 696
    .local v3, "where":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0, v11}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0, v11}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getProviderUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 698
    .local v1, "providerUri":Landroid/net/Uri;
    :goto_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 700
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_3

    .line 701
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 702
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 703
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 704
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 696
    .end local v1    # "providerUri":Landroid/net/Uri;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_1
    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/DB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    goto :goto_0

    .line 706
    .restart local v1    # "providerUri":Landroid/net/Uri;
    .restart local v8    # "c":Landroid/database/Cursor;
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 709
    :cond_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v4, 0x32

    if-le v0, v4, :cond_0

    .line 710
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->showOverCapacityNotice()V

    .line 711
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 712
    const/4 v10, 0x0

    .line 716
    .end local v1    # "providerUri":Landroid/net/Uri;
    .end local v3    # "where":Ljava/lang/String;
    .end local v6    # "bucketId":J
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v10    # "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v11    # "uri":Landroid/net/Uri;
    :cond_4
    return-object v10
.end method

.method private getTitle(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v3, 0x0

    .line 519
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDB:Lcom/samsung/everglades/video/myvideo/common/DB;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v2, v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 522
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private performBezelSconnectHandle(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 671
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.samsung.android.sconnect.START"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 672
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 674
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 680
    :goto_0
    return-void

    .line 675
    :catch_0
    move-exception v0

    .line 676
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Activity Not found!!!"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 677
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 678
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private performCallVideoEditor(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 589
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.EDIT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 590
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "video/mp4"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 591
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 593
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 599
    :goto_0
    return-void

    .line 594
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Activity Not found!!!"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 596
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 597
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private performCallVideoEditorFolders(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 602
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getLocalItemsListFromFolders(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 603
    .local v0, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_0

    .line 604
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performCallVideoEditor(Ljava/util/ArrayList;)V

    .line 606
    :cond_0
    return-void
.end method

.method private performCloudDownload(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v4, 0x0

    .line 616
    if-eqz p1, :cond_0

    .line 617
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    .line 618
    .local v0, "util":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    if-eqz v0, :cond_2

    .line 619
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {v2, v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isContentReady(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 620
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isChinaModel()Z

    move-result v1

    if-nez v1, :cond_1

    .line 621
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0001

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 630
    .end local v0    # "util":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    :cond_0
    :goto_0
    return-void

    .line 623
    .restart local v0    # "util":Lcom/samsung/everglades/video/myvideo/common/CloudUtil;
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    const v3, 0x7f0c010a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 628
    :cond_2
    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->downloadCloudFile(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private performCloudDownloadFolders(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 609
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getLocalItemsListFromFolders(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 610
    .local v0, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_0

    .line 611
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performCloudDownload(Ljava/util/ArrayList;)V

    .line 613
    :cond_0
    return-void
.end method

.method private performDelete(Ljava/util/ArrayList;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;"
        }
    .end annotation

    .prologue
    .line 526
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;-><init>(Landroid/content/Context;)V

    .line 527
    .local v0, "multiDelete":Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
    invoke-virtual {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->setOnDeleteFinishedListener(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;)V

    .line 529
    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->deleteFiles(Ljava/util/ArrayList;)V

    .line 531
    return-object v0
.end method

.method private performDeleteFolders(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 535
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;-><init>(Landroid/content/Context;)V

    .line 536
    .local v1, "multiDelete":Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
    invoke-virtual {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->setOnDeleteFinishedListener(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$OnDeleteFinishedListener;)V

    .line 538
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    .line 539
    .local v0, "deleteAll":Z
    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->deleteFolders(Ljava/util/ArrayList;Z)V

    .line 540
    return-void

    .line 538
    .end local v0    # "deleteAll":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private performMoveToKNOX(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 653
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;-><init>(Landroid/content/Context;)V

    .line 654
    .local v1, "multiMove":Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
    invoke-virtual {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->setOnMoveKNOXFinishedListener(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;)V

    .line 656
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    .line 657
    .local v0, "moveAll":Z
    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->moveKNOXFiles(Ljava/util/ArrayList;Z)V

    .line 658
    return-void

    .line 656
    .end local v0    # "moveAll":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private performMoveToPrivate(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 633
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;-><init>(Landroid/content/Context;)V

    .line 634
    .local v0, "multiMove":Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->MoveFilesToPrivate(Ljava/util/ArrayList;Z)V

    .line 635
    return-void
.end method

.method private performMoveToPrivateFolders(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 638
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;-><init>(Landroid/content/Context;)V

    .line 639
    .local v0, "multiMove":Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->MoveFilesToPrivate(Ljava/util/ArrayList;Z)V

    .line 640
    return-void
.end method

.method private performNearbyFileDownload(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 577
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz p1, :cond_0

    .line 578
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->downloadRemoteFiles(Ljava/util/ArrayList;)V

    .line 580
    :cond_0
    return-void
.end method

.method private performRemoveFromKNOX(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 661
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;-><init>(Landroid/content/Context;)V

    .line 662
    .local v1, "multiMove":Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
    invoke-virtual {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->setOnMoveKNOXFinishedListener(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;)V

    .line 664
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    .line 665
    .local v0, "moveAll":Z
    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->removeKNOXFiles(Ljava/util/ArrayList;Z)V

    .line 666
    return-void

    .line 664
    .end local v0    # "moveAll":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private performRemoveFromPrivate(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 643
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;-><init>(Landroid/content/Context;)V

    .line 644
    .local v0, "multiMove":Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->RemoveFilesFromPrivate(Ljava/util/ArrayList;Z)V

    .line 645
    return-void
.end method

.method private performRemoveFromPrivateFolders(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 648
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;-><init>(Landroid/content/Context;)V

    .line 649
    .local v0, "multiMove":Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->RemoveFilesFromPrivate(Ljava/util/ArrayList;Z)V

    .line 650
    return-void
.end method

.method private performSLinkFileDownload(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 583
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz p1, :cond_0

    .line 584
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->requestDownload(Ljava/util/ArrayList;)V

    .line 586
    :cond_0
    return-void
.end method

.method private performShareVia(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 543
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v2, 0x32

    if-le v1, v2, :cond_0

    .line 544
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->showOverCapacityNotice()V

    .line 545
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 567
    :goto_0
    return-void

    .line 548
    :cond_0
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setVideoWallOn(Z)V

    .line 550
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 551
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v4, :cond_1

    .line 552
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 553
    const-string v1, "video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 554
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 560
    :goto_1
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 561
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->LIGHT_THEME:Z

    if-eqz v1, :cond_2

    .line 562
    const-string v1, "theme"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 566
    :goto_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c005b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 556
    :cond_1
    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 557
    const-string v1, "video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 558
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_1

    .line 564
    :cond_2
    const-string v1, "theme"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2
.end method

.method private performShareViaFolders(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 570
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getLocalItemsListFromFolders(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 571
    .local v0, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_0

    .line 572
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performShareVia(Ljava/util/ArrayList;)V

    .line 574
    :cond_0
    return-void
.end method

.method private performStudio(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v6, 0x1

    const/4 v4, -0x1

    .line 861
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    move v3, v4

    .line 862
    .local v3, "size":I
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.sec.android.mimage.sstudio"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 863
    .local v2, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 864
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "selectedItems"

    if-ne v3, v4, :cond_0

    const/4 p1, 0x0

    .end local p1    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 865
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 866
    const-string v4, "selectedCount"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 867
    invoke-virtual {v2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 868
    const/high16 v4, 0x10000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 870
    :try_start_0
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 876
    :goto_1
    return-void

    .line 861
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "size":I
    .restart local p1    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto :goto_0

    .line 871
    .end local p1    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "size":I
    :catch_0
    move-exception v1

    .line 872
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const-string v4, "Activity Not found!!!"

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 873
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 874
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    const-string v5, "Target Activity Not Found"

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private performStudioFolders(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 879
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getLocalItemsListFromFolders(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 880
    .local v0, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_0

    .line 881
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performStudio(Ljava/util/ArrayList;)V

    .line 883
    :cond_0
    return-void
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 284
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    .line 285
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 290
    :goto_0
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudItemCount:I

    .line 291
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTcloudItemCount:I

    .line 292
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mPrivateItemCount:I

    .line 293
    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDrmItemCount:I

    .line 294
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mBlockUpdateUi:Z

    .line 295
    return-void

    .line 287
    :cond_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    goto :goto_0
.end method

.method public static setOnContentChangedListener(Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;)V
    .locals 0
    .param p0, "l"    # Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;

    .prologue
    .line 907
    sput-object p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnContentChangedListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;

    .line 908
    return-void
.end method

.method public static setOnSelectAllCheckedStateUpdateListener(Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;)V
    .locals 0
    .param p0, "l"    # Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    .prologue
    .line 857
    sput-object p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnSelectAllCheckedStateUpdateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    .line 858
    return-void
.end method

.method public static setOnUpdateTotalSizeListener(Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;)V
    .locals 0
    .param p0, "l"    # Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;

    .prologue
    .line 896
    sput-object p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnUpdateTotalSizeListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;

    .line 897
    return-void
.end method

.method private showOverCapacityNotice()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 729
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 730
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0098

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/16 v4, 0x32

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 731
    .local v0, "overCapacity":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 733
    .end local v0    # "overCapacity":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private syncByContentChanged()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 184
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 186
    .local v9, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 187
    .local v8, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 188
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 190
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 191
    .local v6, "cur":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 192
    :cond_1
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    :cond_2
    if-eqz v6, :cond_0

    .line 196
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 200
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "cur":Landroid/database/Cursor;
    :cond_3
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v7, v0, :cond_4

    .line 201
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 203
    :cond_4
    return-void
.end method

.method private updateCloudItemCount(Landroid/net/Uri;Z)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "added"    # Z

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cloud"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    if-eqz p2, :cond_1

    .line 300
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudItemCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudItemCount:I

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudItemCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudItemCount:I

    goto :goto_0
.end method

.method private updateDrmItemCount(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "added"    # Z

    .prologue
    .line 331
    if-eqz p1, :cond_0

    .line 332
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 333
    if-eqz p2, :cond_1

    .line 334
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDrmItemCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDrmItemCount:I

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDrmItemCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDrmItemCount:I

    goto :goto_0
.end method

.method private updatePrivateItemCount(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "added"    # Z

    .prologue
    .line 318
    if-eqz p1, :cond_0

    .line 319
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getPrivateDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 320
    .local v0, "secretDir":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 321
    if-eqz p2, :cond_1

    .line 322
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mPrivateItemCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mPrivateItemCount:I

    .line 328
    .end local v0    # "secretDir":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 324
    .restart local v0    # "secretDir":Ljava/lang/String;
    :cond_1
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mPrivateItemCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mPrivateItemCount:I

    goto :goto_0
.end method

.method private updateTcloudItemCount(Landroid/net/Uri;Z)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "added"    # Z

    .prologue
    .line 308
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.skp.tcloud"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    if-eqz p2, :cond_1

    .line 310
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTcloudItemCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTcloudItemCount:I

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTcloudItemCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTcloudItemCount:I

    goto :goto_0
.end method


# virtual methods
.method public deSelectAll(Landroid/widget/BaseAdapter;)V
    .locals 0
    .param p1, "adapter"    # Landroid/widget/BaseAdapter;

    .prologue
    .line 376
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->reset()V

    .line 379
    return-void
.end method

.method public finishAfterCommand()V
    .locals 2

    .prologue
    .line 720
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->reset()V

    .line 722
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getCurrentVideoListFragment(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    .line 723
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    if-eqz v0, :cond_0

    .line 724
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->stopActionMode()V

    .line 726
    :cond_0
    return-void
.end method

.method public get()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    const/4 v0, 0x0

    .line 239
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 242
    .restart local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    return-object v0
.end method

.method public getCloudSelectedItemCount()I
    .locals 2

    .prologue
    .line 132
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudItemCount:I

    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTcloudItemCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getDrmItemCount()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mDrmItemCount:I

    return v0
.end method

.method public getFirstItem()Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 136
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_0

    .line 138
    :try_start_0
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-object v1

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/util/NoSuchElementException;
    invoke-virtual {v0}, Ljava/util/NoSuchElementException;->printStackTrace()V

    move-object v1, v2

    .line 141
    goto :goto_0

    .end local v0    # "e":Ljava/util/NoSuchElementException;
    :cond_0
    move-object v1, v2

    .line 144
    goto :goto_0
.end method

.method public getMultiDelete()Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;
    .locals 1

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mMultiDelete:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    return-object v0
.end method

.method public getNumberOfItemFromFolers()I
    .locals 3

    .prologue
    .line 229
    const/4 v1, -0x1

    .line 230
    .local v1, "mItemFromFoldersCount":I
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 231
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getLocalItemsListFromFolders(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 232
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getLocalItemsListFromFolders(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 234
    :cond_0
    return v1
.end method

.method public getPrivateItemCount()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mPrivateItemCount:I

    return v0
.end method

.method public getSelectedItemCount()I
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    .line 125
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    .line 127
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public has(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 277
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    .line 278
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 280
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAllItemSelected()Z
    .locals 2

    .prologue
    .line 165
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCloudItemSelected()Z
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mCloudItemCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTcloudItemSelected()Z
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTcloudItemCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDeleteFinished()V
    .locals 0

    .prologue
    .line 737
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    .line 738
    return-void
.end method

.method public onMoveKNOXFinished()V
    .locals 0

    .prologue
    .line 847
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    .line 848
    return-void
.end method

.method public perform(I)V
    .locals 2
    .param p1, "cmd"    # I

    .prologue
    .line 408
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 410
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 412
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    packed-switch p1, :pswitch_data_0

    .line 503
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 414
    .restart local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performDelete(Ljava/util/ArrayList;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mMultiDelete:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    goto :goto_0

    .line 418
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performDeleteFolders(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 422
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performShareVia(Ljava/util/ArrayList;)V

    .line 423
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 427
    :pswitch_4
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performShareViaFolders(Ljava/util/ArrayList;)V

    .line 428
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 432
    :pswitch_5
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performNearbyFileDownload(Ljava/util/ArrayList;)V

    .line 433
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 437
    :pswitch_6
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performSLinkFileDownload(Ljava/util/ArrayList;)V

    .line 438
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 442
    :pswitch_7
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performCallVideoEditor(Ljava/util/ArrayList;)V

    .line 443
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 447
    :pswitch_8
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performCallVideoEditorFolders(Ljava/util/ArrayList;)V

    .line 448
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 452
    :pswitch_9
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performCloudDownload(Ljava/util/ArrayList;)V

    .line 453
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 457
    :pswitch_a
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performCloudDownloadFolders(Ljava/util/ArrayList;)V

    .line 458
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 462
    :pswitch_b
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performMoveToPrivate(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 466
    :pswitch_c
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performMoveToPrivateFolders(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 470
    :pswitch_d
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performRemoveFromPrivate(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 474
    :pswitch_e
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performRemoveFromPrivateFolders(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 478
    :pswitch_f
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performMoveToKNOX(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 482
    :pswitch_10
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performRemoveFromKNOX(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 486
    :pswitch_11
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performBezelSconnectHandle(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 490
    :pswitch_12
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performStudio(Ljava/util/ArrayList;)V

    .line 491
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 495
    :pswitch_13
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performStudioFolders(Ljava/util/ArrayList;)V

    .line 496
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    goto :goto_0

    .line 412
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_6
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public perform(ILcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 1
    .param p1, "cmd"    # I
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 382
    invoke-virtual {p2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    sparse-switch p1, :sswitch_data_0

    .line 404
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    .line 405
    return-void

    .line 385
    :sswitch_0
    const/16 p1, 0x66

    .line 386
    goto :goto_0

    .line 388
    :sswitch_1
    const/16 p1, 0x69

    .line 389
    goto :goto_0

    .line 391
    :sswitch_2
    const/16 p1, 0x6c

    .line 392
    goto :goto_0

    .line 394
    :sswitch_3
    const/16 p1, 0x70

    .line 395
    goto :goto_0

    .line 397
    :sswitch_4
    const/16 p1, 0x72

    .line 398
    goto :goto_0

    .line 400
    :sswitch_5
    const/16 p1, 0x78

    goto :goto_0

    .line 383
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x68 -> :sswitch_1
        0x6b -> :sswitch_2
        0x6f -> :sswitch_3
        0x71 -> :sswitch_4
        0x77 -> :sswitch_5
    .end sparse-switch
.end method

.method public performCloudContentDownload(ILcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 4
    .param p1, "cmd"    # I
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    const/4 v3, 0x1

    .line 506
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 507
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 508
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getTitle(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    .line 509
    .local v1, "title":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v3, :cond_2

    .line 510
    const/4 v2, 0x0

    invoke-virtual {p0, v0, p2, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->showCloudDonwloadDialog(Ljava/util/ArrayList;Lcom/samsung/everglades/video/myvideo/common/ListType;Ljava/lang/String;)V

    .line 514
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->finishAfterCommand()V

    .line 516
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v1    # "title":Ljava/lang/String;
    :cond_1
    return-void

    .line 511
    .restart local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .restart local v1    # "title":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 512
    invoke-virtual {p0, v0, p2, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->showCloudDonwloadDialog(Ljava/util/ArrayList;Lcom/samsung/everglades/video/myvideo/common/ListType;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public performDetails(Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 6
    .param p1, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    const/4 v4, 0x1

    .line 912
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;

    invoke-direct {v0, p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$3;-><init>(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 937
    .local v0, "UpdateSize":Ljava/lang/Runnable;
    sget-object v3, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 938
    new-instance v2, Ljava/util/ArrayList;

    sget-object v3, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 939
    .local v2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v4, :cond_1

    .line 940
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 941
    .local v1, "UpdateTotalSize":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 942
    new-instance v3, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, p1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;-><init>(Landroid/content/Context;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->showDialog()V

    .line 947
    .end local v1    # "UpdateTotalSize":Ljava/lang/Thread;
    .end local v2    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    :goto_0
    return-void

    .line 943
    .restart local v2    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 944
    new-instance v4, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-direct {v4, v5, v3, p1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialog;->showDialog()V

    goto :goto_0
.end method

.method public performSendToAnotherDevice(Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/content/Intent;
    .locals 4
    .param p1, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 969
    const/4 v1, 0x0

    .line 970
    .local v1, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 971
    new-instance v0, Ljava/util/ArrayList;

    sget-object v3, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 973
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_0

    .line 974
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 975
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getLocalItemsListFromFolders(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 976
    .local v2, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v2, :cond_0

    .line 977
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->sendToAnotherDevice(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    .line 984
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v2    # "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    :goto_0
    return-object v1

    .line 980
    .restart local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->sendToAnotherDevice(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0
.end method

.method public performSlinkShareVia()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 988
    const/4 v1, 0x0

    .line 989
    .local v1, "intent":Landroid/content/Intent;
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 990
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 991
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_0

    .line 992
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->requestShareVia(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    .line 995
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    return-object v1
.end method

.method public performSlinkShareVia(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 999
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz p1, :cond_0

    .line 1000
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->requestShareVia(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    .line 1001
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 1002
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "performSlinkShareVia : (Activity)mContext - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1004
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const/16 v3, 0x64

    invoke-virtual {v2, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1014
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 1005
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 1006
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Activity Not found!!!"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 1007
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 1008
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1011
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    const-string v2, "fail to slink share via"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public put(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 206
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v2, :cond_1

    .line 207
    const/4 v0, 0x0

    .line 208
    .local v0, "size":I
    if-eqz p1, :cond_0

    .line 209
    invoke-direct {p0, p1, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateCloudItemCount(Landroid/net/Uri;Z)V

    .line 210
    invoke-direct {p0, p1, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateTcloudItemCount(Landroid/net/Uri;Z)V

    .line 211
    invoke-direct {p0, p2, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updatePrivateItemCount(Ljava/lang/String;Z)V

    .line 212
    invoke-direct {p0, p2, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateDrmItemCount(Ljava/lang/String;Z)V

    .line 213
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mBlockUpdateUi:Z

    if-nez v2, :cond_0

    .line 215
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    .line 216
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    if-ne v0, v2, :cond_0

    .line 217
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnSelectAllCheckedStateUpdateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    if-eqz v2, :cond_0

    .line 218
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnSelectAllCheckedStateUpdateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    invoke-interface {v2, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;->onUpdate(Z)V

    .line 225
    .end local v0    # "size":I
    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public putBeforCheckCapacity(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 246
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_0

    .line 247
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    const/16 v2, 0x32

    if-lt v1, v2, :cond_1

    .line 248
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->showOverCapacityNotice()V

    .line 254
    :cond_0
    :goto_0
    return v0

    .line 251
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->put(Landroid/net/Uri;Ljava/lang/String;)Z

    .line 252
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public remove(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 258
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_0

    .line 259
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 261
    invoke-direct {p0, p1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateCloudItemCount(Landroid/net/Uri;Z)V

    .line 262
    invoke-direct {p0, p1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateTcloudItemCount(Landroid/net/Uri;Z)V

    .line 263
    invoke-direct {p0, p2, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updatePrivateItemCount(Ljava/lang/String;Z)V

    .line 264
    invoke-direct {p0, p2, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateDrmItemCount(Ljava/lang/String;Z)V

    .line 266
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    .line 267
    .local v0, "size":I
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 268
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnSelectAllCheckedStateUpdateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    if-eqz v1, :cond_0

    .line 269
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnSelectAllCheckedStateUpdateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    invoke-interface {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;->onUpdate(Z)V

    .line 274
    .end local v0    # "size":I
    :cond_0
    return-void
.end method

.method public selectAll(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;Landroid/widget/BaseAdapter;)V
    .locals 5
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p3, "adapter"    # Landroid/widget/BaseAdapter;

    .prologue
    const/4 v4, 0x0

    .line 343
    if-eqz p1, :cond_0

    .line 344
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    iput v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    .line 347
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isShareViaAttr()Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    const/16 v3, 0x32

    if-le v2, v3, :cond_2

    .line 348
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->showOverCapacityNotice()V

    .line 349
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnSelectAllCheckedStateUpdateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    if-eqz v2, :cond_1

    .line 350
    sget-object v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnSelectAllCheckedStateUpdateListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;

    invoke-interface {v2, v4}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnSelectAllCheckedStateUpdateListener;->onUpdate(Z)V

    .line 372
    :cond_1
    :goto_0
    return-void

    .line 355
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->reset()V

    .line 357
    if-eqz p1, :cond_1

    .line 358
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mBlockUpdateUi:Z

    .line 359
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 360
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_4

    .line 361
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v2, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v1

    .line 362
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "_data"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 363
    .local v0, "filePath":Ljava/lang/String;
    invoke-interface {p1}, Landroid/database/Cursor;->isLast()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 364
    iput-boolean v4, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mBlockUpdateUi:Z

    .line 366
    :cond_3
    invoke-virtual {p0, v1, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->put(Landroid/net/Uri;Ljava/lang/String;)Z

    .line 367
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 370
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_4
    iput-boolean v4, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mBlockUpdateUi:Z

    goto :goto_0
.end method

.method public setTotalItemsCountInList(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 169
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    .line 170
    return-void
.end method

.method public showCloudDonwloadDialog(Ljava/util/ArrayList;Lcom/samsung/everglades/video/myvideo/common/ListType;Ljava/lang/String;)V
    .locals 5
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p3, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/samsung/everglades/video/myvideo/common/ListType;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 784
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 785
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0c005f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 786
    if-eqz p1, :cond_0

    .line 787
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 788
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00c3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 794
    :cond_0
    :goto_0
    const v1, 0x7f0c0097

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;Lcom/samsung/everglades/video/myvideo/common/ListType;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 805
    const v1, 0x7f0c000d

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$2;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/MultiSelector;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 812
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 813
    return-void

    .line 789
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v3, :cond_0

    .line 790
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c003b

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public supportSingleCloudDownload(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 778
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 779
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 780
    invoke-virtual {p0, v0, p2, p3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->showCloudDonwloadDialog(Ljava/util/ArrayList;Lcom/samsung/everglades/video/myvideo/common/ListType;Ljava/lang/String;)V

    .line 781
    return-void
.end method

.method public supportSingleDLNADownload(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 764
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 765
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 767
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performNearbyFileDownload(Ljava/util/ArrayList;)V

    .line 768
    return-void
.end method

.method public supportSingleDelete(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 741
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 742
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 743
    invoke-virtual {p2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 744
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performDeleteFolders(Ljava/util/ArrayList;)V

    .line 748
    :goto_0
    return-void

    .line 746
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performDelete(Ljava/util/ArrayList;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mMultiDelete:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    goto :goto_0
.end method

.method public supportSingleMoveToKNOX(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 828
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 829
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 830
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performMoveToKNOX(Ljava/util/ArrayList;)V

    .line 831
    return-void
.end method

.method public supportSingleMoveToPrivate(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 816
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 817
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 818
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performMoveToPrivate(Ljava/util/ArrayList;)V

    .line 819
    return-void
.end method

.method public supportSingleRemoveFromKNOX(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 834
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 835
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 836
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performRemoveFromKNOX(Ljava/util/ArrayList;)V

    .line 837
    return-void
.end method

.method public supportSingleRemoveFromPrivate(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 822
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 823
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 824
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performRemoveFromPrivate(Ljava/util/ArrayList;)V

    .line 825
    return-void
.end method

.method public supportSingleSendToAnotherDevice(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 840
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 841
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 842
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->sendToAnotherDevice(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public supportSingleShareVia(Landroid/net/Uri;Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 751
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 752
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 754
    invoke-virtual {p2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 755
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performShareViaFolders(Ljava/util/ArrayList;)V

    .line 761
    :goto_0
    return-void

    .line 756
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 757
    invoke-virtual {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performSlinkShareVia(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 759
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performShareVia(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public supportSingleSlinkDownload(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 771
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 772
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 774
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performSLinkFileDownload(Ljava/util/ArrayList;)V

    .line 775
    return-void
.end method

.method public updateContentChanged()V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->syncByContentChanged()V

    .line 174
    return-void
.end method

.method public updateContentChanged(Z)V
    .locals 1
    .param p1, "update"    # Z

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->updateContentChanged()V

    .line 178
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnContentChangedListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 179
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mOnContentChangedListener:Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnContentChangedListener;->onUpdate()V

    .line 181
    :cond_0
    return-void
.end method

.method public updateTotalCountByCursorReloaded(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    .line 116
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mHashMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->mTotalItemsCountInList:I

    if-le v0, v1, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->syncByContentChanged()V

    .line 121
    :cond_0
    return-void
.end method
