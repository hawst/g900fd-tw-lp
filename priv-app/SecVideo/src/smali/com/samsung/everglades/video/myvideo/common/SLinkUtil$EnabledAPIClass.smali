.class Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;
.super Ljava/lang/Object;
.source "SLinkUtil.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EnabledAPIClass"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;


# direct methods
.method private constructor <init>(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    .param p2, "x1"    # Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$1;

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;-><init>(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)V

    return-void
.end method

.method private getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 313
    if-nez p1, :cond_0

    const-string v0, "_"

    .line 319
    :goto_0
    return-object v0

    .line 315
    :cond_0
    const-string v0, "!"

    const-string v1, "!!"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 316
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 317
    const-string v0, "_"

    const-string v1, "!_"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 318
    const-string v0, "%"

    const-string v1, "!%"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    .line 319
    goto :goto_0
.end method

.method private getSortOrderString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 284
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "slinksortorder"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    .line 285
    .local v1, "sortOrder":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SLinkUtil - getSortOrderString() order:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 287
    packed-switch v1, :pswitch_data_0

    .line 308
    const-string v2, "title COLLATE LOCALIZED ASC"

    :goto_0
    return-object v2

    .line 289
    :pswitch_0
    const-string v2, "isPlayed DESC"

    goto :goto_0

    .line 292
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 293
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v2, "(date_added * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    const-string v2, "(date_modified * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string v2, "(datetaken * 1) DESC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 299
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :pswitch_2
    const-string v2, "title COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 302
    :pswitch_3
    const-string v2, "(_size * 1) DESC"

    goto :goto_0

    .line 305
    :pswitch_4
    const-string v2, "mime_type ASC"

    goto :goto_0

    .line 287
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public acquireWakeLock(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 456
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$400(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    move-result-object v1

    if-nez v1, :cond_0

    .line 457
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 458
    .local v0, "tag":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->createWakeLock(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    move-result-object v2

    # setter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$402(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 459
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$400(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->acquire()V

    .line 460
    const-string v1, "SLinkUtil - acquireWakeLock"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 462
    .end local v0    # "tag":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public callScsCoreInitServiceIfNeeded()V
    .locals 2

    .prologue
    .line 244
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass$1;-><init>(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;)V

    .line 254
    .local v0, "runnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mThreadPool:Ljava/util/concurrent/ExecutorService;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$300(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mThreadPool:Ljava/util/concurrent/ExecutorService;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$300(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 257
    :cond_0
    return-void
.end method

.method public deleteFile(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 477
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 478
    .local v0, "rowId":J
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->deleteFile(Landroid/content/Context;J)V

    .line 479
    return-void
.end method

.method public deregisterDevice(I)Z
    .locals 4
    .param p1, "deviceId"    # I

    .prologue
    .line 505
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->deregisterDevice(J)Z

    move-result v0

    return v0
.end method

.method public getDeviceIcon(JLandroid/database/Cursor;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "deviceId"    # J
    .param p3, "cusor"    # Landroid/database/Cursor;

    .prologue
    .line 356
    :try_start_0
    invoke-static {p3}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->getNetworkMode(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-eq v1, v2, :cond_0

    .line 357
    const/4 v1, 0x1

    new-array v6, v1, [I

    const/4 v1, 0x0

    const v2, 0x101009e

    aput v2, v6, v1

    .line 364
    .local v6, "states":[I
    :goto_0
    const/4 v0, 0x0

    .line 365
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->LIGHT_THEME:Z

    if-eqz v1, :cond_1

    .line 366
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v1

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->SMALL:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;->DARK:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;

    move-wide v2, p1

    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDeviceIcon(Landroid/content/Context;JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 374
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "states":[I
    :goto_1
    return-object v0

    .line 361
    :cond_0
    const/4 v1, 0x0

    new-array v6, v1, [I

    .restart local v6    # "states":[I
    goto :goto_0

    .line 368
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v1

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->SMALL:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;->LIGHT:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;

    move-wide v2, p1

    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDeviceIcon(Landroid/content/Context;JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 372
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "states":[I
    :catch_0
    move-exception v7

    .line 373
    .local v7, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 374
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getDeviceNameByCurosr(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 348
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 350
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDisplayName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDevicesCursor()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 260
    const-string v3, "transport_type == ? AND (transport_type != ? AND physical_type != ?)"

    .line 264
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 268
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string v5, "transport_type, network_mode DESC"

    .line 270
    .local v5, "order":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->CONTENT_URI_DEVICE:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFileCursorByDeviceId(ILjava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "deviceId"    # I
    .param p2, "strFilter"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 274
    const/4 v3, 0x0

    .line 276
    .local v3, "selection":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "title like \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " escape \'!\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    int-to-long v4, p1

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->getSortOrderString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getHowToUseViewIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->createHowToUseViewIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getLocalDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 344
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;
    .locals 1
    .param p1, "data"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 500
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkModeByCursor(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 379
    if-nez p1, :cond_0

    const/4 v1, 0x0

    .line 393
    :goto_0
    return-object v1

    .line 381
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->getDeviceTransportType(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v0

    .line 383
    .local v0, "transportType":Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 384
    const-string v1, ""

    goto :goto_0

    .line 386
    :cond_1
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->getNetworkMode(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne v1, v2, :cond_3

    .line 387
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isChinaModel()Z

    move-result v1

    if-nez v1, :cond_2

    .line 388
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0108

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 390
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0109

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 393
    :cond_3
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->getNetworkMode(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getSignInActivityIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->createSignInActivityIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getThumbId(Ljava/lang/String;)J
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 411
    monitor-enter p0

    :try_start_0
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    aget-object p1, v0, v1

    .line 412
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getUriById(J)Landroid/net/Uri;
    .locals 1
    .param p1, "videoId"    # J

    .prologue
    .line 323
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCount(I)I
    .locals 8
    .param p1, "deviceId"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 327
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "count(*) AS count"

    aput-object v0, v2, v6

    .line 331
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    int-to-long v4, p1

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 333
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 334
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 335
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 336
    .local v6, "count":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 340
    .end local v6    # "count":I
    :cond_0
    return v6
.end method

.method public declared-synchronized isSLinkThumbId(Ljava/lang/String;)Z
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 407
    monitor-enter p0

    :try_start_0
    const-string v0, "SEC_S-LINK_ITEM_BITMAP-KEY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isSamsungAccountExists()Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->samsungAccountExists()Z

    move-result v0

    return v0
.end method

.method public isSignedIn()Z
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v0

    return v0
.end method

.method public releaseWakeLock()V
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$400(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$400(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->release()V

    .line 467
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$402(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 468
    const-string v0, "SLinkUtil - releaseWakeLock :"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 470
    :cond_0
    return-void
.end method

.method public requestDownload(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 424
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 425
    .local v5, "size":I
    new-array v1, v5, [J

    .line 427
    .local v1, "ids":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 428
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 429
    .local v2, "id":J
    aput-wide v2, v1, v0

    .line 427
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 432
    .end local v2    # "id":J
    :cond_0
    new-instance v4, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 433
    .local v4, "options":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v6

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v7

    invoke-virtual {v6, v7, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->downloadFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 434
    return-void
.end method

.method public requestRefresh()V
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->requestRefresh()V

    .line 474
    return-void
.end method

.method public requestShareVia(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 437
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 438
    .local v10, "size":I
    new-array v7, v10, [J

    .line 440
    .local v7, "ids":[J
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v10, :cond_0

    .line 441
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 442
    .local v8, "id":J
    aput-wide v8, v7, v6

    .line 440
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 445
    .end local v8    # "id":J
    :cond_0
    new-instance v5, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 446
    .local v5, "options":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v0

    invoke-static {v7}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c005b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createModalDownloadActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public sendToAnotherDevice(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v7, 0x0

    .line 482
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 483
    .local v5, "size":I
    new-array v1, v5, [J

    .line 485
    .local v1, "ids":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 486
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 487
    .local v2, "id":J
    aput-wide v2, v1, v0

    .line 485
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 490
    .end local v2    # "id":J
    :cond_0
    new-instance v4, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 491
    .local v4, "options":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    iput-boolean v7, v4, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->allowCloudStorageTargetDevice:Z

    .line 492
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v6

    const-string v7, "SLinkMedia"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 493
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v6

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v7

    invoke-virtual {v6, v7, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createSendToActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v6

    .line 495
    :goto_1
    return-object v6

    :cond_1
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v6

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v7

    invoke-virtual {v6, v7, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createSendToActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v6

    goto :goto_1
.end method

.method public setSyncPriority()V
    .locals 2

    .prologue
    .line 451
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;->this$0:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v0

    .line 452
    .local v0, "m":Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->setSyncMediaPriority(I)V

    .line 453
    return-void
.end method

.method public thumbIdToString(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 399
    if-nez p1, :cond_0

    const/4 v4, 0x0

    .line 403
    :goto_0
    return-object v4

    .line 401
    :cond_0
    const-string v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 402
    .local v2, "id":J
    const-string v4, "device_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 403
    .local v0, "deviceId":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "device_id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SEC_S-LINK_ITEM_BITMAP-KEY"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
