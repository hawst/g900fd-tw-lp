.class public final Lcom/samsung/everglades/video/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final KEY_BUCKET_ID:I = 0x7f090000

.field public static final KEY_FIRST_ROW_TEXTVIEW:I = 0x7f090001

.field public static final KEY_SECOND_ROW_TEXTVIEW:I = 0x7f090002

.field public static final KEY_URI:I = 0x7f090003

.field public static final PopupButtonLayout:I = 0x7f09005f

.field public static final VideoView:I = 0x7f09005d

.field public static final VideoViewLayout:I = 0x7f09005c

.field public static final VideoViewNoVideo:I = 0x7f09005e

.field public static final VideoViewbackBottom:I = 0x7f090065

.field public static final VideoViewbackBottom2:I = 0x7f090066

.field public static final VideoViewbackLeft:I = 0x7f090067

.field public static final VideoViewbackLeft2:I = 0x7f090068

.field public static final VideoViewbackRight:I = 0x7f090069

.field public static final VideoViewbackRight2:I = 0x7f09006a

.field public static final VideoViewbackTop:I = 0x7f090063

.field public static final VideoViewbackTop2:I = 0x7f090064

.field public static final action_autoplay_settings:I = 0x7f090079

.field public static final action_cloud_view_option:I = 0x7f090078

.field public static final action_delete:I = 0x7f090075

.field public static final action_delete_done:I = 0x7f090071

.field public static final action_deregisterd_device:I = 0x7f090073

.field public static final action_details:I = 0x7f090086

.field public static final action_download:I = 0x7f09007c

.field public static final action_edit:I = 0x7f09007d

.field public static final action_move_to_knox:I = 0x7f090082

.field public static final action_move_to_private:I = 0x7f090080

.field public static final action_remove_from_knox:I = 0x7f090083

.field public static final action_remove_from_private:I = 0x7f090081

.field public static final action_rename:I = 0x7f090085

.field public static final action_search:I = 0x7f09006e

.field public static final action_select:I = 0x7f09006f

.field public static final action_sendto_another_device:I = 0x7f090084

.field public static final action_share:I = 0x7f09007b

.field public static final action_sortby:I = 0x7f090070

.field public static final action_studio:I = 0x7f09007e

.field public static final action_video_editor:I = 0x7f09007f

.field public static final action_viewby:I = 0x7f090077

.field public static final asf_file_menu:I = 0x7f09006d

.field public static final bottom_line:I = 0x7f090012

.field public static final check_box:I = 0x7f090036

.field public static final checked:I = 0x7f09006b

.field public static final cloud:I = 0x7f09002d

.field public static final connectionPopupChecktext:I = 0x7f090009

.field public static final connectionPopupcheckbox:I = 0x7f090008

.field public static final connectionexPopupDetailtext:I = 0x7f090006

.field public static final connectiontextlayout:I = 0x7f090007

.field public static final content_area:I = 0x7f090005

.field public static final content_list:I = 0x7f09002b

.field public static final count:I = 0x7f090033

.field public static final counter_text:I = 0x7f090017

.field public static final delete:I = 0x7f090062

.field public static final details:I = 0x7f090042

.field public static final device_menu:I = 0x7f090072

.field public static final device_section_count:I = 0x7f09001a

.field public static final device_section_title:I = 0x7f090019

.field public static final empty_view_device_text:I = 0x7f090021

.field public static final empty_view_local:I = 0x7f090023

.field public static final empty_view_text:I = 0x7f090024

.field public static final first_row_text:I = 0x7f090057

.field public static final folder_icon:I = 0x7f09002e

.field public static final folder_list:I = 0x7f090028

.field public static final folder_margin:I = 0x7f090029

.field public static final folder_margin_img:I = 0x7f09002a

.field public static final folder_picker_list:I = 0x7f090025

.field public static final go_to_button_text:I = 0x7f090022

.field public static final grid_view:I = 0x7f090045

.field public static final list_actionbar_selectall_layout:I = 0x7f09003e

.field public static final list_actionbar_selection_layout:I = 0x7f09003c

.field public static final list_divider:I = 0x7f09001f

.field public static final list_fixed_counter_text:I = 0x7f090047

.field public static final list_fixed_layout:I = 0x7f090046

.field public static final list_item_checkbox_stub:I = 0x7f090050

.field public static final list_item_cloud_stub:I = 0x7f090051

.field public static final list_item_folder_stub:I = 0x7f090052

.field public static final list_item_private_stub:I = 0x7f090055

.field public static final list_item_progressview_stub:I = 0x7f090053

.field public static final list_view:I = 0x7f09004a

.field public static final listcheckbox:I = 0x7f09002c

.field public static final listselectcount:I = 0x7f090054

.field public static final llVideoView:I = 0x7f09005b

.field public static final main_menu:I = 0x7f090074

.field public static final menu_vzcloud:I = 0x7f090076

.field public static final name:I = 0x7f090026

.field public static final nearby_device_icon:I = 0x7f090010

.field public static final nearby_device_list_view:I = 0x7f09000f

.field public static final nearby_device_section:I = 0x7f09000d

.field public static final nearby_no_device_text:I = 0x7f09000e

.field public static final nearby_title_text:I = 0x7f090011

.field public static final network_text:I = 0x7f090018

.field public static final pager:I = 0x7f090041

.field public static final path:I = 0x7f090027

.field public static final percent:I = 0x7f090034

.field public static final private_icon:I = 0x7f09002f

.field public static final processing:I = 0x7f090031

.field public static final progressBar:I = 0x7f090032

.field public static final progress_bar:I = 0x7f09004c

.field public static final progress_view:I = 0x7f09004b

.field public static final progressview:I = 0x7f090030

.field public static final refresh_device_button:I = 0x7f09001b

.field public static final refresh_device_progress:I = 0x7f09001e

.field public static final refresh_image:I = 0x7f09001d

.field public static final refresh_image_nearby:I = 0x7f090020

.field public static final refresh_layout:I = 0x7f09001c

.field public static final rename:I = 0x7f090060

.field public static final row_inner_main_layout:I = 0x7f09004f

.field public static final row_main_layout:I = 0x7f09004e

.field public static final row_text_layout:I = 0x7f090056

.field public static final search_list:I = 0x7f090037

.field public static final search_view:I = 0x7f090038

.field public static final second_row_text:I = 0x7f090058

.field public static final select_all_checkbox:I = 0x7f09003f

.field public static final select_all_layout:I = 0x7f090048

.field public static final select_all_text:I = 0x7f090049

.field public static final selected_count_text:I = 0x7f090040

.field public static final selection_mode_done:I = 0x7f09003a

.field public static final selection_mode_layout:I = 0x7f090039

.field public static final selection_mode_title:I = 0x7f09003b

.field public static final selectoin_mode_asf_file_menu:I = 0x7f090088

.field public static final selectoin_mode_menu:I = 0x7f09007a

.field public static final selectoin_mode_slink_file_menu:I = 0x7f090087

.field public static final sharevia:I = 0x7f090061

.field public static final slink_device:I = 0x7f090014

.field public static final slink_device_delete:I = 0x7f090013

.field public static final slink_device_icon:I = 0x7f090015

.field public static final slink_device_list_view:I = 0x7f09000c

.field public static final slink_device_section:I = 0x7f09000a

.field public static final slink_file_menu:I = 0x7f090089

.field public static final slink_no_device:I = 0x7f09000b

.field public static final slink_title_text:I = 0x7f090016

.field public static final spinner_style_counter:I = 0x7f09003d

.field public static final spinner_text:I = 0x7f09005a

.field public static final thumbnail:I = 0x7f090044

.field public static final thumbnail_layout:I = 0x7f090043

.field public static final unchecked:I = 0x7f09006c

.field public static final user_touch_area_for_checkbox:I = 0x7f090035

.field public static final username_edit:I = 0x7f090004

.field public static final viewstub_personal_emptyview:I = 0x7f09004d

.field public static final vw_thumbnail:I = 0x7f090059


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
