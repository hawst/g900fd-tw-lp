.class Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;
.super Ljava/lang/Object;
.source "ThumbHoverViewer.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)V
    .locals 0

    .prologue
    .line 660
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x2

    .line 663
    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1300()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 665
    const/16 v1, 0xa

    const/4 v2, -0x1

    :try_start_0
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 670
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 730
    :cond_1
    :goto_1
    :pswitch_0
    const/4 v1, 0x0

    return v1

    .line 666
    :catch_0
    move-exception v0

    .line 667
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 672
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 673
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 676
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_1

    .line 687
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2300(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020040

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 678
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020043

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 681
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020046

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 684
    :sswitch_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f02003d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 696
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 697
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # invokes: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getHandler()Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$400(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 699
    :cond_3
    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mIsSPenHovering:Z
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$1300()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 701
    const/16 v1, 0xa

    const/4 v2, -0x1

    :try_start_1
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 702
    :catch_1
    move-exception v0

    .line 703
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 709
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    goto/16 :goto_1

    .line 720
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDetails:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2300(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f02003f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 711
    :sswitch_5
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mRename:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2000(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020042

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 714
    :sswitch_6
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mSharevia:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2100(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020045

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 717
    :sswitch_7
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mDelete:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->access$2200(Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f02003c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 670
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 676
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_0
        0x7f090060 -> :sswitch_1
        0x7f090061 -> :sswitch_2
        0x7f090062 -> :sswitch_3
    .end sparse-switch

    .line 709
    :sswitch_data_1
    .sparse-switch
        0x7f090042 -> :sswitch_4
        0x7f090060 -> :sswitch_5
        0x7f090061 -> :sswitch_6
        0x7f090062 -> :sswitch_7
    .end sparse-switch
.end method
