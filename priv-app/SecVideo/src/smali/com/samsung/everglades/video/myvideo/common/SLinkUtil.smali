.class public Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
.super Ljava/lang/Object;
.source "SLinkUtil.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$1;,
        Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$DummyAPIClass;,
        Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;
    }
.end annotation


# static fields
.field private static final BITMAP_KEY:Ljava/lang/String; = "SEC_S-LINK_ITEM_BITMAP-KEY"

.field public static final CONTENT_URI_DEVICE:Landroid/net/Uri;

.field public static final CONTENT_URI_VIDEOS:Landroid/net/Uri;

.field public static final DEVICE_ID:Ljava/lang/String; = "device_id"

.field public static final TRANSPORT_TYPE:Ljava/lang/String; = "transport_type"

.field private static mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

.field private mDummyApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

.field private mEnabledApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

.field private mPlatformEnabled:Z

.field private mThreadPool:Ljava/util/concurrent/ExecutorService;

.field private mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->CONTENT_URI_DEVICE:Landroid/net/Uri;

    .line 41
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->CONTENT_URI_VIDEOS:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 66
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;

    invoke-direct {v0, p0, v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$EnabledAPIClass;-><init>(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$1;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mEnabledApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    .line 67
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$DummyAPIClass;

    invoke-direct {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$DummyAPIClass;-><init>(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil$1;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mDummyApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    .line 68
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->updatePlatformEnabledState(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    .param p1, "x1"    # Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    return-object p1
.end method

.method public static destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    if-eqz v0, :cond_1

    .line 83
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    .line 84
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 85
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    iput-object v1, v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 87
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    iput-object v1, v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mEnabledApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    .line 88
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    iput-object v1, v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mDummyApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    .line 89
    sput-object v1, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 91
    :cond_1
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    .line 76
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    iput-object p0, v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;

    .line 78
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    return-object v0
.end method

.method private updatePlatformEnabledState(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 96
    .local v1, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v2, "com.samsung.android.sdk.samsunglink"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v2, v2, Landroid/content/pm/ApplicationInfo;->enabled:Z

    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mPlatformEnabled:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mPlatformEnabled:Z

    if-eqz v2, :cond_0

    .line 102
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mEnabledApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    .line 106
    :goto_1
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mPlatformEnabled:Z

    goto :goto_0

    .line 104
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mDummyApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    goto :goto_1
.end method


# virtual methods
.method public acquireWakeLock(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->acquireWakeLock(Landroid/content/Context;)V

    .line 210
    return-void
.end method

.method public callScsCoreInitServiceIfNeeded()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->callScsCoreInitServiceIfNeeded()V

    .line 138
    return-void
.end method

.method public deleteFile(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->deleteFile(Landroid/net/Uri;)V

    .line 218
    return-void
.end method

.method public deregisterDevice(I)Z
    .locals 1
    .param p1, "deviceId"    # I

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->deregisterDevice(I)Z

    move-result v0

    return v0
.end method

.method public getDeviceIcon(JLandroid/database/Cursor;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "deviceId"    # J
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getDeviceIcon(JLandroid/database/Cursor;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceNameByCurosr(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getDeviceNameByCurosr(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDevicesCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getDevicesCursor()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFileCursorByDeviceId(ILjava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "deviceId"    # I
    .param p2, "strFilter"    # Ljava/lang/String;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getFileCursorByDeviceId(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getHowToUseViewIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getHowToUseViewIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getLocalDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getLocalDeviceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;
    .locals 1
    .param p1, "data"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkModeByCursor(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getNetworkModeByCursor(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSignInActivityIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getSignInActivityIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getThumbId(Ljava/lang/String;)J
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getThumbId(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getUriById(J)Landroid/net/Uri;
    .locals 1
    .param p1, "videoId"    # J

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1, p2}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getUriById(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCount(I)I
    .locals 1
    .param p1, "deviceId"    # I

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->getVideoCount(I)I

    move-result v0

    return v0
.end method

.method public isSLinkProviderAvailable()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 115
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 118
    .local v1, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v4, "com.samsung.android.sdk.samsunglink"

    const/16 v5, 0x80

    invoke-virtual {v1, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget v2, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 119
    .local v2, "sLinkVersionCode":I
    const/16 v4, 0x3e8

    if-lt v2, v4, :cond_0

    iget-boolean v4, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mPlatformEnabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    .line 125
    .end local v2    # "sLinkVersionCode":I
    :cond_0
    :goto_0
    return v3

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 122
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public isSLinkThumbId(Ljava/lang/String;)Z
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->isSLinkThumbId(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSamsungAccountExists()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->isSamsungAccountExists()Z

    move-result v0

    return v0
.end method

.method public isSignedIn()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->isSignedIn()Z

    move-result v0

    return v0
.end method

.method public releaseWakeLock()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->releaseWakeLock()V

    .line 214
    return-void
.end method

.method public requestDownload(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->requestDownload(Ljava/util/ArrayList;)V

    .line 194
    return-void
.end method

.method public requestRefresh()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->requestRefresh()V

    .line 202
    return-void
.end method

.method public requestShareVia(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->requestShareVia(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public sendToAnotherDevice(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 221
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->sendToAnotherDevice(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public setSyncPriority()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->setSyncPriority()V

    .line 206
    return-void
.end method

.method public thumbIdToString(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mCurrentApi:Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;

    invoke-interface {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/ISLinkUtil;->thumbIdToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateEnabledAndGetBeRefreshed()Z
    .locals 2

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mPlatformEnabled:Z

    .line 110
    .local v0, "temp":Z
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->updatePlatformEnabledState(Landroid/content/Context;)V

    .line 111
    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->mPlatformEnabled:Z

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
