.class public abstract Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;
.super Landroid/app/Activity;
.source "AbsItemListActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mExitListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$ExitListener;

.field protected mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

.field protected mIsFolderContent:Z

.field protected mSelectAllCheckBox:Landroid/widget/CheckBox;

.field protected mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mIsFolderContent:Z

    .line 120
    new-instance v0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity$1;-><init>(Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mExitListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$ExitListener;

    return-void
.end method

.method private isFolderContent(Landroid/os/Bundle;)Z
    .locals 3
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 102
    const-string v2, "list_additional_infos"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getVideoListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 84
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->invalidateOptionsMenu()V

    .line 85
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->enableStatusBarOpenByNotification(Landroid/view/Window;)V

    .line 38
    const v1, 0x7f040001

    invoke-virtual {p0, v1}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->setContentView(I)V

    .line 39
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 40
    .local v0, "mMultiWindowActivity":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mExitListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$ExitListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setExitListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$ExitListener;)Z

    .line 41
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 45
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 46
    const/4 v0, 0x0

    .line 54
    :goto_0
    return v0

    .line 49
    :cond_0
    const/16 v0, 0x54

    if-ne p1, v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->openSearchList()V

    .line 54
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 111
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->finish()V

    .line 113
    const/4 v0, 0x1

    .line 116
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected setFragment(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f090005

    .line 88
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->isFolderContent(Landroid/os/Bundle;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mIsFolderContent:Z

    .line 90
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mIsFolderContent:Z

    if-eqz v0, :cond_0

    .line 91
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .line 92
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mFolderListFragment:Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 99
    :goto_0
    return-void

    .line 95
    :cond_0
    new-instance v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 96
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 2
    .param p1, "stringId"    # I

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 65
    .local v0, "bar":Landroid/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 66
    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 67
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 59
    .local v0, "bar":Landroid/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 60
    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 61
    return-void
.end method

.method protected setupView()V
    .locals 0

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/AbsItemListActivity;->updateContentFragment()V

    .line 71
    return-void
.end method

.method protected updateContentFragment()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method
