.class public Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
.super Ljava/lang/Object;
.source "MoveKNOXOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;,
        Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;,
        Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    }
.end annotation


# static fields
.field private static final ABORT_MOVE_PROGRESS:I = 0x3

.field private static final CLEAR_KEEP_SCREEN_ON:I = 0x7

.field private static final FAIL_ERROR_CODE_CANCELED:I = 0x2

.field private static final FAIL_ERROR_CODE_CONTAINER_STATE_PROBLEM:I = 0x1

.field private static final FAIL_ERROR_CODE_FILE_MOVE_FAIL:I = 0x8

.field private static final FAIL_ERROR_CODE_NOT_ALLOWED_FILENAME:I = 0x5

.field private static final FAIL_ERROR_CODE_NOT_ALLOWED_SRCPATH:I = 0x4

.field private static final FAIL_ERROR_CODE_NOT_ERROR:I = 0x0

.field private static final FAIL_ERROR_CODE_SRCDIR_REMOVE_FAIL:I = 0x3

.field private static final FAIL_ERROR_CODE_SRC_NOT_EXIST:I = 0x6

.field private static final FAIL_ERROR_CODE_STORAGE_FULL:I = 0x7

.field private static final FINISH_MOVE_PROGRESS:I = 0x2

.field public static final HANDLE_ERR_EXCEPTION:I = 0x5

.field public static final HANDLE_ERR_NOT_ENOUGH_SPACE:I = 0x4

.field private static final INTENT_FILE_RELAY_COMPLETE:Ljava/lang/String; = "com.sec.knox.container.FileRelayComplete"

.field private static final INTENT_FILE_RELAY_DONE:Ljava/lang/String; = "com.sec.knox.container.FileRelayDone"

.field private static final INTENT_FILE_RELAY_EXIST:Ljava/lang/String; = "com.sec.knox.container.FileRelayExist"

.field private static final INTENT_FILE_RELAY_FAIL:Ljava/lang/String; = "com.sec.knox.container.FileRelayFail"

.field private static final INTENT_FILE_RELAY_PROGRESS:Ljava/lang/String; = "com.sec.knox.container.FileRelayProgress"

.field private static final MOVE_FILES:I = 0x1

.field private static final MOVE_FOLDERS:I = 0x3

.field private static final REMOVE_FILES:I = 0x2

.field private static final REMOVE_FOLDERS:I = 0x4

.field private static final SET_KEEP_SCREEN_ON:I = 0x6

.field private static final START_MOVE_PROGRESS:I = 0x0

.field private static final UPDATE_MOVE_PROGRESS:I = 0x1

.field private static final UPDATE_MOVE_PROGRESS_PERCENTAGE:I = 0x8

.field public static popupCreated:Z


# instance fields
.field private errno:I

.field private mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mErrOccured:Z

.field private final mHandler:Landroid/os/Handler;

.field private mKnox2DestId:I

.field private mKnox2DestName:Ljava/lang/String;

.field private mMessage:Landroid/os/Message;

.field private mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

.field private mOnMoveFinishedListener:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;

.field private mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRequestedCmdType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->popupCreated:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->errno:I

    .line 76
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMessage:Landroid/os/Message;

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mKnox2DestId:I

    .line 78
    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mKnox2DestName:Ljava/lang/String;

    .line 79
    iput-boolean v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mErrOccured:Z

    .line 80
    iput v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mRequestedCmdType:I

    .line 266
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 332
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;

    .line 83
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    .line 84
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMessage:Landroid/os/Message;

    .line 85
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMessage:Landroid/os/Message;

    const/16 v1, 0x8

    iput v1, v0, Landroid/os/Message;->what:I

    .line 86
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMessage:Landroid/os/Message;

    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->setKeepScreenOn()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->clearKeepScreenOn()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mRequestedCmdType:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->errno:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
    .param p1, "x1"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->errno:I

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Landroid/os/Message;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMessage:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mErrOccured:Z

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
    .param p1, "x1"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    return-object p1
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->closeProgressDialog()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->showToast(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;)Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mOnMoveFinishedListener:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;

    return-object v0
.end method

.method private declared-synchronized clearKeepScreenOn()V
    .locals 2

    .prologue
    .line 570
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 573
    :cond_0
    monitor-exit p0

    return-void

    .line 570
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized closeProgressDialog()V
    .locals 1

    .prologue
    .line 521
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;->dismiss()V

    .line 523
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mProgressDialog:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$CustomProgressDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    :cond_0
    monitor-exit p0

    return-void

    .line 521
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setKeepScreenOn()V
    .locals 2

    .prologue
    .line 564
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 567
    :cond_0
    monitor-exit p0

    return-void

    .line 564
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private showToast(I)V
    .locals 8
    .param p1, "count"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 528
    iget-boolean v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mErrOccured:Z

    if-nez v4, :cond_2

    .line 529
    const/4 v0, 0x0

    .line 530
    .local v0, "msg":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 531
    .local v2, "rsrcs":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnox2(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 532
    const/4 v1, 0x0

    .line 533
    .local v1, "name":Ljava/lang/String;
    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mRequestedCmdType:I

    if-ne v4, v6, :cond_4

    .line 534
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mKnox2DestName:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 535
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mKnox2DestName:Ljava/lang/String;

    .line 543
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 544
    const/4 v3, -0x1

    .line 545
    .local v3, "strId":I
    if-le p1, v6, :cond_5

    .line 546
    const v3, 0x7f0c004e

    .line 547
    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 557
    .end local v1    # "name":Ljava/lang/String;
    .end local v3    # "strId":I
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 558
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    invoke-static {v4, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 561
    .end local v0    # "msg":Ljava/lang/String;
    .end local v2    # "rsrcs":Landroid/content/res/Resources;
    :cond_2
    return-void

    .line 537
    .restart local v0    # "msg":Ljava/lang/String;
    .restart local v1    # "name":Ljava/lang/String;
    .restart local v2    # "rsrcs":Landroid/content/res/Resources;
    :cond_3
    const v4, 0x7f0c004f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 539
    :cond_4
    iget v4, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mRequestedCmdType:I

    if-ne v4, v5, :cond_0

    .line 540
    const v4, 0x7f0c009b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 549
    .restart local v3    # "strId":I
    :cond_5
    const v3, 0x7f0c004d

    .line 550
    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 554
    .end local v1    # "name":Ljava/lang/String;
    .end local v3    # "strId":I
    :cond_6
    const/high16 v4, 0x7f0b0000

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v4, p1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private startMove(I)V
    .locals 8
    .param p1, "cmdType"    # I

    .prologue
    const/4 v7, 0x0

    .line 122
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mRequestedCmdType:I

    .line 123
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mArrayList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 124
    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    .line 125
    .local v6, "filter":Landroid/content/IntentFilter;
    const-string v0, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 126
    const-string v0, "com.sec.knox.container.FileRelayExist"

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 127
    const-string v0, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 128
    const-string v0, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 129
    const-string v0, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/KnoxUtil;->isKnox2(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-static {v7}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->setVideoWallOn(Z)V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 134
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mKnox2DestId:I

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;-><init>(Ljava/util/Iterator;Landroid/os/Handler;Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    .line 135
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mMoveKNOXFilesThread:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$MoveKNOXFilesThread;->start()V

    .line 136
    sput-boolean v7, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->popupCreated:Z

    .line 138
    .end local v6    # "filter":Landroid/content/IntentFilter;
    :cond_1
    return-void
.end method


# virtual methods
.method public RemoveFilesFromKNOX(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 576
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v0, "RemoveFilesFromKNOX : send intent to move files to knox"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 577
    return-void
.end method

.method public moveKNOXFiles(Ljava/util/ArrayList;Z)V
    .locals 1
    .param p2, "moveAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p1, :cond_0

    .line 111
    :goto_0
    return-void

    .line 108
    :cond_0
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mArrayList:Ljava/util/ArrayList;

    .line 110
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->startMove(I)V

    goto :goto_0
.end method

.method public moveKNOXFolders(Ljava/util/ArrayList;Z)V
    .locals 1
    .param p2, "moveAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 92
    :cond_0
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mArrayList:Ljava/util/ArrayList;

    .line 94
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->startMove(I)V

    goto :goto_0
.end method

.method public removeKNOXFiles(Ljava/util/ArrayList;Z)V
    .locals 1
    .param p2, "moveAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p1, :cond_0

    .line 119
    :goto_0
    return-void

    .line 116
    :cond_0
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mArrayList:Ljava/util/ArrayList;

    .line 118
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->startMove(I)V

    goto :goto_0
.end method

.method public removeKNOXFolders(Ljava/util/ArrayList;Z)V
    .locals 1
    .param p2, "moveAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p1, :cond_0

    .line 103
    :goto_0
    return-void

    .line 100
    :cond_0
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mArrayList:Ljava/util/ArrayList;

    .line 102
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->startMove(I)V

    goto :goto_0
.end method

.method public setOnMoveKNOXFinishedListener(Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;)V
    .locals 0
    .param p1, "I"    # Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;

    .prologue
    .line 584
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation;->mOnMoveFinishedListener:Lcom/samsung/everglades/video/myvideo/list/MoveKNOXOperation$OnMoveKNOXFinishedListener;

    .line 586
    return-void
.end method
