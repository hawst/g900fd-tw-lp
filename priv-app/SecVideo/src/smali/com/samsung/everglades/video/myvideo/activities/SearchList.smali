.class public Lcom/samsung/everglades/video/myvideo/activities/SearchList;
.super Landroid/app/Activity;
.source "SearchList.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# static fields
.field private static final CALL_SEARCH_METHOD:I = 0x14

.field private static final SEARCH_METHOD_CALL_DELAY:I = 0x64

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public final mHandler:Landroid/os/Handler;

.field private mSearchKey:Ljava/lang/String;

.field private mSearchView:Landroid/widget/SearchView;

.field private mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 164
    new-instance v0, Lcom/samsung/everglades/video/myvideo/activities/SearchList$2;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList$2;-><init>(Lcom/samsung/everglades/video/myvideo/activities/SearchList;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/activities/SearchList;)Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/activities/SearchList;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/activities/SearchList;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/activities/SearchList;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchKey:Ljava/lang/String;

    return-object v0
.end method

.method private callSearchMethodWithDelay()V
    .locals 4

    .prologue
    const/16 v2, 0x14

    .line 158
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 160
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 162
    :cond_0
    return-void
.end method

.method private findAndSetFragmentAgain()V
    .locals 3

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 127
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    if-eqz v0, :cond_0

    .line 128
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 130
    :cond_0
    return-void
.end method

.method private hideSIP()V
    .locals 3

    .prologue
    .line 200
    const-string v1, "SearchList : hideSIP()"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 201
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 202
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 203
    return-void
.end method

.method private setSearchViewActionBar(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 133
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 134
    .local v0, "bar":Landroid/app/ActionBar;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 135
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 137
    new-instance v1, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 138
    .local v1, "lp":Landroid/app/ActionBar$LayoutParams;
    invoke-virtual {v0, p1, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 140
    const v2, 0x7f090038

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SearchView;

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchView:Landroid/widget/SearchView;

    .line 142
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 143
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchView:Landroid/widget/SearchView;

    new-instance v3, Lcom/samsung/everglades/video/myvideo/activities/SearchList$1;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList$1;-><init>(Lcom/samsung/everglades/video/myvideo/activities/SearchList;)V

    invoke-virtual {v2, v3}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    .line 150
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 152
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchKey:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchKey:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 153
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchView:Landroid/widget/SearchView;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchKey:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 155
    :cond_0
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 90
    sget-boolean v0, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 91
    const/high16 v0, 0x7f050000

    const v1, 0x7f050001

    invoke-virtual {p0, v0, v1}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->overridePendingTransition(II)V

    .line 93
    :cond_0
    return-void
.end method

.method public getVideoListFragment()Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 219
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 221
    const/16 v3, 0x64

    if-ne p1, v3, :cond_0

    .line 222
    if-nez p3, :cond_1

    .line 223
    const-string v3, "SearchList - onActivityResult() share via download cancel"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    .line 229
    .local v1, "downloadUriList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 230
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 232
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 233
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 234
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    const-string v3, "video/*"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    const-string v4, "android.intent.extra.STREAM"

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 242
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c005b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 238
    :cond_2
    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    const-string v3, "video/*"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 240
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 56
    .local v0, "args":Landroid/os/Bundle;
    const/4 v4, 0x6

    .line 58
    .local v4, "listType":I
    if-eqz v0, :cond_0

    const-string v6, "list_type"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 59
    :cond_0
    const/16 v6, 0xd

    if-ne v4, v6, :cond_1

    .line 60
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v1

    .line 61
    .local v1, "asfUtil":Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    invoke-virtual {v1, p0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->setContextForUI(Landroid/content/Context;)V

    .line 62
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getSelectedRemoteProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v2

    .line 64
    .local v2, "deviceProvider":Lcom/samsung/android/allshare/media/Provider;
    if-nez v2, :cond_1

    .line 65
    const-string v6, "SearchList(Allshare) : finished"

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->finish()V

    .line 70
    .end local v1    # "asfUtil":Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    .end local v2    # "deviceProvider":Lcom/samsung/android/allshare/media/Provider;
    :cond_1
    const v6, 0x7f040014

    invoke-virtual {p0, v6}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->setContentView(I)V

    .line 72
    const-string v6, "layout_inflater"

    invoke-virtual {p0, v6}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 73
    .local v3, "inflator":Landroid/view/LayoutInflater;
    const v6, 0x7f040015

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 74
    .local v5, "v":Landroid/view/View;
    invoke-direct {p0, v5}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->setSearchViewActionBar(Landroid/view/View;)V

    .line 76
    if-nez p1, :cond_3

    .line 77
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->setupView()V

    .line 82
    :goto_0
    sget-boolean v6, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v6, :cond_2

    .line 83
    const/high16 v6, 0x7f050000

    const v7, 0x7f050001

    invoke-virtual {p0, v6, v7}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->overridePendingTransition(II)V

    .line 85
    :cond_2
    return-void

    .line 79
    :cond_3
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->findAndSetFragmentAgain()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 179
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 180
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->hideSIP()V

    .line 181
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->finish()V

    .line 182
    const/4 v0, 0x1

    .line 185
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 190
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 191
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->hideSIP()V

    .line 192
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 213
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .end local p1    # "newText":Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchKey:Ljava/lang/String;

    .line 214
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->callSearchMethodWithDelay()V

    .line 215
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->hideSIP()V

    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 97
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 98
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v1, v1, -0x101

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 99
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 100
    return-void
.end method

.method public openSIP()V
    .locals 3

    .prologue
    .line 195
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 196
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchView:Landroid/widget/SearchView;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 197
    return-void
.end method

.method protected setupView()V
    .locals 7

    .prologue
    .line 103
    const-string v3, "SearchList : setupView"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 106
    .local v0, "args":Landroid/os/Bundle;
    const/4 v2, -0x1

    .line 107
    .local v2, "type":I
    const/4 v1, -0x1

    .line 109
    .local v1, "slink_id":I
    if-nez v0, :cond_0

    .line 110
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "args":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 113
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_0
    const-string v3, "list_type"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 114
    const-string v3, "s_link_device_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 115
    const-string v3, "search_key"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchKey:Ljava/lang/String;

    .line 117
    const-string v3, "list_type"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    const-string v3, "s_link_device_id"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    const-string v3, "search_key"

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mSearchKey:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    new-instance v3, Lcom/samsung/everglades/video/myvideo/fragments/SearchVideoListFragment;

    invoke-direct {v3}, Lcom/samsung/everglades/video/myvideo/fragments/SearchVideoListFragment;-><init>()V

    iput-object v3, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 121
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    invoke-virtual {v3, v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 122
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    const v4, 0x7f090037

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->mVideoListFragment:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    sget-object v6, Lcom/samsung/everglades/video/myvideo/activities/SearchList;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    .line 123
    return-void
.end method
