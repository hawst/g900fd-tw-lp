.class public final Lcom/samsung/everglades/video/myvideo/common/VZCloudUtils;
.super Ljava/lang/Object;
.source "VZCloudUtils.java"


# static fields
.field private static final ISVZW:Z

.field private static final OPERATOR:Ljava/lang/String; = "VZW"

.field private static final SALES_CODE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "VZCloudUtils"

.field public static final VZW_CLOUD_VIDEOS:Ljava/lang/String; = "com.vcast.mediamanager.ACTION_VIDEOS"

.field private static final VZW_ClOUD_PKGNAME:Ljava/lang/String; = "com.vcast.mediamanager"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/VZCloudUtils;->SALES_CODE:Ljava/lang/String;

    .line 24
    const-string v0, "VZW"

    sget-object v1, Lcom/samsung/everglades/video/myvideo/common/VZCloudUtils;->SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/everglades/video/myvideo/common/VZCloudUtils;->ISVZW:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getVZCloudAppName(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 28
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 29
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    const/4 v2, 0x0

    .line 31
    .local v2, "appName":Ljava/lang/String;
    :try_start_0
    const-string v5, "com.vcast.mediamanager"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 32
    invoke-virtual {v4, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    return-object v2

    .line 33
    :catch_0
    move-exception v3

    .line 34
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "VZCloudUtils"

    const-string v6, "getVZCloudAppName|Package not found"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static isVZCloudEnabled(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 56
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 57
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 59
    .local v1, "pkginfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    const-string v4, "com.vcast.mediamanager"

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 60
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v4, v4, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    .line 63
    :cond_0
    :goto_0
    return v3

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "VZCloudUtils"

    const-string v5, "hasVZCloudpkg|Package not found"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 68
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 69
    .local v1, "vzIntent":Landroid/content/Intent;
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "VZCloudUtils"

    const-string v3, "launchVZCloud| intent not found"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "menuitem"    # I

    .prologue
    .line 40
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 42
    .local v1, "vzCloud":Landroid/view/MenuItem;
    if-nez v1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 46
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/VZCloudUtils;->ISVZW:Z

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/VZCloudUtils;->isVZCloudEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    invoke-static {p0}, Lcom/samsung/everglades/video/myvideo/common/VZCloudUtils;->getVZCloudAppName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "appName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 49
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 50
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method
