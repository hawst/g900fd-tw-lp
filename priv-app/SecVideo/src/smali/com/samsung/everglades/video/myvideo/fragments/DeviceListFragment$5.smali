.class Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;
.super Ljava/lang/Object;
.source "DeviceListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)V
    .locals 0

    .prologue
    .line 650
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;
    .param p1, "x1"    # I

    .prologue
    .line 650
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->callNearbyFileList(I)V

    return-void
.end method

.method private callNearbyFileList(I)V
    .locals 4
    .param p1, "nearbyOnlyPostion"    # I

    .prologue
    const/4 v3, 0x0

    .line 672
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 673
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->setSelectedRemoteDevice(I)V

    .line 677
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 678
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 680
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/everglades/video/myvideo/activities/NearbyFileList;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 681
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "KEY_DEVICE_UDN"

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getSelectedRemoteProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 682
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->startActivity(Landroid/content/Intent;)V

    .line 684
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DevicesFragment - getID() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mASFUtil:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$500(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->getSelectedRemoteProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 685
    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 653
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    # getter for: Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->mNearbyDevicesList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->access$300(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_0

    .line 654
    const-string v1, "DevicesFragment - onItemClick - mDeviceList is null"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 669
    :goto_0
    return-void

    .line 657
    :cond_0
    const-string v1, "DevicesFragment - openNearbyFileList"

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 658
    move v0, p3

    .line 659
    .local v0, "nearbyOnlyPostion":I
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isChinaWifiPopupNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 660
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->this$0:Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5$1;

    invoke-direct {v2, p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5$1;-><init>(Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;I)V

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->showChinaWifiConceptDialog(Landroid/content/Context;Lcom/samsung/everglades/video/myvideo/common/Utils$DoOnPositiveButton;)V

    goto :goto_0

    .line 667
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/fragments/DeviceListFragment$5;->callNearbyFileList(I)V

    goto :goto_0
.end method
